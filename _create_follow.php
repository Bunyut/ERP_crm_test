<?php

/**
  * Create :: 2011-03-07
  * แก้ไข createRemindDate() เปลี่ยน $condition ที่ใช้ค้นหา ตามคอนฟิก  $primary_field
  * จะสร้างเหตุการณ์ที่ department เป็นของ sale = 1x และ cr = 62
  *
  * หมายเหตุ : หากมีการแก้ไขรายละเอียดเกี่ยวกับการสร้าง follow_customer อย่าลืมแก้ไขไฟล์ insert_follow_by_config_date.php* ด้วย
  * เพราะจะได้ทำงานได้ตรงกัน (* เป็นไฟล์ที่ใช้สำหรับสร้างทุกเหตุการณ์ไม่ว่าจะเลยกำหนดหรือไม่ ส่วนใหญ่ใช้สร้างเหตุการณ์ส่งให้ตรีเพชร)
  *
  *  Last update ::
  *  Zan@2011-10-06	 เพิ่มเงื่อนไขการเช็กว่ามีกิจกรรมนี้แล้วหรือยังโดยเช็ก emp_id_card ด้วยกรณีมีการหยุดเพราะจะเปลี่ยนคนดูแล กิจกรรมเดิมอาจค้างอยู่ต้องเช็กผู้ดูแลด้วย
  * 				จะเช็กได้เฉพาะของ Sale เพราะเป็นรหัสประชาชน แต่ของ CR ทำไม่ได้เพราะไม่แน่ใจว่าสถานะ 99 ที่มีอยู่แล้วยกเลิกจริง รึเปล่า
  *
  */

# 1. ต้องเขียนเป็นกิจกรรมของแผนก แยกกับกิจกรรมรายคน  โดยเพิ่มตัวเลือกรายคน หรือแบบกลุ่มแผนกตอน สร้างกิจกรรม
# 2. เพิ่ม POSITION CODE ลงไปในตาราง
# 3. ถ้าบันทึกรายคน follow_customer.department_id ต้องเป็นค่าว่าง  หากเป็นรายกลุ่มแผนก follow_customer.emp_id_card ต้องเป็นค่าว่าง

// ดึงคอนฟิกระบบมาใช้
$config_file = 'config/config.php';
$logfunction = 'function/helper_create_log.php';

$file = 'log_create_follow';

if( ! file_exists($config_file)){// กรณีใช้ Cron job ให้ใช้ path จริง
	$config_file = dirname($_SERVER['PHP_SELF']).'/'.$config_file;
	//$file = dirname($_SERVER['PHP_SELF']).'/'.$file;
	$mainpath 	 = dirname($_SERVER['PHP_SELF']).'/'.'files/log_csv';
	$logfunction = dirname($_SERVER['PHP_SELF']).'/'.$logfunction;
}
require_once($config_file);
require_once($logfunction);//function
//print_r($config);
//exit();

//-end require link
session_write_close();
//print_r($config);
//exit();

//เลือกเปิดปิดเพื่อทดสอบการทำงานของแต่ละส่วน
#--

//DEBUG
$create_follow	= true;//false = ข้ามส่วนของการสร้างการติดตาม
$by_cus_sale 	= true;//false = ข้ามในส่วนของการติดตามคนของ Sale
$by_cus_cr 		= true;//false = ข้ามในส่วนของการติดตามคนของ CR
$by_car_sale 	= true;//false = ข้ามในส่วนของการติดตามรถของ Sale
$by_car_cr 		= true;//false = ข้ามในส่วนของการติดตามรถของ CR

$create_special_event = true;//สร้างกิจกรรมพิเศษ status = 10
//$where_debug = "AND `SaleBookingNo` = '1579900166608'";
#--

//$testLOG = true;//คอมเมนต์ไว้ ถ้าเปิดเป็นทดสอบ LOG
//$limitTest = " LIMIT 50";

$saleDepartment			= "1";
$crDepartment			= "62";

//$config['start_event'] 	= "4";    // cut department
//$config['cut_event'] 	= "2";    // cut department

#-- ถ้าไม่กำหนดจะหาฟิลด์(คีย์ที่ไม่ซ้ำ) ที่ใช้ค้นหา pattern ไม่เจอ
//อาจจะเขียนโค๊ดหา primary key โดยตรงของแต่ละตารางเลย
$primary_field = array(
	'MainCusData'=>'CusNo',
	'Sell'=>'Chassi_No_S',
	'follow_CVIP'=>'cha_no',
	'Booking' => 'Booking_No',
);

$numRecord	= 0;
$numBook = 0;

$start_time = date('Y-m-d H:i:s');

$conn = mysql_connect( $config['ServerName'], $config['UserName'], $config['UserPassword'] );
if (!$conn) die( ' ติดต่อ '. $config['ServerName'] .'ไม่ได้ :: '.  mysql_error() );


if($config['db_base_name']){
	mysql_select_db($config['db_base_name'],$conn)or die("ไม่สามารถเลือกใช้งานฐานข้อมูลได้" );
	@mysql_db_query($config['db_base_name'], "SET NAMES UTF8");
}
if($config['db_emp']){
	mysql_select_db($config['db_emp'],$conn)or die("ไม่สามารถเลือกใช้งานฐานข้อมูลได้".mysql_error());
	@mysql_db_query($config['db_emp'], "SET NAMES UTF8");
}
if($config['db_maincus']){
	mysql_select_db($config['db_maincus'],$conn)or die("ไม่สามารถเลือกใช้งานฐานข้อมูลได้".mysql_error());
	@mysql_db_query($config['db_maincus'], "SET NAMES UTF8");
}
if($config['db_organi']){
	mysql_select_db($config['db_organi'],$conn)or die("ไม่สามารถเลือกใช้งานฐานข้อมูลได้      ".$config['db_organi'].mysql_error());
	@mysql_db_query($config['db_organi'], "SET NAMES UTF8");
}


//-------------------------------//
//-------------------------------//
//-------------------------------//
$LogArray = array();
$LogArray['FILE']			= $_SERVER['PHP_SELF'];
$LogArray['LINE']			= __LINE__;
$LogArray['TITLE'] 			= 'เริ่มต้นการสร้างข้อมูล Followup ';
$LogArray['START_TIME']  	= $start_time;
$LogArray['SUCCESS_TIME']	= '-';
$LogArray['FOLLOW']			= '0';
$LogArray['AllCusLOOP']		= '0';
create_log_file($file, $LogArray, $mainpath);
unset($logArray);
echo nl2br("\n START => LOG $mainpath/$file AT $start_time ");
//-------------------------------//
//-------------------------------//
//-------------------------------//



//*************************************   FUNCTION   ***********************************************//
function setDateFormat($date){//สร้างรูปแบบของวันที่ yyyy-mm-dd
	if(ereg("([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})",$date,$arr) || ereg("([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})",$date,$arr)){
		//ถ้าเป็น xx-xx-yyyy หรือ xx/xx/yyyy
		$y = $arr[3];
		$m = sprintf("%02d",$arr[2]);
		$d = sprintf("%02d",$arr[1]);
	}else if(ereg("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})",$date,$arr) || ereg("([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})",$date,$arr)){
		//ถ้าเป็น yyyy-xx-xx หรือ yyyy/xx/xx
		$y = $arr[1];
		$m = sprintf("%02d",$arr[2]);
		$d = sprintf("%02d",$arr[3]);
	}
	if(($y!="" && $m != "" && $d != "") and ($y!= '0000' && $m != '00' && $d != '00')){
		return $y."-".$m."-".$d; //คืนค่า ปี-เดือน-วัน
	}
}

function getBeforeDate($date,$value,$unit) {//หาค่าวันที่นับจากวันที่ระบุ
	global $config;
	$dayArr = explode("-", $date);
	$day = $dayArr[2];
	$month = $dayArr[1];
	$year = $dayArr[0];
	switch($unit){
		case "day": $day = $day-$value;		break;
		case "week": $day = $day-($value*7);break;
		case "month": $month = $month-$value;break;
		case "year": $year = $year-$value;	break;
	}
	$beforeTime = mktime(0,0,0,$month, $day , $year);
	$beforeDate = date('Y-m-d',$beforeTime);
	return $beforeDate;
}

function getAfterDate($date,$value,$unit) {//หาค่าวันที่ ตามค่าที่กำหนด
	global $config;
	$dayArr = explode("-", $date);
	$day = $dayArr[2];
	$month = $dayArr[1];
	$year = $dayArr[0];
	switch($unit){
		case "day": $day = $day+$value;break;
		case "week": $day = $day+($value*7);break;
		case "month": $month = $month+$value;break;
		case "year": $year = $year+$value;break;
	}
	$afterTime = mktime(0,0,0,$month, $day , $year);
	$afterDate = date('Y-m-d',$afterTime);
	return $afterDate;
}

function createRemindDate($arr,$search_key) {//สร้างวันที่ครบกำหนดแจ้งเตือน(วันที่จริง เพื่อเอาไปคำนวณ ก่อนกี่วัน หลังกี่วัน)
	global $config,$primary_field;
	//ดึงค่าตัวแปรชื่อฟิลด์มาใช้สร้างวันที่
	$sql = "SELECT * FROM $config[DataBaseName].follow_field_pattern WHERE id = '$arr[pattern_id_ref]' AND status != '99' ";
	$result  = mysql_query($sql) or die(create_log_error(__LINE__,$sql));
	$rs = mysql_fetch_assoc($result);
	$fieldname 	= $rs['field_name'];
	$db_name 	= $rs['db_name'];
	$table_name = $rs['table_name'];
	if($fieldname!=""){//ถ้ามี pattern จริง
		//ดึง ค่าวันที่ จากชื่อฐานข้อมูลที่กำหนด		$condition ถ้าเป็นการติดตามคน CusNo = รหัสลูกค้า , ถ้าเป็นรถ Chassi_No_S = หมายเลขคัสซี
		$condition = $primary_field[$table_name]."='$search_key' ";
		$sql2 = "SELECT $fieldname FROM $db_name.$table_name WHERE $condition";
		$result2  = mysql_query($sql2) or die(create_log_error(__LINE__,$sql2));
		if($result2){//บางกรณี $condition อาจไม่ถูกต้อง เพราะหาฟิลด์ CusNo และ Chassi_No_S ไม่เจอ
			$date = mysql_fetch_assoc($result2);

			$rs_date = $date[$fieldname];
			$target_date = setDateFormat($rs_date); //จะคืนค่าเป็น yyyy-mm-dd

			$cal_date = $target_date;
			if($cal_date != '' && $cal_date  != '0000-00-00'){ //ถ้าวันที่ไม่ใช่ค่าว่างๆ
				$remind_type 	= $arr['remind_type'];
				$remind_value 	= $arr['remind_value'];
				$remind_unit 	= $arr['remind_unit'];
				$cal_format 	= $arr['cal_format'];
				$cal_value 		= $arr['cal_value'];
				$cal_unit 		= $arr['cal_unit'];
				$start_value 	= $arr['start_value'];
				$start_unit 	= $arr['start_unit'];
				if($cal_format=="before") {//แจ้งเตือนก่อนหน้า...
					$cal_date = getBeforeDate($cal_date , $cal_value, $cal_unit);

				}else if($cal_format=="after") {//แจ้งเตือนหลังจาก...
					$cal_date = getAfterDate($cal_date , $cal_value, $cal_unit);
				}else if($cal_format=="ondate"){
					$cal_date = $cal_date;
				}

				$remind_date = $cal_date;
				if($remind_type=="alway"){//ถ้าเป็นการติดตามแบบต่อเนื่อง (แจ้งเตือนซ้ำ)
					//วันที่ที่ได้จาก pattern อาจจะเลยมาแล้ว เช่นวันเกิด ดังนั้น ต้องสร้างวันที่ให้เป็นปัจจุบัน
					$remind_range = getBeforeDate(date('Y-m-d'),$start_value,$start_unit);
					while($remind_date < $remind_range){//ถ้าวันที่เริ่มแจ้งเตือนครั้งแรก น้อยกว่า ช่วงการแจ้งเตือนของรอบนี้ จะสร้างการแจ้งเตือนถัดไปตามค่าที่กำหนด
						//เรียกฟังก์ชั่นบวกวันที่ไปตามค่าและหน่วยที่กำหนดให้แจ้งเตือนครั้งต่อไป
						$remind_date = getAfterDate($remind_date, $remind_value, $remind_unit);
					}//วนลูปจนกว่าจะได้การแจ้งเตือนรอบต่อไป
				}//check alway
			}//check date
			@mysql_free_result($result2);
		}else {
			echo "pattern ไม่ถูกต้อง pattern_id = $arr[pattern_id_ref] ($condition)::<br>";
		}//end check result2
	}//end check pattern
	return $remind_date;
}

function insertToFollow($data, $debug=null){
	global $config;
	$status = '0';//สถานะ รายการที่สร้างใหม่
	$process_by = $data['process_by'];
	if($process_by==''){$process_by = 'new';}
	$sql = "INSERT INTO $config[DataBaseName].follow_customer ";
	$sql .= "( event_id_ref, follow_type, chassi_no_s, cus_no, cus_name, emp_id_card,
				 start_date, stop_date,	process_date, process_by, status,
				 source_db_table, source_primary_field, source_primary_id,
				 TIS_Event_Group, TIS_Event_Period, TIS_Event_PeriodCode, send_to_tis";
	$sql .= ")VALUES(";
	$sql .= " '$data[event_id]','$data[follow_type]','$data[chassi_no]','$data[cus_no]','$data[cus_name]','$data[emp_id_card]',
				'$data[start_date]','$data[stop_date]',	NOW(), '$process_by', '$status',
				'$data[source_db_table]', '$data[source_primary_field]', '$data[source_primary_id]',
				'$data[TIS_Event_Group]',	'$data[TIS_Event_Period]', '$data[TIS_Event_PeriodCode]', '$data[send_to_tis]')";
	mysql_query("SET NAMES UTF8");
	if(empty($debug)){
		mysql_query($sql) or die(create_log_error(__LINE__,$sql));
	}else{
		echo nl2br("\n\n".$sql);
	}
}

function insertToFollow_specialE($data, $debug=null){  //pt@2012-01-07  ใช้สำหรับกิจกรรมพิเศษเท่านั้น
	global $config;
	$status = '1';//สถานะ รายการที่สร้างใหม่
	$process_by = $data['process_by'];
	if($process_by==''){$process_by = 'new';}
	$sql = "INSERT INTO $config[DataBaseName].follow_customer ";
	$sql .= "( event_id_ref, follow_type, chassi_no_s, cus_no, cus_name, emp_id_card,
				 start_date, stop_date,	process_date, process_by, status,
				 source_db_table, source_primary_field, source_primary_id,
				 TIS_Event_Group, TIS_Event_Period, TIS_Event_PeriodCode, send_to_tis";
	$sql .= ")VALUES(";
	$sql .= " '$data[event_id]','$data[follow_type]','$data[chassi_no]','$data[cus_no]','$data[cus_name]','$data[emp_id_card]',
				'$data[start_date]','$data[stop_date]',	NOW(), '$process_by', '$status',
				'$data[source_db_table]', '$data[source_primary_field]', '$data[source_primary_id]',
				'$data[TIS_Event_Group]',	'$data[TIS_Event_Period]', '$data[TIS_Event_PeriodCode]', '$data[send_to_tis]')";
	mysql_query("SET NAMES UTF8");
	if(empty($debug)){
		mysql_query($sql) or die(create_log_error(__LINE__,$sql));
	}else{
		echo nl2br("\n\n".$sql);
	}
}

function create_log_error($line,$sql){
	global $mainpath,$file,$start_time,$numRecord;
	$LogArray['FILE']		= __FILE__;
	$LogArray['LINE']		= $line;
	$LogArray['TITLE'] 		= 'ข้อผิดพลาดที่พบขณะรันสคริปต์';
	$LogArray['START_TIME'] = $start_time;
	$LogArray['ERROR_TIME']	= date('Y-m-d H:i:s');
	$LogArray['Num']		= $numRecord;
	$LogArray['MYSQL']		= "<font color='#ff0000'>".mysql_error()."</font>";
	$LogArray['SQL']		= $sql;
	create_log_file('ERROR_'.$file,$LogArray,$mainpath);
	echo "\n Error => ".$LogArray['MYSQL'];
	unset($logArray);
}

/********************************* END FUNCTION ********************************/



/*********************************** BEGIN ************************************/
if(!$testLOG){

	$today = date('Y-m-d');
	$num_cus = 0;
	if($create_follow==true){

		//เงื่อนไขคือ เลือกเฉพาะลูกค้าที่มีพนักงานรับผิดชอบ
		$cusSQL = "SELECT CusNo,Cus_Name,Cus_Surename,Resperson FROM $config[Cus].MainCusData ";
		$cusSQL .= "WHERE (Resperson != '' AND Resperson IS NOT NULL) ORDER by CusNo DESC $limitTest ";
		mysql_query('SET NAMES UTF8');
		$cusQry = mysql_query($cusSQL) or die(create_log_error(__LINE__,$cusSQL));
		while($cus = mysql_fetch_assoc($cusQry)) {//วนลูปรายชื่อลูกค้า

			$num_cus++;

			$cusNo 		= "";
			$department = "";
			$cusNo 		= $cus['CusNo'];
			$cusName 	= $cus['Cus_Name']." ".$cus['Cus_Surename'];
			$empID		= $cus['Resperson'];//รหัสพนักงานผู้ดูแลลูกค้า

			//$department = cutDepartment($empID);
			if($by_cus_sale==true)//เปิดไว้ถึงจะทำงาน
			{
				//ลิสต์เหตุการที่เกี่ยวกับคน follow_type = 1, department = 1x (sale)
				$evSQL = "SELECT * FROM $config[DataBaseName].follow_main_event ";
				$evSQL .= "WHERE follow_type = '1' AND MID(department,1,1) = '$saleDepartment' AND pattern_id_ref != '' AND status != '99' AND status != '10' ";
				$evQry = mysql_query($evSQL) or die(create_log_error(__LINE__,$evSQL));
				while($event = mysql_fetch_assoc($evQry)) {
					$remind_date = '';$data1		 = '';
					$data1 						= $event;//เก็บอาร์เรย์ลงใน data1
					$data1['event_id']		= $event['id'];
					$data1['follow_type']	= $event['follow_type'];
					$data1['cus_no']		= $cusNo;
					$data1['cus_name']		= $cusName;
					$data1['emp_id_card']	= $empID;

					/* ตรวจสอบว่าเหตุการณ์นี้เคยสร้างการแจ้งเตือนรึยัง */
					$search = "SELECT COUNT(id) FROM $config[DataBaseName].follow_customer ";
					$search .= "WHERE event_id_ref = '$data1[event_id]' AND cus_no = '$cusNo' AND emp_id_card = '$empID' ";
					$follow_cus = mysql_query($search) or die(create_log_error(__LINE__,$search));
					$rsF = mysql_fetch_array($follow_cus);
					$find = $rsF[0];
					if($find < 1){//ถ้ายังไม่ได้สร้าง

						$remind_date = createRemindDate($event,$cusNo);//ฟังก์ชั่นสร้างวันที่แจ้งเตือนสำหรับแต่ละเหตุการณ์

						if($remind_date != '' && $remind_date != '0000-00-00'){
							$data1['start_date'] = getBeforeDate($remind_date,$event['start_value'],$event['start_unit']);//เริ่มแจ้งเตือน
							$data1['stop_date']  = getAfterDate($remind_date,$event['stop_value'], $event['stop_unit']);//สิ้นสุดการแจ้งเตือน
							//ตรวจสอบถ้ายังไม่เลยกำหนดสิ้นสุด ถึงจะบันทึก (เพราะถ้าเลยกำหนด insert เข้าไปก้ไม่มีการแจ้งเตือน)
							if($data1['stop_date'] >= $today ){
								insertToFollow($data1);//insert ข้อมูลลงใน follow_customer
								$numRecord++;
							}
						}
					}//end find

				} //วนลูป เหตุการณ์ต่อไป ที่เกี่ยวกับคน
				@mysql_free_result($evQry);
			}//if by_cus_sale

			//CR เหตุการที่เกี่ยวกับคน follow_type = 1, department = 62 (cr)
			if($by_cus_cr==true)//เปิดไว้ถึงจะทำงาน
			{
				$ev_crSQL = "SELECT * FROM $config[DataBaseName].follow_main_event ";
				$ev_crSQL .= "WHERE follow_type = '1' AND department = '$crDepartment' AND pattern_id_ref != '' AND status != '99'  AND status != '10'";
				$ev_crQry = mysql_query($ev_crSQL) or die(create_log_error(__LINE__,$ev_crSQL));
				while($cr_event = mysql_fetch_assoc($ev_crQry)) {
					$remind_date = '';$cr_data	 = '';
					$cr_data					= $cr_event;//เก็บอาร์เรย์
					$cr_data['event_id']	= $cr_event['id'];
					$cr_data['follow_type']	= $cr_event['follow_type'];
					$cr_data['cus_no']		= $cusNo;
					$cr_data['cus_name']	= $cusName;
					$cr_data['emp_id_card']	= $crDepartment;//cr เป็นคนทำ ไม่ระบุว่าเป็นใคร

					/* ตรวจสอบว่าเหตุการณ์นี้เคยสร้างการแจ้งเตือนรึยัง */
					$cr_search = "SELECT COUNT(id) FROM $config[DataBaseName].follow_customer ";
					$cr_search .= "WHERE event_id_ref = '$cr_data[event_id]' AND cus_no = '$cusNo' ";
					$cr_follow_cus = mysql_query($cr_search) or die(create_log_error(__LINE__,$cr_search));
					$cr_rsF = mysql_fetch_array($cr_follow_cus);
					$cr_find = $cr_rsF[0];
					if($cr_find < 1){//ถ้ายังไม่ได้สร้าง

						$remind_date = createRemindDate($cr_event,$cusNo);//ฟังก์ชั่นสร้างวันที่แจ้งเตือนสำหรับแต่ละเหตุการณ์

						if($remind_date != '' && $remind_date != '0000-00-00'){
							$cr_data['start_date'] = getBeforeDate($remind_date, $cr_event['start_value'],$cr_event['start_unit']);//เริ่มแจ้งเตือน
							$cr_data['stop_date']  = getAfterDate($remind_date,$cr_event['stop_value'],$cr_event['stop_unit']);//สิ้นสุดการแจ้งเตือน
							//ตรวจสอบถ้ายังไม่เลยกำหนดสิ้นสุด ถึงจะบันทึก (เพราะถ้าเลยกำหนด insert เข้าไปก้ไม่มีการแจ้งเตือน)
							if($cr_data['stop_date'] >= $today ){
								insertToFollow_specialE($cr_data);//insert ใน follow_customer   //pt@2012-01-07  แก้ไขการเรียกฟังก์ชั่น
								$numRecord++;
							}
						}
					}

				}//cr loop
				@mysql_free_result($ev_crQry);
			}//if by_cus_cr

			//เช็กว่าซื้อรถหรือไม่ CAR

			$sellSQL  = "SELECT Chassi_No_S FROM $config[DataBaseName].Sell ";
			$sellSQL .= "WHERE IF(SUBSTRING_INDEX(cusBuy, '_','1') !='', SUBSTRING_INDEX(cusBuy, '_','1'),CusNo) = '$cusNo'";//ถ้าตัด cusBuy แล้วได้ค่าว่างๆ ให้ใช้ CusNo
			$result2  = mysql_query($sellSQL) or die(create_log_error(__LINE__,$sellSQL));
			$numCar   = mysql_num_rows($result2);
			if($numCar > 0){
				while($sell = mysql_fetch_assoc($result2)){
					$chassi_no = "";
					$chassi_no = $sell['Chassi_No_S'];
					if($chassi_no){//ถ้าซื้อรถจะได้เลขคัสซี

					// ลิสต์เหตุการที่เกี่ยวกับรถ follow_type = 2 , department = 1x (sale)
						if($by_car_sale==true)//เช็กการเปิด-ปิดเพื่อทดสอบ
						{
							$evSQL2 = "SELECT * FROM $config[DataBaseName].follow_main_event ";
							$evSQL2 .= "WHERE follow_type = '2' AND MID(department,1,1) = '$saleDepartment' AND pattern_id_ref != '' AND status != '99'  AND status != '10'";
							$evResult2 = mysql_query($evSQL2) or die(create_log_error(__LINE__,$evSQL2));
							while($event2 = mysql_fetch_assoc($evResult2)) {
								$remind_date=$data2 = '';
								$data2						= $event2;//เก็บอาร์เรย์
								$data2['event_id']		= $event2['id'];
								$data2['follow_type']	= $event2['follow_type'];
								$data2['cus_no']		= $cusNo;
								$data2['cus_name']		= $cusName;
								$data2['chassi_no']		= $chassi_no;
								$data2['emp_id_card']	= $empID;

								$search2 = "SELECT COUNT(id) FROM $config[DataBaseName].follow_customer ";
								$search2 .= "WHERE event_id_ref = '$data2[event_id]' AND chassi_no_s = '$data2[chassi_no]' AND emp_id_card = '$empID'";
								$follow_car = mysql_query($search2) or die(create_log_error(__LINE__,$search2));
								$rsF2 = mysql_fetch_array($follow_car);
								$find2 = $rsF2[0];
								/* ตรวจสอบว่าเหตุการณ์นี้เคยสร้างการแจ้งเตือนรึยัง */

								if($find2 < 1){
									//ฟังก์ชั่นสร้างวันที่แจ้งเตือนครั้งแรกสำหรับแต่ละเหตุการณ์  $event2 จะเก็บข้อมูล event ทุกฟิลด์
									$remind_date = createRemindDate($event2,$data2['chassi_no']);

									//ถ้ามีวันที่แล้วถึง insert ข้อมูล (บางเหตุการณ์ต้องรอให้กรอกวันที่ก่อน เช่นจ่ายงวดแรก ทะเบียนหมดอายุ)
									if($remind_date != '' && $remind_date != '0000-00-00'){
										$data2['start_date'] = getBeforeDate($remind_date, $event2['start_value'], $event2['start_unit']);//เริ่มแจ้งเตือน
										$data2['stop_date']  = getAfterDate($remind_date, $event2['stop_value'], $event2['stop_unit']);//สิ้นสุดการแจ้งเตือน
										//ตรวจสอบถ้าไม่เลยกำหนดการถึงจะ insert
										if($data2['stop_date'] >= $today ){
											insertToFollow($data2);//insert ใน follow_customer
											$numRecord++;
										}
									}
								}
							}//วนลูป เหตุการณ์ต่อไป ที่เกี่ยวกับรถ (ของ sale)
							@mysql_free_result($evResult2);
						}// if by_car_sale


						//เหตุการณ์ของ CR
						if($by_car_cr==true)//เช็กการเปิด-ปิดเพื่อทดสอบ
						{
							$ev_crSQL = "SELECT * FROM $config[DataBaseName].follow_main_event ";
							$ev_crSQL.="WHERE follow_type = '2' AND department = '$crDepartment' AND pattern_id_ref != '' AND status != '99' AND status != '10' ";
							$ev_crResult = mysql_query($ev_crSQL) or die(create_log_error(__LINE__,$ev_crSQL));
							while($eventCR = mysql_fetch_assoc($ev_crResult)) {
								$remind_date=$cr_data = '';
								$cr_data  					= $eventCR;//เก็บค่าอาร์เรย์
								$cr_data['event_id']		= $eventCR['id'];
								$cr_data['follow_type']	= $eventCR['follow_type'];
								$cr_data['cus_no']		= $cusNo;
								$cr_data['cus_name']		= $cusName;
								$cr_data['chassi_no']	= $chassi_no;
								$cr_data['emp_id_card']	= $crDepartment;//cr เป็นคนทำ ไม่ระบุว่าเป็นใคร

								$search3 = "SELECT COUNT(id) FROM $config[DataBaseName].follow_customer ";
								$search3 .= "WHERE event_id_ref = '$cr_data[event_id]' AND chassi_no_s = '$cr_data[chassi_no]' ";
								$follow_car2 = mysql_query($search3) or die(create_log_error(__LINE__,$search3));
								$rsCR = mysql_fetch_array($follow_car2);
								$cr_find3 = $rsCR[0];
								/* ตรวจสอบว่าเหตุการณ์นี้เคยสร้างการแจ้งเตือนรึยัง */
								if($cr_find3 < 1){
									//ฟังก์ชั่นสร้างวันที่แจ้งเตือนครั้งแรกสำหรับแต่ละเหตุการณ์  $eventCR จะเก็บข้อมูล event ทุกฟิลด์
									$remind_date = createRemindDate($eventCR,$cr_data['chassi_no']);
									//ถ้ามีวันที่แล้วถึง insert ข้อมูล (บางเหตุการณ์ต้องรอให้กรอกวันที่ก่อน เช่นจ่ายงวดแรก ทะเบียนหมดอายุ)
									if($remind_date != '' && $remind_date != '0000-00-00'){
										$cr_data['start_date'] = getBeforeDate($remind_date, $eventCR['start_value'], $eventCR['start_unit']);//เริ่มแจ้งเตือน
										$cr_data['stop_date']  = getAfterDate($remind_date, $eventCR['stop_value'],$eventCR['stop_unit']);//สิ้นสุดการแจ้งเตือน
										//insertToFollow($cr_data);//insert ข้อมูลลงใน follow_customer
										//$numRecord++;
										//ตรวจสอบถ้าไม่เลยกำหนดการทำ  จะ insert
										if($cr_data['stop_date'] >= $today ){
											insertToFollow($cr_data);//insert ข้อมูลลงใน follow_customer
											$numRecord++;
										}
									}
								}
							}// วนลูปเหตุการณ์ต่อไปของ CR
							@mysql_free_result($ev_crResult);
						}//if by_car_cr

					}//end check chassi
				} //วนลูป รถ คันต่อไป

			}//end if numCar
			@mysql_free_result($result2);

		} //วนลูป ลูกค้า คนต่อไป
		@mysql_free_result($cusQry);
		echo "=> ". number_format($numRecord)." Record.";


		//-------------------------------//
		$LogArray = array();
		$LogArray['FILE']			= $_SERVER['PHP_SELF'];
		$LogArray['LINE']			= __LINE__;
		$LogArray['TITLE'] 			= 'สิ้นสุดการสร้างข้อมูล Followup ';
		$LogArray['START_TIME']  	= '-';
		$LogArray['SUCCESS_TIME']	= date('Y-m-d H:i:s');
		$LogArray['FOLLOW']			= $numRecord;
		$LogArray['AllCusLOOP']		= $num_cus;
		create_log_file($file, $LogArray, $mainpath);
		unset($logArray);
		echo nl2br("\n START => LOG $mainpath/$file AT $start_time ");
		//------------------------------//

	}//if $create_follow



	if($create_special_event==true)//เปิดไว้ถึงจะทำงาน
	{
		$LogArray = array();
		$LogArray['FILE']			= $_SERVER['PHP_SELF'];
		$LogArray['LINE']			= __LINE__;
		$LogArray['TITLE'] 			= 'เริ่มการสร้างข้อมูล การติดตามเฉพาะ';
		$LogArray['START_TIME']  	= date('Y-m-d H:i:s');
		$LogArray['SUCCESS_TIME']  	= '-';
		$LogArray['Booking']		= 0;
		$LogArray['AllCusLOOP']		= 0;
		create_log_file('SPECIAL_FOLLOW_'.$file, $LogArray, $mainpath);
		unset($logArray);
		echo nl2br("\n BEFORE create_special_event => LOG $mainpath/$file");

		//ลูกค้าที่กำลังจองรถอยู่
		$allcusno = array();
		$SaleBookingNo = array();
		$sql = "SELECT Booking_No,`B_cusBuy`,SaleBookingNo,Booking_Date FROM $config[DataBaseName].`Booking`
		WHERE status NOT IN ('9','99') AND B_cusBuy != '' AND `SaleBookingNo` != '' $where_debug";
		$Qry = mysql_query($sql) or die(create_log_error(__LINE__,$sql));
		while($bk = mysql_fetch_assoc($Qry)) {
			$allcusno[$bk['B_cusBuy']] = $bk['B_cusBuy'];
			$SaleBookingNo[$bk['B_cusBuy']][$bk['Booking_No']] = $bk['SaleBookingNo'];
			$BookingDate[$bk['Booking_No']] = $bk['Booking_Date'];
		}
		$in_cusno = "'".implode("', '",$allcusno)."'";
		echo 'COUNT '. count($allcusno).' :: IN booking status NOT IN (\'9\',\'99\') <br>'. $in_cusno.'<br><br>';

		//--UPDATE เป็นกิจกรรมที่ไม่ต้องสร้างต่อเมื่อบันทึกข้อมูล
	//$ev_SQL = "UPDATE $config[DataBaseName].follow_main_event SET remind_type = 'once' WHERE status = '10' AND id = '62' ";
	//	mysql_query($ev_SQL);
		//--

		//เงื่อนไขคือ เลือกเฉพาะลูกค้าที่มีการจองรถอยู่
		//ไม่สนใจการยกเลิกกิจกรรม จะทำจนกว่ามีการยกเลิกการจองเท่านั้น
		$cusSQL = "SELECT CusNo,Cus_Name,Cus_Surename FROM $config[Cus].MainCusData ";
		$cusSQL .= "WHERE CusNo IN ($in_cusno) $limitTest ";
		mysql_query('SET NAMES UTF8');
		$cusQry = mysql_query($cusSQL) or die(create_log_error(__LINE__,$cusSQL));
		$num_cus = 0;
		$update = 0;
		$numBook = 0;
		while($cus = mysql_fetch_assoc($cusQry)) {//วนลูปรายชื่อลูกค้า
			$cusNo 		= $cus['CusNo'];
			$cusName 	= $cus['Cus_Name']." ".$cus['Cus_Surename'];
			//สร้างการติดตาม Booking
			$ev_SQL = "SELECT * FROM $config[DataBaseName].follow_main_event ";
			$ev_SQL .= "WHERE status = '10' AND id = '62' ";
			$ev_Qry = mysql_query($ev_SQL) or die(create_log_error(__LINE__,$ev_SQL));
			while($event = mysql_fetch_assoc($ev_Qry)) {
				$remind_date = '';$data1		 = '';
				$data1 					= $event;//เก็บอาร์เรย์ลงใน data1
				$data1['event_id']		= $event['id'];
				$data1['follow_type']	= $event['follow_type'];
				$data1['cus_no']		= $cusNo;
				$data1['cus_name']		= $cusName;
				$data1['process_by']		= 'special';
				//ให้ทำตลอดจนกว่า จะขายหรือยกเลิกจอง
				$event['remind_type'] = 'alway';

				//วนลูป ลูกค้าหนึ่งคนอาจจะมีเซลที่ทำการจองหลายคน (อาจจะ!!)
				$num = count($SaleBookingNo[$cusNo]);
				if($num>1){
					echo '<br><b>CusNo : '.$cusNo.' : Book '.$num.' >> </b>
					<font style="font-size:12px;">';print_r(array_keys($SaleBookingNo[$cusNo]));
					echo '</font><br>';
				}
				foreach ($SaleBookingNo[$cusNo] as $booking_no=>$empID){
					$data1['emp_id_card']	= $empID;
					$remind_date = createRemindDate($event,$booking_no);//ฟังก์ชั่นสร้างวันที่แจ้งเตือนสำหรับแต่ละเหตุการณ์
					//if($booking_no=='13890' || $cusNo=='5236051'){
					//	echo '<br>DATE :: '.$cusNo.' :: '.$booking_no.' :: '.$BookingDate[$booking_no].' :: ',$remind_date;
					//}
					if($remind_date != '' && $remind_date != '0000-00-00'){
						$data1['start_date'] = getBeforeDate($remind_date,$event['start_value'],$event['start_unit']);//เริ่มแจ้งเตือน
						$data1['stop_date']  = getAfterDate($remind_date,$event['stop_value'], $event['stop_unit']);//สิ้นสุดการแจ้งเตือน

						//ตรวจสอบถ้ายังไม่เลยกำหนดสิ้นสุด ถึงจะบันทึก (เพราะถ้าเลยกำหนด insert เข้าไปก้ไม่มีการแจ้งเตือน)
						if($data1['stop_date'] >= $today ){
							$data1['source_db_table'] = $config['DataBaseName'].'.Booking';//ชื่อฐานข้อมูล.ตาราง
							$data1['source_primary_field'] = 'Booking_No';// ชื่อฟิลด์ที่เป็นคีย์หลัก
							$data1['source_primary_id'] = $booking_no;//รหัสคีย์หลัก

							/* ตรวจสอบว่าเหตุการณ์นี้เคยสร้างการแจ้งเตือนรึยัง */
							//เงื่อนไขคือ ค้นหารายการใหม่ และรายการที่วันที่สิ้นสุดตรงกับ วันที่สิ้นสุดใหม่
							$search = "SELECT id FROM $config[DataBaseName].follow_customer ";
							$search .= "WHERE event_id_ref = '$data1[event_id]' AND cus_no = '$cusNo' AND emp_id_card = '$empID' AND source_primary_id = '$data1[source_primary_id]' AND (status IN ('0','1','3','4') OR stop_date >= '$data1[stop_date]' ) ";
							$follow_cus = mysql_query($search) or die(create_log_error(__LINE__,$search));
							$rsF = mysql_fetch_assoc($follow_cus);

						//	if($booking_no=='13890' || $cusNo=='5236051'){
						//		echo "<br>$cusNo :: $rsF[id]";
						//	}

							if( !$rsF['id'] ){//ถ้ายังไม่ได้สร้าง
								insertToFollow($data1,false);//insert ข้อมูลลงใน follow_customer ,true = debug, false = query
								$numBook++;
								//echo "<br>[$numRecord]";
							}else{//ไม่ต้องสร้าง
								//echo '<br>'.++$nnn.' follow : '.mysql_num_rows($follow_cus);
								//อัพชั่วคราว
								//$sql = "UPDATE $config[DataBaseName].follow_customer SET
								//source_db_table = '$data1[source_db_table]', source_primary_field = '$data1[source_primary_field]',
								//source_primary_id = '$data1[source_primary_id]' WHERE id = '$rsF[id]' AND source_primary_id = 0 ";
								//echo '<br>'.$sql;
								//$ress2 = mysql_query($sql) or die(mysql_error());
								//if($ress2 && mysql_affected_rows() > 0){
									//echo '<br>affected :: '.$rsF['id'].' => '.mysql_affected_rows();
								//	$update++;
								//}
							}
						}//check date

					}//end
				}//foreach
			}//while event

			$num_cus++;
		}//while Cus
		echo nl2br("\n\n Create Booking tracking Success $numBook record \n UPDATE source data $update Record.(default no update)");
		//---------


		$LogArray = array();
		$LogArray['FILE']			= $_SERVER['PHP_SELF'];
		$LogArray['LINE']			= __LINE__;
		$LogArray['TITLE'] 			= 'สิ้นสุดการสร้างข้อมูล';
		$LogArray['START_TIME']  	= '-';
		$LogArray['SUCCESS_TIME']  	= date('Y-m-d H:i:s');
		$LogArray['Booking']		= $numBook;
		$LogArray['AllCusLOOP']		= $num_cus;
		create_log_file('SPECIAL_FOLLOW_'.$file, $LogArray, $mainpath);



	}//special true

}//Check TEST LOG

//*********** LOG Follow ************//

//-------------------------------//
$LogArray = array();
$LogArray['FILE']			= $_SERVER['PHP_SELF'];
$LogArray['LINE']			= __LINE__;
$LogArray['TITLE'] 			= 'สิ้นสุดการรันสคริปต์ create_follow.php';
$LogArray['START_TIME']  	= $start_time;
$LogArray['SUCCESS_TIME']	= date('Y-m-d H:i:s');
$LogArray['FOLLOW']			= 'END';
$LogArray['AllCusLOOP']		= 'END';
create_log_file($file, $LogArray, $mainpath);
unset($logArray);
echo nl2br("\n END => LOG $mainpath/$file");
//------------------------------//

@mysql_close( $conn );


?>