<?php
/**
 * Create by: Nuy
 * Create date: 27.07.2011
 * Last Modify: 30.07.2011@Nuy
 * Last Modify: pt@2011-11
 * Description: หน้าการจัดการแบบฟอร์ม CR สำหรับคำนวณคะแนนควาพึงพอใจ SSI
 */
 
require_once("config/general.php");
require_once("function/general.php");

$thisPage 	= "ssi_setup_form";

## สั่งทำงานโค้ด
$doCode = ""; 		## ทำงาน
// $doCode = "No";  ## ไม่ทำงาน แสดงโค้ด Insert, Update, Delete
// $doCode = "All"; ## ทำงาน แสดงโค้ด Select, Insert, Update, Delete

Conn2DB();

$func = $_REQUEST['func'];

switch($func){
	default:
		$types = $_GET['types'];
		$thisDate = date('Y-m-d');
		if(!$types || $types=='this'){ $where = "WHERE (start_date<='$thisDate' AND start_date!='0000-00-00') AND (stop_date>'$thisDate' OR stop_date='0000-00-00')"; }

		$sql = "SELECT ssi_id,ssi_form_type,ssi_form_name,start_date,stop_date,active_maincus,(SELECT option_count FROM $config[db_base_name].follow_ssi_question_weight WHERE ssi_id = follow_ssi_form.ssi_id LIMIT 1) AS option_count FROM $config[db_base_name].follow_ssi_form $where ORDER BY start_date DESC";
		$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$row = mysql_num_rows($que);

		if($row>0){
			$no = 0; $listQues = '';
			while($fe = mysql_fetch_array($que)){
				$no++;

				//แสดงสถานะ
				if(($fe['start_date']<=$thisDate && $fe['start_date']!='0000-00-00') && ($fe['stop_date']>$thisDate || $fe['stop_date']=='0000-00-00')){ $status = "<font color='green'>ใช้งาน</font>"; }else if($fe['start_date']=='0000-00-00'){ $status = "<font color='blue'>ยังไม่ได้ใช้</font>"; }else{ $status = "<font color='red'>ยกเลิก</font>"; }
				//วันที่ไทย
				if($fe['start_date'] && $fe['start_date']!='0000-00-00'){ $sDate = standard2thai($fe['start_date']); }else{ $sDate = '-'; }
				//วันที่ไทย
				if($fe['stop_date'] && $fe['stop_date']!='0000-00-00'){ $fDate = standard2thai($fe['stop_date']); }else{ $fDate = '-'; }
				//ชนิดแบบสัมภาษณ์
				if($fe['option_count']){ $type = "B"; }else{ $type = "A"; }
				//สถานะ Active Maincus
				if($fe['ssi_form_type'] == 'A' && $fe['start_date']<=$thisDate && $fe['start_date']!='0000-00-00' && ($fe['stop_date']>$thisDate || $fe['stop_date']=='0000-00-00')){
					if ($fe['active_maincus']==''){
						$act_maincus = '<img  src="img/box_inactive.png" style="cursor: pointer;" border="0" title="Inactive" onclick="if(confirm(\'ยืนยันการกำหนดแบบฟอร์มนี้เพื่อใช้คำนวนค่าความพึงพอใจของลูกค้า\')){ activeForm(\'$thisPage.php\',\'id=$fe[ssi_id]&func=activeFrom\',\'$_GET[types]\'); }">';
					}else{
						$act_maincus = '<img src="img/box_active.png" title="Active" style="cursor: pointer;" border="0">'; 
					}
				}else{
					$act_maincus = '&nbsp&nbsp';
				}

				$val = "&id=".$fe['ssi_id']."&type=".$type;

				//สถานะใช้งาน มีปุ่มยกเลิก
				if($status == "<font color='green'>ใช้งาน</font>"){
					$cancel = '<img src="img/delete.png" onclick="if(confirm(\'ยืนยันการยกเลิก\')){ cancelFrom(\'$thisPage.php\',\'id=$fe[ssi_id]&func=cancelFrom\',\'tr#tr_data_$fe[ssi_id]\'); }" style="cursor: pointer;" border="0">';
				}else{
					$cancel = "-";
				}
				$listQues .= $tpl->tbHtml( $thisPage.'.html', 'listQues' );
			}
		}else{
			$listQues = $tpl->tbHtml( $thisPage.'.html', 'noData' );
		}
		$today = date("Y-m-d");
		$sql_chkA_vtive = "SELECT ssi_id FROM $config[db_base_name].follow_ssi_form WHERE active_maincus = 'active' AND (stop_date > '$today' OR stop_date = '0000-00-00' ) ";//AND start_date <= '$today' 
		$que_ca = $logDb->queryAndLogSQL( $sql_chkA_vtive, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$chkA_vtive = mysql_num_rows($que_ca);
		if(!$chkA_vtive){ 
			$setAtoActive = '<text align ="left" style="color:red; ">***ท่านยังไม่ได้เลือกActiveแบบฟอร์มใดเพื่อใช้คำนวนค่าความพึงพอใจ กรุณาคลิก&nbsp;<img src="img/box_inactive.png" border="0">&nbsp;หน้าชื่อแบบฟอร์มเพื่อActiveครับ </text>';
		}else {
			$setAtoActive = '';
		}
		// $listData = $tpl->tbHtml( $thisPage.'.html', 'listData' );
		if(!$types){ echo $tpl->tbHtml( $thisPage.'.html', 'manageQuestion' ); }else{ echo $listQues; }
	break;
	case 'createFrom':
		$types = $_GET['types'];
		$type = $_GET['type'];
		$id = $_GET['id'];

		if($type){ $types = $type; }
		if(!$types){ $types = 'A'; }
		if($types=='B'){ $where = " AND (input_type = 'radio' OR input_type = 'selectbox')"; }else{ $where = " AND input_type = 'pleasure_value'"; }
		
		## เคยเช็คคำถามของ CR เท่านั้น
		## department = '62' AND 
		$event = "SELECT id, event_title, department FROM $config[db_base_name].follow_main_event WHERE status != '99' ORDER BY id ASC";
		// echo "<font color='#0000FF'>FILE :: ".__FILE__." >> LINE :: ".__LINE__."</font><br/>".$event."<br/><br/>";
		$queEvent = $logDb->queryAndLogSQL( $event, " FILE : ".__FILE__." LINE : ".__LINE__."" );

		$run = 0;
		while($feEvent = mysql_fetch_array($queEvent)){

			$noEvent = $run+1;

			$Main = "SELECT question_title,id FROM $config[db_base_name].follow_main_question WHERE event_id_ref = '".$feEvent['id']."' AND status != '99' ORDER BY id ASC";
			// echo "<font color='#0000FF'>FILE :: ".__FILE__." >> LINE :: ".__LINE__."</font><br/>".$Main."<br/><br/>";
			$queMain = $logDb->queryAndLogSQL( $Main, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$rowMain = mysql_num_rows($queMain);

			$no = 0; $ques = '';
			while($feMain = mysql_fetch_array($queMain)){

				$titleQues = $feMain['question_title'];
				$idQues = $feMain['id'];
				$number = $no+1;

				$sql = "SELECT list_title,id,REPLACE(answer_option,\"'\",'') AS answer_option FROM $config[db_base_name].follow_question_list WHERE main_question_id = '$idQues'$where AND status!=99 ORDER BY id ASC";
				$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$row = mysql_num_rows($que);

				$noSub = 0; $subQues = '';
				while($fe = mysql_fetch_array($que)){

					$idSub = $fe['id'];
					$titleSub = $fe['list_title'];
					$subOp = strip_tags($fe['answer_option']);
					//$weightSub ค่าน้ำหนัก $txtWSub น้ำหนักที่แสดงด้านขวา
					if($types=='A'){ $weightSub = $subOp; $txtWSub = " ** ".$subOp; }

					$subQues .= $tpl->tbHtml( $thisPage.'.html', 'listSub' );

				$noSub++;
				}

				if($row>0){ $ques .= $tpl->tbHtml( $thisPage.'.html', 'listQuestion' ); $no++; }
			}

			if($rowMain>0 && $row>0){ $events .= $tpl->tbHtml( $thisPage.'.html', 'listEvent' ); $run++; }
		}


		/*$Main = "SELECT question_title,id FROM $config[db_base_name].follow_main_question WHERE status!=99 ORDER BY id ASC";
		$queMain = $logDb->queryAndLogSQL( $Main, " FILE : ".__FILE__." LINE : ".__LINE__."" );

		$i = 0; $no = 0;
		while($feMain = mysql_fetch_array($queMain)){

			$titleQues = $feMain['question_title'];
			$idQues = $feMain['id'];
			$number = $no+1;

			$sql = "SELECT list_title,id,REPLACE(answer_option,\"'\",'') AS answer_option FROM $config[db_base_name].follow_question_list WHERE main_question_id = '$idQues'$where AND status!=99 ORDER BY id ASC";
			$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$row = mysql_num_rows($que);

			$noSub = 0; $subQues = '';
			while($fe = mysql_fetch_array($que)){

				$idSub = $fe['id'];
				$titleSub = $fe['list_title'];
				$subOp = $fe['answer_option'];

				$subQues .= $tpl->tbHtml( $thisPage.'.html', 'listSub' );

			$noSub++;
			}

			if($row>0){ $ques .= $tpl->tbHtml( $thisPage.'.html', 'listQuestion' ); $no++; }
		$i++;
		}*/

		//แก้ไข
		if($id){
			//ดึงข้อมูลฟอร์ม กับ คำถามหลัก
			$sql = "SELECT follow_ssi_form.ssi_id, follow_ssi_form.ssi_form_name, follow_ssi_form.start_date, follow_ssi_form.stop_date, follow_ssi_title.ssi_title_id, follow_ssi_title.ssi_title_name, follow_ssi_title.weight, follow_ssi_title.ssi_title_code FROM $config[db_base_name].follow_ssi_form JOIN $config[db_base_name].follow_ssi_title ON follow_ssi_form.ssi_id = follow_ssi_title.ssi_id WHERE follow_ssi_form.ssi_id = '$id' ORDER BY follow_ssi_form.ssi_id,follow_ssi_title.ssi_title_id ASC";
			$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );

			while($fe = mysql_fetch_array($que)){
	 
				$start_Date_ = explode('-',$fe['start_date']);
				$stop_Date_ = explode('-',$fe['stop_date']);
				$start_Date = $start_Date_[2].'-'.$start_Date_[1].'-'.$start_Date_[0];
				$stop_Date = $stop_Date_[2].'-'.$stop_Date_[1].'-'.$stop_Date_[0];
				
				/*
				//2011-09-30 pt ผิดจุด!
				if($start_Date != '00-00-0000'){
					$r_only_start = 'readonly = "readonly"'; 		//ไม่ให้แก้วันที่
					$sc_r_only_start = '';  					 	//ไม่โชว์ปฏิทินให้เลือก
				}else{
					$r_only_start = '';								//กรอกได้
					$sc_r_only_start = ' jDate("#ssi_startDate");'; //เปิดปฏิทินให้เลือก
				}
				if($stop_Date != '00-00-0000'){
					$r_only_stop = 'readonly = "readonly"';
					$sc_r_only_stop = '';
				}else{
					$r_only_stop = '';
					$sc_r_only_stop = 'jDate("#ssi_endDate");  ';
				}
				*/
				
				$run = $fe['ssi_title_id'];
				$btOre = 'BTwhite';

				if($types=='A'){

					$wWidthName = '5%';
					$wWidthInput = '5%';
					$wName = 'น้ำหนัก';
					$wInput = '<input class="weight_title" type="text" name="weight[]" id="weight_$run" value="$fe[weight]" style="width: 50px" maxlength="10">';
				}else{
					$wWidthName = '1%';
					$wWidthInput = '1%';
				}

				//ดึงคำถามรอง
				$sub = "SELECT follow_ssi_question_weight.ssi_ques_id, follow_ssi_question_weight.question_list_id, ssi_question_code, follow_ssi_question_weight.weight, follow_ssi_question_weight.option_count, follow_question_list.list_title, follow_question_list.main_question_id, follow_question_list.list_title, REPLACE(follow_question_list.answer_option,\"'\",'') AS answer_option FROM $config[db_base_name].follow_ssi_question_weight,$config[db_base_name].follow_question_list WHERE follow_ssi_question_weight.question_list_id = follow_question_list.id AND follow_ssi_question_weight.ssi_id = '$id' AND follow_ssi_question_weight.ssi_title_id = '$run' ORDER BY follow_ssi_question_weight.ssi_ques_id ASC";
				$qsub = $logDb->queryAndLogSQL( $sub, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				$listSub = '';
				while($feSub = mysql_fetch_array($qsub)){

					//หาอีเวนต์ไอดี
					if($listSub == ''){
						$idEvent = "SELECT event_id_ref FROM $config[db_base_name].follow_main_question WHERE id = '".$feSub['main_question_id']."' LIMIT 1";
						$queidEvent = $logDb->queryAndLogSQL( $idEvent, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						$feidEvent = mysql_fetch_array($queidEvent);

						$event_id = $feidEvent['event_id_ref'];
					}

					$idSub = $feSub['ssi_ques_id']; //ไอดีคำถามหลัก ตาราง follow_ssi_title
					$listID = $feSub['question_list_id']; //ไอดีคำถามรอง ตาราง follow_question_list

					//ถ้าเป็นชนิด A ให้แสดงน้ำหนัก
					if($types=='A'){
						$chk_weight = trim(strip_tags($feSub['answer_option']));
						$subWeight = " <font color='red'>** $chk_weight</font>";
						$txtW = 'น้ำหนัก';
						$question_code ='รหัส: <input type="text" name="codeSub['.$fe[ssi_title_id].'][]" id="codeSub_'.$fe[ssi_title_id].'_'.$idSub.'"style="width: 65px" maxlength="20" value="'.$feSub[ssi_question_code].'">';
						$inputW = '<input type="text" class="weight_question" name="weightSub['.$fe['ssi_title_id'].'][]" id="weightSub_'.$fe[ssi_title_id].'_'.$idSub.'" value="'.$feSub[weight].'"  style="width: 40px" maxlength="10">';
					}

					//ถ้าเป็นแบบ B ให้แสดง select box
					if($types=='B'){

						$ops = strip_tags($feSub['answer_option']);
						$ops = explode(",",$ops);
						$countOps = count($ops);

							$question_code ='รหัส: <input type="text" name="codeSub['.$fe[ssi_title_id].'][]" id="codeSub_'.$fe[ssi_title_id].'_'.$idSub.'"style="width: 65px" maxlength="20" value="'.$feSub[ssi_question_code].'">';

						$option = "<select name='subOp[$run][]' id='subOp_$idSub' style='width: 65px; overflow: hidden;'>";
						for($x=0;$x<$countOps;$x++){
							if(trim($feSub['option_count'])==trim($ops[$x])){ $selected = "selected"; }else{ $selected = ""; }

							$option .= "<option value='$ops[$x]' $selected>$ops[$x]</option>";
						}
						$option .= "</select>";
					}

					$listSub .= $tpl->tbHtml( $thisPage.'.html', 'subQuestion' );
				}
				$ssi_id = $fe['ssi_id'];
				$ssi_title = $fe['ssi_form_name'];
				$list .= $tpl->tbHtml( $thisPage.'.html', 'Question' );
			}
		//เพิ่ม
		}else{
			//เปิด วันที่ให้เลือกวันเริ่มกับวันหยุดใช้ได้
			$sc_r_only_start = ' jDate("#ssi_startDate");';
			$sc_r_only_stop = 'jDate("#ssi_endDate");  ';

			$run = 1;
			$btOre = 'BTorange';

			if($types=='A'){

				$wWidthName = '5%';
				$wWidthInput = '5%';
				$wName = 'น้ำหนัก';
				$wInput = '<input class="weight_title" type="text" name="weight[]" id="weight_$run" value="$fe[weight]" style="width: 50px" maxlength="10">';

			}else{

				$wWidthName = '1%';
				$wWidthInput = '1%';
			}

			$begin = "&nbsp;&nbsp;<a href=\"javascript:void(0)\" onclick=\"insertQues('$thisPage.php','s_start');\" style=\"color: blue; line-height: 18px;\" class=\"nz_manage nz_button BTwhite\">เริ่มใช้งาน</a>";

			$list = $tpl->tbHtml( $thisPage.'.html', 'Question' );
		}

		$frmQuestion = $tpl->tbHtml( $thisPage.'.html', 'frmQuestion' );
		if($_GET['types']){ echo $frmQuestion; }else{ echo $tpl->tbHtml( $thisPage.'.html', 'mainQuestion' ); }
	break;
	case 'insert_question':
		/* Array
		(
			[ssi_title] => ทดสอบ
			[types] => A
			[ssi_id] => 
			[event_id] => 75
			[chk_weight] => 10
			[ssi_startDate] => 09-02-2013
			[ssi_endDate] => 28-02-2013
			[m_] => Array ( [0] => 1 )
			[title_id_] => Array ( [0] => 1 )
			[titleCode] => Array ( [0] => 001 )
			[ques] => Array ( [0] => ทดสอบ1 )
			[weight] => Array ( [0] => 50 )
			[subId] => Array ( [1] => Array ( [0] => 121 ) )
			[s_] => Array ( [1] => Array ( [0] => 1 ) )
			[codeSub] => Array ( [1] => Array ( [0] => 001/01 ) )
			[weightSub] => Array ( [1] => Array ( [0] => 20 ) )
			[func] => insert_question
			[date] => 09-02-2013
			[datestop] => 28-02-2013
		) */
		
		//รับตัวแปรที่ส่งมา
		$ssi_title = $_POST['ssi_title'];
		$m_ = $_POST['m_'];
		$s_ = $_POST['s_'];
		$ques = $_POST['ques'];
		$title_code = $_POST['titleCode'];
		$weight = $_POST['weight'];
		$subId = $_POST['subId'];
		$codeSub = $_POST['codeSub'];
		$weightSub = $_POST['weightSub'];
		$date = $_POST['date'];
		$datestop = $_POST['datestop'];
		$subOp = $_POST['subOp'];
		$types = $_POST['types'];

		if($_POST['ssi_id']){
			$ssi_id = $_POST['ssi_id'];
			$title_id = $_POST['title_id_'];
			$question_id = $_POST['question_id_'];
		}
		//รับตัวแปรที่ส่งมา
		
		//ตัดวันที่ให้ตรงตามฟอแมท
		$date = $_POST['date'];
		$datestop = $_POST['datestop'];
		$ss = explode("-",$date);
		$st = explode("-",$datestop);
		$date = $ss[2].'-'.$ss[1].'-'.$ss[0];
		$datestop = $st[2].'-'.$st[1].'-'.$st[0];
		
		//แก้ไขข้อมูล จากหน้าแก้ไข
		if($ssi_id){
			$form = "UPDATE $config[db_base_name].follow_ssi_form SET ssi_form_name = '$ssi_title', start_date = '$date', stop_date = '$datestop' WHERE ssi_id ='$ssi_id' LIMIT 1";
			$queForm = $logDb->queryAndLogSQL( $form, " FILE : ".__FILE__." LINE : ".__LINE__."" );

			$count = count($m_);

			$no = 0; //ตัวรันคำถามหลัก
			while($no<$count){
				//ถ้ามีคำถามหลักอยู่แล้ว
				if($title_id[$no]){

					$title = "UPDATE $config[db_base_name].follow_ssi_title SET ssi_id = '$ssi_id',ssi_title_name = '$ques[$no]',weight = '$weight[$no]',ssi_title_code = '$title_code[$no]' WHERE ssi_title_id ='$title_id[$no]' LIMIT 1";
					$queTitle = $logDb->queryAndLogSQL( $title, " FILE : ".__FILE__." LINE : ".__LINE__."" );

					$countSub = count($s_[$m_[$no]]); //จำนวนคำถามรองทั้งหมด ในคำถามหลักนี้
					$i = 0; //ตัวรันคำถามรอง
					while($i<$countSub){

						//ถ้ามีคำถามรองอยู่แล้วให้แก้ไข
						if($question_id[$m_[$no]][$i]){
							$quess = "UPDATE $config[db_base_name].follow_ssi_question_weight SET ssi_id = '$ssi_id',ssi_title_id = '$title_id[$no]',question_list_id = '".$subId[$m_[$no]][$i]."', ssi_question_code =  '".$codeSub[$m_[$no]][$i]."',weight = '".$weightSub[$m_[$no]][$i]."',option_count = '".$subOp[$m_[$no]][$i]."' WHERE ssi_ques_id ='".$question_id[$m_[$no]][$i]."' LIMIT 1";
							$queQues = $logDb->queryAndLogSQL( $quess, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						//ถ้ายังไม่มีคำถามรองให้เพิ่ม
						}else{
							$sub = "INSERT INTO $config[db_base_name].follow_ssi_question_weight (ssi_ques_id,ssi_id,ssi_title_id,question_list_id,ssi_question_code, weight,option_count) VALUES (NULL, '$ssi_id', '$title_id[$no]', '".$subId[$m_[$no]][$i]."', '".$codeSub[$m_[$no]][$i]."', '".$weightSub[$m_[$no]][$i]."', '".$subOp[$m_[$no]][$i]."')";
							$queSub = $logDb->queryAndLogSQL( $sub, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						}
					$i++;
					}
				//ถ้ายังไม่มึคำถามหลัก
				}else{

					//เพิ่มลงในฐานข้อมูล follow_ssi_title
					$main = "INSERT INTO $config[db_base_name].follow_ssi_title (ssi_title_id,ssi_id,ssi_title_name, ssi_title_code, weight) VALUES (NULL, '$ssi_id', '$ques[$no]', '$title_code[$no]', '$weight[$no]');"; 
					$queMain = $logDb->queryAndLogSQL( $main, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					//เพิ่มลงในฐานข้อมูล follow_ssi_title

					//ไอดีล่าสุดของ ฟิลด์ ssi_title_id จากตาราง follow_ssi_title
					$mainID = mysql_insert_id();
					//ไอดีล่าสุดของ ฟิลด์ ssi_title_id จากตาราง follow_ssi_title

					$countSub = count($s_[$m_[$no]]); //จำนวนคำถามรองทั้งหมด ในคำถามหลักนี้
					$i = 0; //ตัวรันคำถามรอง
					while($i<$countSub){
						if($s_[$m_[$no]][$i]){
						$sub = "INSERT INTO $config[db_base_name].follow_ssi_question_weight (ssi_ques_id,ssi_id,ssi_title_id,question_list_id, ssi_question_code, weight,option_count) VALUES (NULL, '$ssi_id', '$mainID', '".$subId[$m_[$no]][$i]."', '".$codeSub[$m_[$no]][$i]."', '".$weightSub[$m_[$no]][$i]."', '".$subOp[$m_[$no]][$i]."')";
						$queSub = $logDb->queryAndLogSQL( $sub, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						}
					$i++;
					}

				}
			$no++;
			}
			
			echo "<script type='text/javascript'>browseCheck('main','$thisPage.php?id=$ssi_id&type=$types&func=createFrom','');</script>";
		//เพิ่มข้อมูล จากหน้าเพิ่มข้อมูล
		}else{
			//if($date!='start'){$sDate = $date; $stDate = $datestop;}else{
			if($date=='--start'){ $sDate = date('Y-m-d'); } //ถ้าคลิกเริ่มใช้งาน จะเก็บวันที่ลง start_date
			//}
			//เพิ่มลงในฐานข้อมูล follow_ssi_form
			/*$form = "INSERT INTO $config[db_base_name].`follow_ssi_form` (`ssi_id`,`ssi_form_name`,`start_date`,`stop_date`) SELECT MAX(`ssi_id`)+1,'$_GET[ssi_title]','$sDate','$fDate' FROM $config[db_base_name].`follow_ssi_form`";*/
			$form = "INSERT INTO $config[db_base_name].follow_ssi_form (ssi_id,ssi_form_type,ssi_form_name,start_date,stop_date) VALUES (NULL,'$types','$ssi_title','$sDate','$datestop')";
			$queForm = $logDb->queryAndLogSQL( $form, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			//เพิ่มลงในฐานข้อมูล follow_ssi_form

			//ไอดีล่าสุดของ ฟิลด์ ssi_id จากตาราง follow_ssi_form
			$formID	= mysql_insert_id();
			//ไอดีล่าสุดของ ฟิลด์ ssi_id จากตาราง follow_ssi_form

			$count = count($m_); //จำนวนคำถามหลักทั้งหมด

			$no = 0; //ตัวรันคำถามหลัก
			while($no<$count){
				//เพิ่มลงในฐานข้อมูล follow_ssi_title
				$main = "INSERT INTO $config[db_base_name].follow_ssi_title (ssi_title_id,ssi_id,ssi_title_name, ssi_title_code, weight) VALUES (NULL, '$formID', '$ques[$no]',  '$title_code[$no]', '$weight[$no]');";
				$queMain = $logDb->queryAndLogSQL( $main, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				//เพิ่มลงในฐานข้อมูล follow_ssi_title

				//ไอดีล่าสุดของ ฟิลด์ ssi_title_id จากตาราง follow_ssi_title
				$mainID	= mysql_insert_id();
				//ไอดีล่าสุดของ ฟิลด์ ssi_title_id จากตาราง follow_ssi_title

				$countSub = count($s_[$m_[$no]]); //จำนวนคำถามรองทั้งหมด ในคำถามหลักนี้
				$i = 0; //ตัวรันคำถามรอง
				while($i<$countSub){
					if($s_[$m_[$no]][$i]){
					$sub = "INSERT INTO $config[db_base_name].follow_ssi_question_weight (ssi_ques_id,ssi_id,ssi_title_id,question_list_id, ssi_question_code, weight,option_count) VALUES (NULL, '$formID', '$mainID', '".$subId[$m_[$no]][$i]."', '".$codeSub[$m_[$no]][$i]."', '".$weightSub[$m_[$no]][$i]."', '".$subOp[$m_[$no]][$i]."')";
					$queSub = $logDb->queryAndLogSQL( $sub, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					}
				$i++;
				}
			$no++;
			}

			echo "<script type='text/javascript'>browseCheck('main','$thisPage.php?id=$formID&type=$types&func=createFrom','');</script>";
		}
	break;
	case 'delQues':
		$type	= $_POST['type'];
		$id		= $_POST['id'];
	
		if($type=='main'){ //ลบคำถามหลัก
			$sql = "DELETE FROM $config[db_base_name].`follow_ssi_title` WHERE ssi_title_id = '$id' LIMIT 1";
			$que = $queSub = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$sql = "DELETE FROM $config[db_base_name].follow_ssi_question_weight WHERE ssi_title_id = '$id'";
			$que = $queSub = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		}else{ //ลบคำถามรอง
			$sql = "DELETE FROM $config[db_base_name].follow_ssi_question_weight WHERE ssi_ques_id = '$id' LIMIT 1";
			$que = $queSub = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		}

		if($que){ echo "Y"; }
	break;
	case 'activeFrom':
		$id = $_POST['id'];
		
		$sql = "UPDATE $config[db_base_name].follow_ssi_form SET active_maincus = '' WHERE active_maincus = 'active' LIMIT 1";
		$que = $queSub = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$sql = "UPDATE $config[db_base_name].follow_ssi_form SET active_maincus = 'active' WHERE ssi_id = '$id' LIMIT 1";
		$que2 = $queSub = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	break;
	case 'cancelFrom':
		$id = $_POST['id'];
		$fDate = date('Y-m-d');

		$sql = "UPDATE $config[db_base_name].follow_ssi_form SET stop_date = '$fDate' WHERE ssi_id = '$id' LIMIT 1";
		$que = $queSub = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );

		if($que){
			echo "Y";
		}
	break;
	case 'chkName':
		$name	= $_POST['name'];
		$edit	= $_POST['edit'];
	
		if($name != $edit){
			$sql = "SELECT ssi_id FROM $config[db_base_name].follow_ssi_form WHERE ssi_form_name = '$name' LIMIT 1";
			$que = $queSub = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$row = mysql_num_rows($que);

			if($row>0){
				echo "ชื่อซ้ำ";
			}
		}
	break;
}

CloseDB();
?>