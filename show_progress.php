<?php
/*
| create by : naizan
| create date : 2011-06-14
| ใช้สร้างสถานะการเปอร์เซ็นต์ดาวน์โหลด
| จะทำงานร่วมกับ ajax โดยเรียกจากฟังก์ชั่นจาวาสคริปต์ show_progress(html_obj,session_name);
|
*/
@header('Content-type: text/html; charset=utf-8');
if($_GET['process']=='show'){//แสดงเซสชั่นที่ต้องการ
	@session_start();
	if($_GET['session_name']==''){//ใช้ชื่อเริ่มต้น
		$session_progress = $_SESSION['progress'.session_id()];
	}else{//ใช้ชื่อเซสชั่น ตามที่ส่งมา
		$session_progress = $_SESSION[$_GET['session_name'].session_id()];
	}
	echo $session_progress;
	if($_SESSION[$session_progress]>=100 || $_SESSION[$session_progress]>='100'){
		$_SESSION[$session_progress] = '';
	}
	@session_write_close();
}else{
	/**
	 *
	 * สร้างเซสชั่นคำนวณสถานะการแสดงผล จะเอาจำนวนเรคอร์ดปัจจุบันไปคิดเป็นเปอร์เซ็นต์ของเรคอร์ดทั้งหมด
	 * @param Integer $now เรคอร์ดปัจจุบัน
	 * @param Integer $total เรคอร์ดทั้งหมด
	 * @param Integer $session_name ชื่อเซสชั่นของแต่ละหน้าเพื่อป้องกันปัญหาการโหลดหลายหน้า
	 */
	function create_progress($now,$total,$session_name='progress'){
		$_SESSION[$session_name.session_id()] = 0;
		if($now > 0 && $total>0){
			@session_start();
			$_SESSION[$session_name.session_id()] = round(($now*100)/$total);
			@session_write_close();
		}
	}
}
@session_write_close();
?>