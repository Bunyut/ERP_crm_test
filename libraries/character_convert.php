<?php
function tis2utf8($tis) {
   for( $i=0 ; $i< strlen($tis) ; $i++ ){
      $s = substr($tis, $i, 1);
      $val = ord($s);
      if( $val < 0x80 ){
         $utf8 .= $s;
      } elseif ( ( 0xA1 <= $val and $val <= 0xDA ) or ( 0xDF <= $val and $val <= 0xFB ) ){
         $unicode = 0x0E00 + $val - 0xA0;
         $utf8 .= chr( 0xE0 | ($unicode >> 12) );
         $utf8 .= chr( 0x80 | (($unicode >> 6) & 0x3F) );
         $utf8 .= chr( 0x80 | ($unicode & 0x3F) );
      }
   }
   return $utf8;
}

function utf8_to_tis620($string) {
  $str = $string;
  $res = "";
  for ($i = 0; $i < strlen($str); $i++) {
	if (ord($str[$i]) == 224) {
	  $unicode = ord($str[$i+2]) & 0x3F;
	  $unicode |= (ord($str[$i+1]) & 0x3F) << 6;
	  $unicode |= (ord($str[$i]) & 0x0F) << 12;
	  $res .= chr($unicode-0x0E00+0xA0);
	  $i += 2;
	} else {
	  $res .= $str[$i];
	}
  }
  return $res;
}

if( !function_exists( 'UniThai' ) ){
function UniThai($s) {
  $x = "";
  $len = strlen($s);
  for ( $i = 0; $i < $len; $i++)
  {
   if ( ord($s[$i]) > 128 )
    $x .= "&#".(ord($s[$i]) - 160 + 3584).";";
   else
    $x .= $s[$i];
  }
  return $x;
 }
}


function changeUTF8( $txt ){
	$txt=str_replace("&nbsp;"," ",$txt);
	$out = ""; 

	if(strpos($txt,';&#')){
		$txt=str_replace('ผ', "&#3612;",$txt);

	$out=$txt;
	}else{

	for ($i = 0; $i < strlen($txt); $i++) 
	{
	if (ord($txt[$i]) <= 126) 
	$out .= $txt[$i];
	else 
	$out .= "&#" . (ord($txt[$i]) - 161 + 3585) . ";"; 
	}
	}
	return $out; 
} 
?>