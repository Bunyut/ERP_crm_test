<?php
/**
 * Create by: Nuy
 * Create date: 27.07.2011
 * Last Modify: 30.07.2011@Nuy
 * Last Modify: pt@2011-11
 * Description: หน้าการจัดการแบบฟอร์ม CR สำหรับคำนวณคะแนนควาพึงพอใจ SSI
 */
 
require_once("config/general.php");
require_once("function/general.php");

$thisPage 	= "ssi_report";

## สั่งทำงานโค้ด
$doCode = ""; 		## ทำงาน
// $doCode = "No";  ## ไม่ทำงาน แสดงโค้ด Insert, Update, Delete
// $doCode = "All"; ## ทำงาน แสดงโค้ด Select, Insert, Update, Delete


Conn2DB();
mysql_query( "SET NAMES UTF8" );
session_write_close();

$func = $_REQUEST['func'];

// $config['db_base_name'] = 'ERP_newCRM';
switch($func){
	default:
		$styleT1 = 'BTwhite boldGreen';
		$styleT2 = 'BTblack boldGreen';
		$ssi_mode = $tpl->tbHtml( $thisPage.'.html', 'SSI_REPORT_TAB1' );
		echo $tpl->tbHtml( $thisPage.'.html', 'SSI_REPORT_MAIN' );
	break;
	case 'typeReport':
		$reportType = $_GET['type'];//ใช้เป็นparamในtemplates
		$year_exist = '';
		
		$sql_yearSell = "SELECT DISTINCT(SUBSTRING(Date_Deliver,7,4)) AS Date_Deliver FROM $config[db_easysale].Sell ORDER BY Date_Deliver ";
		$result_yearSell = $logDb->queryAndLogSQL( $sql_yearSell, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($yearSell = mysql_fetch_assoc($result_yearSell)){
			$expYYYY = explode('-',$yearSell['Date_Deliver']);
			
			if(($yearSell['Date_Deliver'] != '') && (count($expYYYY)!=2)){
				$year_exist .= '<option value="'.$yearSell['Date_Deliver'].'">'.$yearSell['Date_Deliver'].'</option>';
			}
		}

		echo $tpl->tbHtml( $thisPage.'.html', 'SELECT_YEAR' );
	break;
	case 'specific':
		$SPEC = '';
		$reportType = $_GET['specific'];//ที่ส่งมาจะเป็นชนิดรายงาน
		$YEAR = $_GET['year'];
		$MONTH = $_GET['month']; //pt@2012-01-31
		
		switch($reportType){
			case 'dealer':
				$sql = "SELECT DISTINCT(dealercode) FROM $config[db_easysale].Sell JOIN $table_config[working_company] ON Sell.sell_branch = working_company.id ";
				$sql .= "WHERE Date_Deliver LIKE '%-$YEAR' AND dealercode != '' ORDER BY working_company.dealercode ";
				$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				
				while($sell_dealer = mysql_fetch_assoc($result)){
					$dealer_name .= "<option value=\"$sell_dealer[dealercode]\">$sell_dealer[dealercode]</option>";
				}
				
				$SPEC = $tpl->tbHtml( $thisPage.'.html', 'SELECT_DEALER' );
			break;
			case 'branch':
				$sql = "SELECT DISTINCT(sell_branch), working_company.name FROM $config[db_easysale].Sell JOIN $table_config[working_company] ON ";
				$sql .= "Sell.sell_branch = working_company.id WHERE Date_Deliver LIKE '%-$YEAR' AND sell_branch != '' ORDER BY working_company.id  ";
				$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				
				while($sell_branch = mysql_fetch_assoc($result)){
					$branch_name .= "<option value=\"$sell_branch[sell_branch]\">$sell_branch[name]</option>";
				}
				
				$SPEC = $tpl->tbHtml( $thisPage.'.html', 'SELECT_BRANCH' );
			break;
			case 'emp':
				$sql = "SELECT DISTINCT(Sell.Sale_Sell), emp_data.Name, emp_data.Surname FROM $config[db_easysale].Sell INNER JOIN $config[db_emp].emp_data ON ";
				$sql .= "Sell.Sale_Sell = emp_data.ID_card WHERE Sell.Date_Deliver LIKE '%-$YEAR' ORDER BY CONCAT(emp_data.Name,' ',emp_data.Surname) ASC";
				// $sql .= "Sell.Sale_Sell = emp_data.ID_card WHERE Sell.Date_Deliver LIKE '%-$YEAR' AND emp_data.Prosonnal_Being!=3 ORDER BY CONCAT(emp_data.Name,' ',emp_data.Surname) ASC";
				$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				
				while($sell_emp = mysql_fetch_assoc($result)){
					if($sell_emp['Name'] != ''){
						$emp_name .= "<option value=\"$sell_emp[Sale_Sell]\">$sell_emp[Name] $sell_emp[Surname]</option>";
					}
				}
				
				$SPEC = $tpl->tbHtml( $thisPage.'.html', 'SELECT_EMP' );
			break;
			case 'cus':  //pt@2012-01-31
				if(date('Y') == $YEAR){
					$lastMonth = date('m');
				}else{
					$lastMonth = 12;
				}
				
				for($m=1;$m<=$lastMonth;$m++){
					$mon = sprintf('%02d',$m);
					$month_name = get_month_th($m);
					$month_exist .= "<option id=\"$m\" value=\"$mon\">$month_name</option>";
				}
				
				$SPEC = $tpl->tbHtml( $thisPage.'.html', 'SELECT_MONTH' );
			break;
			case 'emp4cus':  //pt@2012-01-31
				$sql = "SELECT DISTINCT(Sell.Sale_Sell), emp_data.Name, emp_data.Surname FROM $config[db_easysale].Sell INNER JOIN $config[db_emp].emp_data ON ";
				$sql .= "Sell.Sale_Sell = emp_data.ID_card WHERE Sell.Date_Deliver LIKE '%-$MONTH-$YEAR' ORDER BY CONCAT(emp_data.Name,' ',emp_data.Surname) ASC";
				$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				
				while($sell_emp = mysql_fetch_assoc($result)){
					if($sell_emp['Name'] != ''){
						$emp_name .= "<option value=\"$sell_emp[Sale_Sell]\">$sell_emp[Name] $sell_emp[Surname]</option>";
					}
				}
				
				$SPEC = $tpl->tbHtml( $thisPage.'.html', 'SELECT_EMP' );
			break;
		}

		echo $SPEC;
	break;
	case 'viewDealer':
		$dealer_code = $_GET['view_dealer'];
		$year = $_GET['year'];
		if($_GET['form_id']){
			$form_id = $_GET['form_id'];
			$where = " AND SSI_Report.form_id = '$form_id' ";
		}else{
			$where = '';
		}

		$topic = 'DEALER CODE : '.$dealer_code; // ชื่อหัวตาราง
		#-FORM
		$sql = "SELECT DISTINCT(SSI_Report.form_id) FROM $config[db_base_name].SSI_Report WHERE SSI_Report.report_type = '1' AND SSI_Report.names_code = '$dealer_code' AND SSI_Report.year = '$year' AND SSI_Report.status != '99' AND type_of_list = '1' $where ";
		$res = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while($fet = mysql_fetch_assoc($res)){
			$sql_formName = "SELECT ssi_form_name, ssi_form_type FROM $config[db_base_name].follow_ssi_form WHERE ssi_id = '$fet[form_id]' ";
			$res_formName = $logDb->queryAndLogSQL( $sql_formName, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$fet_formName = mysql_fetch_assoc($res_formName);
			$form_name = $fet_formName['ssi_form_name']; // ชื่อแบบฟอร์ม

			$ps = $it = $pf = $pp = array();
			$ps_average = $pleasureName = $pp_average = $have_datails/*เช็คว่าฟอร์มนี้มีรายการให้echoตาราง*/ = '';
			//หา รายการสรุป ความพึงพอใจ,จำนวนสัมพาษ,perfect,เปอร์เซ็นperfect
			$sql_summary = "SELECT head_sum, list_score , average FROM $config[db_base_name].SSI_Report WHERE head_sum IN ('1','2','3','4') AND SSI_Report.form_id = '$fet[form_id]' AND SSI_Report.report_type = '1' AND SSI_Report.names_code = '$dealer_code' AND SSI_Report.year = '$year' AND SSI_Report.status != '99'";
			$res_summary = $logDb->queryAndLogSQL( $sql_summary, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			while($fet_summary = mysql_fetch_assoc($res_summary)){
				if($fet_summary['head_sum'] == '1'){//pleasure
					$pleasureName = 'ความพึงพอใจ';
					$ps = explode(",",$fet_summary['list_score']);
					$ps_average = $fet_summary['average'];
				}else if($fet_summary['head_sum'] == '2'){//interview
					$it = explode(",",$fet_summary['list_score']);
				}else if($fet_summary['head_sum'] == '3'){//perfect
					$pf = explode(",",$fet_summary['list_score']);
				}else if($fet_summary['head_sum'] == '4'){//percent_perfect
					$pp = explode(",",$fet_summary['list_score']);
					$pp_average = $fet_summary['average'];
				}
			}

			$realMain = '';
			#-MAIN QUESTION
			if($fet_formName['ssi_form_type'] == 'A'){
				$sql_2 = "SELECT DISTINCT(SSI_Report.main_question_id), SSI_Report.weight, SSI_Report.list_score ,  SSI_Report.average FROM $config[db_base_name].SSI_Report WHERE SSI_Report.form_id = '$fet[form_id]' AND SSI_Report.report_type = '1' AND SSI_Report.names_code = '$dealer_code' AND SSI_Report.year = '$year' AND SSI_Report.status != '99'  AND type_of_list = '2' ";
			}else if($fet_formName['ssi_form_type'] == 'B'){
				$sql_2 = "SELECT DISTINCT(SSI_Report.main_question_id) FROM $config[db_base_name].SSI_Report WHERE SSI_Report.form_id = '$fet[form_id]' AND SSI_Report.report_type = '1' AND SSI_Report.names_code = '$dealer_code' AND SSI_Report.year = '$year' AND SSI_Report.status != '99'  AND type_of_list = '3' ";

			}

			$res_2 = $logDb->queryAndLogSQL( $sql_2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			while($fet_2 = mysql_fetch_assoc($res_2)){
				$mainquestion = '';
				if($fet_formName['ssi_form_type'] == 'A'){
					$sql_mainQ = "SELECT ssi_title_name FROM $config[db_base_name].follow_ssi_title WHERE ssi_title_id = '$fet_2[main_question_id]'";
					$res_mainQ = $logDb->queryAndLogSQL( $sql_mainQ, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$fet_mainQ = mysql_fetch_assoc($res_mainQ);
					$mainquestion = $fet_mainQ['ssi_title_name'];
				}
				$m_weight = $fet_2['weight'];

				$m = explode(",",$fet_2['list_score']);

				$m_average = $fet_2['average'];

				$realList = '';
				#-LIST QUESTION
				$sql_3 = "SELECT SSI_Report.list_question_id, SSI_Report.list_score, SSI_Report.weight , SSI_Report.average FROM $config[db_base_name].SSI_Report WHERE SSI_Report.form_id = '$fet[form_id]' AND SSI_Report.main_question_id = '$fet_2[main_question_id]' AND SSI_Report.list_question_id != '0' AND SSI_Report.report_type = '1' AND SSI_Report.names_code = '$dealer_code' AND SSI_Report.year = '$year' AND SSI_Report.status != '99' AND type_of_list = '3' ORDER BY id";
				$res_3 = $logDb->queryAndLogSQL( $sql_3, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				while($fet_3 = mysql_fetch_assoc($res_3)){
					$listquestion = '';
					$sql_listQ = "SELECT list_title FROM $config[db_base_name].follow_question_list WHERE id = '$fet_3[list_question_id]'";
					$res_listQ = $logDb->queryAndLogSQL( $sql_listQ, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$fet_listQ = mysql_fetch_assoc($res_listQ);
					$listquestion = $fet_listQ['list_title'];

					$l_weight = $fet_3['weight'];

					$l = explode(",",$fet_3['list_score']);

					$l_average = $fet_3['average'];

					$realList .= $tpl->tbHtml( $thisPage.'.html', 'LIST_DATA_1' );
					$have_datails++;
				}//list
				$realMain .= $tpl->tbHtml( $thisPage.'.html', 'LIST_MAIN_1' );
			}//main

			if($_GET['excel']){
				header("Content-Type: application/vnd.ms-excel");
				header('Content-Disposition: attachment; filename="'.$form_name.'_SSI_dealer_'.date('j_F_Y').'.xls"');#ชื่อไฟล
				$html = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"
				xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv="Content-type" content="text/html;charset=UTF-8"></head><body>';
				$html .= $tpl->tbHtml( $thisPage.'.html', 'TABLE_VIEW_1' );
				$html .= '</body></html>';
				echo $html;
				exit();
			}else{
				$btn_excel = "
					<a href=\"javascript:void(0);\" onclick= \"javascript:window.open('".$thisPage.".php?func=viewDealer&view_dealer=$dealer_code&year=$year&form_id=$fet[form_id]&excel=1');\" >
					<img height='30' src=\"images/excel-2003.gif\" title='โหลด excel File'/>
					</a>
					";
				$sql_processDate = "SELECT SSI_Report.process_date FROM $config[db_base_name].SSI_Report WHERE SSI_Report.report_type = '1' AND SSI_Report.names_code = '$dealer_code' AND SSI_Report.year = '$year' AND SSI_Report.status != '99' AND type_of_list = '1' $where ORDER BY SSI_Report.process_date DESC LIMIT 1";
				$res_processDate = $logDb->queryAndLogSQL( $sql_processDate, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$fet_processDate = mysql_fetch_assoc($res_processDate);
				$last_update_date = 'ข้อมูลล่าสุดเมื่อวันที่ : '.explode_date($fet_processDate['process_date']);
				if($have_datails) echo $tpl->tbHtml( $thisPage.'.html', 'TABLE_VIEW_1' );
			}
		}//form
	break;
	case 'viewBranch':
		$branch = $_GET['view_branch'];
		$year = $_GET['year'];
		if($_GET['form_id']){
			$form_id = $_GET['form_id'];
			$where = " AND SSI_Report.form_id = '$form_id' ";
		}else{
			$where = '';
		}

		// ชื่อหัวตาราง
		$sql_branchName = "SELECT name FROM $table_config[working_company] WHERE id='$branch'";
		$res_branchName = $logDb->queryAndLogSQL( $sql_branchName, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$fet_branchName = mysql_fetch_assoc($res_branchName);
		$topic = $fet_branchName['name'];

		#-FORM
		$sql = "SELECT DISTINCT(SSI_Report.form_id) FROM $config[db_base_name].SSI_Report WHERE SSI_Report.report_type = '2' AND SSI_Report.names_code = '$branch' AND SSI_Report.year = '$year' AND SSI_Report.status != '99' AND type_of_list = '1' $where ";
		$res = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while($fet = mysql_fetch_assoc($res)){

			$sql_formName = "SELECT ssi_form_name, ssi_form_type FROM $config[db_base_name].follow_ssi_form WHERE ssi_id = '$fet[form_id]' ";
			$res_formName = $logDb->queryAndLogSQL( $sql_formName, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$fet_formName = mysql_fetch_assoc($res_formName);
			$form_name = $fet_formName['ssi_form_name']; // ชื่อแบบฟอร์ม

			$ps = $it = $pf = $pp = array();
			$ps_average = $pleasureName = $pp_average = $have_datails/*เช็คว่าฟอร์มนี้มีรายการให้echoตาราง*/ = '';
			//หา รายการสรุป ความพึงพอใจ,จำนวนสัมพาษ,perfect,เปอร์เซ็นperfect
			$sql_summary = "SELECT head_sum, list_score , average FROM $config[db_base_name].SSI_Report WHERE head_sum IN ('1','2','3','4') AND SSI_Report.form_id = '$fet[form_id]' AND SSI_Report.report_type = '2' AND SSI_Report.names_code = '$branch' AND SSI_Report.year = '$year' AND SSI_Report.status != '99'";
			$res_summary = $logDb->queryAndLogSQL( $sql_summary, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			while($fet_summary = mysql_fetch_assoc($res_summary)){
				if($fet_summary['head_sum'] == '1'){//pleasure
					$pleasureName = 'ความพึงพอใจ';
					$ps = explode(",",$fet_summary['list_score']);
					$ps_average = $fet_summary['average'];
				}else if($fet_summary['head_sum'] == '2'){//interview
					$it = explode(",",$fet_summary['list_score']);
				}else if($fet_summary['head_sum'] == '3'){//perfect
					$pf = explode(",",$fet_summary['list_score']);
				}else if($fet_summary['head_sum'] == '4'){//percent_perfect
					$pp = explode(",",$fet_summary['list_score']);
					$pp_average = $fet_summary['average'];
				}
			}
			$realMain = '';
			#-MAIN QUESTION
			if($fet_formName['ssi_form_type'] == 'A'){
				$sql_2 = "SELECT DISTINCT(SSI_Report.main_question_id), SSI_Report.weight, SSI_Report.list_score ,  SSI_Report.average FROM $config[db_base_name].SSI_Report WHERE SSI_Report.form_id = '$fet[form_id]' AND SSI_Report.report_type = '2' AND SSI_Report.names_code = '$branch' AND SSI_Report.year = '$year' AND SSI_Report.status != '99'  AND type_of_list = '2' ";
			}else if($fet_formName['ssi_form_type'] == 'B'){
				$sql_2 = "SELECT DISTINCT(SSI_Report.main_question_id) FROM $config[db_base_name].SSI_Report WHERE SSI_Report.form_id = '$fet[form_id]' AND SSI_Report.report_type = '2' AND SSI_Report.names_code = '$branch' AND SSI_Report.year = '$year' AND SSI_Report.status != '99'  AND type_of_list = '3' ";
			}
			$res_2 = $logDb->queryAndLogSQL( $sql_2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			while($fet_2 = mysql_fetch_assoc($res_2)){
				$mainquestion = '';
				if($fet_formName['ssi_form_type'] == 'A'){
					$sql_mainQ = "SELECT ssi_title_name FROM $config[db_base_name].follow_ssi_title WHERE ssi_title_id = '$fet_2[main_question_id]'";
					$res_mainQ = $logDb->queryAndLogSQL( $sql_mainQ, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$fet_mainQ = mysql_fetch_assoc($res_mainQ);
					$mainquestion = $fet_mainQ['ssi_title_name'];
				}
				$m_weight = $fet_2['weight'];

				$m = explode(",",$fet_2['list_score']);

				$m_average = $fet_2['average'];

				$realList = '';
				#-LIST QUESTION
				$sql_3 = "SELECT SSI_Report.list_question_id, SSI_Report.list_score, SSI_Report.weight , SSI_Report.average FROM $config[db_base_name].SSI_Report WHERE SSI_Report.form_id = '$fet[form_id]' AND SSI_Report.main_question_id = '$fet_2[main_question_id]' AND SSI_Report.list_question_id != '0' AND SSI_Report.report_type = '2' AND SSI_Report.names_code = '$branch' AND SSI_Report.year = '$year' AND SSI_Report.status != '99' AND type_of_list = '3' ORDER BY id";
				$res_3 = $logDb->queryAndLogSQL( $sql_3, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				while($fet_3 = mysql_fetch_assoc($res_3)){
					$listquestion = '';
					$sql_listQ = "SELECT list_title FROM $config[db_base_name].follow_question_list WHERE id = '$fet_3[list_question_id]'";
					$res_listQ = $logDb->queryAndLogSQL( $sql_listQ, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$fet_listQ = mysql_fetch_assoc($res_listQ);
					$listquestion = $fet_listQ['list_title'];

					$l_weight = $fet_3['weight'];

					$l = explode(",",$fet_3['list_score']);

					$l_average = $fet_3['average'];

					$realList .= $tpl->tbHtml( $thisPage.'.html', 'LIST_DATA_1' );
					$have_datails++;
				}//list
				$realMain .= $tpl->tbHtml( $thisPage.'.html', 'LIST_MAIN_1' );
			}//main
			if($_GET['excel']){
				header("Content-Type: application/vnd.ms-excel");
				header('Content-Disposition: attachment; filename="'.$form_name.'_SSI_branch_'.date('j_F_Y').'.xls"');#ชื่อไฟล
				$html = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"
				xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv="Content-type" content="text/html;charset=UTF-8"></head><body>';
				$html .= $tpl->tbHtml( $thisPage.'.html', 'TABLE_VIEW_1' );
				$html .= '</body></html>';
				echo $html;
				exit();
			}else{
				$btn_excel = "
					<a href=\"javascript:void(0);\" onclick= \"javascript:window.open('".$thisPage.".php?func=viewBranch&view_branch=$branch&year=$year&form_id=$fet[form_id]&excel=1');\" >
					<img height='30' src=\"images/excel-2003.gif\" title='โหลด excel File'/>
					</a>
					";
				$sql_processDate = "SELECT SSI_Report.process_date FROM $config[db_base_name].SSI_Report WHERE SSI_Report.report_type = '2' AND SSI_Report.names_code = '$branch' AND SSI_Report.year = '$year' AND SSI_Report.status != '99' AND type_of_list = '1' $where ORDER BY SSI_Report.process_date DESC LIMIT 1";
				$res_processDate = $logDb->queryAndLogSQL( $sql_processDate, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$fet_processDate = mysql_fetch_assoc($res_processDate);
				$last_update_date = 'ข้อมูลล่าสุดเมื่อวันที่ : '.explode_date($fet_processDate['process_date']);
				if($have_datails) echo $tpl->tbHtml( $thisPage.'.html', 'TABLE_VIEW_1' );
			}
		}//form
	break;
	case 'viewEmp':
		$emp = $_GET['view_emp'];
		$year = $_GET['year'];
		if($_GET['form_id']){
			$form_id = $_GET['form_id'];
			$where = " AND SSI_Report.form_id = '$form_id' ";
		}else{
			$where = '';
		}

		// ชื่อหัวตาราง = ชื่อพนักงาน
		$sql_saleName = "SELECT Name, Surname, Nickname FROM $config[db_emp].emp_data WHERE ID_card='$emp'";
		$res_saleName = $logDb->queryAndLogSQL( $sql_saleName, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$fet_salehName = mysql_fetch_assoc($res_saleName);
		$topic = $fet_salehName['Name'].' '.$fet_salehName['Surname'].' ('.$fet_salehName['Nickname'].')';

		#-FORM
		$sql = "SELECT DISTINCT(SSI_Report.form_id) FROM $config[db_base_name].SSI_Report WHERE SSI_Report.report_type = '3' AND SSI_Report.names_code = '$emp' AND SSI_Report.year = '$year' AND SSI_Report.status != '99' AND type_of_list = '1' $where ";
		$res = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while($fet = mysql_fetch_assoc($res)){
			$sql_formName = "SELECT ssi_form_name, ssi_form_type FROM $config[db_base_name].follow_ssi_form WHERE ssi_id = '$fet[form_id]' ";
			$res_formName = $logDb->queryAndLogSQL( $sql_formName, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$fet_formName = mysql_fetch_assoc($res_formName);
			$form_name = $fet_formName['ssi_form_name']; // ชื่อแบบฟอร์ม

			$ps = $it = $pf = $pp = array();
			$ps_average = $pleasureName = $pp_average = $have_datails/*เช็คว่าฟอร์มนี้มีรายการให้echoตาราง*/ = '';
			//หา รายการสรุป ความพึงพอใจ,จำนวนสัมพาษ,perfect,เปอร์เซ็นperfect
			$sql_summary = "SELECT head_sum, list_score , average FROM $config[db_base_name].SSI_Report WHERE head_sum IN ('1','2','3','4') AND SSI_Report.form_id = '$fet[form_id]' AND SSI_Report.report_type = '3' AND SSI_Report.names_code = '$emp' AND SSI_Report.year = '$year' AND SSI_Report.status != '99'";
			$res_summary = $logDb->queryAndLogSQL( $sql_summary, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			while($fet_summary = mysql_fetch_assoc($res_summary)){
				if($fet_summary['head_sum'] == '1'){//pleasure
					$pleasureName = 'ความพึงพอใจ';
					$ps = explode(",",$fet_summary['list_score']);
					$ps_average = $fet_summary['average'];
				}else if($fet_summary['head_sum'] == '2'){//interview
					$it = explode(",",$fet_summary['list_score']);
				}else if($fet_summary['head_sum'] == '3'){//perfect
					$pf = explode(",",$fet_summary['list_score']);
				}else if($fet_summary['head_sum'] == '4'){//percent_perfect
					$pp = explode(",",$fet_summary['list_score']);
					$pp_average = $fet_summary['average'];
				}
			}
			$realMain = '';
			#-MAIN QUESTION
			if($fet_formName['ssi_form_type'] == 'A'){
				$sql_2 = "SELECT DISTINCT(SSI_Report.main_question_id), SSI_Report.weight, SSI_Report.list_score ,  SSI_Report.average FROM $config[db_base_name].SSI_Report WHERE SSI_Report.form_id = '$fet[form_id]' AND SSI_Report.report_type = '3' AND SSI_Report.names_code = '$emp' AND SSI_Report.year = '$year' AND SSI_Report.status != '99'  AND type_of_list = '2' ";
			}else if($fet_formName['ssi_form_type'] == 'B'){
				$sql_2 = "SELECT DISTINCT(SSI_Report.main_question_id) FROM $config[db_base_name].SSI_Report WHERE SSI_Report.form_id = '$fet[form_id]' AND SSI_Report.report_type = '3' AND SSI_Report.names_code = '$emp' AND SSI_Report.year = '$year' AND SSI_Report.status != '99'  AND type_of_list = '3' ";
			}
			$res_2 = $logDb->queryAndLogSQL( $sql_2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			while($fet_2 = mysql_fetch_assoc($res_2)){
				$mainquestion = '';
				if($fet_formName['ssi_form_type'] == 'A'){
					$sql_mainQ = "SELECT ssi_title_name FROM $config[db_base_name].follow_ssi_title WHERE ssi_title_id = '$fet_2[main_question_id]'";
					$res_mainQ = $logDb->queryAndLogSQL( $sql_mainQ, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$fet_mainQ = mysql_fetch_assoc($res_mainQ);
					$mainquestion = $fet_mainQ['ssi_title_name'];
				}
				$m_weight = $fet_2['weight'];

				$m = explode(",",$fet_2['list_score']);

				$m_average = $fet_2['average'];

				$realList = '';
				#-LIST QUESTION
				$sql_3 = "SELECT SSI_Report.list_question_id, SSI_Report.list_score, SSI_Report.weight , SSI_Report.average FROM $config[db_base_name].SSI_Report WHERE SSI_Report.form_id = '$fet[form_id]' AND SSI_Report.main_question_id = '$fet_2[main_question_id]' AND SSI_Report.list_question_id != '0' AND SSI_Report.report_type = '3' AND SSI_Report.names_code = '$emp' AND SSI_Report.year = '$year' AND SSI_Report.status != '99' AND type_of_list = '3' ORDER BY id";
				$res_3 = $logDb->queryAndLogSQL( $sql_3, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				while($fet_3 = mysql_fetch_assoc($res_3)){
					$listquestion = '';
					$sql_listQ = "SELECT list_title FROM $config[db_base_name].follow_question_list WHERE id = '$fet_3[list_question_id]'";
					$res_listQ = $logDb->queryAndLogSQL( $sql_listQ, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$fet_listQ = mysql_fetch_assoc($res_listQ);
					$listquestion = $fet_listQ['list_title'];

					$l_weight = $fet_3['weight'];

					$l = explode(",",$fet_3['list_score']);

					$l_average = $fet_3['average'];

					$realList .= $tpl->tbHtml( $thisPage.'.html', 'LIST_DATA_1' );
					$have_datails++;
				}//list
				$realMain .= $tpl->tbHtml( $thisPage.'.html', 'LIST_MAIN_1' );
			}//main
			if($_GET['excel']){
				header("Content-Type: application/vnd.ms-excel");
				header('Content-Disposition: attachment; filename="'.$form_name.'_SSI_emp_'.date('j_F_Y').'.xls"');#ชื่อไฟล
				$html = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"
				xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv="Content-type" content="text/html;charset=UTF-8"></head><body>';
				$html .= $tpl->tbHtml( $thisPage.'.html', 'TABLE_VIEW_1' );
				$html .= '</body></html>';
				echo $html;
				exit();
			}else{
				$btn_excel = "
					<a href=\"javascript:void(0);\" onclick= \"javascript:window.open('".$thisPage.".php?func=viewEmp&view_emp=$emp&year=$year&form_id=$fet[form_id]&excel=1');\" >
					<img height='30' src=\"images/excel-2003.gif\" title='โหลด excel File'/>
					</a>
					";
				$sql_processDate = "SELECT SSI_Report.process_date FROM $config[db_base_name].SSI_Report WHERE SSI_Report.report_type = '3' AND SSI_Report.names_code = '$emp' AND SSI_Report.year = '$year' AND SSI_Report.status != '99' AND type_of_list = '1' $where ORDER BY SSI_Report.process_date DESC LIMIT 1";
				$res_processDate = $logDb->queryAndLogSQL( $sql_processDate, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$fet_processDate = mysql_fetch_assoc($res_processDate);
				$last_update_date = 'ข้อมูลล่าสุดเมื่อวันที่ : '.explode_date($fet_processDate['process_date']);
				if($have_datails) echo $tpl->tbHtml( $thisPage.'.html', 'TABLE_VIEW_1' );
			}
		}//form
	break;
	case 'viewCus':
		$emp = $_GET['view_cus'];
		$year = $_GET['year'];
		$month = $_GET['month'];

		if($_GET['form_id']){
			$form_id = $_GET['form_id'];
			$where = " AND SSI_Report.form_id = '$form_id' ";
		}else{
			$where = '';
		}

		#-FORM
		$sql = "SELECT DISTINCT(SSI_Report.form_id) FROM $config[db_base_name].SSI_Report WHERE SSI_Report.report_type = '4'  AND SSI_Report.year LIKE '$year-$month%' AND SSI_Report.status != '99' AND type_of_list = '3' $where";
		$res = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while($fet = mysql_fetch_assoc($res)){
			$tableCus = '';
			$sql_formName = "SELECT ssi_form_name, ssi_form_type FROM $config[db_base_name].follow_ssi_form WHERE ssi_id = '$fet[form_id]' ";
			$res_formName = $logDb->queryAndLogSQL( $sql_formName, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$fet_formName = mysql_fetch_assoc($res_formName);
			$form_name = $fet_formName['ssi_form_name']; // ชื่อแบบฟอร์ม

			$have_datails = 0;
			$realMain = '';
			$list_question_code=$list_weight=''; //ไว้ข้างนอก
			#-LIST ALL
			$sql_2 = "SELECT SSI_Report.year, SSI_Report.names_code, SSI_Report.weight, SSI_Report.list_score ,  SSI_Report.average, SSI_Report.remark FROM $config[db_base_name].SSI_Report WHERE SSI_Report.names_code LIKE '$emp%' AND SSI_Report.form_id = '$fet[form_id]' AND SSI_Report.report_type = '4' AND SSI_Report.year LIKE '$year-$month%' AND SSI_Report.status != '99'  AND type_of_list = '3' ORDER BY SSI_Report.year"; //pt@2012-01-31

			$res_2 = $logDb->queryAndLogSQL( $sql_2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			if(mysql_num_rows($res_2)){
				$btn_excel = "
					<a href=\"javascript:void(0);\" onclick= \"javascript:window.open('".$thisPage.".php?func=viewCus&view_cus=$emp&year=$year&month=$month&form_id=$fet[form_id]&excel=1');\" >
					<img height='30' src=\"images/excel-2003.gif\" title='โหลด excel File'/>
					</a>
					";
				$sql_processDate = "SELECT SSI_Report.process_date FROM $config[db_base_name].SSI_Report WHERE SSI_Report.report_type = '4'  AND SSI_Report.year LIKE '$year-$month%' AND SSI_Report.status != '99' AND type_of_list = '3' $where ORDER BY SSI_Report.process_date DESC LIMIT 1";
				$res_processDate = $logDb->queryAndLogSQL( $sql_processDate, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$fet_processDate = mysql_fetch_assoc($res_processDate);
				$last_update_date = 'ข้อมูลล่าสุดเมื่อวันที่ : '.explode_date($fet_processDate['process_date']);
				$tableCus .= $tpl->tbHtml( $thisPage.'.html', 'TABLE_VIEW_CUS_HEAD' );
			}
			$tableCus_detail = '';
			while($fet_2 = mysql_fetch_assoc($res_2)){
				$expList_score = explode("/SPLIT/",$fet_2['list_score']);
				if($list_question_code==''){ //จะเก็บครั้งเดียวต่อฟอร์ม เพราะมันเป็นค่าเหมือนกัน
					$expCode = explode(",",$expList_score[0]);
					for($i=0;$i<count($expCode);$i++){
						$list_question_code .= '<th >'.$expCode[$i].'</th>';
					}
					$tableCus .= $tpl->tbHtml( $thisPage.'.html', 'TABLE_VIEW_CUS_QUES' );
				}
				if($list_weight==''){ //จะเก็บครั้งเดียวต่อฟอร์ม เพราะมันเป็นค่าเหมือนกัน
					$expWeight = explode(",",$fet_2['weight']);
					for($w=0;$w<count($expWeight);$w++){
						$list_weight .= '<th >'.$expWeight[$w].'</th>';
					}
					$tableCus .= $tpl->tbHtml( $thisPage.'.html', 'TABLE_VIEW_CUS_WID' );
				}

				$exp_date = explode("-",$fet_2['year']);
				$sell_date = $exp_date[2].'/'.$exp_date[1].'/'.$exp_date[0];

				$expCus = explode("/SPLIT/",$fet_2['remark']);
				$cusName = $expCus[1];

				$expSale = explode("/SPLIT/",$fet_2['names_code']);
				$saleNameFull = $expSale[1];

				$expPoint = explode(",",$expList_score[1]);
				$list_point='';
				for($p=0;$p<count($expPoint);$p++){
					$list_point .= '<td style="background-color:#DCDCDC;">'.$expPoint[$p].'</td>';
				}
				$cus_score = $fet_2['average'];

				$tableCus_detail .= $tpl->tbHtml( $thisPage.'.html', 'LIST_DATA_CUS' );

				$have_datails++;
			}
			if(mysql_numrows($res_2)) {

				$tableCus .= $tableCus_detail.$tpl->tbHtml( $thisPage.'.html', 'TABLE_VIEW_CUS_FOOT' );

				if($_GET['excel']){
					header("Content-Type: application/vnd.ms-excel");
					header('Content-Disposition: attachment; filename="'.$form_name.'_SSI_cus_'.date('j_F_Y').'.xls"');#ชื่อไฟล
					$html = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"
					xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv="Content-type" content="text/html;charset=UTF-8"></head><body>';
					$html .= $tableCus;
					$html .= '</body></html>';
					echo $html;
					exit();
				}else{
					echo $tableCus;
				}
			}
		}//form
	break;
}

CloseDB();
?>