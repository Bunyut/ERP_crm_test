$(function() {

    var setDataType;

    $.ajaxPrefilter(function( options ) {
        setDataType = options.dataType;
    });

    $.ajaxSetup({
        beforeSend: function( xhr ) {

            if (setDataType == 'progress') {
                var chkPreload = $('.preloadAjax').size();
                if(chkPreload == 0){
                    // create progress bar
                    crateMultiTagLoad();
                }
            }
            $('button:not([id*="load"])').each(function(){
                $(this).attr('disabled',true);
            });

            $('input[type="submit"]').each(function(){
                $(this).attr('disabled',true);
            });

            // if($('div#loadmore_submit_icon').size() == 0){
            //     var tagLoad = '<div id="loadmore_submit_icon" align="center" ';
            //     tagLoad     = tagLoad + 'style="position: fixed; height: 100%; width: 100%; left: 0px; top: 0px; z-index: 9999; vertical-align: middle;">';
            //     tagLoad     = tagLoad + '<img src="images/preload/kdui_preload_01.gif" ';
            //     tagLoad     = tagLoad + 'style="position:fixed; top:50%; left:50%; margin-top:-70px; margin-left:-50px; width:100px;">';
            //     tagLoad     = tagLoad + ' Loading...</div>';

            //     $('body').prepend(tagLoad);
            // }else{
            //   $('div#loadmore_submit_icon').show();
            // } 

            //alert('beforeSend');
        },
        complete: function(xhr, stat) {

            $('button:not([id*="load"])').each(function(){
                $(this).attr('disabled',false);
            });

            $('input[type="submit"]').each(function(){
                $(this).attr('disabled',false);
            });

            // if($('div#loadmore_submit_icon').size() > 0){
            //     $('div#loadmore_submit_icon').hide();
            // } 

            //alert('complete');
        },
        success: function(result,status,xhr) {
            // alert('success');
        },
        error: function(jqXHR, exception, err) {

            if (jqXHR.status === 0) {
                //alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        },
        xhr: function(){

            var xhr = new XMLHttpRequest();

            xhr.previous_text = ''; 
        
            var prevTag = 'prgIner_0';
            var prevNo  = '';

            var res_global = '';
            var onload_chk = true;

            xhr.res_global = '';
            xhr.onload_chk = true;

            var obj_res;

            xhr.addEventListener("progress",function(e){

                if (setDataType == 'progress') {

                    var new_response_raw        = xhr.responseText.substring(xhr.previous_text.length);
                    var obj_main                = new Object();
                    obj_res                     = new Object();
                    
                    res_global = new_response_raw;
                    xhr.res_global = new_response_raw;

                    if(new_response_raw != '' && xhr.previous_text != '')
                    {
                        new_response_raw    = new_response_raw.replace(/}{/g, "},{");
                        try {
                            obj_main            = JSON.parse( "["+new_response_raw+"]" );
                            var max             = obj_main.length;
                            obj_res             = obj_main[max-1];
                        }catch (e){
                            //console.log(e);
                        }         
                    }
                 
                    var precen      = obj_res.percen;
                    var colorMk     = '';
                   
                    xhr.previous_text           = xhr.responseText;

                    if(true)
                    {

                        if(precen > 100)
                        {
                            precen = 100;
                        }

                        var myDiv3 = $('#'+obj_res.tagNo); 
                        myDiv3.clearQueue();
                        myDiv3.stop();

                        $('#txtPercen_'+obj_res.no).html(precen+"%").show();

                        $('#'+obj_res.tagNo).animate({width: precen+"%"}, 'fast','swing',function(){
     
                                $('#'+obj_res.tagNo).css('overflow','visible');

                            }).css('overflow','visible');
                       
                        prevTag = obj_res.tagNo;
                        prevNo  = obj_res.no;

                    }

                    if(obj_res.titleChange != undefined)
                    {
                        $('.titlePreload').html(obj_res.titleChange );
                    }
                }

            },false);
            xhr.addEventListener("load",function(e){
                
                if (setDataType == 'progress') {
                    templComplete(obj_res);
                }
                

            },false);
            return xhr;
        },
        converters : {
            "* text":  window.String,
            "text html": true,
            "text json": jQuery.parseJSON,
            "text xml": jQuery.parseXML,
            "text noprogress": true,
            "text progress" : function(res){

                var str_rex = '^{"percen":';
                var newRe = new RegExp(str_rex);

                if(newRe.test(res)){
                   
                    var obj_main            = new Object();
                    var obj_res             = new Object();
                    
                    if(res != '')
                    {
                        res                 = res.replace(/}{/g, "},{");
                        obj_main            = JSON.parse( "["+res+"]" );
                        obj_res             = obj_main[obj_main.length-1];
                    }
                    return obj_res.msg;


                }else{
                    //console.log('on text'); 


                    // for (var i = 0; i <=100; i = i+50) {
                        
                    //     $('#txtPercen_0').html(i+"%").show();
                    //     $('#prgIner_0').animate({width: i+"%"}, 'fast','swing',function(){

                    //             $('#prgIner_0').css('overflow','visible');

                    //         }).css('overflow','visible');

                    // };

                    return res;
                }

            }
        }
    });
});
(function($){
    $.fn.acc_loadMore = function( tagShow, url, options, callback ){
	
		var tagThis			= this;
		var settings 		= $.extend( {}, $.fn.acc_loadMore.defaults, options);

		// Loading
		// tagThis.parent().append('<div class="loadmore_loading" align="center" style="display:none"><img src="images/lightbox-ico-loading.gif"> Loading...</div>');

		// click load more
		if( settings.typeWork == 'click' ){

			// this.bind( "click", function() {
			tagThis.click( function() {
				// console.log(options);
				settings				= $(tagThis).data('settings');
				settings.url 		 	= url;
				settings.tagShow 	 	= tagShow;
				settings.tagThis 	 	= tagThis;
				settings.this_loading 	= '.loadmore_loading';
				settings.callback 	 	= callback;
				settings.buttonSearch	= settings.buttonSearch;
				
				// tagThis.hide();
				// $(settings.this_loading).show();
				settings.clear	= false;
				settings.typeS 	= settings.typeShow;
				$.fn.acc_loadMore.getData( settings );
			});
		}
		
		// click search
		if( settings.buttonSearch != ''){
			// $(settings.buttonSearch).parent().append('<div class="loadmore_loading" align="center" style="display:none"><img src="images/lightbox-ico-loading.gif"> Loading...</div>');

			$(settings.buttonSearch).click( function() {

				settings 				= $.extend( {}, $.fn.acc_loadMore.defaults, options);
				settings.url 			= url;
				settings.tagShow 	 	= tagShow;
				settings.tagThis 	 	= tagThis;
				settings.this_loading	= '.loadmore_loading';
				settings.callback 	 	= callback;
				settings.buttonSearch	= settings.buttonSearch;

				if(settings.checkAll != ''){
					$(settings.checkAll).attr('checked', false);
				}

				// $(this).hide();
				// $(settings.this_loading).show();
				settings.clear	= true;
				settings.typeS 	= '';
				$.fn.acc_loadMore.getData( settings );
			});
		}
    };
	
	// Plugin defaults 
	$.fn.acc_loadMore.getData = function( settings ){

		// กรณีมีการส่ง function ทำก่อนดึงข้อมูล
		if(settings.clear === true){
			if(settings.BeforeFn){
				var checkBeforeFn = settings.BeforeFn.call( settings.tagThis );
			
				if(!checkBeforeFn){
					return false;
				}else{
					$(settings.tagShow).html('');
					settings.tagThis.show();
				}
			}else{
				$(settings.tagShow).html('');
				settings.tagThis.show();
			}
		}
		
		/* // กรณีให้เคลียร์ข้อมูล
		if(settings.clear == true){
			// console.log('clear');
			// clear value
			$.fn.acc_loadMore.defaults.valNum 		= 0;
			$.fn.acc_loadMore.defaults.valAll 		= 0;
			$.fn.acc_loadMore.defaults.valTagSearch	= '';
			$.fn.acc_loadMore.defaults.start		= 0;
			$.fn.acc_loadMore.defaults.statusSearch	= false;
		} */
	
		var sendValue	= settings.valSearch;
		var valSearch 	= '';
				
		// กรณีใช้ฟอร์มค้นหา
		if(settings.valTagSearch == '' && settings.statusSearch === false){
			if( settings.tagSearch ){
				var valSearch	= $(settings.tagSearch).serialize();
			
				if(sendValue != ''){
					valSearch = '&' + valSearch;
				}

				settings.valTagSearch = valSearch;
			}else{
				settings.valTagSearch = valSearch;
			}

			settings.statusSearch	= true;
		}else{
			valSearch = settings.valTagSearch;
			settings.statusSearch	= true;
		}
		
		sendValue	= sendValue + valSearch;
		
		// limit
		if(sendValue != ''){
			sendValue	= sendValue + '&limit=' + settings.limit;
		}else{
			sendValue	= sendValue + 'limit=' + settings.limit;
		}
		
		// start
		sendValue	= sendValue + '&start=' + settings.start;
		
		// num
		sendValue			= sendValue + '&num=' + settings.valNum;
		settings.sendValue	= sendValue;
		
		// alert(sendValue);
		
		if(settings.type == 'post'){
			$.fn.acc_loadMore.sendPost( settings );
		}else{
			$.fn.acc_loadMore.sendGet( settings );
		}
	};
	
	// send get 
	$.fn.acc_loadMore.sendGet = function( settings ){
	
		$.get(settings.url, settings.sendValue, function(res){
			try{
				var response = $.parseJSON(res);
				
				if(response.message){
					alert(response.message);
					return false;
				}

				if(response.status == 'success'){

					if(parseFloat(settings.valNum) == 0){
						$(settings.tagShow).html('');
					}
					
					if(response.html != ''){
						settings.start 	= response.start;
						settings.valNum = response.num;
					
						if(settings.typeS == 'append'){
							$(settings.tagShow).append(response.html);
						}else{
							$(settings.tagShow).html(response.html);
						}

						if(settings.bgLoadmore == ''){
							$(settings.tagThis).css('background','');
						}else{
							$(settings.tagThis).css('background',settings.bgLoadmore);
						}
					}else{
						$(settings.tagThis).attr('disabled', true);
						$(settings.tagThis).css('background','#F27A10');
					}

					if(settings.showNum != ''){
						if(response.numAll){
							settings.valAll 	= parseInt(response.numAll);
						}

						if(settings.valAll <= settings.start){
							$(settings.tagThis).attr('disabled', true);
							$(settings.tagThis).css('background','#F27A10');
						}else{
							$(settings.tagThis).attr('disabled', false);
						}
						
						$(settings.showNum).html($.fn.acc_loadMore.numberFormat(settings.start,0,1)+' / '+$.fn.acc_loadMore.numberFormat(settings.valAll,0,1));
					}

					// console.log('tagThis >> '+$(settings.tagThis).attr('id'));
					$(settings.tagThis).data('settings',settings);
					// console.log($(settings.tagThis).data('settings'));

					// $(settings.this_loading).hide();
					// $(settings.tagThis).show();

					// if( settings.buttonSearch != ''){
					// 	$(settings.buttonSearch).show();
					// }

					if ($.isFunction(settings.callback)) {
						settings.callback(response);
					}
					
				}else{
					alert(res);
				}
			}catch(err){
				// alert(err);
				$('div#main').html(err);
			}
		},'progress');
	};
	
	// send get 
	$.fn.acc_loadMore.sendPost = function( settings ){
	
		$.post(settings.url, settings.sendValue, function(res){
			try{
				var response = $.parseJSON(res);
				
				if(response.message){
					alert(response.message);
					return false;
				}

				if(response.status == 'success'){

					if(parseFloat(settings.valNum) == 0){
						$(settings.tagShow).html('');
					}
					
					if(response.html != ''){
						settings.start 	= response.start;
						settings.valNum	= response.num;
					
						if(settings.typeS == 'append'){
							$(settings.tagShow).append(response.html);
						}else{
							$(settings.tagShow).html(response.html);
						}

						if(settings.bgLoadmore == ''){
							$(settings.tagThis).css('background','');
						}else{
							$(settings.tagThis).css('background',settings.bgLoadmore);
						}
					}else{
						$(settings.tagThis).css('background','#F27A10');
					}

					if(settings.showNum != ''){
						if(response.numAll){
							settings.valAll 	= parseInt(response.numAll);
						}

						if(settings.valAll <= settings.start){
							$(settings.tagThis).attr('disabled', true);
							$(settings.tagThis).css('background','#F27A10');
						}else{
							$(settings.tagThis).attr('disabled', false);
						}
						
						$(settings.showNum).html($.fn.acc_loadMore.numberFormat(settings.start,0,1)+' / '+$.fn.acc_loadMore.numberFormat(settings.valAll,0,1));
					}

					// console.log('tagThis >> '+$(settings.tagThis).attr('id'));
					$(settings.tagThis).data('settings',settings);
					// console.log($(settings.tagThis).data('settings'));

					// $(settings.this_loading).hide();
					// $(settings.tagThis).show();

					// if( settings.buttonSearch != ''){
					// 	$(settings.buttonSearch).show();
					// }

					if ($.isFunction(settings.callback)) {
						settings.callback(response);
					}
					
				}else{
					alert(res);
				}
			}catch(err){
				$('div#main').html(err);
			}
		},'progress');
	};

	/* จัดรูปแบบตัวเลข */
	$.fn.acc_loadMore.numberFormat = function(num, decplaces,showtt) {
		// script จัดการ  bug javascript
		// last edit 04-02-2012 by.k
		if(!decplaces){decplaces = 0;}
		if(decplaces==undefined){decplaces = 0;}
		var exp = [];
		num = num.toString();
		exp = num.split('.');
		if(exp[1]){
			if(exp[1].length>=decplaces){
				exp[1] = exp[1]+'1';
			}
		}else{
			exp[1] = '00';
		}
		num = exp[0]+'.'+exp[1];
		var num_pos = Math.pow(10,decplaces);
		num = Math.round(num*num_pos)/num_pos;
		// End last edit 04-02-2012 by.k
		num = parseFloat(num);
		if (!isNaN(num)){
			var str = "" + Math.round (eval(num) * Math.pow(10,decplaces));
			if (str.indexOf("e") != -1) {return "Out of Range";}
			while (str.length <= decplaces){str = "0" + str;}
			var decpoint = str.length - decplaces;var tmpNum = str.substring(0,decpoint);
			//---------------Add Commas--------------------------
			var numRet = tmpNum.toString();if(showtt==1){var re = /(-?\d+)(\d{3})/;while (re.test(numRet)){numRet = numRet.replace(re, "$1,$2");}}
			
			if(parseFloat(decplaces) > 0){
				return numRet + "." + str.substring(decpoint,str.length);
			}else{
				return numRet;
			}

		}else{return "";}//return "0.00";
	};
	
	// Plugin defaults 
	$.fn.acc_loadMore.defaults = { 
		'type' 			: 'get', // tag ที่จะส่งค่าไปด้วย
		'tagSearch' 	: '', // tag ที่จะส่งค่าไปด้วย
		'buttonSearch' 	: '', // button ค้นหา
		'valSearch' 	: '', // ค่าที่จะส่งไป
		'valTagSearch'	: '',
		'statusSearch'	: false, // true = ค้นหาแล้ว, false = ยังไม่ได้ค้นหา
		'typeShow' 		: 'append', // ประเภทการแสดง
		'typeWork' 		: 'scroll', // ประเภทการทำงาน
		'limit' 		: 10, // จำนวนที่จะแสดงผล
		'start' 		: 0, // รายการแสดงเริ่มต้น
		'valNum' 		: 0,
		'valAll' 		: 0,
		'showNum'		: false,
		'BeforeFn' 		: '',
		'bgLoadmore' 	: '',
		'checkAll' 		: '' // input check all
	};
})(jQuery);

function templComplete(obj_res){

    var x = 0;
    var tempInf = [];
    var tempObj = {};
    tempObj.width = 600;
    tempObj.title = 'ค้นหาข้อมูล';
    tempInf[0] = tempObj;

    option = tempInf;

    var maxTag = option.length;

    // console.log('templComplete:100');

    // $.each(option, function(index, subObj){
    //  if(x == maxTag-1){

            
    //      $('#prgIner_'+x).animate({width: 100+"%"}, 'fast','swing',function(){

 //                $(this).css('overflow','visible');

 //                console.log('x:'+x);
 //                $('#txtPercen_'+x).html(100+"%").show();

 //                //$('#txtPercen_0').html(100+"%").show();
 //                alert('break comp');
    //          // $('.preloadAjax').slideUp('slow',function() {
    //          //  $(this).remove();
    //          // });
 //         }).css('overflow','visible');

    //  }else{
            
    //      $('#prgIner_'+x).animate({width: 100+"%"}, 'fast').css('overflow','visible');
 //            $('#txtPercen_'+x).html(100+"%").show();
 //        }
 //     x++;                                  
    // });

    
    //$('#prgIner_0').css({width: "100%",overflow:'visible'});
    $('#prgIner_0').animate({width: "100%"}, 'fast','swing',function(){

            $(this).css('overflow','visible');
            $('#txtPercen_0').html("100%").show();


            $('.preloadAjax').slideUp('slow',function() {
               $(this).remove();
             });

        }).css('overflow','visible');

    // alert('break');console
}

function crateMultiTagLoad() {
        
    var tempInf = [];
    var tempObj = {};
    tempObj.width = 600;
    tempObj.title = 'ค้นหาข้อมูล';
    tempInf[0] = tempObj;

    infGen = tempInf;

    var colorIn = ['#0097FB','#77FC3A','#FCB53A'];
 
    var container = $('<div>').css({
        'width':wTotal+'px',
        'margin-left':'auto',
        'margin-right':'auto',
        'margin-top':'200px'
    });
                        
    var wTotal      = 0;
    var wtxtTotal   = '';

    $.each(infGen, function(index, subObj){

        var txtPercen = $('<div>')
        .attr({
            'id':'txtPercen_'+index
        })
        .addClass('txtPercen')
        .css({
            'color':'#ffffff',
            'top':'15px',
            'position':'relative',
            'font-size':'12px',
            'right':'-5px'

        })
        .html('1%');

        var title = $('<div>')
        .attr({
           'id':'title_'+index
        })
        .css({
           'position':'absolute' ,
           'left':'0px',
           'top':'-28px',
           'color':'#ffffff'
        })
        .addClass('prel_title') 
        .html(subObj.title);

        var prgIner = $('<div>')
        .addClass('prel_pregInner')
        .attr({
           'id':'prgIner_'+index
        })
        .css({
            'width':'0%',
            'height':'100%',
            'text-align':'right',
            'overflow':'visible',
            'background':colorIn[index]
        })
        .append(txtPercen);

        var prgOuter = $('<div>')
        .addClass('prel_prgOuter')
        .attr({
           'id':'prgOuter_'+index
        })
        .css({
            'width':'100%',
            'height':'100%',
            'background':'#424141'
        }).html(prgIner);

        var tempProgress1 = $('<div>')
        .addClass('prel_tempProgress')  
        .attr({
            'id':'tempProgress_'+index
        })  
        .css({
            'width':subObj.width+"px",
            'height':'10px',
            'border-left':'2px solid '+colorIn[index],
            'float':'left',
            'position':'relative'
        })
        .append(prgOuter,title);                          

        wTotal +=   parseInt(subObj.width) + 2;  // plus border 
        
        $(container).append(tempProgress1);                   
    });
    
    $(container).css('width',wTotal+'px');       
    
    var divMainContener = $('<div>').attr({'id':""}).addClass('preloadAjax').append(container);

    $('body').append(divMainContener);              
}
