/**
*
*		Gradual v.1
*		Example:
*
*		- client site
*		jQuery('#crdit_tis_tb').gradualg('credit_tisg.php',{opt:'gradual'},{
*											limit_s:21
*									});		
*		Descript:
*		crdit_tis_tb			=>  tag id ตารางที่เรียกใช้
*		credit_tisg.php 		=>  server script
*		opt:gradual 			=>  param	ส่งไป server
*		limit_s:21				=>  limit เริ่มต้น
*
*
*		- server site
*		sql = "SELECT......limit $_POST['limit_s'],$_POST['limit_e'] ";
*
*		Descript:
*		ตัวแปรที่ต้องรับ
*		$_POST['limit_s']		=> limit เริ่มต้น
*		$_POST['limit_e']	=> limit ความยาว
*
*		Golf.
**/
(function($){

	$.fn.gradualgBtn = function( urlOrData,param, options ){
			
			var isUrl = typeof urlOrData == "string";
			var setting = $.extend({	
				url: isUrl ? urlOrData : null,
				placeBtn:false,
				param:null,
				rowTotal:param.rowTotal ? param.rowTotal :'',
				limit_s:0,
				limit_r:20
			},options);
	 	
			//$(window).unbind('scroll');	
			// $('#divBtnMore').remove();	
			$(this).next('#divBtnMore').remove();	
			return this.each(function(){
				new	$.Gradualg(this,param,setting);
				
			});
	};

	$.Gradualg = function(table,param,setting){
		//variable

		$.data(table,'target',{
						limits:setting.limit_s,
						status:'succ'
					});
					
		//store this in body 
		//--
		// chate edit 28-02-2014 
		var textBtnLoad = '';
		if(setting.rowTotal){ // ถ้าส่งค่ามาจะให้แสดงจำนวนทั้งหมดด้วย

			var rowCount = $(table).find('tbody tr').length;
			var trCount = (setting.rowTotal==0)? '0' : rowCount;
			var textBtnLoad = formatNumber(trCount,0,1)+'/'+ formatNumber(setting.rowTotal,0,1);

		}
		

		
		var num_field = $(table).find('tr:eq(0) th').size();
		var tell_waiting = $('<tr>').html($('<td>').attr({'colspan':num_field}).css({"text-align":"center","color":"#49aac2"}).html('รอสักครู...')).addClass('shadow');
		
		var btnMore = $("<input>")
								.attr({"type":"button","name":"btnGetMore","id":"btnGetMore"})
								.css({
										// "width":'180px',
										"margin-left":'auto',
										"margin-right":'auto',
										"margin-top":'10px',
										"color":"#333333",
										"padding":"5px",
										"text-align":"center"
								})
								.val('แสดงข้อมูลจำนวน '+textBtnLoad);
		// chate edit 28-02-2014 
		if(setting.rowTotal){ // ถ้าส่งค่ามาจะให้แสดงจำนวนทั้งหมดด้วย
			if(rowCount >= setting.rowTotal){ 
				$(btnMore).attr('disabled',true);
			}
		}
		

		var divBtnMore = $('<div>')
								.attr('id','divBtnMore')
								.css({"width":'50px',"margin-left":'auto',"margin-right":'auto',"margin-top":'10px'})
								.html(btnMore);				

		if(setting.url == null || setting.url == ''){
			return errorMassg('ไมได้กำหนด url');
		}

		$(btnMore).bind('click',function(e){
		
		// chate edit 28-02-2014 
		if(setting.rowTotal){ // ถ้าส่งค่ามาจะให้แสดงจำนวนทั้งหมดด้วย
			var rowCount 	= $(table).find('tbody tr').length;
			var trCount 	= parseFloat(rowCount) + parseFloat(setting.limit_s);



			var textBtnLoad = formatNumber(trCount,0,1)+'/'+formatNumber(setting.rowTotal,0,1);
			$(this).val("แสดงข้อมูลจำนวน "+textBtnLoad);
			if(trCount >= setting.rowTotal){ $(this).attr('disabled',true);}
		}


					if($('#'+$(table).attr('id')).html() == null){	

						// $(divBtnMore).remove();	
						// $(table).find('#divBtnMore').remove();	
						$(table).next('#divBtnMore').remove();	
						console.log('remove 1');
						
					}else{
				
							param2 = $.extend(param,{
									limit_s:$.data(table,'target').limits,
									limit_e:setting.limit_r
								});
				
							// sum next limit
							n_limits = $.data(table,'target').limits + setting.limit_r;
							$.data(table,'target',{
										limits:n_limits,
										status:'send'
									});
							//--
				
				
							// waiting calling
							$(table).find('tbody').append(tell_waiting);
							//--
							// jQuery.ajax({
							// 						url:setting.url,
							// 						type: 'POST',
							// 						data:param2,
							// 						success: function (res){
							// 							if(res == ''){
							// 								$(divBtnMore).remove();	
							// 							}
							// 							$.data(table,'target',{
							// 									limits:n_limits,
							// 									status:'succ'
							// 								});
													
							// 							$(table).find('tr.shadow').remove();
							// 							$(table).find('tbody:eq(0)').append(res);
														
							// 							// chate edit 28-02-2014 
							// 							if(setting.rowTotal){ // ถ้าส่งค่ามาจะให้แสดงจำนวนทั้งหมดด้วย
							// 								var chkTrLeng 	= $(table).find('tbody tr').length;
							// 								var addTrTotal 	= chkTrLeng+'/'+setting.rowTotal;
							// 								$("#divBtnMore").find('#btnGetMore').val("แสดงข้อมูลจำนวน "+addTrTotal);
							// 							}

							// 							// chate edit 21-03-2015 เฉพาะเมนู้ รายละเอียดสรุปการจำหน่ายรายคัน
							// 							if($("#tbl_sumaaryOU tfoot tr td font").html()){
							// 								var i =0;
							// 								$("#tbl_sumaaryOU tbody tr").each(function() {
							// 								  	i++;
							// 								});
							// 								$("#tbl_sumaaryOU tfoot tr td font").html('จำนวนทั้งหมด '+i+'รายการ');
							// 							}
							// 							//--
														 
							// 						}
							// 		});

							var tempInf = [];
						    var tempObj = {};
						    tempObj.width = 600;
						    tempObj.title = 'ค้นหาข้อมูล';
						    tempInf[0] = tempObj; 


						    var param_get ='';
						    jQuery.each(param2,function(key,val){

						    	param_get += key+"="+val+"&";

						    });



						    if(setting.type =='post'){ // กรณีที่จะส่งมาเป็น post 
						    	type_post 	= param_get;
						    	type_get	= '';
						    }else{
						    	type_post 	='';
						    	type_get  	= param_get;
						    }

						    // console.log(setting);

							jQuery.preloadMultiAjax(setting.url+"?"+type_get,type_post,tempInf,
								function(res,ev){

									if(res == ''){
										// $(divBtnMore).remove();	
										// $(table).find('#divBtnMore').remove();	
										$(table).next('#divBtnMore').remove();	
										console.log('remove 2');
									}
									$.data(table,'target',{
											limits:n_limits,
											status:'succ'
										});
								
									$(table).find('tr.shadow').remove();
									$(table).find('tbody:eq(0)').append(res);
									
									// chate edit 28-02-2014 
									if(setting.rowTotal){ // ถ้าส่งค่ามาจะให้แสดงจำนวนทั้งหมดด้วย
										var chkTrLeng 	= $(table).find('tbody tr').length;
										var addTrTotal 	= chkTrLeng+'/'+setting.rowTotal;
										$("#divBtnMore").find('#btnGetMore').val("แสดงข้อมูลจำนวน "+addTrTotal);
									}

									// chate edit 21-03-2015 เฉพาะเมนู้ รายละเอียดสรุปการจำหน่ายรายคัน
									if($("#tbl_sumaaryOU tfoot tr td font").html()){
										var i =0;
										$("#tbl_sumaaryOU tbody tr").each(function() {
										  	i++;
										});
										$("#tbl_sumaaryOU tfoot tr td font").html('จำนวนทั้งหมด '+i+'รายการ');
									}
									//--
						        
						       }
						    );

						
						 
					}
			}); 

		
		if(setting.placeBtn){
			$(setting.placeBtnObj).html(divBtnMore);	
		}else{
			$(table).after(divBtnMore);	
		}

		function errorMassg(str){
			alert(str);
			return false;
		}
	
	};

	$.fn.destroy_gradualg = function(){
			
			return this.each(function(){
					$(window).unbind('scroll');	
			});
	};

})(jQuery);