jQuery.fn.inputnumber = function() {
	$(this).live('blur keyup change',function(){
		$(this).val($(this).val().replace(/[^0-9\.]/g,''));
		return false;
	});
};

jQuery.fn.inputmax = function(max) {
	var min = 0;
	$(this).live('blur keyup change',function(){
		if($(this).val() < min || $(this).val() > max){ 
			$(this).val("");
		  	alert('กรุณากรอกข้อมูลให้ถูกต้อง');
		    return false;
		}else{
			return true;
		}
	});
};

jQuery.fn.inputvalidate = function() {
	var has_empty	= '';
	$(this).find('input:text,textarea').each(function(){
		if($.trim($(this).val()) == ''){
			has_empty = 'Y';
			$(this).css('border-color','#ff0000');
		}
	});
	if(has_empty==''){
		return true;
	}else{
		return false;
	}
};