/**
 * Create by: Nuy
 * Create date: 11.08.2014
 * Last Modify: 11.08.2014@Nuy
 * Description: หน้าอัพโหลดไฟล์ผู้รับผิดชอบลูกค้า
 */
 
 var config = {
	support : "application/vnd.ms-excel",			// Valid file formats
	form: "import_resp",							// Form ID
	dragArea: "dragAndDropFiles",					// Upload Area ID
	uploadUrl: "import_resperson.php?func=upload"	// Server side upload url
}

jQuery(document).ready(function() {
	initMultiUploader(config);
});

function ValidateUpload(fup) {
	$('#dragAndDropFiles').empty();
	var validFileExtensions = ["xlsx", "xls", "csv"];
	var StatusUpload = "";
	// var fSize = ($("#multiUpload")[0].files[0].size / 1024);
    // var fup = document.getElementById("multiUpload");           
    var fileName = fup.value;
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
    for (var i = 0; i < validFileExtensions.length; i++) {               
        if (ext == validFileExtensions[i]) {
			// if (fSize < 10240) {
                StatusUpload = "True";
				break;
			// }                  
        } else {
            StatusUpload = "False";
        }                
    }
	
    if (StatusUpload == "True") {
        return true;
    } else {
        fup.value = "";
        alert("ประเภทไฟล์ที่รองรับได้แก่ .xlsx, .xls, .csv (Character UTF-8 เท่านั้น)");
		return false;
    }
}

function CreateDatalist(filename, type)
{
	var dataUrl = 'import_resperson.php?func=list_data&filename=' + filename + '&type=' + type;

	$.ajax({
		"type": "POST",
		"contentType": "application/json; charset=utf-8",
		"url": dataUrl,
		"success": function(objData){
			$('#DivformList').empty();
			$('#dragAndDropFiles').empty();
			$('input#multiUpload').val('');
			
			try{
				var deJson =  jQuery.parseJSON(objData);
				
				if(deJson.alert == ''){
					$('#DivformList').append(deJson.html);
				}else{
					alert(deJson.alert);
				}
			}catch(err){
				alert(err);
			}
		},
		"error": function(jqXHR, exception) {
			$('#dragAndDropFiles').empty();
			$('input#multiUpload').val('');
		
            if (jqXHR.status === 0) {
                // alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
		}
	});
}

function upload_clist(obj){
	var frmUp	= $(obj);
	var sizeUp	= frmUp.find('input[name="list[]"]:checked').size();
	var sizeUps	= frmUp.find('input[name="lists[]"]:checked').size();
	
	// เช็คเลือกรายการ
	if(sizeUp <= 0 && sizeUps <= 0){
		alert('กรุณาเลือกรายการในการบันทึก');
	}else{
		var val 	= frmUp.serializeObject();
		var enJson	= JSON.stringify(val, null, 2);
		var sendURL	= frmUp.attr('action');
		
		$.post(sendURL,{ data : enJson },function(res){
			try{
				var response = $.parseJSON(res);
				
				if(response.message){
					alert(response.message);
					return false;
				}

				if(response.status == true){
					alert('บันทึกข้อมูลเรียบร้อยแล้ว');
					browseCheck('main','import_resperson.php','');
				}else{
					alert(res);
				}
			}catch(err){
				alert(err);
			}
		});
	}
	
	return false;
}