/*!
 * pLoadmore-1.0.0.plugins.js
 * https://github.com/bencomtech/pLoadmore
 * 
 * @author Pichid Detson
 * @email bencomtech@gmail.com
 * @version 1.0.0
 * @require jQuery 1.9.0+
 */
(function($) {
    'use strict';
    // Define the pLoadmore default settings
    $.pLoadmore = {
        defaults: {
            url: null,
            type: 'get',
            data: {},
            start: 0,
            limit: 50,
            before: null,
            after: null,
            everyBefore: null,
            everyAfter: null,
            progress: false,
            moreType: 'button'
        },
        buttton: {
            text: 'แสดงข้อมูลจำนวน',
            class: 'ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary button-click'
        }
    };
    // Constructor
    var pLoadmore = function($el, settings, btnStyle) {
        // Private variable and function
        var currt = settings.start,
            autoLoad = true,
            // Initialization function
            _init = function() {
                _checkBeforeFunction();
                _getData(function(data) {
                    _manageData(data, function(current, total) {

                        switch (settings.moreType) {

                            case 'button':

                                _manageButton(current, total, function() {
                                    if (settings.progress === true && $('#pProgress_wrapper').length > 0) {
                                        $('#pProgress_bar').css('width', '100%').html('100%');
                                        $('#pProgress_wrapper').delay(400).slideUp(function() {
                                            $(this).remove();
                                        });
                                    };
                                    _listenerLoadmore();
                                });

                            break;

                            case 'auto':

                                _manageAuto(current, total, function() {
                                    if (settings.progress === true && $('#pProgress_wrapper').length > 0) {
                                        $('#pProgress_bar').css('width', '100%').html('100%');
                                        $('#pProgress_wrapper').slideUp(function() {
                                            $(this).remove();
                                        });
                                    };
                                    _listenerBottom();
                                });

                            break;

                        };
                    });
                });
                _checkAfterFunction();
            },
            // check and call function before pLoadmore start
            _checkBeforeFunction = function() {
                if (typeof settings.before === 'function') {
                    settings.before();
                };
            },
            // check and call function after pLoadmore start
            _checkAfterFunction = function() {
                if (typeof settings.after === 'function') {
                    settings.after();
                };
            },
            // listener button loadmore
            _listenerLoadmore = function() {
                $('#btn_loadmore').click(function(evt) {
                    _getData(function(data) {
                        _manageData(data, function(current, total) {
                            _manageButton(current, total, function() {
                                if (settings.progress === true && $('#pProgress_wrapper').length > 0) {
                                    $('#pProgress_bar').css('width', '100%').html('100%');
                                    $('#pProgress_wrapper').delay(400).slideUp(function() {
                                        $(this).remove();
                                    });
                                };
                            });
                        });
                    });
                });
            },
            // create button html and apply style
            _genButtonStyle = function() {
                var onButton = '<span class="ui-button-icon-primary ui-icon"></span><span class="ui-button-text">' + btnStyle.text + ' (<span id="current">0</span>/<span id="total">0</span>)</span>';
                var button = '<button style="width: 250px; margin-left: 10px; background: none repeat scroll 0% 0% rgb(242, 122, 16);" type="button" id="btn_loadmore" class="' + btnStyle.class + '">' + onButton + '</button>';
                var wrapper = '<div style="padding: 4px; text-align: center;">' + button + '</div>';
                return wrapper;
            },
            // create button, update data on button, disable or enable button
            _manageButton = function(current, total, callback) {
                if (currt === 0 && $('#btn_loadmore').length === 0) {
                    $el.parent().after(_genButtonStyle());
                };
                if (total !== null) {
                    $('#btn_loadmore').find('#total').text(total);
                };
                if (current !== null) {
                    $('#btn_loadmore').find('#current').text(current);
                };
                if ($('#btn_loadmore').find('#total').text() == $('#btn_loadmore').find('#current').text()) {
                    $('#btn_loadmore').attr('disabled', true).css({
                        'background': 'rgba(242,122,16,1)'
                    });
                } else {
                    $('#btn_loadmore').attr('disabled', false).css({
                        'background': ''
                    });
                };
                if (typeof callback === 'function') {
                    callback();
                };
            },
            _listenerBottom = function() {
                $(window).scroll(function() {
                    if ((autoLoad === true) && ($(document).height() <= $(window).scrollTop() + $(window).height())) {
                        _getData(function(data) {
                            _manageData(data, function(current, total) {
                                _manageAuto(current, total, function() {
                                    if (settings.progress === true && $('#pProgress_wrapper').length > 0) {
                                        $('#pProgress_bar').css('width', '100%').html('100%');
                                        $('#pProgress_wrapper').slideUp(function() {
                                            $(this).remove();
                                        });
                                    };
                                });
                            });
                        });
                    };
                });
            },
            _genDivStyle = function() {
                var div = '<div id="div_loadmore" style="padding: 4px; text-align: center;">' + btnStyle.text + ' (<span id="current">0</span>/<span id="total">0</span>)</div>';
                return div;
            },
            _manageAuto = function(current, total, callback) {
                if (currt === 0 && $('#div_loadmore').length === 0) {
                    $el.parent().after(_genDivStyle());
                    $('#div_loadmore').find('#total').text(total);
                };
                $('#div_loadmore').find('#current').text(current);
                if ($('#div_loadmore').find('#total').text() == $('#div_loadmore').find('#current').text()) {
                    autoLoad = false;
                };
                if (typeof callback === 'function') {
                    callback();
                };
            },
            // render data from ajax
            _manageData = function(data, callback) {
                if (currt === 0) {
                    $el.html(data.html);
                } else {
                    $(data.html).appendTo($el);
                };
                if (typeof callback === 'function') {
                    callback(data.current, data.total);
                };
                currt++;
            },
            // add start and limit variable in post method
            _genDataPostMethod = function() {
                var result = settings.data;
                switch (typeof settings.data) {
                    case 'string':
                        result += '&start=' + parseInt(currt * settings.limit) + '&limit=' + settings.limit;
                        break;
                    case 'object':
                        result.start = parseInt(currt * settings.limit);
                        result.limit = settings.limit;
                        break;
                };
                return result;
            },
            // create xmlhttprequest obj for listenner ajax state 3 (loading)
            _createXhr = function() {
                if (window.XMLHttpRequest) {
                    return new XMLHttpRequest(); //code for IE7+, Firefox, Chrome, Opera, Safari
                } else {
                    return new ActiveXObject("Microsoft.XMLHTTP"); //code for IE6, IE5
                };
            },
            // if set progress: true, this functon is create progressbar on middle screen and show percentage
            _createProgress = function() {
                var html = '<div id="pProgress_wrapper"><div id="pProgress_box"><div id="pProgress_bar"><span>0%</span></div></div></div>';
                $(html).appendTo('body').css({
                    'width': '100%',
                    'height': '100%',
                    'background-color': 'rgba(65,65,65,0.9)',
                    'position': 'fixed',
                    'top': '0',
                    'left': '0',
                    'z-index': '1000000000000000000000'
                });
                $('#pProgress_box').css({
                    'background-color': 'rgba(65,65,65,1)',
                    'position': 'relative',
                    'width': '600px',
                    'top': '50%',
                    'margin-left': 'auto',
                    'margin-right': 'auto',
                    'z-index': '10000000000000000000000',
                    'padding': '2px'
                });
                $('#pProgress_bar').css({
                    'background-color': 'rgba(0,85,250,1)',
                    'width': '0%',
                    'z-index': '100000000000000000000000',
                    '-webkit-transition': 'width 0.1s ease',
                    '-moz-transition': 'width 0.1s ease',
                    'transition': 'width 0.1s ease',
                    'text-align': 'center',
                    'color': 'rgba(255,255,255,1)',
                    'font-size': '0.8em',
                    'font-family': 'Arial, Helvetica, sans-serif',
                    'font-weight': 'bold'
                });
            },
            // set percentage on progressbar
            _setPercenProgress = function(data) {
                if (settings.progress === true) {
                    for (var i in data) {
                        $('#pProgress_bar').css('width', data[i].percen + '%').html(data[i].percen + '%');
                    };
                };
            },
            // request to server and receive response
            _getData = function(callback) {
                if (typeof settings.everyBefore === 'function') {
                    settings.everyBefore();
                };
                if (settings.progress === true && $('#pProgress_wrapper').length === 0) {
                    _createProgress();
                };
                switch (settings.type) {
                    case 'get':
                    case 'GET':
                        $.ajax({
                            url: settings.url + '&start=' + parseInt(currt * settings.limit) + '&limit=' + settings.limit,
                            xhr: function() {
                                var xhr = _createXhr();
                                xhr.previousText = '';
                                xhr.addEventListener('progress', function(evt) {
                                    var resText = xhr.responseText;
                                    if (resText.indexOf('{"percen":') !== -1) {
                                        if (resText.indexOf('{"total":') < 0) {
                                            resText = resText.substr(xhr.previousText.length);
                                            xhr.previousText += resText;
                                            resText = resText.replace(/}{/g, '},{');
                                            var data = JSON.parse('[' + resText + ']');
                                            _setPercenProgress(data);
                                        };
                                    };
                                }, false);
                                return xhr;
                            },
                            success: function(resText) {
                                var index = resText.indexOf('{"total":');
                                if (index !== -1) {
                                    var percenText = resText.substr(0, index);
                                    resText = resText.replace(percenText, '');
                                }
                                var data = JSON.parse(resText);
                                if (typeof callback === 'function') {
                                    callback(data);
                                };
                            },
                        });
                        break;
                    case 'post':
                    case 'PSOT':
                        $.ajax({
                            url: settings.url,
                            type: 'post',
                            data: _genDataPostMethod(),
                            xhr: function() {
                                var xhr = _createXhr();
                                xhr.previousText = '';
                                xhr.addEventListener('progress', function(evt) {
                                    var resText = xhr.responseText;
                                    if (resText.indexOf('{"percen":') !== -1) {
                                        if (resText.indexOf('{"total":') < 0) {
                                            resText = resText.substr(xhr.previousText.length);
                                            xhr.previousText += resText;
                                            resText = resText.replace(/}{/g, '},{');
                                            var data = JSON.parse('[' + resText + ']');
                                            _setPercenProgress(data);
                                        };
                                    };
                                }, false);
                                return xhr;
                            },
                            success: function(resText) {
                                var index = resText.indexOf('{"total":');
                                if (index !== -1) {
                                    var percenText = resText.substr(0, index);
                                    resText = resText.replace(percenText, '');
                                }
                                var data = JSON.parse(resText);
                                if (typeof callback === 'function') {
                                    callback(data);
                                };
                            },
                        });
                        break;
                };
                if (typeof settings.everyAfter === 'function') {
                    settings.everyAfter();
                };
            };
        // Initialization
        _init();
        return $el;
    };
    $.fn.pLoadmore = function(options, style) {
        return this.each(function() {
            var $this = $(this);
            var settings = $.extend({}, $.pLoadmore.defaults, options);
            var btnStyle = $.extend({}, $.pLoadmore.buttton, style);
            if (settings.url === null || settings.url.length == 0 || settings.url == '') {
                return;
            }
            new pLoadmore($this, settings, btnStyle);
        });
    };
})(jQuery);