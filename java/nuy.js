function browseN(tag,path,value,loading){
	//alert(tag+':'+path+':'+value+':'+loading);
	//$('input[type="button"]').attr("disabled", "disabled");
	if(loading!='' && loading!='default'){
		$(loading).show();
		$(tag).empty();
		$.post(path, value, function(res){ $(tag).html(res); })
		.complete(function(){ /*$('input[type="button"]').attr("disabled", "");*/ $(loading).hide(); });
		//$(loading).fadeIn(200,function(){ $(loading).fadeOut(200,function(){ $.post(path, value, function(res){ $(tag).html(res); }); }); });
	}else if(loading=='default'){
		$(tag).empty();
		var strLoadingText = "<div style='text-align:center' id='pre_loading'><img src='images/loading.gif'  style='padding-left:auto;padding-right:auto;'/><br /><span style='font-size: 12px; font-weight: bold; color: #000;'>Loading ...</span></div>"
		$(tag).html(strLoadingText);
		
		$.post(path, value, function(res){ $(tag).empty(); $(tag).html(res); })
		.complete(function(){ /*$('input[type="button"]').attr("disabled", "");*/ });
	}else{
		$.post(path, value, function(res){ $(tag).empty(); $(tag).html(res); })
		.complete(function(){ /*$('input[type="button"]').attr("disabled", "");*/ });
	}
}

/**
 * ส่งข้อมูลไปทั้งฟอร์ม
 * @param ch ส่วนที่ทำการแสดง
 * @param path หน้าที่รับข้อมูลที่ส่งไป
 * @param value ค่าที่ต้องส่งไปหน้ารับข้อมูล
 * @param tag ส่วนของข้อมูลที่ต้องการส่งไป
 * @param type ชนิด
 */
function browseFrm(ch,path,value,tag,type){
	var response = '';
	
	if(value){ value += '&'+jQuery(tag).serialize(); }else{ value = jQuery(tag).serialize(); }
	switch(type){
		default:
			jQuery.post(path, value, function(res){ response = res; })
			.complete(function(){ jQuery(ch).empty(); jQuery(ch).html(response); });
		break;
		case 'loading':
			jQuery(ch).empty();
			var strLoadingText = "<div style='text-align:center; margin-top: 40px;' id='pre_loading'><img src='images/loading.gif'  style='padding-left:auto;padding-right:auto;'/><br /><span style='font-size: 12px; font-weight: bold; color: #000;'>Loading ...</span></div>"
			jQuery(ch).html(strLoadingText);
			
			jQuery.post(path, value, function(res){ response = res; })
			.complete(function(){
				jQuery(ch).empty();
				jQuery(ch).html(response);
			});
		break;
	}
}

//upload file ajax
function Jupload(page,value,shows,loading){
	$.ajaxFileUpload
	(
		{
			url:page,
			secureuri:false,
			fileElementId:value,
			dataType: 'json',
			success: function (data, status)
			{
				if(typeof(data.error) != 'undefined')
				{
					if(data.error != '')
					{
						alert(data.error);
					}else
					{
						alert(data.msg);
						browseCheck(shows,'receivable_accounts.php?method=list_data','');
						//$("span#divPage").html(data.msg);
						/*var data=data.msg;
						value = data.split(",");
						var re = value[1].split(":");
						var vReturn = String;
						
						if(re.length>1){
							for(var i=0;i<re.length;i++){
								if(i!=''){
									vReturn+='&'+re[i];
								}else{
									vReturn+='&'+re[i];
								}
							}
						}else{
							vReturn = value[1];
						}
						
						//alert(value[0]);
						//alert(vReturn);
						Ajax('GET',value[0],shows,vReturn,loading);*/
						//Ajax('GET',value[0],shows,value[1],loading);
					}
				}
			},
			error: function (data, status, e)
			{
				alert(e);
			}
		}
	)
}
// กรอกได้เฉพาะตัวเลข และ -
function checktel(e){ keyPressed = e.which;
if(((keyPressed < 48) || (keyPressed > 57)) && (keyPressed != 0) && (keyPressed != 8) && (keyPressed != 46) && (keyPressed != 45)) keyPressed = e.preventDefault();
}
// ตัดเพือหาตัวแปรในรูปแบบ test,test2
function cutSerialize(value){
	var txt = '';
	var cutEQ = new Array();
	var cutAnd = value.split('&');
	var len = cutAnd.length;
	for(var i=0;i<len;i++){
		cutEQ = cutAnd[i].split('=');
		if((i+1)!=cutAnd.length){ txt += cutEQ[0]+','; }else{ txt += cutEQ[0]; }
	}
	return txt;
}
// เลือกได้เฉพาะไฟล์รูป
function chkFile(tag){
	var fty = new Array(".gif",".jpg",".jpeg",".png");
	value = $(tag).val().toLowerCase();
	var re = false;
	
	if(value !=""){
        for(i=0;i<fty.length;i++){
            if(value.lastIndexOf(fty[i])>=0){ 
               re = true;
            }
        }
    }
	if(re==false){ $(tag).val(''); }
	return re;
}
//เช็ครหัสบัตรประชาชน
function chkIDCard(input,path,tag,edit){
	if($(input).val().length==13){
		$(input).css('background-color','#CD5C5C'); $(input).attr('disabled','disabled');
		$.post(path, 'method=chkCard&idCard='+$(input).val()+'&edit='+edit, function(res){ $(tag).html(res); })
		.complete(function(){ $(input).removeAttr('disabled'); $(input).css('background-color',''); });
	}
}
//เลขประจำตัวผู้เสียภาษี
function chkIDTax(input,path,tag,edit){   // input#tax_id_number,receivable_accounts.php,font#chkTax,
	if($(input).val().length==13){
		$(input).css('background-color','#CD5C5C'); $(input).attr('disabled','disabled');
		$.post(path, 'method=chkTax&idCardTax='+$(input).val()+'&edit='+edit, function(res){ $(tag).html(res); })
		.complete(function(){ $(input).removeAttr('disabled'); $(input).css('background-color',''); });
	}
}
//เช็ครหัสบัญชีลูกหนี้
function chkAcc(input,path,tag,edit){
	if($(input).val().length>1){
		$(input).css('background-color','#CD5C5C'); $(input).attr('disabled','disabled');
		$.post(path, 'method=chkAccount&noAcc='+$(input).val()+'&edit='+edit, function(res){ $(tag).html(res); })
		.complete(function(){ $(input).removeAttr('disabled'); $(input).css('background-color',''); });
	}
}
//เช็ค input radio box
function chkRadio(){
	$('input#radioBox').val('');
	if($('input#cust_title:checked').val()){ $('input#radioBox').val($('input#radioBox').val()+$('input#cust_title:checked').val()+','); }else{ $('input#radioBox').val($('input#radioBox').val()+','); }
	if($('input#cust_type_group:checked').val()){ $('input#radioBox').val($('input#radioBox').val()+$('input#cust_type_group:checked').val()+','); }else{ $('input#radioBox').val($('input#radioBox').val()+','); }
	if($('input#sex:checked').val()){ $('input#radioBox').val($('input#radioBox').val()+$('input#sex:checked').val()+','); }else{ $('input#radioBox').val($('input#radioBox').val()+','); }
	if($('input#reMail:checked').val()){ $('input#radioBox').val($('input#radioBox').val()+$('input#reMail:checked').val()+','); }else{ $('input#radioBox').val($('input#radioBox').val()+','); }
}
//เลือกธนาคาร แล้วแสดงลิสสาขา
function select_bank(path,tag,val){
	$.post(path,'method=list_branch&name='+val,function(res){
      res = '<option value="">- เลือกสาขา -</option>'+res;
		$(tag).html(res);
	});
}
//insert receivable_accounts.php
function insert_account(path){
	chkRadio();
	var txtA = '';
	var be_txt = $('select[name="firstname"] option:selected').index();
	if(!$('input#cust_title:checked').val()){ $('input#cust_title:first').focus(); alert('กรุณาเลือกคำนำหน้าชื่อ'); return false; }
	if($('input#cust_title:checked').val()=='อื่นๆ'){ if(!$('select#firstname').val() && be_txt=="0"){ $('select#firstname').focus(); alert('กรุณาเลือกคำนำหน้าชื่อ'); return false; } }
	/* if(!$('input#num_account').val()){ $('input#num_account').focus(); alert('กรุณากรอกรหัสบัญชีลูกหนี้'); $('input#num_account').css('border','1px dashed #FF0000'); return false; }else{ $('input#num_account').css('border',''); }
	if($('input#chkAccount').val()=='N'){ $('input#num_account').focus(); alert('รหัสบัญชีลูกหนี้ซ้ำ'); $('input#num_account').css('border','1px dashed #FF0000'); return false; }else{ $('input#num_account').css('border',''); }*/
	/* if(!$('input#cust_type_group:checked').val()){ $('input#cust_type_group:first').focus(); alert('กรุณาเลือกประเภทลูกหนี้'); return false; } */
	if(!$('input#cust_name').val()){ alert('กรุณากรอกชื่อ'); $('input#cust_name').css('border','1px dashed #FF0000'); $('input#cust_name').focus(); return false; }else{ $('input#cust_name').css('border',''); }
	/* if(!$('input#cust_surename').val() && $('input#cust_type_group:checked').val()!=2){ txtA += 'กรุณากรอกนามสกุล'; $('input#cust_surename').css('border','1px dashed #FF0000'); }else{ $('input#cust_surename').css('border',''); }
	if(txtA){ if(txtA=='กรุณากรอกนามสกุล'){ $('input#cust_surename').focus(); }else{ $('input#cust_name').focus(); } alert(txtA); return false; } */
	/*if(!$('input#sex:checked').val() && $('input#cust_type_group:checked').val()!=2){ $('input#sex:first').focus(); alert('กรุณาเลือกเพศ'); return false; }*/
	if(!$('select#receivable_type').val()){ $('select#receivable_type').focus(); alert('กรุณาเลือกประเภทลูกหนี้'); $('select#receivable_type').css('border','1px dashed #FF0000'); return false; }else{ $('select#receivable_type').css('border',''); }
	if(!$('input#cust_id_card').val() && $('input#cust_type_group:checked').val()!=2){ $('input#cust_id_card').focus(); alert('กรุณากรอกหมายเลขบัตรประชาชน'); $('input#cust_id_card').css('border','1px dashed #FF0000'); return false; }else{ $('input#cust_id_card').css('border',''); }
	if($('input#chkCard').val()=='N' && $('input#cust_type_group:checked').val()!=2){ $('input#cust_id_card').focus(); alert('หมายเลขบัตรประชาชนซ้ำ'); $('input#cust_id_card').css('border','1px dashed #FF0000'); return false; }else{ $('input#cust_id_card').css('border',''); }
	
	if(!$('select#acc_type_dialog').val()){ $('select#acc_type_dialog').focus(); alert('กรุณาเลือกหมวดหมู่หลัก'); $('select#acc_type_dialog').css('border','1px dashed #FF0000'); return false; }else{ $('select#acc_type_dialog').css('border',''); }
	if(!$('select#account_type').val()){ $('select#account_type').focus(); alert('กรุณาเลือกประเภทบัญชี'); $('select#account_type').css('border','1px dashed #FF0000'); return false; }else{ $('select#account_type').css('border',''); }
	if(!$('select#account_group').val()){ $('select#account_group').focus(); alert('กรุณาเลือกหมวดหมู่บัญชี'); $('select#account_group').css('border','1px dashed #FF0000'); return false; }else{ $('select#account_group').css('border',''); }
	if(!$('select#account_main').val()){ $('select#account_main').focus(); alert('กรุณาเลือกชื่อบัญชีหลัก'); $('select#account_main').css('border','1px dashed #FF0000'); return false; }else{ $('select#account_main').css('border',''); }
	
	$('#manage_receivable_accounts').hide();
	var strLoadingText = "<div style='text-align:center' id='pre_loading'><img src='images/loading.gif'  style='padding-left:auto;padding-right:auto;'/><br /><span style='font-size: 12px; font-weight: bold; color: #000;'>Loading ...</span></div>"
	$('div#loadAcc').html(strLoadingText);

	//value ได้มาจาก function cutSerialize
	value = "method,idAcc,radioBox,cust_title,cust_name,cust_surename,cust_nikename,cust_type_group,cust_birthday,sex,home_tel,mobile_tel,work_tel," +
			"receivable_type,chkCard,cust_id_card,recei_date_contact,recei_time_payment,recei_credit_line,recei_billing_date,last_date_for_sale," +
			"last_date_for_payment,receivable_check,post_bill_adjustmen,Cus_Big_Picture,Cus_Small_Picture,reMail,geography_doc,province_doc," +
			"amphur_doc,district_doc,village_doc,add_doc,post_doc,geography_card,province_card,amphur_card,district_card,village_card,add_card," +
			"post_card,geography_work,province_work,amphur_work,district_work,village_work,add_work,post_work,name_work,position_work,email_work," +
			"remark,customer_pic,num_account,chkAccount,acc_id,acc_log,firstname,accounts_branch,tax_id_number,bank_account_numbers,type,trade_id_number," +
			"acc_type_dialog,account_type,account_group,account_main";

	$("input[name='bank_detail[]']").each(function(){
		value += ','+$(this).attr('id');
	});

	Jupload(path,value,'main','');
}
//search receivable_accounts.php
function searchAcc(path,ShowDateq){
	if($('input#type_number').val()=='' && $('input#type_name').val()=='' && $('input#type_acc_number').val()=='' && $('select#cust_type').val()==''){
		$('input#type_number').css('border','1px dashed #FF0000');
		$('input#type_name').css('border','1px dashed #FF0000');
		$('input#type_acc_number').css('border','1px dashed #FF0000');
		$('select#cust_type').css('border','1px dashed #FF0000');
		$('input#type_number').focus();
		return false;
	}
	browseCheck('listData','receivable_accounts.php?method=list_data&'+ShowDateq+'&status=search&'+$('#assets_form_contain').find('input,select,textarea').serialize(),'');
}
//edit receivable_accounts.php
function edit_account(path,ShowDateq){
	if(!$('input#accounts_id:checked').val()){ alert('กรุณาเลือกข้อมูลทีต้องการแก้ไข'); return false; }
	$.post(path, 'method=editAcc&'+ShowDateq+'&idAcc='+$('input#accounts_id:checked').val(), function(res){ $('div#main').empty(); $('div#main').html(res); })
	.complete(function(){ $('#customer_info_list').hide(); $('#manage_receivable_accounts').show(); })
}
//report receivable_accounts.php
function report_account(path,ShowDateq){
	if(!$('input#accounts_id:checked').val()){ alert('กรุณาเลือกข้อมูลทีต้องดูข้อมูล'); return false; }
	$.post(path, 'method=reportAccount&'+ShowDateq+'&idAcc='+$('input#accounts_id:checked').val(), function(res){ $('div#main').empty(); $('div#main').html(res); })
	.complete(function(){ $('#customer_info_list').hide(); $('#manage_receivable_accounts').show(); })
}
//เช็คคำนำหน้าชื่อ ให้แสดงลิสคำหน้าชื่อ อื่นๆ หรือไม่
function selectCusTitle(val){
	switch(val){
		case 'นาย':
			$('select#firstname').attr('disabled','disabled');
		break;
		case 'นาง':
			$('select#firstname').attr('disabled','disabled');
		break;
		case 'นางสาว':
			$('select#firstname').attr('disabled','disabled');
		break;
		case 'อื่นๆ':
			$('select#firstname').attr('disabled','');
		break;
	}
}
//คลิกให้ check radio box
function clickIconImage(){
	$('label.icon-image').click(function() {
 		var txt = $(this).attr('txt');
 		value = txt.split(",");
 		$('input'+value[0]+'[value="'+value[1]+'"]').attr('checked',true);
	});
}
//แสดงป้อบอัพถ้าค้นหาไม่เจอ
function alertCus(path,val){
	if(confirm('ไม่พบข้อมูล ลูกหนี้หนี้ที่ต้องการค้นหา แต่มีข้อมูลในฐานข้อมูลลูกค้า\nต้องการใช้ข้อมูลลูกค้า?')){
		$.get(path,'method=alertCus&'+val,function(data){
			$('div#alertCus').html(data).addClass('ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-resizable').center(); 
		});
	}
}
//เลือกลูกค้าจากการค้นหา
function selectCus(path,cusNo,name){
	if(confirm('ยืนยันการเพิ่ม '+name+' เป็นลูกหนี้')){
   	$.post(path, 'method=chkSelect&cusNo='+cusNo, function(res){ $('div#listData').html(res); });
   }
}
//ไม่มีข้อมูลใน RECEIVABLE_ACCOUNTS แต่มีใน maincus_data ไปหน้าเพิ่ม
function addCus(tag,path,val,type){

	$.post(path, val, function(res){ $('div#'+tag).html(res); })
	.complete(function(){ new_receivable_accounts(type); });

}
//เช็คชื่อและนามสกุลใน maincus_data ว่ามีหรือยัง
function chkAccount(path,editName,editSurname){
   var val = '';

	if($('input#cust_name').val() && $('input#cust_name').val()!=editName){
		var name = $('input#cust_name').val();
   	var surname = $('input#cust_surename').val();
        
  		val = 'method=chkName&name='+name+'&surname='+surname;

      $.post(path, val, function(res){ 
      	if(res){ 
      		if(confirm('มีลูกค้าที่มีชื่อเหมือนกัน ต้องการเลือกลูกค้าที่มีอยู่แล้วหรือไม่')){
					//$('div#alertCus').html(res).addClass('ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-resizable').center();      		
      		
            	val = 'method=editAcc&type=search&&idAcc='+res;

            	$.post(path, val, function(res){ $('div#main').html(res); })
					.complete(function(){ new_receivable_accounts('add'); });
 				} 
 			} 
 		});
	}
}
//เช็ค ACC_NUMBER ใน RECEIVABLE_TYPE
function chk_accNumber(path,edit){

	if($('input#receivable_acc_number').val()){

		var val = 'method=chk_accNumber&accNumber='+$('input#receivable_acc_number').val()+'&edit='+edit;
		$.post(path, val, function(res){ $('font#alertNumber').html(res); });
		
	}

}
//เลือกวันที่วันนี้ใน receivable_report.html
function thisDate(thisDate,since,till){  $('input'+since).val(thisDate);$('input'+till).val(thisDate); }
//เช็คการเลือกวันที่เริ่มต้นใน receivable_report.html
function chkStart(tagStart,tagFinish){
	value = $(tagStart).val();
	var dates = value.split("-");
	dates[1] = dates[1]-1; dates[2] = dates[2] = dates[2]-543;
	
	$(tagFinish).datepicker('destroy');
	$(tagFinish).datepicker({ dateFormat: 'dd-mm-yy', minDate: new Date(dates[2],dates[1],dates[0]) });
}
//เช็คการเลือกวันที่สุดท้ายใน receivable_report.html
function chkFinish(tagStart,tagFinish){
	value=$(tagFinish).val();
	var dates = value.split("-");
	dates[1]=dates[1]-1; dates[2] = dates[2] = dates[2]-543;
	
	$(tagStart).datepicker('destroy');
	$(tagStart).datepicker({ dateFormat: 'dd-mm-yy', maxDate: new Date(dates[2],dates[1],dates[0]) });
}
//เช็คฟอร์ม บันทึกใบกำกับสินค้า
var chk_Date = new Array('','0000-00-00','00-00-0000');
function chk_menu_sub(since,till,show_zone_manage,gen_case){
	var re = 'Y';
	if($.inArray($('input'+since).val(), chk_Date)>-1){ 
		$('input'+since).css('border','1px dashed #FF0000'); $('input'+since).focus(); re = 'N';   
	}else{ 
		$('input'+since).css('border',''); 
	}
	if($.inArray($('input'+till).val(), chk_Date)>-1){ 
		$('input'+till).css('border','1px dashed #FF0000'); if(re=='Y'){ $('input'+till).focus(); } re = 'N'; 
	}else{ 
		$('input'+till).css('border','');
	}
	if(gen_case=='vat'){
		if(!$('select#company_vat').val()){ $('select#company_vat').css('border','1px dashed #FF0000'); if(re=='Y'){ $('select#company_vat').focus(); } re = 'N';
		}else{ $('select#company_vat').css('border',''); }	
	}	
	
	if(re=='N'){ return false; }
	window.open('receivable_report.php?method=report&'+$('div'+show_zone_manage).find('input,select').serialize()+'&gen_case='+gen_case, 'receivable_report',
	'toolbar=no,location=no,directories=no,status=yes,menubar=yes,scrollbars=yes,copyhistory=no,resizable=yes');
}
// ฟังค์ชั่น การเรียงข้อมูล
var aAsc = [];
function sortTable(nr) {
	aAsc[nr] = aAsc[nr]=="asc"?"desc":"asc";
	$("table#receivable_report>tbody>tr").tsort("td:eq("+nr+")[abbr]",{order:aAsc[nr]});
}
// ฟังชั่น แสดงเป็นไฟล์ excel
function reportExcel(path){
    path += '&print=excel';
    
    window.open(path,'newwindow','height=50,width=50,top=0,left=0,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes,directories=no,status=no,titlebar=no');
    //$.get(path, '', function(res){ /*$(tag).html(res);*/ });
}
// ฟังชั่น แสดงเป็นไฟล์ pdf
function reportPDF(path){
    path += '&print=pdf';
    
    window.open(path,'newwindow','height=50,width=50,top=0,left=0,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes,directories=no,status=no,titlebar=no');
    //$.get(path, '', function(res){ /*$(tag).html(res);*/ });
}
// ฟังชั่น แสดงเป็นไฟล์ excel
function open_exel(target,report_case){
	switch (report_case){
		case "billing": 
			case_name = '';//$('div').find('input').serialize();
		break;
	}
	window.open(target+'?method=open_exel&export='+report_case+'&'+case_name,'newwindow','height=50,width=50,top=0,left=0,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes,directories=no,status=no,titlebar=no');
}
// ฟังชั่น เลือกลูกค้าทั้งหมด แล้วชื่อลูกค้าที่เลือกไว้จะหายไป
function allCus(tagCus,tagAll){
	if($(tagAll).attr('checked')){ if($(tagCus).val()){ $(tagCus).val(''); } }else{ $(tagAll).attr('checked','checked'); }
}
// เลือก radio ใน receivable_updates_checkall ให้เปลี่ยนแท็บข้างล่างด้วย
function radioTab(tab,val){
   $('a[href='+tab+']').click();
}
// เลือก tab ใน receivable_updates_checkall ให้เปลี่ยน radio ข้างบนด้วย
function tabRadio(radio,val){
    $(radio).attr('checked','checked');
    $('input#type').val(val);
    
    if(val=='credit'){
        $('li.cheque_search').addClass('hidden');
        $('li.credit_search').removeClass('hidden');
    }else{
        $('li.cheque_search').removeClass('hidden');
        $('li.credit_search').addClass('hidden');
    }
}

function tabSearch(radio,val){ 
    if(val=='invoice'){
        // $('li.billing_search').addClass('hidden');
        // $('li.invoice_search').removeClass('hidden');
		$('.billing_search').addClass('hidden');
        $('.invoice_search').removeClass('hidden');
		$('#payall_billing_num').val('');
    }else{
        // $('li.billing_search').removeClass('hidden');
        // $('li.invoice_search').addClass('hidden');
		$('.billing_search').removeClass('hidden');
        $('.invoice_search').addClass('hidden');
		$('#payall_invoice_num').val('');
    }
}
// บันทึกปรับปรุงเช็ครับล่วงหน้า
function confirm_verify_paymentAll(){
	if(confirm('ยืนยันการปรับปรุงเช็ค !!') == true){
		var case_name = $('input#type').val();
		var tab = parseInt($('input#selected_index').val());
		var frame = ''; var nRadio = '';

		switch(tab){
			case 0:
				nRadio = 'input#cheque_type';
				frame = 'div#verify_payment_check';
			break;
			case 1:
				nRadio = 'input#credit_type';
				frame = 'div#verify_payment_credit';
			break;
		}
		
		var size = $(frame+" input[name='into_account[]']").size(),list = $(frame+" input[name='into_account[]']"),total_volume = 0;
		for(var i=0; i<size; i++){ if(list[i].value !=''){ total_volume++;  } }
		if(total_volume==0){ alert(' กรุณากรอกวันที่ปรับปรุงและเลขบัญชี  อย่างน้อย 1 รายการ !'); return false; }
		
		$.post('receivable_updates_check.php',$(frame).find('input,select').serialize()+'&method=confirm_verify_payment&case_name='+case_name+'&action_from=updates_checkall',function(data){
			alert(data);
			
			$.post('receivable_updates_checkall.php','method=list_data&'+$('div#rotator').find('input,select').serialize(),function(res){
				$('div#main').html(res);
			})
			.complete(function(){ radioTab('#tab-'+(tab+1),case_name); });
		});
	}
}
//เช็คการค้นหาปรับปรุงเช็ครับล่วงหน้า
function search_updates_checkall(path){
	var tab = parseInt($('input#selected_index').val());
	var val = $('div#rotator').find('input,select').serialize();
	
	$('div#main').empty();
	var strLoadingText = "<div style='text-align:center' id='pre_loading'><img src='images/loading.gif'  style='padding-left:auto;padding-right:auto;'/><br /><span style='font-size: 12px; font-weight: bold; color: #000;'>Loading ...</span></div>"
	$('div#main').html(strLoadingText);	
	
   $.post(path,'method=list_data&'+val,function(res){
		$('div#main').html(res);
	})
	.complete(function(){ radioTab('#tab-'+(tab+1),$('input#type').val()); });
}
//popup บันทึกออกใบลดหนี้ / เพิ่มหนี้ ใน receivable_credit_note
function frm_credit_note(path,id,invoice){
   $.get(path,'method=from_data&id='+id+'&invoice='+invoice,function(data){
       $('div#divShowForm').html(data).addClass('ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-resizable').center();
   });
}
//คำนวณเงิน ใบลดหนี้ / เพิ่มหนี้ ใน receivable_credit_note
function cal_note(){
	var i = 0,sum = 0,diff = 0,vat = 0,total = 0,amount = 0;
	var count = $("input[name='real[]']").size(),list = $("input[name='real[]']");
	
	while(i<count){
	
		sum += numSplitText(list[i].value);    
	
	i++;
	}
	
	diff = sum-numSplitText($('input#balance').val()); //หาผลต่าง
	if(diff<0){ diff = 0-diff; $('input#type').val(0); }else{ $('input#type').val(1); } //ไม่ให้ติดลบ และหาว่าเป็นใบเพิ่มหนี้ หรือลดหนี้
	vat = diff*numSplitText($('input#inputVat').val())/100 //หาภาษี
	total = diff+vat//หาผลรวม
	
	//ส่วนที่เป็น input
	$('input#correct').val(roundnumber(sum,2));
	$('input#diff').val(roundnumber(diff,2));
	$('input#vat').val(roundnumber(vat,2));
	$('input#total').val(roundnumber(total,2));
	//ส่วนที่แสดงผล
	$('td#correct').html(formatNumber(sum,2,1)); //มูลค่าที่ถูกต้อง
	$('td#diff').html(formatNumber(diff,2,1)); //ผลต่าง
	$('td#vat').html(formatNumber(vat,2,1)); //ภาษีมูลค่าเพิ่ม
	$('td#total').html(formatNumber(total,2,1)); //รวมทั้งสิ้น
}
//เช็คการกรอกข้อมูล ก่อนบันทึก ใบลดหนี้ / เพิ่มหนี้ ใน receivable_credit_note
function chk_credit_note(path){
	var status = false, i = 0;

	if(!$('input#num_credit').val()){ $('input#num_credit').css('border','1px dashed #FF0000'); $('input#num_credit').focus(); return false; }else{ $('input#num_credit').css('border',''); }
	if(!$('input#date').val()){ $('input#date').css('border','1px dashed #FF0000'); $('input#date').focus(); return false; }else{ $('input#date').css('border',''); }
  
	var count = $("input[name='real[]']").size(),list = $("input[name='real[]']");

	while(i<count){
	
		if(numSplitText(list[i].value)>0){
			status = true;
		}
	
	i++;
	}
	
	if(status==false){ $("input[name='real[]']").css('border','1px dashed #FF0000'); return false; }
	
	$.post(path,$('div#dialog-form').find('input,select,textarea').serialize()+'&method=save',function(data){
		alert(data);
		set_jnone('divShowForm');
		search_credit_note('keyword','creditnote');
	});
}
//ค้นหากิจการใน working_company
function search_company_data(){

	var val = '&company_sname_search='+$('#company_sname_search').val()+'&company_fname_search='+$('#company_fname_search').val();
	$('tbody#company_list_info').empty();
	var strLoadingText = "<div style='text-align:center' id='pre_loading'><img src='images/loading.gif'  style='padding-left:auto;padding-right:auto;'/><br /><span style='font-size: 12px; font-weight: bold; color: #000;'>Loading ...</span></div>"
	$('tbody#company_list_info').html(strLoadingText);
	
	$.get('working_company.php','method=reload_list'+val,function(data){  
		$('tbody#company_list_info').html(data);
	});
}
//ฟอร์มเพิ่มกิจการใน working_company
function into_company_form(){
	$.get('working_company.php','method=into_company_form',function(data){  
		$('#into_company_form').html(data).addClass('ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable ui-resizable').center();
	});
}
//ฟอร์มแก้ไขกิจการใน working_company
function edit_company_list(path,id){
   $.get('working_company.php','method=edit_company_list&id='+id,function(data){
	    $('#into_company_form').html(data).addClass('ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable ui-resizable').center();
   });
}
//เช็คชื่อกิจการมีแล้วหรือยัง working_company
function chkData_company(path,tag,edit,field,show){
	var val = $(tag).val().trim();
	edit = edit.trim();
	
	if(val!=edit){
		browseN(show,path,'method=chkData&val='+val+'&field='+field,'');
	}else{
		$(show).html('');
	}
}
//เช็คข้อมูลก่อน เพิ่ม/แก้ไข กิจการ ใน working_company
function confirm_into_company(path){
   var re = true, fo = '', method = 'confirm_into_company';

	//เช็คชื่อกิจการ
	if(!$('input#company_fname').val()){ $('input#company_fname').css('border','1px dashed #FF0000'); re = false; if(!fo){ fo = 'input#company_fname'; } }else{ $('input#company_fname').css('border',''); }
	if($('font#chkName').html()){ $('input#company_fname').css('border','1px dashed #FF0000'); re = false; if(!fo){ fo = 'input#company_fname'; } }else{ if($('input#company_fname').val()){ $('input#company_fname').css('border',''); } }
	//เช็ครหัสบริษัท
	if(!$('input#company_code').val()){ $('input#company_code').css('border','1px dashed #FF0000'); re = false; if(!fo){ fo = 'input#company_code'; } }else{ $('input#company_code').css('border',''); }
	if($('font#chkCode').html()){ $('input#company_code').css('border','1px dashed #FF0000'); re = false; if(!fo){ fo = 'input#company_code'; } }else{ if($('input#company_code').val()){ $('input#company_code').css('border',''); } }
	//เช็คชื่อย่อ
	if(!$('input#company_sname').val()){ $('input#company_sname').css('border','1px dashed #FF0000'); re = false; if(!fo){ fo = 'input#company_sname'; } }else{ $('input#company_sname').css('border',''); }
	if($('font#chkSName').html()){ $('input#company_sname').css('border','1px dashed #FF0000'); re = false; if(!fo){ fo = 'input#company_sname'; } }else{ if($('input#company_sname').val()){ $('input#company_sname').css('border',''); } }
	//เช็คเลขประจำตัวผู้เสียภาษี
	if(!$('input#company_inv').val()){ $('input#company_inv').css('border','1px dashed #FF0000'); re = false; if(!fo){ fo = 'input#company_inv'; } }else{ $('input#company_inv').css('border',''); }
	if($('font#chkINV').html()){ $('input#company_inv').css('border','1px dashed #FF0000'); re = false; if(!fo){ fo = 'input#company_inv'; } }else{ if($('input#company_inv').val()){ $('input#company_inv').css('border',''); } }

	if(re==false){ $(fo).focus(); return re; }

	if($('input#company_id').val()){ method = 'confirm_edit_company'; }

	$.post(path,$('form#confirm_into_company').find('input,select,textarea').serialize()+'&method='+method,function(res){
		browseCheck('company_list_info','working_company.php?method=reload_list','');
		alert(res);
		set_jnone('into_company_form');
	});
}
//ลบกิจการ ใน working_company
function dels_company_list(path,id){
	if(confirm('ยืนยันการลบข้อมูล')){
		$.post(path,'method=dels_company_list&id='+id,function(res){
			if(res){
				$('tr#type_list_'+id).remove();
			}
		});
	}
}
/*-----------------------CHECKFORM--------------------------*/
/**
 * ใช้เช็คค่าว่างฟอร์ม
 * -- หน้าที่ใช้ --
 * checkFormAll.js
 * @param Array dataCH ใช้รับค่า [ไอดีที่ต้องการเช็ค][ชนิดของไอดีที่เช็ค เช่น typeTEXTBOX][ข้อความ alert ที่ต้องการ] return true or false
*/
var alertAll='';
function checkFormSubmitNuy(dataCH){
   var alertAll='';
   var count = dataCH.length;
  
	for(var i in dataCH){
		if(dataCH[i][1]=="typeTEXTBOX"){
			alertAll += CHtypeTEXTBOXNuy(dataCH[i][3],dataCH[i][2]);
		}
		if(dataCH[i][1]=="typeDATEPICKER"){
			alertAll += CHtypeDATEPICKER(dataCH[i][3],dataCH[i][2]);
		}
		if(dataCH[i][1]=="typeValueDate"){
			alertAll += CHValueDate(dataCH[i][3],dataCH[i][2]);
		}
		if(dataCH[i][1]=="typeSelectAddress"){
			alertAll += CHValueAddress(dataCH[i][3],dataCH[i][2]);
		}
	}

	if(alertAll!=''){
		alert(alertAll);
		return false;
	}else{
		return true;		
	}
}
/**
 * ใช้เช้คค่าว่าง TEXTBOX
 * -- หน้าที่ใช้ -- 
 * checkform.js
 * @param String idCH ไอดีที่จะเช็คค่า
 * @param String alertCH ข้อความ alert
*/
function CHtypeTEXTBOXNuy(valCH,alertCH){
	valCH = jQuery.trim(valCH);
	if(valCH=='' || valCH=="undefined" || !valCH || valCH=='0.00'){
		alertCH=alertCH+'\n';
	}else{
		alertCH='';
	}

	return alertCH;
}
/**
* ใช้เช็คการกรอกตัวเลข ถ้าไม่ใช่ตัวเลขให้ เป็น 0
 * @param String val ค่าที่ส่งมา
 * @param String tag แท็ก html
*/
function chkNumAcc(val,tag){
	val = $(tag).val();
	var rec = roundnumber(val, 0);
   if(!rec){ rec = 0; }
	$(tag).val(rec);
}
/**
* เปลี่ยนเรียงค่าสภาพคล่อง
 * @param String type ชนิด เป็น up หรือ down
 * @param String id ไอดี 
 * @param String tag แท็ก
 * @param String url หน้าที่จะส่งไป
*/
function chOrder(type,id,tag,url){
	var thisVal = 0,chVal = 0,thisType = 0,chType = 0,oid = 0;
	thisVal = $(tag).val();
	thisType = $(tag).attr('types');
	
	if(type=='up'){
		chVal = $(tag).parents('tr').prev().find('input').val();
		chType = $(tag).parents('tr').prev().find('input').attr('types');
		oid = $(tag).parents('tr').prev().find('input').attr('oid');
		
		if(chVal==thisVal){ chVal = parseFloat(chVal) - 1; }
	}else{
		chVal = $(tag).parents('tr').next().find('input').val();
		chType = $(tag).parents('tr').next().find('input').attr('types');
		oid = $(tag).parents('tr').next().find('input').attr('oid');
		
		if(chVal==thisVal){ chVal =  parseFloat(chVal) + 1; }
	}
	
	if((chVal || chVal==0) && thisType == chType && oid){
		$.post(url+'?func=chOrder','id='+id+'&oid='+oid+'&type='+type+'&thisVal='+thisVal+'&chVal='+chVal,function(res){
			switch(url){
				case 'acc_number_type.php': search_acc_type(); break;
				case 'acc_number_group.php': search_acc_group(); break;
				case 'acc_main_name.php': search_acc_main_name(); break;
			}
		});
	}
}
/**
* สลับปุ่ม เพิ่ม/แก้ไข สมุดรายวันเฉพาะ daily_specific_journal
 * @param String type ชนิด เป็นเพิ่มหรือแก้ไข
*/
function chButton_specific(type){
	var button = '';
	
	if(type=='edit'){
		button = '<span class="ui-button-icon-primary ui-icon ui-icon ui-icon-script"></span><span class="ui-button-text">แก้ไข</span>';
	}else if(type=='clear'){
		clear_box_specific();
		button = '<span class="ui-button-icon-primary ui-icon ui-icon ui-icon-circle-plus"></span><span class="ui-button-text">เพิ่ม</span>';
	}else{
		button = '<span class="ui-button-icon-primary ui-icon ui-icon ui-icon-circle-plus"></span><span class="ui-button-text">เพิ่ม</span>';
	}
	
	$('button#button_add_row').html(button);
}
/**
* ลบแถวและถ้าไม่มีข้อมูลให้ซ่อนปุ่มบันทึก daily_specific_journal
 * @param String tag แท็กที่ต้องการลบ
*/
function remove_listSpecific(tag){
    var counter = $('#box-table-a >tbody >tr').length;
    if(counter==1){
        $('button#saveSpecific').hide();
    }

    $('tr#'+tag).remove();
}
/**
* ลบแถวและถ้าไม่มีข้อมูลให้ซ่อนปุ่มบันทึก daily_specific_journal
 * @param String tag แท็กที่ต้องการลบ
*/
function saveDaily_specific(type){
	if($('.check_list_box:checked').size()==0){
		alert('คุณยังไม่ได้เลือกรายการ'); return false;
	}

	if(confirm('ยืนยันการบันทึก')==true){
		//search_specific();
		dataFormSubmit('specific_list_body','daily_specific_journal.php?func='+type,'','spacific_form');
		$('button#saveSpecific').hide();
		$('input#check_all_list').attr('checked','');
	}
}
/**
*ค้นหาข้อมูลใน daily_journal
 * @param String tag แท็กที่ต้องการลบ
*/
function search_dailyJournal(case_name){
	if(case_name=='all'){
		browseCheck('content','daily_journal.php?func=search_daily','');
	}else{
		browseCheck('content','daily_journal.php?func=search_daily&com='+$('select#company_id').val()+'&date_b='+$('input#date_input_b').val()+'&date_e='+$('input#date_input_e').val()+'&journal_doc_code='+$('input#daily_journal_doc_code').val(),'');
	}
}
/**
* เลือกรหัสไปรษณีย์จากการเลือกอำเภอ
 * @param String tag ตำแหน่งที่จะแสดงข้อความ
 * @param String tagInput ตำแหน่งที่ต้องการแสดงรหัสไปรษณีย์
 * @param String path หน้าที่ดึงข้อมูลมาแสดง
 * @param String val ค่าที่สงไป
*/
function postcodeDefault(tag,tagInput,path,val){
    $.post(path, val, function(res){
        res = res.split('###');

        if(res[1]){
            var ment = '<font color="green">แนะนำ '+res[0]+'</font><br> '+res[1];
            $(tag).empty(); $(tag).html(ment);
        }
        $(tagInput).val(res[0]); 
    })
    .complete(function(){  });
}
/**
* เช็คฟอร์มก่อนบันทึกสมุดรายวันเฉพาะ แบบที่ 2
 * @param String url หน้าที่บันทึกข้อมูล
 * @param String tag ตำแหน่งที่ต้องการเปลี่ยน
 * @param String form_id ฟอร์มที่ต้องการเช็ค
 * @param String param สถานะ เป็น เพิ่ม หรือแก้ไข
 * @param String id ไอดี ที่ส่งไปแก้ไข
*/
function frmSubmit_specific_journal(url,tag,form_id,param,id){
	var arr_check = [];
	$.each( $(tag), function(){
		arr_check.push(Array($(this).attr('id'),'typeTEXTBOX', $(this).attr('alt'), $(this).val() )) ;
	});
	
	if(checkFormSubmitNuy(arr_check)==true){
		dataFormSubmit('specific_list_body',url+'.php?func=save_daily&param='+param+'&id='+id,'',form_id);
	}
}
/**
* ค้นหาข้อมูลในสมุดรายวันเฉพาะ แบบที่ 2
*/
function search_specific2(){
	var com_id='',date='',specific='',doc='',name='',acc='',ra = '';

	if($('select#company_id_search').val()){ com_id = $('select#company_id_search').val(); }
	if($('input#date_input_search').val()){ date = $('input#date_input_search').val(); }
	if($('select#specific_id_search').val()){ specific = $('select#specific_id_search').val(); }
	if($('input#doc_code_search').val()){ doc = $('input#doc_code_search').val(); }
	if($('input#name_list_search').val()){ name = $('input#name_list_search').val(); }
	if($('input#acc_id_refer_search').val()){ var acc = $('input#acc_id_refer_search').val(); }
	
	PbrowseCheck('specific_list_body','daily_specific_journal2.php?func=search_data&comid='+com_id+'&date='+date+'&specific='+specific+'&doc='+doc+'&name='+name+'&acc='+acc,'');
}
/**
* ลบข้อมูลสมุดรายวันเฉพาะ แบบที่ 2
 * @param String numR แถวที่จะลบ
*/
function specific_del2(numR){
	if($('#id_head_'+numR).val()!="" && $('#id_list_'+numR).val()!=""){
		var hid = $('#id_head_'+numR).val();
		var lid = $('#id_list_'+numR).val();
		$.get('daily_specific_journal2.php?func=del_daily&hid='+hid+'&lid='+lid,'');
		remove_box('tr_'+numR);
	}else{
		remove_box('tr_'+numR);
	}
	alert('ลบข้อมูลเรียบร้อย');
}

function clickto_print(url,form,type){
	if(type=='form'){
		document.getElementById(form).target = '_blank';
		document.getElementById(form).method = 'post';
	    document.getElementById(form).action = url;
	    document.getElementById(form).submit();
	}else if(type=='noInput'){
		var exp = url.split('?');
		var Targets = '_blank';
		
		switch(exp[0]){
			case 'withholding_certificate_exports_report.php':
				exp[1] += '&'+$('form#'+form).find('input[name="section"]:checked,input[name="reach"]:checked,input[name="deduct"]:checked').serialize();
			break;
		}

		document.getElementById(form).target = Targets;
		document.getElementById(form).method = 'post';
	    document.getElementById(form).action = exp[0]+'?'+exp[1];
	    document.getElementById(form).submit();
	}else{
		window.open(url,null,'height=700,width=400,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes');
	}
}

/**
 * Create by: Nuy
 * Create date: 27.07.2011
 * Last Modify: 03.08.2011@Nuy
 * Description: หน้าการจัดการแบบฟอร์ม CR สำหรับคำนวณคะแนนควาพึงพอใจ SSI
 */

/**
* 
* เช็คชื่อฟอร์มแบบสัมภาษณ์ความพอใจในฐานข้อมูลว่าซ้ำหรือเปล่า
* @param String path   ไฟล์ php ที่เรียกใช้
*/
//var ttm='';
function chkNameQues(path,edit){
	//setTimeout(function(){ttm = '';	},1500);
	//if(ttm==''){
	if($('input#ssi_title').val().length>0){
		$.post(path, 'func=chkName&name='+$('input#ssi_title').val()+'&edit='+edit, function(res){
			if(res=='ชื่อซ้ำ'){
				$('font#chkName').html(res);
				$('input#ssi_title').css('background-color','#FFDAB9');
			}else{
				$('font#chkName').html('');
				$('input#ssi_title').css('background-color','');
			}
		});
	}
	//}
	//ttm=1;
}
/**
* 
* เมื่อคลิกที่คำถามหลักแสดงคำถามรอง
*
*/
function showMainQues(){
	$('div.listEvent').click(function(){
		var id = $(this).attr('id').split('_');

		//ถ้า hQues ยังไม่แสดง
      if($('div#hQues_'+id[1]).attr('style')!='display: block;'){
         if($('input#event_id').val() && $('input#event_id').val()!=id[1]){
             if(confirm('ยืนยันการเปลี่ยนอีเวนต์')){
             	//ลบคำถามรองทั้งหมด ในหน้าแก้ไข
					$('input[id^=question_id_]').each(function(){
               	delQues('',$(this).val(),'sub');
               });             
             
             	$('input#event_id').val(id[1]); //เปลี่ยนไอดีอีเวนต์
               $('table#tableQues tr[id^=ts]').remove(); //ลบแถวคำถามรอง
               $('input[id^=id_sub_]').attr('checked',''); //checkbox ที่เช็คอยู่ให้เอาออก
               $('div.listSub').removeClass('selectQues'); //คลาส selectQues เอาออก
               $('div[id^=hQues]').attr('style','display: none;'); //ให้คำถามในอีเวนต์ซ่อนไว้
             }else{
               return false;
             }
         }else{
             $('input#event_id').val(id[1]);
			}
      }

		$('div#hQues_'+id[1]).toggle();

      if($('div#hQues_'+id[1]).attr('style')=='display: none;'){
         $('div#hQues_'+id[1]+' input[id^=id_sub_]').attr('checked','');
         $('div#hQues_'+id[1]+' div.listSub').removeClass('selectQues');
      }   
	});
}
/**
* 
* เมื่อคลิกที่คำถามหลักแสดงคำถามรอง
*
*/
function showSubQues(){
	$('div.listQues').click(function(){
		var id = $(this).attr('id').split('_');
		$('div#hSub_'+id[1]).toggle();
		
		if($('div#hSub_'+id[1]).attr('style')=='display: none;'){
			$('div#hSub_'+id[1]+' input[id^=id_sub_]').attr('checked','');
         $('div#hSub_'+id[1]+' div.listSub').removeClass('selectQues');
		}
	});
}
/**
* 
* เลือกคำถามหลักในแบบฟอร์ม CR เพื่อเพิ่มคำถามรอง
* @param no   ตำแหน่งที่เลือก
*/
function selectQues(no){
	if(!$('input#id_sub_'+no+':checked').val()){
		$('input#id_sub_'+no).attr('checked','checked');
	}else{
		$('input#id_sub_'+no).attr('checked','');
	}
}
/**
* 
* คลิกภายในกรอบก็สามารถเลือก checkbox ได้
*/
function clickFrame(){
	$('div.listSub').click(function(){
		var id = $(this).attr('id').split('_');
		id = id[1];
		
		$(this).toggleClass('selectQues');

		if(!$('input#id_sub_'+id+':checked').val()){
			$('input#id_sub_'+id).attr('checked','checked');
		}else{
			$('input#id_sub_'+id).attr('checked','');
		}
	});
}
/**
* 
* เพิ่มคำถามหลักในแบบฟอร์ม CR
* @param no   ตำแหน่งที่เลือก
*/
function plusQues(){
	var no,html = '';
	var types = $('input#types').val();
	var wWidthName = '';
	var wWIdthInput = '';

	no = parseInt($('input[id^=m_]:last').val()); //หาตัวรันเลขคำถามหลักตัวล่าสุด
	if(!no){ no = 0; }
	no = no+1;
	
	if(types=='A'){ 
		
		wWidthName = '5%';
		wWIdthInput = '5%';
		
	}else{
	
		wWidthName = '1%';
		wWIdthInput = '1%';
		
	}

	html += '<tr id="tr'+no+'">';
	html += '<td>';
	html += '<table id="tableSub_'+no+'" width="100%" border="0" cellpadding="3" cellspacing="0">';
	html += '<col width="4%">';
	html += '<col width="4%">';
	html += '<col width="">';
	html += '<col width="'+wWidthName+'">';
	html += '<col width="'+wWIdthInput+'">';
	html += '<col width="4%">';
	html += '<tbody>';
	html += '<tr>';
	html += '<td style="text-align: center; vertical-align: bottom;">';
	html += '<img src="img/delete.png" onclick="delQues(\'tr#tr'+no+'\',\'\',\'\');" style="cursor: pointer; margin-bottom: 5px;" border="0">';
	html += '<input type="hidden" id="m_'+no+'" name="m_[]" value="'+no+'"></td>';
	html += '<td style="vertical-align: bottom;">รหัส :<br/><input type="text" name="titleCode[]" id="titleCode_'+no+'" maxlength="10" style="width : 50px;" ></td>';
	html += '<td style="vertical-align: bottom;">หัวข้อ :<br/><input type="text" name="ques[]" id="ques_'+no+'" style="width: 98%"></td>';
	html += '<td style="vertical-align: middle;">';
	if(types=='A'){ html += 'น้ำหนัก'; }
	html += '</td>';
	html += '<td style="vertical-align: bottom;">';
	if(types=='A'){ html += '<input class="weight_title" type="text" name="weight[]" id="weight_'+no+'"  style="width: 50px" maxlength="10">'; }
	html += '</td>';
	html += '<td align="center" style="text-align: center; vertical-align: bottom;">';
	html += '<img src="img/add.png" border="0" onclick="plusSubQues('+no+');" style="cursor: pointer; margin-bottom: 4px;"></td>';
	html += '</tr>';
	html += '</tbody>';
	html += '</table>';
	html += '</td>';
	html += '</tr>';

	$('table#tableQues tbody:first').append(html);
}
/**
* 
* เพิ่มคำถามรองในคำถามหลัก ในแบบฟอร์ม CR
* @param idMain   ไอดีคำถามหลัก
*/
function plusSubQues(idMain){
	var no,idSub,inArray,i = 0;
	var runSame = 0; var runWeight = 0;
	var arraySub = [];
	var txt = ''; var rep = ''; var chkWeight = '';
	no = parseInt($('input[id^=s_]:last').val()); //หาตัวรันเลขคำถามรองตัวล่าสุด
	if(!no){ no = 0; }

	//วนหาไอดีคำถามรอง แล้วเก็บลงอาร์เรย เฉพาะในคำถามหลัก idMain
	$('table#tableSub_'+idMain+' input[id^=subId_]').each(function(){
		arraySub[i] = $(this).val();
		i++;
	});

	//วนหาไอดีลิสคำถามรองทางขวา ที่ได้เลือกไว้
	$('input[id^=id_sub_]:checked').each(function(){  
		idSub = $(this).val();
		txtList = $('div#sub_'+idSub).html().split('">&nbsp;');
		txtList = txtList[1]; //ข้อความคำถามรอง
		txt = txtList.split(' <font color="red">');
		txt = txt[0]; //ข้อความคำถามรองไม่เอาน้ำหนัก

     	inArray = $.inArray(idSub, arraySub); //เช็คว่ามีคำถามรองซ้ำในคำถามหลักหรือไม่
      
		//ถ้ามีคำถามรองนี้อยู่แล้ว
		if(inArray>-1){
			runSame++;
			rep += runSame+'. '+txt+'\n';
		//ยังไม่มีคำถามรองนี้
		}else{
			//ถ้าเป็นแบบ A
			if($('input#types').val()=='A'){
				//ถ้ายังไม่มีคำถามรอง
				if(!$('input#chk_weight').val()){
				$('input#chk_weight').val($(this).attr('weight'));

				no++;
				subHTML(no,idMain,idSub,txtList,$('input#types').val()); //เพิ่มคำถามรอง
				//ถ้ามีคำถามรองอยู่แล้ว
				}else{
				//เช็คว่าคำถามที่จะเพิ่ม มีน้ำหนักเท่ากันหรือไม่
				if($(this).attr('weight')!=$('input#chk_weight').val()){
				runWeight++;
				chkWeight += runWeight+'. '+txt+'\n';
				}else{
				no++;
				subHTML(no,idMain,idSub,txtList,$('input#types').val());
				}
				}
				//ถ้าเป็นแบบ B
			}else{
				no++;				
				subHTML(no,idMain,idSub,txtList,$('input#types').val());
			}
		}
		this.checked = false;
	});
	
	//แสดงข้อผิดพลาด
	$('div.selectQues').removeClass('selectQues');
	if(rep){ rep += 'ซ้ำในคำถามหลัก'; alert(rep); }
	if(chkWeight && !rep && $('input#types').val()=='A'){ chkWeight += 'น้ำหนักไม่ตรงกับคำถามอื่น'; alert(chkWeight); }
}
/**
* 
* html คำถามรองที่เพิ่ม
* @param no   ตัวแปรรันตัวเลข
* @param idMain   ไอดีคำถามหลัก
* @param idSub   ไอดีคำถามรอง
* @param txtList   ชื่อคำถามรอง
* @param types   ชนิดคำถาม มี A,B
*/
function subHTML(no,idMain,idSub,txtList,types){
	html = '<tr id="ts'+no+'">';
	html += '<td align="center"><input type="hidden" id="subId_'+no+'" name="subId['+idMain+'][]" value="'+idSub+'"></td>';
	html += '<td align="right"><img src="img/delete.png" onclick="delQues(\'tr#ts'+no+'\',\'\',\'\');" style="cursor: pointer;" border="0"><input type="hidden" id="s_'+idMain+'_'+no+'" name="s_['+idMain+'][]" value="'+no+'"></td>';
	html += '<td>รหัส: <input type="text" name="codeSub['+idMain+'][]" id="codeSub_'+idMain+'_'+no+'" style="width: 65px" maxlength="9" >'+txtList+'</td>';
	html += '<td align="center">';
	if(types == 'A'){ html += 'น้ำหนัก'; }
	html += '</td>';
	html += '<td>';
	if(types == 'A'){ html += '<input type="text" class="weight_question" name="weightSub['+idMain+'][]" id="weightSub_'+idMain+'_'+no+'" style="width: 40px" maxlength="10">'; }
	html += '</td>';
	//แสดง ออฟชั่นลิสบล้อค ถ้าเป็นชนิด B
	if($('input#types').val()=='B'){

		ops = $('input#subOp_'+idSub).val().split(',');
			
		html += '<td align="center">';
		html += '<select name="subOp['+idMain+'][]" id="subOp_'+no+'" style="width: 65px; overflow: hidden;">';
		i = 0; countOpt = ops.length;
		while(i<countOpt){
		html += '<option value="'+ops[i]+'">'+ops[i]+'</option>';
		i++;
		}
		html += '</select>';
		html += '</td>';
			
		}else{ html += '<td></td>'; }
	html += '</tr>';
						
	$('table#tableQues table#tableSub_'+idMain+' tbody').append(html);
}
/**
* 
* เช็คแบบฟอร์ม CR ก่อนบันทึกลงฐานข้อมูล
* @param  path   ไฟล์ที่จะส่งไปบันทึก
* @param  start   จะเริ่มใช้งานเลยหรือไม่ ส่ง s_start มา ถือว่าเริ่มใช้งานคือบันทึกเวลาเริ่มใช้เป็นปัจจบัน / ส่งs_save คือบันทึก วันเวลาตามที่ถูกกำหนด
*Last modify : pete 2/8/54
*/
function insertQues(path,start){
	var status = 'Y';
	var id,inArray,i = 0;
	var value = '';
	var quesName = [];
	var ws = 0;//pete 1/8/54 --> sumValue ส่งไป คำนวน
	var w = 0;
	var types = $('input#types').val();

	if(!$('input#ssi_title').val()){ $('input#ssi_title').focus(); $('input#ssi_title').addClass('errorInput'); return false; }else{ $('input#ssi_title').removeClass('errorInput'); } //เช็คการกรอกชื่อแบบฟอร์ม
	if($('font#chkName').html()!=''){ $('input#ssi_title').focus(); $('input#ssi_title').addClass('errorInput'); return false; }else{ $('input#ssi_title').removeClass('errorInput'); } //เช็คชื่อแบบฟอร์มว่าซ้ำหรือไม่
	if($('input[id^=m_]').length==0){ alert('ไม่มีคำถามหลัก'); return false; } //เช็คว่ามีคำถามหลักหรือไม่
	
	//วนลูปเช็คการกรอกข้อมูล คำถามหลัก
	var run = 1;
	$('input[id^=m_]').each(function(){
		idt = $(this).val();
		
		if(!$('input#ques_'+idt).val()){ 
			$('input#ques_'+idt).focus(); 
			$('input#ques_'+idt).addClass('errorInput'); 
			status = 'N'; 
			return false; 
		}else{ 
			$('input#ques_'+idt).removeClass('errorInput'); 
		}
		
		if(types=='A'){
			if(!$('input#weight_'+idt).val()) { 
				$('input#weight_'+idt).focus(); 
				$('input#weight_'+idt).addClass('errorInput'); 
				status = 'N'; 
				return false; 
			}else{
				//w += parseInt($('input#weight_'+idt).val()); 
				w += parseFloat($('input#weight_'+idt).val()); 
				$('input#weight_'+idt).removeClass('errorInput'); 
			}
		}
		
		//วนลูปเช็คการกรอกข้อมูล คำถามรอง เฉพาะแบบ A
		if(types=='A'){
			ws=0;
			$('input[id^=s_'+idt+'_]').each(function(){
				id = $(this).val();
				if(!$('input#codeSub_'+idt+'_'+id).val()) { 
					$('input#codeSub_'+idt+'_'+id).focus(); 
					$('input#codeSub_'+idt+'_'+id).addClass('errorInput');
					status = 'N'; return false; 
				}else{ 
					$('input#codeSub_'+idt+'_'+id).removeClass('errorInput');	
				}		
				if(!$('input#weightSub_'+idt+'_'+id).val()) {
					$('input#weightSub_'+idt+'_'+id).focus(); 
					$('input#weightSub_'+idt+'_'+id).addClass('errorInput'); status = 'N';
					return false; 
				}else{ 
					$('input#weightSub_'+idt+'_'+id).removeClass('errorInput');			
					//ws += parseInt($('input#weightSub_'+idt+'_'+id).val()); 
					ws += parseFloat($('input#weightSub_'+idt+'_'+id).val());
				}
			});//each
			//chkWeight_val(2,ws,idt,run);
			status = chkWeight_val(2,ws,idt,run);
		}
		if(status=='N'){ return false; }
		
	run++;
	});//each
	if(status=='N'){ return false; }
	
	
	if(types=='A'){ 
		status= chkWeight_val(1,w,null,null); 
	}
	if(status=='N'){ 
		return false; 
	}
	
	//เช็คว่าชื่อคำถามหลักซ้ำหรือไม่
	if(status=='Y'){
		$('input.errorInput').removeClass('errorInput');
		$('input[id^=ques_]').each(function(){
			inArray = $.inArray($(this).val(), quesName);
			if(inArray>-1){
				inArray = inArray+1;
				$('input#ques_'+inArray).addClass('errorInput');
				$(this).addClass('errorInput');
				status = 'N';
			}else{
				quesName[i] = $(this).val();
			}
			i++;
		});
    }

	if(status=='N'){ return false; }
	
	var d_start = $('input#ssi_startDate').val();
	var d_stop 	= $('input#ssi_endDate').val();
	if(d_start == ''){d_start = '00-00-0000';}
	if(d_stop == ''){d_stop = '00-00-0000';}
	value = '&func=insert_question';
	if(start=='s_start'){//ถ้าคลิกเริ่มใช้งาน
		value += '&date=start'; value += '&datestop='+d_stop;
	}
	else if(start=='s_save'){//คลิก บันทึก
		value += '&date='+d_start; value += '&datestop='+d_stop;
	}
	
	if(status=='Y'){	
		$.post(path,$('div#leftQuestion').find('input,select').serialize()+''+value,function(data){
			$('#main').html(data);
		});
	}
}
/**
* 
* ลบคำถามในแบบฟอร์ม CR
* @param  tag   แท้กที่ต้องการลบ
*/
function delQues(tag,id,type){
	if(tag){
		if(confirm('ยืนยันการลบ')){
			if(id){
				if(tag){
      			$.post('ssi_setup_form.php', 'func=delQues&id='+id+'&type='+type, function(res){ if(res){ $(tag).remove(); } });
      		}
  			}else{
				$(tag).remove();
   		}
   	}
   }else{
   	$.post('ssi_setup_form.php', 'func=delQues&id='+id+'&type='+type, function(res){ });
   }
}
/**
* 
* ยกเลิกฟอร์มแบบสัมภาษณ์
* @param  path   ไฟล์ที่จะส่งไป
* @param  val   ค่าที่จะส่งไป
* @param  tag   แท็กที่ต้องการลบ
*/
function cancelFrom(path,val,tag){
	$.post(path, val, function(res){
		if(res){
			$(tag).remove();
		}
	});
}
/**
* 
* คลิกแก้ไขฟอร์มแบบสัมภาษณ์
* @param  id   ไอดีที่ส่งมา
*/
function editSSI(path,val){
	browseCheck('main',path+'?func=createFrom'+val,'');
}
/**
* 
* เลือกอีเวนต์ของฟอร์มแบบสัมภาษณ์ที่ต้องการแก้ไข
*/
function selectEvent(){
    $('div#hQues_'+$('input#event_id').val()).attr('style','display: block;');
}


/**
* 
* เชคน้ำหนัก ให้ได้ 100 ใน OBJ แต่ละก่อน
* @param  no   เอาไว้เชคว่าเป็น นน รวม ของ title หรือ Sub
* @param  val   ค่ารวมน้ำหนัก
* @param  title_no  เลขไอดีคำถามหลัก
* pete 2/8/54
*/
function chkWeight_val(no,val,title_no,run){
	switch(no){
		case 1: {	
		//if(val!=''){		
			if(val!=0){
				if(val != 100){
					if(val>100) {alert('หัวข้อหลัก :: น้ำหนักรวม('+val+') เกิน 100');}
					else {alert('หัวข้อหลัก ::  น้ำหนักรวม('+val+') ไม่ถึง 100');}
					return "N";
				}else{	//กรณีกรอกสมบูรณ์		
					return "Y";	
				}//end if
			}else {
				alert('กรุณากรอกน้ำหนัก Title');
				return "N";
			}//end if
			//}else {return "N"}
		}//end case 1
		case 2: {
			//if(val!=''){// ถ้าไมได้เพิ่มคำถามย่อย ไม่เข้าเงื่อนไข
			if(title_no){
				if(val!=0){
					if(val != 100){
						if(val>100) {
							alert('- น้ำหนักรวมของห้วข้อย่อย('+val+') เกิน 100 ในหัวข้อหลักข้อที่ '+run +' :: '+ $('#ques_'+title_no).val());
						} else {
							alert('- น้ำหนักรวมของห้วข้อย่อย('+val+') ไม่ถึง 100 ในหัวข้อหลักข้อที่ '+ run +' :: '+ $('#ques_'+title_no).val());
						}
						return "N";
					}else{	//กรณีกรอกสมบูรณ์	
						return "Y";	
					}//end if
				}else {
					alert('กรุณากรอกน้ำหนักของคำถามย่อย');
					return "N";
				}//end if
			}else {
				return "Y";
			}
		}//end case2
	}//end switch
}// end function chkWeight_val()


// Description: หน้าการจัดการแบบฟอร์ม CR สำหรับคำนวณคะแนนควาพึงพอใจ SSI -
// ssi_setup_form
function jDate(datepicker){
	$(datepicker).datepicker({
		dateFormat : 'dd-mm-yy',
		changeMonth: true,changeYear: true
	});
}

function activeForm(path, val, type) {
	$.post(path, val);
	if (type == 'all') {
		browseCheck('listData', 'ssi_setup_form.php?types=all', '');
	}else {
		browseCheck('main', 'ssi_setup_form.php', '');
	}
}

function chkUpload_regis(){
	if(confirm('ยืนยันการอัพโหลดข้อมูล')){
		if(!jQuery('input#fileUpload').val()){ alert('กรุณาเลือกไฟล์'); jQuery('input#fileUpload').focus(); return false; }
		//if(!jQuery('input#empID').val()){ alert('กรุณากรอกบัตรพนักงาน'); jQuery('input#empID').focus(); return false; }
		
		uploadFile_progress('uploadRegis');
	}else{
		return false;
	}
}

function uploadFile_progress(name){
	if(name == 'uploadRegis'){
		//	create form to fly
		//var tag_pass 	=  '#ReportsellpartsShow';
		//jQuery(tag_pass).html('');
		
		// jQuery('div#showFrom').css('display','none');
		
		// create status iframe
		//var statusFrame = new Element('iframe', { id: 'statusFrame', name: 'statusFrame' }).hide();
		//var  statusFrame = jQuery('<iframe>').attr({'id':'statusFrame','name':'statusFrame'}).hide();
		/****************************************************
		*							create HTML progress
		*
		*****************************************************/
		var getHtml_dx = '<div id="message_st">โปรดรอสักครู่...</div>';
		getHtml_dx += '<div id="bar_out"><div id="bar_in"><div id="txt_per_in">100%</div></div></div><div id="txt_up">process..</div>';
		/****************************************************/
		var show_div = jQuery('<div>').attr({id:'status_ex'	}).css('margin-top','0px').html(getHtml_dx);

		//jQuery('td#showIfm').append(statusFrame);
		//jQuery('iframe#statusFrame').append(show_div);
		//jQuery('form#frmSearch').css('display','none');
		jQuery('div#showProgress').html(show_div);
		jQuery('div#showProgress').css('display','block');
		//--
		return;
	}
}
//endfunction

// BEGIN ssi report
function select_SSI_type(obj) {
	document.getElementById('dv_selectYear').innerHTML = ' ';
	document.getElementById('dv_selectSpec').innerHTML = ' ';
	document.getElementById('dv_selectMonth').innerHTML = ' ';
	document.getElementById('dv_selectEmp4Cus').innerHTML = ' ';
	document.getElementById('dv_report_details').innerHTML = ' ';
	
	var path;
	switch (obj){
	case 'tab_dealer':
		path = "ssi_report.php?func=typeReport&type=dealer";
		break;
	case 'tab_branch':
		path = "ssi_report.php?func=typeReport&type=branch";
		break;
	case 'tab_emp':
		path = "ssi_report.php?func=typeReport&type=emp";
		break;
	case 'tab_emp_cus':
		path = "ssi_report.php?func=typeReport&type=cus";
		break;
	}
	if (obj != 'empty'){
		browseCheck('dv_selectYear', path, '');
	}
}

function reportSSI_yearSelect(reportType) { //พอเลือกปีแล้ว ก็จะเข้าฟังก์ชั่นนี้

	document.getElementById('dv_selectSpec').innerHTML = ' ';
	document.getElementById('dv_selectMonth').innerHTML = ' ';
	document.getElementById('dv_selectEmp4Cus').innerHTML = ' ';
	document.getElementById('dv_report_details').innerHTML = ' ';
	
	year = $('#select_year').val();//ต้องส่งปีไปด้วย เพราะจะเอาไปคัดรายการแสดงในปีนั้นๆต่อ
	browseCheck('dv_selectSpec', 'ssi_report.php?func=specific&specific='+ reportType +'&year=' + year, '');
}

function reportSSI_specificSelect(reportType) { // พอเลือกรายการเฉพาะปุ๊บ

	var year = $('#select_year').val();
	var param = "&year=" + year ;
	var div = '';
	var path = '';
	var specfigValue = '';
	switch (reportType){
	case 'dealer':
		specfigValue = $('#dealerOnSell').val();
		div = 'dv_report_details';
		path = "ssi_report.php?func=viewDealer&view_dealer=" + specfigValue + "" + param;
		document.getElementById('dv_selectEmp4Cus').innerHTML = ' ';
		break;
	case 'branch':
		specfigValue = $('#branchOnSell').val();
		div = 'dv_report_details';
		path = "ssi_report.php?func=viewBranch&view_branch=" + specfigValue + "" + param;
		document.getElementById('dv_selectEmp4Cus').innerHTML = ' ';
		break;
	case 'emp':
		specfigValue = $('#EmpOnSell').val();
		div = 'dv_report_details';
		path = "ssi_report.php?func=viewEmp&view_emp=" + specfigValue + "" + param;
		document.getElementById('dv_selectEmp4Cus').innerHTML = ' ';
		break;
	case 'cus':
		specfigValue = $('#select_month').val();
		div = 'dv_report_details';
		path = "ssi_report.php?func=viewCus&view_cus=%&month=" + specfigValue + "" + param;
		browseCheck( 'dv_selectEmp4Cus' , "ssi_report.php?func=specific&specific=emp4cus&month=" + specfigValue + "" + param , ''); // พิเศษหว่าที่สามารถให้เลือกเดือนก่อนแล้วเลือกพนักงานได้อีกชั้น
		break;
	case 'emp4cus':
		specfigValue1 = $('#EmpOnSell').val();
		specfigValue2 = $('#select_month').val();
		div = 'dv_report_details';
		path = "ssi_report.php?func=viewCus&view_cus=" + specfigValue1 + "" + param + "&month=" + specfigValue2;
		break;	
		
	}//switch
	browseCheck( div , path , '');
}
// END ssi report

// BEGIN SQ FORM
function addIdCode(obj){
	maxCode = parseInt($(obj).val())+1;
	var url = 'manage_SQ_form.php?func=maxCode&maxCode='+maxCode;
	var param = '';
	$.get(url, param, function(result) {
		$('#tbNextCode'+maxCode).append(result);
	});
	$(obj).val(maxCode);
}

function save_allIdCode(){
	maxCode = parseInt($('#max_idCode').val());
	var param ='func=resultCode&arrResultCode='+maxCode;
	var temps ='';
	var status = true; //ใช้เชคว่ารายการย่อยๆ ถูกบันทึกค่าไว้หมดรึยัง  จะได้เอาไปลง ตารางรวดเดียว
	for(i = 0 ; i < maxCode ; i++){ // ดึงค่าที่ถูกตั้งค่าต่างๆมา เข้าตาราง
		num=i+1;
		var temps = $('#result_thisCode_'+ num).val();
		if(temps !='' && temps != null){
		temps = '&var'+i+'='+temps;
		param += temps;
		}else{
			status = false;
		}
	}
	if(status != false){
		$.post('manage_SQ_form.php?',param,function(data){
			if(data == '1'){
				alert(' ตั้งค่าสำเร็จ ');
				//$('#main').html(data);
				browseCheck('main', 'manage_SQ_form.php?func=tab2', '');
			}else{
				alert('การตั้งค่าล้มเหลว :'+data);
			}
		});
	}else{
		alert('กรุณาบันทึกการตั้งค่าย่อยให้เรียบร้อยก่อนครับ');
	}
}

function selectQevent(thisCode){
	idEvent = $("#dv_Qevent"+thisCode).val();
	browseCheck('dv_Mevent'+thisCode, 'manage_SQ_form.php?func=Qevent&Qevent='+idEvent+'&maxCode='+thisCode, '');
	//alert(idEvent);
}

function selectMevent(thisCode){
	idMain = $("#se_Mevent"+thisCode).val();
	browseCheck('dv_Levent'+thisCode, 'manage_SQ_form.php?func=Mevent&Mevent='+idMain+'&maxCode='+thisCode, '');
}

function selectLevent(thisCode){
	idList = $("#se_Levent"+thisCode).val();
	$("#dv_saveFixCode"+thisCode).html('<img  src="img/1281150751_disk.png" style="cursor: pointer;" title="บันทึก" onclick="save_ListFixCode(\''+thisCode+'\',\''+idList+'\'); ">');
	//browseCheck('dv_Levent'+thisCode, 'manage_SQ_form.php?Levent='+idList+'&maxCode='+thisCode, '');
}

function save_ListFixCode(thisCode,idList){ // presave จะเก็บ ค่า ที่ได้ ไว้ที่ result_thisCode_ เพื่อรอบันทึกเข้า DB
	input_code = $("#inp_"+thisCode).val();
	idList     = $("#se_Levent"+thisCode).val();
	if(input_code == '' || input_code == null){
		$("#inp_"+thisCode).focus();
		alert('กรุณากรอกรหัสให้เรียบร้อยก่อนบันทึกครับ');
	}else if(idList == '' || input_code == null){
		alert('กรุณาเลือกแหล่งที่มาให้เรียบร้อยก่อนบันทึกครับ');
	}else{
		preSave = input_code+'='+idList;
		maxCode = parseInt($('#max_idCode').val());
		for(i = 0 ; i < maxCode ; i++){ //เป็นการเชคการบันทึกรหัสซ้ำเดิม
			num=i+1;
			var a = $('#result_thisCode_'+ num).val();
			if(preSave == a){
				alert('มีการตั้งค่ารหัสกับคำถามเดียวกันนี้ไปแล้วครับ');
				return false;
			}
		}
		$("#result_thisCode_"+thisCode).val(preSave);
		$("#inp_"+thisCode).attr('readonly',true);
		$("#se_Levent"+thisCode).attr("disabled", true);
		$("#se_Mevent"+thisCode).attr("disabled", true);
		$("#dv_Qevent"+thisCode).attr("disabled", true);
		$("#dv_saveFixCode"+thisCode).html('<img  src="img/edit2.png" style="cursor: pointer;" title="แก้ไข" onclick="edit_ListFixCode(\''+thisCode+'\');">  <img  src="img/delete.png" style="cursor: pointer;" title="ลบ" onclick="if(confirm(\'ท่านต้องการลบ '+input_code+' !?\')){del_ListFixCode(\''+thisCode+'\')};">');
	}
}

function edit_ListFixCode(thisCode,idList){ // แก้ไข presave จะเก็บ ค่า ที่ได้ ไว้ที่ result_thisCode_ เพื่อรอบันทึกเข้า DB
	$("#result_thisCode_"+thisCode).val('');
	$("#inp_"+thisCode).removeAttr('readonly');
	$("#se_Levent"+thisCode).removeAttr("disabled");
	$("#se_Mevent"+thisCode).removeAttr("disabled");
	$("#dv_Qevent"+thisCode).removeAttr("disabled");
	$("#dv_saveFixCode"+thisCode).html('<img  src="img/1281150751_disk.png" style="cursor: pointer;" title="บันทึก" onclick="save_ListFixCode(\''+thisCode+'\',\''+idList+'\'); ">');
}

function del_ListFixCode(thisCode){	//thisCode คือลำดับ ของรหัส
	browseCheck('main_mode', 'manage_SQ_form.php?del_ListFixCode='+thisCode, '');
}

function changeKrong4DN(){
	var b_id = $('#select_branch').val();
	var start_d = $('#start_dDate').val();
	var stop_d = $('#stop_dDate').val();
	if(start_d != ''){
		expstrt = start_d.split("-");
		start_d = expstrt[2]+'-'+expstrt[1]+'-'+expstrt[0];
	}
	if(start_d != ''){
		expstop = stop_d.split("-");
		stop_d = expstop[2]+'-'+expstop[1]+'-'+expstop[0];
	}
	browseCheck('dv_manage_main', 'manage_SQ_form.php?func=activeTab&select_branch='+b_id+'&start_dDate='+start_d+'&stop_dDate='+stop_d, '');
}

function refresh_SQ_point(cusno,VinNo){
	if(confirm('ยืนยันการเรียกคืนค่าปริยายของ Vin no. '+VinNo)==true){
	testtext='';
	param = '&cusno='+cusno+'&VinNo='+VinNo;
	 $.post('manage_SQ_form.php?func=refreshSQ',param,function(data){
		 var aaaa = data.split(",");
		 	for(i=0;i<aaaa.length;i++){	
		 		 var bbbb = aaaa[i].split("=");
		 		//อัพเดทเลขในหน้าโปรแกรม ของDN ที่ กด refresh
		 		newPoint = bbbb[1];
		 		$('#td_'+VinNo+'_'+i).text(newPoint);// เซตค่าใหม่เพื่อแสดงในช่อง
				$('#'+VinNo+'_'+i).val(newPoint); // เซตค่าใหม่ ลงinput ที่ซ่อนไว้
		 	}
		});
	}else{return false;}
}

//pt@2011-12-21 เพิ่ม serialData ให้เลือกโหลดไฟล์ เป็น record ได้
function download_SQ_from(idRef_Vins){
	serialData = $("#form_listDN").serialize();
	var formdata = decodeURIComponent(serialData);
	param = '&idRef_Vins='+idRef_Vins+'&'+formdata;	/*$.post('manage_SQ_form.php?download_SQ_from=1',param,function(data){	});	*/
	window.open('manage_SQ_form.php?func=downloadSQ'+param,'newWin1','height=50,width=50,top=0,left=0,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes,directories=no,status=no,titlebar=no');
	changeKrong4DN();
}

//pt@2011-12-21 
function chk_all_DN(type){ // type btnall=ปุ่มเลือกทั้งหมด , btnone=ปุ่มเลือกรายrecord
	switch(type){
	case 'btnall' : 
		if(!$('input[name=ALLDNID]').is(':checked')){  // ถ้าเช็ค All 
			$('input[id^=DNID]').each(function(){
				 this.checked = false; 
			});
		 }else{
			 $('input[id^=DNID]').each(function(){
				 this.checked = true; 		
			});
		 }
		break;
		
	case 'btnone' : 
		statusX = 'checkAll';
			$('input[id^=DNID]').each(function(){
				 if(this.checked == false){
					 statusX = 'Ultraman';
				 } 
			});
			if(statusX == 'checkAll'){
				 $('input[name=ALLDNID]').attr('checked','checked');
			}else{
				 $('input[name=ALLDNID]').attr('checked','');
			}
		break;
	}
}

function BTNresetDatedelivery(){
	$('#start_dDate').val('');
	$('#stop_dDate').val('');
	//var b_id = $('#select_branch').val();
	//start_d = stop_d = '';
	//browseCheck('dv_manage_main', 'manage_SQ_form.php?active_tab2_change=1&select_branch='+b_id, '');
}
// END SQ FORM

// BEGIN RESPERSON CHANGE
function show_hide_search(param){
	if(param=="search_sale"){
		$('#search').val('search_sale');
		$('#search_cus').hide().find('input,select').attr('disabled',true);
		$('#search_sale').show().find('input,select').attr('disabled',false);
	}else if(param=="search_cus"){
		$('#search').val('search_cus');
		$('#search_sale').hide().find('input,select').attr('disabled',true);
		$('#search_cus').show().find('input,select').attr('disabled',false);
	}
}

function editRawPoint(obj,inputType){
	rawPoint = $('#'+obj).val();
	jQuery.fn.exists = function(){return jQuery(this).length>0;} ;// pt@2011-11-05 ประกาศฟังก์ชั่น เชคว่าTAGนั้นมีอยู่ไหม
	if($('#inp_'+obj).exists() ){
		// ถ้าเป็นการคลิกซ้ำ ก็ให้ข้ามไปไม่ทำไร
	}else{
		$('#td_'+obj).removeAttr("onclick");
		$('#td_'+obj).html('<input id="inp_'+obj+'" class ="textCenter" style="width:48px" value="'+rawPoint+'"><img  src="img/1281150751_disk.png" style="cursor: pointer;" title="บันทึก" onclick="save_rawPoint(\''+obj+'\',\''+inputType+'\'); ">');//pt2011-11-19
	}
	$('#inp_'+obj).focus();
}

function save_rawPoint(obj,inputType){ //////
	var b_id = $('#select_branch').val();
	var start_d = $('#start_dDate').val();
	var stop_d = $('#stop_dDate').val();
	var newPoint = $('#inp_'+obj).val();
	param = '&newPoint='+newPoint+'&VinNo_Position='+obj+'&select_branch='+b_id+'&start_dDate='+start_d+'&stop_dDate='+stop_d;
	
	switch(inputType){//pt2011-11-19
	case 'type_1':
		if(newPoint==0||newPoint==1||newPoint==''||newPoint==' '||newPoint=='   '){
		$.post('manage_SQ_form.php?func=activeTab&save_newPoint=1',param,function(data){
			$('#tdtd_sum_Q').html(data); 
			$('#td_'+obj).html(''); 
			$('#td_'+obj).text(newPoint);// เซตค่าใหม่เพื่อแสดงในช่อง
			$('#'+obj).val(newPoint); // เซตค่าใหม่ ลงinput ที่ซ่อนไว้
			var fn = "editRawPoint('"+obj+"','"+inputType+"')";
			var newclick = new Function(fn);
			$('#td_'+obj).attr('onclick','').click(newclick);
		});
		}else{
			alert('กรอกคะแนนผิดพลาด \n*กำหนดการรับค่าเพียง 0,1 และค่าว่างเท่านั้น');
			return false;
		}
		break;
	case 'type_10':
		if(newPoint>=0 && newPoint<=10){
		$.post('manage_SQ_form.php?func=activeTab&save_newPoint=1',param,function(data){
			$('#tdtd_sum_Q').html(data); 
			$('#td_'+obj).html(''); 
			$('#td_'+obj).text(newPoint);// เซตค่าใหม่เพื่อแสดงในช่อง
			$('#'+obj).val(newPoint); // เซตค่าใหม่ ลงinput ที่ซ่อนไว้
			var fn = "editRawPoint('"+obj+"','"+inputType+"')";
			var newclick = new Function(fn);
			$('#td_'+obj).attr('onclick','').click(newclick);
		});
		}else{
			alert('กรอกคะแนนผิดพลาด \n*กำหนดการรับค่าเพียง 0-10เท่านั้น');
			return false;
		}
		break;
	}
}

function editAVG_SQ(code,col_position,ovalue,inputType){ // เมื่อกดแก้ไข ค่าเฉลี่ย //modify pt2011-11-19
	jQuery.fn.exists = function(){return jQuery(this).length>0;} ;// pt@2011-11-05 ประกาศฟังก์ชั่น เชคว่าTAGนั้นมีอยู่ไหม
	if($('#inp_avg'+code).exists() ){
		// ถ้าเป็นการคลิกซ้ำ ก็ให้ข้ามไปไม่ทำไร
	}else{
	$("#td_sum_"+code).removeAttr("onclick");
		$("#td_sum_"+code).html('<input id="inp_avg'+code+'" class ="textCenter" style="width:48px" value="'+ovalue+'"><img  src="img/1281150751_disk.png" style="cursor: pointer;" title="บันทึก" onclick="optimizeAverageCalculator(\''+code+'\',\''+col_position+'\',\''+inputType+'\'); ">');//pt2011-11-19
	}
	$('#inp_avg'+code).focus();
}

function optimizeAverageCalculator(code,position,inputType){ // OAC
	var target_avg 		= $('#inp_avg'+code).val(); // ค่าเฉลี่ยนที่ตั้งใหม่
	var list_DN 		= $('#collectDN4avgOptimize').val();
	var list_Vin 		= list_DN; //IT10 ไม่ใช้ DN ตอนนี้แก้ไขข้อมูลเป็น Vin แล้ว
	var VinNo 			= list_Vin.split(",");
	var arr_rawValue 	= new Array();
	var lengthDN 		= VinNo.length;
	if(lengthDN == 0 || list_Vin == ''){alert('ไม่พบรายการที่สามารถแก้ไขได้ครับ'); return false;}

	switch(inputType){//pt2011-11-19
	case 'type_1':
		if(target_avg>=0&&target_avg<=1||target_avg==''||target_avg==' '||target_avg=='   '){
		
		for(i=0;i<lengthDN;i++){ // ดึงเอาคะแนน ของแต่ละDN ของคำถามนี้ ไว้ในarr_rawValue
			arr_rawValue[i] = $('#'+DN_No[i]+'_'+position).val();
		}
		var cp_recep 	 	= new Array();
		var arr_newValue 	= new Array();
		arr_newValue 		= arr_rawValue;
		saveHANG 			= 0;
		cp_recep 			= compare_OAV_value(arr_rawValue,target_avg,inputType);
		status   			= cp_recep[1];
		
		while(status == 'NOT_OK'){ // ถ้ายังไม่โอเค ให้เข้าไปปรับค่าใหม่ จนกว่าจะโอเค 555 ระวัง ไม่ยอมออกloop ! *เซตsaveHANG ไว้1001รอบ;100DN*คะแนนเต็ม10 = 1000รอบ
			if(saveHANG > 1000){ alert('กรอกข้อมูลไม่ถูกต้อง! Please check your input!! '); return false;}	saveHANG++;
			var flag ='Y';
			var maximum = ''; // find Max value that is not equal 10
			for (j=0; j<arr_newValue.length; j++){
		        if (arr_newValue[j]==0) {
		            maximum = 0;   // new maximum
		        }
		    }
			for(i=0;i<lengthDN;i++){
					var val = arr_newValue[i];
					if(flag == 'Y'){//การทำงานคือ ถ้า มีการบวกเพียงครั้งเดียว จะไม่ทำการบวดกเลขอื่นอีก แล้วนำค่าไปเชคว่าถึงค่าเฉลี่ยเป้าหมายรึยัง ถ้ายัง ก็บวกไปอีก1 การบวกจะเริ่มบวกจากค่าที่=9 ไล่ลงไป
						/*if(val == 0 ||val == 1 ||val == 2 ||val == 3 ||val == 4 ||val == 5 ){ //ถ้าคะแนนต่ำมากๆ ปัดให้เป็น 10
							val = 10;	flag = 'N';
						}else */
						if(val == maximum){// เพิ่มค่า ทีละ1 เริ่มจากค่าสูงสุด=9
							val++;	flag = 'N';
						}
					}
					arr_newValue[i] = val;
			}
			//หลังจาก+เพิ่มไป1 ก็ส่งเข้าไปเช็คหาค่าเฉลี่ยว่า OK รึยัง
			cp_recep 		= compare_OAV_value(arr_newValue,target_avg,inputType);
			arr_newValue 	= cp_recep[0];
			status   		= cp_recep[1];
		}
		//ส่งค่าใหม่ไปอัพเดท
		var b_id = $('#select_branch').val();
		var start_d = $('#start_dDate').val();
		var stop_d = $('#stop_dDate').val();
		param = '&list_Vin='+list_Vin+'&position='+position+'&arr_newValue='+arr_newValue+'&select_branch='+b_id+'&start_dDate='+start_d+'&stop_dDate='+stop_d;
		  $.post('manage_SQ_form.php?func=activeTab&save_newAVG=1',param,function(data){
			$('#tdtd_sum_Q').html(data); 
			for(i=0;i<lengthDN;i++){ // วนแสดงค่าใหม่ ลง ไป ใน คอลัม  position
				$('#'+VinNo[i]+'_'+position).val(arr_newValue[i]);
				$('#td_'+VinNo[i]+'_'+position).text(arr_newValue[i]); //แสดงค่าใหม่
			}
		});
		}else{
				alert('กรอกคะแนนผิดพลาด \n*กำหนดการรับค่าเพียง 0,1 และค่าว่างเท่านั้น');
				return false;
		}
		break;
		
		
	case 'type_10':
		if(target_avg>=0 && target_avg<=10){

			for(i=0;i<lengthDN;i++){ // ดึงเอาคะแนน ของแต่ละDN ของคำถามนี้ ไว้ในarr_rawValue
				arr_rawValue[i] = $('#'+VinNo[i]+'_'+position).val();
			}
			var cp_recep 	 	= new Array();
			var arr_newValue 	= new Array();
			arr_newValue 		= arr_rawValue;
			saveHANG 			= 0;
			cp_recep 			= compare_OAV_value(arr_rawValue,target_avg,inputType);
			status   			= cp_recep[1];
			
			while(status == 'NOT_OK'){ // ถ้ายังไม่โอเค ให้เข้าไปปรับค่าใหม่ จนกว่าจะโอเค 555 ระวัง ไม่ยอมออกloop ! *เซตsaveHANG ไว้1001รอบ;100DN*คะแนนเต็ม10 = 1000รอบ
				if(saveHANG > 1000){ alert('Please check your input!! RETURN FALSE!'); return false;}	saveHANG++;
				var flag ='Y';
				var maximum = 0; // find Max value that is not equal 10
				for (j=0; j<arr_newValue.length; j++){
			        if (arr_newValue[j] > maximum && arr_newValue[j]!= 10) {
			            maximum = parseInt(arr_newValue[j]);   // new maximum
			        }
			    }
				for(i=0;i<lengthDN;i++){
					var val = arr_newValue[i];
					if(flag == 'Y'){//การทำงานคือ ถ้า มีการบวกเพียงครั้งเดียว จะไม่ทำการบวดกเลขอื่นอีก แล้วนำค่าไปเชคว่าถึงค่าเฉลี่ยเป้าหมายรึยัง ถ้ายัง ก็บวกไปอีก1 การบวกจะเริ่มบวกจากค่าที่=9 ไล่ลงไป
						/*if(val == 0 ||val == 1 ||val == 2 ||val == 3 ||val == 4 ||val == 5 ){ //ถ้าคะแนนต่ำมากๆ ปัดให้เป็น 10
							val = 10;	flag = 'N';
						}else */
						if(val == maximum){// เพิ่มค่า ทีละ1 เริ่มจากค่าสูงสุด=9
							val++;	flag = 'N';
						}
					}
					arr_newValue[i] = val;
				}
				//หลังจาก+เพิ่มไป1 ก็ส่งเข้าไปเช็คหาค่าเฉลี่ยว่า OK รึยัง
				cp_recep 		= compare_OAV_value(arr_newValue,target_avg,inputType);
				arr_newValue 	= cp_recep[0];
				status   		= cp_recep[1];
			}
			//ส่งค่าใหม่ไปอัพเดท
			var b_id = $('#select_branch').val();
			var start_d = $('#start_dDate').val();
			var stop_d = $('#stop_dDate').val();
			param = '&list_Vin='+list_Vin+'&position='+position+'&arr_newValue='+arr_newValue+'&select_branch='+b_id+'&start_dDate='+start_d+'&stop_dDate='+stop_d;
			  $.post('manage_SQ_form.php?func=activeTab&save_newAVG=1',param,function(data){
				$('#tdtd_sum_Q').html(data); 
				for(i=0;i<lengthDN;i++){ // วนแสดงค่าใหม่ ลง ไป ใน คอลัม  position
					$('#'+VinNo[i]+'_'+position).val(arr_newValue[i]);
					$('#td_'+VinNo[i]+'_'+position).text(arr_newValue[i]); //แสดงค่าใหม่
				}
			});
		}else{
			alert('กรอกคะแนนผิดพลาด \n*กำหนดการรับค่าเพียง 0-10เท่านั้น');
			return false;
		}
		break;
	}
}

function compare_OAV_value(arr_seachVal,targetAVG,inputType){ // ใช้คำนวนว่า ค่าคะแนนใหม่เฉลียกันได้มากกว่าค่าเฉลี่ยเป้าหมายรึยัง
	var cp_return 		= new Array();
	var countL = arr_seachVal.length;
	var sum=0;
	
	if(inputType =='type_1' && targetAVG==''){ //กรณีเซตค่าเฉลี่ย เป็นค่าว่าง และเป็นชนิด1 //pt2011-11-19
		for(i=0;i<countL;i++){
			arr_seachVal[i] = '';
		}
		cp_return[0] = arr_seachVal;
		cp_return[1] = 'OK';
		cp_return[2] = 0;
		
	}else if(inputType =='type_1' && targetAVG==0){ //pt2011-11-19
		for(i=0;i<countL;i++){
			arr_seachVal[i] = 0;
		}
		cp_return[0] = arr_seachVal;
		cp_return[1] = 'OK';
		cp_return[2] = 0;
	}else{
		for(i=0;i<countL;i++){
			sum = sum+parseInt(arr_seachVal[i]);
		}
		thisAVG = sum/countL;
		if(thisAVG >= targetAVG){//		alert('มากกกว่า '+thisAVG +' >= '+ targetAVG);
			cp_return[0] = arr_seachVal;
			cp_return[1] = 'OK';
			cp_return[2] = thisAVG;
		}else{//		alert('ยังน้อยยกว่า '+thisAVG +' < '+ targetAVG);
			cp_return[0] = arr_seachVal;
			cp_return[1] = 'NOT_OK';
			cp_return[2] = thisAVG;
		}
	}
	return cp_return;
}