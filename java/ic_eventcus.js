function margtext(){
	var cusname=$('#cusname').val();
	var idcus=$('#cusid_hidden').val();
	if(idcus!=''){
		$('#cusname').val(idcus+'_'+cusname);
	}
}

function clearbox(boxid){
	$(boxid).val("");
}

function addevent2DB(url){
	var cusname	=$('#cusname').val();
	var event	=$('#event').val();
	var details	=$('#eventdetail').val();
	var cusid	=$('#cusid_hidden').val().split('_');
	var smonth	=$('#smonth').val();
	var syear	=$('#syear').val();
	var idEvent =$('#idEvent').val();
	
	if((cusname=='') || (event=='') || (details=='')){
		alert('กรอกข้อมูลยังไม่ครบ');
	}else{
		if(cusid[0] == ""){
			alert('ชื่อลูกค้าไม่มีในระบบ');
		}else{
			$.post(url,{cusname:cusname,event:event,eventdetail:details,add2db:"add2db",smonth:smonth,syear:syear,idEvent:idEvent},
			function(data){
				//$('#addbox').fadeOut(1000);
				$('#main').html(data);
			});
		}
	}
}

function searchEvent(){
	var Sname 	=$('#name').val();
	var Syear	=$('#years').val();
	var Smonth	=$('#month').val();
	browseCheck('tbBirthday','ic_followcus_show.php?func=ic_eventcus&search=search&Sname='+Sname+'&Syear='+Syear+'&Smonth='+Smonth);
}
/// Add BY Bird เพื่อให้ค้นหาไปหน้า กรองตามสาขาได้ /////////////////
function searchEvent_special(){
	var Sname 	=$('#name').val();
	var Syear	=$('#years').val();
	var Smonth	=$('#month').val();
	browseCheck('tbBirthday','ic_followcus_show_special.php?func=ic_eventcus&search=search&Sname='+Sname+'&Syear='+Syear+'&Smonth='+Smonth);
}
/// END Add BY Bird เพื่อให้ค้นหาไปหน้า กรองตามสาขาได้ /////////////////
function deleteevent(url,eventid){
	if(confirm('ต้องการลบหรือไม่')==true){
	//$.get('ic_eventcus.php',{delete:eventid},function(data){
	$.get(url,{'delete':eventid},function(data){
			$('#main').html(data);
			alert('การลบเสร็จสมบูรณ์');
		});
	}
}