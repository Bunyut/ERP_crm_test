jQuery('div#popupCRM').show('slow');

function browseCheckNew(jshow, target, param, callback){
	var opt;
	var selectors = "#"+jshow;
	jQuery(selectors).empty();
	var strLoadingText = "<div style='left: 50%; margin-left: -33px; margin-top: -88px; position: absolute; text-align: center; top: 50%;' id='pre_loading'><img src='images/lightbox-ico-loading.gif'  style='padding-left:auto;padding-right:auto;'/><br /><span style='font-size: 12px; font-weight: bold; color: #000;'>Loading ...</span></div>"
	jQuery(selectors).html(strLoadingText);
	jQuery.ajax({
		url: target,
		context:document.body,
		data:param,
		success: function(resultHtml){	
			jQuery(selectors).empty();
			jQuery(selectors).html(resultHtml);
			jQuery('body,html').animate({scrollTop:0},200);   // set back to top away
			jQuery('.back').remove(); 						 // remove class = back from manu
   		}
	}).done(function(){
		callback();
	});
}

function closePopUpCRM() {
	jQuery('div#popupCRM').hide('slow');
}