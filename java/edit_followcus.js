$(document).ready(function(){  
    var dateBefore=null;  
    $('.datepicker').datepicker({  
        dateFormat: 'dd-mm-yy',  
        showOn: 'button',  
     	buttonImage: 'images/calendar.gif',
        buttonImageOnly: true,  
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
        monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
        changeMonth: true,  
        changeYear: true,  
        beforeShow:function(){    
            if($(this).val()!=""){  
                var arrayDate=$(this).val().split("-");       
                arrayDate[2]=parseInt(arrayDate[2])-543;  
                $(this).val(arrayDate[0]+"-"+arrayDate[1]+"-"+arrayDate[2]);  
            }  
            setTimeout(function(){  
                $.each($(".ui-datepicker-year option"),function(j,k){  
                    var textYear=parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
                    $(".ui-datepicker-year option").eq(j).text(textYear);  
                });               
            },50);  
        },  
        onChangeMonthYear: function(){  
            setTimeout(function(){  
                $.each($(".ui-datepicker-year option"),function(j,k){  
                    var textYear=parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
                    $(".ui-datepicker-year option").eq(j).text(textYear);  
                });               
            },50);        
        },  
        onClose:function(){  
            if($(this).val()!="" && $(this).val()==dateBefore){           
                var arrayDate=dateBefore.split("-");  
                arrayDate[2]=parseInt(arrayDate[2])+543;  
                $(this).val(arrayDate[0]+"-"+arrayDate[1]+"-"+arrayDate[2]);      
            }         
        },  
        onSelect: function(dateText, inst){   
            dateBefore=$(this).val();  
            var arrayDate=dateText.split("-");  
            arrayDate[2]=parseInt(arrayDate[2])+543;  
            $(this).val(arrayDate[0]+"-"+arrayDate[1]+"-"+arrayDate[2]);  
        }     
  
    }); 
	$('.pleasure_value').inputnumber();
	$('.pleasure_value').inputmax(10);
});
$( "#dialog-form" ).dialog({
	  autoOpen: false,
	  height: 600,
	  width: 1024,
	  modal: true,
	  buttons: {
	    'บันทึกข้อมูล': function() {
	    	var answer 		= $('#edit_follow').serialize();
	    	var json_data 	= $("#data").data("all");
	    	json_data = JSON.stringify(json_data);
	    	if($('#edit_follow').inputvalidate()){
	    		$.post('edit_followcus.php?operator=update_answer&json_data='+json_data,answer,function(response){
		    		if(response=true){
		    			alert('แก้ไขข้อมูลเรียบร้อยแล้ว');
		    			$("#dialog-form").dialog( "close" );
		    		}
				});
	    	}else{
	    		alert('กรุณากรอกข้อมูลให้ครบทุกช่อง');
	    	}
	    }
	  },
});
var id_temp = "";
function get_event(cus_id){
	if(id_temp != cus_id){
		$.get('edit_followcus.php?operator=get_event&id='+cus_id,'',function(response){
			$("#event_"+cus_id).html('<td colspan=7><table>'+response+'</table></td>');
			$("#event_"+id_temp).fadeOut();
			$("#event_"+cus_id).fadeIn();
			id_temp = cus_id;
		});
	}else{
		$("#event_"+cus_id).toggle();
	}
}
function get_answer(cus_no,event_id){
	$("#edit_follow").html('<div id="loading" align="center"><br><br><br><br><img src="images/lightbox-ico-loading.gif"> กำลังโหลด...</div>');
	$("#dialog-form").dialog( "open" );
	$.get('edit_followcus.php?operator=get_answer&cus_no='+cus_no+'&event_id='+event_id,'',function(response){
		setTimeout(function(){
			$("#edit_follow").html(response);
		},500);
	});
}
var limit 		= 0;
var temp_limit  = 10;
var where 		= "";
$(document).ready(function(){
	$('.search').click(function(){
		where 	= $('#tb_search_form').serialize();
		limit 	= 0;
		temp_limit = 10;
		id_temp = "";
	});
	$('.loadmore').click(function(){
		var btn = $(this);
		$(this).hide();
		$('.animation_image').show();
		setTimeout(function() {
     		limit+=10;
			$.get('edit_followcus.php?operator=get_more&temp_limit='+temp_limit+'&limit='+limit+'&'+where,'',function(response){
				$("#result_data").append(response);
				$('.animation_image').hide();
				btn.show();
			});
		}, 2000);
		
	});

});
