//$(function() { $('#footer-wrap').css({position:"fixed"}); })
//clear data ก่อน ใส่ข้อมูลใหม่ลงไป
var VAT = 0.07;
var VAT_VAL = 7;

/* $(function() {
    $.ajaxSetup({
        beforeSend: function( xhr ) {

            $('button').each(function(){
                $(this).attr('disabled',true);
            });

            $('input[type="submit"]').each(function(){
                $(this).attr('disabled',true);
            });

            if($('div#loadmore_submit_icon').size() == 0){
                var tagLoad = '<div id="loadmore_submit_icon" align="center" ';
                tagLoad     = tagLoad + 'style="position: fixed; top: 50%; left: 50%; margin-top: -48px; margin-left: -54px; z-index: 100;">';
                tagLoad     = tagLoad + '<img src="images/preload/kdui_preload_01.gif"> Loading...</div>';

                $('body').prepend(tagLoad);
            }else{
            	 $('div#loadmore_submit_icon').show();
            }

            // alert('beforeSend');
        },
        complete: function(xhr, stat) {
            $('button').each(function(){
                $(this).attr('disabled',false);
            });

            $('input[type="submit"]').each(function(){
                $(this).attr('disabled',false);
            });

            if($('div#loadmore_submit_icon').size() > 0){
                $('div#loadmore_submit_icon').hide();
            }

            // alert('complete');
        },
        success: function(result,status,xhr) {
            // alert('success');
        },
        error: function(jqXHR, exception) {
            if (jqXHR.status === 0) {
                // alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        }
    });
}); */

/* เช็คบ็อกซ์ / ไม่เช็ค ทุกอัน */
function checkAll(chkName,flag){
	   var checkFlag = $("INPUT[name='" + chkName + "']").attr('checked');
	   if(checkFlag){ // เช็คบ็อกถูกเช็คเอาไว้แล้ว
		   flag = 0;
	   }else{
		   flag= 1;  
	   }
	   
	   if (flag == 0)
	   {//[type='checkbox']
	      $("INPUT[name='" + chkName + "']").attr('checked', false);
	   }
	   else
	   {
	      $("INPUT[name='" + chkName + "']").attr('checked', true);
	   }
}

jQuery.fn.center = function () {
    this.css("position","absolute");  this.css("display","block");
    this.css("top", ( $(window).height() - this.height() ) / 2+$(window).scrollTop() + "px");
    this.css("left", ( $(window).width() - this.width() ) / 2+$(window).scrollLeft() + "px");
    return this;
}

function browseCheck( jshow, target, param){ 

	// Function Generate คู่มือ (2015-12-22)
	if( target.indexOf("?") >= 0 ){
		jQuery.post( '/ERP_masterdata/manual.php?'+target.split('?')[1], function(res){
			if(res != '' ){
				if( res != '<!-- END Manual -->' ){
					jQuery("html").find("#manualBtn").remove();
					jQuery("body").append(res);
				}else{
					jQuery("html").find("#manualBtn").remove();
				}
			}
		});
	}
	//
	var opt;
	var selectors = "#"+jshow;
	$(selectors).empty();
	var strLoadingText = "<div style='left: 50%; margin-left: -33px; margin-top: -88px; position: absolute; text-align: center; top: 50%;' id='pre_loading'><img src='images/lightbox-ico-loading.gif'  style='padding-left:auto;padding-right:auto;'/><br /><span style='font-size: 12px; font-weight: bold; color: #000;'>Loading ...</span></div>"
	$(selectors).html(strLoadingText);
	$.ajax({
		url: target,
		context:document.body,
		data:param,
		success: function(resultHtml){	
			$(selectors).empty();
			$(selectors).html(resultHtml);
			$('body,html').animate({scrollTop:0},200);   // set back to top away
			$('.back').remove(); 						 // remove class = back from manu
			//$("body").css("overflow", "hidden");
   		}
	});	
}

function browseCheckTo( jshow, target,param){ 
	var opt;
	var selectors = "#"+jshow;
	$(selectors).empty();
	var strLoadingText = "<div style='text-align:center' id='pre_loading'><img src='images/lightbox-ico-loading.gif'  style='padding-left:auto;padding-right:auto;'/><br /><span style='font-size: 12px; font-weight: bold; color: #000;'>Loading ...</span></div>"
	$(selectors).html(strLoadingText);
	$.ajax({
		url: target+'&<? SID ?>&' + (+ new Date),
		context:document.body,
		data:param,
		success: function(resultHtml){	
			$(selectors).empty();
			$(selectors).html(resultHtml);
			$('.back').remove(); 						 // remove class = back from manu
			//$("body").css("overflow", "hidden");
   		}
	});	
}

//เอาข้อมูลที่ได้ ต่อจาก ข้อมูลเดิมเลย ไม่เคลียร์ข้อมูล
function appendBrowseCheck(targetDiv, link,param){
  	
	var selectors = "#"+targetDiv;
	var strLoadingText = "<span style='text-align: center' id='imgLoading'><img src='images/lightbox-ico-loading.gif' /></span>"
	$(selectors).html(strLoadingText);
	
	$.ajax({
		url: link+'&<? SID ?>&' + (+ new Date),
		global:false,
		context:document.body,
		data:param,
		success: function(responsehtml) {$('#imgLoading').html("");$(selectors).append(responsehtml);}
	}) ;
}

//เอาข้อมูลที่ได้ วางไว้ก่อนหน้าข้อมูลที่มีอยู่แล้ว 
function prependBrowseCheck(targetDiv, link,param){
  	var selectors = "#"+targetDiv;
	//var strLoadingText = "<div style='text-align: center'><img src='images/loading.gif' /><br /><span style='font-size: 12px; font-weight: bold; color: #000'>Loading ...</span></div>"
	//$(selectors).html(strLoadingText);
	$.ajax({
		url: link+'&<? SID ?>&' + (+ new Date),
		global:false,
		context:document.body,
		data:param,
		success: function(responsehtml) { $(selectors).prepend(responsehtml);}
	}) ;
}
/* use for preview image before upload  */
function preview(what) {
	if(jQuery.browser.msie) { document.getElementById("preview-photo").src=what.value; return;}
	else if(jQuery.browser.safari) {document.getElementById("preview-photo").src=what.value; return;}
	document.getElementById("preview-photo").src=what.files[0].getAsDataURL();
	var h = jQuery("#preview-photo").height();  var w = jQuery("#preview-photo").width();
	if ((h > 100) && (w > 100)){
		if (h > w){jQuery("#preview-photo").css("height", "150px"); jQuery("#preview-photo").css("width", "auto");}
		else { jQuery("#preview-photo").css("width", "150px"); jQuery("#preview-photo").css("height", "auto"); }
	}
}

/* use for gerneral page  */
function format_decimals(id){ 
	var number = ""; number = $(id).val(); number = textTOnum(number); number = parseFloat(number); 
	if(number != ''|| number != '0'|| number != 0){ number = formatNumber(number,2,1); $(id).val(number); } 
}
function textTOnum(ams){ /*  ตัดข้อความเอา ลูกน้ำออก */
	if(!ams){ var ams = 0+',';}   ams = eval("'"+ams+"'");
	if(ams.indexOf(',') < 0) { ams = ams+','; }
	var strd='';var str= ams.split(',');
	if(str.length > 1){
	  for (var i=0;i<str.length;i++) { strd=strd+str[i]; }
		if(strd ==0) str = ams; else str = strd;
	}else str = ams;
	if(str=='.NaN') str='0.00';
	return parseFloat(str);
}
function roundnumber(rnum, rlength) { // Arguments: number to round, number of decimal places<br>// ทำตำแหน่งทศนิยมตามต้องการ
	var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength); return newnumber;
}
function formatNumber(num, decplaces,showtt) {
	num = parseFloat(num);
	if (!isNaN(num)){
		var str = "" + Math.round (eval(num) * Math.pow(10,decplaces));
		if (str.indexOf("e") != -1) {return "Out of Range";}
		while (str.length <= decplaces){ str = "0" + str;}
		var decpoint = str.length - decplaces;var tmpNum = str.substring(0,decpoint);
		//---------------Add Commas--------------------------
		var numRet = tmpNum.toString();if(showtt==1){var re = /(-?\d+)(\d{3})/;while (re.test(numRet)){numRet = numRet.replace(re, "$1,$2");}}
		return numRet + "." + str.substring(decpoint,str.length);
	}else{ return ""; }//return "0.00"; 
}
function checknumber(e){ keyPressed = e.which;
	if(((keyPressed < 48) || (keyPressed > 57)) && (keyPressed != 0) && (keyPressed != 8) && (keyPressed != 46)) keyPressed = e.preventDefault();
}
function date_picker(){  
    $(".datepicker").datepicker({
		dateFormat: "dd-mm-yy",
		changeMonth: true,
		changeYear: true ,
		beforeShow: function(input, inst){
			inst.dpDiv.css({marginTop: -input.offsetHeight + 'px', marginLeft: input.offsetWidth + 'px'});
		}
	});       
}
function dateMonthYear(jShow){
	$(function() {
		$(jShow).datepicker({
			dateFormat: "dd-mm-yy",
			changeMonth: true,
			changeYear: true, 
			beforeShow:function(input, inst){
				var dt = $.datepicker.parseDate('dd-mm-yy', $(input).val());
					if(dt){dt.setYear(dt.getFullYear() - 543); //$(this).val();//$(this).val($.datepicker.formatDate('dd-mm-yy', dt));
				}
				setTimeout(function(){
					$.each($(".ui-datepicker-year option"),function(j,k){
						var textYear=parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
						$(".ui-datepicker-year option").eq(j).text(textYear);
					});
				},50);
			},
			onChangeMonthYear: function(){  
				setTimeout(function(){  
					$.each($(".ui-datepicker-year option"),function(j,k){  
						var textYear=parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
						$(".ui-datepicker-year option").eq(j).text(textYear);  
					});               
				},50);
			}
		});
	});
}
function IsNumeric(input) {
    return /^-?(0|[1-9]\d*|(?=\.))(\.\d+)?$/.test(input);
}
function call_overlay(div){ $(div).show().addClass('ui-widget-overlay').css({width:"100%" ,height:$(document).height(),opacity:.3});}
function ShowCenter(display){
	var el,px,py,halfW,halfH;
	el = $("#"+display);
	halfW = gWH().width/2;////ตำแหน่งกึ่งกลางแนวนอน (จะคงที่ตลอด เพราะไม่ให้มี scroll bar ทางแนวนอน)
	halfH = (gWH().height/2) + getScrollTop();//ตำแหน่งกึ่งกลางแนวตั้ง ของหน้าจอขณะนั้น
	px = halfW - (el.width()/2);
	py = halfH - (el.height()/2) - 20;
	if(py<1){py=0;}
	el.css({left : px, top : py,'position':'absolute'});
	el.fadeIn();
}
function gWH(){  //ความกว้าง , ความสูง ของหน้าจอ (ไม่ใช่ความสูงของ document *ความสูงของ document = scrollHeight)
    var e = window, a = 'inner';  
    if ( !( 'innerWidth' in window ) ){  
        a = 'client';  
        e = document.documentElement || document.body;  
    }  
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }  
}
function getScrollTop(){  //ตำแหน่งขอบบน ของจอภาพขณะนั้น
    var e = document.documentElement || document.body;
    return e.scrollTop;
}
function auto_com(target,input_id){  
	$(function() {
		//$(hidden_val).val("");
		$(input_id).autocomplete({
			source: target,minLength: 3,
			select: function(event, ui) {
				//$(hidden_val).val(ui.item.hidden_value);
			}
		});
	});
}

/* --------------- new use general -----------------*/
function auto_comHidden(target,input_id,hidden_val,shoe){
	$(function() {
		$(hidden_val).val("");
		$(input_id).autocomplete({
			delay : 500,//milliseconds #naizan 2011-05-20
			source: target,
			minLength: 2,
			select: function(event, ui) {
				$(hidden_val).val(ui.item.hidden_value);
				if(shoe=="changeResperson"){ check_resperson($(hidden_val).val());}
			}
		});
	});
} 

function remove_class(id,class_name){$(id).removeClass(class_name);}
function adds_class(id,class_name){$(id).addClass(class_name);}
function set_none(id){ if(document.getElementById(id))document.getElementById(id).style.display = 'none';}
function set_show(id){if(document.getElementById(id))document.getElementById(id).style.display = 'inline';}
function set_jnone(id){if(document.getElementById(id)) $('#'+id).hide();}
function set_jshow(id){if(document.getElementById(id)) $('#'+id).show();}
function set_focus(id){if(document.getElementById(id)) $('#'+id).focus();}
function add_attr(ele,att){$(ele).attr(att,att);}
function rem_attr(ele,att){$(ele).removeAttr(att);}
function clear_val(ele){$(ele).val('');}
function reset_form(form_id){ $(form_id).get(0).reset(); }
function remove_ele(ele){ $(ele).remove(); }
function clear_html(ele){ $(ele).html(''); }
function ui_tabs(div_tabs){ $(div_tabs).tabs(); }
function adds_css(ele,att,sty){ $(ele).css(att,sty);}
function checked_alls(names,case_name,target,css_proper){ 
	$("input[name='"+ names +"']:checkbox").attr("checked",case_name); 
}
/* *
 *     แยกเครื่องหมาย vat ว่า + sinv - 
 * @param vat_value=> ค่าของ VAT 
 * @param return_case => ค่าที่จะ return กลับ  มี  'systax','vatval'
 * */
function select_vat_value(vat_value,return_case){ 
	var string = '+71';
	var length = string.length;

	if(length<3){
		var from = (length-1);
		var to   = (length);
	}else{
		var from = 1;
        var to   = (length);
	}
	var systax = string.substring('0', '1');  // เครื่องหมาย + หรือ -
	var vatval = string.substring(from, to);  // vat value
   
	if(return_case!=undefined){
		switch(return_case){
			case 'systax':
				return systax;
			break;
			case 'vatval':
				return vatval;
			break;
		}
	}else{
		return systax+'|||'+vatval;
	}
}
/*==============================================*/
/*		get position XY							*/
/*		เป็น function อื่นที่ไว้ให้เรียกใช้ได้ตลอด				*/
/*==============================================*/
function findPosX(obj){
    var curleft = 0;
    if(obj.offsetParent)
        while(1){ 
          curleft += obj.offsetLeft;
          if(!obj.offsetParent) break;
          obj = obj.offsetParent;
        }
    else if(obj.x) curleft += obj.x;
    return curleft;
}
function findPosY(obj){
	var curtop = 0;
	if(obj.offsetParent)
		while(1){
		  curtop += obj.offsetTop;
		  if(!obj.offsetParent)
			break;
		  obj = obj.offsetParent;
		}
	else if(obj.y)
		curtop += obj.y;
	return curtop;
}
function check_feb_date(date_val,systax,format,target,pattern){  // ใช้สำหรับ check วันที่หมดอายุ ในหน้า บันทึกรับเข้า / แก้ไข assets โดยวันที่หมดอายุของเดือนกุมภาพันธ์ ห้ามเกินวันที่  29-02
	if(date_val){
		var date = date_val.split(systax);
		switch(format){
			case 'ymd': 
				if(date[1]=='02'){ if(date[2]>28){ date[2]='28'; }}
				if(pattern==543){ +date[1] - +pattern; }
				alert(date[2]+'-'+date[1]+'-'+date[0]);
				$(target).val(date[2]+'-'+date[1]+'-'+date[0]);  
			break;
			case 'dmy': 
				if(date[1]=='02'){ if(date[0]>28){ date[0]='28'; }}
				if(pattern==543){ +date[1] - +pattern; }
				$(target).val(date[0]+'-'+date[1]+'-'+date[2]);  
			break;
		}
	}
}
function show_dialog(url,showFormDiv){
	$(showFormDiv).draggable();
	$.get(url,function(data){
		$(showFormDiv).html(data);
		$(showFormDiv).removeClass("displayNone");
		$("#popup_overlay,#divShowForm,.popup_overlay").removeClass("displayNone");
		if(showFormDiv=='#divShowForm2'){
			$(showFormDiv).attr("style","margin-left:auto;margin-right:auto;width:400px;top:5px;height:200px;position:absolute").fadeIn(); 
		}
	});
}
function close_dialog(target){ $(target).attr("style","display:none"); }
function cal_inc_vat(target,value,vat,any){ 
	value = textTOnum(value); var vat_val = roundnumber(value*vat/100,2); 
	$(target).val(vat_val); format_decimals(target);
	if(any){ 
		var total = roundnumber(value+vat_val,2)
		$(any).val(total); format_decimals(any);
	}
}
function autoTab(obj,pattern,symbol){
	/* กำหนดรูปแบบข้อความโดยให้ _ แทนค่าอะไรก็ได้ แล้วตามด้วยเครื่องหมาย
	หรือสัญลักษณ์ที่ใช้แบ่ง เช่นกำหนดเป็น  รูปแบบเลขที่บัตรประชาชน
	4-2215-54125-6-12 ก็สามารถกำหนดเป็น  _-____-_____-_-__
	รูปแบบเบอร์โทรศัพท์ 08-4521-6521 กำหนดเป็น __-____-____
	หรือกำหนดเวลาเช่น 12:45:30 กำหนดเป็น __:__:__
	ตัวอย่างข้างล่างเป็นการกำหนดรูปแบบเลขบัตรประชาชน
	*/
	//var pattern=new String("_-____-_____-_-__"); // กำหนดรูปแบบในนี้
	//var pattern_ex=new String("-"); // กำหนดสัญลักษณ์หรือเครื่องหมายที่ใช้แบ่งในนี้
	var pattern=new String(pattern);
	var pattern_ex=new String(symbol);
	var returnText=new String("");
	var obj_l=obj.value.length;
	var obj_l2=obj_l-1;
	for(i=0;i<pattern.length;i++){			
		if(obj_l2==i && pattern.charAt(i+1)==pattern_ex){
			returnText+=obj.value+pattern_ex;
			obj.value=returnText;
		}
	}
	if(obj_l>=pattern.length){
		obj.value=obj.value.substr(0,pattern.length);			
	}
}
function call_flexigrid(){ 
	$("#myflexigrid").flexigrid({ // กำหนดให้สร้าง data grid ให้กับ แท็ก table ที่มี id=myflexigrid
		url: 'receivable_manage_updates_check.php', // กำหนด url ของไฟล์ที่จะใช้เชื่อมต่อฐานข้อมูลมาแสดง
		dataType: 'json', // กำหนดชนิดของไฟล์ข้อมูลที่ต้องการใช้งานในที่นี้ใช้ json 
		colModel : [  // กำหนดลักษณะการแสดงของคอลัมน์ในตาราง อ่านคำอธิบายด้านล่าง
				{display: 'ลำดับ', name : 'RECEIVABLE_PAYMENT_ID', width : 40, sortable : true, align: 'center'},
				{display: 'รหัสลูกค้า', name : 'RECEIVABLE_PAYMENT_CUSNUM', width : 500, sortable : true, align: 'left'},
				{display: 'รวมเป็นเงิน', name : 'RECEIVABLE_PAYMENT_AMOUNT', width : 40, sortable : true, align: 'center'},
				{display: 'เข้าบัญชี', name : 'RECEIVABLE_PAYMENT_INTO_ACC', width : 100, sortable : true, align: 'center'}
			],
		sortname: "RECEIVABLE_PAYMENT_ID", // กำหนดการจัดเรียงเริ่มต้น เรียงจาก field ใดในฐานข้อมูล
		sortorder: "asc", // กำหนดเรียกจากมากไปน้อย หรือน้ยอไปมาก desc / asc
		usepager: true, // กำหนดให้แสดงส่วนการแบ่งหน้าหรือไม่ true / false
		title: 'รายการบทความใน www.ninenik.com', // หัวข้อตาราง
		useRp: false, // กำหนดให้แสดงการ กำหนดจำนวนรายการแสดงต่อหน้า หรือไม่ true / false
		rp: 10, // กำหนดจำนวนรายการที่จะแสดงในแต่ละหน้า
		showTableToggleBtn: false, // กำหนดให้แสดงปุ่ม ซ่อน / แสดงตารางหรือไม่ true / false
		width: 750, // กำหนดความกว้าง
		height: 255  // กำหนดความสูง
	});   
	// กำหนดลักษณะการแสดงของคอลัมน์ในตาราง
	//		display คือ กำหนด ชื่อข้อความที่ต้องการแสดงหัวข้อคอลัมน์
	//		name คือ ชื่อ field ของตารางในฐานข้อมูลที่สอดคล้องกัน
	//		widh คือ ความกว้างของคอลัมน์ที่ต้องการแสดง หน่วยเป็น pixel (กำหนดแต่ตัวเลข) 
	//		sortable คือ กำหนดว่าคอลัมน์สามารถทำการเรียงข้อมูลได้หรือไม่ ค่า true / false
	//		align คือ กำหนดการจัดตำแหน่่งการแสดงข้อมูล ค่า left / center / right
	//		hide คือ กำหนดให้แสดงหรือไม่แสดงคอลัมน์นั้น ค่า true / false (ส่วนเพิ่มเติม กำหนดหรือไม่ก็ได้)
}	
/* jQuery lightBox plugin  */
function use_lightBox(image_target){ $(image_target).lightBox(); }


/* use for ic_followcus_form */
function form_title_module(){
	followGetData('display_event','ic_followcus_form.php?func=event_form&'+$('#tb_pattern_form').find('input').serialize());
}

function formPrevTitleModule(){
	followGetData('question_list','ic_followcus_form.php?func=create_wait_event&event_id='+$('#event_title_ids').val()+'&'+$('#tb_pattern_form').find('input').serialize());
	hideObj('div_tb_question');
	changeClass(this.id,'','BTwhite');
	//document.getElementById('bt_manage').className='nz_button';
}

function option_event_permiss(tr,target,element){  
	$.get("ic_followcus_form.php",$('.event_permiss').serialize()+'&func=event_permiss&case_name='+tr,function(data){
		$('#'+$(element).attr('id')).removeClass("border_orange");
		$('#'+target).html(data).focus().addClass("border_orange"); $('#'+tr).removeAttr("style"); 
	});
}

function follow_organization_comp(){  
	var suggest = $('input[name=suggest]:checked').val();
	var target  = 'ic_autocomplete.php?event=follow_organization&suggest='+suggest;
	var input_id = '#auto_follower_up';
	$(function() {
		$(input_id).autocomplete({
			source: target,minLength: 3,
			select: function(event, ui) {		
				switch(suggest){
					case 'suggest_company': 		var tr_ids 	 = 'comp'+ui.item.id_value; 	break;
					case 'suggest_department': 		var tr_ids 	 = 'depa'+ui.item.id_value; 	break;
				}
				if($('#'+tr_ids).size()>0){  $('#'+tr_ids).remove(); }
				
				$.ajax({
					url: 'ic_followcus_form.php?func=follow_organization_selected&suggest='+suggest+'&id_value='+ui.item.id_value+'&value='+ui.item.value+'&company_id='+ui.item.company_id+'&departs_id='+ui.item.departs_id+'&section_id='+ui.item.section_id,
					global:false,
					context:document.body, 
					//success: function(responsehtml) {$('#the-follwer-list').append(responsehtml);}
					success: function(responsehtml) {
						var cutRes = responsehtml.split('*@#@*');
						
						if($('#the-follwer-list tr').size()>0){
							alert('สามารถระบุผู้ติดตามกิจกรรมได้เพียงเรคคอร์ดเดียว  !! '); return false;
						}
						
						removeRes_follow_list(cutRes[0],cutRes[1]);
					}
				});
			
				// $('#the-follwer-list tr').each(function(key,value){ 
					// var divs = $(this).attr('id');
					// alert(divs);
					// var size = $('#'+divs).size();
					// alert(size);
					// if(size>1){  divs.remove(); }
				// });	
			},
			close: function(cevent, cui) { 
				$('#auto_follower_up').val('').focus(); 
			}
		});
	}); 
}

function removeRes_follow_list(val1,val2){
	/* var cutVal = val1.split('#');
	var numCut = cutVal.length;
	var arrChk = new Array();
    var chk = false;
    var txtI = '';

    var no = 0;
	$(cutVal).each(function(key,value){
        if(no>0){ txtI += '#'; }
		txtI += value; arrChk[no] = txtI; no++;
		//if(numCut!=(key+1)){ txtII += value+'#'; arrChk[no] = txtII; no++; }
	});

    $(arrChk).each(function(key,value){
        //$('input[value="'+value+'"]').parent().parent().remove();
		if($('input[value="'+value+'"]').size()>0){
			chk = true;
		}
    });

	if(chk==true){ return false; }

	if($('input[value^="'+val1+'"]').size()>0){
    	$('input[value^="'+val1+'"]').parent().parent().remove();
	} */

	$('tbody#the-follwer-list').append(val2);
}

/*function removeRes_follow_list(val1,val2){
    //แสดงส่วนที่ต่ำที่สุด
    var cutVal1 = val1.split('#');
    var plusVal = '';

    if($('input[value^="'+val1+'"]').size()==0){
	    $(cutVal1).each(function(key,value){
	        if(plusVal){ plusVal += '#'; }
	        plusVal += value;
	
	        $('input[value="'+plusVal+'"]').parent().parent().remove();
	    });
	
	    $('#the-follwer-list').append(val2);
    }
}*/

function autoLoadData(jsonParam){
	var arr 		= JSON.parse(jsonParam);
	var response	= '';
	var decode		= '';
	var sendV		= '';
	
	if(arr.sDB<1){
		$.xhrPool.abortAll();
	}
	if(arr.bLoad){
		autoLoading(arr.bLoad, 'TOP', 'ON'); // แสดง load
	}
	if(arr.toggle){
		$(arr.toggle).css('display','none');
	}
	
	// เช็คฟอร์ม
	if(arr.form){
		if(arr.value){
			arr.value = arr.value + '&' + $(arr.form).serialize();
		}else{
			arr.value = $(arr.form).serialize();
		}
	}
	// เช็ค json
	if(arr.value){
		arr.value = arr.value + '&vJson=' + jsonParam;
	}else{
		arr.value = 'vJson=' + jsonParam;
	}
	
	$.post(arr.path, arr.value, function(res){
		response	= res;
	})
	.complete(function(){
		decode = chkResponseJson(response);
		$(arr.tag).append(decode.html); // แสดงข้อมูล
		
		if(arr.bLoad){
			autoLoading(arr.bLoad, 'TOP', 'OFF'); // ซ่อน load
		}
		
		if(decode.status == 'OK'){
			decode.html	= '';
			var sendV = JSON.stringify(decode, null, 2); // encode json
		
			// alert(sendV);
			autoLoadData(sendV);
		}else{
			if(arr.toggle){ $(arr.toggle).css('display','block'); }
			return false;
		}
	}, 'json');
}

function IsJsonString(txt){
	var data

	try     {  data = eval('('+txt+')'); }
	catch(e){  data = false;             }

	return data;
}

function chkResponseJson(response){
	var decode = '';

	// ถ้าเป็น json
	if(IsJsonString(response)){
		decode		= JSON.parse(response); // decode json
	// ถ้าไม่เป็น json
	}else{
		if(response){
			$('div#main').html(response);
			return false;
		}
	}
	
	return decode;
}

$.xhrPool = [];
$.xhrPool.abortAll = function() {
	// alert('abortAll');
    $(this).each(function(idx, jqXHR) {
        jqXHR.abort();
    });
    $(this).each(function(idx, jqXHR) {
        var index = $.inArray(jqXHR, $.xhrPool);
        if (index > -1) {
            $.xhrPool.splice(index, 1);
        }
    });
};

$.ajaxSetup({
    beforeSend: function(jqXHR) {
        $.xhrPool.push(jqXHR);
    },
    complete: function(jqXHR) {
        var index = $.inArray(jqXHR, $.xhrPool);
        if (index > -1) {
            $.xhrPool.splice(index, 1);
        }
    }
});

function autoLoading(tag, position, status){
	if(status == 'ON'){
		if(position == 'TOP'){
			$('<div>', {
				id: 'autoPreloadData',
				style: 'widht: 100%; text-align: center; margin: 4px 0 10px;'
			}).html('<img src="images/loading.gif">').prependTo(tag);
		}else{
			$('<div>', {
				id: 'autoPreloadData',
				style: 'widht: 100%; text-align: center; margin: 4px 0 10px;'
			}).html('<img src="images/loading.gif">').appendTo(tag);
		}
	}else{
		$('div#autoPreloadData').remove();
	}
}