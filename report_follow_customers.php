<?php
require_once("config/general.php");
require_once("function/general.php");
require_once("classes/report_follow_customers.class.php");

ini_set('max_execution_time', 300);

global $tpl;
Conn2DB();

$thisPage = "report_follow_customers";
$cls_rp_followcus = new report_follow_customers($thisPage);//classes/report_follow_customers.class.php
//echo "<pre>".print_r($_GET)."</pre>";

$Action = $_GET['action'];
switch ($Action) {
	case 'export_excel' :
		include ("libraries/PHPExcel/PHPExcel.php");
		$objPHPExcel = new PHPExcel();
		function gen_cell($pos){
			$alpabet = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
			$length  = count($alpabet)-1;
			if( $pos > $length ){
				$exp = explode('.',$pos/count($alpabet));
				$diff 	=  $exp[0]-1;
				if( $diff >= count($alpabet) ){
					$return = gen_cell($diff);
				}
				if($exp[1] > 0){
					$mod =  $pos%count($alpabet);
				}else{
					$mod = 0;
				}

				return $return.$alpabet[$diff].$alpabet[$mod];
			}else{
				return $alpabet[$pos];
			}
		}
		// generate
		$objPHPExcel->getProperties()->setCreator("PHP");
		$objPHPExcel->getProperties()->setLastModifiedBy("PHP");
		$objPHPExcel->getProperties()->setTitle("รายงานรายละเอียดการดูแลลูกค้าหลังการขาย");
		$objPHPExcel->getProperties()->setSubject("รายงานรายละเอียดการดูแลลูกค้าหลังการขาย");
		$objPHPExcel->getProperties()->setDescription("รายงานรายละเอียดการดูแลลูกค้าหลังการขาย");

		$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
		$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getDefaultStyle()->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getDefaultStyle()->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getDefaultStyle()->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getDefaultStyle()->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		// ข้อมูลทั้งหมด
		$where = '';
			if( $_GET['idCom'] != '' ){  $where.=" AND r_working_company = '".$_GET['idCom']."' "; 	}
			if( $_GET['idTeam'] != '' ){ $where.=" AND r_section = '".$_GET['idTeam']."' "; 		}
			if( $_GET['slEven'] != '' ){ $where.=" AND event_id = '".$_GET['slEven']."' "; 		}
		if ($_GET['page_value'] == 'OU') {
			if($_GET["id_TeamCommand"] 	 != ""){ $where .= " AND r_id_card IN (".$_GET["id_TeamCommand"].")"; }
		}

		if( $_GET['StartDate'] != '' ){
			$exp 	= explode('-',$_GET['StartDate']);
			$start 	= (($exp[2]*1)-543)."-".$exp[1]."-".$exp[0];
			if( $_GET['StopDate'] != '' ){
				$exp2 	= explode('-', $_GET['StopDate']);
				$end 	= (($exp2[2]*1)-543)."-".$exp2[1]."-".$exp2[0];
			}else{
				$end 	= date("Y-m-d");
			}
			$where.=" AND ( start_date BETWEEN '".$start."' AND '".$end."'  
						OR 	stop_date BETWEEN '".$start."' AND '".$end."' 
						OR (
					         ( '".$start."' BETWEEN start_date AND stop_date ) AND  
					         ( '".$end."' BETWEEN start_date AND stop_date ) 
							)

						) ";
		}

		$sql = " 	SELECT 	report_follow.event_id
						,	report_follow.follow_cus_id
						,	report_follow.follow_cus_his_id

						,	report_follow.CusNo
						,	report_follow.event_title
						,	CONCAT( IF(report_follow.Be IS NOT NULL
										, report_follow.Be
										, CONCAT('')	 
									), ' ' ,
									IF(report_follow.Cus_Name IS NOT NULL
										, report_follow.Cus_Name
										, CONCAT('')	 
									), ' ' ,
									IF(report_follow.Cus_Surename IS NOT NULL
										, report_follow.Cus_Surename
										, CONCAT('')	 
									)
							) AS CusName
						, 	report_follow.ADDR_VILLAGE
						,	report_follow.ADDR_NUMBER
						, 	report_follow.ADDR_GROUP_NO
						,	report_follow.ADDR_SUB_DISTRICT
						,	report_follow.ADDR_DISTRICT
						,	report_follow.ADDR_PROVINCE
						,	report_follow.ADDR_POSTCODE
						,	report_follow.TEL_NUM
						,	report_follow.CUS_TYPE_LEVEL_DESC
						,	report_follow.EmpName
						,	report_follow.CompanyName
						,	CONCAT(DATE_FORMAT(report_follow.date_value , '%d/%m/'),DATE_FORMAT(report_follow.date_value , '%Y')+543) AS date_value
						,	CONCAT(DATE_FORMAT(report_follow.process_date , '%d/%m/'),DATE_FORMAT(report_follow.process_date , '%Y')+543) AS process_date
						,	report_follow.follow_contact_status
						,	report_follow.follow_convenient_status
						,	report_follow.follow_prospect_status
						,	report_follow.follow_contact_reason
						,	report_follow.follow_status_remark
						,	CONCAT(DATE_FORMAT(report_follow.repeat_date , '%d/%m/'),DATE_FORMAT(report_follow.repeat_date , '%Y')+543) AS repeat_date
						,	DATE_FORMAT(report_follow.repeat_date , '%H:%i น.') AS repeat_time
						, 	report_follow.quest_list_id
						, 	report_follow.answer
					FROM $config[db_base_name].report_follow 
					WHERE 1=1 $where 
					ORDER BY date_value 
				";
		$res = mysql_query($sql);
		if (@mysql_num_rows($res)  > 0) {
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setTitle("ALL");
			// Set column Style
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('20');
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('30');
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('30');
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('15');
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('15');
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('15');
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('20');
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('20');
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth('20');
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth('20');
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth('20');
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth('20');
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth('30');
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth('30');
			$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth('20');
			$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth('15');
			$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth('15');
			$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth('15');
			$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth('15');
			$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth('15');
			$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth('15');
			$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth('30');
			$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth('30');
			$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth('20');
			$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth('20');

			$objPHPExcel->getActiveSheet()->getStyle('A:Y')->applyFromArray( array( 'fill' => array( 'type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '99ccff') ) ) ); 
			// Set Header Style
			$objPHPExcel->getActiveSheet()->getStyle('A1:Y2')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A1:Y2')->applyFromArray( array( 'fill' => array( 'type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '77aaff') ) ) ); 		

			// Header 
			$objPHPExcel->getActiveSheet()->mergeCells('A1:A2');
			$objPHPExcel->getActiveSheet()->mergeCells('B1:B2');
			$objPHPExcel->getActiveSheet()->mergeCells('C1:C2');
			$objPHPExcel->getActiveSheet()->mergeCells('D1:D2');
			$objPHPExcel->getActiveSheet()->mergeCells('E1:E2');
			$objPHPExcel->getActiveSheet()->mergeCells('F1:F2');
			$objPHPExcel->getActiveSheet()->mergeCells('G1:G2');
			$objPHPExcel->getActiveSheet()->mergeCells('H1:H2');
			$objPHPExcel->getActiveSheet()->mergeCells('I1:I2');
			$objPHPExcel->getActiveSheet()->mergeCells('J1:J2');
			$objPHPExcel->getActiveSheet()->mergeCells('K1:K2');
			$objPHPExcel->getActiveSheet()->mergeCells('L1:L2');
			$objPHPExcel->getActiveSheet()->mergeCells('M1:M2');
			$objPHPExcel->getActiveSheet()->mergeCells('N1:N2');
			$objPHPExcel->getActiveSheet()->mergeCells('O1:O2');
			$objPHPExcel->getActiveSheet()->mergeCells('P1:W1');
			$objPHPExcel->getActiveSheet()->mergeCells('X1:Y1');

			$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'หมายเลขลูกค้า' );
			$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'กิจกรรม' );
			$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'ชื่อ - นามสกุล' );
			$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'หมู่บ้าน' );
			$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'บ้านเลขที่' );
			$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'หมู่ที่' );
			$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ตำบล' );
			$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'อำเภอ' );
			$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'จังหวัด' );
			$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'รหัสไปรษณีย์' );
			$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'เบอร์โทรศัพท์' );
			$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'เกรดลูกค้า' );
			$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'เซลล์' );
			$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'บริษัท' );
			$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'วันที่ทำกิจกรรม' );
			$objPHPExcel->getActiveSheet()->SetCellValue('P1', 'ชุดคำถามมาตรฐาน' );
			$objPHPExcel->getActiveSheet()->SetCellValue('X1', 'โทรซ้ำ' );

			$objPHPExcel->getActiveSheet()->SetCellValue('P2', 'ติดต่อได้' );
			$objPHPExcel->getActiveSheet()->SetCellValue('Q2', 'ติดต่อไม่ได้' );
			$objPHPExcel->getActiveSheet()->SetCellValue('R2', 'สะดวกคุย' );
			$objPHPExcel->getActiveSheet()->SetCellValue('S2', 'ไม่สะดวกคุย' );
			$objPHPExcel->getActiveSheet()->SetCellValue('T2', 'สนใจ' );
			$objPHPExcel->getActiveSheet()->SetCellValue('U2', 'ไม่สนใจ' );
			$objPHPExcel->getActiveSheet()->SetCellValue('V2', 'สาเหตุ' );
			$objPHPExcel->getActiveSheet()->SetCellValue('W2', 'หมายเหตุ' );
			$objPHPExcel->getActiveSheet()->SetCellValue('X2', 'วันที่โทร' );
			$objPHPExcel->getActiveSheet()->SetCellValue('Y2', 'วันที่โทรซ้ำ' );
			$rows 	= 3;
			$event 	= array();
			while ($rs = mysql_fetch_assoc($res) ) {
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rows, $rs['CusNo']		);
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rows, $rs['event_title']	);
				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rows, $rs['CusName']		);
				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rows, $rs['ADDR_VILLAGE'] );
				##################################
				$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rows, $rs['ADDR_NUMBER'] );
				$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rows, $rs['ADDR_GROUP_NO'] );
				$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rows, $rs['ADDR_SUB_DISTRICT'] );
				$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rows, $rs['ADDR_DISTRICT'] );
				$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rows, $rs['ADDR_PROVINCE'] );
				$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rows, $rs['ADDR_POSTCODE'] );
				$objPHPExcel->getActiveSheet()->SetCellValue('K'.$rows, $rs['TEL_NUM'] );
				$objPHPExcel->getActiveSheet()->SetCellValue('L'.$rows, $rs['CUS_TYPE_LEVEL_DESC'] );
				$objPHPExcel->getActiveSheet()->SetCellValue('M'.$rows, $rs['EmpName'] );
				$objPHPExcel->getActiveSheet()->SetCellValue('N'.$rows, $rs['CompanyName'] );
				$objPHPExcel->getActiveSheet()->SetCellValue('O'.$rows, $rs['date_value'] );
				##################################
				if( $rs['follow_contact_status'] == '1' ){
					$objPHPExcel->getActiveSheet()->SetCellValue('P'.$rows, $rs['process_date'] );
				}else{
					$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rows, $rs['process_date'] );
				}
				##################################
				if( $rs['follow_convenient_status'] == '1' ){
					$objPHPExcel->getActiveSheet()->SetCellValue('R'.$rows, $rs['process_date'] );
				}else{
					$objPHPExcel->getActiveSheet()->SetCellValue('S'.$rows, $rs['process_date'] );
				}
				##################################
				if( $rs['follow_prospect_status'] == '1' ){
					$objPHPExcel->getActiveSheet()->SetCellValue('T'.$rows, $rs['process_date'] );
				}else{
					$objPHPExcel->getActiveSheet()->SetCellValue('U'.$rows, $rs['process_date'] );
				}
				##################################
				$objPHPExcel->getActiveSheet()->SetCellValue('V'.$rows, $rs['follow_contact_reason'] );
				$objPHPExcel->getActiveSheet()->SetCellValue('W'.$rows, $rs['follow_status_remark'] );
				##################################
				$objPHPExcel->getActiveSheet()->SetCellValue('X'.$rows, $rs['repeat_date'] );
				$objPHPExcel->getActiveSheet()->SetCellValue('Y'.$rows, $rs['repeat_time'] );
				$rows++;
				$event[  $rs['event_title'] ][] = $rs;
			}
			$sheet = 0;
			foreach ($event as $title => $result){
				$sheet++;
				$objPHPExcel->createSheet($sheet);
				$objPHPExcel->setActiveSheetIndex($sheet);
				$objPHPExcel->getActiveSheet()->setTitle( substr($title, 0 , 80 ) );
				// Set column Style
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('20');
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('30');
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('30');
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('15');
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('15');
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('15');
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('20');
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('20');
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth('20');
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth('20');
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth('20');
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth('20');
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth('30');
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth('30');
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth('20');
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth('15');
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth('15');
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth('15');
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth('15');
				$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth('15');
				$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth('15');
				$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth('30');
				$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth('30');
				$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth('20');
				$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth('20');

				$objPHPExcel->getActiveSheet()->getStyle('A:Y')->applyFromArray( array( 'fill' => array( 'type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '99ccff') ) ) ); 
				// Set Header Style
				$objPHPExcel->getActiveSheet()->getStyle('A1:Y2')->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('A1:Y2')->applyFromArray( array( 'fill' => array( 'type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '77aaff') ) ) ); 		

				// Header 
				$objPHPExcel->getActiveSheet()->mergeCells('A1:A2');
				$objPHPExcel->getActiveSheet()->mergeCells('B1:B2');
				$objPHPExcel->getActiveSheet()->mergeCells('C1:C2');
				$objPHPExcel->getActiveSheet()->mergeCells('D1:D2');
				$objPHPExcel->getActiveSheet()->mergeCells('E1:E2');
				$objPHPExcel->getActiveSheet()->mergeCells('F1:F2');
				$objPHPExcel->getActiveSheet()->mergeCells('G1:G2');
				$objPHPExcel->getActiveSheet()->mergeCells('H1:H2');
				$objPHPExcel->getActiveSheet()->mergeCells('I1:I2');
				$objPHPExcel->getActiveSheet()->mergeCells('J1:J2');
				$objPHPExcel->getActiveSheet()->mergeCells('K1:K2');
				$objPHPExcel->getActiveSheet()->mergeCells('L1:L2');
				$objPHPExcel->getActiveSheet()->mergeCells('M1:M2');
				$objPHPExcel->getActiveSheet()->mergeCells('N1:N2');
				$objPHPExcel->getActiveSheet()->mergeCells('O1:O2');
				$objPHPExcel->getActiveSheet()->mergeCells('P1:W1');
				$objPHPExcel->getActiveSheet()->mergeCells('X1:Y1');

				$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'หมายเลขลูกค้า' );
				$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'กิจกรรม' );
				$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'ชื่อ - นามสกุล' );
				$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'หมู่บ้าน' );
				$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'บ้านเลขที่' );
				$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'หมู่ที่' );
				$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ตำบล' );
				$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'อำเภอ' );
				$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'จังหวัด' );
				$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'รหัสไปรษณีย์' );
				$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'เบอร์โทรศัพท์' );
				$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'เกรดลูกค้า' );
				$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'เซลล์' );
				$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'บริษัท' );
				$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'วันที่ทำกิจกรรม' );
				$objPHPExcel->getActiveSheet()->SetCellValue('P1', 'ชุดคำถามมาตรฐาน' );
				$objPHPExcel->getActiveSheet()->SetCellValue('X1', 'โทรซ้ำ' );

				$objPHPExcel->getActiveSheet()->SetCellValue('P2', 'ติดต่อได้' );
				$objPHPExcel->getActiveSheet()->SetCellValue('Q2', 'ติดต่อไม่ได้' );
				$objPHPExcel->getActiveSheet()->SetCellValue('R2', 'สะดวกคุย' );
				$objPHPExcel->getActiveSheet()->SetCellValue('S2', 'ไม่สะดวกคุย' );
				$objPHPExcel->getActiveSheet()->SetCellValue('T2', 'สนใจ' );
				$objPHPExcel->getActiveSheet()->SetCellValue('U2', 'ไม่สนใจ' );
				$objPHPExcel->getActiveSheet()->SetCellValue('V2', 'สาเหตุ' );
				$objPHPExcel->getActiveSheet()->SetCellValue('W2', 'หมายเหตุ' );
				$objPHPExcel->getActiveSheet()->SetCellValue('X2', 'วันที่โทร' );
				$objPHPExcel->getActiveSheet()->SetCellValue('Y2', 'วันที่โทรซ้ำ' );

				$last = end($result);
				$tabpos = array();
				$tabtype= array();
				$cols 	= 25;

				$sql 	= " 	SELECT 
								  follow_main_question.id
								, follow_main_question.question_title 
								, follow_question_list.id AS quest_list_id
								, follow_question_list.list_title
								, follow_question_list.order_number
								FROM follow_main_question 
								INNER JOIN follow_question_list ON follow_main_question.id = follow_question_list.main_question_id 
								WHERE follow_main_question.event_id_ref = '".$last['event_id']."'
								ORDER BY follow_main_question.id , follow_question_list.order_number
						";
				$res_list = mysql_query($sql);
				if (@mysql_num_rows($res_list)  > 0) {
					while($rss = mysql_fetch_assoc($res_list)){
						$objPHPExcel->getActiveSheet()->SetCellValue( gen_cell($cols).'2' , $rss['list_title'] );
						$objPHPExcel->getActiveSheet()->getColumnDimension( gen_cell($cols) )->setWidth('20');
						$objPHPExcel->getActiveSheet()->getStyle( gen_cell($cols) )->applyFromArray( array( 'fill' => array( 'type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '99ccff') ) ) ); 
						$objPHPExcel->getActiveSheet()->getStyle(gen_cell($cols).'1:'.gen_cell($cols).'2')->getFont()->setBold(true);
						$objPHPExcel->getActiveSheet()->getStyle(gen_cell($cols).'1:'.gen_cell($cols).'2')->applyFromArray( array( 'fill' => array( 'type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '77aaff') ) ) ); 		
						$tabtype[$rss['question_title']][] 	= gen_cell($cols);
						$tabpos[$rss['quest_list_id']] 		= gen_cell($cols);
						$objPHPExcel->getActiveSheet()->SetCellValue( $tabtype[$rss['question_title']][0].'1' , $rss['question_title'] );
						$objPHPExcel->getActiveSheet()->mergeCells( $tabtype[$rss['question_title']][0].'1:'.end($tabtype[$rss['question_title']]).'1');
						$cols++;
					}
				}

				$rows = 3;
				foreach($result as $keys => $rs){
					$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rows, $rs['CusNo']		);
					$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rows, $rs['event_title']	);
					$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rows, $rs['CusName']		);
					$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rows, $rs['ADDR_VILLAGE'] );
					##################################
					$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rows, $rs['ADDR_NUMBER'] );
					$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rows, $rs['ADDR_GROUP_NO'] );
					$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rows, $rs['ADDR_SUB_DISTRICT'] );
					$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rows, $rs['ADDR_DISTRICT'] );
					$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rows, $rs['ADDR_PROVINCE'] );
					$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rows, $rs['ADDR_POSTCODE'] );
					$objPHPExcel->getActiveSheet()->SetCellValue('K'.$rows, $rs['TEL_NUM'] );
					$objPHPExcel->getActiveSheet()->SetCellValue('L'.$rows, $rs['CUS_TYPE_LEVEL_DESC'] );
					$objPHPExcel->getActiveSheet()->SetCellValue('M'.$rows, $rs['EmpName'] );
					$objPHPExcel->getActiveSheet()->SetCellValue('N'.$rows, $rs['CompanyName'] );
					$objPHPExcel->getActiveSheet()->SetCellValue('O'.$rows, $rs['date_value'] );
					##################################
					if( $rs['follow_contact_status'] == '1' ){
						$objPHPExcel->getActiveSheet()->SetCellValue('P'.$rows, $rs['process_date'] );
					}else{
						$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rows, $rs['process_date'] );
					}
					##################################
					if( $rs['follow_convenient_status'] == '1' ){
						$objPHPExcel->getActiveSheet()->SetCellValue('R'.$rows, $rs['process_date'] );
					}else{
						$objPHPExcel->getActiveSheet()->SetCellValue('S'.$rows, $rs['process_date'] );
					}
					##################################
					if( $rs['follow_prospect_status'] == '1' ){
						$objPHPExcel->getActiveSheet()->SetCellValue('T'.$rows, $rs['process_date'] );
					}else{
						$objPHPExcel->getActiveSheet()->SetCellValue('U'.$rows, $rs['process_date'] );
					}
					##################################
					$objPHPExcel->getActiveSheet()->SetCellValue('V'.$rows, $rs['follow_contact_reason'] );
					$objPHPExcel->getActiveSheet()->SetCellValue('W'.$rows, $rs['follow_status_remark'] );
					##################################
					$objPHPExcel->getActiveSheet()->SetCellValue('X'.$rows, $rs['repeat_date'] );
					$objPHPExcel->getActiveSheet()->SetCellValue('Y'.$rows, $rs['repeat_time'] );
					$quest_list = explode(',', $rs['quest_list_id']);
					$answer 	= explode(',', $rs['answer']);
					if( count($quest_list[0]) ){
						foreach ($quest_list as $anspos => $qlis) {
							if( $tabpos[trim($qlis)] != '' ){
								$objPHPExcel->getActiveSheet()->SetCellValue( $tabpos[trim($qlis)].$rows, $answer[trim($anspos)] );
							}
						}
					}
					$rows++;
				}
			}
		}
		// download		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="รายงานรายละเอียดการดูแลลูกค้าหลังการขาย.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter( $objPHPExcel, 'Excel5' );
		$objWriter->save('php://output');
	break;
	case 'export_excel_old':
		$page_name = '/files/CSV_Files/Report_Follow';
		$path_file = dirname(__FILE__).$page_name;
		$name_file = 'รายงานการดูแลลูกค้าหลังการขาย_'.date('j_F_Y_His').'.xls';
		if (!file_exists($path_file)) {
		    mkdir($path_file, 0777, true);
		}

		include ("libraries/PHPExcel/PHPExcel.php");
		// สร้าง object ของ Class  PHPExcel  ขึ้นมาใหม่
		$objPHPExcel = new PHPExcel();

		// กำหนดค่าต่างๆ
		$objPHPExcel->getProperties()->setCreator("www.ideafunction.com");
		$objPHPExcel->getProperties()->setLastModifiedBy("www.ideafunction.com");
		$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
		$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
		$objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.");

		// เพิ่มข้อมูลเข้าใน Cell
		$objPHPExcel->setActiveSheetIndex(0);
		
		$ID_Card 		= ""; // เก็บ ID_Card ทั้งหมดของสายงาน เฉพาะหน้า OU
		$idPosition 	= ""; // เก็บ idPosition ทั้งหมดของสายงาน เฉพาะหน้า OU
		$GroupEven 		= "";
		$Sheet 			= 0;
		if ($_GET['page_value'] == 'OU') { $ID_Card = $_GET['id_TeamCommand']; $idPosition = $_GET['pc_TeamCommand']; }
		$arr_data 	= $cls_rp_followcus->getDataFollowCusJoinMainCusData( $_GET['idCom'], "", $_GET['idTeam'], "", $_GET['slEven'] , $_GET['StartDate'], $_GET['StopDate'], $ID_Card);
		//echo "<pre>".print_r($_GET)."</pre>";
		//#########################################  Sheet ALL ############################################
		// Add new sheet
	    $objWorkSheet = $objPHPExcel->createSheet($Sheet); //Setting index when creating
	    $Row = 4;
		foreach ($arr_data as $arr_excel) { // sheet กิจกรรม
			
	    	//----------------------- title -----------------------------------
	    	switch ( $_GET['page_value'] ) {
	    		case 'ALL':
	    			$title = "รายงานพนักงานในความรับผิดชอบ (ALL)"; 
	    		break;
	    		case 'working':
	    			$title = "รายงานพนักงานในความรับผิดชอบ (สาขา)"; 
	    		break;
	    		case 'OU':
	    			$title = "รายงานพนักงานในความรับผิดชอบ (OU)"; 
	    		break;
	    	}
	    	
	    	$objWorkSheet = $cls_rp_followcus->getExcel_Head($objWorkSheet, $title);
	    	$objWorkSheet->setTitle("ALL");
	    	$objPHPExcel->setActiveSheetIndex($Sheet)->mergeCells('P2:W2');
	    	$objPHPExcel->setActiveSheetIndex($Sheet)->mergeCells('X2:Y2');
	    	$objPHPExcel->getActiveSheet()
			    ->getStyle('A2:X3')
			    ->getAlignment()
			    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			//-----------------------end title -----------------------------------

			
			//----------------------- detail -----------------------------------
			$objWorkSheet->setCellValue('A'.$Row, $arr_excel['cus_no']);
		    $objWorkSheet->setCellValue('B'.$Row, $arr_excel['event_title']);
		    $objWorkSheet->setCellValue('C'.$Row, $arr_excel['CusFullName']);

		    $objWorkSheet->setCellValue('D'.$Row, $arr_excel['ADDR_VILLAGE']);
		    $objWorkSheet->setCellValue('E'.$Row, $arr_excel['ADDR_NUMBER']);
		    $objWorkSheet->setCellValue('F'.$Row, $arr_excel['ADDR_GROUP_NO']);


		    $objWorkSheet->setCellValue('G'.$Row, $arr_excel['ADDR_SUB_DISTRICT']);
		    $objWorkSheet->setCellValue('H'.$Row, $arr_excel['ADDR_DISTRICT']);
		    $objWorkSheet->setCellValue('I'.$Row, $arr_excel['ADDR_PROVINCE']);
		    $objWorkSheet->setCellValue('J'.$Row, $arr_excel['ADDR_POSTCODE']);
		    $objWorkSheet->setCellValue('K'.$Row, " ".$arr_excel['TEL_NUM']);
		    $objWorkSheet->setCellValue('L'.$Row, $arr_excel['ClassCus']);
		    $objWorkSheet->setCellValue('M'.$Row, $arr_excel['EmpFullName']);
		    $objWorkSheet->setCellValue('N'.$Row, $arr_excel['WorkName']);
		    $objWorkSheet->setCellValue('O'.$Row, $arr_excel['process_date']);		    

		    $objWorkSheet->setCellValue('P'.$Row, $arr_excel['follow_contact_status'] 		== 1 ? $arr_excel['historys_date']:'');
		    $objWorkSheet->setCellValue('Q'.$Row, $arr_excel['follow_contact_status'] 		== 2 ? $arr_excel['historys_date']:'');
		    $objWorkSheet->setCellValue('R'.$Row, $arr_excel['follow_convenient_status'] 	== 1 ? $arr_excel['historys_date']:'');
		    $objWorkSheet->setCellValue('S'.$Row, $arr_excel['follow_convenient_status'] 	== 2 ? $arr_excel['historys_date']:'');
		    $objWorkSheet->setCellValue('T'.$Row, $arr_excel['follow_prospect_status'] 		== 1 ? $arr_excel['historys_date']:'');
		    $objWorkSheet->setCellValue('U'.$Row, $arr_excel['follow_prospect_status'] 		== 2 ? $arr_excel['historys_date']:'');
		    $follow_contact_reason = "";
			    switch ($arr_excel['follow_contact_reason']) {
		    	case 1 : $follow_contact_reason = 'ไม่รับสาย'; 		break;
		    	case 2 : $follow_contact_reason = 'เบอร์โทรผิด'; 	break;
		    	case 3 : $follow_contact_reason = 'สายไม่ว่าง'; 	break;
		    	case 4 : $follow_contact_reason = 'ไม่มีสัญญาณ'; 	break;
		    	case 5 : $follow_contact_reason = 'อื่นๆ'; 			break;
		   		#default: $follow_contact_reason = 'อื่นๆ'; 			break;
		    }
		    $objWorkSheet->setCellValue('V'.$Row, $follow_contact_reason );
		    $objWorkSheet->setCellValue('W'.$Row, $arr_excel['follow_status_remark'] );
		    $objWorkSheet->setCellValue('X'.$Row, $arr_excel['date_value']);
		    $objWorkSheet->setCellValue('Y'.$Row, $arr_excel['time_value']);
			$Row++;
			//----------------------- end detail -----------------------------------

		}// end for	



		$Sheet++;
		foreach ($arr_data as $arr_excel) { // sheet กิจกรรม
		$sql_question = "SELECT
								follow_main_question.id AS id_follow_main_question,
								follow_main_question.question_title,
								follow_question_list.id AS id_follow_question_list,
								follow_question_list.list_title,
								follow_answer.id AS id_follow_answer,
								follow_answer.answer
							FROM $config[db_base_name].follow_main_question 
							LEFT JOIN $config[db_base_name].follow_question_list ON follow_main_question.id 		= follow_question_list.main_question_id
							LEFT JOIN $config[db_base_name].follow_answer
									 ON follow_question_list.id = follow_answer.ques_list_id
									AND follow_main_question.id = follow_answer.main_ques_id
									AND follow_main_question.event_id_ref = follow_answer.event_id_ref	
									AND follow_customer_historys_id = (
										SELECT MAX(follow_customer_historys.id_log)
										FROM $config[db_base_name].follow_customer_historys
										WHERE follow_customer_historys.ref_follow_id = $arr_excel[id_follow_customer]
									)													
							WHERE 1 = 1 
								  AND follow_main_question.event_id_ref = $arr_excel[follow_main_event_id]
								  AND follow_main_question.question_type = 0
							ORDER BY $config[db_base_name].follow_main_question.id, follow_question_list.id";						
			$res_question = mysql_query($sql_question);
			$arrQuestion  = array();
			$g_question   = "";
			$arr_question_list = array();
			if (@mysql_num_rows($res_question)  > 0) {
				while ($arr_question = mysql_fetch_assoc($res_question) ) {
					$arrQuestion[] = $arr_question;
					if ( $g_question != $arr_question['id_follow_question_list'] ) {
						$arr_question_list[] = $arr_question;
					}//end if
					$g_question = $arr_question['id_follow_question_list'];
				}//end while
			}
			//echo "<br><br><pre>".print_r($arr_question_list)."</pre>";

			if ($GroupEven != $arr_excel['follow_main_event_id']) {
				// Add new sheet
		    	$objWorkSheet = $objPHPExcel->createSheet($Sheet); //Setting index when creating
		    	//----------------------- title -----------------------------------
		    	$title = "รายงานพนักงานในความรับผิดชอบ กิจกรรม".$arr_excel['event_title']; 
		    	$objWorkSheet = $cls_rp_followcus->getExcel_Head($objWorkSheet, $title);
		    	$objPHPExcel->setActiveSheetIndex($Sheet)->mergeCells('P2:W2');
	    		$objPHPExcel->setActiveSheetIndex($Sheet)->mergeCells('X2:Y2');
		    	$objPHPExcel->getActiveSheet()
				    ->getStyle('A2:X3')
				    ->getAlignment()
				    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
				$arr_collum = array( 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK',
									 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX'
									 , 'AY', 'AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK'
									 , 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX'
									 , 'BY', 'BZ');				
				$group_question = "";
				$group_question_title =  "";
				$Column_merge = 0;		
				$arr_no  = 0;		
				if (count($arr_question_list)  > 0 ) {					
					foreach ($arr_question_list as $ar_question) {
						if($group_question != $ar_question['id_follow_main_question']){
						 	$Row = 2;							
						 	$objWorkSheet->setCellValue($arr_collum[$arr_no].$Row, $ar_question['question_title']);						
						  	$Column_merge = $arr_no;
						}else{
							$Row = 2;
							$objPHPExcel->setActiveSheetIndex($Sheet)->mergeCells( $arr_collum[$Column_merge].$Row.':'.$arr_collum[$arr_no].$Row );
								$objPHPExcel->getActiveSheet()
							    ->getStyle( $arr_collum[$Column_merge].$Row.':'.$arr_collum[$arr_no].$Row )
							    ->getAlignment()
							    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}
						

						 $group_question = $ar_question['id_follow_main_question'];
						$Row = 3;
						$objWorkSheet->setCellValue($arr_collum[$arr_no].$Row, $ar_question['list_title']);	
						//$objPHPExcel->getActiveSheet()->getStyle($arr_collum[$arr_no].$Row)->getAlignment()->setWrapText(true);							
						
					$arr_no++;
					$arr_question_list = null;
					}//end for	
				}//end if			
			
				//-----------------------end title -----------------------------------
				$Row = 4;
				
		    	$objWorkSheet->setTitle("กิจกรรมที่ ".$Sheet);
		    	$Sheet++;
			}
			$GroupEven = $arr_excel['follow_main_event_id'];


			//----------------------- detail -----------------------------------
			$objWorkSheet->setCellValue('A'.$Row, $arr_excel['cus_no']);
		    $objWorkSheet->setCellValue('B'.$Row, $arr_excel['event_title']);
		    $objWorkSheet->setCellValue('C'.$Row, $arr_excel['CusFullName']);

		    $objWorkSheet->setCellValue('D'.$Row, $arr_excel['ADDR_VILLAGE']);
		    $objWorkSheet->setCellValue('E'.$Row, $arr_excel['ADDR_NUMBER']);
		    $objWorkSheet->setCellValue('F'.$Row, $arr_excel['ADDR_GROUP_NO']);


		    $objWorkSheet->setCellValue('G'.$Row, $arr_excel['ADDR_SUB_DISTRICT']);
		    $objWorkSheet->setCellValue('H'.$Row, $arr_excel['ADDR_DISTRICT']);
		    $objWorkSheet->setCellValue('I'.$Row, $arr_excel['ADDR_PROVINCE']);
		    $objWorkSheet->setCellValue('J'.$Row, $arr_excel['ADDR_POSTCODE']);
		    $objWorkSheet->setCellValue('K'.$Row, " ".$arr_excel['TEL_NUM']);
		    $objWorkSheet->setCellValue('L'.$Row, $arr_excel['ClassCus']);
		    $objWorkSheet->setCellValue('M'.$Row, $arr_excel['EmpFullName']);
		    $objWorkSheet->setCellValue('N'.$Row, $arr_excel['WorkName']);
		    $objWorkSheet->setCellValue('O'.$Row, $arr_excel['process_date']);	   
		   	$objWorkSheet->setCellValue('P'.$Row, $arr_excel['follow_contact_status'] 		== 1 ? $arr_excel['historys_date']:'');
		    $objWorkSheet->setCellValue('Q'.$Row, $arr_excel['follow_contact_status'] 		== 2 ? $arr_excel['historys_date']:'');
		    $objWorkSheet->setCellValue('R'.$Row, $arr_excel['follow_convenient_status'] 	== 1 ? $arr_excel['historys_date']:'');
		    $objWorkSheet->setCellValue('S'.$Row, $arr_excel['follow_convenient_status'] 	== 2 ? $arr_excel['historys_date']:'');
		    $objWorkSheet->setCellValue('T'.$Row, $arr_excel['follow_prospect_status'] 		== 1 ? $arr_excel['historys_date']:'');
		    $objWorkSheet->setCellValue('U'.$Row, $arr_excel['follow_prospect_status'] 		== 2 ? $arr_excel['historys_date']:'');
		    $follow_contact_reason = "";
			    switch ($arr_excel['follow_contact_reason']) {
		    	case 1 : $follow_contact_reason = 'ไม่รับสาย'; 		break;
		    	case 2 : $follow_contact_reason = 'เบอร์โทรผิด'; 	break;
		    	case 3 : $follow_contact_reason = 'สายไม่ว่าง'; 	break;
		    	case 4 : $follow_contact_reason = 'ไม่มีสัญญาณ'; 	break;
		    	case 5 : $follow_contact_reason = 'อื่นๆ'; 			break;
		   		#default: $follow_contact_reason = 'อื่นๆ'; 			break;
		    }
		    $objWorkSheet->setCellValue('V'.$Row, $follow_contact_reason );
		    $objWorkSheet->setCellValue('W'.$Row, $arr_excel['follow_status_remark'] );
		    $objWorkSheet->setCellValue('X'.$Row, $arr_excel['date_value']);
		    $objWorkSheet->setCellValue('Y'.$Row, $arr_excel['time_value']);

		    $arr_no = 0;

		    if (count($arrQuestion)  >0 ) {
		    	foreach ($arrQuestion as $ar_question) {
			    	$objWorkSheet->setCellValue($arr_collum[$arr_no].$Row,str_replace("'", "", $ar_question['answer']) );								
					$arr_no++;
		    	}
		    }
		   
			$Row++;
			//----------------------- end detail -----------------------------------

		}// end for	

//exit();
		// บันทึกไฟล์ Excel 2007
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save($path_file.'/'.$name_file); //ชื่อไฟล์ เมื่อมีการเรียกไฟล์นี้ทำงานก็จะทำการสร้าง ไฟล์ไว้ใน ตำแหน่งของที่กำหนดชื่อไฟล์

		echo dirname($_SERVER['PHP_SELF']).$page_name.'/'.$name_file;
		
		// // header("Content-Type: application/vnd.ms-excel");
		// // header('Content-Disposition: attachment; filename="'.date('j_F_Y').'_ข้อมูลลูกค้า.xls"');#ชื่อไฟล
		// // echo '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"
		// // xmlns="http://www.w3.org/TR/REC-html40"><HTML><HEAD><meta http-equiv="Content-type" content="text/html;charset=UTF-8" /></HEAD><BODY>';
		// echo $tpl->tbHtml($thisPage.'.html', 'Table_ExportExcel_title');	

		break;
	
	case 'Search':
		require_once("classes/PreLoaderMultiAjax.class.php"); //$_GET['idCom'] = 44;
		if($_GET['type'] != 'Export_excel') {  $loading = new PreLoaderMultiAjax(); }
		$arr_return 	= array();//arr ที่จะส่งกลับให้ javascript
		$strHTML 		= ""; // เก็บค่า HTML
		$GroupWork		= ""; // เก็บค่าบริษัทที่ไม่ซำ้กัน
		$GroupDeparment	= ""; // เก็บค่าแผนกที่ไม่ซำ้กัน
		$GroupSection	= ""; // เก็บค่าฝ่ายที่ไม่ซำ้กัน
		$GroupPosition	= ""; // เก็บค่าตำแหน่งที่ไม่ซำ้กัน
		$ID_Card 		= ""; // เก็บ ID_Card ทั้งหมดของสายงาน เฉพาะหน้า OU
		$idPosition 	= ""; // เก็บ idPosition ทั้งหมดของสายงาน เฉพาะหน้า OU

		if ($_GET['page_value'] == 'OU') { $ID_Card = $_GET['id_TeamCommand']; $idPosition = $_GET['pc_TeamCommand']; }
		if($_GET['type'] != 'Export_excel') {  $loading->setLoopMax(0,100); }// set ค่าให้กำ progesspar
		$arr_follow 	= $cls_rp_followcus->getDataFollowCus( $_GET['idCom'], "", $_GET['idTeam'], "", $_GET['slEven'] , $_GET['StartDate'], $_GET['StopDate'], $ID_Card);
		if($_GET['type'] != 'Export_excel') { $loading->eachTell(10,100); }
		if(count($arr_follow) > 0){			
			$arr_ou 		= $cls_rp_followcus->getTeam_OU( $_GET['idCom'], $_GET['idTeam'], $ID_Card, $idPosition);	if($_GET['type'] != 'Export_excel') {  $loading->eachTell(15,100); }	
			$arr_clCustomer = $cls_rp_followcus->getColunmCustomer( $arr_follow );										if($_GET['type'] != 'Export_excel') {  $loading->eachTell(20,100); }//ลูกค้า (ราย)
			$arr_clEven 	= $cls_rp_followcus->getColunmEven( $arr_follow );											if($_GET['type'] != 'Export_excel') {  $loading->eachTell(30,100); }//กิจกรรม 
			$arr_clWaitEven	= $cls_rp_followcus->getColunmWaitEven( $arr_follow ); 										if($_GET['type'] != 'Export_excel') {  $loading->eachTell(35,100); }//กิจกรรม
			$arr_clAllEven  = $cls_rp_followcus->getColunm_Complete_Expire_Cancel_WaitCancel_Interested( $arr_follow );	if($_GET['type'] != 'Export_excel') {  $loading->eachTell(40,100); } //ทำแล้ว หมดอายุ ยกเลิก รอยกเลิก สนใจ  ไม่สนใจ
			//echo " arr_ou <pre>";print_r($arr_ou)."</pre>"; exit();			
			for ($i=0; $i < count($arr_ou); $i++) {
				$work_name 		= $arr_ou[$i]['WorkName'];//." id >> ".$arr_ou[$i]['WorkCompany'];
				$DeparmentName 	= $arr_ou[$i]['DeparmentName'];//." id >> ".$arr_ou[$i]['Department'];
				$SectionName	= $arr_ou[$i]['SectionName'];//." id >> ".$arr_ou[$i]['Section'];
				$PositionName 	= $arr_ou[$i]['PositionName']." (".$arr_ou[$i]['PositionCode'].")";//." id >> ".$arr_ou[$i]['Position'];
				$EMP_Fullname	= $arr_ou[$i]['EMP_Fullname'];//." id >> ".$arr_ou[$i]['ID_Card'];				

				if($GroupWork != $arr_ou[$i]['WorkCompany']){ //ถ้า บริษัทไม่บริษัทเดิมให้ แสดง บริษัทใหม่
					$strHTML 	.= $cls_rp_followcus->CrateHTML_Working($work_name, $arr_clCustomer, $arr_clEven, $arr_clWaitEven, $arr_clAllEven, $arr_ou[$i]['WorkCompany']);		
					$str_tfoot 	 = $cls_rp_followcus->CrateHTML_tFoot($work_name, $arr_clCustomer, $arr_clEven, $arr_clWaitEven, $arr_clAllEven, $arr_ou[$i]['WorkCompany']);		
				}//end else GroupWork
				$GroupWork = $arr_ou[$i]['WorkCompany'];

				if($GroupDeparment != $arr_ou[$i]['Department']){ //ถ้า บริษัทไม่แผนกเดิมให้ แสดง แผนกใหม่
					$strHTML .= $cls_rp_followcus->CrateHTML_Department($DeparmentName, $arr_clCustomer, $arr_clEven, $arr_clWaitEven, $arr_clAllEven, $arr_ou[$i]['WorkCompany'] ,$arr_ou[$i]['Department']);
				}//end else GroupDeparment
				$GroupDeparment = $arr_ou[$i]['Department'];

				if($GroupSection != $arr_ou[$i]['Section']){ //ถ้า บริษัทไม่ฝ่ายหรือทีมเดิมให้ แสดง ฝ่ายหรือทีมใหม่
					$strHTML .= $cls_rp_followcus->CrateHTML_Section($SectionName, $arr_clCustomer, $arr_clEven, $arr_clWaitEven, $arr_clAllEven, $arr_ou[$i]['WorkCompany'] ,$arr_ou[$i]['Department'], $arr_ou[$i]['Section']);
				}//end else GroupSection
				$GroupSection = $arr_ou[$i]['Section']; 
				
				if($GroupPosition != $arr_ou[$i]['Position']){ //ถ้า บริษัทไม่ตำแน่งหรือทีมเดิมให้ แสดง ตำแน่งหรือทีมใหม่
					$strHTML .= $cls_rp_followcus->CrateHTML_Position($PositionName, $arr_clCustomer, $arr_clEven, $arr_clWaitEven, $arr_clAllEven, $arr_ou[$i]['WorkCompany'] ,$arr_ou[$i]['Department'], $arr_ou[$i]['Section'], $arr_ou[$i]['Position']);										
				}//end else GroupPosition
				$GroupPosition = $arr_ou[$i]['Position'];
						
					 
				$strHTML .= $cls_rp_followcus->CrateHTML_Employee($EMP_Fullname, $arr_clCustomer, $arr_clEven, $arr_clWaitEven, $arr_clAllEven, $arr_ou[$i]['WorkCompany'] ,$arr_ou[$i]['Department'], $arr_ou[$i]['Section'], $arr_ou[$i]['Position'], $arr_ou[$i]['ID_Card']);
			}//end for
			
			


			# ตอบกลับ progressbar
			//if($eachTell%1 == 0){
			if($_GET['type'] != 'Export_excel') { 
				$persend_loadmore = ($i * 100 ) /60;
				$loading->eachTell($persend_loadmore ,100);
			}
			//}
			# --
		}//end if count  arr_follow

		$arr_follow 		= null;
		$arr_ou 			= null;
		$arr_clCustomer 	= null;
		$arr_clEven 		= null;
		$arr_arr_clWaitEven = null;
		$arr_clAllEven		= null;

		if($_GET['type'] != 'Export_excel') { $loading->eachTell(100,0); }
		$arr_return['strHTML'] 		= $strHTML;
		if ($strHTML == '') { // กรณีที่ไม่พบข้อมูล
			$arr_return['strHTML'] 		= $tpl->tbHtml($thisPage.'.html','Table_IsnullData');
			$arr_return['str_tfoot'] 	= "";
		}else{//กรณีที่มีข้อมูล
			$arr_return['str_tfoot'] 	= $str_tfoot;	
		}
		
		if($_GET['type'] == 'Export_excel') {
			header("Content-Type: application/vnd.ms-excel");
			header('Content-Disposition: attachment; filename="รายงานการดูแลลูกค้าหลังการขาย_'.date('j_F_Y').'.xls"');#ชื่อไฟล
			echo '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"
			xmlns="http://www.w3.org/TR/REC-html40"><HTML><HEAD><meta http-equiv="Content-type" content="text/html;charset=UTF-8" /></HEAD><BODY>';
			
			$strHTML_tb  = '<table id="tbMain" class="tb_main" cellspacing="0" cellpadding="2">';
			$strHTML_tb .= $tpl->tbHtml($thisPage.'.html','head_table'); 
			$strHTML_tb .= $arr_return['strHTML'];
			$strHTML_tb .= $arr_return['str_tfoot'];
			$strHTML_tb .= '<table>';
			echo($strHTML_tb);
		}else{
			echo json_encode($arr_return);
			$loading->endPreLoad(); # ปิด preload ** ต้องเอาไว้หลังจาก echo หรือเอาไว้สุดท้ายเลย
		}
	
	break;

	case 'getTeam':
		echo $cls_rp_followcus->get_slTeam($_GET['idCom']);
	break;

	case 'getEven':
		echo $cls_rp_followcus->get_slEven($_GET['idCom']);
	break;

	default:
		$slEven 		= $cls_rp_followcus->get_slEven();
		$page_value		= $_GET['page'];
		$head_table		= $tpl->tbHtml($thisPage.'.html','head_table'); 

		switch ($_GET['page']) {
			case 'ALL':				 
				$page_name		= "(ALL)";
				$slCompany 		= $cls_rp_followcus->get_slCompany();
				$CompanyTitle   = $tpl->tbHtml($thisPage.'.html','Table_title_companyAll');				
				$strHtml 		= $tpl->tbHtml($thisPage.'.html','MainBody');
				break;
			case 'working':
				$page_name		= "(สาขา)";
				$idCompany 		= $_SESSION['SESSION_Working_Company'];
				$arr_Company    = $cls_rp_followcus->get_NameCompany($idCompany);
				$CompanyName 	= $arr_Company['name'];
				$slTeam 		= $cls_rp_followcus->get_slTeam($idCompany);
				$CompanyTitle   = $tpl->tbHtml($thisPage.'.html','Table_title_company');
				$strHtml 		= $tpl->tbHtml($thisPage.'.html','MainBody');
				break;
			case 'OU':
				$page_name		= "(OU)";
				$idCompany 		= $_SESSION['SESSION_Working_Company'];
				$arr_Company    = $cls_rp_followcus->get_NameCompany($idCompany); 

				$command_array = $cls_rp_followcus->getTeamCommand($_SESSION['SESSION_Position_id'],2);
				//$command_array = $cls_rp_followcus->getTeamCommand('1012',2);
				$position_id  = implode(',',$command_array['position']);
				if($position_id  == ''){ $command_array['position'] = "''"; }

				$id_card  = implode(',',$command_array['id_card']);
				if($id_card  == ''){ $command_array['id_card'] = "''"; }
				$sql 	= "SELECT PositionCode, Department, Section FROM $config[db_organi].`position` WHERE `id` IN (" . $position_id . ") ";
				$result = mysql_query($sql) or die ('<br>'.mysql_error() .'<br>'. $sql );
				while($emp_row = mysql_fetch_assoc($result)){
					$array_position_code[] 	= $emp_row['PositionCode'];
					$array_Section[] 	= $emp_row['Section'];	
				}

				$position_pc  = implode(',',$array_position_code);
				if($position_pc  == ''){ $array_position_code = "''"; }

				$Section_id  = implode(',',$array_Section);
				if($Section_id  == ''){ $array_Section = "''"; }

				$id_TeamCommand	= $id_card;
				$pc_TeamCommand = $position_pc;
			
				$CompanyName 	= $arr_Company['name'];
				$CompanyID		= $arr_Company['id'];
				$slTeam 		= $cls_rp_followcus->get_slTeam($idCompany, $Section_id);
				$CompanyTitle   = $tpl->tbHtml($thisPage.'.html','Table_title_company');	
				$strHtml 		= $tpl->tbHtml($thisPage.'.html','MainBody');						
				break;
		}
		echo $strHtml;

		break;
}
CloseDB();

?>