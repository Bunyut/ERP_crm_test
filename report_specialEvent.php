<?php
require_once 'config/general.php';
require_once("function/general.php");
require_once("function/report_specialEvent.php");
require_once("function/ic_followcus.php");
require_once("helper/get_ou_data.php");

Conn2DB();
$Q_Event	= '62'; //รหัส กิจกรรมพิเศษ
$Q_Main 	= '48'; //รหัส คำถามหลัก
$Q_List 	= array('236','237'); //รหัส รายการคำถาม

//$start_month = (date('Y')+543).date('m');
//$start_month = "255307";
$no_ = 1;

if(isset($_GET['beginDate']) && isset($_GET['endDate'])){
	$start_date = $_GET['beginDate'];
	$stop_date  = $_GET['endDate'];
	$beginTemp = explode('-',$_GET['beginDate']);
	$endTemp   = explode('-',$_GET['endDate']);
	$begin = $beginTemp[2].'-'.$beginTemp[1].'-'.$beginTemp[0];
	$end   = $endTemp[2].'-'.$endTemp[1].'-'.$endTemp[0];

	$sql_event =  "SELECT stop_value, stop_unit FROM $config[db_easysale].follow_main_event WHERE id = '$Q_Event'";
	$res_event = mysql_query($sql_event)or die(' error on query stopdate in main event table Line : '.__LINE__);
	$fet_event = mysql_fetch_assoc($res_event);
	
	$real_begin = getAfterDate($begin, $fet_event['stop_value'], $fet_event['stop_unit']);
	$real_end   = getAfterDate($end  , $fet_event['stop_value'], $fet_event['stop_unit']);
	$whereDate = " AND follow_customer.stop_date BETWEEN '$real_begin' AND '$real_end'  ";
}else{
	$whereDate = '';
}

# หากิจกรรมตนเอง และลูกน้อง
$SESSION_ID_card 			= $_SESSION['SESSION_ID_card'];
$SESSION_Position_id 		= $_SESSION['SESSION_Position_id'];
$SESSION_Working_Company	= $_SESSION['SESSION_Working_Company'];
$SESSION_Department 		= $_SESSION['SESSION_Department'];
$SESSION_Section 			= $_SESSION['SESSION_Section'];

$arrCheck = haveFollower($SESSION_Position_id,$SESSION_Working_Company,$SESSION_Department,$SESSION_Section);

/* print_r($arrCheck);echo "$SESSION_Position_id,$SESSION_Working_Company,$SESSION_Department,$SESSION_Section<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<";
exit(); */

if($_SESSION['SESSION_username'] != 'admin'){// ถ้าไม่ใช่ admin จะมองเห็นแต่ของตัวเอง และลูกน้อง
	$ID_EMP = $_SESSION['SESSION_ID_card'];
	$whereadmin = '';
}else{
	$ID_EMP = '%';
	$whereadmin = " AND SaleLevel = 'BigSupervisor' ";
}

//หากิจกรรมตนเอง และลูกน้อง
$sql_emp = "SELECT id, id_card, Name_Sername, SaleLevel FROM $config[db_easysale].SaleTemp WHERE status != '99' AND start_month = '$start_month' AND id_card LIKE '%$ID_EMP%' $whereadmin";
$res_emp = mysql_query($sql_emp)or die('error file specailEvent Line:'.__LINE__." error:".mysql_error());
while($fet_emp = mysql_fetch_assoc($res_emp)){
	//ลูกค้าที่ จะทำกิจกรรม  ถ้าไม่ กรุป ก็สามารถแสดง ทุก follow_cus ของลูกค้าคนนั้นได้เลย  //JOIN $config[db_easysale].follow_answer ON follow_customer.id = follow_answer.follow_customer_id
	$sql_fc = "SELECT DISTINCT(follow_customer.cus_no), follow_customer.id, follow_customer.emp_id_card,emp_data.Name, emp_data.Surname, emp_data.Nickname
, follow_customer.cus_name,  follow_customer.source_db_table, follow_customer.source_primary_field
, follow_customer.source_primary_id, follow_customer.stop_date, follow_customer.process_date, follow_customer.id, follow_customer.status 
FROM $config[db_easysale].follow_customer JOIN $config[db_emp].emp_data 
ON follow_customer.emp_id_card = emp_data.ID_card 
WHERE follow_customer.event_id_ref = '$Q_Event' AND follow_customer.emp_id_card = '$fet_emp[id_card]' AND follow_customer.source_db_table != '' AND follow_customer.status < '90' $whereDate   
ORDER BY follow_customer.stop_date, follow_customer.cus_no";
	$result_fc = mysql_query($sql_fc) or die(mysql_error()." <br>".__FILE__.' line '.__LINE__.'<br>'.$sql_fc);
	$num_fc = mysql_num_rows($result_fc);

	while($fet_fc = mysql_fetch_assoc($result_fc)){
		$emp_name = $BDate = $AMPR = $date_stop = $ProcessDate = $mrk1 = $mrk2 = $mrk3 = $mrk4 = $mrk5 = '';

		//หา อำเภอ ลูกค้า
		$cus_Data = getMainCusDataFormCusNo($fet_fc['cus_no'],null,'C_Amp');
		$expC = explode("_",$cus_Data['C_Amp']);
		if($cus_Data['C_Amp'] == ''){
			$cus_Data = getMainCusDataFormCusNo($fet_fc['cus_no'],null,'Cus_Amp');
			$expC = explode("_",$cus_Data['Cus_Amp']);
		}
		$AMPR = $expC[1];

		//หาวันที่จอง
		$sql_b_date = "SELECT Booking_Date FROM $fet_fc[source_db_table] WHERE $fet_fc[source_primary_field] = '$fet_fc[source_primary_id]'";   ///****
		$res_b_date = mysql_query($sql_b_date)or die(mysql_error().' file specialEvent Line: '.__LINE__);
		$fet_b_date = mysql_fetch_assoc($res_b_date);

		if($fet_b_date['Booking_Date']){
			$expd = explode(" ",$fet_b_date['Booking_Date']);
			$BDate0 = standard2thaidate($expd[0], '-');
			$BDate = "<div title='$BDate0[text]'>$BDate0[number]</div>";
		}else{
			$BDate = '-';
		}
		//ชื่อพนักงาน
		$emp_name = $fet_fc[Name].' '.$fet_fc[Surname].' ('.$fet_fc[Nickname].')';

		//หาวันที่ครบกำหนด
		$sql_event =  "SELECT stop_value, stop_unit FROM $config[db_easysale].follow_main_event WHERE id = '$Q_Event'";
		$res_event = mysql_query($sql_event)or die(' error on query stopdate in main event table Line : '.__LINE__);
		$fet_event = mysql_fetch_assoc($res_event);
		$real = getBeforeDate($fet_fc['stop_date'], $fet_event['stop_value'], $fet_event['stop_unit']);
		$real_date = standard2thaidate($real, '-');
		$real_date = "<div title='$real_date[text]'>$real_date[number]</div>"; //วันที่ครบกำหนดของเหตุการณ์

		//วันที่ทำรายการ
		if($fet_fc['status'] == 2){
			$exppd = explode(" ",$fet_fc['process_date']);
			$exppd2 = explode("-",$exppd[0]);
			$ProcessDate0 = standard2thaidate($exppd[0], '-');
			$ProcessDate = "<div title='$ProcessDate0[text]'>$ProcessDate0[number]</div>";
		}else{
			$ProcessDate = "-";
		}

		//หาคำตอบของลูกค้า
		for($i=0 ; $i<count($Q_List); $i++){
			$sql_ans = "SELECT ques_list_id, answer FROM $config[db_easysale].follow_answer
			WHERE ques_list_id = '$Q_List[$i]' AND cus_no = '$fet_fc[cus_no]' AND status != '99' AND follow_customer_id = '$fet_fc[id]' 
			ORDER BY id DESC";
			$res_ans = mysql_query($sql_ans)or die(mysql_error().' file: '.__FILE__.'Line: '.__LINE__);
			$fet_ans = mysql_fetch_assoc($res_ans);

			if($fet_ans['ques_list_id'] == '236'){
				if(str_replace("'","",$fet_ans['answer']) == 'ใช่'){
					$mrk1 = '<img src="img/marks_blue.png" height = "20px">';
				}else if(str_replace("'","",$fet_ans['answer']) == 'ไม่ใช่'){
					$mrk2 = '<img src="img/marks_blue.png" height = "20px">';
				}else if(str_replace("'","",$fet_ans['answer']) == 'ติดต่อไม่ได้'){
					$mrk3 = '<img src="img/marks_blue.png" height = "20px">';
				}
			}else if($fet_ans['ques_list_id'] == '237'){
				if(str_replace("'","",$fet_ans['answer']) == 'ใช่'){
					$mrk4 = '<img src="img/marks_blue.png" height = "20px">';
				}else if(str_replace("'","",$fet_ans['answer']) == 'ไม่ใช่'){
					$mrk5 = '<img src="img/marks_blue.png" height = "20px">';
				}
			}
		}

	if($no_%2 == 1){
			$STYLE = '  #F0FFF0 ';
		}else{
			$STYLE = '  #C1FFC1 ';
		}
		$list_ .= $tpl->tbHtml('report_specialEvent.html','LISTS');
		$no_++;
	}

	//เช็ตคว่า มีลูก รึ ปล่าว
	if($fet_emp['SaleLevel'] == 'Supervisor'){//เข้าไปหารายการของลูก
		$list_ = specialEventSup($no_,$list_,$fet_emp['id'],$whereDate);
	}else

	if($fet_emp['SaleLevel'] == 'BigSupervisor'){//ถ้าตัวเองเป็น บิ๊กให้เรียกฟังก์ชันเข้าไปหารายการของลูก
		$list_ = specialEventBig($no_,$list_,$fet_emp['id'],$whereDate);
	}
}
$thheadtable .= $tpl->tbHtml('report_specialEvent.html','COLUMN');

$mainSpecial .= $tpl->tbHtml('report_specialEvent.html','DETAILS');

echo $tpl->tbHtml('report_specialEvent.html','MAINEVENT');


CloseDB();
?>
