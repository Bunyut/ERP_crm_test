<?php
/**
 * SQ Form - จัดการส่งค่าความพึงพอใจไปให้TIS
 * create by   :PETE@2011-11-05
 * Last Modify : pt@2011-12-21 เลือกโหลดเป็น record ได้
 * Last Modify : PT@2012-07-25 IT10  เปลี่ยนตัวเทียบทั้งไฟล์  DN_NO  ไปเป็น  VinNo
 *
 * รอเขียนใหม่ PT เขียนโค้ดให้สั้นกว่านี้และเข้าใจง่ายกว่านี้
 *
 * JAVA pete.js
 */

require_once("config/general.php");
require_once("function/general.php");
require_once("function/manage_SQ_form.php");

$thisPage 	= "manage_SQ_form";

## สั่งทำงานโค้ด
$doCode = ""; 		## ทำงาน
// $doCode = "No";  ## ไม่ทำงาน แสดงโค้ด Insert, Update, Delete
// $doCode = "All"; ## ทำงาน แสดงโค้ด Select, Insert, Update, Delete

Conn2DB();
mysql_query( "SET NAMES UTF8" );

//OU reqiure
// require_once("helper/get_ou_data.php");
// $permission = check_access_permission($id_card=null, $position_id=null);
//

session_write_close();

$today_date = date('Y-m-d');
//style ปุ่ม
$BTNtab1 = 'BTsnow boldGreen';
$BTNtab2 = 'BTosnow boldGreen';
$BTNtab3 = 'BTosnow boldGreen';

$NumLIMIT = '500';
$LIMIT_WHERE = " LIMIT $NumLIMIT " ;

if(isset($_GET['del_ListFixCode'])){ // ลบรหัสคำถาม ที่แทรกอยู่ใน list_question_id
	$position = $_GET['del_ListFixCode'];
	$sql_findDel = "SELECT id,list_question_id, Date_Setup FROM $config[db_base_name].D_NET WHERE Type_Status = '90' ORDER BY id DESC LIMIT 1";
	$res_findDel = $logDb->queryAndLogSQL( $sql_findDel, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	$findDel = mysql_fetch_assoc($res_findDel);
	
	$expeach = explode(",",$findDel[list_question_id]);
	$newListQuestion = '';
	for($i=0;$i<count($expeach);$i++){
		$n=$i+1;
		if($n != $position){
			if($newListQuestion == ''){
				$newListQuestion .= $expeach[$i];
			}else{
				$newListQuestion .= ','.$expeach[$i];
			}
		}else{
			$newListQuestion .= '';
			$delwhat = $expeach[$i];
		}
	}
	
	$date_del = $findDel[Date_Setup].',del '.$delwhat.' on '.$today_date;
	$sql_updListQ = "UPDATE $config[db_base_name].D_NET SET list_question_id = '$newListQuestion', Date_Setup = '$date_del' WHERE id = '$findDel[id]'";
	$logDb->queryAndLogSQL( $sql_updListQ, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	//จบกระบวนการแล้วให้ลงไปเข้า CASE ELSE{} สุดท้าย
}

$func = $_REQUEST['func'];

switch($func){
	default:
		$max_idCode = 1;
		$id_code = 'inp_'.$max_idCode;
		$result_thisCode = 'result_thisCode_'.$max_idCode;

		//เชคดูการตั้งค่าล่าสุด
		$sql_dNet_listquestion = "SELECT list_question_id, Date_Setup FROM $config[db_base_name].D_NET WHERE Type_Status = '90' ORDER BY id DESC LIMIT 1";
		$res_listQuestion = $logDb->queryAndLogSQL( $sql_dNet_listquestion, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$num_rowListQ = mysql_num_rows($res_listQuestion);

		if($num_rowListQ){ //ถ้าเคยเซตค่าหรัสไว้ก่อนแล้ว จะดึงขึ้นมาโชว์ในหน้าตั้งค่า
			$listQuestion = mysql_fetch_assoc($res_listQuestion);
			//pt@2011-12-10 ------------------------------------
			$listQuest = explode(" ",$listQuestion[Date_Setup]);
			$x_dmY = explode("-",$listQuest[(count($listQuest)-1)]);
			$listQuestiondmY = $x_dmY[(count($x_dmY)-1)].'/'.$x_dmY[(count($x_dmY)-2)].'/'.$x_dmY[(count($x_dmY)-3)];
			//end pt@2011-12-10 ------------------------------------
			$indivCode = explode(",",$listQuestion['list_question_id']);
			for($i=0;$i<count($indivCode);$i++){
				$max_idCode = $i+1;
				$id_code = 'inp_'.$max_idCode;
				$result_thisCode = 'result_thisCode_'.$max_idCode;
				$expFixCode = explode("=",$indivCode[$i]);

				//เริ่มหาจากคำถามย่อย quetion list
				$sql_quetion_list1 = "SELECT main_question_id FROM $config[db_base_name].follow_question_list WHERE id = '$expFixCode[1]'";
				$res_quest1 = $logDb->queryAndLogSQL( $sql_quetion_list1, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$quest1 = mysql_fetch_assoc($res_quest1);
				
				$sql_quetion_list2 = "SELECT list_title,id FROM $config[db_base_name].follow_question_list WHERE main_question_id = '$quest1[main_question_id]' AND status!=99 ORDER BY id ASC";
				$res_quest2 = $logDb->queryAndLogSQL( $sql_quetion_list2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				
				$list_ques = '<select disabled="" style="width:300px" id="se_Levent'.$max_idCode.'"  onchange="selectLevent(\''.$max_idCode.'\');"><option value="0" >-- กรุณาเลือกคำถามย่อย --</option>';
				while($quest2 = mysql_fetch_assoc($res_quest2)){
					if($quest2[id] == $expFixCode[1]){
						$list_ques .= '<option value="'.$quest2[id].'"  selected="">'.$quest2[list_title].'</option>';
					}else{
						$list_ques .= '<option value="'.$quest2[id].'" >'.$quest2[list_title].'</option>';
					}
				}
				$list_ques .= '</select>';
				
				//หาคำถามหลักต่อ โดยใช้ $quest1[main_question_id]
				$sql_mainquestion1 = "SELECT event_id_ref FROM $config[db_base_name].follow_main_question WHERE id = '$quest1[main_question_id]'";
				$res_main1 =  $logDb->queryAndLogSQL( $sql_mainquestion1, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$main1 = mysql_fetch_assoc($res_main1);
				
				$sql_mainquestion2 = "SELECT question_title,id FROM $config[db_base_name].follow_main_question WHERE event_id_ref = '$main1[event_id_ref]' AND status != '99' ORDER BY id ASC";
				$res_main2 = $logDb->queryAndLogSQL( $sql_mainquestion2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				
				$main_ques = '<select disabled="" style="width:270px" id="se_Mevent'.$max_idCode.'" class ="textCenter" onchange="selectMevent(\''.$max_idCode.'\');"><option value="0" >-- กรุณาเลือกคำถามหลัก --</option>';
				while($main2 = mysql_fetch_array($res_main2)){
					if($main2[id] == $quest1[main_question_id]){
						$main_ques .= '<option value="'.$main2[id].'" selected="">'.$main2[question_title].'</option>';
					}else{
						$main_ques .= '<option value="'.$main2[id].'" >'.$main2[question_title].'</option>';
					}
				}
				$main_ques .= '</select> =>';
				
				//หา อีเว้นท์ ต่ออีก
				$sql_event = "SELECT id, event_title, department FROM $config[db_base_name].follow_main_event WHERE status != '99' ORDER BY id ASC";
				$resEvent = $logDb->queryAndLogSQL( $sql_event, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$e_option ='<option value="0">-- กรุณาเลือกกิจกรรม --</option>';
				while($feEvent = mysql_fetch_array($resEvent)){
					if($feEvent[id] == $main1[event_id_ref]){
						$e_option .= '<option value="'.$feEvent[id].'" selected="">'.$feEvent[event_title].'</option>';
					}else{
						$e_option .= '<option value="'.$feEvent[id].'" >'.$feEvent[event_title].'</option>';
					}
				}
				if($max_idCode % 2){
					$tb_color = '#F0FFF0';
				}else{
					$tb_color = '#FFF5EE';
				}
				$set_source.= '<table  align =  "center" style="background-color:'.$tb_color.';"><tr><td align="center" width="40px">'.$max_idCode.'
							<input id="'.$result_thisCode.'" type="hidden" value="'.$indivCode[$i].'"></td><td align="center" width="110px"><input readonly="" id="'.$id_code.'" value="'.$expFixCode[0].'" style="width:70px;"> :
							</td><td  style="width:340px"><select disabled="" style="width:270px" class ="textCenter" id="dv_Qevent'.$max_idCode.'" onchange="selectQevent(\''.$max_idCode.'\');">
							'.$e_option.'</select> =></td><td style="width:340px"><div  id="dv_Mevent'.$max_idCode.'">'.$main_ques.'</div></td><td style="width:340px">
							<div   id="dv_Levent'.$max_idCode.'">'.$list_ques.'</div></td><td  style="width:110px"><div  id="dv_saveFixCode'.$max_idCode.'"><img  src="img/edit2.png" style="cursor: pointer;" title="แก้ไข" onclick="edit_ListFixCode(\''.$max_idCode.'\');">
							<img  src="img/delete.png" style="cursor: pointer;" title="ลบ" onclick="if(confirm(\'ท่านต้องการลบรหัส '.$expFixCode[0].' !?\')){del_ListFixCode(\''.$max_idCode.'\')};">
							</div></td></tr></table>';
			}//for จำนวน รหัสที่เคยตั้งไว้
			$tbNextCode = 'tbNextCode'.((int)$max_idCode+1);
			$set_source .= '<div id="'.$tbNextCode.'"></div>';
			$setting_mode = $tpl->tbHtml( $thisPage.'.html', 'SETTING_MODE' );
		}  else {// numrows เข้าเมื่อ ไม่มีการตั้งค่ารหัสมาก่อนเลย จะให้เริ่มสร้างใหม่
			$sql_event = "SELECT id, event_title, department FROM $config[db_base_name].follow_main_event WHERE status != '99' ORDER BY id ASC";
			$resEvent = $logDb->queryAndLogSQL( $sql_event, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			while($feEvent = mysql_fetch_array($resEvent)){
				$e_option .= '<option value="'.$feEvent[id].'" >'.$feEvent[event_title].'</option>';
			}
			$tbNextCode = 'tbNextCode'.((int)$max_idCode+1); //ใช้กำหนดลำดับต่อไป
			$question_source = '<td  style="width:320px"><select style="width:300px" class ="textCenter" id="dv_Qevent'.$max_idCode.'" onchange="selectQevent(\''.$max_idCode.'\');"><option value="0">-- กรุณาเลือกกิจกรรม --</option>'.$e_option.'</select> -></td><td><div  style="width:320px" id="dv_Mevent'.$max_idCode.'"></div></td><td><div  style="width:300px" id="dv_Levent'.$max_idCode.'"></div></td><td><div  style="width:280px" id="dv_saveFixCode'.$max_idCode.'"></div></td>';
			$set_source = $tpl->tbHtml( $thisPage.'.html', 'SET_SOURCE' );
			$setting_mode = $tpl->tbHtml( $thisPage.'.html', 'SETTING_MODE' );
		}
		
		if(isset($_GET['del_ListFixCode'])){ // ถ้าเกิดการลบรหัสในหน้าตั้งค่าจะอัพเดทเฉพาะ div main_mode
			echo $tpl->tbHtml( $thisPage.'.html', 'SETTING_MODE' );
		}else{// เมื่อเปิดเมนูปกติๆ
			echo $tpl->tbHtml( $thisPage.'.html', 'MAIN' );
		}
	break;
	case 'tab2':
		$BTNtab1 = 'BTosnow boldGreen';
		$BTNtab2 = 'BTsnow boldGreen';
		$BTNtab3 = 'BTosnow boldGreen';

		$sql_branch = "SELECT DISTINCT(sell_branch), working_company.name
				FROM $config[db_easysale].Sell JOIN $table_config[working_company] ON Sell.sell_branch = working_company.id
				ORDER BY working_company.id  ";
		$result = $logDb->queryAndLogSQL( $sql_branch, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		$branch_name = '<option value=""> - แสดงของทุกสาขา - </option>';//pt@2011-12-10
		while($branch = mysql_fetch_assoc($result)){
			if(isset($_GET['select_branch'])){
				if($_GET['select_branch'] == $branch['sell_branch']){
					$branch_name .= '<option value="'.$branch[sell_branch].'" selected="">'.$branch[name].'</option>';
				}else{
					$branch_name .= '<option value="'.$branch[sell_branch].'">'.$branch[name].'</option>';
				}
			}else{
				$branch_name .= '<option value="'.$branch[sell_branch].'">'.$branch[name].'</option>';
			}
		}
		
		if($_GET['start_dDate']){
			$start_date = $_GET['start_dDate'];
		}
		if($_GET['stop_dDate']){
			$stop_date = $_GET['stop_dDate'];
		}
		
		$management_mode =  $tpl->tbHtml( $thisPage.'.html', 'SELECT_BRANCH');
		echo $tpl->tbHtml( $thisPage.'.html', 'MAIN' );
	break;
	case 'maxCode':
		$max_idCode = $_GET['maxCode'];
		$id_code = 'inp_'.$max_idCode;
		$result_thisCode = 'result_thisCode_'.$max_idCode;
		
		$sql_event = "SELECT id, event_title, department FROM $config[db_base_name].follow_main_event WHERE status != '99' ORDER BY id ASC";
		$resEvent = $logDb->queryAndLogSQL( $sql_event, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($feEvent = mysql_fetch_array($resEvent)){
			$e_option .= '<option value="'.$feEvent[id].'" >'.$feEvent[event_title].'</option>';
		}
		
		$tbNextCode = 'tbNextCode'.((int)$max_idCode+1);//'
		if($max_idCode % 2){
			$tb_color = '#F0FFF0';
		}else{
			$tb_color = '#FFF5EE';
		}
		
		echo '<table  align =  "center" style="background-color:'.$tb_color.';"><tr><td align="center" width="50px">'.$max_idCode.'<input id="'.$result_thisCode.'" type="hidden"></td><td align="center" width="90px"><input id="'.$id_code.'" style="width:70px;"> : </td><td  style="width:320px"><select style="width:300px" class ="textCenter" id="dv_Qevent'.$max_idCode.'" onchange="selectQevent(\''.$max_idCode.'\');"><option value="0">-- กรุณาเลือกกิจกรรม --</option>'.$e_option.'</select> -></td><td><div  style="width:320px" id="dv_Mevent'.$max_idCode.'"></div></td><td><div  style="width:300px" id="dv_Levent'.$max_idCode.'"></div></td><td><div  style="width:280px" id="dv_saveFixCode'.$max_idCode.'"></div></td></tr></table><div id="'.$tbNextCode.'"></div>';
	break;
	case 'resultCode':
		//INSERT or UPDATE
		$return = '1';
		$maxCode = $_POST['arrResultCode'];
		$list_question_id = '';
		
		for($i=0;$i<$maxCode;$i++){
			$var = $_POST['var'.$i];
			if($list_question_id != ''){
				$list_question_id .= ','.$var;
			}else{
				$list_question_id .= $var;
			}
		}
		
		//เชคว่ามีการตั้งค่าซ้ำเดิมรึปล่าว ถ้าซ้ำ จะเป็นการอัพเดท วันที่แก้ไขฉยๆ
		$sql_chkexistSetup = "SELECT id, Date_Setup, list_question_id FROM $config[db_base_name].D_NET WHERE Type_Status = '90' ORDER BY id DESC LIMIT 1";
		$res_chkexistSetup = $logDb->queryAndLogSQL( $sql_chkexistSetup, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$fet_chkexistSetup = mysql_fetch_assoc($res_chkexistSetup);
		
		if($list_question_id == $fet_chkexistSetup[list_question_id]){
			$dateupdate = $fet_chkexistSetup[Date_Setup].',update on '.$today_date;
			$sql_updSetup = "UPDATE $config[db_base_name].D_NET SET Date_Setup='$dateupdate' WHERE id = '$fet_chkexistSetup[id]'";
		}else{
			$sql_insert_setup = "INSERT INTO $config[db_base_name].D_NET (list_question_id, Date_Setup, Type_Status) VALUES ('$list_question_id', '$today_date', '90')";
			$logDb->queryAndLogSQL( $sql_insert_setup, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		}
		
		echo $return;
	break;
	case 'Qevent':
		$max_idCode = $_GET['maxCode'];
		$Main = "SELECT question_title,id FROM $config[db_base_name].follow_main_question WHERE event_id_ref = '$_GET[Qevent]' AND status != '99' ORDER BY id ASC";
		$queMain = $logDb->queryAndLogSQL( $Main, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$rowMain = mysql_num_rows($queMain);
		//$no = 0; $ques = '';
		if($rowMain!=0){
			$main_ques = '<select style="width:300px" id="se_Mevent'.$max_idCode.'" class ="textCenter" onchange="selectMevent(\''.$max_idCode.'\');"><option value="0" >-- กรุณาเลือกคำถามหลัก --</option>';
			while($feMain = mysql_fetch_array($queMain)){
				$main_ques .= '<option value="'.$feMain[id].'" >'.$feMain[question_title].'</option>';
			}
			$main_ques .= '</select> ->';
		}else{
			$main_ques = 'ผิดพลาด ไม่พบรายการย่อย!';
		}
		echo $main_ques;
	break;
	case 'Mevent':
		$max_idCode = $_GET['maxCode'];
		
		$sql = "SELECT list_title,id,REPLACE(answer_option,\"'\",'') AS answer_option FROM $config[db_base_name].follow_question_list WHERE main_question_id = '$_GET[Mevent]' AND status!='99' ORDER BY id ASC";
		$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$row = mysql_num_rows($que);

		if($row!=0){
			$list_ques = '<select style="width:300px" id="se_Levent'.$max_idCode.'"  onchange="selectLevent(\''.$max_idCode.'\');"><option value="0" >-- กรุณาเลือกคำถามย่อย --</option>';
			while($fe = mysql_fetch_array($que)){
				$list_ques .= '<option value="'.$fe[id].'" >'.$fe[list_title].'</option>';
			}
			$list_ques .= '</select>';
		}else{
			$list_ques = 'ผิดพลาด ไม่พบรายการย่อย!';
		}
		echo $list_ques;
	break;
	case 'activeTab':
		if(isset($_GET['save_newAVG'])){  //เมื่อมีการเซตค่าเฉลี่ย
			//ต้อง Update  คะแนนย่อย ในคอลัมนี้ทั้งหมด
			$position  = $_POST['position'];
			$exp_Vin    = explode(",",$_POST['list_Vin']);
			$exp_value = explode(",",$_POST['arr_newValue']);//ลิสคะแนน
			$val = 0;
			for($i=0;$i<count($exp_Vin);$i++){
				$sql_listANS = "SELECT id,list_answer_value FROM $config[db_base_name].D_NET WHERE VinNo = '$exp_Vin[$i]' AND Type_Status = '10' ORDER BY id DESC LIMIT 1 ";
				$res_listANS = $logDb->queryAndLogSQL( $sql_listANS, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$fet_listANS = mysql_fetch_assoc($res_listANS);
				$expANS1 = explode(",",$fet_listANS['list_answer_value']);

				//แทรกคะแนนใหม่ลงไปแทนของเดิมในลิส ณ ตำแหน่งเดิม
				$NewList_answer = $sign = '';
				for($j=0;$j<count($expANS1);$j++){
					$ANSS = $expANS1[$j];
					if($j == $position){
						$expA = explode("=",$ANSS);
						$ANSS = $expA[0].'='.$exp_value[$val];
					}
					$NewList_answer .= $sign.$ANSS;
					$sign = ',';
				}
				$val++;// ลำดับคะแนนตัวใหม่
				//บันทึกกลับลงไปในฐานข้อมูล
				$sql_upLIST = "UPDATE $config[db_base_name].D_NET SET list_answer_value = '$NewList_answer' WHERE id = '$fet_listANS[id]'";
				$logDb->queryAndLogSQL( $sql_upLIST, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			}

			// เซตค่าเพื่อใช้ต่อ เงื่อนไขนอก
			if($_POST['start_dDate'] != ''){
				$exp_start = explode("-",$_POST['start_dDate']);
				$_GET['start_dDate'] = $exp_start[2].'-'.$exp_start[1].'-'.$exp_start[0];
			}
			if($_POST['stop_dDate'] != ''){
				$exp_stop = explode("-",$_POST['stop_dDate']);
				$_GET['stop_dDate'] = $exp_stop[2].'-'.$exp_stop[1].'-'.$exp_stop[0];
			}
			$_GET['select_branch'] = $_POST['select_branch'];
		}
		
		//if(isset($_GET['save_newPoint'])){ ที่จริงอยู่ if นอก
		if(isset($_GET['save_newPoint'])){  // เมื่อมีการเซฟแก้ไข คะแนนย่อยๆๆ
			$newPoint = $_POST['newPoint'];
			$expVin_Position = explode("_",$_POST['VinNo_Position']);
			$newListAnswer = '';
			$sql_listANS = "SELECT id,list_answer_value FROM $config[db_base_name].D_NET WHERE VinNo = '$expVin_Position[0]' AND Type_Status = '10' ORDER BY id DESC LIMIT 1 ";
			$res_listANS = $logDb->queryAndLogSQL( $sql_listANS, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$fet_listANS = mysql_fetch_assoc($res_listANS);
			$expANS1 = explode(",",$fet_listANS[list_answer_value]);
			for($i=0;$i<count($expANS1);$i++){
				$ans_i = $expANS1[$i];

				if($expVin_Position[1] == $i){
					$expANS2 = explode("=",$expANS1[$i]);
					$ans_i = $expANS2[0].'='.$newPoint;
				}
				if($newListAnswer == ''){
					$newListAnswer .= $ans_i;
				}else{
					$newListAnswer .= ','.$ans_i;
				}
			}
			$sql_update_newans = "UPDATE $config[db_base_name].D_NET  SET list_answer_value = '$newListAnswer' WHERE id = '$fet_listANS[id]'";
			$logDb->queryAndLogSQL( $sql_update_newans, " FILE : ".__FILE__." LINE : ".__LINE__."" );

			########### เซตค่าเพื่อใช้ต่อ เงื่อนไขนอก  ||  DECLARE VALUE สำหรับใช้ PHASE II  #############
			if($_POST['start_dDate'] != ''){
				$exp_start = explode("-",$_POST['start_dDate']);
				$_GET['start_dDate'] = $exp_start[2].'-'.$exp_start[1].'-'.$exp_start[0];
			}
			if($_POST['stop_dDate'] != ''){
				$exp_stop = explode("-",$_POST['stop_dDate']);
				$_GET['stop_dDate'] = $exp_stop[2].'-'.$exp_stop[1].'-'.$exp_stop[0];
			}
			$_GET['select_branch'] = $_POST['select_branch'];
		}//END if(isset($_GET['save_newPoint']))
		############################################# END PHASE I #############################################
		
		############################################# BEGIN PHASE II #############################################
		//เชคดูการตั้งค่าล่าสุด  ถ้ายังไม่มีจะไม่แสดงรายการDN
		$sql_dNet_listquestion = "SELECT list_question_id, Date_Setup FROM $config[db_base_name].D_NET WHERE Type_Status = '90' ORDER BY id DESC LIMIT 1";
		$res_listQuestion = $logDb->queryAndLogSQL( $sql_dNet_listquestion, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$num_rowListQ = mysql_num_rows($res_listQuestion);

		if($num_rowListQ){
			$input_type_q_list = array();
			$tb_detailsDN ='';
			$suan_no_send = 0; // ตัวส่วน เวลาหาค่าเฉลี่ย
			$WHERE = "WHERE 1=1 ";
			if($_GET['start_dDate'] != '' && $_GET['stop_dDate'] != ''){
				$WHERE .= " AND DATE(STR_TO_DATE(Sell.Date_Deliver, '%d-%m-%Y')) BETWEEN '$_GET[start_dDate]' AND '$_GET[stop_dDate]' ";
			}
			if(isset($_GET['select_branch']) && $_GET['select_branch'] ){
				$WHERE .= " AND Sell.sell_branch LIKE '$_GET[select_branch]' ";//pt@2011-12-10
			}else{
				$WHERE .= " AND Sell.sell_branch LIKE '%' "; //pt@2011-12-10
			}
			$WHERE .= " ORDER BY DATE(STR_TO_DATE(Sell.Date_Deliver, '%d-%m-%Y')) ";
			$WHERE .= $LIMIT_WHERE;

			//SELECT D_NET เอาลิสคำถามออกมา
			$tdtdTopicANS = '';
			$sqlHeadquestion = "SELECT id, list_question_id FROM $config[db_base_name].D_NET WHERE Type_Status = '90' ORDER BY id DESC LIMIT 1";//PT  บรรทัดนี้ไม่ต้องเขียนก็ได้  ให้ fetch จาก$res_listQuestion
			$res_Headquestion = $logDb->queryAndLogSQL( $sqlHeadquestion, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$Headquestion = mysql_fetch_assoc($res_Headquestion);
			$paramDownload = $Headquestion['id'].'_'; // for DOWNLOAD_BTN
			$exp_Headquestion = explode(",",$Headquestion[list_question_id]);
			for($i=0;$i<count($exp_Headquestion);$i++){
				$expgetCode = explode("=",$exp_Headquestion[$i]);
				$tdtdTopicANS .= '<th align="center" width="50px">'.$expgetCode[0].'</th>';

				//สร้างตัวกำหนดว่า คำถามนี้ เป็น คำถามคะแนนเต็ม10 หรือว่า คำถามนี้เป็น คำถาม  Yes, No
				$sql_type = "SELECT input_type  FROM $config[db_base_name].follow_question_list WHERE id='$expgetCode[1]' ";
				$res_type = $logDb->queryAndLogSQL( $sql_type, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$fet_type = mysql_fetch_assoc($res_type);
				if($fet_type['input_type']=='pleasure_value'){
					$input_type_q_list[$i] = 'type_10';
				}else{
					$input_type_q_list[$i] = 'type_1';
				}
			}

			/* $sql_DN = "SELECT Main_Vehicle.DNCode, Main_Vehicle.MV_Chass_No, working_company.name, Sell.cusBuy, Sell.CusNo, Sell.Date_Deliver, Sell.Sell_Type_S, Main_Vehicle.Model_GName, Main_Vehicle.MDL_EXP, Main_Vehicle.MV_Model_CodeName, Main_Vehicle.Color
				FROM $config[db_easysale].Sell
				JOIN $config[db_easysale].Main_Vehicle
				ON Sell.Eng_No_S = Main_Vehicle.MV_Eng_No
				JOIN $table_config[working_company]
				ON Sell.sell_branch = working_company.id
				$WHERE "; */
			$sql_DN = "SELECT Main_Vehicle.DNCode, Main_Vehicle.MV_Chass_No, working_company.name, Sell.cusBuy, Sell.CusNo, Sell.Date_Deliver, Sell.Sell_Type_S, Main_Vehicle.Model_GName, Main_Vehicle.MDL_EXP, Main_Vehicle.MV_Model_CodeName, Main_Vehicle.Color
				FROM $config[db_easysale].Sell
				JOIN $config[db_easysale].Main_Vehicle
				ON Sell.Chassi_No_S = Main_Vehicle.MV_Chass_No
				JOIN $table_config[working_company]
				ON Sell.sell_branch = working_company.id
				$WHERE ";
				$res_DN = $logDb->queryAndLogSQL( $sql_DN, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$num_rowDN = mysql_num_rows($res_DN);
				$count_rowStyle = 1;
				$count_DN = 0;
				$sign = '';

				while($DN_No = mysql_fetch_assoc($res_DN)){
					$btnRefresh = ''; //pt@2011-12-23
					$count_rowStyle++;
					$count_DN++;
					$expcusBuy = explode("_",$DN_No['cusBuy']);
					$tdtdANS = '';
					$tdtd_sum_Q = '';

					//ดึง ไอดี คำถามที่ตั้งค่าไว้  ถ้ามีการตั้งค่าใหม่ แล้วเรียกดูข้อมูล ข้อมูลจะกลายเป็นค่าDefualt จะไม่ใช้ค่าที่ถูกเก็บไว้เดิมในD_NET เพราะ ถือเป็นการตั้งค่าคนละรอบกัน
					$sqlANS = "SELECT id,list_question_id FROM $config[db_base_name].D_NET WHERE Type_Status = '90' ORDER BY id DESC LIMIT 1";
					$res_ANS = $logDb->queryAndLogSQL( $sqlANS, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$ANS = mysql_fetch_assoc($res_ANS);
					$exp_ANS = explode(",",$ANS['list_question_id']);

					//เชคว่าเคยเก็บค่าของ DN นี้ไว้แล้วรึยัง
					$sql_chkDN_exists = "SELECT id, DN_No, Sell_Branch, Date_Delivery, cusBuy, typeBuy, modelBuy, list_answer_value, Date_Download FROM $config[db_base_name].D_NET
										WHERE (Type_Status = '10' AND VinNo = '$DN_No[MV_Chass_No]' AND id_setup_ref= '$ANS[id]')
										OR (Type_Status = '10' AND VinNo = '$DN_No[MV_Chass_No]' AND Date_Download != '')
										ORDER BY id DESC";
					$res_chkDN_exists = $logDb->queryAndLogSQL( $sql_chkDN_exists, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$_chkDN_exists = mysql_fetch_assoc($res_chkDN_exists);
					$num_chkDN = mysql_num_rows($res_chkDN_exists);
					if(!$num_chkDN){
						$temp_listANS = '';
						for($i=0;$i<count($exp_ANS);$i++){

							//เอาคำตอบจริงๆของคนนี้คำถามนี้
							$expidQ = explode("=",$exp_ANS[$i]);
							$sql_fol_ans = "SELECT REPLACE(answer,\"'\",'') AS ANS FROM $config[db_base_name].follow_answer
							WHERE ques_list_id = '$expidQ[1]' AND cus_no = '$expcusBuy[0]' ORDER BY id DESC LIMIT 1";
							$res_fol_ans = $logDb->queryAndLogSQL( $sql_fol_ans, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							$fol_ans = mysql_fetch_assoc($res_fol_ans);
							if($fol_ans['ANS'] == ''){
								$fol_ans['ANS'] = '';
							}else if($fol_ans['ANS'] == 'ใช่' || $fol_ans['ANS'] == 'Yes' || $fol_ans['ANS'] == 'มี'){
								$fol_ans['ANS'] = '1';
							}else if($fol_ans['ANS'] == 'ไม่ใช่' || $fol_ans['ANS'] == 'No' || $fol_ans['ANS'] == 'ไม่มี'){
								$fol_ans['ANS'] = '0';
							}

							if($temp_listANS == ''){
								$temp_listANS .=     $expidQ[1].'='.$fol_ans['ANS'];
							}else{
								$temp_listANS .= ','.$expidQ[1].'='.$fol_ans['ANS'];
							}
						}
						//บันทึกลง D_NET ค่านี้ถือเป็น ดีฟอล
						$sql_insert_real_answer = "INSERT INTO $config[db_base_name].D_NET (DN_No, VinNo , Sell_Branch, Date_Delivery, cusBuy, typeBuy, modelBuy, list_answer_value, id_setup_ref, Type_Status)
												VALUES ('$DN_No[DNCode]', '$DN_No[MV_Chass_No]', '$DN_No[name]', '$DN_No[Date_Deliver]', '$expcusBuy[1]', '$DN_No[Sell_Type_S]', '$DN_No[Model_GName] $DN_No[MV_Model_CodeName] $DN_No[Color]', '$temp_listANS', '$ANS[id]', '10')";
						$logDb->queryAndLogSQL( $sql_insert_real_answer, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					}else{
						#####
						#####PT@2012-02-29 แก้ปัญหาต้องคอยกด Refresh  [เพิ่ม else]
						#####
						//เช็คก่อนว่าคะแนนที่ถูกเก็บไว้เป็น ค่าว่างทั้งหมดรึปล่าว
						$temp_listANS = '';
						$expans = explode("=,",$_chkDN_exists['list_answer_value']);
						if(count($expans) == count($exp_ANS)){
							$expLast = explode("=",$expans[(count($expans)-1)]);
							if($expLast[1] == '' || $expLast[1] == null){//ถึงตอนนี้ถ้าเข้าifนี้ถือว่าคะแนนเป็นค่าว่างทั้งหมด
								// จัดการดึงค่าคะแนน ปริยาย ออกมาจัดรูปแบบเพื่อใส่ลง DB
								$expansr = explode(",",$_chkDN_exists['list_answer_value']);
								for($ir=0;$ir<count($expansr);$ir++){
									//เอาคำตอบจริงๆของคนนี้คำถามนี้
									$expidAr = explode("=",$expansr[$ir]);
									$sql_fol_ans = "SELECT REPLACE(answer,\"'\",'') AS ANS FROM $config[db_base_name].follow_answer
												WHERE ques_list_id = '$expidAr[0]' AND cus_no = '$expcusBuy[0]' ORDER BY id DESC LIMIT 1";
									$res_fol_ans = $logDb->queryAndLogSQL( $sql_fol_ans, " FILE : ".__FILE__." LINE : ".__LINE__."" );
									$fol_ans = mysql_fetch_assoc($res_fol_ans);
									if($fol_ans['ANS'] == ''){
										$fol_ans['ANS'] = '';
									}else if($fol_ans['ANS'] == 'ใช่' || $fol_ans['ANS'] == 'Yes' || $fol_ans['ANS'] == 'มี'){
										$fol_ans['ANS'] = '1';
									}else if($fol_ans['ANS'] == 'ไม่ใช่' || $fol_ans['ANS'] == 'No' || $fol_ans['ANS'] == 'ไม่มี'){
										$fol_ans['ANS'] = '0';
									}

									if($temp_listANS == ''){
										$temp_listANS .=     $expidAr[0].'='.$fol_ans['ANS'];
									}else{
										$temp_listANS .= ','.$expidAr[0].'='.$fol_ans['ANS'];
									}
								}
								$sql_update_newans = "UPDATE $config[db_base_name].D_NET  SET Date_Delivery = '$DN_No[Date_Deliver]' , list_answer_value = '$temp_listANS' WHERE VinNo  = '$DN_No[MV_Chass_No]' ORDER BY id DESC LIMIT 1";
								$logDb->queryAndLogSQL( $sql_update_newans, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							}
						}
					}#####END PT@2012-02-29 แก้ปัญหาต้องคอยกด Refresh

					//SELECT ANS เอา คำตอบ ของลูกค้าคนนี้ ออกมา ยัดลง $tdtdANS
					//เชคหาDN ที่เคยถูกโหลดไปแล้ว  เชคrecord ที่ DN เลขนี้ ซึ่งอาจถูกโหลดไฟล์ไปในช่วงที่ทำการตั้งค่าไม่ใช่ปัจจุบัน
					$sql_getd_NET_ans = "SELECT id, DN_No, VinNo, Sell_Branch, Date_Delivery, cusBuy, typeBuy, modelBuy, list_answer_value, Date_Download
										FROM $config[db_base_name].D_NET
										WHERE Type_Status = '10' AND VinNo = '$DN_No[MV_Chass_No]' AND Date_Download <> '' ORDER BY id DESC ";
					$res_getd_NET_ans = $logDb->queryAndLogSQL( $sql_getd_NET_ans, " FILE : ".__FILE__." LINE : ".__LINE__."" );

					$numRow = mysql_num_rows($res_getd_NET_ans);
					//echo '<br>'.$numRow .'  '.$sql_getd_NET_ans;
					$getd_NET_ans = mysql_fetch_assoc($res_getd_NET_ans);
					$expfindDNoldExists = explode(",",$getd_NET_ans[list_answer_value]);
					$chkDownload = ''; //pt@2011-12-21
					if(!$numRow){ //ยังไม่เคยถูกโหลดไฟล์
						//ดึง คำตอบในD_NET
						$sql_getd_NET_ans = "SELECT id, DN_No, VinNo, Sell_Branch, Date_Delivery, cusBuy, typeBuy, modelBuy, list_answer_value, Date_Download
										FROM $config[db_base_name].D_NET
										WHERE Type_Status = '10' AND VinNo = '$DN_No[MV_Chass_No]' ORDER bY id DESC";
						$res_getd_NET_ans = $logDb->queryAndLogSQL( $sql_getd_NET_ans, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						$getd_NET_ans = mysql_fetch_assoc($res_getd_NET_ans);
						// for DOWNLOAD BTN
						$expbtn_dl = explode("_",$paramDownload);if($expbtn_dl[1] == ''){	$paramDownload .= $DN_No['MV_Chass_No'];} else {$paramDownload .= ','.$DN_No['MV_Chass_No'];} // for DOWNLOAD BTN
						$expDNANS = explode(",",$getd_NET_ans['list_answer_value']);

						for($i=0;$i<count($expDNANS);$i++){
							// เอาคำตอบจริงๆของคนนี้คำถามนี้
							$expanswer = explode("=",$expDNANS[$i]);
							$ansreadonly = ' onclick="editRawPoint(\''.$getd_NET_ans[VinNo].'_'.$i.'\',\''.$input_type_q_list[$i].'\')" ';
							$tdtdANS .= '<input type="hidden" id="'.$getd_NET_ans[VinNo].'_'.$i.'" value="'.$expanswer[1].'"><td id = "td_'.$getd_NET_ans[VinNo].'_'.$i.'" align="center" '. $ansreadonly.'> &nbsp;'.$expanswer[1].'&nbsp; </td>';
							$sumQ[$i] += $expanswer[1];
						}

						//pt@2011-12-21 เพิ่ม checkBox ให้เลือกโหลด Record
						$chkDownload = '<input id="DNID" type="checkbox"  onchange="chk_all_DN(\'btnone\');"  value="'.$getd_NET_ans['VinNo'].'" name="DNID[]">';

						$suan_no_send++;
						$collectDN4avgOptimize .= $sign.$getd_NET_ans['VinNo']; // ใช้สำหรับ เวลา ผู้ใช้แก้ ค่าเฉลี่ย แล้ว จะเอารายการนี้ไปใช้ปรับค่าย่อยๆ ของ คอลัมนั้น
						$sign = ',';

						//pt@2011-12-23 เพิ่มการ รีเฟรช คะแนน ให้เป็น ปัจจุบัน
						$btnRefresh = '<img src="img/refreshDefualt.png" style="cursor: pointer;" title="เรียกใช้ค่าปริยาย" onclick="refresh_SQ_point(\''.$expcusBuy[0].'\',\''.$DN_No[MV_Chass_No].'\');" >'; //pt@2011-12-23
					}else{ //pt2011-11-22 แก้ให้มองเห็นคะแนนที่เคยถูกโหลดไปแล้ว และ โหลดไฟล์ได้ใหม่
						#//เคยโหลดไฟล์ไปแล้ว จะไม่ให้แก้ไข
						//text-decoration:line-through
						#	$exdatDL 		= explode("-",$getd_NET_ans['Date_Download']);
						#	$dateDL  		= $exdatDL[2].'/'.$exdatDL[1].'/'.$exdatDL[0];
						#	$ansreadonly 	=' style="color:lightgreen; " ';
						#	$tdtdANS 	   .= '<input type="hidden" id="'.$getd_NET_ans[DN_No].'_'.$i.'" value="0"><td id = "td_'.$getd_NET_ans[DN_No].'_'.$i.'" colspan = "'.count($exp_ANS).'" align="center" '. $ansreadonly.'><b> &nbsp;  DOWNLOADED on '.$dateDL.' &nbsp; </b></td>';
						/////////////////////////วนเอาคำถามที่เคยถูกโหลด ออกมาตามช่องคำถามใหม่ /////////////////////
						$ansreadonly =' style="color:orange; " ';//text-decoration:line-through
						//for($i=0;$i<count($expfindDNoldExists);$i++){
						for($i=0;$i<count($exp_ANS);$i++){// คำถาม
							$Quset_in = 'N';
							$expidQ = explode("=",$exp_ANS[$i]);//แตกคำถาม
							for($j=0;$j<count($expfindDNoldExists);$j++){ //คำตอบ ซึ่งบางทีมีไม่ครบ บางทีมีเกิน  เพราะ ถูกโหลดไปแล้ว
								//เอาคำตอบจริงๆของคนนี้คำถามนี้
								$expanswerExists = explode("=",$expfindDNoldExists[$i]);//แตกคำตอบ
								if($expidQ[1] == $expanswerExists[0]){$Quset_in = 'Y';}
							}
							if($Quset_in = 'Y'){
								$expanswerExists = explode("=",$expfindDNoldExists[$i]);//แตกคำตอบ
								$tdtdANS .= '<input type="hidden" id="'.$fet_findDNoldExists[VinNo].'_'.$i.'" value="0"><td id = "td_'.$fet_findDNoldExists[VinNo].'_'.$i.'" align="center" '. $ansreadonly.'> &nbsp;'./*$expfindDNoldExists[$i]*/$expanswerExists[1].'&nbsp; </td>';
							}else{
								$tdtdANS .= '<input type="hidden" id="'.$fet_findDNoldExists[VinNo].'_'.$i.'" value="0"><td id = "td_'.$fet_findDNoldExists[VinNo].'_'.$i.'" align="center" '. $ansreadonly.'> &nbsp;&nbsp; </td>';
							}
							#		$sumQ[$i] += $expanswerExists[1];
						}
						//pt@2011-12-21 เพิ่ม checkBox ให้เลือกโหลด Record
						$chkDownload = '<input id="DNID" type="checkbox"  onchange="chk_all_DN(\'btnone\');" value="'.$getd_NET_ans['VinNo'].'" name="DNID[]">';

						$expbtn_dl = explode("_",$paramDownload);if($expbtn_dl[1] == ''){	$paramDownload .= $DN_No['MV_Chass_No'];} else {$paramDownload .= ','.$DN_No['MV_Chass_No'];} // for DOWNLOAD BTN  //เก็บไอดีสำหรับDN ที่ยังไม่เคยถูกโหลดด้วย เพราะว่าแก้ไขสามารถดาวโหลด ซ้ำได้
					} //end pt2011-11-22
					if($count_rowStyle%2){
						$style_S = 'style="background-color:#F0FFF0;"';
					}else{
						$style_S = 'style="background-color:#FFFFF9;"';
					}

					$tb_detailsDN .= $tpl->tbHtml( $thisPage.'.html', 'DETAILS_DN' );
				}
				$HEADtb_detailsDN .= $tpl->tbHtml( $thisPage.'.html', 'HEAD_DETAILS_DN');//ต้องประกาศช้าหน่อย เพราะรอค่าตัวแปร
				$tb_detailsDN = $HEADtb_detailsDN.$tb_detailsDN;
				if($num_rowDN == 0 ){ // ถ้าไม่มีข้อมูล ก็ให้  = ไม่พบรายการไปเลย
					$tb_detailsDN = '<table align = "center"> <tr><td style = "background-color:white; font-size:24px;"> ไม่พบรายการที่ท่านค้นหา </td></tr>';
				}else{
					if(!isset($_GET['save_newPoint']) && !isset($_GET['save_newAVG'])){$tdtd_sum_Q = '<tr height="25px" style="background-color: #E0EEE0" id="tdtd_sum_Q">';}
					//pt@2011-12-21  เพิ่ม  <th align="left"><input onchange="chk_all_DN();" type="checkbox" value="LoadAll" name="ALLDNID" id="ALLDNID">ทั้งหมด</th>
					$tdtd_sum_Q .= '<th align="left"><input onchange="chk_all_DN(\'btnall\');" type="checkbox" value="LoadAll" name="ALLDNID" id="ALLDNID">ทั้งหมด</th><th align="center">'. $count_DN .' รายการ</th><th></th><th></th><th></th><th></th><th align = "right">เฉลี่ย : </th>';
					if($suan_no_send == 0){
						$suan_no_send=1;
					}
					for($i=0;$i<count($exp_ANS);$i++) {
						$expidQ = explode("=",$exp_ANS[$i]);
						$tdtd_sum_Q .= '<input type="hidden" id="avg_'.$expidQ[0].'_'.$i.'" value="'.sprintf('%.02f',$sumQ[$i]/$suan_no_send).'"> <th align ="center" id = "td_sum_'.$expidQ[0].'" onclick="editAVG_SQ(\''.$expidQ[0].'\',\''.$i.'\',\''.sprintf('%.02f',$sumQ[$i]/$suan_no_send).'\',\''.$input_type_q_list[$i].'\')">'.sprintf('%.02f',$sumQ[$i]/$suan_no_send).'</th>';
					}
					if(!isset($_GET['save_newPoint']) && !isset($_GET['save_newAVG'])){
						$tdtd_sum_Q .= '<th></th></tr>';//pt@2011-12-22
					}else{//หลังจากกดบันทึกการแก้ไข
						$tdtd_sum_Q .= '<th></th>'; //pt@2011-12-22
					}
					# - ปุ่มด้านล่างสุด TAB 2
					//<button id="btn_save_tab2"> บันทึก SQ Form </button> &nbsp; &nbsp;
					//$save_4_prepare_download = '<div align="center" > <button id="btn_download_CSV"> Download .CSV  </button> </div> ';
				}
				if($num_rowDN>=$NumLIMIT){ // หมายเหตุด้านล่างสุด pt TEST
					$alert_100row = " ** จำกัดการแสดงผล $NumLIMIT รายการแรกเท่านั้น ** ";
				}else{
					$alert_100row ='';
				}
		}else{
			//ถ้าไม่มีการตั้งค่า
			$tdtd_sum_Q = '<table align = "center"> <tr><td style = "background-color:yellow; color:red;  font-size:24px;"> กรุณาตั้งค่ารหัสก่อน แล้วค่อยกลับมาจัดการคะแนนครับ </td></tr>';
		}// จบการเชคว่ามีการตั้งค่ารึยัง
		
		if(isset($_GET['save_newPoint']) || isset($_GET['save_newAVG'])){  // จะเกิดขึ้นเมื่อ มีการเซตคะแนนใหม่ หรือ เซตค่าเฉลี่ยใหม่
			$tb_detailsDN = $tdtd_sum_Q; //ถ้าเป็นการเปลี่ยนค่าคะแนน  : อัพเดทแถว เฉลี่ย
		}else{
			$tb_detailsDN .= $tpl->tbHtml( $thisPage.'.html', 'FOOT_DETAILS_DN' );
		}
		echo $tb_detailsDN;
	break;
	case 'refreshSQ':
		$fet_listQuestion = getQuestion_DNet90("$config[db_base_name].D_NET");

		if($fet_listQuestion){
			$cusno = $_POST['cusno'];
			$VinNo = $_POST['VinNo'];
			$expQ = explode(',',$fet_listQuestion['list_question_id']);
			// จัดการดึงค่าคะแนน ปริยาย ออกมาจัดรูปแบบเพื่อใส่ลง DB
			for($i=0;$i<count($expQ);$i++){
				//เอาคำตอบจริงๆของคนนี้คำถามนี้
				$expidQ = explode("=",$expQ[$i]);
				$sql_fol_ans = "SELECT REPLACE(answer,\"'\",'') AS ANS FROM $config[db_base_name].follow_answer
							WHERE ques_list_id = '$expidQ[1]' AND cus_no = '$cusno' ORDER BY id DESC LIMIT 1";
				$res_fol_ans = $logDb->queryAndLogSQL( $sql_fol_ans, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$fol_ans = mysql_fetch_assoc($res_fol_ans);
				if($fol_ans['ANS'] == ''){
					$fol_ans['ANS'] = '';
				}else if($fol_ans['ANS'] == 'ใช่' || $fol_ans['ANS'] == 'Yes' || $fol_ans['ANS'] == 'มี'){
					$fol_ans['ANS'] = '1';
				}else if($fol_ans['ANS'] == 'ไม่ใช่' || $fol_ans['ANS'] == 'No' || $fol_ans['ANS'] == 'ไม่มี'){
					$fol_ans['ANS'] = '0';
				}

				if($temp_listANS == ''){
					$temp_listANS .=     $expidQ[1].'='.$fol_ans['ANS'];
				}else{
					$temp_listANS .= ','.$expidQ[1].'='.$fol_ans['ANS'];
				}
			}
			$sql_update_newans = "UPDATE $config[db_base_name].D_NET SET list_answer_value = '$temp_listANS' WHERE VinNo  = '$VinNo' ORDER BY id DESC LIMIT 1";
			$logDb->queryAndLogSQL( $sql_update_newans, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			echo $temp_listANS;
		}
	break;
	case 'downloadSQ':
		$filename = 'SQ_Form.csv';
		$sDir = "files/CSV_Files/";
		
		if(!file_exists($sDir)){
			mkdir($sDir, 0777, true);
			chmod($sDir, 0777);
		}
		
		$filePath = $sDir.$filename;
		$objWrite = fopen("$filePath", "w");
		$sqlHeadquestion = "SELECT id, list_question_id FROM $config[db_base_name].D_NET WHERE Type_Status = '90' ORDER BY id DESC LIMIT 1";
		$res_Headquestion = $logDb->queryAndLogSQL( $sqlHeadquestion, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$Headquestion = mysql_fetch_assoc($res_Headquestion);
		$exp_Headquestion = explode(",",$Headquestion['list_question_id']);
		
		// เก็บหัวตารางเข้า row CSV
		$th = 'Vin';
		for($i= 0;$i<count($exp_Headquestion);$i++){
			$x_cd = explode("=",$exp_Headquestion[$i]);
			$th .= '|'.$x_cd[0];
		}
		
		if($th != 'Vin'){
			$title = $th."\n";
			fwrite($objWrite, $title);
			$th='';
		}

		// pt@2011-12-21 ใช้รายการ DN  จากการเลือกเช็คบ็อค
		if(isset($_GET['ALLDNID'])){
			$expref = explode("_", $_GET['idRef_Vins']); // 0=ไอดีของquestionlist  1=DN,DN,DN
			$expVins =  explode(",",$expref[1]);
		}else{
			$expVins = $_GET['DNID'];
		}
		//end pt@2011-12-21

		$arr_idLoad = array();
		for($i=0;$i<count($expVins);$i++){# pt2011-11-22 แก้ให้มองเห็นคะแนนที่เคยถูกโหลดไปแล้ว และ โหลดไฟล์ได้ใหม่
			//$sql = "SELECT id, list_answer_value, Date_Download FROM $config[db_base_name].D_NET WHERE DN_No = '$expdn[$i]' AND id_setup_ref = '$expref[0]' AND Date_Download IS NOT NULL ORDER BY id DESC";
			$sql = "SELECT id, list_answer_value, Date_Download FROM $config[db_base_name].D_NET WHERE VinNo = '$expVins[$i]' ORDER BY id DESC"; // DESC เพราะ ว่า เอา record สุดท้าย
			$res = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$fet_dn = mysql_fetch_assoc($res);
			//เก็บไอดีไว้ อัพเดทวันทีโหลดไฟล์
			$arr_idLoad[] = $fet_dn['id'];
			// เก็บเข้า row CSV
			$col = $expVins[$i];
			$sign = '|';
			$expANS1 = explode(",",$fet_dn['list_answer_value']);

			if($fet_dn['Date_Download']==''){ //ยังไม่เคยถูก โหลดไฟล์
				for($j=0;$j<count($expANS1);$j++){
					$expANS2 = explode("=",$expANS1[$j]);
					$col .= $sign.iconv("UTF-8", "TIS-620", $expANS2[1]);
				}
			}else{ //เคยถูกโหลดไฟล์ไปแล้ว
				for($j=0;$j<count($exp_Headquestion);$j++){
					$x_cd = explode("=",$exp_Headquestion[$j]);//แตกคำถาม
					$Quset_in = 'N';

					for($ss=0;$ss<count($expANS1);$ss++){
						$expANSs = explode("=",$expANS1[$ss]); //แตกคำตอบ
						if($x_cd[1]==$expANSs[0]){
							$Quset_in='Y';
						}
					}

					$expANS2 = explode("=",$expANS1[$j]); //แตกคำตอบ
					if($Quset_in == 'Y'){
						$col .= $sign.iconv("UTF-8", "TIS-620", $expANS2[1]);
					}else{
						$col .= $sign.' ';
					}
				}
			}
			$csv = $col."\n";
			fwrite($objWrite, $csv);
			//สิ้นสุดส่วนของการเขียนไฟล์
		}
		
		if( ! $csv ){//ถ้าไม่ได้ระบุวันที่ หรือไม่พบข้อมูล
			fwrite($objWrite, iconv("UTF-8", "TIS-620", 'No selected data . (ไม่พบข้อมูลที่ต้องการโหลด คุณอาจลืม!เลือกรายการก่อนดาวโหลดครับ)'));
		}else{
			//เมื่อพร้อมให้โหลดไฟล์แล้ว อัพเดทวันที่โหลดลงไป เพื่อไม่ให้โหลดข้อมูลมาแก้ไขอีก
			for($i=0;$i<count($arr_idLoad);$i++){
				$sqlchk = "SELECT Date_Download FROM $config[db_base_name].D_NET WHERE id = '$arr_idLoad[$i]'";
				$reschk = $logDb->queryAndLogSQL( $sqlchk, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$fetchk = mysql_fetch_assoc($reschk);
				
				if($fetchk['Date_Download']  == ''){
					$sql_download_stamp = "UPDATE $config[db_base_name].D_NET SET Date_Download = '$today_date' WHERE id = '$arr_idLoad[$i]'";
				}else{
					$dateDownload = $fetchk['Date_Download'].','.$today_date;
					$sql_download_stamp = "UPDATE $config[db_base_name].D_NET SET Date_Download = '$dateDownload' WHERE id = '$arr_idLoad[$i]'";
				}
				$logDb->queryAndLogSQL( $sql_download_stamp, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			}
		}
		
		fclose($objWrite);
		header("Content-Type: application/force-download");
		header('Content-Disposition: attachment; filename="'. $filename .'"');#ชื่อไฟล
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ". @filesize($filePath));
		set_time_limit(0);
		@readfile($filePath) or die("File not found.");
	break;
}

CloseDB();
?>