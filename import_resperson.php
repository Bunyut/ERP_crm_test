<?php
/**
 * Create by: Nuy
 * Create date: 11.08.2014
 * Last Modify: 11.08.2014@Nuy
 * Description: หน้าอัพโหลดไฟล์ผู้รับผิดชอบลูกค้า
 */
 
$thisPage 	= "import_resperson";
 
require("config/general.php");
require("function/general.php");
require("function/".$thisPage.".php");
require("function/read_files.class.php");
require("inc/manageMain_data.php");

## สั่งทำงานโค้ด
$doCode = ""; 		## ทำงาน
// $doCode = "No";  ## ไม่ทำงาน แสดงโค้ด Insert, Update, Delete
// $doCode = "All"; ## ทำงาน แสดงโค้ด Select, Insert, Update, Delete

Conn2DB();

$func 		= $_REQUEST['func'];
$objClist	= new importClist;

switch($func){
	default:
		echo $tpl->tbHtml( $thisPage.'.html', 'FORM' );
	break;
	case 'upload':
	
		$arrJson = array('status' => true, 'html' => '', 'type' => '');
		$type	= pathinfo( $_FILES['file']['name'], PATHINFO_EXTENSION ) ;
		
		if($type != 'xls' && $type != 'xlsx' && $type != 'csv'){
			$arrJson['status']	= false;
			$arrJson['html']	= 'ประเภทไฟล์ที่รองรับได้แก่ .xlsx, .xls, .csv';
			$arrJson['type']	= '';
		}else{
			if($_FILES['file']['name'] != ""){
				$fileTemp = $_FILES["file"]["tmp_name"];
				$path = "libraries/Multiple_file_upload/uploads/".date('Y-m-d');
				$filemame = date("hisa").'_'.$_FILES["file"]["name"];
				
				if( !file_exists( __DIR__ .'/'.$path) ){
					mkdir(__DIR__ .'/'.$path, 0700);
				}

				$filePath = $path."/".$filemame ;
				
				if(move_uploaded_file($fileTemp, $filePath)){
					$arrJson['status']	= true;
					$arrJson['html']	= $filePath;
					$arrJson['type']	= $type;
					// echo($filemame);
				}
			}
		}
		
		echo json_encode($arrJson);
	break;
	case 'list_data';
		$res = array('status'=>true, 'html'=>'', 'alert'=>'');
		$objFiles	= new ReadFiles;
	
		if($_GET['type'] == 'csv'){
			$resRead	= $objFiles->readCSV($_GET); // function/read_files.class.php
		}else if($_GET['type'] == 'xls'){
			$resRead	= $objFiles->readExcel($_GET); // function/read_files.class.php
		}else if($_GET['type'] == 'xlsx'){
			$resRead	= $objFiles->readExcel2007($_GET); // function/read_files.class.php
		}
		
		$listBody 		= '';
		$listRepeat 	= '';
		$listHist 		= '';
		$listNoCus 		= '';
		$listNoEmp 		= '';
		$listNoScName 	= '';
		
		$no		= 0; ## จำนวนทั้งหมด
		$noFail	= 0; ## จำนวนที่ไม่ถูกต้อง
		$noSucc	= 0; ## จำนวนที่ถูกต้อง
		foreach($resRead AS $key => $value){
			$no++;
			$optionEmp		= '';
			
			$value		= trimArrays($value);
			
			## กรณีเป็นรายการแรก
			if($no == 1){
				## ดึงฟิลด์ทีต้องมีในไฟล์ Clist
				$fieldChk	= $objFiles->fieldClist();
				
				## เช็คฟิลด์ในไฟล์มีครบหรือเปล่า
				$checkCl 	= $objFiles->checkFieldFiles($fieldChk, $value);
				
				## กรณีคอลัมน์ไม่ครบให้แจ้งเตือน และหยุดการทำงาน
				if($checkCl['status'] == false){
					$msgAlert		= implode(', ', $checkCl['error']);
					$res['alert']	.= "คอลัมน์ในไฟล์ไม่ถูกต้องตามที่กำหนด\nขาดคอลัมน์\n".$msgAlert."";
					break;
				}
			}
				
			## วันที่ทำการติดตาม
			$actionDate				= changeDateFormat($value['ActionDate']); // function/general.php
			$value['ActionDate']	= $actionDate['date'];
			
			## วันที่ส่งมอบ
			$DeliveryDate			= changeDateFormat($value['DeliveryDate']); // function/general.php
			$value['DeliveryDate']	= $DeliveryDate['date'];
			
			## วันที่ติดตาม
			$FollowDate				= changeDateFormat($value['FollowDate']); // function/general.php
			$value['FollowDate']	= $FollowDate['date'];
			$resp_date				= $value['FollowDate'];
			$txt_date				= dateChange($resp_date,'Th','-','/','-'); // function/general.php
			
			## ScName
			$ScName			= explode(':',$value['ScName']);
			$value['ScName']= $ScName[0];
			
			## แสดงผล
			{
				## ชื่อลูกค้า
				if(trim($value['PersonName'])){
					$cus_name 	= $value['PersonName'];
				}else{
					$cus_name 	= $value['CompanyName'];
				}
				
				## ประเภทการติดตาม
				$follow_type 	= $value['FollowType'];

				## ScName
				$sc_name 		= $value['ScName'];

				## สาขา
				$branch		 	= $value['BranchName'];

				## VIN
				$vin		 	= $value['VIN'];

				## หมายเหตุ
				$remark		 	= $value['Remark'];
				
				## รายละเอียด
				$detail		 	= $value['ContactDetail'];
				
				## ข้อสังเกตุ
				$note		 	= '';
			}
			
			## ค่าใน checkbox
			$arrVal		= $value;
			
			$value['ScName']						= trim($value['ScName']);
		
			## เช็คข้อมูลซ้ำ
			$arrCheck								= array();
			$arrCheck['where']['FollowDate']		= $value['FollowDate'];
			$arrCheck['where']['PersonName']		= $value['PersonName'];
			$arrCheck['where']['CompanyName']		= $value['CompanyName'];
			$arrCheck['where']['FollowType']		= $value['FollowType'];
			$arrCheck['where']['FollowPeriod']		= $value['FollowPeriod'];
			$arrCheck['where']['ScName']			= $value['ScName'];
			$arrCheck['where']['ActionDate']		= $value['ActionDate'];
			$arrCheck['where']['BranchName']		= $value['BranchName'];
			$arrCheck['where']['VIN']				= $value['VIN'];
			$arrCheck['where']['Engine']			= $value['Engine'];
			$arrCheck['where']['DeliveryDate']		= $value['DeliveryDate'];
			$arrCheck['where']['Phone']				= $value['Phone'];
			$arrCheck['where']['Remark']			= $value['Remark'];
			$arrCheck['where']['ContactDetail']		= $value['ContactDetail'];
			$arrCheck['where']['SQ1Score']			= $value['SQ1Score'];
			$arrCheck['where']['SQ2Score']			= $value['SQ2Score'];
			$arrCheck['where']['StatusId']			= $value['StatusId'];
			$arrCheck['where']['FollowUpStatus']	= $value['FollowUpStatus'];
			$arrCheck['other_where']				= 'LIMIT 1';
			$arrCheck['field']						= 'id';
			$chkRepeat								= $objClist->selectClistIOS($arrCheck);
			
			## ตรวจสอบประวัติขาย
			$listStatus		= true;
			if(empty($chkRepeat)){
				$resHistSale			= $objClist->checkHistorySale($value);
				
				## กรณีมีประวัติขาย
				if($resHistSale['res_status'] == true){
					## เช็คข้อมูลลูกค้า
					$arrCheck					= array();
					$arrCheck['where']['CusNo']	= $resHistSale[0]['cusNo'];
					$arrCheck['other_where']	= 'LIMIT 1';
					$arrCheck['field']			= 'CusNo, Resperson';
					$resCus						= $objClist->selectCustomer($arrCheck);
					
					## กรณีมีข้อมูลลูกค้า
					if(!empty($resCus)){
						$arrVal['cus_no']		= $resCus[0]['CusNo'];
						$listVal 				= json_encode($arrVal);
						$resCus['Resperson']	= trim($resCus[0]['Resperson']);
					
						## กรณี ScName มีค่า
						if($value['ScName']){
							## เช็คข้อมูลพนักงาน
							$arrCheck						= array();
							$arrCheck['where']['TIS_id']	= $value['ScName'];
							$arrCheck['other_where']		= 'AND emp_data.Prosonnal_Being != 3 AND emp_data.username != "#####Out Off###"';
							$arrCheck['field']				= 'emp_data.ID_Card, emp_data.Code, emp_data.TIS_id, emp_data.Name, emp_data.Surname, emp_data.Nickname, position.Name AS position';
							$resEmp							= $objClist->selectEmp($arrCheck);
							
							## กรณีมีข้อมูลพนักงาน
							if(!empty($resEmp)){
								## กรณีมีพนักงานมากกว่า 1
								if(count($resEmp)>1){
									
									$resEmp['no']		= $no;
									$resEmp['default']	= trim($ScName[1]);
									$resEmp['name']		= 'ScNames';
									$note				= $objClist->optionEmp($resEmp);
								
									$nameList			= 'lists[]';
									$listEmpRepeat		.= $tpl->tbHtml( $thisPage.'.html', 'LIST_BODY' );
									$listStatus			= false;
								}
							## กรณีไม่มีข้อมูลพนักงาน
							}else{
								$noFail++;
								$listNoEmp	.= $tpl->tbHtml( $thisPage.'.html', 'FAIL_BODY' );
								$listStatus	= false;
							}
						## กรณี ScName ไม่มีค่า
						}else{
							$noFail++;
							$listNoScName	.= $tpl->tbHtml( $thisPage.'.html', 'FAIL_BODY' );
							$listStatus		= false;
						}
					## กรณีไม่มีข้อมูลลูกค้า
					}else{
						$noFail++;
						$listNoCus	.= $tpl->tbHtml( $thisPage.'.html', 'FAIL_BODY' );
						$listStatus	= false;
					}
				## ไม่พบประวัติขาย
				}else{
					$noFail++;
					$listHist	.= $tpl->tbHtml( $thisPage.'.html', 'FAIL_BODY' );
					$listStatus	= false;
				}
			## มีข้อมูลซ้ำ
			}else{
				$noFail++;
				$listRepeat	.= $tpl->tbHtml( $thisPage.'.html', 'FAIL_BODY' );
				$listStatus	= false;
			}
			
			## ไม่เข้าเงื่อนไขอื่น
			if($listStatus == true){
				$resEmp['no']		= $no;
				$resEmp['default']	= trim($ScName[1]);
				$resEmp['name']		= 'ScName';
				$note				= $objClist->optionEmp($resEmp);
			
				$nameList			= 'list[]';
				$listBody			.= $tpl->tbHtml( $thisPage.'.html', 'LIST_BODY' );
			}
		}
		
		$changeAll		= '';

		## รายการที่มีในระบบแล้ว
		{
			$txtTB		= 'รายการที่มีในระบบแล้ว';
			$listH		= $tpl->tbHtml( $thisPage.'.html', 'FAIL_HEAD' );
		
			if($listRepeat){
				$listB 	= $listRepeat;
			}else{
				$listB 	= $tpl->tbHtml( $thisPage.'.html', 'NO_DATA' );
			}
			
			$list_tb	.= $tpl->tbHtml( $thisPage.'.html', 'TB' );
		}
		
		## รายการที่ไม่พบประวัติขาย
		{
			$txtTB		= 'ไม่พบประวัติขายในระบบ';
			$listH		= $tpl->tbHtml( $thisPage.'.html', 'FAIL_HEAD' );
		
			if($listHist){
				$listB 	= $listHist;
			}else{
				$listB 	= $tpl->tbHtml( $thisPage.'.html', 'NO_DATA' );
			}
		
			$list_tb	.= $tpl->tbHtml( $thisPage.'.html', 'TB' );
		}
		
		## รายการที่ไม่พบข้อมูลลูกค้า
		{
			$txtTB		= 'ไม่พบข้อมูลลูกค้าในระบบ';
			$listH		= $tpl->tbHtml( $thisPage.'.html', 'FAIL_HEAD' );
		
			if($listNoCus){
				$listB 	= $listNoCus;
			}else{
				$listB 	= $tpl->tbHtml( $thisPage.'.html', 'NO_DATA' );
			}
		
			$list_tb	.= $tpl->tbHtml( $thisPage.'.html', 'TB' );
		}
		
		## รายการที่ไม่มี ScName
		{
			$txtTB		= 'รายการที่ไม่มี ScName';
			$listH		= $tpl->tbHtml( $thisPage.'.html', 'FAIL_HEAD' );
		
			if($listNoScName){
				$listB 	= $listNoScName;
			}else{
				$listB 	= $tpl->tbHtml( $thisPage.'.html', 'NO_DATA' );
			}
		
			$list_tb	.= $tpl->tbHtml( $thisPage.'.html', 'TB' );
		}
		
		## รายการที่ไม่มีข้อมูลพนักงาน
		{
			$txtTB		= 'รายการที่ไม่พบข้อมูลพนักงาน';
			$listH		= $tpl->tbHtml( $thisPage.'.html', 'FAIL_HEAD' );
		
			if($listNoEmp){
				$listB 	= $listNoEmp;
			}else{
				$listB 	= $tpl->tbHtml( $thisPage.'.html', 'NO_DATA' );
			}
		
			$list_tb	.= $tpl->tbHtml( $thisPage.'.html', 'TB' );
		}
		
		## รายการที่มีข้อมูลพนักงาน หลายคน
		{
			$txtTB		= 'ScName ในระบบซ้ำ';
			$changeAll	= "checkAll('lists[]',this.value);";
			$listH		= $tpl->tbHtml( $thisPage.'.html', 'LIST_HEAD' );
		
			if($listEmpRepeat){
				$listB 	= $listEmpRepeat;
			}else{
				$listB 	= $tpl->tbHtml( $thisPage.'.html', 'NO_DATA' );
			}
		
			$list_tb	.= $tpl->tbHtml( $thisPage.'.html', 'TB' );
		}

		## รายการปกติ
		{
			$txtTB		= 'รายการที่นำเข้าปกติ';
			$changeAll	= "checkAll('list[]',this.value);";
			$listH		= $tpl->tbHtml( $thisPage.'.html', 'LIST_HEAD' );
		
			if($listBody){
				$listB 	= $listBody;
			}else{
				$listB 	= $tpl->tbHtml( $thisPage.'.html', 'NO_DATA' );
			}
		
			$list_tb	.= $tpl->tbHtml( $thisPage.'.html', 'TB' );
		}
		
		$noSucc	= $no - $noFail;
		
		if($res['alert']==''){
			$res['html']	= $tpl->tbHtml($thisPage.'.html','LIST_TB');
		}else{
			## ลบไฟล์ที่อัพโหลดขึ้นไป
			$res['html']	= $tpl->tbHtml($thisPage.'.html','NO_DATA');
			unlink($_GET['filename']);
		}
		
		echo json_encode($res);
		
	break;
	case 'save':
	
		$objDB	= new manageDb;
		$data 	= json_decode($_POST['data'], true);
		
		##บันทึกแบบปกติ
		if(!empty($data['list'])){
			foreach($data['list'] AS $key=>$value){
				$listVal	= json_decode($value, true);
				$scVal		= json_decode($data['ScName'][$key], true);
				
				## เช็คว่ามีการแก้ไขหรือไม่
				$chkResp				= array();
				$chkResp['cus_no']		= $listVal['cus_no'];
				$chkResp['emp_no']		= $scVal['ID_Card'];
				$chkResp['position']	= $scVal['Code'];
				$chkResp['resp_date']	= $listVal['FollowDate'];
				$chkCus					= $objClist->checkRespMainCus($chkResp);
			
				if($chkCus == false){
					## บันทึกข้อมูลตาราง main_cusdata
					## จะ sync ข้อมูลลูกค้ากลาง
					$arrResp			= $chkResp;
					$arrResp['sync']	= true;
					$objClist->saveReperson($arrResp); ## function/import_resperson.php
					
					CloseDB();
					Conn2DB();
				
					## ลบข้อมูลตาราง follow_customer สถานะ 0, 1
					$objClist->delFollowCustomerResp($chkResp); ## function/import_resperson.php
					
					## บันทึกลงตาราง Clist ของขาย
					$arrIns					= $listVal;
					unset($arrIns['position']);
					unset($arrIns['cus_no']);
					$arrIns['db']			= $config['db_easysale'].'.CList_IOS';
					$arrIns['insert_date']	= date('Y-m-d H:i:s');
					
					$insert	= $objDB->insertDb($arrIns);
					$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				}
			}
		}
		
		## บันทึกแบบ ScName ซ้ำ
		if(!empty($data['lists'])){
			foreach($data['lists'] AS $key=>$value){
				$listVal	= json_decode($value, true);
				$scVal		= json_decode($data['ScNames'][$key], true);
				
				## เช็คว่ามีการแก้ไขหรือไม่
				$chkResp				= array();
				$chkResp['cus_no']		= $listVal['cus_no'];
				$chkResp['emp_no']		= $scVal['ID_Card'];
				$chkResp['position']	= $scVal['Code'];
				$chkResp['resp_date']	= $listVal['FollowDate'];
				$chkCus					= $objClist->checkRespMainCus($chkResp);
			
				if($chkCus == false){
					## บันทึกข้อมูลตาราง main_cusdata
					## จะ sync ข้อมูลลูกค้ากลาง
					$arrResp			= $chkResp;
					$arrResp['sync']	= true;
					$objClist->saveReperson($arrResp); ## function/import_resperson.php
					
					CloseDB();
					Conn2DB();
				
					## ลบข้อมูลตาราง follow_customer สถานะ 0, 1
					$objClist->delFollowCustomerResp($chkResp); ## function/import_resperson.php
					
					## บันทึกลงตาราง Clist ของขาย
					$arrIns					= $listVal;
					unset($arrIns['position']);
					unset($arrIns['cus_no']);
					$arrIns['db']			= $config['db_easysale'].'.CList_IOS';
					$arrIns['insert_date']	= date('Y-m-d H:i:s');
					
					$insert	= $objDB->insertDb($arrIns);
					$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				}
			}
		}
		
		$res['status']	= true;
		$res['message']	= '';
		
		echo json_encode($res);
		
	break;
}

CloseDB();
?>