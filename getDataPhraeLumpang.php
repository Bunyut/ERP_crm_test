<?php
require_once ("config/general.php");
require_once ("function/general.php");

//ส่วนของหัวหน้า sale , รอสัมภาษณ์ , รออนุมัติยกเลิกการติดตาม
/****************************
* css = css/followcus.css   *
* 	    css/nz_button.css	*
* js  = java/followcus.js   *
****************************/

## สั่งทำงานโค้ด
$doCode = ""; 		## ทำงาน
// $doCode = "No";  ## ไม่ทำงาน แสดงโค้ด Insert, Update, Delete
// $doCode = "All"; ## ทำงาน แสดงโค้ด Select, Insert, Update, Delete

Conn2DB();

$thisPage = "getDataPhraeLumpang";

$objMain		 = 	new manageMainDB();		
$res			 = 	list_data($arr); ## function/ic_birthday.php
$tagHTML = '<table>';
$tagHTML .= '<tr style="background-color:#FFFFCC;color:#3300CC;" align="center">
					<td>วันที่เกิด</td>
					<td>หมายเลขลูกค้า</td>
					<td>คำนำหน้า</td>
					<td>ชื่อ - สกุล</td>
					<td>อาชีพ</td>
					<td>ที่อยู่</td>
					<td>ตำบล</td>
					<td>อำเภอ</td>
					<td>จังหวัด</td>
					<td>รหัสไปรษณีย์</td>
					<td>เกรด ลูกค้า</td>
					<td>เบอร์โทรบ้าน</td>
					<td>เบอร์โทรที่ทำงาน</td>
					<td>เบอร์โทรมือถือ</td>
					<td>ประวัติการขาย</td>
					<td>ประวัติการสนใจ(ครั้ง)</td>
					<td>ประวัติการจอง(คัน)</td>
					<td>ประวัติการขาย(คัน)</td>
		</tr>';
foreach($_SESSION['birthDayExcel'] AS $key=>$value){
	$tagHTML .= $value;
}
$tagHTML .= '<table>';
// echo htmlspecialchars($tagHTML);
		
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename="Data_Phrae_Lump.xls"');#ชื่อไฟล
$html = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv="Content-type" content="text/html;charset=UTF-8"></head><body>';
$html .= $tagHTML;
$html .= '</body></html>';
echo $html;
echo "<script type='text/javascript'>
window.open('$thisPage.php?action=excel','newWin1','height=50,width=50,top=0,left=0,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes,directories=no,status=no,titlebar=no');
</script>";


function list_data($arr){
	global $config, $logDb, $objMain;
		
	unset($_SESSION['birthDayExcel']);
	$_SESSION['birthDayExcel'] = array();
		
	$return['html']	= '';
	$sql = "SELECT ";
	$sql .= "MAIN_CUS_GINFO.DateOfBirth AS month, ";
	$sql .= "MAIN_CUS_GINFO.CusNo, ";
	$sql .= "MAIN_CUS_GINFO.Be, ";
	$sql .= "MAIN_CUS_GINFO.Cus_Name, ";
	$sql .= "MAIN_CUS_GINFO.Cus_Surename,";
	$sql .= "AMIAN_CUS_TYPE.CUS_TYPE_LEVEL_DESC ";
	$sql .= "FROM ";
	$sql .= "".$config['db_maincus'].".MAIN_CUS_GINFO ";
	$sql .= "INNER JOIN ".$config['db_maincus'].".MIAN_CUS_TYPE_REF ON MAIN_CUS_GINFO.CusNo = MIAN_CUS_TYPE_REF.CTYPE_REF_CUSNO "; 
	$sql .= "LEFT JOIN ".$config['db_maincus'].".AMIAN_CUS_TYPE ON MIAN_CUS_TYPE_REF.CTYPE_GRADE_REF = AMIAN_CUS_TYPE.CUS_TYPE_ID ";
	$sql .= "LEFT JOIN ".$config['db_maincus'].".MAIN_ADDRESS  ON MAIN_CUS_GINFO.CusNo =  MAIN_ADDRESS .ADDR_CUS_NO "; 
	$sql .= "WHERE MAIN_ADDRESS.ADDR_PROVINCE IN(42,40) ";
	$sql .= "GROUP BY MAIN_CUS_GINFO.CusNo ";
	$sql .= "ORDER BY ADDR_MAIN_ACTIVE DESC, ADDR_TYPE ASC ";
			
	mysql_query("SET NAMES utf8");
	$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );

	$total_num = mysql_num_rows($re);
		
	while($rs=mysql_fetch_array($re)){
		
		$mainType 			= array();
		
		$arrAdd['CusNo']	= $rs['CusNo'];
		$mainAdd			= $objMain->getMainCusAddress($arrAdd); ## ที่อยู่
			
		$arrTel['type']		= '1,2,3'; ## 1=มือถือ,2=ที่ทำงาน,3=บ้าน,4=fax,5=อื่นๆ
		$arrTel['CusNo']	= $rs['CusNo'];
		$mainTel			= $objMain->getTelePhoneNumber($arrTel); ## เบอร์โทรศัพท์

		$sqljob 	=	"SELECT MAIN_JOBS.JOB_NAME , MAIN_JOBS.JOB_DESC FROM ".$config['db_maincus'].".MAIN_JOBS WHERE MAIN_JOBS.JOB_CUS_NO = ".$rs['CusNo']." AND MAIN_JOBS.JOB_MAIN_ACTIVE = 1 AND MAIN_JOBS.JOB_STATUS != 99 ";
		$qryJob 	=	$logDb->queryAndLogSQL( $sqljob, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$mainJob 	=	mysql_fetch_assoc($qryJob);
			
			//modify pt 13-08-2011
		$sql = "SELECT COUNT(Int_Num) AS s
			FROM ".$config['db_easysale'].".CusInt
			WHERE Int_CusNo = '$rs[CusNo]'";
		$ci = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$total_ci = mysql_fetch_array($ci);

		$sql = "SELECT COUNT(Booking_no) AS b
			FROM ".$config['db_easysale'].".Booking
			WHERE B_CusNo = '$rs[CusNo]'";
		$bk = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$total_bk = mysql_fetch_array($bk);
		

		$sql = "SELECT COUNT(Sell_No) AS total_buy FROM ".$config['db_easysale'].".Sell WHERE substring_index(cusBuy,'_',1) = '$rs[CusNo]' ";
		$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$total_buy = mysql_fetch_array($result);
		if($total_buy['total_buy']){ $sales_history = "YES"; }else{ $sales_history = "NO"; }
		//$obj = mysql_fetch_object($result);
		//if(!empty($obj->Sell_No)){ $sales_history = "YES"; }else{ $sales_history = "NO"; }
		
		mysql_free_result($result);

		$dateMonth	= dateChange($rs['month'],'Th','-','/','-');
			
		$return['html'] .= '<tr>
		<td>'.$dateMonth.'</td>
		<td>'.$rs['CusNo'].'</td>
		<td>'.$rs['Be'].'</td>
		<td>'.$rs['Cus_Name'].' '.$rs['Cus_Surename'].'</td>
		<td>'.$mainJob['JOB_NAME'].' '.$mainJob['JOB_DESC'].'</td>
		<td>'.$mainAdd['add'].'</td>
		<td>'.$mainAdd['tum'].'</td>
		<td>'.$mainAdd['amp'].'</td>
		<td>'.$mainAdd['pro'].'</td>
		<td>'.$mainAdd['code'].'</td>
		<td>'.$rs['CUS_TYPE_LEVEL_DESC'].'</td>
		<td>'.$mainTel[3]['TEL_NUM'].'</td>
		<td>'.$mainTel[2]['TEL_NUM'].'</td>
		<td>'.$mainTel[1]['TEL_NUM'].'</td>
		<td>'.$sales_history.'</td>
		<td>'.$total_ci['s'].'</td>
		<td>'.$total_bk['b'].'</td>
		<td>'.$total_buy['total_buy'].'</td>
		</tr>';
	}
		
	$_SESSION['birthDayExcel'][] = $return['html'];
		
	$return['sDB']		= $arr['sDB'] + $arr['nDB']; 	## ค่าเริ่มต้นใหม่
	$return['status']	= ($total_num>0)? 'OK' : 'LIMIT' ;
	// $return['status']	= 'LIMIT' ;
		
	return $return;
}

CloseDB();
?>