<?php
/**
* Pagination Class
*
*
* @copyright Isuzu Chiangrai Group (2010)
* @author Benjarat Buathong
* @version 1.0
* @todo separate page
**/
require_once ("config/general.php");

class Pagination
{
	/**
    * Records Per Page
	*
    * @access public
    **/
	var $_recordPerPage= 20;
	
	/**
    * Total Pages
	*
    * @access public
    **/
	var $_totalPages= 0;
	
	/**
    * Start record
	*
    * @access public
    **/
	var $startRecord= 1;
	
	/**
    * End Record
	*
    * @access public
    **/
	var $endRecord = 20;
	
	/**
    * Current Page
	*
    * @access public
    **/
	var $currentPage= 1;
	
	/**
    * Start Page
	*
    * @access public
    **/
	var $startPage= 1;
	
	/**
    * End Page
	*
    * @access public
    **/
	var $endPage= 10;
	
	/*###################### constructor  ######################*/
	function __constructor()
	{
		//return $this->setPaginationPattern();
	}
	
	function setTotalPage($totalRecord)
	{
		$this->_totalPages = ceil($totalRecord / $this->_recordPerPage);
		//return $this->_totalPages;
	}
    
    function setRecordPerPage($recordNumber){
        $this->_recordPerPage = $recordNumber;
    }
    
    function getRecordPerPage(){
        return $this->_recordPerPage;
    }
	
	function setCurrentPage($page){
		$this->currentPage = $page;
	}
	
	function getCurrentPage(){
		return $this->currentPage;
	}
	/*#################  get & set start record   ######################*/
	function setStartRecord(){
		$this->startRecord = (($this->_recordPerPage * $this->getCurrentPage()) - $this->_recordPerPage);
	}
	
	function getStartRecord(){
		return $this->startRecord;
	}
	/*#################  get & set end record   ######################*/
	function setEndRecord(){
		$this->endRecord = $this->getCurrentPage()*$this->_recordPerPage;
	}
	
	function getEndRecord(){
		return $this->endRecord;
	}
	/*#################  get & set start page   ######################*/
	function setStartPage(){
		$this->startPage = $this->getCurrentPage() -5 ;
	}
	
	function getStartPage(){
		return $this->startPage;
	}
	
	/*#################  get & set end page   ######################*/
	function setEndPage($end){
		$endLimit =  $this->getCurrentPage() +5;
		if ($endLimit > $this->_totalPages){
			$endLimit = $this->_totalPages;
		}
		$this->endPage = $endLimit;
	}
	
	function getEndPage(){
		return $this->endPage;
	}

	function setPaginationPattern($div , $link){
		if($this->getCurrentPage() >= 10 ){
			$this->setStartPage($this->startPage);
			$this->setEndPage($this->endPage);
		}       

		if($this->_totalPages < $this->endPage){
			$this->setEndPage($this->_totalPages);
		}

		$html .= "<center> <a href=\"javascript:void(0);\" onclick=\"javascript:browseCheck('$div','$link&cpage=1');\" style='text-decoration:none;'> |<< </a> ";
			for($i = $this->getStartPage() ;$i<=($this->getEndPage());$i++){
				if($i == $this->currentPage){
					$html .= "&nbsp;<b>".($i)."</b>&nbsp;";
				}else{
					$html .= "<a href=\"javascript:void(0);\" onclick=\"javascript:browseCheck('$div','$link&cpage=$i');\" style='text-decoration:none;'><span id='paginationStyle' style='line-height:30px;'>&nbsp;".($i)."&nbsp;</span></a>";
				}
			}//end for
		$html .= " <a href=\"javascript:void(0);\" onclick=\"javascript:browseCheck('$div','$link&cpage=$this->_totalPages');\" style='text-decoration:none;'> >>| </a> ";
		$html .= "</center>";
		return $html;
	}
	
}
?>
