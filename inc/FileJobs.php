<?php
class FileJobs
{

  var $fileName;
	var $chmod  =	'0777';
	
  function setFileName( $sFileName ){
    $this->fileName = $sFileName;
  } // end function setFileName
	
 	function addFile( $sFileName = null ){
		
		if( isset( $sFileName ) )
			$this->setFileName( $sFileName );	

		if( is_file( $this->fileName ) )
			return false;
		else{
			@touch( $this->fileName );
			@chmod( $this->fileName, $this->chmod );
			if( is_file( $this->fileName ) )
				return true;
			else
				return false;
		}
	} // end function addFile

	function throwNameOfFile( $name ){
		$ext =	  explode( '.', $name );
    $iCount = count( $ext );
    unset( $ext[$iCount-1] );
    $sName = implode( '.', $ext );
    return $sName;
	} // end function throwNameOfFIle

	function throwExtOfFile( $name ){
		$ext =	explode( '.', $name );
		return	strtolower( $ext[count( $ext )-1] ); 
	} // end function throwExtOfFile

  function throwNameExtOfFile( $sName ){
    $aName[0] = $this->throwNameOfFile( $sName );
    $aName[1] = $this->throwExtOfFile( $sName );
    return $aName;
  } // end function throwNameExtOfFile

  function throwFile( $sFile ){
    if( is_file( $sFile ) ){
      $rFile =    fopen( $sFile, 'r' );
      $sContent = fread( $rFile, filesize( $sFile ) );
      fclose( $rFile );

      return $sContent;
    }
    else
      return null;
  } // end function throwFile

function checkCorrectFile( $name, $is = 'jpg|jpeg' ){
		return preg_match( '/^('.$is.'|)$/', $this->throwExtOfFile( $name ) );
	} // end function checkCorrectFile

   function changeFileName( $sFileName ){

    $sFileName = ereg_replace( '\$', '_', $sFileName );
    $sFileName = ereg_replace( '\'', '`', $sFileName );
    $sFileName = ereg_replace( '\"', '`', $sFileName );
    $sFileName = ereg_replace( '\~', '_', $sFileName );
    $sFileName = ereg_replace( '\/', '_', $sFileName );
    $sFileName = ereg_replace( '\\\\', '_', $sFileName );
    $sFileName = ereg_replace( '\?', '_', $sFileName );
    $sFileName = ereg_replace( '#', '_', $sFileName );
    $sFileName = ereg_replace( '%', '_', $sFileName );
    $sFileName = ereg_replace( '\+', '_', $sFileName );

    $sFileName = changePolishToNotPolish( $sFileName );

    return $sFileName;
  } // end function changeFileName

  function checkIsFile( $sFileOutName, $sOutDir = '', $sExt = null ){
    
    $sFileName = $sFileOutName;

    for( $i = 1; is_file( $sOutDir.$sFileOutName ); $i++ )
      $sFileOutName = basename( $sFileName, '.'.$sExt ).'['.$i.'].'.$sExt;

    return $sFileOutName;
  } // end function checkIsFile

 function uploadFile( $aFiles, $sOutDir = null, $sFileOutName = null ){
    $sUpFileSrc =   $aFiles['tmp_name'];

    $sUpFileName =  $this->changeFileName( $aFiles['name'] );
    $sExtFile =     $this->throwExtOfFile( $aFiles['name'] );

    if( !isset( $sFileOutName ) )
      $sFileOutName = $sUpFileName;

    $sFileOutName = $this->checkIsFile( $sFileOutName, $sOutDir, $sExtFile );

    if( move_uploaded_file( $sUpFileSrc, $sOutDir.$sFileOutName ) ){
     // chmod( $sOutDir.$sFileOutName, 0777 );
      return $sFileOutName;
    }
    else
      return null; 
  } // end function uploadFile

};
?>