<?php
class genMenu{
	/*****************/
	private $status = 1;
	private $programID = 29;
	private $dataBase = 'icchecktime2';
	private $dbManage = 'menu_manage';
	private $dbProcess = 'menu_process';
	private $dbMenu = 'menu';
	private $chkAdmin = false;
	private $chkMenu = false;
	private $headChk = 0;
	/*****************/
	private $idCard = null;
	private $company = null;
	private $department = null;
	private $section = null;
	private $position = null;
	/*****************/
	
	private function regular_links($link){
		$patt = "/(onClick|onclick)=\"(?<str>.*)(;\")>/";
		if(preg_match_all($patt,$link, $matches)){
			#print_r($matches);
			$return = 'onclick="'.$matches['str'][0].'"';
		}else{
			$return = '';
		}
		
		return $return;
	}
	
	private function regular_href($link){
		$return = str_replace('href="javascript:void(0);"','',$link);
		
		return $return;
	}
	
	public function chkADMIN($arr){
		if(count($arr)>0 && $arr){
			foreach($arr AS $key=>$value){
				foreach($value AS $keys=>$values){
					if($_SESSION[$keys]==$values){
						$this->chkAdmin = true;
						break;
					}
				}
			}
		}
		
		return $this->chkAdmin;
		/*if($this->chkAdmin){
			echo "<br/><br/>Admin = Y";
		}else{
			echo "<br/><br/>Admin = N";
		}*/
	}
	
	public function genMenu_programe($programID=29,$dataBase='icchecktime2',$arrADMIN=null){
		/****************************/
		$this->programID = $programID;
		$this->dataBase = $dataBase;
		$this->dbManage = $dataBase.".".$this->dbManage;
		$this->dbMenu = $dataBase.".".$this->dbMenu;
		$this->dbProcess = $dataBase.".".$this->dbProcess;
		/****************************/
		$this->idCard = $_SESSION['SESSION_ID_card'];
		$this->company = $_SESSION['SESSION_Working_Company'];
		$this->department = $_SESSION['SESSION_Department'];
		$this->section = $_SESSION['SESSION_Section'];
		$this->position = $_SESSION['SESSION_Position_id'];
		/****************************/
		
		if(count($arrADMIN)>0 && $arrADMIN){
			$this->chkADMIN($arrADMIN);
		}
		$menu = $this->genMenu_listID();
		return $menu;
	}
	
	public function chkMenu_generalPage($programID,$dataBase,$page,$type=null){
		if($programID){
			// echo "<br/><br/>".$programID.",".$dataBase.",".$page;
			/****************************/
			$this->programID = $programID;
			$this->dataBase = $dataBase;
			$this->dbManage = $dataBase.".".$this->dbManage;
			$this->dbMenu = $dataBase.".".$this->dbMenu;
			$this->dbProcess = $dataBase.".".$this->dbProcess;
			/****************************/
			$this->idCard = $_SESSION['SESSION_ID_card'];
			$this->company = $_SESSION['SESSION_Working_Company'];
			$this->department = $_SESSION['SESSION_Department'];
			$this->section = $_SESSION['SESSION_Section'];
			$this->position = $_SESSION['SESSION_Position_id'];
			/****************************/
			
			$chk = "SELECT ".$this->dbMenu.".id,".$this->dbMenu.".link FROM ".$this->dbMenu." WHERE ".$this->dbMenu.".place_id='".$this->programID."' AND ";
			$chk .= $this->dbMenu.".link LIKE '%".$page."%' AND ".$this->dbMenu.".status!='99' LIMIT 1";
			// echo "<br/><br/>".$chk;
			$qChk = mysql_query($chk) or die("<br>".$chk."<br>Error : ".__FILE__." Line : ".__LINE__."<br>".mysql_error());
			$fChk = mysql_fetch_assoc($qChk);
			
			if(!$fChk['id']){
				return $this->chkUser_menu($fChk['id'], $type);
			}else{
				return $this->chkUser_menu($fChk['id'], $type);
			}
		}else{
			return true;
		}
	} 
	
	private function chkMenu($id){
		$chk = "SELECT ".$this->dbMenu.".id,".$this->dbMenu.".link FROM ".$this->dbMenu." WHERE ".$this->dbMenu.".place_id='".$this->programID."' AND ";
		$chk .= $this->dbMenu.".parent_id='".$id."' AND ".$this->dbMenu.".status!='99' LIMIT 1";
		//echo "<br/><br/>".$chk;
		$qChk = mysql_query($chk) or die("<br>".$chk."<br>Error : ".__FILE__." Line : ".__LINE__."<br>".mysql_error());
		$fChk = mysql_fetch_assoc($qChk);
		
		if(!$fChk['id']){
			return $this->chkUser_menu($id);
		}else{
			/*if($fChk['link']){
				return $this->chkUser_menu($id);
			}else{
				return $this->chkUser_menu($id);
			}*/
			return true;
		}
	}
	
	private function chkUser_menu($id,$type=null){
		if(!$this->chkAdmin){
			if ($type == null) {
				$where = "(".$this->dbManage.".id_card='".$this->idCard."' OR ";
				$where .= $this->dbManage.".working_company_id='".$this->company."' OR ";
			}else {
				$where = "(".$this->dbManage.".working_company_id='".$this->company."' OR ";
			}
			$where .= $this->dbManage.".department_id='".$this->department."' OR ";
			$where .= $this->dbManage.".section_id='".$this->section."' OR ";
			$where .= $this->dbManage.".position_id='".$this->position."') AND";
		
			$sql = "SELECT id FROM ".$this->dbManage." WHERE $where ".$this->dbManage.".menu_id='".$id."' AND ".$this->dbManage.".status!='99' LIMIT 1";
			// echo "<br/><br/>".$sql;
			$que = mysql_query($sql) or die("<br>".$sql."<br>Error : ".__FILE__." Line : ".__LINE__."<br>".mysql_error());
			$fe = mysql_fetch_assoc($que);
			
			if($fe['id']){
				return true;
			}else{
				return false;
			}
		}else{
			return true;
		}
	}
	
	public function chkSession_login(){
		/****************************/
		$this->idCard = $_SESSION['SESSION_ID_card'];
		$this->company = $_SESSION['SESSION_Working_Company'];
		$this->department = $_SESSION['SESSION_Department'];
		$this->section = $_SESSION['SESSION_Section'];
		$this->position = $_SESSION['SESSION_Position_id'];
		/****************************/
		
		$this->chkLogin();
	}
	
	private function chkLogin(){
		if(!$this->idCard || !$this->company || !$this->department || !$this->section || !$this->position){
			if(!$this->chkAdmin){
				session_destroy();
				$text_alert = 'alert("การเข้าสู่ระบบไม่ถูกต้อง ข้อมูลบางส่วนไม่ครบถ้วน กรุณาล็อคอินใหม่..."); window.location = "../login/";';
				echo '<script type="text/javascript">'.$text_alert.'</script>';
				exit();
			}
		}
	}
	
	private function genMenu_listID(){
		$this->chkLogin();
		
		$sql = "SELECT DISTINCT(".$this->dbMenu.".id),".$this->dbMenu.".parent_id,".$this->dbMenu.".name,".$this->dbMenu.".menu_desc,".$this->dbMenu.".link,";
		$sql .= $this->dbProcess.".process FROM ".$this->dbMenu." LEFT JOIN ".$this->dbProcess." ON ".$this->dbMenu.".process_id=".$this->dbProcess.".id WHERE ";
		$sql .= $this->dbMenu.".place_id='".$this->programID."' AND ".$this->dbMenu.".status!='99' AND ".$this->dbMenu.".parent_id='' ORDER BY ";
		$sql .= $this->dbMenu.".rankking ASC";
		//AND ".$this->dbMenu.".id='702'
		//echo $sql."<br/><br/>";
		$que = mysql_query($sql) or die("<br>".$sql."<br>Error : ".__FILE__." Line : ".__LINE__."<br>".mysql_error());
		
		$idMenu = array(); $no = 0;
		while($fe = mysql_fetch_assoc($que)){
			
			$chkMenu = $this->chkMenu($fe['id']);
			if($chkMenu){
				$this->headChk = $fe['id'];
				
				$idMenu['head']['id'][$fe['id']] = $fe['id'];
				$idMenu['head']['name'][$fe['id']] = $fe['name'];
				$idMenu['head']['desc'][$fe['id']] = $fe['menu_desc'];
				$idMenu['head']['link'][$fe['id']] = $fe['link'];
				$idMenu['head']['process'][$fe['id']] = $fe['process'];
				
				$idMenu = $this->genMenu_sub($fe['id'],$idMenu);
				//if(count($arrSub)>0){ $idMenu = array_merge($idMenu,$arrSub); }
			}
			
			$no++;
			if($no>1000){ echo '<br/><br/>OVER HEAT  : '.$sql.'<br/><br/>'; exit(); }
		}
		
		if(count($idMenu['head']['id'])==0){
			$idMenu['head']['id'][0] = 0;
			$idMenu['head']['name'][0] = ' คุณไม่สามารถใช้งานเมนู ';
			$idMenu['head']['desc'][0] = ' คุณไม่สามารถใช้งานเมนู ';
			$idMenu['head']['link'][0] = '#';
		}
		
		$menu = $this->genMenu_list($idMenu);
		return $menu;
	}
	
	private function genMenu_sub($id,$idMenu=null){
		
		$sql = "SELECT DISTINCT(".$this->dbMenu.".id),".$this->dbMenu.".parent_id,".$this->dbMenu.".name,".$this->dbMenu.".menu_desc,".$this->dbMenu.".link, ";
		$sql .= $this->dbProcess.".process FROM ".$this->dbMenu." LEFT JOIN ".$this->dbProcess." ON ".$this->dbMenu.".process_id=".$this->dbProcess.".id WHERE ";
		$sql .= $this->dbMenu.".place_id='".$this->programID."' AND ".$this->dbMenu.".status!='99' AND ";
		$sql .= $this->dbMenu.".parent_id='".$id."' ORDER BY ".$this->dbMenu.".rankking ASC";
		//echo $sql."<br/><br/>";
		$que = mysql_query($sql) or die("<br>".$sql."<br>Error : ".__FILE__." Line : ".__LINE__."<br>".mysql_error());
		$row = mysql_num_rows($que);
		
		$chk = false;
		while($fe = mysql_fetch_assoc($que)){

			$chkMenu = $this->chkMenu($fe['id']);
			if($chkMenu){
				$idMenu['id'.$id]['id'][$fe['id']] = $fe['id'];
				$idMenu['id'.$id]['name'][$fe['id']] = $fe['name'];
				$idMenu['id'.$id]['desc'][$fe['id']] = $fe['menu_desc'];
				$idMenu['id'.$id]['link'][$fe['id']] = $fe['link'];
				$idMenu['id'.$id]['process'][$fe['id']] = $fe['process'];
			
				$idMenu = $this->genMenu_sub($fe['id'],$idMenu);
				//if(count($arrSub)>0){ $idMenu = array_merge($idMenu,$arrSub); }
				
				$chk = true;
			}
		}
		
		if($chk==false && $row>0){
			unset($idMenu['head']['id'][$this->headChk]);
			unset($idMenu['head']['name'][$this->headChk]);
			unset($idMenu['head']['desc'][$this->headChk]);
			unset($idMenu['head']['link'][$this->headChk]);
			unset($idMenu['head']['process'][$this->headChk]);
		}
		
		return $idMenu;
	}
	
	private function genMenu_list($idMenu){
		
		foreach($idMenu['head']['id'] AS $key=>$value){
			$numArr = count($idMenu[$value]['id']);
			
			if($idMenu['head']['link'][$value] && $numArr==0){
				$url = $idMenu['head']['link'][$value];
				$link = str_replace('$url', $url, str_replace('\"','"',$idMenu['head']['process'][$value]));
				$link = $this->regular_href($link);
				$onclick = $this->regular_links($link);
				
				//$menu .= '<li title="'.$idMenu['head']['desc'][$value].'" '.$onclick.'>'.$link.''.$idMenu['head']['name'][$value].'</a></li>';
				$menu .= '<li title="'.$idMenu['head']['desc'][$value].'" '.$onclick.'>'.$idMenu['head']['name'][$value].'</li>';
			}else{
				$menu .= '<li title="'.$idMenu['head']['desc'][$value].'">'.$idMenu['head']['name'][$value];
				$menu .= $this->genMenu_subList($idMenu,$value);
				$menu .= '</li>';
			}
		}
		
		return $menu;
	}
	
	private function genMenu_subList($idMenu,$idMain){
		if(count($idMenu['id'.$idMain]['id'])>0){
			$menu .= '<ul style="width: 220px;">';
			foreach($idMenu['id'.$idMain]['id'] AS $keys=>$values){
				if(!$idMenu['id'.$idMain]['link'][$values]){ 
					//$url = $idMenu['id'.$idMain]['link'][$values] = '#';
					$link = '<a>'; 
				}else{ 
					$url = $idMenu['id'.$idMain]['link'][$values]; 
					$link = str_replace('$url', $url, str_replace('\"','"',$idMenu['id'.$idMain]['process'][$values]));
					$link = $this->regular_href($link);
					$onclick = $this->regular_links($link);
				}
				
				$menu .= '<li title="'.$idMenu['id'.$idMain]['desc'][$values].'">'.$link.''.$idMenu['id'.$idMain]['name'][$values].'</a>';
				// $menu .= '<li title="'.$idMenu['id'.$idMain]['desc'][$values].'" '.$onclick.'>'.$idMenu['id'.$idMain]['name'][$values].'';
				
				$menu .= $this->genMenu_subList($idMenu,$values);
				$menu .= '</li>';
			}
			$menu .= '</ul>';
		}
		
		return $menu;
	}
	
	public function chkPage_out($programID,$dataBase){
		$this->programID = $programID;
		$this->dataBase = $dataBase;
		$this->dbManage = $dataBase.".".$this->dbManage;
		$this->dbMenu = $dataBase.".".$this->dbMenu;
		//$this->dbProcess = $dataBase.".".$this->dbProcess;
		/****************************/
		$this->idCard = $_SESSION['SESSION_ID_card'];
		$this->company = $_SESSION['SESSION_Working_Company'];
		$this->department = $_SESSION['SESSION_Department'];
		$this->section = $_SESSION['SESSION_Section'];
		$this->position = $_SESSION['SESSION_Position_id'];
		/****************************/
		$alert = 'Y';
		
		$page = substr($_SERVER["PHP_SELF"],strrpos($_SERVER["PHP_SELF"],"/")+1);

		$sql = "SELECT ".$this->dbMenu.".id FROM ".$this->dbMenu." WHERE ".$this->dbMenu.".place_id='".$this->programID."' AND ";
		$sql .= $this->dbMenu.".status!='99' AND ".$this->dbMenu.".link LIKE '%".$page."%' ORDER BY ".$this->dbMenu.".rankking ASC LIMIT 1";
		//echo $sql."<br/><br/>";
		$que = mysql_query($sql) or die("<br>".$chk."<br>Error : ".__FILE__." Line : ".__LINE__."<br>".mysql_error());
		$fe = mysql_fetch_assoc($que);
		
		if($fe['id']){
			$chk = $this->chkUser_menu($fe['id']);
			
			if($chk){
				$alert('N');
			}
		}
		
		if($alert=='Y'){
			session_destroy();
			$text_alert = 'alert("การเข้าสู่ระบบไม่ถูกต้อง ข้อมูลบางส่วนไม่ครบถ้วน กรุณาล็อคอินใหม่..."); window.location = "../login/";';
			echo '<script type="text/javascript">'.$text_alert.'</script>';
		}
	}
}
?>
