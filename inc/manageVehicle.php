<?php
class manageMain_data // begin class manageMain_data
{
	var $gb;
	var $config;
	var $server;
	var $conn;
	var $chk_result; //ผลการค้นหาข้อมูล
	var $idRef; //ไอดี ของฐานข้อมูลหลัก
	var $BU; //มาจากโปรแกรมไหน
	var $customer; //ข้อมูลลูกค้า
	var $vehicle; //ข้อมูลรถ
	var $relation; //ข้อมูลความสัมพันธ์รถกับคน
	var $brand; //ยี่ห้อรถ
	var $comp; //บริษัทผู้จำหน่ายรถ
	
	/**
	*
	*			gQueryAndLogUpdate
	*			$atc		=	0:insert, 1:update, 2:delete
	*			$note   	=  comment
	*			$sql 		=   sql || last_insert_id
	*			$tb_name = ชื่อ ตารางที่จะ insert
	*/
	function gQueryAndLogUpdate($atc,$sql,$note="",$tb_name="",$run_id_name=""){
		$sql_log = "";
		#echo $sql.'<br><br>';
		#$sql = "UPDATE main_cusdata.MAIN_ADDRESS SET ADDR_VILLAGE='ป่าไผ่',ADDR_NUMBER='182 ม.4',ADDR_SUB_DISTRICT='5952_เมือง' , ADDR_DISTRICT='661_พาน' , ADDR_PROVINCE='45_ เชียงราย',ADDR_POSTCODE='57120',ADDR_GEOGRAPHY='1_ภเหนือ',ADDR_GPS_LATITUDE='123456789',ADDR_GPS_LONGTITUDE='987654321',ADDR_MAP_DESC='' WHERE ADDR_ID_NUM='3' LIMIT 1";
		switch($atc){
			case '0':
			//insert
				/*$last_id = $sql;
				if($last_id=="" && $tb_insert == ""){
					echo "error not have last_insert_id ro tb_name";
					exit();
				}*/
				
				list($db,$tb) = explode(".",$tb_name);
		
				#value is updated
				//$run_id_name = ($run_id_name == "") ? "id":$run_id_name;
				$sql = "SELECT * FROM $db.".$tb." WHERE $run_id_name limit 1";
				//echo $sql.'|||';
				$rt_inslog = mysql_query($sql);
				while($r_inslog = @mysql_fetch_assoc($rt_inslog)){
					foreach($r_inslog AS $field => $val){
						$field_names .= $field.'#+';
						$value			.= $val.'#+$';
					}
				}
				
				$run_id_name = addslashes($run_id_name);
				$sql_log = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_LOGS (`databasename`,`tablename`,`id_t`,`emp_id`,`emp_time`,`log_status`,`fieldname`,";
				$sql_log .= "`before`,`afters`,`note`)VALUES( '".$db."','".$tb."','".$run_id_name."','".$_SESSION['SESSION_ID_card']."',NOW(),'0','".$field_names."',";
				$sql_log .= "'','".$value."','".$note."')";
				#field name
				//echo $sql_log;
				mysql_query($sql_log);
			break;
			case '1':
			//update
	
				$db 		=	"";
				$tb			=  "";
				$content	=  "";
				$where		=  "";
				$arr_exp = array();
				$arr_content = array();
				
				$sql_field 	= "";
				$sql_val 	= "";
				
				$sql_field_log 			= "";
				$sql_val_old_log 		= "";
				$sql_val_new_log 		= "";
				
			
				#$patt1  			=  "/^UPDATE(?<db>[ก-๙\sa-z_\d\s]+).(?<tb>[ก-๙\sa-z_\d\s]+)SET(?<content>[ก-๙\sa-z=\d\s',_]+)/";
				$patt1  			=  "/^UPDATE(?<db>[ก-๙\sa-z_\d\s.A-Z+\-]+)SET(?<content>[เก-๙\sa-z=\d\s'\.@,_A-Z\-:+]+)/";
				$patt2     		= "/'[\s]*,/";
				#$patt1  			=  "/^UPDATE(?<db>[ก-๙\sa-z_\d\s]+)[.]*(?<tb>[ก-๙\sa-z_\d\s]+)SET(?<content>[ก-๙\sa-z=\d\s',_]+)/";
				$synt = explode('WHERE',$sql);
				#echo $synt[0]."<br><br>";
				if(preg_match($patt1,$synt[0], $matches)){
					
					$ar_dbtb = explode('.',$matches['db']);
					if(count($ar_dbtb) == 1){
							if($this->config['cusNew']['db']){
								$db 		= 	$this->config['cusNew']['db'];
								$tb			=	$ar_dbtb[0];
							}else{
								$db 		= 	$this->config['vehicle']['db'];
								$tb			=	$ar_dbtb[0];
							}
					}else{
						$db 		= 	$ar_dbtb[0];
						$tb			=	$ar_dbtb[1];
					}
					$content	= $matches['content'];
					#echo $content.'<br>';
					$arr_exp = preg_split($patt2, $content);
					
					/* echo '<textarea style="width:300px;height:300px">';
					print_r($arr_exp);
					echo '</textarea>';
					echo '<br><br>';  */
					
					$count_e =1;
					$count_max = count($arr_exp);
					foreach($arr_exp AS $no=>$data){
					
						$f_e = array();
						$f_e = explode('=', $data);
						$arr_content[$f_e[0]] = str_replace("'", "", $f_e[1]);
						
						$sql_field   			.=  ($count_e < $count_max)? "".trim($f_e[0]).",":"".trim($f_e[0])."";
						$sql_val 				.=  ($count_e < $count_max)? "'".trim($f_e[1])."','":"'".trim($f_e[1])."'";
						$sql_field_log   		.=  ($count_e < $count_max)? "".trim($f_e[0])."#+":"".trim($f_e[0])."";
						$sql_val_new_log 	.=  ($count_e < $count_max)? "".trim(str_replace("'", "", $f_e[1]))."#+":"".trim(str_replace("'", "", $f_e[1]))."";
						$count_e++;
					}
					
				}else{
					echo "sql error:".$sql;
				}
				#	หา run_no
				#echo 'db:'.$db.'<br>tb:'.$tb;
				
				#$db_and_tb = ($db != '')? $db.".".$tb:$tb;
				$sql_r = "SHOW COLUMNS FROM ".$db.".".$tb;
				#echo "<br>".$sql_r."<br>";
				#echo "sql error:".$sql.'<br>';
				$rt	=	mysql_query($sql_r);
				while ($get_info = mysql_fetch_row($rt)) {
						
					$field_no 				=  $get_info[0];
					break;
				}
				#--
				
				#exit();
				$sql_select = "SELECT ".$field_no.",".$sql_field." FROM ".$db.".".$tb."  WHERE ".$synt[1];
				#echo "select:".$sql_select."<br>";
				$rt_select 	= mysql_query($sql_select);
				while($r_select	= @mysql_fetch_array($rt_select))
				{
						$id_run		= $r_select[$field_no];
						
						$arr_sql_field = explode(',',$sql_field);
						$count_e = 1;
						$count_max  = count($arr_sql_field);
						$sql_val_old_log	= "";
						#	หาข้อมูลเดิม
						foreach($arr_sql_field AS $e_no=>$f_n){
						
							$sql_val_old_log	 	.=  ($count_e < $count_max)? "".$r_select[trim($f_n)]."#+":"".$r_select[trim($f_n)]."";
							$count_e++;
						}
						#--
						#echo '$sql_val_old_log:'.$sql_val_old_log.'<br>';
						#echo '$sql_val_new_log:'.$sql_val_new_log.'<br>';
						$sql_log = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_LOGS (`databasename`,`tablename`,`id_t`,`emp_id`,`emp_time`,`log_status`,";
						$sql_log .= "`fieldname`,`before`,`afters`,`note`)VALUES( '".trim($db)."','".trim($tb)."','".$id_run."','".$_SESSION['SESSION_ID_card']."',";
						$sql_log .= "NOW(),'1','".$sql_field."','".$sql_val_old_log."','".$sql_val_new_log."','".$note."')";
						#field name
						#echo $sql_log."<br>";
						mysql_query($sql_log);
				}
				#print_r($arr_content);
			break;
			case '2':
			//delete
			
			break;
		}
	}
	
	function gQueryLogError($sql,$file){
		$sql = addslashes($sql);
		$file = addslashes($file);
		
		$insert = "INSERT INTO ".$this->config['cusNew']['db'].".LOGS_ERROR(ERROR_SQL,ERROR_FILE,EMP_ID,DATE_ADD)VALUES('".$sql."','".$file."',";
		$insert .= "'".$_SESSION['SESSION_ID_card']."',NOW())";
		mysql_query($insert);
	}
	
	//ข้อมูลรถ ## ฟิลด์ไหนที่ต้องการกำหนดค่าเองให้ใส่เครื่องหมาย ''
	function arrayVehicle(){
		{
			//ฟิลด์ ฐานข้อมูล VEHICLE_INFO ##main
			$this->vehicle['vehicle']['vehicle'] = array('tb'=>'VEHICLE_INFO','status'=>'VEHICLE_STATUS','id'=>'VEHICLE_ID','id_ref'=>'VEHICLE_ID',
			'0'=>'VEHICLE_BRAND_ID','1'=>'VEHICLE_BRAND_NAME','2'=>'VEHICLE_SERIES','3'=>'VEHICAL_CODE_COLOR','4'=>'VEHICLE_COLOR','5'=>'VEHICLE_ENGINES',
			'6'=>'VEHICLE_CHASSIS','7'=>'VEHICLE_REGIS','8'=>'VEHICLE_MODEL_ID','9'=>'VEHICLE_MODEL_NAME','10'=>'VEHICLE_MODEL_SVC','11'=>'VEHICLE_MODEL_CD',
			'12'=>'VEHICLE_MODEL_FULL','13'=>'VEHICLE_NUM_PERSON','14'=>'VEHICLE_OIL_TYPE','15'=>'VEHICLE_WHEELS_NUM','16'=>'VEHICLE_WEIGHTS','17'=>'VEHICLE_WLPWS',
			'18'=>'VEHICLE_PISTON','19'=>'VEHICLE_CC_NUM','20'=>'VEHICLE_POWERS','21'=>'VEHICLE_PICS','22'=>'VEHICLE_STATUS','23'=>'VEHICLE_MARKS',
			'24'=>'VEHICLE_ADD_DATE','25'=>'VEHICLE_ADD_PERSON');
			
			{
			//ฟิลด์ ฐานข้อมูล ahistory ##service เทียบกับ ##main
			//VEHICLE_BRAND_ID,VEHICLE_BRAND_NAME,VEHICLE_SERIES,VEHICLE_COLOR,VEHICLE_MODEL_ID,VEHICLE_MODEL_NAME,VEHICLE_MODEL_SVC,VEHICLE_MODEL_CD,VEHICLE_MODEL_FULL
			$this->vehicle['service']['vehicle'] = array('tb'=>'ahistory','status'=>'status','id'=>'id','id_ref'=>'idRef_vihicle','0'=>"''",'1'=>"''",'2'=>'',
			'3'=>"''",'4'=>"''",'5'=>'engin_id','6'=>'chassis','7'=>'register','8'=>"''",'9'=>"''",'10'=>"''",'11'=>"''",'12'=>"''",'13'=>'',
			'14'=>'','15'=>'','16'=>'','17'=>'','18'=>'','19'=>'','20'=>'','20'=>'','22'=>'status',
			'23'=>'comment_car','24'=>'add_time','25'=>'emp_id');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->vehicle['service']['vehicle']['chk'] = array('5','6');
			}
			
			if($BU=='sale'){
			//ฟิลด์ ฐานข้อมูล ahistory ##service เทียบกับ ## sale
			$this->vehicle['service']['sale'] = array('tb'=>'ahistory','status'=>'status','id'=>'id','id_ref'=>'idRef_vihicle','0'=>'abrand_id','1'=>'asell_com_id',
			'2'=>'agroup_id','3'=>'amodel_id','4'=>'model_name','5'=>'model','6'=>'MDL_CD','7'=>'fullmodel_name','8'=>'ayoung_type_id','9'=>'aunit_class_id',
			'10'=>'car_flash','11'=>'owner_id','12'=>'user_id','13'=>'cus_id','14'=>'driver_id','15'=>'register_status','16'=>'chassis','17'=>'engin_id',
			'18'=>'register','19'=>'register_province','20'=>'dn_code','21'=>'deliver_day','22'=>'color_id','23'=>'lotno','24'=>'year','25'=>'data_type','26'=>'level',
			'27'=>'mile_avg','28'=>'mile_last','29'=>'comment_car','30'=>'emp_id','31'=>'add_time','32'=>'status','33'=>'cvhc_list','34'=>'chk','35'=>'type_add',
			'36'=>'time_edit','37'=>'type_company','38'=>'idRef_vihicle');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->vehicle['service']['sale']['chk'] = array('16','17');
			}
		}
		
		{
			{
			//ฟิลด์ ฐานข้อมูล ahistory ##sale เทียบกับ ##main
			//VEHICLE_BRAND_ID,VEHICLE_BRAND_NAME,VEHICLE_SERIES,VEHICLE_COLOR,VEHICLE_MODEL_ID,VEHICLE_MODEL_NAME,VEHICLE_MODEL_SVC,VEHICLE_MODEL_CD,VEHICLE_MODEL_FULL
			$this->vehicle['sale']['vehicle'] = array('tb'=>'ahistory','status'=>'status','id'=>'id','id_ref'=>'idRef_vihicle','0'=>"''",'1'=>"''",'2'=>'',
			'3'=>"''",'4'=>"''",'5'=>'engin_id','6'=>'chassis','7'=>'register','8'=>"''",'9'=>"''",'10'=>"''",'11'=>"''",'12'=>"''",'13'=>'',
			'14'=>'','15'=>'','16'=>'','17'=>'','18'=>'','19'=>'','20'=>'','20'=>'','22'=>'status',
			'23'=>'comment_car','24'=>'add_time','25'=>'emp_id');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->vehicle['sale']['vehicle']['chk'] = array('5','6');
			}
			
			if($BU=='sale'){
			//ฟิลด์ ฐานข้อมูล ahistory ##sale เทียบกับ ## service
			$this->vehicle['sale']['service'] = array('tb'=>'Sell','status'=>'SDate_upstatus','id'=>'Sell_No','id_ref'=>'idRef_vihicle','0'=>"''",'1'=>'',
			'2'=>'','3'=>'','4'=>'','5'=>'','6'=>'','7'=>'','8'=>'','9'=>'','10'=>'','11'=>'','12'=>"''",'13'=>"''",'14'=>'','15'=>'','16'=>'Chassi_No_S',
			'17'=>'Eng_No_S','18'=>'','19'=>'','20'=>'','21'=>'deliver_day','22'=>'color_id','23'=>'lotno','24'=>'year','25'=>'data_type','26'=>'level',
			'27'=>'mile_avg','28'=>'mile_last','29'=>'comment_car','30'=>'emp_id','31'=>'add_time','32'=>'status','33'=>'cvhc_list','34'=>'chk','35'=>'type_add',
			'36'=>'time_edit','37'=>'type_company','38'=>'idRef_vihicle');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->vehicle['sale']['service']['chk'] = array('16','17');
			}
		}
	}
	
	//ข้อมูลความสัมพันธ์ รถกับคน ## ฟิลด์ไหนที่ต้องการกำหนดค่าเองให้ใส่เครื่องหมาย ''
	function arrayRelationship(){
		{
			//ฟิลด์ ฐานข้อมูล VEHICLE_RELATIONSHIP ##main
			$this->relation['vehicle']['vehicle'] = array('tb'=>'VEHICLE_RELATIONSHIP','status'=>'RELATES_STATUS','id'=>'RELATES_ID','id_ref'=>'RELATES_ID',
			'0'=>'RELATES_VEHICLE_ID','1'=>'RELATES_CUS_ID','2'=>'RELATES_TYPE','3'=>'RELATES_COMP_ID','4'=>'RELATES_MARKS','5'=>'RELATES_STATUS',
			'6'=>'RELATES_ADD_DATE','7'=>'RELATES_ADD_PERSON');
			
			{
			//ฟิลด์ ฐานข้อมูล ahistory ##service เทียบกับ ##main
			$this->relation['service']['vehicle'] = array('tb'=>'ahistory','status'=>'status','id'=>'id','id_ref'=>'idRef_vihicle','0'=>'idRef_vihicle','1'=>"''",
			'2'=>'','3'=>"''",'4'=>'','5'=>'status','6'=>'add_time','7'=>'emp_id','cancel_ref'=>'idRef_vihicle');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->relation['service']['vehicle']['chk'] = array('0','1');
			}
			
			if($BU=='sale'){
			//ฟิลด์ ฐานข้อมูล ahistory ##service เทียบกับ ## sale
			$this->relation['service']['sale'] = array('tb'=>'ahistory','status'=>'status','id'=>'id','id_ref'=>'idRef_vihicle','0'=>'abrand_id','1'=>'asell_com_id',
			'2'=>'agroup_id','3'=>'amodel_id','4'=>'model_name','5'=>'model','6'=>'MDL_CD','7'=>'fullmodel_name','8'=>'ayoung_type_id','9'=>'aunit_class_id',
			'10'=>'car_flash','11'=>'owner_id','12'=>'user_id','13'=>'cus_id','14'=>'driver_id','15'=>'register_status','16'=>'chassis','17'=>'engin_id',
			'18'=>'register','19'=>'register_province','20'=>'dn_code','21'=>'deliver_day','22'=>'color_id','23'=>'lotno','24'=>'year','25'=>'data_type','26'=>'level',
			'27'=>'mile_avg','28'=>'mile_last','29'=>'comment_car','30'=>'emp_id','31'=>'add_time','32'=>'status','33'=>'cvhc_list','34'=>'chk','35'=>'type_add',
			'36'=>'time_edit','37'=>'type_company','38'=>'idRef_vihicle','cancel_ref'=>'idRef_vihicle');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->relation['service']['sale']['chk'] = array('16','17');
			}
		}
		
		{
			{
			//ฟิลด์ ฐานข้อมูล ahistory ##sale เทียบกับ ##main
			$this->relation['sale']['vehicle'] = array('tb'=>'ahistory','status'=>'status','id'=>'id','id_ref'=>'idRef_vihicle','0'=>'idRef_vihicle','1'=>"''",
			'2'=>'','3'=>"''",'4'=>'','5'=>'status','6'=>'add_time','7'=>'emp_id','cancel_ref'=>'idRef_vihicle');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->relation['sale']['vehicle']['chk'] = array('0','1');
			}
			
			if($BU=='sale'){
			//ฟิลด์ ฐานข้อมูล ahistory ##sale เทียบกับ ## service
			$this->relation['sale']['service'] = array('tb'=>'ahistory','status'=>'status','id'=>'id','id_ref'=>'idRef_vihicle','0'=>'abrand_id','1'=>'asell_com_id',
			'2'=>'agroup_id','3'=>'amodel_id','4'=>'model_name','5'=>'model','6'=>'MDL_CD','7'=>'fullmodel_name','8'=>'ayoung_type_id','9'=>'aunit_class_id',
			'10'=>'car_flash','11'=>'owner_id','12'=>'user_id','13'=>'cus_id','14'=>'driver_id','15'=>'register_status','16'=>'chassis','17'=>'engin_id',
			'18'=>'register','19'=>'register_province','20'=>'dn_code','21'=>'deliver_day','22'=>'color_id','23'=>'lotno','24'=>'year','25'=>'data_type','26'=>'level',
			'27'=>'mile_avg','28'=>'mile_last','29'=>'comment_car','30'=>'emp_id','31'=>'add_time','32'=>'status','33'=>'cvhc_list','34'=>'chk','35'=>'type_add',
			'36'=>'time_edit','37'=>'type_company','38'=>'idRef_vihicle','cancel_ref'=>'idRef_vihicle');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->relation['sale']['service']['chk'] = array('16','17');
			}
		}
	}
	
	//ยี่ห้อรถ ## ฟิลด์ไหนที่ต้องการกำหนดค่าเองให้ใส่เครื่องหมาย ''
	function arrayBrand($BU){
		{
			//ฟิลด์ ฐานข้อมูล VEHICLE_BRANDS ##main
			$this->brand['vehicle']['vehicle'] = array('tb'=>'VEHICLE_BRANDS','status'=>'BRANDS_STATUS','id'=>'BRANDS_ID','id_ref'=>'BRANDS_ID','0'=>'BRANDS_NAME',
			'1'=>'BRANDS_MARK','2'=>'BRANDS_STATUS','3'=>'BRANDS_ADD_DATE','4'=>'BRANDS_ADD_PERSON');
			
			{
			//ฟิลด์ ฐานข้อมูล abrand ##service เทียบกับ ##main
			$this->brand['service']['vehicle'] = array('tb'=>'abrand','status'=>'status','id'=>'id','id_ref'=>'idRef_vihicle','0'=>'name','1'=>'','2'=>'status',
			'3'=>'add_time','4'=>'emp_id');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->brand['service']['vehicle']['chk'] = array('0');
			}
			
			if($BU=='sale'){
			//ฟิลด์ ฐานข้อมูล abrand ##service เทียบกับ ## sale
			$this->brand['service']['sale'] = array('tb'=>'abrand','status'=>'status','id'=>'id','id_ref'=>'idRef_vihicle','0'=>'name','1'=>'emp_id','2'=>'add_time',
			'3'=>'status','4'=>'idRef_vihicle');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->brand['service']['sale']['chk'] = array('0');
			}
		}
		
		{
			{
			//ฟิลด์ ฐานข้อมูล abrand ##sale เทียบกับ ##main
			$this->brand['sale']['vehicle'] = array('tb'=>'Main_Competitor','status'=>'Status','id'=>'ID','id_ref'=>'idRef_vihicle','0'=>'Name','1'=>'Remark',
			'2'=>'Status','3'=>'UpdateTime','4'=>'Updater');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->brand['sale']['vehicle']['chk'] = array('0');
			}
			
			if($BU=='sale'){
			//ฟิลด์ ฐานข้อมูล abrand ##sale เทียบกับ ## service
			$this->brand['sale']['service'] = array('tb'=>'Main_Competitor','status'=>'Status','id'=>'ID','id_ref'=>'idRef_vihicle','0'=>'Name','1'=>'Updater',
			'2'=>'UpdateTime','3'=>'Status','4'=>'idRef_vihicle');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->brand['sale']['service']['chk'] = array('0');
			}
		}
	}
	
	//บริษัทผู้จำหน่ายรถ ## ฟิลด์ไหนที่ต้องการกำหนดค่าเองให้ใส่เครื่องหมาย ''
	function arrayCompany(){
		{
			//ฟิลด์ ฐานข้อมูล VEHICLE_COMPANY ##main
			$this->comp['vehicle']['vehicle'] = array('tb'=>'VEHICLE_COMPANY','status'=>'SELLER_COMP_STATUS','id'=>'SELLER_COMP_ID','id_ref'=>'SELLER_COMP_ID',
			'0'=>'SELLER_COMP_NAME','1'=>'SELLER_COMP_BRANCH','2'=>'SELLER_COMP_ADD','3'=>'SELLER_COMP_MARK','4'=>'SELLER_COMP_STATUS','5'=>'SELLER_ADD_DATE',
			'6'=>'SELLER_ADD_PERSON');
			
			{
			//ฟิลด์ ฐานข้อมูล asell_company ##service เทียบกับ ##main
			$this->comp['service']['vehicle'] = array('tb'=>'asell_company','status'=>'status','id'=>'id','id_ref'=>'idRef_vihicle','0'=>'name','1'=>'','2'=>'address',
			'3'=>'','4'=>'status','5'=>'add_time','6'=>'emp_id');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->comp['service']['vehicle']['chk'] = array('0');
			}
			
			if($BU=='sale'){
			//ฟิลด์ ฐานข้อมูล asell_company ##service เทียบกับ ## sale
			$this->comp['service']['sale'] = array('tb'=>'asell_company','status'=>'status','id'=>'id','id_ref'=>'idRef_vihicle','0'=>'name','1'=>'','2'=>'address',
			'3'=>'','4'=>'status','5'=>'add_time','6'=>'emp_id','7'=>'idRef_vihicle');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->comp['service']['sale']['chk'] = array('0');
			}
		}
		
		{ ##sale
			{
			//ฟิลด์ ฐานข้อมูล abrand ##sale เทียบกับ ##main
			$this->comp['sale']['vehicle'] = array('tb'=>'asell_company','status'=>'status','id'=>'id','id_ref'=>'idRef_vihicle','0'=>'name','1'=>'','2'=>'address',
			'3'=>'','4'=>'status','5'=>'add_time','6'=>'emp_id');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->comp['sale']['vehicle']['chk'] = array('0');
			}
			
			if($BU=='sale'){
			//ฟิลด์ ฐานข้อมูล abrand ##sale เทียบกับ ## service
			$this->comp['sale']['service'] = array('tb'=>'asell_company','status'=>'status','id'=>'id','id_ref'=>'idRef_vihicle','0'=>'name','1'=>'','2'=>'address',
			'3'=>'','4'=>'status','5'=>'add_time','6'=>'emp_id','7'=>'idRef_vihicle');
			
			//ฟิลด์ เอาไว้เช็ค
			$this->comp['sale']['service']['chk'] = array('0');
			}
		}
	}
	
	function setConnect(){
		global $configBu; //global config
		$this->BU = $configBu;
		
		$this->server['ServerName']	= "localhost";
		$this->server['UserName'] = "root";
		$this->server['UserPassword'] = "123456";
		
		//if($BU!='main'){
			$this->config['vehicle']['db'] = 'MainVehicle';
			$this->config['vehicle']['sv'] = $this->server['ServerName'];
			$this->config['vehicle']['user'] = $this->server['UserName'];
			$this->config['vehicle']['pass'] = $this->server['UserPassword'];
		//if($BU!='service'){
		
		//if($BU!='service'){
			$this->config['service']['db'] = 'icservice';
			$this->config['service']['sv'] = $this->server['ServerName'];
			$this->config['service']['user'] = $this->server['UserName'];
			$this->config['service']['pass'] = $this->server['UserPassword'];
		//}
		
		//if($BU!='sale'){
			$this->config['sale']['db'] = 'new_icmba';
			$this->config['sale']['sv'] = $this->server['ServerName'];
			$this->config['sale']['user'] = $this->server['UserName'];
			$this->config['sale']['pass'] = $this->server['UserPassword'];
		//}
		
			$this->config['cusNew']['db'] = 'main_cusdata';
			$this->config['cusNew']['sv'] = $this->server['ServerName'];
			$this->config['cusNew']['user'] = $this->server['UserName'];
			$this->config['cusNew']['pass'] = $this->server['UserPassword'];
		
		{ //เปิดปิดฐานข้อมูล
			$this->disDB['vehicle'] = 'ON';
			$this->disDB['service'] = 'ON';
			$this->disDB['sale'] = 'ON';
		}
		
		{ //array ไว้เช็คตารางฐานข้อมูล
			$this->chkDB['vehicle'] = array('VEHICLE_INFO','VEHICLE_BRANDS','VEHICLE_COMPANY','VEHICLE_RELATIONSHIP','VEHICLE_REGIS');
			$this->chkDB['service'] = array('ahistory','abrand','asell_company');
			$this->chkDB['sale'] = array('Main_Competitor','Sell');
			$this->chkDB['cusNew'] = array('LOGS_ERROR','MAIN_LOGS');
		}
		
		$this->connect();
	}
	
	//เช็คฐานข้อมูล
	function chk_Data($BU){
		$return = false;
		
		$chkDB = "SHOW DATABASES LIKE '".$this->config[$BU]['db']."'";
		$queDB = mysql_query($chkDB);
		$feDB = @mysql_fetch_row($queDB);
		
		if(!$queDB){ $this->gQueryLogError($chkDB,'manageMain_data.php Line : '.__LINE__.''); }
		
		//เช็คฐานข้อมูล
		if($feDB[0]){
			foreach($this->chkDB[$BU] AS $key => $value){
				$chkTB = "SHOW TABLES FROM ".$this->config[$BU]['db']." LIKE '".$value."'";
				$queTB = mysql_query($chkTB);
				$feTB = @mysql_fetch_row($queTB);
				
				if(!$queTB){ $this->gQueryLogError($chkTB,'manageMain_data.php Line : '.__LINE__.''); }
				
				//เช็คตารางฐานข้อมูล
				if($feTB[0]){
					$return = true;
				}else{
					$return = false;
					break;
				}
			}
		}else{
			$return = false;
		}
		
		return $return;
	}
	
	function connect(){
		$arrCon = array();
		
		$i = 0;
		foreach($this->config AS $key => $value){
			if($this->disDB[$key]=='ON'){
				$chkArray = @array_search($this->config[$key]['user'],$arrCon['user']);
				
				if(!$chkArray){ $chkArray = 0; }
				if($this->config[$key]['sv']!=$arrCon['sv'][$chkArray] || $this->config[$key]['user']!=$arrCon['user'][$chkArray] || $this->config[$key]['pass']!=$arrCon['pass'][$chkArray]){
					$this->conn = mysql_connect($this->config[$key]['sv'],$this->config[$key]['user'],$this->config[$key]['pass']);
					if(!$this->conn);
					
					$arrCon['user'][$i] = $this->config[$key]['user'];
					$arrCon['pass'][$i] = $this->config[$key]['pass'];
					$arrCon['sv'][$i] = $this->config[$key]['sv'];
					
					$i++;
				}
				
				if($this->chk_Data($key)){
					mysql_select_db($this->config[$key]['db'],$this->conn);
					@mysql_db_query($this->config[$key]['db'], "SET NAMES UTF8");
					
					$this->disDB[$key] = 'ON'; 
				}else{
					$this->disDB[$key] = 'OFF';
				}
				//echo $key." : ".$this->disDB[$key]."<br/>";
			}
		}
	}
	
	//เช็คฐานข้อมูลว่ามีแล้วหรือยัง
	function chkSQL($arr,$skey,$id,$ch,$status){
		$BU = $this->BU;
		$chk_result = $this->chk_result;
		list($BUM,$keyM) = $this->returnKey($skey,$arr);
		
		//เช็คเฉพาะของขายเท่านั้น
		//if($BU=='sale'){
			foreach($arr[$BU][$skey]['chk'] AS $key => $value){
				if($key!=0){ $cmm = ","; $and = " AND "; }
				if($ch[$skey][$key]){
					$fValue = $ch[$skey][$key]; $fValue2 = preg_replace("/(^'{1}||'{1}$)/",'',$fValue);
				}else{
					$fValue = $arr[$BU][$skey][$value]; $fValue2 = $fValue;
				}
				
				$field .= $cmm.$fValue;
				$where .= $and."LOWER(".$arr[$BUM][$keyM][$value].")='##*-+".$fValue2."+-*##'";
			}
			
			if(!$chk_result){
				if($arr[$BU][$skey]['id_ref']){ $field .= ",".$arr[$BU][$skey]['id_ref']; }
				
				$whereChk = $arr[$BU][$skey]['id']."='".$id."' AND ".$arr[$BU][$skey]['status']."!='99'";
				$chk = "SELECT ".$field." FROM ".$this->config[$BU]['db'].".".$arr[$BU][$skey]['tb']." WHERE ".$whereChk." LIMIT 1";
				$query = mysql_query($chk);
				$feChk = @mysql_fetch_assoc($query);
				$feChk['where'] = $where;
				
				if(!$query){ $this->gQueryLogError($chk,'manageMain_data.php Line : '.__LINE__.''); }
				
				$chk_result = $this->chk_result = $feChk;
			}

			/*print_r($chk_result);
			echo "<br>".$arr[$BU][$skey]['id_ref'];
			echo "<br><br>";*/
			
			if($chk_result[$arr[$BU][$skey]['id_ref']]){
				
				$where1 = $arr[$BUM][$keyM]['id_ref']."='".$chk_result[$arr[$BU][$skey]['id_ref']]."' AND ".$arr[$BUM][$keyM]['status']."!='99'";
				$sql = "SELECT ".$arr[$BUM][$keyM]['id']." FROM ".$this->config[$skey]['db'].".".$arr[$BUM][$keyM]['tb']." WHERE ".$where1." LIMIT 1";
				$que = mysql_query($sql);
				$fe = @mysql_fetch_assoc($que);
				
				if(!$que){ $this->gQueryLogError($sql,'manageMain_data.php Line : '.__LINE__.''); }
				
				if($fe[$arr[$BUM][$keyM]['id']]){
					if($skey=='vehicle'){ $this->idRef = $fe[$arr[$BUM][$keyM]['id']]; } //ไอดี MainVehicle
					
					$return['status'] = 'update';
					$return['id'] = $fe[$arr[$BUM][$keyM]['id']];
					$return['where'] = $where;
				}else{
					foreach($chk_result AS $key => $value){
						$where = str_replace("##*-+".$key."+-*##",strtolower($chk_result[$key]),$where);
					}
					
					$where .= " AND ".$arr[$BUM][$keyM]['status']."!='99'";
					$sql = "SELECT ".$arr[$BUM][$keyM]['id']." FROM ".$this->config[$skey]['db'].".".$arr[$BUM][$keyM]['tb']." WHERE ".$where." LIMIT 1";
					$que = mysql_query($sql);
					$fe = @mysql_fetch_assoc($que);
					
					if(!$que){ $this->gQueryLogError($sql,'manageMain_data.php Line : '.__LINE__.''); }
	
					if($fe[$arr[$BUM][$keyM]['id']]){
						if($skey=='vehicle'){ $this->idRef = $fe[$arr[$BUM][$keyM]['id']]; } //ไอดี MainVehicle

						$return['status'] = 'update';
						$return['id'] = $fe[$arr[$BUM][$keyM]['id']];
						$return['where'] = $where;
					}else{
						$return['status'] = 'insert';
						$return['id'] = $fe[$arr[$BUM][$keyM]['id']];
						$return['where'] = $where;
					}
				}
				
			}else{
				foreach($chk_result AS $key => $value){
					$where = str_replace("##*-+".$key."+-*##",strtolower($chk_result[$key]),$where);
				}
				
				$where .= " AND ".$arr[$BUM][$keyM]['status']."!='99'";
				$sql = "SELECT ".$arr[$BUM][$keyM]['id']." FROM ".$this->config[$skey]['db'].".".$arr[$BUM][$keyM]['tb']." WHERE ".$where." LIMIT 1";
				$que = mysql_query($sql);
				$fe = @mysql_fetch_assoc($que);
				
				if(!$que){ $this->gQueryLogError($sql,'manageMain_data.php Line : '.__LINE__.''); }

				if($fe[$arr[$BUM][$keyM]['id']]){
					if($skey=='vehicle'){ $this->idRef = $fe[$arr[$BUM][$keyM]['id']]; } //ไอดี MainVehicle
					
					$return['status'] = 'update';
					$return['id'] = $fe[$arr[$BUM][$keyM]['id']];
					$return['where'] = $where;
				}else{
					$return['status'] = 'insert';
					$return['id'] = $fe[$arr[$BUM][$keyM]['id']];
					$return['where'] = $where;
				}
			}
		/*}else{
			$return = 'insert';
		}*/
		
		return $return;
	}
	
	//เพิ่ม
	function insertDB($id,$key,$main,$slave,$arr){
		$BU = $this->BU;
		list($BUM,$keyM) = $this->returnKey($key,$arr);
		
		$sql = "INSERT INTO ".$this->config[$key]['db'].".".$arr[$BUM][$keyM]['tb']."(".$main.") SELECT ".$slave;
		echo $sql .= " FROM ".$this->config[$BU]['db'].".".$arr[$BU][$key]['tb']." WHERE ".$arr[$BU][$key]['id']."='".$id."' AND ".$arr[$BU][$key]['status']."!='99'";
		echo "<br><br>";
		$query = mysql_query($sql);
		
		if(!$query){
			$this->gQueryLogError($sql,'manageMain_data.php Line : '.__LINE__.'');
		}else{
			$idRun = mysql_insert_id(); //last id
			
			$this->gQueryAndLogUpdate(0,'','manageMain_data.php Line : '.__LINE__.'',$this->config[$key]['db'].'.'.$arr[$BUM][$keyM]['tb'],
			$arr[$BUM][$keyM]['id']."='".$idRun."'");
			
			if($key=='vehicle' && !$arr[$BU][$key]['cancel_ref']){
				$update = "UPDATE ".$this->config[$BU]['db'].".".$arr[$BU][$key]['tb']." SET idRef_vihicle='".$idRun."' WHERE ".$arr[$BU][$key]['id']."='".$id."' ";
				echo $update .= "AND ".$arr[$BU][$key]['status']."!='99' LIMIT 1";
				echo "<br><br>";
				$this->gQueryAndLogUpdate(1,$update,'manageMain_data.php Line : '.__LINE__.'');
				$query = mysql_query($update);
				
				if(!$query){ $this->gQueryLogError($update,'manageMain_data.php Line : '.__LINE__.''); }
				
				$this->idRef = $idRun;
			}
		}
	}
	
	//แก้ไข
	function updateDB($chk,$key,$main,$slave,$arr,$id,$ch){
		$BU = $this->BU;
		list($BUM,$keyM) = $this->returnKey($key,$arr);
		
		$sql = "SELECT ".$slave." FROM ".$this->config[$BU]['db'].".".$arr[$BU][$key]['tb']." WHERE ".$arr[$BU][$key]['id']."='".$id."' AND ";
		$sql .= $arr[$BU][$key]['status']."!='99' LIMIT 1";
		$query = mysql_query($sql);
		$fe = @mysql_fetch_assoc($query);
		
		if(!$query){ $this->gQueryLogError($sql,'manageMain_data.php Line : '.__LINE__.''); }
		
		if($keyM=='vehicle'){ unset($arr[$BUM][$keyM]['id_ref']); }
		unset($arr[$BUM][$keyM]['chk']);
		
		foreach($arr[$BUM][$keyM] AS $uKey => $uValue){			
			if($uValue==$arr[$BUM][$keyM]['id_ref'] && !$arr[$BUM][$keyM]['cancel_ref']){ $fe[$arr[$BU][$key][$uKey]] = $this->idRef; }
			if($ch[$BUM][$uKey] && $arr[$BU][$key][$uKey]=="''"){ $fe[$arr[$BU][$key][$uKey]] = preg_replace("/(^'{1}||'{1}$)/",'',$ch[$BUM][$uKey]); }
			
			$main = str_replace("##*-+".$uKey."+-*##",$fe[$arr[$BU][$key][$uKey]],$main);
		}
		
		//echo "main = ".$main."<br><br>";

		$update = "UPDATE ".$this->config[$key]['db'].".".$arr[$BUM][$keyM]['tb']." SET ".$main." WHERE ".$arr[$BUM][$keyM]['id']."='".$chk['id']."' AND ";
		echo $update .= $arr[$BUM][$keyM]['status']."!='99' LIMIT 1";
		echo "<br><br>";
		$this->gQueryAndLogUpdate(1,$update,'manageMain_data.php Line : '.__LINE__.'');
		$query = mysql_query($update);
		
		if(!$query){ $this->gQueryLogError($update,'manageMain_data.php Line : '.__LINE__.''); }
		
		if($key=='vehicle' && !$arr[$BUM][$keyM]['id_ref']){
			$this->idRef = $chk['id'];
			
			$update = "UPDATE ".$this->config[$BU]['db'].".".$arr[$BU][$key]['tb']." SET idRef_vihicle='".$this->idRef."' WHERE ".$arr[$BU][$key]['id']."='".$id."' ";
			echo $update .= "AND ".$arr[$BU][$key]['status']."!='99' LIMIT 1";
			echo "<br><br>";
			$this->gQueryAndLogUpdate(1,$update,'manageMain_data.php Line : '.__LINE__.'');
			$que = mysql_query($update);
			
			if(!$que){ $this->gQueryLogError($update,'manageMain_data.php Line : '.__LINE__.''); }
		}
	}
	
	//หา key array
	function returnKey($key,$arr){
		$BU = $this->BU;
		
		if($key=='vehicle'){
			$keyM = 'vehicle'; $BUM = 'vehicle';
		}else{
			$BUM = $key; $keyM = $BU;
			/*$keyM = $key; $BUM = $BU;
			
			if(!$arr[$BUM][$keyM]['tb']){
				$keyM = $BU; $BUM = $key;
			}*/
		}
		
		$return = array($BUM,$keyM);
		
		return $return;
	}
	
	//บันทึกข้อมูล
	function insert($arr,$ch,$id){
		$BU = $this->BU;
		$totalArr = $arr;
		
		if($arr && $BU && $id){ //ถ้ามีข้อมูลทั้งหมด
			foreach($arr[$BU] AS $key => $value){
			//if($BU=='service' && $key=='sale'){ $this->disDB[$key] = 'OFF'; } //ถ้ามาจาก service ไม่ไปจัดการ sale
			if($this->disDB[$key]=='ON'){
				list($BUM,$keyM) = $this->returnKey($key,$arr);
				
				//print_r($value); echo "<br><br>";
				$slave = $main = $main_edit = $cmm = null;
				
				//วนลูปหาฟิลด์ ฐานข้อมูลที่ส่งมา
				$loopF = $value; unset($loopF['tb']); unset($loopF['status']); unset($loopF['id']); unset($loopF['chk']); unset($loopF['id_ref']); unset($loopF['cancel_ref']);
				//print_r($loopF); echo "<br><br>";
				
				$loop = 0;
				foreach($loopF AS $keys => $values){
					$keys = (string)$keys;
					
					if($values && $arr[$BUM][$keyM][$keys]){ //ต้องมีค่าทั้งคู่
						if($loop!=0){ $cmm = ","; }
						if($ch[$key][$keys]){ $values = $ch[$key][$keys]; }
						
						$slave .= $cmm.$values;
						$main .= $cmm.$arr[$BUM][$keyM][$keys];
						$main_edit .= $cmm.$arr[$BUM][$keyM][$keys]."='##*-+".$keys."+-*##'";
						
						$loop++;
					}
				}
				
				$chk = $this->chkSQL($totalArr,$key,$id,$ch,'insert'); //เช็คว่าจะแก้ไขหรืออัพเดท*/
				//print_r($chk); echo "<br>";
				
				if($chk['status']=='update'){
					$this->updateDB($chk,$key,$main_edit,$slave,$arr,$id,$ch);
				}else{
					$this->insertDB($id,$key,$main,$slave,$arr);
				}
			}}
		}else{
			echo "ข้อมูลมาไม่ครบ ไม่สามารถแก้ไขฐานข้อมูล ".$this->config[$key]['db']." ได้";
		}
		
		$this->chk_result = $this->idRef = null;
		@mysql_close( $this->conn );
	}
	
	//แก้ไขข้อมูล
	function update($arr,$ch,$id){
		$BU = $this->BU;
		$totalArr = $arr;
		
		if($arr && $BU && $id){
			foreach($arr[$BU] AS $key => $value){
			//if($BU=='service' && $key=='sale'){ $this->disDB[$key] = 'OFF'; } //ถ้ามาจาก service ไม่ไปจัดการ sale
			if($this->disDB[$key]=='ON'){
				list($BUM,$keyM) = $this->returnKey($key,$arr);
				
				//print_r($value); echo "<br><br>";
				$slave = $main = $main_edit = $cmm = null;
				
				//วนลูปหาฟิลด์ ฐานข้อมูลที่ส่งมา
				$loopF = $value; unset($loopF['tb']); unset($loopF['status']); unset($loopF['id']); unset($loopF['chk']); unset($loopF['id_ref']); unset($loopF['cancel_ref']);
				//print_r($loopF); echo "<br><br>";
				
				$loop = 0;
				foreach($loopF AS $keys => $values){
					$keys = (string)$keys;
					
					if($values && $arr[$BUM][$keyM][$keys]){ //ต้องมีค่าทั้งคู่
						if($loop!=0){ $cmm = ","; }
						if($ch[$key][$keys]){ $values = $ch[$key][$keys]; }
						
						$slave .= $cmm.$values;
						$main .= $cmm.$arr[$BUM][$keyM][$keys];
						$main_edit .= $cmm.$arr[$BUM][$keyM][$keys]."='##*-+".$keys."+-*##'";
						
						$loop++;
					}
				}
				
				$chk = $this->chkSQL($totalArr,$key,$id,$ch,'update'); //เช็คว่าจะแก้ไขหรืออัพเดท*/
				//print_r($chk); echo "<br>";
				
				/*if($chk['status']=='update'){
					$this->updateDB($chk,$key,$main_edit,$slave,$arr,$id,$ch);
				}else{
					$this->insertDB($id,$key,$main,$slave,$arr);
				}*/
			}}
		}else{
			echo "ข้อมูลมาไม่ครบ ไม่สามารถแก้ไขฐานข้อมูล ".$this->config[$key]['db']." ได้";
		}
		
		$this->chk_result = $this->idRef = null;
		@mysql_close( $this->conn );
	}
	
	//บันทึกข้อมูลรถ
	function insertVehicle($id){
		$this->setConnect();
		$this->arrayVehicle();
		
		$BU = $this->BU;
		
		$vehicle = $this->vehicle;
		//$ch['vehicle'][24] = '1570500002839';
		$ch['vehicle'][7] = 'CONCAT(register_province," ",register) AS register';
		
		if($BU=='service'){
			{
				$ahis = "SELECT abrand_id,color_id,amodel_id,model,model_name,MDL_CD,fullmodel_name FROM ".$this->config[$BU]['db'].".ahistory WHERE id='".$id."' ";
				$ahis .= "AND status!='99' LIMIT 1";
				$qAhis = mysql_query($ahis);
				$fAhis = @mysql_fetch_assoc($qAhis);
				
				if(!$qAhis){ $this->gQueryLogError($ahis,'manageMain_data.php Line : '.__LINE__.''); }
			}

			if($fAhis['abrand_id']){
				$brand = "SELECT name,idRef_vihicle FROM ".$this->config[$BU]['db'].".abrand WHERE id='".$fAhis['abrand_id']."' AND status!='99' LIMIT 1";
				$qBrand = mysql_query($brand);
				$fBrand = @mysql_fetch_assoc($qBrand);
				
				if(!$qBrand){ $this->gQueryLogError($brand,'manageMain_data.php Line : '.__LINE__.''); }
			}
			
			if($fAhis['color_id']){
				$color = "SELECT color_name,detailcolor FROM ".$this->config[$BU]['db'].".colorcar WHERE id='".$fAhis['color_id']."' AND status!='99' LIMIT 1";
				$qColor = mysql_query($color);
				$fColor = @mysql_fetch_assoc($qColor);
				
				if(!$qColor){ $this->gQueryLogError($color,'manageMain_data.php Line : '.__LINE__.''); }
			}
			
			$ch['vehicle'][0] = "'".$fBrand['idRef_vihicle']."'";
			$ch['vehicle'][1] = "'".$fBrand['name']."'";
			$ch['vehicle'][3] = "'".$fColor['color_name']."'";
			$ch['vehicle'][4] = "'".$fColor['detailcolor']."'";
			$ch['vehicle'][8] = "'".$fAhis['amodel_id']."'";
			$ch['vehicle'][9] = "'".$fAhis['model_name']."'";
			$ch['vehicle'][10] = "'".$fAhis['model']."'";
			$ch['vehicle'][11] = "'".$fAhis['MDL_CD']."'";
			$ch['vehicle'][12] = "'".$fAhis['fullmodel_name']."'";
		}
		
		$this->insert($vehicle,$ch,$id);
	}
	
	//บันทึกข้อมูลรถ
	function updateVehicle($id){
		$this->setConnect();
		$this->arrayVehicle();
		$BU = $this->BU;
		
		$vehicle = $this->vehicle;
		//$ch['vehicle'][24] = '1570500002839';
		$ch['vehicle'][7] = 'CONCAT(register_province," ",register) AS register';
		
		if($BU=='service'){
			{
				$ahis = "SELECT abrand_id,color_id,amodel_id,model,model_name,MDL_CD,fullmodel_name FROM ".$this->config[$BU]['db'].".ahistory WHERE id='".$id."' ";
				$ahis .= "AND status!='99' LIMIT 1";
				$qAhis = mysql_query($ahis);
				$fAhis = @mysql_fetch_assoc($qAhis);
				
				if(!$qAhis){ $this->gQueryLogError($ahis,'manageMain_data.php Line : '.__LINE__.''); }
			}

			if($fAhis['abrand_id']){
				$brand = "SELECT name,idRef_vihicle FROM ".$this->config[$BU]['db'].".abrand WHERE id='".$fAhis['abrand_id']."' AND status!='99' LIMIT 1";
				$qBrand = mysql_query($brand);
				$fBrand = @mysql_fetch_assoc($qBrand);
				
				if(!$qBrand){ $this->gQueryLogError($brand,'manageMain_data.php Line : '.__LINE__.''); }
			}
			
			if($fAhis['color_id']){
				$color = "SELECT color_name,detailcolor FROM ".$this->config[$BU]['db'].".colorcar WHERE id='".$fAhis['color_id']."' AND status!='99' LIMIT 1";
				$qColor = mysql_query($color);
				$fColor = @mysql_fetch_assoc($qBrand);
				
				if(!$qColor){ $this->gQueryLogError($color,'manageMain_data.php Line : '.__LINE__.''); }
			}
			
			$ch['vehicle'][0] = "'".$fBrand['idRef_vihicle']."'";
			$ch['vehicle'][1] = "'".$fBrand['name']."'";
			$ch['vehicle'][3] = "'".$fColor['color_name']."'";
			$ch['vehicle'][4] = "'".$fColor['detailcolor']."'";
			$ch['vehicle'][8] = "'".$fAhis['amodel_id']."'";
			$ch['vehicle'][9] = "'".$fAhis['model_name']."'";
			$ch['vehicle'][10] = "'".$fAhis['model']."'";
			$ch['vehicle'][11] = "'".$fAhis['MDL_CD']."'";
			$ch['vehicle'][12] = "'".$fAhis['fullmodel_name']."'";
		}
		
		$this->update($vehicle,$ch,$id);
	}
	
	//บันทึกข้อมูลความสัมพันธ์ระหว่างรถกับคน
	function insertRelation($id){
		$this->insertVehicle($id); //เพิ่มในฐานข้อมูลรถก่อน
		
		$this->setConnect();
		$this->arrayRelationship();
		$BU = $this->BU;
		
		$relation = $this->relation;
		
		if($BU=='service'){
			$ch['vehicle'][6] = 'FROM_UNIXTIME(add_time) AS add_time';
			$ahis = "SELECT user_id,asell_com_id FROM ".$this->config[$BU]['db'].".ahistory WHERE id='".$id."' AND status!='99' LIMIT 1";
			$qAhis = mysql_query($ahis);
			$fAhis = mysql_fetch_assoc($qAhis);
			
			if(!$qAhis){ $this->gQueryLogError($ahis,'manageMain_data.php Line : '.__LINE__.''); }
			
			if($fAhis['user_id']){
				$cus = "SELECT cus_id FROM ".$this->config[$BU]['db'].".customer WHERE id='".$fAhis['user_id']."' AND status!='99' LIMIT 1";
				$qCus = mysql_query($cus);
				$fCus = mysql_fetch_assoc($qCus);
				
				if(!$qCus){ $this->gQueryLogError($cus,'manageMain_data.php Line : '.__LINE__.''); }
			}
			
			if($fAhis['asell_com_id']){
				$comp = "SELECT idRef_vihicle FROM ".$this->config[$BU]['db'].".asell_company WHERE id='".$fAhis['asell_com_id']."' AND status!='99' LIMIT 1";
				$qComp = mysql_query($comp);
				$fComp = mysql_fetch_assoc($qComp);
				
				if(!$qComp){ $this->gQueryLogError($comp,'manageMain_data.php Line : '.__LINE__.''); }
			}
			
			$ch['vehicle'][1] = "'".$fCus['cus_id']."'";
			$ch['vehicle'][3] = "'".$fComp['idRef_vihicle']."'";
		}
		
		$this->insert($relation,$ch,$id);
	}
	
	//แก้ไขข้อมูลความสัมพันธ์ระหว่างรถกับคน
	function updateRelation($id){
		$this->insertVehicle($id); //เพิ่มในฐานข้อมูลรถก่อน
		
		$this->setConnect();
		$this->arrayRelationship();
		$BU = $this->BU;
		
		$relation = $this->relation;
		
		if($BU=='service'){
			$ch['vehicle'][6] = 'FROM_UNIXTIME(add_time) AS add_time';
			$ahis = "SELECT user_id,asell_com_id FROM ".$this->config[$BU]['db'].".ahistory WHERE id='".$id."' AND status!='99' LIMIT 1";
			$qAhis = mysql_query($ahis);
			$fAhis = mysql_fetch_assoc($qAhis);
			
			if(!$qAhis){ $this->gQueryLogError($ahis,'manageMain_data.php Line : '.__LINE__.''); }
			
			if($feAhis['user_id']){
				$cus = "SELECT cus_id FROM ".$this->config[$BU]['db'].".customer WHERE id='".$feAhis['user_id']."' AND status!='99' LIMIT 1";
				$qCus = mysql_query($cus);
				$fCus = mysql_fetch_assoc($qCus);
				
				if(!$qCus){ $this->gQueryLogError($cus,'manageMain_data.php Line : '.__LINE__.''); }
			}
			
			if($fAhis['asell_com_id']){
				$comp = "SELECT idRef_vihicle FROM ".$this->config[$BU]['db'].".asell_company id='".$fAhis['asell_com_id']."' AND status!='99' LIMIT 1";
				$qComp = mysql_query($comp);
				$fComp = mysql_fetch_assoc($qComp);
				
				if(!$qComp){ $this->gQueryLogError($comp,'manageMain_data.php Line : '.__LINE__.''); }
			}
			
			$ch['vehicle'][1] = "'".$fCus['cus_id']."'";
			$ch['vehicle'][3] = "'".$fComp['idRef_vihicle']."'";
		}
		
		$this->update($relation,$ch,$id);
	}
	
	//บันทึกยี่ห้อรถ
	function insertBrand($id){
		$this->setConnect();
		$BU = $this->BU;
		$this->arrayBrand($BU);
		
		$brand = $this->brand;
		if($BU=='service'){
			$ch['vehicle'][3] = 'FROM_UNIXTIME(add_time) AS add_time';
		}else if($BU=='sale'){
			$ch['vehicle'][3] = 'FROM_UNIXTIME(UpdateTime) AS UpdateTime';
		}
		
		$this->insert($brand,$ch,$id);
	}
	
	//แก้ไขยี่ห้อรถ
	function updateBrand($id){
		$this->setConnect();
		$BU = $this->BU;
		$this->arrayBrand($BU);
		
		$brand = $this->brand;
		if($BU=='service'){
			$ch['vehicle'][3] = 'FROM_UNIXTIME(add_time) AS add_time';
		}else if($BU=='sale'){
			$ch['vehicle'][3] = 'FROM_UNIXTIME(UpdateTime) AS UpdateTime';
		}
		
		$this->update($brand,$ch,$id);
	}
	
	//บันทึกบริษัทที่จำหน่ายรถ
	function insertCompany($id){
		$this->setConnect();
		$this->arrayCompany();
		$BU = $this->BU;
		
		if($BU=='service'){
			$comp = $this->comp;
			$ch['vehicle'][5] = 'FROM_UNIXTIME(add_time) AS add_time';
			
			$this->insert($comp,$ch,$id);
		}
	}
	
	//แก้ไขบริษัทที่จำหน่ายรถ
	function updateCompany($id){
		$this->setConnect();
		$this->arrayCompany();
		$BU = $this->BU;
		
		if($BU=='service'){
			$comp = $this->comp;
			$ch['vehicle'][5] = 'FROM_UNIXTIME(add_time) AS add_time';
			 
			$this->update($comp,$ch,$id);
		}
	}
	
	function setConnect_customer(){
		global $configBu; //global config
		$this->BU = $configBu;
		
		$this->server['ServerName']	= "localhost";
		$this->server['UserName'] = "root";
		$this->server['UserPassword'] = "123456";
		
		//if($BU!='main'){
			$this->config['cusNew']['db'] = 'main_cusdata';
			$this->config['cusNew']['sv'] = $this->server['ServerName'];
			$this->config['cusNew']['user'] = $this->server['UserName'];
			$this->config['cusNew']['pass'] = $this->server['UserPassword'];
		//if($BU!='service'){
		
		//if($BU!='service'){
			$this->config['cusOld']['db'] = 'new_maincusdata';
			$this->config['cusOld']['sv'] = $this->server['ServerName'];
			$this->config['cusOld']['user'] = $this->server['UserName'];
			$this->config['cusOld']['pass'] = $this->server['UserPassword'];
		//}
		
		{ //เปิดปิดฐานข้อมูล
			$this->disDB['cusNew'] = 'ON';
			$this->disDB['cusOld'] = 'ON';
		}
		
		{ //array ไว้เช็คตารางฐานข้อมูล
			$this->chkDB['cusNew'] = array('AMIAN_CUS_TYPE','CHANGE_GRADE_HISTORY','MAIN_ADDRESS','MAIN_CUS_GINFO','MAIN_EDUCATION','MAIN_EMAILS','MAIN_JOBS',
			'MAIN_TELEPHONE','MIAN_CUS_TYPE_REF');
			$this->chkDB['cusOld'] = array('aMain_amphur','aMain_district','aMain_geography','aMain_postcode','aMain_province','first_name','MainCusData',
			'MainCusData_CusEmp_Rel','MainCusData_CusRelation','MainCusData_CusRelationTable','MainCusData_JobTable','MainCus_Type');
		}
		
		if($this->disDB['cusNew']=='ON' && $this->disDB['cusOld']=='ON'){
			$this->connect();
		}
	}
	
	//เพิ่ม แก้ไข ข้อมูลลูกค้า
	function manageDB_Customer($id,$status){
		$sql = "SELECT * FROM ".$this->config['cusOld']['db'].".MainCusData WHERE CusNo='".$id."' LIMIT 1";
		$query = mysql_query($sql);
		$fe = @mysql_fetch_assoc($query);
		
		if(!$query){
			$this->gQueryLogError($sql,'manageMain_data.php Line : '.__LINE__.'');
		}
		
		//ข้อมูลทั่วไป
		if($status=='update'){
			$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_CUS_GINFO SET Picture='".$fe['Picture']."',Be='".$fe['Be']."',BeType='".$fe['BeType']."',";
			$update .= "Cus_Name='".$fe['Cus_Name']."',Cus_Surename='".$fe['Cus_Surename']."',Cus_Nickname='".$fe['Cus_Nickname']."',";
			$update .= "Cus_Big_Picture='".$fe['Cus_Big_Picture']."',Cus_Small_Picture='".$fe['Cus_Small_Picture']."',Sex='".$fe['Sex']."',";
			$update .= "DateOfBirth='".$fe['DateOfBirth']."',Cus_IDNo_Type='".$fe['Cus_IDNo']."',Cus_IDNo='".$fe['Cus_IDNo']."',Married_Status='".$fe['Status']."',";
			$update .= "Date_Receive='".$fe['Data_Received']."',Data_Received_Num='".$fe['Data_Received_Num']."',1stSourceData='".$fe['1stSourceData']."',";
			$update .= "Remark='".$fe['Remark']."',Updater='".$fe['Updater']."',UpdaterNO='".$fe['UpdaterNO']."',UpdateT='".$fe['UpdateT']."' ";
			$update .= "WHERE CusNo='".$id."' LIMIT 1";
			$this->gQueryAndLogUpdate(1,$update,'manageMain_data.php Line : '.__LINE__.'');
			$query = mysql_query($update);
			
			if(!$query){
				$this->gQueryLogError($update,'manageMain_data.php Line : '.__LINE__.'');
			}
		}else{
			$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_CUS_GINFO(CusNo,Picture,Be,BeType,Cus_Name,Cus_Surename,Cus_Nickname,Cus_Big_Picture,";
			$insert .= "Cus_Small_Picture,Sex,DateOfBirth,Cus_IDNo_Type,Cus_IDNo,Married_Status,Date_Receive,Data_Received,Data_Received_Num,1stSourceData,Remark,";
			$insert .= "Updater,UpdaterNO,UpdateT)VALUES('".$fe['CusNo']."','".$fe['Picture']."','".$fe['Be']."','".$fe['BeType']."','".$fe['Cus_Name']."',";
			$insert .= "'".$fe['Cus_Surename']."','".$fe['Cus_Nickname']."','".$fe['Cus_Big_Picture']."','".$fe['Cus_Small_Picture']."','".$fe['Sex']."',";
			$insert .= "'".$fe['DateOfBirth']."','".$fe['Cus_IDNo']."','".$fe['Cus_IDNo']."','".$fe['Status']."','".$fe['Date_Receive']."',";
			$insert .= "'".$fe['Data_Received']."','".$fe['Data_Received_Num']."','".$fe['1stSourceData']."','".$fe['Remark']."','".$fe['Updater']."',";
			$insert .= "'".$fe['UpdaterNO']."','".$fe['UpdateT']."')";
			$query = mysql_query($insert);
			
			if(!$query){
				$this->gQueryLogError($insert,'manageMain_data.php Line : '.__LINE__.'');
			}else{
				$this->gQueryAndLogUpdate(0,'','manageMain_data.php Line : '.__LINE__.'',$this->config['cusNew']['db'].'.MAIN_CUS_GINFO',"CusNo='".$fe['CusNo']."'");
			}
		}
		//ข้อมูลทั่วไป
		
		//ข้อมูลประวัติดการศึกษา
		//1=ประถมศึกษา,2=ม.ต้น,3=ม.ปลาย / ปวช.,4=ปริญญาตรี,5=ปริญญาโท,6=ปริญญาเอก
		$arrayEdu = array('1'=>'Primary_Edu','2'=>'Secondary_Edu','3'=>'Highschool_Edu','4'=>'Bachelor','5'=>'Master','6'=>'Phd');
		
		foreach($arrayEdu AS $key => $field){
			if($fe[$field]){
				if($status=='update'){
					$chk = "SELECT EDU_ID_NUM FROM ".$this->config['cusNew']['db'].".MAIN_EDUCATION WHERE EDU_CUS_NO='".$fe['CusNo']."' AND EDU_TYPE='".$key."' LIMIT 1";
					$qChk = mysql_query($chk);
					$fChk = @mysql_fetch_assoc($qChk);
					
					if(!$qChk){
						$this->gQueryLogError($chk,'manageMain_data.php Line : '.__LINE__.'');
					}
					
					if($fChk['EDU_ID_NUM']){
						$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_EDUCATION SET EDU_NAME='".$fe[$field]."' WHERE ";
						$update .= "EDU_ID_NUM='".$fChk['EDU_ID_NUM']."' LIMIT 1";
						$this->gQueryAndLogUpdate(1,$update,'manageMain_data.php Line : '.__LINE__.'');
						$query = mysql_query($update);
						
						if(!$query){
							$this->gQueryLogError($update,'manageMain_data.php Line : '.__LINE__.'');
						}
					}else{
						$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_EDUCATION(EDU_CUS_NO,EDU_NAME,EDU_ADDRESS,EDU_DETAIL,EDU_TYPE)VALUES(";
						$insert .= "'".$fe['CusNo']."','".$fe[$field]."','','','".$key."');";
						$query = mysql_query($insert);
						
						if(!$query){
							$this->gQueryLogError($insert,'manageMain_data.php Line : '.__LINE__.'');
						}else{
							$this->gQueryAndLogUpdate(0,'','manageMain_data.php Line : '.__LINE__.'',$this->config['cusNew']['db'].'.MAIN_EDUCATION',
							"EDU_CUS_NO='".$fe['CusNo']."' AND EDU_TYPE='".$key."'");
						}
					}
				}else{
					$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_EDUCATION(EDU_CUS_NO,EDU_NAME,EDU_ADDRESS,EDU_DETAIL,EDU_TYPE)VALUES(";
					$insert .= "'".$fe['CusNo']."','".$fe[$field]."','','','".$key."');";
					$query = mysql_query($insert);
					
					if(!$query){
						$this->gQueryLogError($insert,'manageMain_data.php Line : '.__LINE__.'');
					}else{
						$this->gQueryAndLogUpdate(0,'','manageMain_data.php Line : '.__LINE__.'',$this->config['cusNew']['db'].'.MAIN_EDUCATION',
						"EDU_CUS_NO='".$fe['CusNo']."' AND EDU_TYPE='".$key."'");
					}
				}
			}
		}
		//ข้อมูลประวัติดการศึกษา
		
		//ข้อมูลที่อยู่หลัก
		//1=ที่อยู่ตามบัตร ปชช,1=ที่อยู่ส่งเอกสาร,1=ที่ทำงาน
		$arrayAdd[1] = array('0'=>'C_Add','1'=>'C_Vill','2'=>'C_Tum','3'=>'C_Amp','4'=>'C_Pro','5'=>'C_GEO','6'=>'C_Code','7'=>'C_AddGPSLatitude',
		'8'=>'C_AddGPSLongtitude','9'=>'C_AddMapDescribe');
		$arrayAdd[2] = array('0'=>'Cus_Add','1'=>'Cus_Vil','2'=>'Cus_Tum','3'=>'Cus_Amp','4'=>'Cus_Pro','5'=>'Cus_GEO','6'=>'Cus_Code','7'=>'Cus_AddGPSLatitude',
		'8'=>'Cus_AddGPSLongtitude','9'=>'Cus_AddMapDescribe');
		$arrayAdd[3] = array('0'=>'W_Add','1'=>'W_Vill','2'=>'W_Tum','3'=>'W_Amp','4'=>'W_Pro','5'=>'W_GEO','6'=>'W_Code','7'=>'Cus_WGPSLatitude',
		'8'=>'Cus_WGPSLongtitude','9'=>'Cus_WMapDescribe');
		
		foreach($arrayAdd AS $key => $value){
			if($fe[$value[0]]){
				if($status=='update'){
					$chk = "SELECT ADDR_ID_NUM FROM ".$this->config['cusNew']['db'].".MAIN_ADDRESS WHERE ADDR_CUS_NO='".$fe['CusNo']."' AND ADDR_TYPE='".$key."' LIMIT 1";
					$qChk = mysql_query($chk);
					$fChk = @mysql_fetch_assoc($qChk);
					
					if(!$qChk){
						$this->gQueryLogError($chk,'manageMain_data.php Line : '.__LINE__.'');
					}
					
					if($fChk['ADDR_ID_NUM']){
						$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_ADDRESS SET ADDR_VILLAGE='".$fe[$value[1]]."',ADDR_NUMBER='".$fe[$value[0]]."',";
						$update .= "ADDR_SUB_DISTRICT='".$fe[$value[2]]."',ADDR_DISTRICT='".$fe[$value[3]]."',ADDR_PROVINCE='".$fe[$value[4]]."',";
						$update .= "ADDR_POSTCODE='".$fe[$value[6]]."',ADDR_GEOGRAPHY='".$fe[$value[5]]."',ADDR_GPS_LATITUDE='".$fe[$value[7]]."',";
						$update .= "ADDR_GPS_LONGTITUDE='".$fe[$value[8]]."',ADDR_MAP_DESC='".$fe[$value[9]]."' WHERE ADDR_ID_NUM='".$fChk['ADDR_ID_NUM']."' LIMIT 1";
						$this->gQueryAndLogUpdate(1,$update,'manageMain_data.php Line : '.__LINE__.'');
						$query = mysql_query($update);
						
						if(!$query){
							$this->gQueryLogError($update,'manageMain_data.php Line : '.__LINE__.'');
						}
					}else{
						if($key==1){ $main = 1; }else{ $main = 0; }
					
						$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_ADDRESS(ADDR_CUS_NO,ADDR_VILLAGE,ADDR_ROOM_NO,ADDR_FLOOR_NO,ADDR_NUMBER,";
						$insert .= "ADDR_GROUP_NO,ADDR_LANE,ADDR_ROAD,ADDR_SUB_DISTRICT,ADDR_DISTRICT,ADDR_PROVINCE,ADDR_POSTCODE,ADDR_GEOGRAPHY,ADDR_GPS_LATITUDE,";
						$insert .= "ADDR_GPS_LONGTITUDE,ADDR_MAP_DESC,ADDR_REMARK,ADDR_TYPE,ADDR_MAIN_ACTIVE)VALUES('".$fe['CusNo']."','".$fe[$value[1]]."','','',";
						$insert .= "'".$fe[$value[0]]."','','','','".$fe[$value[2]]."','".$fe[$value[3]]."','".$fe[$value[4]]."','".$fe[$value[6]]."',";
						$insert .= "'".$fe[$value[5]]."','".$fe[$value[7]]."','".$fe[$value[8]]."','".$fe[$value[9]]."','','".$key."','".$main."');";
						$query = mysql_query($insert);
						
						if(!$query){
							$this->gQueryLogError($insert,'manageMain_data.php Line : '.__LINE__.'');
						}else{
							$this->gQueryAndLogUpdate(0,'','manageMain_data.php Line : '.__LINE__.'',$this->config['cusNew']['db'].'.MAIN_ADDRESS',
							"ADDR_CUS_NO='".$fe['CusNo']."' AND ADDR_TYPE='".$key."'");
						}
					}
				}else{
					if($key==1){ $main = 1; }else{ $main = 0; }
					
					$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_ADDRESS(ADDR_CUS_NO,ADDR_VILLAGE,ADDR_ROOM_NO,ADDR_FLOOR_NO,ADDR_NUMBER,";
					$insert .= "ADDR_GROUP_NO,ADDR_LANE,ADDR_ROAD,ADDR_SUB_DISTRICT,ADDR_DISTRICT,ADDR_PROVINCE,ADDR_POSTCODE,ADDR_GEOGRAPHY,ADDR_GPS_LATITUDE,";
					$insert .= "ADDR_GPS_LONGTITUDE,ADDR_MAP_DESC,ADDR_REMARK,ADDR_TYPE,ADDR_MAIN_ACTIVE)VALUES('".$fe['CusNo']."','".$fe[$value[1]]."','','',";
					$insert .= "'".$fe[$value[0]]."','','','','".$fe[$value[2]]."','".$fe[$value[3]]."','".$fe[$value[4]]."','".$fe[$value[6]]."','".$fe[$value[5]]."',";
					$insert .= "'".$fe[$value[7]]."','".$fe[$value[8]]."','".$fe[$value[9]]."','','".$key."','".$main."');";
					$query = mysql_query($insert);
					
					if(!$query){
						$this->gQueryLogError($insert,'manageMain_data.php Line : '.__LINE__.'');
					}else{
						$this->gQueryAndLogUpdate(0,'','manageMain_data.php Line : '.__LINE__.'',$this->config['cusNew']['db'].'.MAIN_ADDRESS',
						"ADDR_CUS_NO='".$fe['CusNo']."' AND ADDR_TYPE='".$key."'");
					}
				}
			}
		}
		//ข้อมูลที่อยู่หลัก
		
		//อีเมล์
		//0=อีเมล์ที่ทำงาน,1=อีเมล์ส่วนตัว
		$arrayEmail['email'] = array('0'=>'W_Email','1'=>'Email');
		$arrayEmail['des'] = array('0'=>'อีเมล์ที่ทำงาน','1'=>'อีเมล์ส่วนตัว');
		
		foreach($arrayEmail['email'] AS $key => $value){
			if($fe[$value]){
				if($status=='update'){
					$chk = "SELECT EMAILS_ID_NUM FROM ".$this->config['cusNew']['db'].".MAIN_EMAILS WHERE EMAILS_CUS_NO='".$fe['CusNo']."' AND ";
					$chk .= "EMAILS_STATUS='".$key."' LIMIT 1";
					$qChk = mysql_query($chk);
					$fChk = @mysql_fetch_assoc($qChk);
					
					if(!$qChk){
						$this->gQueryLogError($chk,'manageMain_data.php Line : '.__LINE__.'');
					}
					
					if($fChk['EMAILS_ID_NUM']){
						$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_EMAILS SET EMAILS_ADDRESS='".$fe[$value]."',EMAILS_DESC='".$arrayEmail['des'][$key]."' ";
						$update .= "WHERE EMAILS_ID_NUM='".$fChk['EMAILS_ID_NUM']."' LIMIT 1";
						$this->gQueryAndLogUpdate(1,$update,'manageMain_data.php Line : '.__LINE__.'');
						$query = mysql_query($update);
						
						if(!$query){
							$this->gQueryLogError($update,'manageMain_data.php Line : '.__LINE__.'');
						}
					}else{
						$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_EMAILS(EMAILS_CUS_NO,EMAILS_ADDRESS,EMAILS_DESC,EMAILS_STATUS)VALUES";
						$insert .= "('".$fe['CusNo']."','".$fe[$value]."','".$arrayEmail['des'][$key]."','".$key."');";
						$query = mysql_query($insert);
						
						if(!$query){
							$this->gQueryLogError($insert,'manageMain_data.php Line : '.__LINE__.'');
						}else{
							$this->gQueryAndLogUpdate(0,'','manageMain_data.php Line : '.__LINE__.'',$this->config['cusNew']['db'].'.MAIN_EMAILS',
							"EMAILS_CUS_NO='".$fe['CusNo']."' AND EMAILS_STATUS='".$key."'");
						}
					}
				}else{
					$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_EMAILS(EMAILS_CUS_NO,EMAILS_ADDRESS,EMAILS_DESC,EMAILS_STATUS)VALUES";
					$insert .= "('".$fe['CusNo']."','".$fe[$value]."','".$arrayEmail[$key]."','".$key."');";
					$query = mysql_query($insert);
					
					if(!$query){
						$this->gQueryLogError($insert,'manageMain_data.php Line : '.__LINE__.'');
					}else{
						$this->gQueryAndLogUpdate(0,'','manageMain_data.php Line : '.__LINE__.'',$this->config['cusNew']['db'].'.MAIN_EMAILS',
						"EMAILS_CUS_NO='".$fe['CusNo']."' AND EMAILS_STATUS='".$key."'");
					}
				}
			}
		}
		//อีเมล์
		
		//ข้อมูลอาชีพ
		$job = "SELECT JobID FROM ".$this->config['cusOld']['db'].".MainCusData_JobTable WHERE Job='".$fe['Job']."' LIMIT 1";
		$qJob = mysql_query($job);
		$fJob = @mysql_fetch_assoc($qJob);
		
		if(!$qJob){
			$this->gQueryLogError($job,'manageMain_data.php Line : '.__LINE__.'');
		}
		
		if($status=='update'){
			$chk = "SELECT JOB_ID_NUM FROM ".$this->config['cusNew']['db'].".MAIN_JOBS WHERE JOB_CUS_NO='".$fe['CusNo']."' LIMIT 1";
			$qChk = mysql_query($chk);
			$fChk = @mysql_fetch_assoc($qChk);
			
			if(!$qChk){
				$this->gQueryLogError($chk,'manageMain_data.php Line : '.__LINE__.'');
			}
			
			if($fChk['JOB_ID_NUM']){
				$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_JOBS SET JOB_REF_MAIN_ID='".$fJob['JobID']."',JOB_NAME='".$fe['Job']."',";
				$update .= "JOB_DESC='".$fe['Job_Des']."' WHERE JOB_ID_NUM='".$fChk['JOB_ID_NUM']."' LIMIT 1";
				$this->gQueryAndLogUpdate(1,$update,'manageMain_data.php Line : '.__LINE__.'');
				$query = mysql_query($update);
				
				if(!$query){
					$this->gQueryLogError($update,'manageMain_data.php Line : '.__LINE__.'');
				}
			}else{
				$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_JOBS(JOB_CUS_NO,JOB_REF_MAIN_ID,JOB_NAME,JOB_DESC,JOB_MAIN_ACTIVE,JOB_STATUS)VALUES";
				$insert .= "('".$fe['CusNo']."','".$fJob['JobID']."','".$fe['Job']."','".$fe['Job_Des']."','1','0');";
				$query = mysql_query($insert);
				
				if(!$query){
					$this->gQueryLogError($insert,'manageMain_data.php Line : '.__LINE__.'');
				}else{
					$this->gQueryAndLogUpdate(0,'','manageMain_data.php Line : '.__LINE__.'',$this->config['cusNew']['db'].'.MAIN_JOBS',
					"JOB_CUS_NO='".$fe['CusNo']."' AND JOB_REF_MAIN_ID='".$fJob['JobID']."'");
				}
			}
		}else{
			$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_JOBS(JOB_CUS_NO,JOB_REF_MAIN_ID,JOB_NAME,JOB_DESC,JOB_MAIN_ACTIVE,JOB_STATUS)VALUES";
			$insert .= "('".$fe['CusNo']."','".$fJob['JobID']."','".$fe['Job']."','".$fe['Job_Des']."','1','0');";
			$query = mysql_query($insert);
			
			if(!$query){
				$this->gQueryLogError($insert,'manageMain_data.php Line : '.__LINE__.'');
			}else{
				$this->gQueryAndLogUpdate(0,'','manageMain_data.php Line : '.__LINE__.'',$this->config['cusNew']['db'].'.MAIN_JOBS',
				"JOB_CUS_NO='".$fe['CusNo']."' AND JOB_REF_MAIN_ID='".$fJob['JobID']."'");
			}
		}
		//ข้อมูลอาชีพ
		
		//เบอร์โทรศัพท์
		$arrTel = array('1'=>'M_Tel','2'=>'W_Tel','3'=>'H_Tel');
		
		foreach($arrTel AS $key => $value){
			if($fe[$value]){
				if($status=='update'){
					$chk = "SELECT TEL_ID FROM ".$this->config['cusNew']['db'].".MAIN_TELEPHONE WHERE TEL_CUS_NO='".$fe['CusNo']."' AND TEL_TYPE='".$key."' LIMIT 1";
					$qChk = mysql_query($chk);
					$fChk = @mysql_fetch_assoc($qChk);
					
					if(!$qChk){
						$this->gQueryLogError($chk,'manageMain_data.php Line : '.__LINE__.'');
					}
					
					if($fChk['TEL_ID']){
						$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_TELEPHONE SET TEL_NUM='".$fe[$value]."' WHERE TEL_ID='".$fChk['TEL_ID']."' LIMIT 1";
						$this->gQueryAndLogUpdate(1,$update,'manageMain_data.php Line : '.__LINE__.'');
						$query = mysql_query($update);
						
						if(!$query){
							$this->gQueryLogError($update,'manageMain_data.php Line : '.__LINE__.'');
						}
					}else{
						$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_TELEPHONE(TEL_CUS_NO,TEL_NUM,TEL_TYPE,TEL_DESC,TEL_REMARK)VALUES";
						$insert .= "('".$fe['CusNo']."','".$fe[$value]."','".$key."','','');";
						$query = mysql_query($insert);
						
						if(!$query){
							$this->gQueryLogError($insert,'manageMain_data.php Line : '.__LINE__.'');
						}else{
							$this->gQueryAndLogUpdate(0,'','manageMain_data.php Line : '.__LINE__.'',$this->config['cusNew']['db'].'.MAIN_TELEPHONE',
							"TEL_CUS_NO='".$fe['CusNo']."' AND TEL_TYPE='".$key."'");
						}
					}
				}else{
					$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_TELEPHONE(TEL_CUS_NO,TEL_NUM,TEL_TYPE,TEL_DESC,TEL_REMARK)VALUES";
					$insert .= "('".$fe['CusNo']."','".$fe[$value]."','".$key."','','');";
					$query = mysql_query($insert);
					
					if(!$query){
						$this->gQueryLogError($insert,'manageMain_data.php Line : '.__LINE__.'');
					}else{
						$this->gQueryAndLogUpdate(0,'','manageMain_data.php Line : '.__LINE__.'',$this->config['cusNew']['db'].'.MAIN_TELEPHONE',
						"TEL_CUS_NO='".$fe['CusNo']."' AND TEL_TYPE='".$key."'");
					}
				}
			}
		}
		//เบอร์โทรศัพท์
	}
	
	//บันทึกข้อมูลลูกค้า
	function manageCustomer($id){
		$this->setConnect_customer();
		
		if($this->disDB['cusNew']=='ON' && $this->disDB['cusOld']=='ON'){
			//เช็คข้อมูลว่ามีหรือยัง
			$chk = "SELECT CusNo FROM ".$this->config['cusNew']['db'].".MAIN_CUS_GINFO WHERE CusNo='".$id."' LIMIT 1";
			$qChk = mysql_query($chk);
			$fChk = @mysql_fetch_assoc($qChk);
			
			if(!$qChk){
				$this->gQueryLogError($chk,'manageMain_data.php Line : '.__LINE__.'');
			}else{
				//update
				if($fChk['CusNo']){
					$this->manageDB_Customer($id,'update');
				//insert
				}else{
					$this->manageDB_Customer($id,'insert');
				}
			}
		}
		
		@mysql_close( $this->conn );
	}
}; // end class manageMain_data
?>