<?php
	## ได้เฉพาะทศนิยม
	## class ปัดเศษทศนิยม ให้ตรงกับผลรวม
	class roundingDecimal{
		private $position = 2; ## 2 ตำแหน่ง
		private $sum = 0;
		private $listSum = 0;
		private $vat = 0.07; ## 7 percent
		private $sumRound = 0;
		private $listRound = array();
		
		## ปัดทศนิยม ใช้แทน round
		public function my_round($num,$pos){
			$new_num = $num;
			$exp = explode('.',(string)$num);
			if($exp[1]){
				$len = strlen($exp[1])+1;
				if($len<=$pos){$len = $pos+1;}
				$new_num =  $num + (float)("0.".sprintf("%0".$len."d", 1));
				//echo "<br>$new_num =  $num + (float)(\"0.\".sprintf(\"%0\".$len.\"d\", 1)) = ".(float)("0.".sprintf("%0".$len."d", 1))." = ".round($new_num,$pos);
			}
			return round($new_num,$pos);
		}
	
		## คำนวณภาษีให้เรียบร้อย ** ดัดแปลงใช้กับอย่างอื่นได้
		public function rounding_Decimal($position,$sum,$list,$vat){
			$this->position = $position;
			$this->sum = $sum;
			$this->listSum = $list;
			$this->vat = $vat;
			
			// ## หา vat
			if(count($this->listSum)>0){
				$sumVat = $this->sum*$this->vat;
				$sumRound = $this->my_round($sumVat,$this->position);
				
				// echo "<textarea>";
				// echo "main : ".$sumVat." : ".$sumRound."\n";
			
				foreach($this->listSum AS $key=>$value){
					$listVat[$key] = $value*$this->vat;
					$listRound[$key] = $this->my_round($value*$this->vat,$this->position);
					
					// echo "list : ".$value*$this->vat." : ".$listRound[$key]."\n";
				}
			}
			
			// echo "sum : ".array_sum($listVat)." : ".array_sum($listRound)."\n";
			// echo "</textarea><br/><br/>";
			$return = $this->rounding_numDecimal($sumRound,$listVat,$this->position);
			
			return $return;
		}
		
		private function manageCal_Decimal($cutDecimal,$type){
			## ถ้าจะบวกเพิ่ม
			if($type=='Plus'){
				// $valPlus = "0.".sprintf("%0".$this->position."d", 1); ## นำค่านี้ไปบวก
				$valPlus = (1/pow(10,$this->position)); ## นำค่านี้ไปบวก
				arsort($cutDecimal); ## เรียงจากมากไปน้อย
			## ถ้าจะลดลง
			}else{
				// $valPlus = "-0.".sprintf("%0".$this->position."d", 1); ## นำค่านี้ไปลบ
				$valPlus = ((1/pow(10,$this->position))*-1); ## นำค่านี้ไปบวก
				asort($cutDecimal); ## เรียงจากน้อยไปมาก
			}
			
			// echo $valPlus;
			
			// echo "<br/><br/>cutDecimal : <br/>";
			// print_r($cutDecimal);
			// echo "<br/></br><textarea>";
			
			$cut = 'N'; $i = 0;
			while($cut=='N'){
				$cut = $this->cutDecimal($cutDecimal,$valPlus);
				
				$i++;
				if($i==1000){ $cut='Y'; } ## limit มันไว้
			}
			
			// echo "</textarea>";
			// echo "<br/><br/>";
			// print_r($this->listRound);
		}
		
		private function cutDecimal($cutDecimal,$valPlus){
			$break = 'N';
		
			foreach($cutDecimal AS $key=>$value){
			
				$this->listRound[$key] = $this->listRound[$key]+$valPlus;
				// echo "Round : ".$this->listRound[$key];
				// echo "\n";
				
				// echo "\n\n";
				// print_r($this->listRound);
				
				// echo "\n\n $key : ".$this->my_round($this->sumRound,$this->position)."==".$this->my_round(array_sum($this->listRound),$this->position);
				if($this->my_round($this->sumRound,$this->position)==$this->my_round(array_sum($this->listRound),$this->position)){
					// echo "<br/>Oh break!!<br/>";
					$break = 'Y'; break; 
				}
			}
			
			return $break;
		}
		
		public function rounding_numDecimal($sumRound,$numeric,$position){
			$this->position = $position;
			$this->sumRound = $sumRound;
			
			foreach($numeric AS $key=>$value){
				$this->listRound[$key] = $this->my_round($value,$position);
			}
			
			$diff = $this->sumRound-array_sum($this->listRound); ## ส่วนต่าง
			$cutDecimal = $this->cutValue_decimalList($numeric,$position);
			
			if($diff>0){
				$this->manageCal_Decimal($cutDecimal,$position,'Plus');
			}else if($diff<0){
				$this->manageCal_Decimal($cutDecimal,$position,'Cut');
			}else{
				## return $this->sumRound;
			}
			
			return $this->listRound;
		}
		
		## ถ้าอยากปัดจำนวนเต็มก็น่าจะตรงนี้
		## เอาค่าที่ตัดจากทศนิยม ไปเรียงลำดับ
		public function cutValue_decimalList($arr,$position){
			foreach($arr AS $key=>$value){
				list($int,$decimal) = explode('.',$value);
				
				## ปัดในส่วนทศนิยม
				if($position>0){
					$sub = abs(substr($decimal,$position));
					$arrSub[$key] = '0.'.$sub;
				## ปัดในส่วนจำนวนเต็ม
				}else if($position<0){
					$arrSub[$key] = 0;
				## ไม่ปัด
				}else{
					$arrSub[$key] = 0;
				}
			}

			return $arrSub;
		}
	}
?>