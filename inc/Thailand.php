<?php
/*###########################################  FUNCTION  GETProvince #########################################################*/

if( !function_exists( 'GETProvince' ) ){
  function GETProvince( $iProvince = null,$geography = null){
	global $tpl,$conn,$config;
	$Province=null;
	$iProvinceArry=explode("_",$iProvince);
	$sql="select * from ".$config['db_emp'].".province "; 
	if($geography){
		$geographyArry=explode("_",$geography);
	$sql .=" where geo_id ='$geographyArry[0]' "; 
	}
	$sql .=" ORDER BY `province_name`";
	mysql_query("SET NAMES utf8");
	$result=mysql_query($sql);

	while($rs = mysql_fetch_array( $result )){
		$selected=null;
		$rs['province_name']=str_replace(' ', "",$rs['province_name']);
		if($iProvinceArry[0]== $rs['province_id'] ) $selected='selected';
		else if($iProvinceArry[0]== $rs['province_name'] ) $selected='selected';
		else if($iProvinceArry[1]== $rs['province_name'] ) $selected='selected';
		
		$Province .='<option '.$selected.' value="'.$rs['province_id']."_".$rs['province_name'] .'" >'.$rs['province_name'] .'</option>';
	}
      return $Province;
  } 
}
/*###############################################################################################################################*/

/*###########################################  FUNCTION  GETGEO #########################################################*/
if( !function_exists( 'GETGEO' ) ){
  function GETGEO( $geography = null){
	  global $tpl,$conn,$config;
		$sgeography=null;
		if(!$geography){
			$geography="0";
		}
		$geographyArry=explode("_",$geography);
			$sql="select * from ".$config['db_emp'].".geography ORDER BY `geo_id`"; 
			mysql_query("SET NAMES utf8");
		$result=mysql_query($sql);

	while($rs = mysql_fetch_array( $result )){
		$selected=null;
		$rs['geo_name']=str_replace(' ', "",$rs['geo_name']);
		if($geographyArry[0]== $rs['geo_id'] ) $selected='selected';
		else if($geographyArry[0]== $rs['geo_name'] ) $selected='selected';
		else if($geographyArry[1]== $rs['geo_name'] ) $selected='selected';

		$sgeography .='<option '.$selected.' value="'.$rs['geo_id']."_".$rs['geo_name'] .'" >'.$rs['geo_name'] .'</option>';
	}
      return $sgeography;
  } 
}
/*###############################################################################################################################*/

/*###########################################  FUNCTION  GETAmphur #########################################################*/
if( !function_exists( 'GETAmphur' ) ){
  function GETAmphur( $iProvince = null,$iamphur = null){
	  global $tpl,$conn,$config;
		$GETAmphur=null;
		$iProvinceArry=explode("_",$iProvince);
		$iamphurArry=explode("_",$iamphur);
			$sql="select * from ".$config['db_emp'].".amphur where province_id='$iProvinceArry[0]' ORDER BY `amphur_name`"; 
			mysql_query("SET NAMES utf8");
		$result=mysql_query($sql);
			$Num = mysql_num_rows($result);

		if(!$Num ){
			$sql="select * from ".$config['db_emp'].".amphur  ORDER BY `amphur_name`"; 
			mysql_query("SET NAMES utf8");
		$result=mysql_query($sql);
			$Num = mysql_num_rows($result);

		}

	while($rs = mysql_fetch_array( $result )){
	$selected=null;
	$rs['amphur_name']=str_replace(' ', "",$rs['amphur_name']);

	if($iamphurArry[0]== $rs['amphur_id'] ) $selected='selected';
	else if($iamphurArry[0]== $rs['amphur_name'] ) $selected='selected';
	else if($iamphurArry[1]== $rs['amphur_name'] ) $selected='selected';
	else if($iamphurArry[0].$iProvinceArry[1]== $rs['amphur_name'] ) $selected='selected';
	else if($iamphurArry[1].$iProvinceArry[1]== $rs['amphur_name'] ) $selected='selected';
	$GETAmphur .='<option '.$selected.' value="'.$rs['amphur_id']."_".$rs['amphur_name'] .'" >'.$rs['amphur_name'] .'</option>';
		}
		  return $GETAmphur;
	  } 
}
/*###############################################################################################################################*/

/*###########################################  FUNCTION  GETDistrict #########################################################*/
if( !function_exists( 'GETDistrict' ) ){
  function GETDistrict( $iProvince = null,$iamphur = null,$idistrict = null){
	  global $tpl,$conn,$config;

	$iProvinceArry=explode("_",$iProvince);
	$iamphurArry=explode("_",$iamphur);
	$idistrictArry=explode("_",$idistrict);

	$sql="select * from ".$config['db_emp'].".district where province_id='$iProvinceArry[0]' and amphur_id='$iamphurArry[0]' ORDER BY `district_name`"; 
	mysql_query("SET NAMES utf8");
	$result=mysql_query($sql);
	$Num = mysql_num_rows($result);

	if(!$Num ){
		$sql="select * from ".$config['db_emp'].".district  ORDER BY `district_name`"; 
		mysql_query("SET NAMES utf8");
		$result=mysql_query($sql);
		$Num = mysql_num_rows($result);
	}
	$GETDistrictss=null;
	while($rs = mysql_fetch_array( $result )){
	$selected=null;
	$rs['district_name']=str_replace(' ', "",$rs['district_name']);
	if($idistrictArry[0]== $rs['district_id'] ) $selected='selected';
	else if($idistrictArry[0]== $rs['district_name'] ) $selected='selected';
	else if($idistrictArry[1]== $rs['district_name'] ) $selected='selected';
	$GETDistrictss .='<option '.$selected.' value="'.$rs['district_id']."_".$rs['district_name'] .'" >'.$rs['district_name'] .'</option>';
		}
		  return $GETDistrictss;
	  } 
}
/*###############################################################################################################################*/

/*###########################################  FUNCTION  PostCode #########################################################*/
if( !function_exists( 'PostCode' ) ){
  function PostCode( $iProvince = null,$iamphur = null,$idistrict = null){
	  global $tpl,$conn,$config;
	$GETDistrict=null;
	$iProvinceArry=explode("_",$iProvince);
	$iamphurArry=explode("_",$iamphur);
	$idistrictArry=explode("_",$idistrict);


	$sql="select * from ".$config['db_emp'].".postcode where province_id='$iProvinceArry[0]' and amphur_id='$iamphurArry[0]' ORDER BY `PostCode`"; 
	mysql_query("SET NAMES utf8");
	$result=mysql_query($sql);
	$Num = mysql_num_rows($result);

	while($rs = mysql_fetch_array( $result )){
	$GETDistrict="<font size='1' color='#000000'><b>*รหัสแนะนำ <a  href='javascript:void(0);' 
		onclick=\"javascript:document.getElementById('Postcode').value='".$rs['PostCode']."';\">
		<font size='3' color='#327E04'>".$rs['PostCode']."</font></a> ".$rs['PostOffice'] ."  " .$rs['Note']."*</b></font>";
	}
      return $GETDistrict;
  } 
}
/*###############################################################################################################################*/
if( !function_exists( 'getPostCode' ) ){     
function getPostCode($province = null, $amphur = null ){
    global $tpl, $conn, $config;
    $provinceArray = explode("_",$province);
    $amphurArray = explode("_",$amphur);   
    $resultArray = null;
    
    $sql = " SELECT *    
             FROM ".$config['db_emp'].".postcode
             WHERE province_id='$provinceArray[0]' and amphur_id='$amphurArray[0]' ORDER BY `PostCode`"; 
    mysql_query("SET NAMES utf8");   
    $result=mysql_query($sql);   
    $Num = mysql_num_rows($result);     
    while($rs = mysql_fetch_array( $result )){
        $resultArray[0] = $rs['PostCode']; 
        $resultArray[1] = $rs['Note'];    
    }    
        return $resultArray;                                           
  }
}
?>