<?php
if( !function_exists( 'dbThrowCheckExt' ) ){
  function dbThrowCheckExt( $db, $sWord, $check ){
    return $GLOBALS['oFF']->throwData($db, $sWord, $check );
  } // end function dbThrowOrderExt
}
if( !function_exists( 'dbListCheck' ) ){
  function dbListCheck($db){
    global $config;
    return $GLOBALS['oFF']->throwFileArrayPages(  $db, null, $GLOBALS['iPage'], ADMIN_LIST );
  } // end function dbListOrders
}
if( !function_exists( 'dbListCheckSearch' ) ){
  function dbListCheckSearch( $db,$sWord ){
    global $config;
    $aFile      = file(  $db );
    $iCount     = count( $aFile );
    $iFindPage  = 0;
    $iFindAll   = 0;
    $sWord      = preg_quote( $sWord );
    for( $i = 1; $i < $iCount; $i++ ){
      $aExp = explode( '$', $aFile[$i] );
      if( eregi( $sWord, $aFile[$i] ) ){
        $iFindPage++;
        $iFindAll++;
        if( $iFindPage == 1 )
          $aPageStart[] = $i;
       if( isset( $aPageStart[$GLOBALS['iPage'] - 1] ) && !isset( $aPageEnd[$GLOBALS['iPage'] - 1] ) ){
          $aData[] = $aExp;
        }
        if( $iFindPage == ADMIN_LIST ){
          $aPageEnd[] = $i;
          $iFindPage =  0;
        }}
		} // end for

    if( isset( $aData ) ){
      $aData[0]['iFindAll'] = $iFindAll;
      return $aData;
    }else
      return null;
  } // end function dbListOrdersSearch
}

if( !function_exists( 'dbListCheckStatus' ) ){
  function dbListCheckStatus($db, $iStatus, $CheckCount=null, $Count ){
    global $config;

  $iStatus=$_GET['iStatus'];
    $aFile      = file(  $db );
    $iCount     = count( $aFile );
    $iFindPage  = 0;
    $iFindAll   = 0;

    $aStatus    = dbThrowMemberStatus($db,  $CheckCount, $Count );

    for( $i = 1; $i < $iCount; $i++ ){
      $aExp = explode( '$', $aFile[$i] );
      if( isset( $aStatus[$aExp[0]] ) && $aStatus[$aExp[0]] == $iStatus ){
        $iFindPage++;
        $iFindAll++;
        
        if( $iFindPage == 1 )
          $aPageStart[] = $i;

        if( isset( $aPageStart[$GLOBALS['iPage'] - 1] ) && !isset( $aPageEnd[$GLOBALS['iPage'] - 1] ) ){
          $aData[] = $aExp;
        }

        if( $iFindPage == ADMIN_LIST ){
          $aPageEnd[] = $i;
          $iFindPage =  0;
        }
      }
    } // end for

    if( isset( $aData ) ){
      $aData[0]['iFindAll'] = $iFindAll;
      return $aData;
    }
    else
      return null;
  } // end function dbListOrdersStatus
}

if( !function_exists( 'dbThrowMemberStatus' ) ){
  function dbThrowCheckStatus( $db,  $CheckCount, $Count ){
	      global $config;
    return $GLOBALS['oFF']->throwFileArraySmall( $db, null, $CheckCount, $Count  );
  } // end function dbThrowOrdersStatus
}
if( !function_exists( 'dbDelAllFile' ) ){
  function dbDelAllFile( $db, $CheckCount, $iOrder ){
    global $oFF;
    $oFF->deleteInFile(  $db, $iOrder, $CheckCount );
  } // end function dbDelOrder
}
if( !function_exists( 'throwGuaranteerStatusSelect' ) ){
  function throwGuaranteerStatusSelect( $iStatus = null ){
    return throwSelectFromArray( throwGuaranteeStatus( null, true ), $iStatus );
  } // end function throwOrderStatusSelect
}
if( !function_exists( 'throwGuaranteeStatus' ) ){
  function throwGuaranteeStatus( $iStatus = 0, $bAll = null ){
    global $lang;
	$aStatus[0] = $lang['Notgeneral'];
    $aStatus[1] = $lang['general'];
    $aStatus[2] = $lang['memberSatus'];
    $aStatus[3] = $lang['Goldmember'];
    $aStatus[4] = $lang['PlayMemberOne'];
    if( isset( $bAll ) )
      return $aStatus;
    else
      return $aStatus[$iStatus];
  } // end function throwOrderStatus
}
if( !function_exists( 'throwUserStatusSelect' ) ){
  function throwUserStatusSelect( $iStatus = null ){
    return throwSelectFromArray( throwUserFromArray( null, true ), $iStatus );
  } // end function throwOrderStatusSelect
}
if( !function_exists( 'throwUserFromArray' ) ){
  function throwUserFromArray( $iStatus = 0, $bAll = null ){
    global $lang;
	$aStatus[0] = $lang['User_NotOpen'];
    $aStatus[1] = $lang['User_Open'];
    if( isset( $bAll ) )
      return $aStatus;
    else
      return $aStatus[$iStatus];
  } // end function throwOrderStatus
}
if(!function_exists('DbSaveUser')){
function DbSaveUser( $aForm,$db ){
   global $oFF;

     $oFF->setRow( $aForm );
    $oFF->addToFile($db , 'rsort' );  

  } // end function saveOrder
}
if(!function_exists('dbsaveUser')){
  function dbsaveUser( $aData,$Db, $bErase = true ){
    global $oFF;
    if( isset( $bErase ) )
      $oFF->deleteInFile( $Db, $aData[0], 0 );

    $oFF->setRow( $aData );
    $oFF->addToFile( $Db, 'rsort' );  
  } // end function dbAddOrderExtension
}
?>