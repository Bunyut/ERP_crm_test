<?php
//require_once( $_SERVER['DOCUMENT_ROOT'].'/ERP_masterdata/inc/main_function.php' );
require_once( dirname(dirname(dirname(__FILE__))).'/ERP_masterdata/inc/main_function.php' );

class manageMain_data extends configAllDatabase // begin class manageMain_data
{
	var $config;
	var $server;
	var $conn;
	var $chkDB; //เช็คฐานข้อมูล
	var $disDB; //ฐานข้อมูลที่ไม่ใช้
	var $idRef; //ไอดี ของฐานข้อมูลหลัก
	var $BU; //มาจากโปรแกรมไหน
	var $chkRef; //
	// var $dbCh = 3; //1 = 199.199.194.170 , 2= 199.199.199.248, 3= 199.199.194.180, 4= 199.199.194.170 ##report

	/*
	 * easyservice = service
	 * easysale = sale
	 */
	public function manageMain_data($bu=null){
		$this->BU = $bu;
	}

	/*
	 * easyservice = service
	 * easysale = sale
	 */
	public function setBU($bu){
		$this->BU = $bu;
	}

	public function test(){
		echo $this->BU;
	}

	/**
	 *
	 *			gQueryAndLogUpdate
	 *			$atc		=	0:insert, 1:update, 2:delete
	 *			$note   	=  comment
	 *			$sql 		=   sql || last_insert_id
	 *			$tb_name = ชื่อ ตารางที่จะ insert
	 */
	public function gQueryAndLogUpdate($atc,$sql,$note="",$tb_name="",$run_id_name=""){
		$sql_log = "";
		#echo $sql.'<br><br>';
		switch($atc){
			case '0':
				//insert
				/*$last_id = $sql;
				if($last_id=="" && $tb_insert == ""){
				echo "error not have last_insert_id ro tb_name";
				exit();
				}*/

				list($db,$tb) = explode(".",$tb_name);

				#value is updated
				//$run_id_name = ($run_id_name == "") ? "id":$run_id_name;
				$sql = "SELECT * FROM $db.".$tb." WHERE $run_id_name limit 1";
				//echo $sql.'|||';
				$rt_inslog = mysql_query($sql);
				while($r_inslog = @mysql_fetch_assoc($rt_inslog)){
					foreach($r_inslog AS $field => $val){
						$field_names .= $field.'#+';
						$value			.= $val.'#+$';
					}
				}

				$run_id_name = addslashes($run_id_name);
				$sql_log = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_LOGS (`databasename`,`tablename`,`id_t`,`emp_id`,`emp_time`,`log_status`,`fieldname`,";
				$sql_log .= "`before`,`afters`,`note`)VALUES( '".$db."','".$tb."','".$run_id_name."','".$_SESSION['SESSION_ID_card']."',NOW(),'0','".$field_names."',";
				$sql_log .= "'','".$value."','".$note."')";
				#field name
				//echo $sql_log;
				$query = mysql_query($sql_log) or die("<br>".__FILE__."<br>".__LINE__."<br>".mysql_error());
				break;
			case '1':
				//update

				$db 		=	"";
				$tb			=  "";
				$content	=  "";
				$where		=  "";
				$arr_exp = array();
				$arr_content = array();

				$sql_field 	= "";
				$sql_val 	= "";

				$sql_field_log 			= "";
				$sql_val_old_log 		= "";
				$sql_val_new_log 		= "";

					
				#$patt1  			=  "/^UPDATE(?<db>[ก-๙\sa-z_\d\s]+).(?<tb>[ก-๙\sa-z_\d\s]+)SET(?<content>[ก-๙\sa-z=\d\s',_]+)/";
				$patt1  			=  "/^UPDATE(?<db>[ก-๙\sa-z_\d\s.A-Z+\-]+)SET(?<content>[เก-๙\sa-z=\d\s'*\.@,_A-Z\-:+]+)/";
				$patt2     		= "/'[\s]*,/";
				#$patt1  			=  "/^UPDATE(?<db>[ก-๙\sa-z_\d\s]+)[.]*(?<tb>[ก-๙\sa-z_\d\s]+)SET(?<content>[ก-๙\sa-z=\d\s',_]+)/";
				$synt = explode('WHERE',$sql);
				#echo $synt[0]."<br><br>";
				if(preg_match($patt1,$synt[0], $matches)){

					$ar_dbtb = explode('.',$matches['db']);
					if(count($ar_dbtb) == 1){
						if($this->config['cusNew']['db']){
							$db 		= 	$this->config['cusNew']['db'];
							$tb			=	$ar_dbtb[0];
						}else{
							$db 		= 	$this->config['vehicle']['db'];
							$tb			=	$ar_dbtb[0];
						}
					}else{
						$db 		= 	$ar_dbtb[0];
						$tb			=	$ar_dbtb[1];
					}
					$content	= $matches['content'];
					#echo $content.'<br>';
					$arr_exp = preg_split($patt2, $content);

					/* echo '<textarea style="width:300px;height:300px">';
					 print_r($arr_exp);
					 echo '</textarea>';
					 echo '<br><br>';  */

					$count_e =1;
					$count_max = count($arr_exp);
					foreach($arr_exp AS $no=>$data){
							
						$f_e = array();
						$f_e = explode('=', $data);
						$arr_content[$f_e[0]] = str_replace("'", "", $f_e[1]);

						$sql_field   			.=  ($count_e < $count_max)? "".trim($f_e[0]).",":"".trim($f_e[0])."";
						$sql_val 				.=  ($count_e < $count_max)? "'".trim($f_e[1])."','":"'".trim($f_e[1])."'";
						$sql_field_log   		.=  ($count_e < $count_max)? "".trim($f_e[0])."#+":"".trim($f_e[0])."";
						$sql_val_new_log 	.=  ($count_e < $count_max)? "".trim(str_replace("'", "", $f_e[1]))."#+":"".trim(str_replace("'", "", $f_e[1]))."";
						$count_e++;
					}

				}else{
					echo "sql error:".$sql;
				}
				#	หา run_no
				#echo 'db:'.$db.'<br>tb:'.$tb;

				#$db_and_tb = ($db != '')? $db.".".$tb:$tb;
				$sql_r = "SHOW COLUMNS FROM ".$db.".".$tb;
				#echo "<br>".$sql_r."<br>";
				#echo "sql error:".$sql.'<br>';
				$rt	=	mysql_query($sql_r);
				while ($get_info = @mysql_fetch_row($rt)) {

					$field_no 				=  $get_info[0];
					break;
				}
				#--

				#exit();
				$sql_select = "SELECT ".$field_no.",".$sql_field." FROM ".$db.".".$tb."  WHERE ".$synt[1];
				#echo "select:".$sql_select."<br>";
				$rt_select 	= mysql_query($sql_select);
				while($r_select	= @mysql_fetch_array($rt_select))
				{
					$id_run		= $r_select[$field_no];

					$arr_sql_field = explode(',',$sql_field);
					$count_e = 1;
					$count_max  = count($arr_sql_field);
					$sql_val_old_log	= "";
					#	หาข้อมูลเดิม
					foreach($arr_sql_field AS $e_no=>$f_n){

						$sql_val_old_log	 	.=  ($count_e < $count_max)? "".$r_select[trim($f_n)]."#+":"".$r_select[trim($f_n)]."";
						$count_e++;
					}
					#--
					#echo '$sql_val_old_log:'.$sql_val_old_log.'<br>';
					#echo '$sql_val_new_log:'.$sql_val_new_log.'<br>';
					$sql_log = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_LOGS (`databasename`,`tablename`,`id_t`,`emp_id`,`emp_time`,`log_status`,";
					$sql_log .= "`fieldname`,`before`,`afters`,`note`)VALUES( '".trim($db)."','".trim($tb)."','".$id_run."','".$_SESSION['SESSION_ID_card']."',";
					$sql_log .= "NOW(),'1','".$sql_field."','".$sql_val_old_log."','".$sql_val_new_log."','".$note."')";
					#field name
					#echo $sql_log."<br>";
					mysql_query($sql_log);
				}
				#print_r($arr_content);
				break;
			case '2':
				//delete
					
				break;
		}
	}

	/**
	 *			เก็บข้อมูล
	 *			$db		=	ชื่อฐานข้อมูล
	 *			$tb		=	ชื่อตารางฐานข้อมูล
	 *			$where  =	เงื่อนไข
	 *			$BU		=	จากโปรแกรมไหน
	 *			$do		=
	 */
	public function getLog_data($db=null,$tb,$where=null,$BU='undefine',$do='update'){

		switch($do){
			case 'insert':
				$do = 1;
				break;
			case 'update':
				$do = 2;
				break;
			case 'delete':
				$do = 3;
				break;
		}

		$sql = "SELECT * FROM ".$db.".".$tb." WHERE ".$where." LIMIT 1";
		$que = mysql_query($sql);
		$fe = @mysql_fetch_assoc($que);

		if(!$que){
			$this->gQueryLogError($sql,__FILE__.' Line : '.__LINE__);
		}

		if(!empty($fe)){
			$filed = null;
			foreach($fe AS $key => $value){
				//echo $key." => ".$value."<br><br>";
				if($filed){ $filed .= ","; }
				$filed .= $key;

				if($val){ $val .= ","; }
				$val .= "'".$value."'";
			}

			$insert = "INSERT INTO ".$db.".LOG_".$tb."(".$filed.",BU,DO)VALUES(".$val.",'".$BU."','".$do."')";
			# echo "<font color='#0000FF'>FILE :: ".__FILE__." >> LINE :: ".__LINE__."</font><br/>".$insert."<br/><br/>";
			$query = mysql_query($insert);
			
			if(!$query){
				$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
			}
		}
	}

	public function gQueryLogRes($sql,$id){
		$sql = addslashes($sql);
		$id = addslashes($id);

		$insert = "INSERT INTO ".$this->config['cusNew']['db'].".LOG_RESPON(LOGRES_SQL,ID_CARD,EMP_ID,DATE_ADD)VALUES('".$sql."','".$id."',";
		$insert .= "'".$_SESSION['SESSION_ID_card']."',NOW())";
		$query = mysql_query($insert);

		if(!$query){
			$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
		}
	}

	public function gQueryLogError($sql,$file){
		$sql = addslashes($sql);
		$file = addslashes($file);

		$insert = "INSERT INTO ".$this->config['cusNew']['db'].".LOGS_ERROR(ERROR_SQL,ERROR_FILE,EMP_ID,DATE_ADD)VALUES('".$sql."','".$file."',";
		$insert .= "'".$_SESSION['SESSION_ID_card']."',NOW())";
		mysql_query($insert);
	}

	//เซตคอนฟิก
	public function setConnect($bu=null){
		
		if($bu){
			$this->BU = $bu;
		}else if(!$this->BU){
			global $configBu; //global config
			$this->BU = $configBu;
		}
		
		/* $this->con['ServerName']	= "localhost";
		$this->con['UserName']		= "root";
		$this->con['UserPassword'] = "123456";

		$this->con['db_base_name'] = "main_crm";   			 	 	# - crm
		$this->con['db_emp'] 		= "easyhr_icchecktime2";   		 	# -	checktime
		$this->con['db_maincus'] 	= "main_cusdata";         	 		# - new maincus
		$this->con['Cus']			= 'new_maincusdata';				# - old maincus
		$this->con['db_organi'] 	= "easyhr_OU";     					# - ou
		$this->con['db_leasing'] 	= "easyloan_icleasing";     		# - leasing
		$this->con['db_easysale'] 	= "new_icmba";     					# - easysale
		$this->con['db_service'] 	= "icservice";     					# - easysale
		$this->con['db_vehicle'] 	= "main_vehicle";     				# - mainVehicle */
		
		$this->calConfigAllDatabase();
		
		$ServerName		= $this->con['ServerName'];
		$UserName 		= $this->con['UserName'];
		$UserPassword 	= $this->con['UserPassword'];

		$vehicle_db 	= $this->con['db_vehicle'];
		$service_db 	= $this->con['db_service'];
		$sale_db 		= $this->con['db_easysale'];
		$cusNew_db 		= $this->con['db_maincus'];
		
		$this->sale		= $sale_db;

		$this->server['ServerName']	= $ServerName;
		$this->server['UserName'] = $UserName;
		$this->server['UserPassword'] = $UserPassword;

		//if($BU!='main'){
		$this->config['vehicle']['db'] = $vehicle_db;
		$this->config['vehicle']['sv'] = $this->server['ServerName'];
		$this->config['vehicle']['user'] = $this->server['UserName'];
		$this->config['vehicle']['pass'] = $this->server['UserPassword'];
		//if($BU!='service'){

		//if($BU!='service'){
		$this->config['service']['db'] = $service_db;
		$this->config['service']['sv'] = $this->server['ServerName'];
		$this->config['service']['user'] = $this->server['UserName'];
		$this->config['service']['pass'] = $this->server['UserPassword'];
		//}

		//if($BU!='sale'){
		//$this->config['sale']['db'] = $sale_db;
		//$this->config['sale']['sv'] = $this->server['ServerName'];
		//$this->config['sale']['user'] = $this->server['UserName'];
		//$this->config['sale']['pass'] = $this->server['UserPassword'];
		//}

		$this->config['cusNew']['db'] = $cusNew_db;
		$this->config['cusNew']['sv'] = $this->server['ServerName'];
		$this->config['cusNew']['user'] = $this->server['UserName'];
		$this->config['cusNew']['pass'] = $this->server['UserPassword'];

		{ //เปิดปิดฐานข้อมูล
			$this->disDB['vehicle'] = 'ON';
			$this->disDB['service'] = 'ON';
			//$this->disDB['sale'] = 'ON';
			$this->disDB['cusNew'] = 'ON';
		}

		{ //array ไว้เช็คตารางฐานข้อมูล
			$this->chkDB['vehicle'] = array('VEHICLE_INFO','VEHICLE_BRANDS','VEHICLE_COMPANY','VEHICLE_RELATIONSHIP','VEHICLE_REGIS');
			$this->chkDB['service'] = array('ahistory','abrand','asell_company');
			//$this->chkDB['sale'] = array('Main_Competitor','Sell','regis_car');
			$this->chkDB['cusNew'] = array('LOGS_ERROR','MAIN_LOGS');
		}

		$this->connect();
	}

	//connect ฐานข้อมูล
	public function connect(){
		$arrCon = array();

		$i = 0;
		foreach($this->config AS $key => $value){
			if($this->disDB[$key]=='ON'){
				$chkArray = @array_search($this->config[$key]['user'],$arrCon['user']);

				if(!$chkArray){ $chkArray = 0; }
				if($this->config[$key]['sv']!=$arrCon['sv'][$chkArray] || $this->config[$key]['user']!=$arrCon['user'][$chkArray] || $this->config[$key]['pass']!=$arrCon['pass'][$chkArray]){
					$this->conn = mysql_connect($this->config[$key]['sv'],$this->config[$key]['user'],$this->config[$key]['pass']);
					if(!$this->conn);

					$arrCon['user'][$i] = $this->config[$key]['user'];
					$arrCon['pass'][$i] = $this->config[$key]['pass'];
					$arrCon['sv'][$i] = $this->config[$key]['sv'];

					$i++;
				}

				if($this->chk_Data($key)){
					mysql_select_db($this->config[$key]['db'],$this->conn);
					@mysql_db_query($this->config[$key]['db'], "SET NAMES UTF8");

					$this->disDB[$key] = 'ON';
				}else{
					$this->disDB[$key] = 'OFF';
				}
			}
		}
		//@mysql_close( $this->conn );
	}

	//ไว้หา biz_id
	public function fullName_Bu($BU){
		switch($BU){
			case 'sale':
				$fullName = 'EASY SALE';
				break;
			case 'service':
				$fullName = 'EASY SERVICE';
				break;
			case 'leasing':
				$fullName = 'EASY LEASING';
				break;
		}

		return $fullName;
	}

	//เช็คฐานข้อมูล
	public function chk_Data($BU){
		$return = false;

		$chkDB = "SHOW DATABASES LIKE '".$this->config[$BU]['db']."'";
		$queDB = mysql_query($chkDB);
		$feDB = @mysql_fetch_row($queDB);

		if(!$queDB){ $this->gQueryLogError($chkDB,__FILE__.' Line : '.__LINE__); }

		//เช็คฐานข้อมูล
		if($feDB[0]){
			foreach($this->chkDB[$BU] AS $key => $value){
				$chkTB = "SHOW TABLES FROM ".$this->config[$BU]['db']." LIKE '".$value."'";
				$queTB = mysql_query($chkTB);
				$feTB = @mysql_fetch_row($queTB);

				if(!$queTB){ $this->gQueryLogError($chkTB,__FILE__.' Line : '.__LINE__); }

				//เช็คตารางฐานข้อมูล
				if($feTB[0]){
					$return = true;
				}else{
					$return = false;
					break;
				}
			}
		}else{
			$return = false;
		}

		return $return;
	}

	// ล็อกตารางฐานข้อมูล
	public function logTable_DB($tb){
		### mysql_query("LOCK TABLES $tb WRITE"); // lock
		### mysql_query("SET AUTOCOMMIT = 0");
	}

	//ปลดล็อกตารางฐานข้อมูล
	public function unlogTable_DB(){
		### mysql_query("COMMIT");
		### mysql_query("UNLOCK TABLES"); // unlock
	}

	//เช็ค ว่าจะต้องเป็น เพิ่มหรือแก้ไข
	public function chk_Table($arr){

		//ส่วนที่จะดึงไปใช้
		foreach($arr['vReturn'] AS $key => $value){
			if($value){
				if($vReturn){ $vReturn .= ","; }
				$vReturn .= $value;
			}
		}

		if(abs($arr['idRef']['field'][$arr['idRef']['field']['chk']])){
			foreach($arr['idRef']['field'] AS $key => $value){
				if($arr['idRef']['mark'][$key] && $key){
					if($whereChk){ $whereChk .= " AND "; }
					$value = addslashes(stripslashes($value));
					$whereChk .= $key."".$arr['idRef']['mark'][$key]."'".$value."'";
				}
			}

			$chk = "SELECT ".$vReturn." FROM ".$this->config[$arr['BU']]['db'].".".$arr['db']." WHERE ".$whereChk." LIMIT 1";
			// echo $chk."<br/><br/>";
			$qChk = mysql_query($chk);
			$fChk = @mysql_fetch_array($qChk);

			if(!$qChk){ $this->gQueryLogError($chk,__FILE__.' Line : '.__LINE__); }
		}

		if($fChk[$arr['vReturn']['id']]){
			$return['statusTb'] = 'update';
			$return = array_merge($return,$fChk);
		}else{
			//เช็คเท่ากับ
			foreach($arr['field'] AS $key => $value){
				if($arr['mark'][$key] && $key){
					if($where){ $where .= " AND "; }
					$value = addslashes(stripslashes($value));
					$where .= $key."".$arr['mark'][$key]."'".$value."'";
				}
			}

			$sql = "SELECT ".$vReturn." FROM ".$this->config[$arr['BU']]['db'].".".$arr['db']." WHERE ".$where." LIMIT 1";
			// echo $sql."<br/><br/>";
			$que = mysql_query($sql);
			$fe = @mysql_fetch_array($que);

			if(!$que){ $this->gQueryLogError($sql,__FILE__.' Line : '.__LINE__); }

			if($fe[$arr['vReturn']['id']]){
				$return['statusTb'] = 'update';
				$return = array_merge($return,$fe);
			}else{
				$return['statusTb'] = 'insert';
				$return = $return;
			}
		}

		return $return;
	}

	//เพิ่มข้อมูล
	public function insertDB($key,$sendArr,$chk,$arr){
		if(count($arr)>0){
			$field = $value = '';

			foreach($arr AS $kField => $vField){
				if($kField){
					if($field){ $field .= ","; }
					if($value){ $value .= ","; }
					$field .= $kField;
					// echo $vField."<br/>";
					if($vField){ $vField = addslashes(stripslashes($vField)); }
					$value .= "'".$vField."'";
				}
			}

			$this->logTable_DB($this->config[$key]['db'].".".$sendArr['db']);

			$insert = "INSERT INTO ".$this->config[$key]['db'].".".$sendArr['db']." (".$field.")VALUES(".$value.")";
			 //echo $insert."<br><br>";
			$query = mysql_query($insert);

			if(!$query){
				$this->unlogTable_DB();

				$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
			}else{
				$idRun = mysql_insert_id(); //last id

				$this->unlogTable_DB();

				if($key=='vehicle'){
					$this->getLog_data($this->config[$key]['db'],$sendArr['db'],$sendArr['vReturn']['id']."='".$idRun."'",$this->BU,'insert');

					$this->idRef = $idRun;
				}else{
					$this->gQueryAndLogUpdate(0,'',__FILE__.' Line : '.__LINE__,$this->config[$key]['db'].".".$sendArr['db'],
					$sendArr['vReturn']['id']."='".$idRun."'");
				}
			}
		}
	}

	//แก้ไขข้อมูล
	public function updateDB($key,$sendArr,$chk,$arr){
		if(count($arr)>0){
			$set = '';
			foreach($arr AS $kField => $vField){
				if($kField){
					if($set){ $set .= ","; }
					// echo "".$kField." >> ".$vField."<br/>";
					if($vField){ $vField = addslashes(stripslashes($vField)); }
					$set .= $kField."='".$vField."'";
				}
			}

			$update = "UPDATE ".$this->config[$key]['db'].".".$sendArr['db']." SET ".$set." WHERE ".$sendArr['vReturn']['id']."='".$chk[$sendArr['vReturn']['id']]."' LIMIT 1";
			// echo $update."<br><br>";

			if($key=='vehicle'){
				$this->getLog_data($this->config[$key]['db'],$sendArr['db'],$sendArr['vReturn']['id']."='".$chk[$sendArr['vReturn']['id']]."'",$this->BU,'update');
			}else{
				$this->gQueryAndLogUpdate(1,$update,__FILE__.' Line : '.__LINE__);
			}

			$query = mysql_query($update);

			if(!$query){ $this->gQueryLogError($update,__FILE__.' Line : '.__LINE__); }

			if($key=='vehicle'){
				$this->idRef = $chk[$sendArr['vReturn']['id']];
			}
		}
	}

	public function chkArr($arr,$chk){
		foreach($chk AS $key => $value){
			if(@array_key_exists($key,$arr)){ $data_arr[$key] = $arr[$key]; }
		}

		return $data_arr;
	}

	public function textLength($text){
		$text = stripslashes(trim($text));
		$len = strlen($text);
		
		return $len;
	}
	
	public function substr_chassis($text){
		$text = trim($text);
		$len = $this->textLength($text);
		
		if($len>8){
			$text = mb_substr($text,-8,8,'UTF-8');
		}

		return $text;
	}
	
	public function substr_engine($text){
		$text = trim($text);
		$len = $this->textLength($text);
		
		if($len>6){
			$text = mb_substr($text,-6,6,'UTF-8');
		}

		return $text;
	}
	
	public function updateOther($arr){
		if($arr['chk_dbName']){ $arr['chk_dbName'] .= "."; }
		if($arr['chk_where']){ $arr['chk_where'] = "WHERE ".$arr['chk_where']; }
		
		$update = "UPDATE ".$arr['chk_dbName']."".$arr['chk_tbName']." SET ".$arr['chk_field']."='".$this->idRef."' ";
		$update .= $arr['chk_where']." LIMIT 1";
		//echo $update."<br><br>";
		$this->gQueryAndLogUpdate(1,$update,__FILE__.' Line : '.__LINE__);
		
		$query = mysql_query($update);
		
		if(!$query){
			$this->gQueryLogError($update,__FILE__.' Line : '.__LINE__);
		}
	}

	//เช็ค array (ยี่ห้อ)
	public function chk_arrayBrand($arr){
		$chk = array(
			/*'chk_dbName' => 'new_icmba_crm', //ชื่อฐานข้อมูล
			'chk_tbName' => 'regis_car', //ชื่อตารางฐานข้อมูล
			'chk_where' => 'no=\''.$fe['no'].'\'', //เงื่อนไขที่จะมาแก้ไข
			'chk_field' => 'idRef_vihicle', //ฟิลด์ที่จะแก้ไข*/
			'BRANDS_NAME' => $arr['BRANDS_NAME'], //ชื่อยี่ห้อ ** จำเป็นต้องใช้ในการเช็ค
			'BRANDS_MARK' => $arr['BRANDS_MARK'], //หมายเหตุ
			'BRANDS_STATUS' => $arr['BRANDS_STATUS'], //สถานะ
			'BRANDS_ADD_DATE' => $arr['BRANDS_ADD_DATE'], //วันที่เพิ่มข้อมูล  Ex. timestamp
			'BRANDS_ADD_PERSON' => $arr['BRANDS_ADD_PERSON'], //พนักงานที่เพิ่มข้อมูล
			'idRef_vihicle' => $arr['idRef_vihicle'], //id ที่ใช้เชื่อมต่อกับ Mainvihicle
			'opt' => $arr['opt'], //insert, update, delete 
		);

		$data_arr = $this->chkArr($arr,$chk);

		return $data_arr;
	}

	//บันทึกยี่ห้อรถ
	public function manageBrand($arr){

		$this->setConnect();
		$BU = $this->BU;
		
		$arrLoop = $this->config;
		unset($arrLoop['cusNew']);
		if($arr['chk_dbName'] && $arr['chk_tbName'] && $arr['chk_field']){
			$arrLoop['other']['db'] = $arr['chk_dbName'];
		}

		foreach($arrLoop AS $key => $value){
			if($this->disDB[$key]=='ON' || $key=='other'){
				switch($key){
					case 'vehicle':
						$ch = null;

						$sendArr['BU'] = $key;
						$sendArr['db'] = 'VEHICLE_BRANDS'; //ชื่อตาราง
						$sendArr['vReturn'] = array('id'=>'BRANDS_ID'); //ค่าที่จะคืนกลับมา
						$sendArr['field'] = array('LOWER(BRANDS_NAME)'=>strtolower($arr['BRANDS_NAME']),'BRANDS_STATUS'=>'99'); //ฟิลด์ใน where
						$sendArr['mark'] = array('LOWER(BRANDS_NAME)'=>'=','BRANDS_STATUS'=>'!='); //เครื่องหมายใน where
						$sendArr['idRef']['field'] = array('chk'=>'BRANDS_ID','BRANDS_ID'=>$arr['idRef_vihicle'],'BRANDS_STATUS'=>'99'); //ฟิลด์ใน where
						$sendArr['idRef']['mark'] = array('BRANDS_ID'=>'=','BRANDS_STATUS'=>'!='); //เครื่องหมายใน where

						$ch = $this->chk_arrayBrand($arr);
						unset($ch['idRef_vihicle']);
						unset($ch['opt']);
						if(@array_key_exists('BRANDS_ADD_DATE',$arr)){ $ch['BRANDS_ADD_DATE'] = date('Y-m-d H:i:s',abs($ch['BRANDS_ADD_DATE'])); } //แปลงวันที่

						if($sendArr['field'] && $sendArr['mark']){
							$chk = $this->chk_Table($sendArr); //เช็คข้อมูล
						}

						if($chk['statusTb']=='update'){
							$this->updateDB($key,$sendArr,$chk,$ch); //แก้ไขข้อมูล
						}else{
							$this->insertDB($key,$sendArr,$chk,$ch); //เพิ่มข้อมูล
						}
					break;
					case 'service':
						if($BU!=$key || $arr['opt']!='delete'){
							$ch = null;

							$sendArr['BU'] = $key;
							$sendArr['db'] = 'abrand'; //ชื่อตาราง
							$sendArr['vReturn'] = array('id'=>'id'); //ค่าที่จะคืนกลับมา

							$sendArr['field'] = array('LOWER(name)'=>strtolower($arr['BRANDS_NAME']),'status'=>'99'); //ฟิลด์ใน where
							$sendArr['mark'] = array('LOWER(name)'=>'=','status'=>'!='); //เครื่องหมายใน where
							$sendArr['idRef']['field'] = array('chk'=>'idRef_vihicle','idRef_vihicle'=>$arr['idRef_vihicle'],'status'=>'99'); //ฟิลด์ใน where
							$sendArr['idRef']['mark'] = array('idRef_vihicle'=>'=','status'=>'!='); //เครื่องหมายใน where

							if($BU!=$key){
								if(@array_key_exists('BRANDS_NAME',$arr)){ $ch['name'] = $arr['BRANDS_NAME']; } //ชื่อ
								if(@array_key_exists('BRANDS_ADD_PERSON',$arr)){ $ch['emp_id'] = $arr['BRANDS_ADD_PERSON']; } //พนักงานที่เพิ่ม
								if(@array_key_exists('BRANDS_ADD_DATE',$arr)){ $ch['add_time'] = $arr['BRANDS_ADD_DATE']; } //วันที่เพิ่ม
								if(@array_key_exists('BRANDS_STATUS',$arr)){ $ch['status'] = $arr['BRANDS_STATUS']; } //สถานะ
								$ch['idRef_vihicle'] = $this->idRef;
							}else{
								$ch['idRef_vihicle'] = $this->idRef;
							}

							if($sendArr['field'] && $sendArr['mark']){
								$chk = $this->chk_Table($sendArr); //เช็คข้อมูล
							}

							if($chk['statusTb']=='update'){
								$this->updateDB($key,$sendArr,$chk,$ch); //แก้ไขข้อมูล
							}else{
								$this->insertDB($key,$sendArr,$chk,$ch); //เพิ่มข้อมูล
							}
						}
					break;
					case 'other':
						$this->updateOther($arr); //เพิ่ม id run ฐานข้อมูลกลาง
					break;
				}
			}
		}

		return $this->idRef;
		@mysql_close( $this->conn );
	}

	//เช็ค array (บริษัท)
	public function chk_arrayCompany($arr){
		$chk = array(
			/*'chk_dbName' => 'new_icmba_crm', //ชื่อฐานข้อมูล
			'chk_tbName' => 'regis_car', //ชื่อตารางฐานข้อมูล
			'chk_where' => 'no=\''.$fe['no'].'\'', //เงื่อนไขที่จะมาแก้ไข
			'chk_field' => 'idRef_vihicle', //ฟิลด์ที่จะแก้ไข*/
			'SELLER_COMP_NAME' => $arr['SELLER_COMP_NAME'], //ชื่อบริษัท ** จำเป็นต้องใช้ในการเช็ค
			'SELLER_COMP_BRANCH' => $arr['SELLER_COMP_BRANCH'], //สาขา
			'SELLER_COMP_ADD' => $arr['SELLER_COMP_ADD'], //ที่อยู่บริษัท
			'SELLER_COMP_MARK' => $arr['SELLER_COMP_MARK'], //หมายเหตุ
			'SELLER_COMP_STATUS' => $arr['SELLER_COMP_STATUS'], //สถานะ
			'SELLER_ADD_DATE' => $arr['SELLER_ADD_DATE'], //วันที่เพิ่มข้อมูล  Ex. timestamp
			'SELLER_ADD_PERSON' => $arr['SELLER_ADD_PERSON'], //พนักงานที่เพิ่มข้อมูล
			'idRef_vihicle' => $arr['idRef_vihicle'], //id ที่ใช้เชื่อมต่อกับ Mainvihicle
			'opt' => $arr['opt'], //insert, update, delete 
		);

		$data_arr = $this->chkArr($arr,$chk);

		return $data_arr;
	}

	//บันทึกข้อมูลบริษัท
	public function manageCompany($arr){
		$this->setConnect();
		$BU = $this->BU;
		
		$arrLoop = $this->config;
		unset($arrLoop['cusNew']);
		if($arr['chk_dbName'] && $arr['chk_tbName'] && $arr['chk_field']){
			$arrLoop['other']['db'] = $arr['chk_dbName'];
		}

		foreach($arrLoop AS $key => $value){
			if($this->disDB[$key]=='ON' || $key=='other'){
				switch($key){
					case 'vehicle':
						$ch = null;

						$sendArr['BU'] = $key;
						$sendArr['db'] = 'VEHICLE_COMPANY'; //ชื่อตาราง
						$sendArr['vReturn'] = array('id'=>'SELLER_COMP_ID'); //ค่าที่จะคืนกลับมา
						$sendArr['field'] = array('LOWER(SELLER_COMP_NAME)'=>strtolower($arr['SELLER_COMP_NAME']),'SELLER_COMP_STATUS'=>'99'); //ฟิลด์ใน where
						$sendArr['mark'] = array('LOWER(SELLER_COMP_NAME)'=>'=','SELLER_COMP_STATUS'=>'!='); //เครื่องหมายใน where
						$sendArr['idRef']['field'] = array('chk'=>'SELLER_COMP_ID','SELLER_COMP_ID'=>$arr['idRef_vihicle'],'SELLER_COMP_STATUS'=>'99'); //ฟิลด์ใน where
						$sendArr['idRef']['mark'] = array('SELLER_COMP_ID'=>'=','SELLER_COMP_STATUS'=>'!='); //เครื่องหมายใน where

						$ch = $this->chk_arrayCompany($arr);
						unset($ch['idRef_vihicle']);
						unset($ch['opt']);
						if(@array_key_exists('SELLER_ADD_DATE',$ch)){ $ch['SELLER_ADD_DATE'] = date('Y-m-d H:i:s',abs($ch['SELLER_ADD_DATE'])); } //แปลงวันที่

						if($arr['SELLER_COMP_NAME']){
							if($sendArr['field'] && $sendArr['mark']){
								$chk = $this->chk_Table($sendArr); //เช็คข้อมูล
							}

							if($chk['statusTb']=='update'){
								$this->updateDB($key,$sendArr,$chk,$ch); //แก้ไขข้อมูล
							}else{
								$this->insertDB($key,$sendArr,$chk,$ch); //เพิ่มข้อมูล
							}
						}
					break;
					//มีเฉพาะ service
					case 'service':
						if($BU!=$key || $arr['opt']!='delete'){
							$ch = null;

							$sendArr['BU'] = $key;
							$sendArr['db'] = 'asell_company'; //ชื่อตาราง
							$sendArr['vReturn'] = array('id'=>'id'); //ค่าที่จะคืนกลับมา
							$sendArr['field'] = array('LOWER(name)'=>strtolower($arr['SELLER_COMP_NAME']),'status'=>'99'); //ฟิลด์ใน where
							$sendArr['mark'] = array('LOWER(name)'=>'=','status'=>'!='); //เครื่องหมายใน where
							$sendArr['idRef']['field'] = array('chk'=>'idRef_vihicle','idRef_vihicle'=>$arr['idRef_vihicle'],'status'=>'99'); //ฟิลด์ใน where
							$sendArr['idRef']['mark'] = array('idRef_vihicle'=>'=','status'=>'!='); //เครื่องหมายใน where

							$ch['idRef_vihicle'] = $this->idRef;

							if($arr['SELLER_COMP_NAME']){
								if($sendArr['field'] && $sendArr['mark']){
									$chk = $this->chk_Table($sendArr); //เช็คข้อมูล
								}

								if($chk['statusTb']=='update'){
									$this->updateDB($key,$sendArr,$chk,$ch); //แก้ไขข้อมูล
								}else{
									//$this->insertDB($key,$sendArr,$chk,$ch); //เพิ่มข้อมูล
								}
							}
						}
					break;
					case 'other':
						$this->updateOther($arr); //เพิ่ม id run ฐานข้อมูลกลาง
					break;
				}
			}
		}

		return $this->idRef;
		@mysql_close( $this->conn );
	}

	//เช็ค array (รถ)
	public function chk_arrayVehicle($arr){
		$chk = array(
			/*'chk_dbName' => 'new_icmba_crm', //ชื่อฐานข้อมูล
			'chk_tbName' => 'regis_car', //ชื่อตารางฐานข้อมูล
			'chk_where' => 'no=\''.$fe['no'].'\'', //เงื่อนไขที่จะมาแก้ไข
			'chk_field' => 'idRef_vihicle', //ฟิลด์ที่จะแก้ไข*/
			'VEHICLE_BRAND_ID' => $arr['VEHICLE_BRAND_ID'], //refer tb BRAND_ID ของตารางหลัก
			'VEHICLE_BRAND_NAME' => $arr['VEHICLE_BRAND_NAME'], //ยี่ห้อรถยนต์
			'VEHICLE_SERIES' => $arr['VEHICLE_SERIES'], //รุ่นรถยนต์
			'VEHICAL_CODE_COLOR' => $arr['VEHICAL_CODE_COLOR'], //รหัสสีรถ
			'VEHICLE_COLOR' => $arr['VEHICLE_COLOR'], //สีรถยนต์
			'VEHICLE_ENGINES' => $arr['VEHICLE_ENGINES'], //หมายเลขเครื่องยนต์
			'VEHICLE_FULL_ENGINES' => $arr['VEHICLE_FULL_ENGINES'], //หมายเลขเครื่องยนต์
			'VEHICLE_CHASSIS' => $arr['VEHICLE_CHASSIS'], //หมายเลขคัสซีแบบย่อ เอาเฉพาะ 8 ตัวหลัง
			'VEHICLE_FULL_CHASSIS' => $arr['VEHICLE_FULL_CHASSIS'], //หมายเลขคัสซีแบบเต็ม
			'VEHICLE_REGIS' => $arr['VEHICLE_REGIS'], //หมายเลขเลขทะเบียน
			'VEHICLE_MODEL_ID' => $arr['VEHICLE_MODEL_ID'], //refer รหัสแบบรถ
			'VEHICLE_MODEL_NAME' => $arr['VEHICLE_MODEL_NAME'], //ชื่อรหัสแบบรถ
			'VEHICLE_MODEL_SVC' => $arr['VEHICLE_MODEL_SVC'], //ModelSVCGroup for tripech
			'VEHICLE_MODEL_CD' => $arr['VEHICLE_MODEL_CD'], //Model CD
			'VEHICLE_MODEL_FULL' => $arr['VEHICLE_MODEL_FULL'], //Model แบบเต็ม
			'VEHICLE_NUM_PERSON' => $arr['VEHICLE_NUM_PERSON'], //จำนวนคนนั่ง
			'VEHICLE_OIL_TYPE' => $arr['VEHICLE_OIL_TYPE'], //ประเภทน้ามัน
			'VEHICLE_WHEELS_NUM' => $arr['VEHICLE_WHEELS_NUM'], //จำนวนล้อ
			'VEHICLE_WEIGHTS' => $arr['VEHICLE_WEIGHTS'], //น้ำหนักรวม
			'VEHICLE_WLPWS' => $arr['VEHICLE_WLPWS'], //น้ำหนักบรรทุก/น้ำหนักลงเพลา
			'VEHICLE_PISTON' => $arr['VEHICLE_PISTON'], //จำนวนลูกสูบ
			'VEHICLE_CC_NUM' => $arr['VEHICLE_CC_NUM'], //จำนวนซีซี
			'VEHICLE_POWERS' => $arr['VEHICLE_POWERS'], //จำนวนแรงม้า
			'VEHICLE_PICS' => $arr['VEHICLE_PICS'], //รูปรถยนต์
			'VEHICLE_STATUS' => $arr['VEHICLE_STATUS'], //สถานะรถยนต์
			'VEHICLE_MARKS' => $arr['VEHICLE_MARKS'], //หมายเหตุรถยนต
			'VEHICLE_ADD_DATE' => $arr['VEHICLE_ADD_DATE'], //วันที่เพิ่มข้อมูล Ex. timestamp
			'VEHICLE_ADD_PERSON' => $arr['VEHICLE_ADD_PERSON'], //พนักงานที่เพิ่มข้อมูล
			'idRef_vihicle' => $arr['idRef_vihicle'], //id ที่ใช้เชื่อมต่อกับ Mainvihicle
			
			//ส่วนที่มีใน ahistory โปรแกรม service
			'Date_Deliver' => $arr['Date_Deliver'],
			'opt' => $arr['opt'], //insert, update, delete 
		);

		$data_arr = $this->chkArr($arr,$chk);

		return $data_arr;
	}

	//บันทึกข้อมูลรถ
	public function manageVehicle($arr){
		$this->setConnect();
		$BU = $this->BU;
		
		$arrLoop = $this->config;
		unset($arrLoop['cusNew']);
		if($arr['chk_dbName'] && $arr['chk_tbName'] && $arr['chk_field']){
			$arrLoop['other']['db'] = $arr['chk_dbName'];
		}

		foreach($arrLoop AS $key => $value){
			if($this->disDB[$key]=='ON' || $key=='other'){
				switch($key){
					case 'vehicle':
						if($arr['VEHICLE_BRAND_ID']>0 && $this->substr_chassis($arr['VEHICLE_CHASSIS'])!="" && $this->substr_engine($arr['VEHICLE_ENGINES'])!=""){
							$ch = null;
							
							$sendArr['BU'] = $key;
							$sendArr['db'] = 'VEHICLE_INFO'; //ชื่อตาราง
							$sendArr['vReturn'] = array('id'=>'VEHICLE_ID','full_chassis'=>'VEHICLE_FULL_CHASSIS','full_engine'=>'VEHICLE_FULL_ENGINES'); //ค่าที่จะคืนกลับมา
							$sendArr['field'] = array(
							'VEHICLE_BRAND_ID'=>$arr['VEHICLE_BRAND_ID'],
							'IF(LENGTH(TRIM(VEHICLE_ENGINES))>6,RIGHT(TRIM(VEHICLE_ENGINES),6),TRIM(VEHICLE_ENGINES))'=>$this->substr_engine($arr['VEHICLE_ENGINES']),
							'IF(LENGTH(TRIM(VEHICLE_CHASSIS))>8,RIGHT(TRIM(VEHICLE_CHASSIS),8),TRIM(VEHICLE_CHASSIS))'=>$this->substr_chassis($arr['VEHICLE_CHASSIS']),
							'VEHICLE_STATUS'=>'99'
							); //ฟิลด์ใน where
							$sendArr['mark'] = array(
							'VEHICLE_BRAND_ID'=>'=',
							'IF(LENGTH(TRIM(VEHICLE_ENGINES))>6,RIGHT(TRIM(VEHICLE_ENGINES),6),TRIM(VEHICLE_ENGINES))'=>'=',
							'IF(LENGTH(TRIM(VEHICLE_CHASSIS))>8,RIGHT(TRIM(VEHICLE_CHASSIS),8),TRIM(VEHICLE_CHASSIS))'=>'=',
							'VEHICLE_STATUS'=>'!='
							); //เครื่องหมายใน where
							
							$sendArr['idRef']['field'] = array('chk'=>'VEHICLE_ID','VEHICLE_ID'=>$arr['idRef_vihicle'],'VEHICLE_STATUS'=>'99'); //ฟิลด์ใน where
							$sendArr['idRef']['mark'] = array('VEHICLE_ID'=>'=','VEHICLE_STATUS'=>'!='); //เครื่องหมายใน where

							$ch = $this->chk_arrayVehicle($arr);

							unset($ch['idRef_vihicle']);
							unset($ch['opt']);

							if(@array_key_exists('VEHICLE_ADD_DATE',$arr)){
								if(!preg_match("/([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})/",$ch['VEHICLE_ADD_DATE'],$regs)){
									$ch['VEHICLE_ADD_DATE'] = date('Y-m-d H:i:s',abs($ch['VEHICLE_ADD_DATE']));
								}
							} //แปลงวันที่

							if($sendArr['field'] && $sendArr['mark']){
								$chk = $this->chk_Table($sendArr); //เช็คข้อมูล
							}
							
							## ถ้าเป็นของขาย ถึงจะบันทึกลง ฟิลด์นี้
							if($BU=='sale'){
								## ถ้าเลขเลขเครื่องยาวกว่า 6 ให้บันทึกลงเลขเครื่องเต็ม
								if($len = $this->textLength($arr['VEHICLE_FULL_ENGINES'])>6){ $ch['VEHICLE_FULL_ENGINES']	= $arr['VEHICLE_FULL_ENGINES']; }
								## ถ้าเลขแซสซียาวกว่า 8 ให้บันทึกลงเลขแซสซีเต็ม
								if($len = $this->textLength($arr['VEHICLE_FULL_CHASSIS'])>8){ $ch['VEHICLE_FULL_CHASSIS']	= $arr['VEHICLE_FULL_CHASSIS']; }
							}
							
							$ch['VEHICLE_ENGINES']			= $this->substr_engine($ch['VEHICLE_ENGINES']);
							$ch['VEHICLE_CHASSIS']			= $this->substr_chassis($ch['VEHICLE_CHASSIS']);
							
							## ถ้ามีทะเบียน ถึงจะแก้ไข หรือเพิ่มทะเบียน
							if(str_replace(' ','',$arr['VEHICLE_REGIS'])){
								$ch['VEHICLE_REGIS']		= str_replace(' ','',$ch['VEHICLE_REGIS']);
							}

							if($chk['statusTb']=='update'){
								## ต้องเป็น service เท่านั้นถึงจะแก้ไข
								if($BU=='service'){
									$this->updateDB($key,$sendArr,$chk,$ch); //แก้ไขข้อมูล
								}else{
									// $this->idRef	= $chk['VEHICLE_ID'];
									$chSale	= array();
									$chSale['VEHICLE_FULL_ENGINES']	= $arr['VEHICLE_FULL_ENGINES'];
									$chSale['VEHICLE_FULL_CHASSIS']	= $arr['VEHICLE_FULL_CHASSIS'];
								
									$this->updateDB($key,$sendArr,$chk,$chSale); //แก้ไขข้อมูล
								}
							}else{
								$this->insertDB($key,$sendArr,$chk,$ch); //เพิ่มข้อมูล
							}
						}
					break;
					case 'service':
						if($BU!=$key || $arr['opt']!='delete'){
							if($arr['VEHICLE_BRAND_ID']>0 && $this->substr_chassis($arr['VEHICLE_CHASSIS'])!="" && $this->substr_engine($arr['VEHICLE_ENGINES'])!=""){
								$ch = null;

								if(@array_key_exists('VEHICLE_BRAND_ID',$arr) && $arr['VEHICLE_BRAND_ID']){
									$brand2 = "SELECT id,name FROM ".$this->config[$key]['db'].".abrand WHERE idRef_vihicle='".$arr['VEHICLE_BRAND_ID']."' LIMIT 1";
									$qBrand2 = mysql_query($brand2);
									$fBrand2 = @mysql_fetch_assoc($qBrand2);

									if(!$qBrand2){ $this->gQueryLogError($brand2,__FILE__.' Line : '.__LINE__); }

									$arr['abrand_id'] = $fBrand2['id']; //ไอดียี่ห้อ
								}
								
								$sendArr['BU'] = $key;
								$sendArr['db'] = 'ahistory'; //ชื่อตาราง
								$sendArr['vReturn'] = array('id'=>'id'); //ค่าที่จะคืนกลับมา
								$sendArr['field'] = array(
								'abrand_id'=>$arr['abrand_id'],
								'IF(LENGTH(TRIM(chassis))>8,RIGHT(TRIM(chassis),8),TRIM(chassis))'=>$this->substr_chassis($arr['VEHICLE_CHASSIS']),
								'IF(LENGTH(TRIM(engin_id))>6,RIGHT(TRIM(engin_id),6),TRIM(engin_id))'=>$this->substr_engine($arr['VEHICLE_ENGINES']),
								'status'=>'99'
								); //ฟิลด์ใน where
								$sendArr['mark'] = array(
								'abrand_id'=>'=',
								'IF(LENGTH(TRIM(chassis))>8,RIGHT(TRIM(chassis),8),TRIM(chassis))'=>'=',
								'IF(LENGTH(TRIM(engin_id))>6,RIGHT(TRIM(engin_id),6),TRIM(engin_id))'=>'=',
								'status'=>'!='
								); //เครื่องหมายใน where
								$sendArr['idRef']['field'] = array('chk'=>'idRef_vihicle','idRef_vihicle'=>$arr['idRef_vihicle'],'status'=>'99'); //ฟิลด์ใน where
								$sendArr['idRef']['mark'] = array('idRef_vihicle'=>'=','status'=>'!='); //เครื่องหมายใน where
								
								//สีรถ ถ้าส่งมาจากขาย
								if($BU=='sale'){
									if(@array_key_exists('VEHICAL_CODE_COLOR',$arr)){
										$color = "SELECT id FROM ".$this->config[$key]['db'].".colorcar WHERE color_name='".$arr['VEHICAL_CODE_COLOR']."' LIMIT 1";
										$qColor = mysql_query($color);
										$fColor = @mysql_fetch_assoc($qColor);
										
										$ch['color_id'] = $fColor['id'];
									}else{
										$ch['color_id'] = 'A';
									}
								}

								if($BU!=$key){
									## ถ้าเป็นของขาย ถึงจะบันทึกลง ฟิลด์นี้
									if($BU=='sale'){
										## ถ้าเลขเลขเครื่องยาวกว่า 6 ให้บันทึกลงเลขเครื่องเต็ม
										if($len = $this->textLength($arr['VEHICLE_FULL_ENGINES'])>6){ $ch['full_engine'] = $arr['VEHICLE_FULL_ENGINES']; }
										## ถ้าเลขแซสซียาวกว่า 8 ให้บันทึกลงเลขแซสซีเต็ม
										if($len = $this->textLength($arr['VEHICLE_FULL_CHASSIS'])>8){ $ch['full_chassis'] = $arr['VEHICLE_FULL_CHASSIS']; }
									}
									
									if(@array_key_exists('VEHICLE_CHASSIS',$arr)){
									 	$ch['chassis'] = $this->substr_chassis($arr['VEHICLE_CHASSIS']); 
									}
									if(@array_key_exists('VEHICLE_ENGINES',$arr)){ 
										$ch['engin_id'] = $this->substr_engine($arr['VEHICLE_ENGINES']); 
									}
									if(@array_key_exists('VEHICLE_REGIS',$arr)){
									 	$ch['register'] = str_replace(' ','',$arr['VEHICLE_REGIS']);
									 }
									//if(@array_key_exists('REGIS_NUMBER',$arr)){ $ch['register_province'] = 'ชร'; }
									if(@array_key_exists('Date_Deliver',$arr)){ $ch['deliver_day'] = $arr['Date_Deliver']; }
									if(@array_key_exists('VEHICLE_ADD_PERSON',$arr)){ $ch['emp_id'] = $arr['VEHICLE_ADD_PERSON']; }
									if(@array_key_exists('VEHICLE_ADD_DATE',$arr)){ $ch['add_time'] = $arr['VEHICLE_ADD_DATE']; }
									if(@array_key_exists('BRANDS_STATUS',$arr)){ $ch['status'] = $arr['BRANDS_STATUS']; }
									if(@array_key_exists('abrand_id',$arr)){ $ch['abrand_id'] = $arr['abrand_id']; }
									// if(@array_key_exists('VEHICLE_MODEL_ID',$arr)){ $ch['amodel_id'] = $arr['VEHICLE_MODEL_ID']; }
									if(@array_key_exists('VEHICLE_MODEL_NAME',$arr)){ $ch['model_name'] = $arr['VEHICLE_MODEL_NAME']; }
									if(@array_key_exists('VEHICLE_MODEL_CD',$arr)){ $ch['MDL_CD'] = $arr['VEHICLE_MODEL_CD']; }
									if(@array_key_exists('VEHICLE_MODEL_FULL',$arr)){ $ch['fullmodel_name'] = $arr['VEHICLE_MODEL_FULL']; }
									$ch['idRef_vihicle'] = $this->idRef;
								}else{
									$ch['idRef_vihicle'] = $this->idRef;
								}

								if($sendArr['field'] && $sendArr['mark']){
									$chk = $this->chk_Table($sendArr); //เช็คข้อมูล
								}

								## ต้องเป็น service เท่านั้นถึงจะแก้ไข
								if($BU==$key){
									$this->updateDB($key,$sendArr,$chk,$ch); //แก้ไขข้อมูล
								}else{
									if($chk['statusTb']=='update'){
										## ต้องเป็น service เท่านั้นถึงจะแก้ไข
										if($BU=='service'){
											$this->updateDB($key,$sendArr,$chk,$ch); //แก้ไขข้อมูล
										}else{
											$chSale	= array();
											$chSale['full_engine']	= $arr['VEHICLE_FULL_ENGINES'];
											$chSale['full_chassis']	= $arr['VEHICLE_FULL_CHASSIS'];
										
											$this->updateDB($key,$sendArr,$chk,$chSale); //แก้ไขข้อมูล
										}
									}else{
										$this->insertDB($key,$sendArr,$chk,$ch); //เพิ่มข้อมูล
									} 
								}
							}
						}
					break;
					case 'other':
						$this->updateOther($arr); //เพิ่ม id run ฐานข้อมูลกลาง
					break;
				}
			}
		}

		return $this->idRef;
		@mysql_close( $this->conn );
	}

	//เช็ค customer ของ service
	public function chkCustomer_service($cusNo){
		$sql = "SELECT id FROM ".$this->config['service']['db'].".customer WHERE cus_id='".$cusNo."' AND status!='99' LIMIT 1";
		$que = mysql_query($sql);
		$fe = @mysql_fetch_assoc($que);

		if(!$que){ $this->gQueryLogError($sql,__FILE__.' Line : '.__LINE__); }

		if($fe['id']){
			$return = $fe['id'];
		}else{
			if($cusNo){
				$this->logTable_DB($this->config['service']['db'].".customer");

				$insert = "INSERT INTO ".$this->config['service']['db'].".customer(cus_id,money_type,money_typesell,add_time,emp_id)";
				$insert .= "VALUE('".$cusNo."','1','1','".time()."','".$_SESSION['SESSION_ID_card']."')";
				$query = mysql_query($insert);

				$idRun = mysql_insert_id();

				$this->unlogTable_DB();

				$this->gQueryAndLogUpdate(0,'',__FILE__.' Line : '.__LINE__,$this->config['service']['db'].".customer","id='".$idRun."'");

				$return = $idRun;
			}
		}

		return $return;
	}

	//เช็ค array (ความสัมพันธ์รถกับคน)
	public function chk_arrayRelation($arr){
		$chk = array(
			/*'chk_dbName' => 'new_icmba_crm', //ชื่อฐานข้อมูล
			'chk_tbName' => 'regis_car', //ชื่อตารางฐานข้อมูล
			'chk_where' => 'no=\''.$fe['no'].'\'', //เงื่อนไขที่จะมาแก้ไข
			'chk_field' => 'idRef_vihicle', //ฟิลด์ที่จะแก้ไข*/
			'RELATES_VEHICLE_ID' => $arr['RELATES_VEHICLE_ID'], //refer VEHICLE_INFO
			'RELATES_CUS_ID' => $arr['RELATES_CUS_ID'], //ref CusNo
			'RELATES_TYPE' => $arr['RELATES_TYPE'], //รูปแบบ
			'RELATES_COMP_ID' => $arr['RELATES_COMP_ID'], //refer VEHICLE_COMPANY
			'RELATES_MARKS' => $arr['RELATES_MARKS'], //หมายเหตุ
			'RELATES_STATUS' => $arr['RELATES_STATUS'], //สถานะ
			'RELATES_ADD_DATE' => $arr['RELATES_ADD_DATE'], //หมายเลขคัสซี Ex. timestamp
			'RELATES_ADD_PERSON' => $arr['RELATES_ADD_PERSON'], //พนักงานที่เพิ่มข้อมูล
			'idRef_vihicle' => $arr['idRef_vihicle'], //id ที่ใช้เชื่อมต่อกับ Mainvihicle
		);

		$data_arr = $this->chkArr($arr,$chk);

		return $data_arr;
	}

	//บันทึกข้อมูลความสัมพันธ์รถกับคน
	public function manageRelation($arr){
		$this->setConnect();
		$BU = $this->BU;

		$arrLoop = $this->config;
		unset($arrLoop['cusNew']);
		if($arr['chk_dbName'] && $arr['chk_tbName'] && $arr['chk_field']){
			$arrLoop['other']['db'] = $arr['chk_dbName'];
		}

		foreach($arrLoop AS $key => $value){
			if($this->disDB[$key]=='ON' || $key=='other'){
				switch($key){
					case 'vehicle':
						$ch = null;

						$sendArr['BU'] = $key;
						$sendArr['db'] = 'VEHICLE_RELATIONSHIP'; //ชื่อตาราง
						$sendArr['vReturn'] = array('id'=>'RELATES_ID'); //ค่าที่จะคืนกลับมา
						$sendArr['field'] = array('RELATES_VEHICLE_ID'=>$arr['RELATES_VEHICLE_ID'],'RELATES_TYPE'=>$arr['RELATES_TYPE'],'RELATES_STATUS'=>'99'); //ฟิลด์ใน where
						$sendArr['mark'] = array('RELATES_VEHICLE_ID'=>'=','RELATES_TYPE'=>'=','RELATES_STATUS'=>'!='); //เครื่องหมายใน where
						/*$sendArr['field'] = array('RELATES_VEHICLE_ID'=>$arr['RELATES_VEHICLE_ID'],'RELATES_CUS_ID'=>$arr['RELATES_CUS_ID'],'RELATES_STATUS'=>'99'); //ฟิลด์ใน where
						$sendArr['mark'] = array('RELATES_VEHICLE_ID'=>'=','RELATES_CUS_ID'=>'=','RELATES_STATUS'=>'!='); //เครื่องหมายใน where*/
						/*$sendArr['idRef']['field'] = array('chk'=>'RELATES_VEHICLE_ID','RELATES_VEHICLE_ID'=>$arr['idRef_vihicle'],'RELATES_STATUS'=>'99'); //ฟิลด์ใน where
						 $sendArr['idRef']['mark'] = array('RELATES_VEHICLE_ID'=>'=','RELATES_STATUS'=>'!='); //เครื่องหมายใน where */

						$ch = $this->chk_arrayRelation($arr);

						unset($ch['idRef_vihicle']);
						if(@array_key_exists('RELATES_ADD_DATE',$arr)){
							if(!preg_match("/([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})/",$ch['RELATES_ADD_DATE'],$regs)){
								$ch['RELATES_ADD_DATE'] = date('Y-m-d H:i:s',abs($ch['RELATES_ADD_DATE']));
							}
						} //แปลงวันที่

						if(($sendArr['field'] && $sendArr['mark']) || ($sendArr['idRef']['field'] && $sendArr['idRef']['mark'])){
							$chk = $this->chk_Table($sendArr); //เช็คข้อมูล
						}

						if($chk['statusTb']=='update'){
							$this->updateDB($key,$sendArr,$chk,$ch); //แก้ไขข้อมูล
						}else{
							$this->insertDB($key,$sendArr,$chk,$ch); //เพิ่มข้อมูล
						}
					break;
					case 'service':
						$ch = $cus_id = null;

						if(@array_key_exists('RELATES_CUS_ID',$arr)){
							$cus_id = $this->chkCustomer_service($arr['RELATES_CUS_ID']);
						}

						$sendArr['BU'] = $key;
						$sendArr['db'] = 'ahistory'; //ชื่อตาราง
						$sendArr['vReturn'] = array('id'=>'id'); //ค่าที่จะคืนกลับมา
						$sendArr['field'] = array('idRef_vihicle'=>$arr['RELATES_VEHICLE_ID'],'status'=>'99'); //ฟิลด์ใน where
						$sendArr['mark'] = array('idRef_vihicle'=>'=','status'=>'!='); //เครื่องหมายใน where
						/*$sendArr['idRef']['field'] = array('chk'=>'idRef_vihicle','idRef_vihicle'=>$arr['idRef_vihicle'],'status'=>'99'); //ฟิลด์ใน where
						$sendArr['idRef']['mark'] = array('idRef_vihicle'=>'=','status'=>'!='); //เครื่องหมายใน where */

						if($BU!=$key){
							/*switch($BU){
								case 'sale':
								if(@array_key_exists('RELATES_COMP_ID',$arr)){
								$comp = "SELECT id,name FROM ".$this->config[$key]['db'].".asell_company WHERE idRef_vihicle='".$arr['RELATES_COMP_ID']."' LIMIT 1";
								$qComp = mysql_query($comp);
								$fComp = @mysql_fetch_assoc($qComp);

								if(!$qComp){ $this->gQueryLogError($comp,__FILE__.' Line : '.__LINE__); }

								$ch['asell_com_id'] = $fComp['id']; //ไอดียี่ห้อ
								}
								break;
								}*/

							if(@array_key_exists('RELATES_CUS_ID',$arr)){ $ch['user_id'] = $cus_id; $ch['cus_id'] = $cus_id; }
							//if(@array_key_exists('RELATES_STATUS',$arr)){ $ch['status'] = $arr['RELATES_STATUS']; }
							if(@array_key_exists('RELATES_ADD_DATE',$arr)){ $ch['add_time'] = $arr['RELATES_ADD_DATE']; }
							if(@array_key_exists('RELATES_ADD_PERSON',$arr)){ $ch['emp_id'] = $arr['RELATES_ADD_PERSON']; }
							//$ch['idRef_vihicle'] = $arr['RELATES_VEHICLE_ID'];
						}else{
							//$ch['idRef_vihicle'] = $arr['RELATES_VEHICLE_ID'];
						}

						if($sendArr['field'] && $sendArr['mark']){
							$chk = $this->chk_Table($sendArr); //เช็คข้อมูล
						}

						if($chk['statusTb']=='update'){
							// $this->updateDB($key,$sendArr,$chk,$ch); //แก้ไขข้อมูล
						}else{
							// $this->insertDB($key,$sendArr,$chk,$ch); //เพิ่มข้อมูล
						}
					break;
					case 'other':
						$this->updateOther($arr); //เพิ่ม id run ฐานข้อมูลกลาง
					break;
				}
			}
		}

		return $this->idRef;
		@mysql_close( $this->conn );
	}

	//เช็ค array (ข้อมูลการจดทะเบียน)
	public function chk_arrayRegister($arr){

		$chk = array(
			/*'chk_dbName' => 'new_icmba_crm', //ชื่อฐานข้อมูล
			'chk_tbName' => 'regis_car', //ชื่อตารางฐานข้อมูล
			'chk_where' => 'no=\''.$fe['no'].'\'', //เงื่อนไขที่จะมาแก้ไข
			'chk_field' => 'idRef_vihicle', //ฟิลด์ที่จะแก้ไข*/
			'REGIS_DATE' => $arr['REGIS_DATE'], //วันจดทะเบียน Ex.2012-04-09
			'REGIS_CUS_ID' => $arr['REGIS_CUS_ID'], //ref cusno
			'REGIS_VEHICLE_ID' => $arr['REGIS_VEHICLE_ID'], //id ref VEHICLE_INFO
			'REGIS_NUMBER' => $arr['REGIS_NUMBER'], //เลขทะเบียน
			'REGIS_PROVINCE' => $arr['REGIS_PROVINCE'], //จังหวัด
			'REGIS_CAR_TYPE' => $arr['REGIS_CAR_TYPE'], //ประเภท
			'REGIS_CAR_LOOK' => $arr['REGIS_CAR_LOOK'], //ประเภท รย. ส่งมาเฉพาะตัวเลข Ex.1
			'REGIS_BRAND_ID' => $arr['REGIS_BRAND_ID'], //ยี่ห้อรถ
			'REGIS_BRAND' => $arr['REGIS_BRAND'], //ยี่ห้อรถ
			'REGIS_PATTERN' => $arr['REGIS_PATTERN'], //แบบรถ
			'REGIS_RELEASE' => $arr['REGIS_RELEASE'], //รุ่นปี ค.ศ.
			'REGIS_COLOR' => $arr['REGIS_COLOR'], //สี
			'REGIS_CHESSY' => $arr['REGIS_CHESSY'], //เลขตัวรถ
			'REGIS_MACHINE_BRAND' => $arr['REGIS_MACHINE_BRAND'], //ยี่ห้อเครื่องยนต์
			'REGIS_NUM_MACHINE' => $arr['REGIS_NUM_MACHINE'], //เลขเครื่องยนต์
			'REGIS_NUM_PISTON' => $arr['REGIS_NUM_PISTON'], //จำนวนลูกสูบ
			'REGIS_NUM_CC' => $arr['REGIS_NUM_CC'], //จำนวนซีซี
			'REGIS_NUM_POWER' => $arr['REGIS_NUM_POWER'], //จำนวนแรงม้า
			'REGIS_WEIGHT' => $arr['REGIS_WEIGHT'], //น้ำหนักรถ
			'REGIS_WLPWS' => $arr['REGIS_WLPWS'], //น้ำหนักบรรทุก/น้ำหนักลงเพลา
			'REGIS_ALLWEIGHT' => $arr['REGIS_ALLWEIGHT'], //น้ำหนักรวม
			'REGIS_FINANCE_ID' => $arr['REGIS_FINANCE_ID'], //refer
			'REGIS_STATUS' => $arr['REGIS_STATUS'], //สถานะ
			'REGIS_COMPETITOR_ID' => $arr['REGIS_COMPETITOR_ID'], //refer
			'TBL_SELL_ID_REF' => $arr['TBL_SELL_ID_REF'], //refer table SELL
			'REGIS_WHEEL' => $arr['REGIS_WHEEL'], //จำนวนล้อ
			'REGIS_MARK' => $arr['REGIS_MARK'], //หมายเหตุ
			'REGIS_ADD_DATE' => $arr['REGIS_ADD_DATE'], //วันที่ที่เพิ่มข้อมูล Ex. timestamp
			'REGIS_ADD_PERSON' => $arr['REGIS_ADD_PERSON'], //พนักงานที่เพิ่มข้อมูล
			'idRef_vihicle' => $arr['idRef_vihicle'], //id ที่ใช้เชื่อมต่อกับ Mainvihicle
		);

		$data_arr = $this->chkArr($arr,$chk);

		return $data_arr;
	}

	//บันทึกข้อมูลการจดทะเบียน
	public function manageRegister($arr){
		$this->setConnect();
		$BU = $this->BU;
		
		$arrLoop = $this->config;
		unset($arrLoop['cusNew']);
		if($arr['chk_dbName'] && $arr['chk_tbName'] && $arr['chk_field']){
			$arrLoop['other']['db'] = $arr['chk_dbName'];
		}

		foreach($arrLoop AS $key => $value){
			if($this->disDB[$key]=='ON' || $key=='other'){
				switch($key){
					case 'vehicle':
						$ch = null;

						$sendArr['BU'] = $key;
						$sendArr['db'] = 'VEHICLE_REGIS'; //ชื่อตาราง
						$sendArr['vReturn'] = array('id'=>'REGIS_ID'); //ค่าที่จะคืนกลับมา
						$sendArr['field'] = array(
						'REGIS_CUS_ID'=>$arr['REGIS_CUS_ID'],
						'REGIS_CHESSY'=>$arr['REGIS_CHESSY'],
						'IF(LENGTH(TRIM(REGIS_NUM_MACHINE))>6,RIGHT(TRIM(REGIS_NUM_MACHINE),6),TRIM(REGIS_NUM_MACHINE))'=>$this->substr_engine($arr['REGIS_NUM_MACHINE']),
						'REGIS_STATUS '=>'99'
						); //ฟิลด์ใน where
						$sendArr['mark'] = array(
						'REGIS_CUS_ID'=>'=',
						'REGIS_CHESSY'=>'=',
						'IF(LENGTH(TRIM(REGIS_NUM_MACHINE))>6,RIGHT(TRIM(REGIS_NUM_MACHINE),6),TRIM(REGIS_NUM_MACHINE))'=>'=',
						'REGIS_STATUS'=>'!='
						); //เครื่องหมายใน where
						$sendArr['idRef']['field'] = array('chk'=>'REGIS_ID','REGIS_ID'=>$arr['idRef_vihicle'],'REGIS_STATUS '=>'99'); //ฟิลด์ใน where
						$sendArr['idRef']['mark'] = array('REGIS_ID'=>'=','REGIS_STATUS'=>'!='); //เครื่องหมายใน where

						$ch = $this->chk_arrayRegister($arr);
						$ch['REGIS_FULL_MACHINE'] = $ch['REGIS_NUM_MACHINE'];
						$ch['REGIS_NUM_MACHINE'] = $this->substr_engine($ch['REGIS_NUM_MACHINE']);
						if(@array_key_exists('REGIS_ADD_DATE',$arr)){
							if(!preg_match("/([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})/",$ch['REGIS_ADD_DATE'],$regs)){
								$ch['REGIS_ADD_DATE'] = date('Y-m-d H:i:s',abs($ch['REGIS_ADD_DATE']));
							}
						}

						unset($ch['idRef_vihicle']);

						if($sendArr['field'] && $sendArr['mark']){
							$chk = $this->chk_Table($sendArr); //เช็คข้อมูล
						}

						if($chk['statusTb']=='update'){
							$this->updateDB($key,$sendArr,$chk,$ch); //แก้ไขข้อมูล
						}else{
							$this->insertDB($key,$sendArr,$chk,$ch); //เพิ่มข้อมูล
						}
					break;
					case 'service':
						if($BU!=$key || $arr['opt']!='delete'){
							if($arr['REGIS_BRAND_ID']>0 && $arr['REGIS_NUMBER']!="" && $this->substr_chassis($arr['REGIS_CHESSY'])!="" && $this->substr_engine($arr['REGIS_NUM_MACHINE'])!=""){
								$ch = $cus_id = null;

								// if(@array_key_exists('REGIS_CUS_ID',$arr)){
									// $cus_id = $this->chkCustomer_service($arr['REGIS_CUS_ID']);
								// }
								// 'user_id'=>$cus_id,
								// 'user_id'=>'=',

								$sendArr['BU'] = $key;
								$sendArr['db'] = 'ahistory'; //ชื่อตาราง
								$sendArr['vReturn'] = array('id'=>'id'); //ค่าที่จะคืนกลับมา
								$sendArr['field'] = array(
								'IF(LENGTH(TRIM(chassis))>8,RIGHT(TRIM(chassis),8),TRIM(chassis))'=>$this->substr_chassis($arr['REGIS_CHESSY']),
								'IF(LENGTH(TRIM(engin_id))>6,RIGHT(TRIM(engin_id),6),TRIM(engin_id))'=>$this->substr_engine($arr['REGIS_NUM_MACHINE']),
								'status'=>'99'
								); //ฟิลด์ใน where
								$sendArr['mark'] = array(
								'IF(LENGTH(TRIM(chassis))>8,RIGHT(TRIM(chassis),8),TRIM(chassis))'=>'=',
								'IF(LENGTH(TRIM(engin_id))>6,RIGHT(TRIM(engin_id),6),TRIM(engin_id))'=>'=',
								'status'=>'!='); //เครื่องหมายใน where
								$sendArr['idRef'] = null;
								/*$sendArr['idRef']['field'] = array('chk'=>'idRef_vihicle','idRef_vihicle'=>$arr['idRef_vihicle'],'status'=>'99'); //ฟิลด์ใน where
								 $sendArr['idRef']['mark'] = array('idRef_vihicle'=>'=','status'=>'!='); //เครื่องหมายใน where */

								if($BU!=$key){
									switch($BU){
										case 'sale':
											if(@array_key_exists('REGIS_BRAND_ID',$arr)){
												$brand2 = "SELECT id,name FROM ".$this->config[$key]['db'].".abrand WHERE idRef_vihicle='".$arr['REGIS_BRAND_ID']."' LIMIT 1";
												$qBrand2 = mysql_query($brand2);
												$fBrand2 = @mysql_fetch_assoc($qBrand2);

												if(!$qBrand2){ $this->gQueryLogError($brand2,__FILE__.' Line : '.__LINE__); }

												$ch['abrand_id'] = $fBrand2['id']; //ไอดียี่ห้อ
											}

											/*if(@array_key_exists('REGIS_PROVINCE',$arr)){
												$province = "SELECT ProvinceCode FROM ".$this->config[$key]['db'].".tis_Province WHERE ProvinceName='".$arr['REGIS_PROVINCE']."' LIMIT 1";
												$qProvince = mysql_query($province);
												$fProvince = mysql_fetch_assoc($qProvince);

												if(!$qProvince){ $this->gQueryLogError($province,__FILE__.' Line : '.__LINE__); }

												$ch['register_province'] = $fProvince['ProvinceCode'];
												}*/
											break;
									}

									//if(@array_key_exists('REGIS_CUS_ID',$arr)){ $ch['user_id'] = $cus_id; $ch['cus_id'] = $cus_id; }
									if(@array_key_exists('REGIS_NUMBER',$arr)){ $ch['register'] = $arr['REGIS_NUMBER']; }
									//if(@array_key_exists('REGIS_PROVINCE',$arr)){ $ch['register_province'] = $arr['REGIS_PROVINCE']; }
									if(@array_key_exists('REGIS_CAR_LOOK',$arr)){ $ch['register_status'] = $arr['REGIS_CAR_LOOK']; }
									
									## ถ้าเลขเลขเครื่องยาวกว่า 6 ให้บันทึกลงเลขเครื่องเต็ม
									if($len = $this->textLength($arr['REGIS_NUM_MACHINE'])>6){ $ch['full_engine'] = $arr['REGIS_NUM_MACHINE']; }
									## ถ้าเลขแซสซียาวกว่า 8 ให้บันทึกลงเลขแซสซีเต็ม
									if($len = $this->textLength($arr['REGIS_CHESSY'])>8){ $ch['full_chassis'] = $arr['REGIS_CHESSY']; }
									
									if(@array_key_exists('REGIS_CHESSY',$arr)){ $ch['chassis'] = $this->substr_chassis($arr['REGIS_CHESSY']); }
									if(@array_key_exists('REGIS_NUM_MACHINE',$arr)){ $ch['engin_id'] = $this->substr_engine($arr['REGIS_NUM_MACHINE']); }
									if(@array_key_exists('REGIS_STATUS',$arr)){ $ch['status'] = $arr['REGIS_STATUS']; }
									if(@array_key_exists('REGIS_ADD_DATE',$arr)){ $ch['add_time'] = $arr['REGIS_ADD_DATE']; }
									if(@array_key_exists('REGIS_ADD_PERSON',$arr)){ $ch['emp_id'] = $arr['REGIS_ADD_PERSON']; }
									//$ch['idRef_vihicle'] = $this->idRef;
								}else{
									//$ch['idRef_vihicle'] = $this->idRef;
								}

								if($sendArr['field'] && $sendArr['mark']){
									$chk = $this->chk_Table($sendArr); //เช็คข้อมูล
								}

								if($chk['statusTb']=='update'){
									// $this->updateDB($key,$sendArr,$chk,$ch); //แก้ไขข้อมูล
								}else{
									if(@array_key_exists('REGIS_CUS_ID',$arr)){ $ch['user_id'] = $cus_id; $ch['cus_id'] = $cus_id; }
									$this->insertDB($key,$sendArr,$chk,$ch); //เพิ่มข้อมูล
								}
							}
						}
					break;
					case 'other':
						$this->updateOther($arr); //เพิ่ม id run ฐานข้อมูลกลาง
					break;
					/*case 'sale':
						$ch = null;

						$sendArr['BU'] = $key;
						$sendArr['db'] = 'regis_car'; //ชื่อตาราง
						$sendArr['vReturn'] = array('id'=>'no'); //ค่าที่จะคืนกลับมา
						$sendArr['field'] = array('regis_id_cus'=>$arr['REGIS_CUS_ID'],'regis_num_chessy'=>$arr['REGIS_CHESSY'],
											'regis_num_machine'=>$arr['REGIS_NUM_MACHINE']); //ฟิลด์ใน where
						$sendArr['mark'] = array('CusNo'=>'=','regis_num_chessy'=>'=','regis_num_machine'=>'='); //เครื่องหมายใน where
						$sendArr['idRef']['field'] = array('chk'=>'idRef_vihicle','idRef_vihicle'=>$arr['idRef_vihicle']); //ฟิลด์ใน where
						$sendArr['idRef']['mark'] = array('idRef_vihicle'=>'=','status'=>'!='); //เครื่องหมายใน where

						$ch['idRef_vihicle'] = $this->idRef;

						if($sendArr['field'] && $sendArr['mark']){
							$chk = $this->chk_Table($sendArr); //เช็คข้อมูล
						}

						if($chk['statusTb']=='update'){
							$this->updateDB($key,$sendArr,$chk,$ch); //แก้ไขข้อมูล

							if($arr['REGIS_NUMBER'] && abs($arr['TBL_SELL_ID_REF'])){
								$update = "UPDATE ".$this->config[$key]['db'].".Sell SET reg_no='".$arr['REGIS_NUMBER']."' WHERE ";
								$update .= "Sell_No='".abs($arr['TBL_SELL_ID_REF'])."' LIMIT 1";
								$query = mysql_query($update);

								if(!$query){ $this->gQueryLogError($update,__FILE__.' Line : '.__LINE__); }
							}
						}else{
							//$this->insertDB($key,$sendArr,$chk,$ch); //เพิ่มข้อมูล
						}
					break;*/
				}
			}
		}

		return $this->idRef;
		@mysql_close( $this->conn );
	}

	public function setConnect_customer($bu=null){
		if($bu){
			$this->BU = $bu;
		}else if(!$this->BU){
			global $configBu; //global config
			$this->BU = $configBu;
		}

		/* switch($this->dbCh){
			case '1':
				$ServerName	= "localhost";
				$UserName = "root";
				$UserPassword = "123456";

				$cusNew_db = "main_cusdata";
				$cusOld_db = "new_maincusdata";
				$OU_db = "OU";
			break;
			case '2':
				$ServerName	= "localhost";
				$UserName = "db_user";
				$UserPassword = "123456";

				$cusNew_db = "main_cusdata";
				$cusOld_db = "new_maincusdata";
				$OU_db = "main_ou";
			break;
			case '3':
				$ServerName	= "localhost";
				$UserName = "root";
				$UserPassword = "123456";

				$cusNew_db = "main_cusdata";
				$cusOld_db = "new_maincusdata";
				$OU_db = "easyhr_OU";
			break;
			case '4':
				$ServerName	= "localhost";
				$UserName = "root";
				$UserPassword = "123456";

				$cusNew_db = "main_cusdata";
				$cusOld_db = "report_maincusdata";
				$OU_db = "OU";
			break;
		} */
		
		/* $this->con['ServerName']	= "localhost";
		$this->con['UserName']		= "root";
		$this->con['UserPassword'] = "123456";

		$this->con['db_base_name'] = "main_crm";   			 	 	# - crm
		$this->con['db_emp'] 		= "easyhr_icchecktime2";   		 	# -	checktime
		$this->con['db_maincus'] 	= "main_cusdata";         	 		# - new maincus
		$this->con['Cus']			= 'new_maincusdata';				# - old maincus
		$this->con['db_organi'] 	= "easyhr_OU";     					# - ou
		$this->con['db_leasing'] 	= "easyloan_icleasing";     		# - leasing
		$this->con['db_easysale'] 	= "new_icmba";     					# - easysale
		$this->con['db_service'] 	= "icservice";     					# - easysale
		$this->con['db_vehicle'] 	= "main_vehicle";     				# - mainVehicle */
		
		$this->calConfigAllDatabase();
		
		$ServerName		= $this->con['ServerName'];
		$UserName		= $this->con['UserName'];
		$UserPassword	= $this->con['UserPassword'];
		$cusNew_db		= $this->con['db_maincus'];
		$cusOld_db		= $this->con['Cus'];
		$OU_db			= $this->con['db_organi'];
		$emp_db			= $this->con['db_emp'];

		$this->server['ServerName']	= $ServerName;
		$this->server['UserName'] = $UserName;
		$this->server['UserPassword'] = $UserPassword;

		//if($BU!='main'){
		$this->config['cusNew']['db'] = $cusNew_db;
		$this->config['cusNew']['sv'] = $this->server['ServerName'];
		$this->config['cusNew']['user'] = $this->server['UserName'];
		$this->config['cusNew']['pass'] = $this->server['UserPassword'];
		//if($BU!='service'){

		//if($BU!='service'){
		$this->config['cusOld']['db'] = $cusOld_db;
		$this->config['cusOld']['sv'] = $this->server['ServerName'];
		$this->config['cusOld']['user'] = $this->server['UserName'];
		$this->config['cusOld']['pass'] = $this->server['UserPassword'];
		//}

		//if($BU!='OU'){
		$this->config['OU']['db'] = $OU_db;
		$this->config['OU']['sv'] = $this->server['ServerName'];
		$this->config['OU']['user'] = $this->server['UserName'];
		$this->config['OU']['pass'] = $this->server['UserPassword'];
		//}

		// emp_db
		$this->config['emp']['db'] = $emp_db;
		$this->config['emp']['sv'] = $this->server['ServerName'];
		$this->config['emp']['user'] = $this->server['UserName'];
		$this->config['emp']['pass'] = $this->server['UserPassword'];
		//

		{ //เปิดปิดฐานข้อมูล
			$this->disDB['cusNew'] = 'ON';
			$this->disDB['cusOld'] = 'ON';
			$this->disDB['OU'] = 'ON';
			$this->disDB['emp'] = 'ON';
		}

		{ //array ไว้เช็คตารางฐานข้อมูล
			$this->chkDB['cusNew'] = array('AMIAN_CUS_TYPE','CHANGE_GRADE_HISTORY','MAIN_ADDRESS','MAIN_CUS_GINFO','MAIN_EDUCATION','MAIN_EMAILS','MAIN_JOBS',
			'MAIN_TELEPHONE','MIAN_CUS_TYPE_REF');
			$this->chkDB['cusOld'] = array('aMain_amphur','aMain_district','aMain_geography','aMain_postcode','aMain_province','first_name','MainCusData',
			'MainCusData_CusEmp_Rel','MainCusData_CusRelation','MainCusData_CusRelationTable','MainCusData_JobTable','MainCus_Type');
			$this->chkDB['OU'] = array('biz_name','relation_position','position');
			$this->chkDB['emp'] = array('emp_data');
		}

		if($this->disDB['cusNew']=='ON' && $this->disDB['cusOld']=='ON'){
			$this->connect();
		}
	}

	//เพิ่ม แก้ไข ข้อมูลลูกค้า
	public function manageDB_Customer($id,$status){
		$BU = $this->BU;

		$sql = "SELECT * FROM ".$this->config['cusOld']['db'].".MainCusData WHERE CusNo='".$id."' LIMIT 1";
		$query = mysql_query($sql);
		$fe = @mysql_fetch_assoc($query);

		if(!$query){
			$this->gQueryLogError($sql,__FILE__.' Line : '.__LINE__);
		}

		$fe['Remark'] = addslashes($fe['Remark']);
		
		//ข้อมูลทั่วไป
		if($status=='update'){
			$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_CUS_GINFO SET Picture='".$fe['Picture']."',Be='".$fe['Be']."',BeType='".$fe['BeType']."',";
			$update .= "Cus_Name='".$fe['Cus_Name']."',Cus_Surename='".$fe['Cus_Surename']."',Cus_Nickname='".$fe['Cus_Nickname']."',";
			$update .= "Cus_Big_Picture='".$fe['Cus_Big_Picture']."',Cus_Small_Picture='".$fe['Cus_Small_Picture']."',Sex='".$fe['Sex']."',";
			$update .= "DateOfBirth='".$fe['DateOfBirth']."',Cus_IDNo_Type='".$fe['Cus_IDNo']."',Cus_IDNo='".$fe['Cus_IDNo']."',Married_Status='".$fe['Status']."',";
			$update .= "Time_for_talk='".$fe['Time_for_talk']."',Date_Receive='".$fe['Date_Receive']."',Data_Received_Num='".$fe['Data_Received_Num']."',";
			$update .= "1stSourceData='".$fe['1stSourceData']."',Remark='".$fe['Remark']."',Updater='".$fe['Updater']."',UpdaterNO='".$fe['UpdaterNO']."',";
			$update .= "UpdateT='".$fe['UpdateT']."', company_type='".$fe['company_type']."', cus_branch_id='".$fe['cus_branch_id']."', ";
			$update .= "cus_branch_txt='".$fe['cus_branch_txt']."' WHERE CusNo='".$id."' LIMIT 1";
			//$this->gQueryAndLogUpdate(1,$update,__FILE__.' Line : '.__LINE__);
			// echo $update."<br/><br/>";
			$this->getLog_data($this->config['cusNew']['db'],'MAIN_CUS_GINFO',"CusNo='".$id."'",$BU,'update');
			$query = mysql_query($update);

			if(!$query){
				$this->gQueryLogError($update,__FILE__.' Line : '.__LINE__);
			}
		}else{
			$this->logTable_DB($this->config['cusNew']['db'].".MAIN_CUS_GINFO");

			$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_CUS_GINFO(CusNo,Picture,Be,BeType,Cus_Name,Cus_Surename,Cus_Nickname,Cus_Big_Picture,";
			$insert .= "Cus_Small_Picture,Sex,DateOfBirth,Cus_IDNo_Type,Cus_IDNo,Married_Status,Time_for_talk,Date_Receive,Data_Received,Data_Received_Num,";
			$insert .= "1stSourceData,Remark,Updater,UpdaterNO,UpdateT,company_type,cus_branch_id,cus_branch_txt)";
			$insert .= "VALUES('".$fe['CusNo']."','".$fe['Picture']."','".$fe['Be']."','".$fe['BeType']."',";
			$insert .= "'".$fe['Cus_Name']."','".$fe['Cus_Surename']."','".$fe['Cus_Nickname']."','".$fe['Cus_Big_Picture']."','".$fe['Cus_Small_Picture']."',";
			$insert .= "'".$fe['Sex']."','".$fe['DateOfBirth']."','".$fe['Cus_IDNo']."','".$fe['Cus_IDNo']."','".$fe['Status']."','".$fe['Time_for_talk']."',";
			$insert .= "'".$fe['Date_Receive']."','".$fe['Data_Received']."','".$fe['Data_Received_Num']."','".$fe['1stSourceData']."','".$fe['Remark']."',";
			$insert .= "'".$fe['Updater']."','".$fe['UpdaterNO']."','".$fe['UpdateT']."','".$fe['company_type']."','".$fe['cus_branch_id']."',";
			$insert .= "'".$fe['cus_branch_txt']."')";
			// echo $insert."<br/><br/>";
			$query = mysql_query($insert);

			$idRun = mysql_insert_id();
			$this->unlogTable_DB();

			if(!$query){
				$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
			}else{
				//$this->gQueryAndLogUpdate(0,'',__FILE__.' Line : '.__LINE__,$this->config['cusNew']['db'].'.MAIN_CUS_GINFO',"CusNo='".$fe['CusNo']."'");
				$this->getLog_data($this->config['cusNew']['db'],'MAIN_CUS_GINFO',"CusNo='".$idRun."'",$BU,'insert');
			}
		}
		//ข้อมูลทั่วไป

		//ข้อมูลผู้ดูแล / ผู้รับผิดชอบลูกค้า
		if($fe['Resperson'] && $this->disDB['OU']=='ON'){
			## fix ให้เป็นของ sale ฐานข้อมูลเก่ามีแต่ของขาย
			$fullName_Bu = $this->fullName_Bu('sale');

			$biz = "SELECT biz_id FROM ".$this->config['OU']['db'].".biz_name WHERE biz_name='".$fullName_Bu."' LIMIT 1";
			$qBiz = mysql_query($biz);
			$fBiz = @mysql_fetch_assoc($qBiz);

			if(!$qBiz){ $this->gQueryLogError($biz,__FILE__.' Line : '.__LINE__); }

			// กรณีไม่มีตำแหน่งงาน
			// ดึงตำแหน่งงานจากใน emp_data
			if(trim($fe['Resperson_posCode']) == ''){
				$sqlEmp	= "SELECT Code ";
				$sqlEmp	.= "FROM ".$this->config['emp']['db'].".emp_data ";
				$sqlEmp	.= "WHERE ID_Card = '".$fe['Resperson']."' ";
				$sqlEmp	.= "ORDER BY DataNo DESC LIMIT 1";
				$queEmp	= mysql_query($sqlEmp);
				$feEmp	= @mysql_fetch_assoc($queEmp);

				if(trim($feEmp['Code'])){
					$fe['Resperson_posCode'] = trim($feEmp['Code']);
				}
			}

			$dep = "SELECT po.Department,po.id,po.PositionCode ";
			$dep .= "FROM ".$this->config['OU']['db'].".relation_position rel ";
			$dep .= "INNER JOIN ".$this->config['OU']['db'].".position po ";
			$dep .= "ON rel.position_id=po.id ";
			$dep .= "WHERE rel.status!='99' ";
			$dep .= "AND po.Status!='99' AND po.Department!='' ";
			$dep .= "AND rel.id_card='".$fe['Resperson']."' ";
			$dep .= "AND TRIM(po.PositionCode)='".trim($fe['Resperson_posCode'])."' LIMIT 1";
			//echo $dep."<br/><br/>";
			$qDep = mysql_query($dep);
			$fDep = @mysql_fetch_assoc($qDep);

			if(!$qDep){ $this->gQueryLogError($dep,__FILE__.' Line : '.__LINE__); }

			$chk = "SELECT RESP_ID FROM ".$this->config['cusNew']['db'].".MAIN_RESPONSIBILITY WHERE RESP_CUSNO='".$fe['CusNo']."' AND ";
			$chk .= "BUSINESS_REFS='".$fBiz['biz_id']."' LIMIT 1";
			$qChk = mysql_query($chk);
			$fChk = @mysql_fetch_assoc($qChk);

			if(!$qChk){ $this->gQueryLogError($chk,__FILE__.' Line : '.__LINE__); }

			if($fChk['RESP_ID']){
				$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_RESPONSIBILITY SET RESP_BU_CUSNO='',RESP_CUSNO='".$fe['CusNo']."',";
				$update .= "BUSINESS_REFS='".$fBiz['biz_id']."',DEPARTMENT_REFS='".$fDep['Department']."',POS_EMP_CODE='".$fe['Resperson_posCode']."',";
				$update .= "RESP_IDCARD='".$fe['Resperson']."',RESP_DATE='".$fe['respon_date']."',RESP_TIME_STAMP=NOW() WHERE RESP_CUSNO='".$fe['CusNo']."' AND ";
				$update .= "BUSINESS_REFS='".$fBiz['biz_id']."' LIMIT 1";
				//$this->gQueryAndLogUpdate(1,$update,__FILE__.' Line : '.__LINE__);
				$this->getLog_data($this->config['cusNew']['db'],'MAIN_RESPONSIBILITY',"RESP_CUSNO='".$fe['CusNo']."' AND BUSINESS_REFS='".$fBiz['biz_id']."'",
				$BU,'update');
				$query = mysql_query($update);

				if(!$query){
					$this->gQueryLogError($update,__FILE__.' Line : '.__LINE__);
				}
			}else{
				$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_RESPONSIBILITY(RESP_BU_CUSNO,RESP_CUSNO,BUSINESS_REFS,DEPARTMENT_REFS,";
				$insert .= "POS_EMP_CODE,RESP_IDCARD,RESP_DATE,RESP_TIME_STAMP)VALUES('','".$fe['CusNo']."','".$fBiz['biz_id']."','".$fDep['Department']."',";
				$insert .= "'".$fe['Resperson_posCode']."','".$fe['Resperson']."','".$fe['respon_date']."',NOW());";
				$query = mysql_query($insert);

				if(!$query){
					$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
				}else{
					/*$this->gQueryAndLogUpdate(0,'',__FILE__.' Line : '.__LINE__,$this->config['cusNew']['db'].'.MAIN_RESPONSIBILITY',
					 "RESP_CUSNO='".$fe['CusNo']."'");*/
					$this->getLog_data($this->config['cusNew']['db'],'MAIN_RESPONSIBILITY',"RESP_CUSNO='".$fe['CusNo']."' AND BUSINESS_REFS='".$fBiz['biz_id']."'",
					$BU,'insert');
				}
			}
		}

		//ประเภทลูกค้า
		if($fe['Cus_Type']=='NoType'){
			$fe['Cus_Type']	= '';
		}

		// if($fe['Cus_Type'] && $fe['Cus_Type']!='NoType'){
			$re = $this->manageCustomer_cusType($fe['Cus_Type']);
			$fullBU = $this->fullName_Bu($BU);
			$sqlIn = '';
			
			// $chk = "SELECT CUS_TYPE_ID FROM ".$this->config['cusNew']['db'].".AMIAN_CUS_TYPE WHERE LOWER(CUS_TYPE_BU_DESC)='".strtolower($re['CUS_TYPE_BU_DESC'])."'";
			// $qChk = mysql_query($chk);
			
			// if(!$qChk){
			// 	$this->gQueryLogError($chk,__FILE__.' Line : '.__LINE__);
			// }else{
			// 	while($fChk = @mysql_fetch_assoc($qChk)){
			// 		if($sqlIn){ $sqlIn .= ","; }
			// 		$sqlIn .= $fChk['CUS_TYPE_ID'];
			// 	}
			// }
			
			$chk2 = "SELECT CTYPE_REF_ID,CTYPE_INSERT_DATE FROM ".$this->config['cusNew']['db'].".MIAN_CUS_TYPE_REF WHERE CTYPE_REF_CUSNO='".$fe['CusNo']."' ";
			$chk2 .= "LIMIT 1";
			// $chk2 .= "AND CTYPE_GRADE_REF IN(".$sqlIn.") LIMIT 1";
			$qChk2 = mysql_query($chk2);
			$fChk2 = @mysql_fetch_assoc($qChk2);
			
			if(!$fChk2['CTYPE_REF_ID']){
				$this->logTable_DB($this->config['cusNew']['db'].".MIAN_CUS_TYPE_REF");
					
				$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MIAN_CUS_TYPE_REF(CTYPE_REF_ID,CTYPE_REF_CUSNO,CTYPE_GRADE_REF,CTYPE_INSERT_DATE)VALUES('',";
				$insert .= "'".$fe['CusNo']."','".$re['CUS_TYPE_ID']."',NOW());";
				$query = mysql_query($insert);
				
				$idType = mysql_insert_id();
				$this->unlogTable_DB();
				
				$this->getLog_data($this->config['cusNew']['db'],'MIAN_CUS_TYPE_REF',"CTYPE_REF_CUSNO='".$fe['CusNo']."' AND CTYPE_GRADE_REF='".$re['CUS_TYPE_ID']."'",
				$BU,'insert');
			}else{
				$update = "UPDATE ".$this->config['cusNew']['db'].".MIAN_CUS_TYPE_REF SET CTYPE_GRADE_REF='".$re['CUS_TYPE_ID']."',CTYPE_INSERT_DATE=NOW() ";
				$update .= "WHERE CTYPE_REF_ID='".$fChk2['CTYPE_REF_ID']."' LIMIT 1";
				$this->getLog_data($this->config['cusNew']['db'],'MIAN_CUS_TYPE_REF',"CTYPE_REF_ID='".$fChk2['CTYPE_REF_ID']."'",$BU,'update');
				$query = mysql_query($update);

				$idType = $fChk2['CTYPE_REF_ID'];
				if(!$query){
					$this->gQueryLogError($update,__FILE__.' Line : '.__LINE__);
				}
			}

			if(!$query){
				$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
			}else{
				$insertCh = "INSERT INTO ".$this->config['cusNew']['db'].".CHANGE_GRADE_HISTORY(CHANGE_GRADE_ID,CHANGE_GRADE_CUSNO,CUS_TYPE_ID,CTYPE_REF_ID,";
				$insertCh .= "CUS_TYPE_BU,CUS_TYPE_BU_DESC,CUS_TYPE_KIND,CUS_TYPE_KIND_DESC,CUS_TYPE_LEVEL,CUS_TYPE_LEVEL_DESC,CHANGE_INSERT_DATE,CHANGE_GRADE_DATE,";
				$insertCh .= "CHANGE_GRADE_PERSON)VALUES(NULL,'".$fe['CusNo']."','".$re['CUS_TYPE_ID']."','".$idType."','".$re['CUS_TYPE_BU']."',";
				$insertCh .= "'".$re['CUS_TYPE_BU_DESC']."','".$re['CUS_TYPE_KIND']."','".$re['CUS_TYPE_KIND_DESC']."','".$re['CUS_TYPE_LEVEL']."',";
				$insertCh .= "'".$re['CUS_TYPE_LEVEL_DESC']."','".$fChk2['CTYPE_INSERT_DATE']."',NOW(),'".$fe['UpdaterNO']."');";
				$queryCh = mysql_query($insertCh);

				if(!$queryCh){
					$this->gQueryLogError($insertCh,__FILE__.' Line : '.__LINE__);
				}else{
					$this->getLog_data($this->config['cusNew']['db'],'CHANGE_GRADE_HISTORY',"CHANGE_GRADE_CUSNO='".$fe['CusNo']."' AND 
					CUS_TYPE_ID='".$re['CUS_TYPE_ID']."' AND CTYPE_REF_ID='".$idType."'",$BU,'insert');
				}
			}
		// }

		//ข้อมูลประวัติดการศึกษา..
		//1=ประถมศึกษา,2=ม.ต้น,3=ม.ปลาย / ปวช.,4=ปริญญาตรี,5=ปริญญาโท,6=ปริญญาเอก
		$arrayEdu = array('1'=>'Primary_Edu','2'=>'Secondary_Edu','3'=>'Highschool_Edu','4'=>'Bachelor','5'=>'Master','6'=>'Phd');

		foreach($arrayEdu AS $key => $field){
			if($fe[$field]){
				if($status=='update'){
					$chk = "SELECT EDU_ID_NUM FROM ".$this->config['cusNew']['db'].".MAIN_EDUCATION WHERE EDU_CUS_NO='".$fe['CusNo']."' AND EDU_TYPE='".$key."' LIMIT 1";
					$qChk = mysql_query($chk);
					$fChk = @mysql_fetch_assoc($qChk);

					if(!$qChk){
						$this->gQueryLogError($chk,__FILE__.' Line : '.__LINE__);
					}

					if($fChk['EDU_ID_NUM']){
						$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_EDUCATION SET EDU_NAME='".$fe[$field]."' WHERE ";
						$update .= "EDU_ID_NUM='".$fChk['EDU_ID_NUM']."' LIMIT 1";
						//$this->gQueryAndLogUpdate(1,$update,__FILE__.' Line : '.__LINE__);
						$this->getLog_data($this->config['cusNew']['db'],'MAIN_EDUCATION',"EDU_ID_NUM='".$fChk['EDU_ID_NUM']."'",$BU,'update');
						$query = mysql_query($update);

						if(!$query){
							$this->gQueryLogError($update,__FILE__.' Line : '.__LINE__);
						}
					}else{
						$this->logTable_DB($this->config['cusNew']['db'].".MAIN_EDUCATION");

						$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_EDUCATION(EDU_CUS_NO,EDU_NAME,EDU_ADDRESS,EDU_DETAIL,EDU_TYPE)VALUES(";
						$insert .= "'".$fe['CusNo']."','".$fe[$field]."','','','".$key."');";
						$query = mysql_query($insert);

						$idRun = mysql_insert_id();
						$this->unlogTable_DB();

						if(!$query){
							$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
						}else{
							/*$this->gQueryAndLogUpdate(0,'',__FILE__.' Line : '.__LINE__,$this->config['cusNew']['db'].'.MAIN_EDUCATION',
							 "EDU_CUS_NO='".$fe['CusNo']."' AND EDU_TYPE='".$key."'");*/
							$this->getLog_data($this->config['cusNew']['db'],'MAIN_EDUCATION',"EDU_CUS_NO='".$fe['CusNo']."' AND EDU_TYPE='".$key."'",$BU,'insert');
						}
					}
				}else{
					$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_EDUCATION(EDU_CUS_NO,EDU_NAME,EDU_ADDRESS,EDU_DETAIL,EDU_TYPE)VALUES(";
					$insert .= "'".$fe['CusNo']."','".$fe[$field]."','','','".$key."');";
					$query = mysql_query($insert);

					if(!$query){
						$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
					}else{
						/*$this->gQueryAndLogUpdate(0,'',__FILE__.' Line : '.__LINE__,$this->config['cusNew']['db'].'.MAIN_EDUCATION',
						 "EDU_CUS_NO='".$fe['CusNo']."' AND EDU_TYPE='".$key."'");*/
						$this->getLog_data($this->config['cusNew']['db'],'MAIN_EDUCATION',"EDU_CUS_NO='".$fe['CusNo']."' AND EDU_TYPE='".$key."'",$BU,'insert');
					}
				}
			}
		}
		//ข้อมูลประวัติดการศึกษา

		//ข้อมูลที่อยู่หลัก
		//1=ที่อยู่ตามบัตร ปชช,2=ที่อยู่ส่งเอกสาร,3=ที่ทำงาน
		$arrayAdd[1] = array('0'=>'C_Add','1'=>'C_Vill','2'=>'C_Tum','3'=>'C_Amp','4'=>'C_Pro','5'=>'C_GEO','6'=>'C_Code','7'=>'C_AddGPSLatitude',
		'8'=>'C_AddGPSLongtitude','9'=>'C_AddMapDescribe');
		$arrayAdd[2] = array('0'=>'Cus_Add','1'=>'Cus_Vil','2'=>'Cus_Tum','3'=>'Cus_Amp','4'=>'Cus_Pro','5'=>'Cus_GEO','6'=>'Cus_Code','7'=>'Cus_AddGPSLatitude',
		'8'=>'Cus_AddGPSLongtitude','9'=>'Cus_AddMapDescribe');
		$arrayAdd[3] = array('0'=>'W_Add','1'=>'W_Vill','2'=>'W_Tum','3'=>'W_Amp','4'=>'W_Pro','5'=>'W_GEO','6'=>'W_Code','7'=>'Cus_WGPSLatitude',
		'8'=>'Cus_WGPSLongtitude','9'=>'Cus_WMapDescribe');

		foreach($arrayAdd AS $key => $value){
			if($fe[$value[0]]){
				if($status=='update'){
					$chk = "SELECT ADDR_ID_NUM FROM ".$this->config['cusNew']['db'].".MAIN_ADDRESS WHERE ADDR_CUS_NO='".$fe['CusNo']."' AND ADDR_TYPE='".$key."' LIMIT 1";
					$qChk = mysql_query($chk);
					$fChk = @mysql_fetch_assoc($qChk);

					if(!$qChk){
						$this->gQueryLogError($chk,__FILE__.' Line : '.__LINE__);
					}

					if($fChk['ADDR_ID_NUM']){
						$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_ADDRESS SET ADDR_VILLAGE='".$fe[$value[1]]."',ADDR_NUMBER='".$fe[$value[0]]."',";
						$update .= "ADDR_SUB_DISTRICT='".$fe[$value[2]]."',ADDR_DISTRICT='".$fe[$value[3]]."',ADDR_PROVINCE='".$fe[$value[4]]."',";
						$update .= "ADDR_POSTCODE='".$fe[$value[6]]."',ADDR_GEOGRAPHY='".$fe[$value[5]]."',ADDR_GPS_LATITUDE='".$fe[$value[7]]."',";
						$update .= "ADDR_GPS_LONGTITUDE='".$fe[$value[8]]."',ADDR_MAP_DESC='".$fe[$value[9]]."' WHERE ADDR_ID_NUM='".$fChk['ADDR_ID_NUM']."' LIMIT 1";
						//$this->gQueryAndLogUpdate(1,$update,__FILE__.' Line : '.__LINE__);
						$this->getLog_data($this->config['cusNew']['db'],'MAIN_ADDRESS',"ADDR_ID_NUM='".$fChk['ADDR_ID_NUM']."'",$BU,'update');
						$query = mysql_query($update);

						if(!$query){
							$this->gQueryLogError($update,__FILE__.' Line : '.__LINE__);
						}
					}else{
						if($key==2){ $main = 1; }else{ $main = 0; }
							
						$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_ADDRESS(ADDR_CUS_NO,ADDR_VILLAGE,ADDR_ROOM_NO,ADDR_FLOOR_NO,ADDR_NUMBER,";
						$insert .= "ADDR_GROUP_NO,ADDR_LANE,ADDR_ROAD,ADDR_SUB_DISTRICT,ADDR_DISTRICT,ADDR_PROVINCE,ADDR_POSTCODE,ADDR_GEOGRAPHY,ADDR_GPS_LATITUDE,";
						$insert .= "ADDR_GPS_LONGTITUDE,ADDR_MAP_DESC,ADDR_REMARK,ADDR_TYPE,ADDR_MAIN_ACTIVE)VALUES('".$fe['CusNo']."','".$fe[$value[1]]."','','',";
						$insert .= "'".$fe[$value[0]]."','','','','".$fe[$value[2]]."','".$fe[$value[3]]."','".$fe[$value[4]]."','".$fe[$value[6]]."',";
						$insert .= "'".$fe[$value[5]]."','".$fe[$value[7]]."','".$fe[$value[8]]."','".$fe[$value[9]]."','','".$key."','".$main."');";
						$query = mysql_query($insert);

						if(!$query){
							$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
						}else{
							/*$this->gQueryAndLogUpdate(0,'',__FILE__.' Line : '.__LINE__,$this->config['cusNew']['db'].'.MAIN_ADDRESS',
							 "ADDR_CUS_NO='".$fe['CusNo']."' AND ADDR_TYPE='".$key."'");*/
							$this->getLog_data($this->config['cusNew']['db'],'MAIN_ADDRESS',"ADDR_CUS_NO='".$fe['CusNo']."' AND ADDR_TYPE='".$key."'",$BU,'insert');
						}
					}
				}else{
					if($key==2){ $main = 1; }else{ $main = 0; }

					$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_ADDRESS(ADDR_CUS_NO,ADDR_VILLAGE,ADDR_ROOM_NO,ADDR_FLOOR_NO,ADDR_NUMBER,";
					$insert .= "ADDR_GROUP_NO,ADDR_LANE,ADDR_ROAD,ADDR_SUB_DISTRICT,ADDR_DISTRICT,ADDR_PROVINCE,ADDR_POSTCODE,ADDR_GEOGRAPHY,ADDR_GPS_LATITUDE,";
					$insert .= "ADDR_GPS_LONGTITUDE,ADDR_MAP_DESC,ADDR_REMARK,ADDR_TYPE,ADDR_MAIN_ACTIVE)VALUES('".$fe['CusNo']."','".$fe[$value[1]]."','','',";
					$insert .= "'".$fe[$value[0]]."','','','','".$fe[$value[2]]."','".$fe[$value[3]]."','".$fe[$value[4]]."','".$fe[$value[6]]."','".$fe[$value[5]]."',";
					$insert .= "'".$fe[$value[7]]."','".$fe[$value[8]]."','".$fe[$value[9]]."','','".$key."','".$main."');";
					$query = mysql_query($insert);

					if(!$query){
						$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
					}else{
						/*$this->gQueryAndLogUpdate(0,'',__FILE__.' Line : '.__LINE__,$this->config['cusNew']['db'].'.MAIN_ADDRESS',
						 "ADDR_CUS_NO='".$fe['CusNo']."' AND ADDR_TYPE='".$key."'");*/
						$this->getLog_data($this->config['cusNew']['db'],'MAIN_ADDRESS',"ADDR_CUS_NO='".$fe['CusNo']."' AND ADDR_TYPE='".$key."'",$BU,'insert');
					}
				}
			}
		}
		//ข้อมูลที่อยู่หลัก

		//อีเมล์
		//0=อีเมล์ที่ทำงาน,1=อีเมล์ส่วนตัว
		$arrayEmail['email'] = array('0'=>'W_Email','1'=>'Email');
		$arrayEmail['des'] = array('0'=>'อีเมล์ที่ทำงาน','1'=>'อีเมล์ส่วนตัว');

		foreach($arrayEmail['email'] AS $key => $value){
			if($fe[$value]){
				if($status=='update'){
					$chk = "SELECT EMAILS_ID_NUM FROM ".$this->config['cusNew']['db'].".MAIN_EMAILS WHERE EMAILS_CUS_NO='".$fe['CusNo']."' AND ";
					$chk .= "EMAILS_STATUS='".$key."' LIMIT 1";
					$qChk = mysql_query($chk);
					$fChk = @mysql_fetch_assoc($qChk);

					if(!$qChk){
						$this->gQueryLogError($chk,__FILE__.' Line : '.__LINE__);
					}

					if($fChk['EMAILS_ID_NUM']){
						$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_EMAILS SET EMAILS_ADDRESS='".$fe[$value]."',EMAILS_DESC='".$arrayEmail['des'][$key]."' ";
						$update .= "WHERE EMAILS_ID_NUM='".$fChk['EMAILS_ID_NUM']."' LIMIT 1";
						//$this->gQueryAndLogUpdate(1,$update,__FILE__.' Line : '.__LINE__);
						$this->getLog_data($this->config['cusNew']['db'],'MAIN_EMAILS',"EMAILS_ID_NUM='".$fChk['EMAILS_ID_NUM']."'",$BU,'update');
						$query = mysql_query($update);

						if(!$query){
							$this->gQueryLogError($update,__FILE__.' Line : '.__LINE__);
						}
					}else{
						$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_EMAILS(EMAILS_CUS_NO,EMAILS_ADDRESS,EMAILS_DESC,EMAILS_STATUS)VALUES";
						$insert .= "('".$fe['CusNo']."','".$fe[$value]."','".$arrayEmail['des'][$key]."','".$key."');";
						$query = mysql_query($insert);

						if(!$query){
							$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
						}else{
							/*$this->gQueryAndLogUpdate(0,'',__FILE__.' Line : '.__LINE__,$this->config['cusNew']['db'].'.MAIN_EMAILS',
							 "EMAILS_CUS_NO='".$fe['CusNo']."' AND EMAILS_STATUS='".$key."'");*/
							$this->getLog_data($this->config['cusNew']['db'],'MAIN_EMAILS',"EMAILS_CUS_NO='".$fe['CusNo']."' AND EMAILS_STATUS='".$key."'",$BU,'insert');
						}
					}
				}else{
					$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_EMAILS(EMAILS_CUS_NO,EMAILS_ADDRESS,EMAILS_DESC,EMAILS_STATUS)VALUES";
					$insert .= "('".$fe['CusNo']."','".$fe[$value]."','".$arrayEmail[$key]."','".$key."');";
					$query = mysql_query($insert);

					if(!$query){
						$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
					}else{
						/*$this->gQueryAndLogUpdate(0,'',__FILE__.' Line : '.__LINE__,$this->config['cusNew']['db'].'.MAIN_EMAILS',
						 "EMAILS_CUS_NO='".$fe['CusNo']."' AND EMAILS_STATUS='".$key."'");*/
						$this->getLog_data($this->config['cusNew']['db'],'MAIN_EMAILS',"EMAILS_CUS_NO='".$fe['CusNo']."' AND EMAILS_STATUS='".$key."'",$BU,'insert');
					}
				}
			}
		}
		//อีเมล์

		//ข้อมูลอาชีพ
		$job = "SELECT JobID FROM ".$this->config['cusOld']['db'].".MainCusData_JobTable WHERE Job='".$fe['Job']."' LIMIT 1";
		$qJob = mysql_query($job);
		$fJob = @mysql_fetch_assoc($qJob);

		if(!$qJob){
			$this->gQueryLogError($job,__FILE__.' Line : '.__LINE__);
		}

		if($status=='update'){
			$chk = "SELECT JOB_ID_NUM FROM ".$this->config['cusNew']['db'].".MAIN_JOBS WHERE JOB_CUS_NO='".$fe['CusNo']."' LIMIT 1";
			$qChk = mysql_query($chk);
			$fChk = @mysql_fetch_assoc($qChk);

			if(!$qChk){
				$this->gQueryLogError($chk,__FILE__.' Line : '.__LINE__);
			}

			if($fChk['JOB_ID_NUM']){
				$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_JOBS SET JOB_REF_MAIN_ID='".$fJob['JobID']."',JOB_NAME='".$fe['Job']."',";
				$update .= "JOB_DESC='".$fe['Job_Des']."' WHERE JOB_ID_NUM='".$fChk['JOB_ID_NUM']."' LIMIT 1";
				//$this->gQueryAndLogUpdate(1,$update,__FILE__.' Line : '.__LINE__);
				$this->getLog_data($this->config['cusNew']['db'],'MAIN_JOBS',"JOB_ID_NUM='".$fChk['JOB_ID_NUM']."'",$BU,'update');
				$query = mysql_query($update);

				if(!$query){
					$this->gQueryLogError($update,__FILE__.' Line : '.__LINE__);
				}
			}else{
				$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_JOBS(JOB_CUS_NO,JOB_REF_MAIN_ID,JOB_NAME,JOB_DESC,JOB_MAIN_ACTIVE,JOB_STATUS)VALUES";
				$insert .= "('".$fe['CusNo']."','".$fJob['JobID']."','".$fe['Job']."','".$fe['Job_Des']."','1','0');";
				$query = mysql_query($insert);

				if(!$query){
					$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
				}else{
					/*$this->gQueryAndLogUpdate(0,'',__FILE__.' Line : '.__LINE__,$this->config['cusNew']['db'].'.MAIN_JOBS',
					 "JOB_CUS_NO='".$fe['CusNo']."' AND JOB_REF_MAIN_ID='".$fJob['JobID']."'");*/
					$this->getLog_data($this->config['cusNew']['db'],'MAIN_JOBS',"JOB_CUS_NO='".$fe['CusNo']."' AND JOB_REF_MAIN_ID='".$fJob['JobID']."'",$BU,'insert');
				}
			}
		}else{
			$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_JOBS(JOB_CUS_NO,JOB_REF_MAIN_ID,JOB_NAME,JOB_DESC,JOB_MAIN_ACTIVE,JOB_STATUS)VALUES";
			$insert .= "('".$fe['CusNo']."','".$fJob['JobID']."','".$fe['Job']."','".$fe['Job_Des']."','1','0');";
			$query = mysql_query($insert);

			if(!$query){
				$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
			}else{
				/*$this->gQueryAndLogUpdate(0,'',__FILE__.' Line : '.__LINE__,$this->config['cusNew']['db'].'.MAIN_JOBS',
				 "JOB_CUS_NO='".$fe['CusNo']."' AND JOB_REF_MAIN_ID='".$fJob['JobID']."'");*/
				$this->getLog_data($this->config['cusNew']['db'],'MAIN_JOBS',"JOB_CUS_NO='".$fe['CusNo']."' AND JOB_REF_MAIN_ID='".$fJob['JobID']."'",$BU,'insert');
			}
		}
		//ข้อมูลอาชีพ

		//เบอร์โทรศัพท์
		$arrTel = array('1'=>'M_Tel','2'=>'W_Tel','3'=>'H_Tel','4'=>'W_Fax','5'=>'Pagger');

		foreach($arrTel AS $key => $value){
			if($fe[$value]){
				if($status=='update'){
					$chk = "SELECT TEL_ID FROM ".$this->config['cusNew']['db'].".MAIN_TELEPHONE WHERE TEL_CUS_NO='".$fe['CusNo']."' AND TEL_TYPE='".$key."' LIMIT 1";
					$qChk = mysql_query($chk);
					$fChk = @mysql_fetch_assoc($qChk);

					if(!$qChk){
						$this->gQueryLogError($chk,__FILE__.' Line : '.__LINE__);
					}

					if($fChk['TEL_ID']){
						$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_TELEPHONE SET TEL_NUM='".$fe[$value]."' WHERE TEL_ID='".$fChk['TEL_ID']."' LIMIT 1";
						//$this->gQueryAndLogUpdate(1,$update,__FILE__.' Line : '.__LINE__);
						$this->getLog_data($this->config['cusNew']['db'],'MAIN_TELEPHONE',"TEL_ID='".$fChk['TEL_ID']."'",$BU,'update');
						$query = mysql_query($update);

						if(!$query){
							$this->gQueryLogError($update,__FILE__.' Line : '.__LINE__);
						}
					}else{
						$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_TELEPHONE(TEL_CUS_NO,TEL_NUM,TEL_TYPE,TEL_DESC,TEL_REMARK)VALUES";
						$insert .= "('".$fe['CusNo']."','".$fe[$value]."','".$key."','','');";
						$query = mysql_query($insert);

						if(!$query){
							$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
						}else{
							/*$this->gQueryAndLogUpdate(0,'',__FILE__.' Line : '.__LINE__,$this->config['cusNew']['db'].'.MAIN_TELEPHONE',
							 "TEL_CUS_NO='".$fe['CusNo']."' AND TEL_TYPE='".$key."'");*/
							$this->getLog_data($this->config['cusNew']['db'],'MAIN_TELEPHONE',"TEL_CUS_NO='".$fe['CusNo']."' AND TEL_TYPE='".$key."'",$BU,'insert');
						}
					}
				}else{
					$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_TELEPHONE(TEL_CUS_NO,TEL_NUM,TEL_TYPE,TEL_DESC,TEL_REMARK)VALUES";
					$insert .= "('".$fe['CusNo']."','".$fe[$value]."','".$key."','','');";
					$query = mysql_query($insert);

					if(!$query){
						$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
					}else{
						/*$this->gQueryAndLogUpdate(0,'',__FILE__.' Line : '.__LINE__,$this->config['cusNew']['db'].'.MAIN_TELEPHONE',
						 "TEL_CUS_NO='".$fe['CusNo']."' AND TEL_TYPE='".$key."'");*/
						$this->getLog_data($this->config['cusNew']['db'],'MAIN_TELEPHONE',"TEL_CUS_NO='".$fe['CusNo']."' AND TEL_TYPE='".$key."'",$BU,'insert');
					}
				}
			}
		}
		//เบอร์โทรศัพท์
		
		## BEGIN ความสัมพันธ์ลูกค้ากับพนักงาน
		## หาว่ามีความสัมพันธ์เท่าไร
		$sqlEmp	= "SELECT * FROM ".$this->config['cusOld']['db'].".MainCusData_CusEmp_Rel WHERE CusNo_Ref='".$id."'";
		$queEmp = mysql_query($sqlEmp);
		
		if(!$queEmp){
			$this->gQueryLogError($sqlEmp,__FILE__.' Line : '.__LINE__);
		}else{
			while($feEmp = @mysql_fetch_assoc($queEmp)){
				## หา ว่าเป็นประเภท id ไหน
				$sqlType = "SELECT REL_TYPE_ID FROM ".$this->config['cusNew']['db'].".MAIN_RELATION_TYPE WHERE TRIM(REL_TYPE_NAME)='".trim($feEmp['Rel_Level'])."' LIMIT 1";
				$queType = mysql_query($sqlType);
				
				if(!$queType){
					$this->gQueryLogError($sqlType,__FILE__.' Line : '.__LINE__);
				}else{
					$feType	= @mysql_fetch_assoc($queType);
				
					if($feType['REL_TYPE_ID']>0 && $feEmp['Emp_Id_Ref']){
						## เช็คว่ามีแล้วหรือยัง
						$sqlChk	= "SELECT REL_EMP_ID FROM ".$this->config['cusNew']['db'].".MAIN_RELATION_EMP WHERE REL_CUSNO='".$id."' AND REL_EMPID='".$feEmp['Emp_Id_Ref']."' AND REL_EMP_TYPE='".$feType['REL_TYPE_ID']."' LIMIT 1";
						$queChk = mysql_query($sqlChk);
					
						if(!$queChk){
							$this->gQueryLogError($sqlChk,__FILE__.' Line : '.__LINE__);
						}else{
							$feChk	= @mysql_fetch_assoc($queChk);
						
							## ถ้ามีแล้ว
							if($feChk['REL_EMP_ID']){
								$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_RELATION_EMP ";
								$update .= "SET REL_EMP_GROUP='".$feEmp['Rel_Grp']."', REL_EMP_REMARK='".$feEmp['Rel_Remark']."' ";
								$update .= "WHERE REL_EMP_ID='".$feChk['REL_EMP_ID']."' LIMIT 1";
								// echo $update."<br/><br/>";
								$this->getLog_data($this->config['cusNew']['db'],'MAIN_RELATION_EMP',"REL_EMP_ID='".$feChk['REL_EMP_ID']."'",$BU,'update');
								$query	= mysql_query($update);
								
								if(!$query){
									$this->gQueryLogError($update,__FILE__.' Line : '.__LINE__);
								}
							## ถ้ายังไม่มี
							}else{
								$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_RELATION_EMP (REL_EMP_ID, REL_CUSNO, REL_EMPID, REL_EMP_TYPE, REL_EMP_GROUP, ";
								$insert .= "REL_EMP_REMARK) VALUES (NULL, '".$id."', '".$feEmp['Emp_Id_Ref']."', '".$feType['REL_TYPE_ID']."', '".$feEmp['Rel_Grp']."', ";
								$insert .= "'".$feEmp['Rel_Remark']."');";
								// echo $insert."<br/><br/>";
								$query = mysql_query($insert);
								
								if(!$query){
									$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
								}else{
									$this->getLog_data($this->config['cusNew']['db'],'MAIN_RELATION_EMP',"REL_CUSNO='".$id."' AND REL_EMP_TYPE='".$feEmp['Emp_Id_Ref']."' AND REL_EMP_TYPE='".$feType['REL_TYPE_ID']."'",$BU,'insert');
								}
							}
						}
					}
				}
			}
		}
		## END ความสัมพันธ์ลูกค้ากับพนักงาน
		
		## BEGIN ความสัมพันธ์ลูกค้ากับลูกค้า
		## หาว่ามีความสัมพันธ์เท่าไร
		$sqlRel	= "SELECT * FROM ".$this->config['cusOld']['db'].".MainCusData_CusRelation WHERE Relat_CusNo_P1='".$id."'";
		$queRel = mysql_query($sqlRel);
		
		if(!$queRel){
			$this->gQueryLogError($sqlRel,__FILE__.' Line : '.__LINE__);
		}else{
			while($feRel = @mysql_fetch_assoc($queRel)){
				## หา ว่าเป็นประเภท id ไหน
				$sqlType = "SELECT REL_TYPE_ID, REL_REF_ID FROM ".$this->config['cusNew']['db'].".MAIN_RELATION_TYPE WHERE TRIM(REL_TYPE_NAME)='".trim($feRel['Relat_Relation'])."' LIMIT 1";
				$queType = mysql_query($sqlType);
				
				if(!$queType){
					$this->gQueryLogError($sqlType,__FILE__.' Line : '.__LINE__);
				}else{
					$feType	= @mysql_fetch_assoc($queType);
				
					if($feType['REL_TYPE_ID']>0 && $feRel['Relat_CusNo_P2']){
						## เช็คว่ามีแล้วหรือยัง
						$sqlChk	= "SELECT REL_CUS_ID FROM ".$this->config['cusNew']['db'].".MAIN_RELATION_CUS WHERE REL_CUSNO_P1='".$id."' AND REL_CUSNO_P2='".$feRel['Relat_CusNo_P2']."' AND REL_CUS_TYPE='".$feType['REL_TYPE_ID']."' LIMIT 1";
						$queChk = mysql_query($sqlChk);
					
						if(!$queChk){
							$this->gQueryLogError($sqlChk,__FILE__.' Line : '.__LINE__);
						}else{
							$feChk	= @mysql_fetch_assoc($queChk);
						
							## ถ้ามีแล้ว
							if($feChk['REL_CUS_ID']){
								## คนที่ 1
								$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_RELATION_CUS ";
								$update .= "SET REL_CUS_GROUP='".$feRel['Group_Name']."', REL_CUS_REMARK='".$feRel['Relat_Remark']."' ";
								$update .= "WHERE REL_CUS_ID='".$feChk['REL_CUS_ID']."' LIMIT 1";
								// echo $update."<br/><br/>";
								$this->getLog_data($this->config['cusNew']['db'],'MAIN_RELATION_CUS',"REL_CUS_ID='".$feChk['REL_CUS_ID']."'",$BU,'update');
								$query	= mysql_query($update);
								
								if(!$query){
									$this->gQueryLogError($update,__FILE__.' Line : '.__LINE__);
								}
								
								## คนที่ 2
								$update = "UPDATE ".$this->config['cusNew']['db'].".MAIN_RELATION_CUS ";
								$update .= "SET REL_CUS_GROUP='".$feRel['Group_Name']."', REL_CUS_REMARK='".$feRel['Relat_Remark']."' ";
								$update .= "WHERE REL_CUSNO_P1='".$feRel['Relat_CusNo_P2']."' AND REL_CUSNO_P2='".$id."' AND REL_CUS_TYPE='".$feType['REL_REF_ID']."' LIMIT 1";
								// echo $update."<br/><br/>";
								$this->getLog_data($this->config['cusNew']['db'],'MAIN_RELATION_CUS',"REL_CUSNO_P1='".$feRel['Relat_CusNo_P2']."' AND REL_CUSNO_P2='".$id."' AND REL_CUS_TYPE='".$feType['REL_REF_ID']."'",$BU,'update');
								$query	= mysql_query($update);
								
								if(!$query){
									$this->gQueryLogError($update,__FILE__.' Line : '.__LINE__);
								}
							## ถ้ายังไม่มี
							}else{
								## คนที่ 1
								$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_RELATION_CUS (REL_CUS_ID, REL_CUSNO_P1, REL_CUSNO_P2, REL_CUS_TYPE, REL_CUS_GROUP, ";
								$insert .= "REL_CUS_REMARK) VALUES (NULL, '".$id."', '".$feRel['Relat_CusNo_P2']."', '".$feType['REL_TYPE_ID']."', '".$feRel['Group_Name']."', ";
								$insert .= "'".$feRel['Relat_Remark']."');";
								// echo $insert."<br/><br/>";
								$query = mysql_query($insert);
								
								if(!$query){
									$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
								}else{
									$this->getLog_data($this->config['cusNew']['db'],'MAIN_RELATION_CUS',"REL_CUSNO_P1='".$id."' AND REL_CUSNO_P2='".$feRel['Relat_CusNo_P2']."' AND REL_CUS_TYPE='".$feType['REL_TYPE_ID']."'",$BU,'insert');
								}
								
								## คนที่ 2
								$insert = "INSERT INTO ".$this->config['cusNew']['db'].".MAIN_RELATION_CUS (REL_CUS_ID, REL_CUSNO_P1, REL_CUSNO_P2, REL_CUS_TYPE, REL_CUS_GROUP, ";
								$insert .= "REL_CUS_REMARK) VALUES (NULL, '".$feRel['Relat_CusNo_P2']."', '".$id."', '".$feType['REL_REF_ID']."', '".$feRel['Group_Name']."', ";
								$insert .= "'".$feRel['Relat_Remark']."');";
								// echo $insert."<br/><br/>";
								$query = mysql_query($insert);
								
								if(!$query){
									$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
								}else{
									$this->getLog_data($this->config['cusNew']['db'],'MAIN_RELATION_CUS',"REL_CUSNO_P1='".$feRel['Relat_CusNo_P2']."' AND REL_CUSNO_P2='".$id."' AND REL_CUS_TYPE='".$feType['REL_REF_ID']."'",$BU,'insert');
								}
							}
						}
					}
				}
			}
		}
		## END ความสัมพันธ์ลูกค้ากับลูกค้า
	}

	//ประเภทลูกค้า
	public function manageCustomer_cusType($name,$type='null'){
		$BU = $this->BU;
		$this->setConnect_customer();

		$sql = "SELECT * FROM ".$this->config['cusNew']['db'].".AMIAN_CUS_TYPE WHERE LOWER(CUS_TYPE_LEVEL_DESC)='".strtolower($name)."' AND CUS_TYPE_STATUS!='99' LIMIT 1";
		$que = mysql_query($sql);
		$fe = @mysql_fetch_assoc($que);

		if(!$que){
			$this->gQueryLogError($sql,__FILE__.' Line : '.__LINE__);
		}else{
			if(trim($name)!=''){
				if(!$fe['CUS_TYPE_ID']){
					$fullBU = $this->fullName_Bu($BU);

					//id และ ชื่อ BU
					$biz = "SELECT biz_id,biz_name FROM ".$this->config['OU']['db'].".biz_name WHERE biz_name='".$fullBU."' LIMIT 1";
					$qBiz = mysql_query($biz);
					$fBiz = @mysql_fetch_assoc($qBiz);

					//ข้อมูลบันทึก
					$idBiz = $fBiz['biz_id'];
					$nameBiz = $fBiz['biz_name'];
					$kind = '1';
					$kindDesc = 'กำลังชื้อ';
					$level = 'S';
					$levelDesc = $name;
					//ข้อมูลบันทึก
					
					$insert = "INSERT INTO ".$this->config['cusNew']['db'].".AMIAN_CUS_TYPE(CUS_TYPE_BU,CUS_TYPE_BU_DESC,CUS_TYPE_KIND,CUS_TYPE_KIND_DESC,";
					$insert .= "CUS_TYPE_LEVEL,CUS_TYPE_LEVEL_DESC,CUS_TYPE_STATUS)VALUES('".$idBiz."','".$nameBiz."','".$kind."','".$kindDesc."','".$level."',";
					$insert .= "'".$levelDesc."','');";
					$query = mysql_query($insert);

					$cus_type_id = mysql_insert_id();

					if(!$query){
						$this->gQueryLogError($insert,__FILE__.' Line : '.__LINE__);
					}else{
						$this->getLog_data($this->config['cusNew']['db'],'AMIAN_CUS_TYPE',"LOWER(CUS_TYPE_LEVEL_DESC)='".strtolower($name)."'",$BU,'insert');

						$fe['CUS_TYPE_ID']			= $cus_type_id;
						$fe['CUS_TYPE_BU']			= $idBiz;
						$fe['CUS_TYPE_BU_DESC']		= $nameBiz;
						$fe['CUS_TYPE_KIND']		= $kind;
						$fe['CUS_TYPE_KIND_DESC']	= $kindDesc;
						$fe['CUS_TYPE_LEVEL']		= $level;
						$fe['CUS_TYPE_LEVEL_DESC']	= $levelDesc;
						$fe['CUS_TYPE_STATUS']		= 0;
					}
				}
			}
		}

		return $fe;
	}

	//บันทึกข้อมูลลูกค้า
	public function manageCustomer($id){
		if(abs($id)){
			$this->setConnect_customer();

			if($this->disDB['cusNew']=='ON' && $this->disDB['cusOld']=='ON'){
				//เช็คข้อมูลว่ามีหรือยัง
				$chk = "SELECT CusNo FROM ".$this->config['cusNew']['db'].".MAIN_CUS_GINFO WHERE CusNo='".$id."' LIMIT 1";
				$qChk = mysql_query($chk);
				$fChk = @mysql_fetch_assoc($qChk);

				if(!$qChk){
					$this->gQueryLogError($chk,__FILE__.' Line : '.__LINE__);
				}else{
					//update
					if($fChk['CusNo']){
						$this->manageDB_Customer($id,'update');
					//insert
					}else{
						$this->manageDB_Customer($id,'insert');
					}
				}
			}

			@mysql_close( $this->conn );
		}
	}
}
?>