<?php
require_once dirname(dirname(dirname(__FILE__))).'/ERP_crm/config/connect_db.php';
Conn2DB();

## สั่งทำงานโค้ด
$doCode = ""; 		## ทำงาน
// $doCode = "No";  ## ไม่ทำงาน แสดงโค้ด Insert, Update, Delete
// $doCode = "All"; ## ทำงาน แสดงโค้ด Select, Insert, Update, Delete

class crm_popup{
	private $dbName 	= 'main_crm';
	private $checkTime 	= 'easyhr_icchecktime2';
	private $pathCRM 	= '';
	private $pathImg 	= '../';
	private $BU 		= '';

	public function config_General(){
		global $config;

		## เอาไว้ใช้ส่งค่า
		$this->dbName 	 = $config['db_base_name'];
		$this->checkTime = $config['db_emp'];
		return $config;
	}

	public function getFollow_crm($buID,$arrAdmin=null,$pathImg=null,$itsMe=null){
		global $logDb;
	
		$folderCRM		= 'ERP_crm';
		$this->pathCRM 	= dirname(dirname(dirname(__FILE__))).'/'.$folderCRM;
		$this->pathImg 	= $pathImg;
		
		// require_once ($this->pathCRM."/function/nuy.class.php");
		require_once ($this->pathCRM."/function/ic_followcus.php");
		// require_once ($this->pathCRM."/function/general.php");
		require_once ($this->pathCRM."/inc/genMenu_ou.php");
		
		$gen = new genMenu();
		
		## เช็คสิทธิในการเข้าดูเมนู
		# P'Edit 2012-11-28 DEV10478 
		# ic_followcus2.php?operator=main_wait_for_approve
		$arrPage = array(
			'extra'=>'ic_followcus_show.php?func=cuslist',
			// 'extras'=>'ic_followcus_show_special.php?func=cuslist',
			'head'=>'ic_followcus2.php?operator=cuslist&listData=LINE&can_self_approve=1',
			// 'heads'=>'ic_followcus2.php?operator=cuslist&listData=LINE&can_self_approve=2'
		);
		# End P'Edit 2012-11-28 DEV10478
		
		if(count($arrAdmin)>0 && $arrAdmin){
			$chkAdmin = $gen->chkADMIN($arrAdmin);
		}
		
		if(!$chkAdmin){
			foreach($arrPage AS $key => $value){
				$gen = new genMenu();
				$chk[$key] = $gen->chkMenu_generalPage($buID,$this->checkTime,$value);
				if(!$chk['all'] && $chk[$key]){ $chk['all'] = true; }
			}
		}else{
			foreach($arrPage AS $key => $value){
				$chk[$key] = true;
				$chk['all'] = true;
			}
		}

		// echo "<pre>";
		// print_r($chk);
		// echo "</pre>";

		if($chk['all']){
			// นับ #ZAN@2012-03-01 เพิ่ม POSITION CODE แล้ว
			if($chk['extra'] || $chk['extras']){
				$_SESSION['numFollow'] = getNumFollowCusEventNew();
				// getNumFollowCusEvent(); //จะนับจำนวน $_SESSION[numFollow]และสุ่มรายการใหม่  //function/ic_followcus.php
			}
		
			// $emp_id_card 	= getEmp_follower($_SESSION['SESSION_ID_card']); //function/general.php
			$command_array = getCommandByPosition($_SESSION['SESSION_Position_id']);
			if(!empty($command_array[0])){ 
				$emp_id_card = implode(",",$command_array[0]); 
			}
			if(!empty($command_array[1])){ 
				$emp_pos_id = implode(",",$command_array[1]); 
			}
			
			if($emp_id_card){ 
				$whereCancel = "AND (process_by IN($emp_id_card) OR emp_id_card IN($emp_id_card))";
				
				$sql = "SELECT id FROM ".$this->dbName.".follow_customer WHERE status = '90' $whereCancel GROUP by cus_no,cancel_type";
				$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				
				$approve_num 	= mysql_num_rows($re);
				$numEmp 		= count(explode("','", $emp_id_card));
			}else{
				$approve_num 	= 0;
				$numEmp 		= 0;
			}
			
			if($_SESSION['numFollow'] || $approve_num){
				## หา place_id ของ place checktime
				$sqlPlace	= "SELECT id FROM ".$this->checkTime.".place WHERE path='".$folderCRM."' AND status!=99 LIMIT 1";
				$quePlace	= $logDb->queryAndLogSQL( $sqlPlace, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$fePlace	= mysql_fetch_assoc($quePlace);
				$placeID	= $fePlace['id'];
			
				$html .= '<style type="text/css">
						#framePopup{
							background-color: #EEEEEE;
							border: 1px solid #FF0000;
							border-radius: 5px;
							box-shadow: 0px 0px 3px #999999;
							bottom: 10px;
							padding: 0 10px;
							position: fixed;
							right: 10px;
							width: 380px;
							z-index: 100000000000;
							font-family: Tahoma,Helvetica,sans-serif;
							font-size: 12px;
						}

						.popupCRM #closePopup{
							float: left;
							width: 350px;
							color: #FF0000;
							margin: 6px 4px 6px 0;
							text-align: right;
						}

						.popupCRM #imgClosepop{
							float: right;
							width: 20px;
							height: 20px;
							margin: 6px 0;
							cursor: pointer;
						}

						.popupCRM .listPopup{
							float: left;
							width: 100%;
							color: #000;
							margin-bottom: 4px;

						}
						.popupCRM hr {
						    border-color: #999999 -moz-use-text-color -moz-use-text-color;
						    border-style: dashed none none;
						    border-width: 1px 0 0;
						    height: 1px;
						    margin-top: 12px;
						}
					</style>';
				$html .= '<div id="framePopup" class="popupCRM">';
				$html .= '<div id="closePopup"><span onclick="closePopup_crm();" style="cursor: pointer;">ปิดกล่องแจ้งเตือน</span></div>';
				$html .= '<div id="imgClosepop" onclick="closePopup_crm();"><img src="'.$this->pathImg.'images/closed.png" /></div>';
				
				if($chk['extra'] || $chk['extras']){
					$linkPopup	= "";
					$enJson		= "";
					$arrPath	= array();
					if($itsMe=='other'){

						$pathLink	= explode('?',$arrPage['extra']);

						$arrPath[0]['file']		= $pathLink[0];
						$arrPath[0]['condition']= $pathLink[1];
						$enJson					= json_encode($arrPath);
						$enJson					= str_replace('"','@C@',$enJson);
						$enJson					= str_replace('{','@E@',$enJson);
						$enJson					= str_replace('}','@B@',$enJson);
						$linkPopup				= str_replace('&','@D@',$enJson);
					}else{
						$pathLink	= $arrPage['extra'];
						$linkPopup				= $pathLink;
					}

					// echo "<br />",$linkPopup;

					foreach ($_SESSION['numFollow'] as $key => $value) {
						## รายชื่อลูกค้าที่ต้องโทรหา ic_followcus_show_special.php
						$html .= '<div class="listPopup" onclick="browseCheck_crm(\'main\',\''.$linkPopup.'&search=1&place_id='.$placeID.'\',\'\',function(){$(\'input#btn_search_cust\').click();});" style="cursor: pointer;">'.$value['event_title'].' <span id="numFollow" style="color: #FF0000;">'.$value['CNT_CUST'].'</span> รายการ</div>';
					}
				}
				
				if(($chk['head'] || $chk['heads']) && $approve_num > 0){
					$linkPopup	= "";
					$enJson		= "";
					$arrPath	= array();
					if($itsMe=='other'){
						if($chk['heads']){
							$pathLink 	= explode('?',$arrPage['heads']);
						}else{
							$pathLink	= explode('?',$arrPage['head']);
						}
					
						$arrPath[0]['file']		= $pathLink[0];
						$arrPath[0]['condition']= $pathLink[1];
						$enJson					= json_encode($arrPath);
						$enJson					= str_replace('"','@C@',$enJson);
						$enJson					= str_replace('{','@E@',$enJson);
						$enJson					= str_replace('}','@B@',$enJson);
						$linkPopup				= str_replace('&','@D@',$enJson);
					}else{
						if($chk['heads']){
							$pathLink 	= $arrPage['heads'];
						}else{
							$pathLink	= $arrPage['head'];
						}
					
						$linkPopup				= $pathLink;
					}

					// echo "<br />",$linkPopup;

					// ## รายการอนุมัติยกเลิกการติดตาม ic_followcus2.php
					$html .= '<div class="listPopup" onclick="browseCheck_crm(\'main\',\''.$linkPopup.'&head=1&place_id='.$placeID.'\',\'\',function(){browseCheck(\'follow_content\',\'ic_followcus2.php?operator=wait_for_approve\',\'\');hideObj(\'divMainDetail,cursor\');});" style="cursor: pointer;"><hr />รายการอนุมัติยกเลิกการติดตาม <span id="approve_num" style="color: #FF0000;">'.$approve_num.'</span> รายการ</div>';
				}
				
				$html .= '</div>';
				$html .= '<script type="text/javascript">
						function closePopup_crm(){
							jQuery(\'div#framePopup\').remove();
						};';
						
				if($itsMe=='other'){
					$html .= 'function browseCheck_crm(jshow,target,param,callback){
								// var cutTarget = target.split(\'?\');
								// var cutCon = cutTarget[1].split(\'=\');
								$.post("'.$this->pathImg.'index.php","checkPermissionPopup='.$arrPage['extra'].'",function(res){
									if (res === "Y") {
										window.location.href="'.$this->pathImg.'index.php?page="+target+"";
									}else {
										return;
									}
								});
								
							}';
				}else{
					$html .= 'function browseCheck_crm(jshow,target,param,callback){
								if (status !== false) {
									var opt;
									var selectors = "#"+jshow;
									$(selectors).empty();
									var strLoadingText = "<div style=\'text-align:center\' id=\'pre_loading\'><img src=\'images/lightbox-ico-loading.gif\'  style=\'padding-left:auto;padding-right:auto;\'/><br /><span style=\'font-size: 12px; font-weight: bold; color: #000;\'>Loading ...</span></div>"
									$(selectors).html(strLoadingText);
									$.ajax({
										url: target,
										context:document.body,
										data:param,
										success: function(resultHtml){	
											$(selectors).empty();
											$(selectors).html(resultHtml);
											$(\'body,html\').animate({scrollTop:0},200);   // set back to top away
											$(\'.back\').remove(); 						 // remove class = back from manu
											//$("body").css("overflow", "hidden");
										}
									}).done(function(){
										callback();
									});
								}
							}';
				}
				
				$html .= '</script>';
			}
		}
			
		return $html;
	}
}
?>