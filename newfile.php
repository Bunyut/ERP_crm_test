<?php
	case 'cuslist':
		## status ต้องเป็น 1 ส่วน emp_id_card จะเป็น ฝ่ายก็ได้

		//echo $_SESSION['SESSION_ID_card'];
		//$cusName = "IF(SUBSTR(cus_name,1,1) in ('เ','แ','ไ','ใ','โ'),CONCAT(SUBSTR(cus_name,2,1),SUBSTR(cus_name,1,1),SUBSTR(cus_name,3)),cus_name)";
		$cusName	= "CONVERT(cus_name USING TIS620)";

		//pt@2012-01-26  กำหนดตัวแปรที่ส่งมา
		if($_GET['order']) $_POST['order'] = $_GET['order'];
		if($_GET['order_event']) $_POST['order_event'] = $_GET['order_event'];
		if($_GET['order_province']) $_POST['order_province'] = $_GET['order_province'];
		if($_GET['order_aumphur']) $_POST['order_aumphur'] = $_GET['order_aumphur'];
		if($_GET['order_tumbon']) $_POST['order_tumbon'] = $_GET['order_tumbon'];
		//-

		if ($_POST['order']) { //การจัดเรียง
			$_SESSION["SESSION_order"] = $_POST['order'];
		}
		switch ($_SESSION["SESSION_order"]) {
			case "cusname_asc":
				$order = "$cusName ,stop_date";
				$select1 = "selected='selected'";
				break;
			case "cusname_desc":
				$order = "$cusName DESC,stop_date";
				$select2 = "selected='selected'";
				break;
			case "expire_date_asc":
				$order = "stop_date,$cusName";
				$select3 = "selected='selected'";
				break;
			case "expire_date_desc":
				$order = "stop_date DESC,$cusName";
				$select4 = "selected='selected'";
				break;
			default:
				$order = "stop_date,$cusName";
				break; //default = เรียงตามชื่อลูกค้า ก - ฮ
		}
		$onchange = "followPostData('main','$followPage?operator=cuslist','order='+document.getElementById('order').value + '&order_event='+document.getElementById('order_event').value + '&order_province='+document.getElementById('order_province').value + '&order_aumphur='+document.getElementById('order_aumphur').value + '&order_tumbon='+document.getElementById('order_tumbon').value );";
		$onchange_pro = "followPostData('main','$followPage?operator=cuslist','order='+document.getElementById('order').value + '&order_event='+document.getElementById('order_event').value + '&order_province='+document.getElementById('order_province').value );";
		$onclick_excel = "javascript:window.open('$followPage?operator=cuslist&cuslist_excel=1&order='+document.getElementById('order').value + '&order_event='+document.getElementById('order_event').value + '&order_province='+document.getElementById('order_province').value + '&order_aumphur='+document.getElementById('order_aumphur').value + '&order_tumbon='+document.getElementById('order_tumbon').value );";
		//	$onchange = "followPostData('main','$followPage?operator=cuslist','order='+document.getElementById('order').value);";
		$id_card = $_GET['id_card'];
		if ($id_card) { //ถ้าส่งค่ามาจากหน้ารายชื่อพนักงานของ Super Manager Admin
			$emp = select_emp_nametonickname($id_card);
			$back_display = '';
			if ($_GET['back']) { //ถ้าส่งมาจากหน้า รายชื่อพนักงานในความรับผิดชอบ จะเก็บ session ใหม่
				$_SESSION['back_emplist'] = $_GET['back'];
				$_SESSION['anchor_emplist'] = $_GET['anchor'];
			}
			
			$empOu = select_empOu($id_card); //function/general.php
			
			$emp_response 	= "<div align='left' style='width:100%;padding:10px;color:green;background:#ffffff' class='title3'>";
			$emp_response	.= "รายการที่กำลังติดตามอยู่ในขณะนี้ของ ::  <font color='orangered'>$emp</font> :: [พนักงานผู้ดูแลลูกค้า] <a href=\"javascript:void(0)\" class='nz_button BTorange' style='color:#ffffff' onclick=\"javascript:browseCheck('follow_content','$followPage?operator=getAllCustomer&id_card=$id_card&mode=forward&back=$followPage?operator=cuslist','');\">รายชื่อลูกค้าทั้งหมด</a> </div>";
			$backID 		= "id_card=".$id_card;
			$onchange 		= "followPostData('main','$followPage?operator=cuslist&$backID','order='+document.getElementById('order').value);";
			$leader 		= "leader=yes";
			//$check_super_enter = true;
		}
	
		if ($id_card == "") { //ถ้าเข้าดูรายการของตัวเอง
			//$check_super_enter 	= false;
			if($_GET['select_idEmp']){//pt@2012-01-04
				$id_card = parseInt($_GET['select_idEmp']);
				$empOu = select_empOu($id_card); //function/general.php
			}else{
				$id_card = $_SESSION['SESSION_ID_card'];
				$empOu = select_empOu($id_card,'session'); //function/general.php
			}
	
			//end pt@2012-01-04
			$back_display = 'none';
		}
	
		# Check CR
		//if( $checkEventDepartment == $config['cr_department'] ){
		//เข้าฟังชั่น เรียงเงื่อนไข select ข้อมูลที่จะแสดง
		$whereOu = condition_sortOU($empOu,'cus'); //function/general.php
	
			//ดึงมาเฉพาะเหตุการณ์รอสัมภาษณ์ของ CR pattern_id_ref ต้องเป็นค่าว่าง
			//$sql = "SELECT id,event_title FROM $config[db_base_name].follow_main_event
				//WHERE (department='".$checkEventDepartment."' ) AND pattern_id_ref != '' AND status != '99' AND visible != 'no' ";
			$sql = "SELECT event.id,event.event_title FROM $config[db_base_name].follow_main_event event LEFT JOIN $config[db_base_name].follow_customer cus ON ";
			$sql .= "event.id=cus.event_id_ref WHERE $whereOu AND event.status!= '99' AND event.visible != 'no' AND cus.status!= '99' ";
			$sql .= "GROUP BY event.id ORDER BY event.event_title ASC";
			$arr_opt = array(
					'value_field'=>'id', 						// ฟิลด์ที่จะใช้เป็น value ของ option
					'text_field'=>'event_title',				// ฟิลด์ที่จะใช้เป็น text ของ option
					'select_value'=>$_GET['select_event'],		// ตัวเลือกเริ่มต้น
			);
			// pt 2011-11-25
			$event_dropdown = get_event_dropdown($sql, $arr_opt );
			$select_event_cr = <<<DROPDOWN
			<b><font color="red">เลือกกิจกรรม ::</font></b>
			<select id="s_event" style="font-size:14px"
					onchange="ic_event_cus_krong()">
					$event_dropdown
			</select>
DROPDOWN;

			//สาขา ให้ CR เลือก
			$sql_b = "SELECT id, name FROM $config[Member].working_company WHERE status != '99' AND name != ''";
			$result_b = mysql_query($sql_b)or die('ERROR '.mysql_error());
			$branch_cr = '<option class="textCenter" value="all"> เลือกสาขาที่ต้องการ </option>';
			while($fe_b = mysql_fetch_assoc($result_b)){
				if($_GET['select_branch']==$fe_b['id'])		$branch_cr .= '<option class="textCenter" SELECTED value="'.$fe_b[id].'"> '.$fe_b[name].' </option>';
				else 	$branch_cr .= '<option class="textCenter" value="'.$fe_b[id].'"> '.$fe_b[name].' </option>';
			}
			$select_branch_cr = "<b><font color=\"red\">เลือกสาขาที่ต้องการ ::</font></b>
			<select id=\"s_branch\" class = \"textCenter\" style=\"font-size:14px\"
			onchange=\"ic_event_cus_krong()\">
			$branch_cr
			</select>";
		//}
		//end CR
		if($_GET['select_event']){//กรองชื่อลูกค้าตาม Event
			//pt@2012-01-04
			//$other_id_card = "";
			//$team_id_card = get_emp_saleTemp($_SESSION['SESSION_ID_card'], 'id_card');
			//if($team_id_card) $other_id_card =  ",".$team_id_card;
	
			//ถ้าเป็นกิจหรรมพิเศษ จะ แสดงให้เลือกทำรายการให้ พนักงาน
			$team_id_card = get_emp_saleTemp($_SESSION['SESSION_ID_card'], 'id_card');
			$team_id_card = "'".$_SESSION['SESSION_ID_card']."',".$team_id_card;
			$expSaleTemp = explode(",",$team_id_card);
			$option_list_emp = '';
			for($i=0;$i<count($expSaleTemp);$i++){
				$idSaleTemp =  str_replace("'", '' ,$expSaleTemp[$i]);
				$sql_info = "SELECT Name , Surname , Nickname FROM $config[db_emp].emp_data WHERE ID_card = '$idSaleTemp'";
				$res_info = mysql_query($sql_info)or die('error $followPage Line:'.__LINE__);
				$fet_info = mysql_fetch_assoc($res_info);
	
				$sled = '';
				if($idSaleTemp == $id_card)	$sled = ' selected="selected" ';
	
				$option_list_emp .= '<option value="'.$idSaleTemp.'" '.$sled.'> '.$fet_info[Name].' '.$fet_info[Surname].' ('.$fet_info[Nickname].') </option>';
			}
			$other_where = " AND follow_customer.event_id_ref = '$_GET[select_event]' ";
			if($_GET['select_event']=='62'){ //สำหรับเหตุการเฉพาะ
				$fix_event = "fix_event=".$_GET['select_event'] ;
			}
	
			//end pt@2012-01-04
		}
		//กรองตามชื่อสาขา
		if($_GET['select_branch']){//กรองชื่อลูกค้าตาม Event
			//if($_GET['select_branch']=='all'){$_GET['select_branch']='%';}
			if($_GET['select_branch']!='all'){
				$joinSell_b = "JOIN $config[db_easysale].Sell ON Sell.Chassi_No_S = follow_customer.chassi_no_s ";
				$other_where_b = " AND Sell.sell_branch LIKE '$_GET[select_branch]' ";
			}
		}
	
		//pt@2012-01-26
		$other_where_event = '';
		if($_POST['order_event']){
			$other_where_event .= " AND follow_customer.event_id_ref = '$_POST[order_event]'";
		}
		
		$today = date('Y-m-d');
		$tb_follow = "$config[db_base_name].follow_customer";
		$tb_event = "$config[db_base_name].follow_main_event";
		
		//เข้าฟังชั่น เรียงเงื่อนไข select ข้อมูลที่จะแสดง
		$whereOu = condition_sortOU($empOu,$tb_follow); //function/general.php

		$sql = "SELECT $tb_follow.id AS follow_id,event_id_ref,$tb_follow.follow_type,follow_customer.chassi_no_s,cus_no,cus_name,emp_id_card,";
		$sql .= "MIN(stop_date) AS stop_date, $tb_follow.status AS follow_status, process_by,$tb_event.event_title,$tb_event.stop_value, $tb_event.stop_unit ";
		$sql .= "FROM $tb_follow LEFT JOIN $tb_event ON $tb_follow.event_id_ref=$tb_event.id  $joinSell_b";
		//$sql .= "WHERE emp_id_card IN('$id_card','$checkEventDepartment') $other_where $other_where_b $other_where_event AND ";
		$sql .= "WHERE $whereOu $other_where $other_where_b $other_where_event AND (start_date <= '$today' AND stop_date >= '$today') AND ";
		$sql .= "$tb_follow.status NOT IN('5','99') AND $tb_event.visible!='no' GROUP by cus_no ";
		//เพราะมีการดึงวันที่หมดอายุด้วยคำสั่ง MIN() หากไม่ group จะแสดงเรคอร์ดเดียว
		// end pt 2011-11-25

		//สร้างแถบแสดงรายการแบบแบ่งหน้า  ต้องสร้างตัวแปร $config[] และ สไตล์ชีท .select , .nonselect
		// $config['page_limit'] = 20; //รายการที่ต้องการแสดงต่อหนึ่งหน้า
		// $config['page_number'] 	= 10; //จำนวนหมายเลขหน้า ถ้าเกินที่กำหนดจะแสดง หน้าถัดไป
		// $onclick['div'] = "main"; //เลเยอร์ที่ต้องการแสดงผล
		// $onclick['url'] = "$followPage?operator=cuslist&$backID"; //url เมื่อคลิกเลขหน้า
		// $val = createPageView($sql,$_GET['pageNumber'],$onclick);//return ค่ากลับเป็น อาร์เรย์
		// $pager = $val['pageview']; //เอาไปวางจุดที่ต้องการแสดงหมายเลขหน้า
		// $goto = $val['goto']; // ค่าเริ่มต้น query ข้อมูล
		// $limit = $config['page_limit']; //จำนวนรายการที่ต้องการ

		$sql .= "ORDER by $order ";

		//$sql .= "LIMIT $goto,$limit";
		//echo '<br/>'.$sql.'<br/>';

		$cls[1] = "background-color: #C1FFC1;";
		$cls[2] = "background-color: #B4EEB4;";
		$clsToday[1] = "background-color:#FFCC99;";
		$clsToday[2] = "background-color:#F89A3D;";
		$clsTomorrow[1] = "background-color:#F7F6BE";
		$clsTomorrow[2] = "background-color:#F8F791";
		/*$cls[1] = "class='event_tr1'";
		$cls[2] = "class='event_tr2'";
		$clsToday[1] = "class='event_trHilight1_1'";
		$clsToday[2] = "class='event_trHilight1_2'";
		$clsTomorrow[1] = "class='event_trHilight2_1'";
		$clsTomorrow[2] = "class='event_trHilight2_2'";*/
		//$border = "border-top : 1px solid #828282; text-align: center;";
		$a = 0;
		mysql_query('SET NAMES UTF8');
		$follow = mysql_query($sql) or die(show_sql_error($sql,__FILE__,$line=__LINE__));
		while ($rs = mysql_fetch_assoc($follow)) {
			$border = '';
			
			/*		if($rs['cus_no']!=$prevCusno){ //ถ้า cus_no เปลี่ยน ให้เปลี่ยนสีพื้นหลัง
			 //$border = "border-top : 2px solid #4F4F4F;";
			 $a++;
			 if($a>2){$a=1;}
			 }
			 $prevCusno = $rs['cus_no'];
			 $class = $cls[$a];
			 */
			$a++;
			if ($a > 2) {
				$a = 1;
			}

			$today = date("Y-m-d");
			$numDiffDate = numDiffDate($today, $rs['stop_date']);
			$TRtitle = "";
			if ($rs['stop_date'] == $today) { //ถ้าวันสุดท้ายตรงกับวันนี้
				$class = $clsToday[$a];
				$TRtitle = "จะต้องทำภายในวันนี้แล้วนะครับ";
			} else if ($numDiffDate == 1) {
				$class = $clsTomorrow[$a];
				$TRtitle = "วันพรุ่งนี้เป็นวันสุดท้ายแล้วนะครับ";
			} else {
				$class = $cls[$a];
			}

			$follow_id = $rs['follow_id'];
			$cus_no = $rs['cus_no'];
			$cus_name = $rs['cus_name'];
			$event_title = $rs['event_title'];
			$event_id = $rs['event_id_ref'];
			$chassi_no_s = $rs['chassi_no_s'];

			//วันที่ติดตามจริง เท่ากับ วันสุดท้าย - ค่าที่บวกเพิ่ม อ้างอิงจาก follow_main_event
			$real = getBeforeDate($rs['stop_date'], $rs['stop_value'], $rs['stop_unit']);
			$real_date = standard2thaidate($real, '-');
			$real_date = "<div title='$real_date[text]'>$real_date[number]</div>"; //วันที่ครบกำหนดของเหตุการณ์

			$expire = standard2thaidate($rs['stop_date'], '-');
			$expire_date = "<div title='$expire[text]'>$expire[number]</div>"; //วันที่เริ่มติดต่อ
			
			//ข้อมูลลูกค้า
			$cusArr = selectDB("$config[db_maincus].MAIN_CUS_GINFO","Time_for_talk","CusNo='$cus_no' LIMIT 1");
			//หาไอดีประเภทลูกค้า
			$typeID = selectDB("$config[db_maincus].MIAN_CUS_TYPE_REF","CTYPE_GRADE_REF","CTYPE_REF_CUSNO='$cus_no' LIMIT 1");
			//ประเภทลูกค้า
			$cusType = selectDB("$config[db_maincus].AMIAN_CUS_TYPE","CUS_TYPE_LEVEL_DESC","CUS_TYPE_ID='".$typeID['CTYPE_GRADE_REF']."' LIMIT 1");
			//เบอร์มือถือ
			$telMobile = selectDB("$config[db_maincus].MAIN_TELEPHONE","TEL_NUM","TEL_CUS_NO='$cus_no' AND TEL_TYPE='1' LIMIT 1");
			//เบอร์บ้าน
			$telHome = selectDB("$config[db_maincus].MAIN_TELEPHONE","TEL_NUM","TEL_CUS_NO='$cus_no' AND TEL_TYPE='3' LIMIT 1");
			//เบอร์ที่ทำงาน
			$telWork = selectDB("$config[db_maincus].MAIN_TELEPHONE","TEL_NUM","TEL_CUS_NO='$cus_no' AND TEL_TYPE='2' LIMIT 1");

			$cus_info = "<a href='javascript:void(0);' onclick=\"NewWindow('$followPage?operator=getMainCusData&amp;cus_no=$cus_no');\"><img src='img/directory.gif' border='0' title='ดูข้อมูลประวัติส่วนตัวลูกค้า'></a>";
			$cus_level = $cusType['CUS_TYPE_LEVEL_DESC']; // เกรดของลูกค้า
			$pleasure_value = $cusArr["Pleasure_value"];
			$cus_telnumber = checkNull($telMobile['TEL_NUM'])." / ".checkNull($telHome['TEL_NUM'])." / ".checkNull($telWork['TEL_NUM']);
			$time_for_talk = $cusArr["Time_for_talk"];

			/*
			 $followdata = "follow_cus_id=$follow_id&follow_type=$rs[follow_type]&chassi_no_s=$chassi_no_s&cus_no=$cus_no&cus_name=$rs[cus_name]&start_date=$rs[start_date]&stop_date=$rs[stop_date]&status=$rs[follow_status]&pleasure_value=$pleasure_value";

			 $url1 = "$followPage?operator=getEventScript&event_id=$event_id&event_title=$event_title&$followdata";
			 $url2 = "$followPage?operator=getEventQuestion&event_id=$event_id&event_title=$event_title&$followdata";
			 $break_up_th = "";$break_up_td = "";$break = "";
			 if($check_super_enter == true && ($_SESSION["Super_Level"] == "Supervisor" || $_SESSION["Super_Level"] == "BigSupervisor")){
			 $url2 		 = "";//ถ้าเป็นหัวหน้าจะไม่เห็นแบบฟอร์มคำถาม
			 $break_up_th.= "<th><img src='img/break_up.png' width='22' height='22' title='ยุติการติดตาม'></th>";
			 $break_up_td = "<td width='25' style='$border' id='break_$follow_id'><div style='position:relative;z-index:1px'><a href=\"javascript:void(0)\" onclick=\"javascript:if(confirm('ต้องการยกเลิกการติดตามเหตุการณ์ $event_title ของคุณ $cus_name หรือไม่?')==true){browseCheck('break_$follow_id','$followPage?operator=break_up&follow_id=$follow_id&event_title=$event_title&cus_name=$cus_name','');}breake = true;\"><img src='img/break_up.png' width='25' height='25' title='ยุติการติดตามรายการนี้'></a></div></td>";
			 $break = true;
			 }
			 $click = "showQuestionDetail('$url1','$url2','TR$follow_id');breake=false;changeObject('bt_script','div_script');";
			 */
			//$TRonclick = "follow_toggle('detail$cus_no','$followPage?operator=showCusEvent&id_card=$id_card&cus_no=$cus_no&$leader&$fix_event','$cus_no')";
			$TRonclick = "follow_toggle('detail$cus_no','$followPage?operator=showCusEvent&id_card=$id_card&cus_no=$cus_no&$leader&$fix_event&empId=$rs[emp_id_card]','$cus_no')";
			
			//check ว่ามีคนจองไว้หรือยัง
			$cr_book = '';
			//if( $checkEventDepartment == $config['cr_department'] && strlen($rs['process_by'])==13 && abs($rs['process_by']) ){
			if(!$rs['emp_id_card'] && strlen($rs['process_by'])==13 && abs($rs['process_by']) ){
				$bgColor = "yellow";
				$booking_name = select_emp_nametonickname($rs['process_by']);
				$title	= "จองไว้โดยคุณ ".$booking_name;
				if($_SESSION['SESSION_ID_card'] != $rs['process_by']){
					$TRonclick	= "alert('ผู้ที่จองไว้ ต้องทำการยกเลิกการจองก่อน คุณจึงจะสามารถทำรายการนี้ได้ครับ');";
					$booking  = "<img src='img/process_by.png'>จองไว้โดย :: <font color='black'>$booking_name</font>";
				}else if($_SESSION['SESSION_ID_card'] == $rs['process_by']) {
					$continue = $TRonclick;
					$TRonclick = "";
					$cancel = "<img src='img/cancel_process.png' align='absbottom' style='margin-top:2px;' title='ยกเลิกการจอง'
							onclick=\"cancel_cr_process('$cus_no','1','$id_card','$checkEventDepartment');\">";
					$booking = "<img src='img/process_by.png'>คุณได้จองไว้แล้ว คลิกที่นี่ เพื่อทำรายการต่อ";
				}
				$cr_book = "<div  id='book$cus_no'><span style='border:1px solid orange;background-color:$bgColor;' onclick=\"$continue\">
				$booking
					</span>
					$cancel</div>";
			}
			$click = $TRonclick;

			if($_GET['cuslist_excel']){ //ถ้ากดโหลดไฟล์ จะเป็นอีกเทมเพลตนึง
				//บัตรปชช
				$addCard = selectDB("$config[db_maincus].MAIN_ADDRESS","substring_index(ADDR_SUB_DISTRICT,'_',-1) AS C_Tum,
				substring_index(ADDR_DISTRICT,'_',-1) AS C_Amp,substring_index(ADDR_PROVINCE,'_',-1) AS C_Pro","ADDR_CUS_NO='$cus_no' AND ADDR_TYPE='1' LIMIT 1");
				
				if(!$addCard['C_Tum']){
					//เอกสาร
					$addCard = selectDB("$config[db_maincus].MAIN_ADDRESS","substring_index(ADDR_SUB_DISTRICT,'_',-1) AS C_Tum,
					substring_index(ADDR_DISTRICT,'_',-1) AS C_Amp,substring_index(ADDR_PROVINCE,'_',-1) AS C_Pro","ADDR_CUS_NO='$cus_no' AND ADDR_TYPE='2' LIMIT 1");
					
					if(!$addCard['C_Tum']){
						//ที่ทำงาน
						$addCard = selectDB("$config[db_maincus].MAIN_ADDRESS","substring_index(ADDR_SUB_DISTRICT,'_',-1) AS C_Tum,
						substring_index(ADDR_DISTRICT,'_',-1) AS C_Amp,substring_index(ADDR_PROVINCE,'_',-1) AS C_Pro","ADDR_CUS_NO='$cus_no' AND ADDR_TYPE='3' LIMIT 1");
					}
				}
				
				$cus_Pro = $addCard['C_Pro']; $cus_Amp = $addCard['C_Amp']; $cus_Tum = $addCard['C_Tum'];
				
				$cus_list .= $tpl->tbHtml($followHTML, 'CUS_LIST_FOR_EXPORT_WITH_ADDRESS');
			}else{
				$cus_list .= $tpl->tbHtml($followHTML, 'CUS_LIST');
			}
		} //while loop

		if($_GET['select_event']){ //pt@2012-01-04
			$styleHidden = '';
		}else{
			$styleHidden = 'none';
		}
		if ($cus_list == "") {
			$colspan = 0;
			$cus_list = $tpl->tbHtml($followHTML, 'NULL');
		}

		//if($check_super_enter == true){
		$break_up_menu = "<img src='img/break_up2.png' align='absmiddle' width='25' height='25' title='ยุติการติดตาม'> = ยกเลิกรายการติดตาม |";
		//}

		$popup_block = $tpl->tbHtml($followHTML, 'POPUP_BLOCK');


		//ถ้าเป็นหน้าโทรหาลูกค้าปกติ ถึงจะ โชว์แถบกรอง เช่นถ้าเป็นหน้ากิจกรรมพิเศษ จะส่งรหัสกิจกรรมมาโดยเฉาะ ก็จะไม่แสดงแถบกรอง
		if($_GET['select_event']){
			$fornormalmode = 'none'; $textEvent = $textPro = $textAmp = $textTum = '';
		}
		else{
			$fornormalmode = '';
			$textEvent = 'กิจกรรม: ';
			$textPro = 'จังหวัด: ';
			$textAmp = 'อำเภอ: ';
			$textTum = 'ตำบล: ';
			$textEx = 'ออกรายงาน: ';
		}

		//เลือกจังหวัด
		$op_province = GETProvince($_POST['order_province'],null);

		//เลือกอำเภอ
		if($_POST['order_province'])
		$op_aumphur = GETAmphur($_POST['order_province'],$_POST['order_aumphur']);

		//เลือกตำบล
		if($_POST['order_province'] && $_POST['order_aumphur'])
		$op_tumbon = GETDistrict($_POST['order_province'],$_POST['order_aumphur'],$_POST['order_tumbon']);

		//เข้าฟังชั่น เรียงเงื่อนไข select ข้อมูลที่จะแสดง
		//$whereOu = condition_sortOU($empOu,$tb_follow); //function/general.php
		
		//เลือกEvent
		$sql_e = " SELECT  $tb_event.event_title, $tb_event.id";
		$sql_e .= " FROM $tb_follow LEFT JOIN $tb_event ON $tb_follow.event_id_ref=$tb_event.id  $joinSell_b";
		$sql_e .= " WHERE $whereOu $other_where $other_where_b AND ";
		$sql_e .= "(start_date <= '$today' AND stop_date >= '$today') AND $tb_follow.status ='1' AND $tb_event.visible!='no' GROUP by event_id_ref";
		$res_e = mysql_query($sql_e) or die(show_sql_error($sql_e,__FILE__,$line=__LINE__));
		while($fet_e = mysql_fetch_assoc($res_e)){
			if($_POST['order_event'] == $fet_e['id']) $selected_event = 'selected';
			else $selected_event = '';
			$GETEvent .='<option '.$selected_event.' value="'.$fet_e['id'] .'" >'.$fet_e['event_title'] .'</option>';
		}

		if($_GET['cuslist_excel']){ //ถ้ากดโหลดไฟล์ จะเป็นอีกเทมเพลตนึง
			header("Content-Type: application/vnd.ms-excel");
			header('Content-Disposition: attachment; filename="'.'รายชื่อลูกค้าที่ต้องโทรหา'.date('j_F_Y').'.xls"');#ชื่อไฟล์
			$html = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"
			xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv="Content-type" content="text/html;charset=UTF-8"></head><body>';
			$html .= $tpl->tbHtml($followHTML, 'TABLE_FOR_EXPORT_WITH_ADDRESS');
			$html .= '</body></html>';
			echo $html;
			exit();

		}else{
			echo $tpl->tbHtml($followHTML, 'MAIN_FOLLOW');
		}
		
	break;
?>