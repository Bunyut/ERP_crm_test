﻿DELIMITER $$
 
DROP  FUNCTION  IF  EXISTS  `calEventFollow`$$

	CREATE  FUNCTION  `calEventFollow`(
		beginDate VARCHAR(10), finishDate VARCHAR(10), plus INT(4), unitD VARCHAR(200), stop INT(4), unitS VARCHAR(200)
	) RETURNS VARCHAR(10)
    DETERMINISTIC
	BEGIN
		DECLARE res VARCHAR(10) DEFAULT ''; # วันที่ return กลับ
		# DECLARE lastD INT(2) DEFAULT 30; # วันที่สุดท้ายของเดือน
		DECLARE diff INT(20) DEFAULT 0; # เป็นจำนวนวัน
		DECLARE space INT(20) DEFAULT 0; # ความห่าง อาจเป็น ปี เดือน หรือ วัน
		DECLARE multiply INT(20) DEFAULT 0; # วันที่เพิ่มจากการคำนวณ
		DECLARE dateDefault INT(2) DEFAULT 0; # วันที่เพิ่มจากการคำนวณ
		
		SET unitS			= LOWER(unitS);
		
		# ถ้ามีวันที่สิ้นสุด
		IF stop > 0 THEN
			CASE unitS
				# ถ้าเป็นปี
				WHEN 'year' THEN
					SET finishDate 	= DATE_FORMAT(DATE_ADD(finishDate, INTERVAL stop YEAR), '%Y-%m-%d');
				# ถ้าเป็นเดือน
				WHEN 'month' THEN
					SET finishDate 	= DATE_FORMAT(DATE_ADD(finishDate, INTERVAL stop MONTH), '%Y-%m-%d');
				# ถ้าเป็นสัปดาห์
				WHEN 'week' THEN
					SET stop	= ROUND(stop * 7);
					SET finishDate 	= DATE_FORMAT(DATE_ADD(finishDate, INTERVAL stop DAY), '%Y-%m-%d');
				# ถ้าเป็นวัน
				WHEN 'day' THEN
					SET finishDate 	= DATE_FORMAT(DATE_ADD(finishDate, INTERVAL stop DAY), '%Y-%m-%d');
			END CASE;
		END IF;
		
		SET res 			= beginDate;
		SET diff 			= DATEDIFF(finishDate, beginDate);
		SET unitD			= LOWER(unitD);
		
		CASE unitD
			# ถ้าเป็นปี
			WHEN 'year' THEN
				SET space		= FLOOR(diff / 365); # จำนวนปีที่ห่างกัน ปัดลงตลอด
				SET multiply	= FLOOR(FLOOR(space / plus) * plus); # ต้อง คูณ plus จำนวนกี่ครั้ง
				SET res			= DATE_FORMAT(DATE_ADD(res, INTERVAL multiply YEAR), '%Y-%m-%d'); # คำนวณวันทีเพิ่ม
				
				# บวกแล้วไม่น้อยกว่าวันที่เช็ค
				IF res < finishDate THEN
					WHILE res  <= finishDate DO
						SET res 	= DATE_FORMAT(DATE_ADD(res, INTERVAL plus YEAR), '%Y-%m-%d');
					END WHILE;
					
					SET res 		= DATE_FORMAT(DATE_SUB(res, INTERVAL plus YEAR), '%Y-%m-%d');
				END IF;
			# ถ้าเป็นเดือน
			WHEN 'month' THEN
				SET dateDefault		= DAYOFMONTH(beginDate); # วันที่เริ่มต้น
				SET space			= FLOOR(diff / 31); # จำนวนที่ห่างกัน ปัดลงตลอด (ใช้ 31 เพื่อ ไม่ให้ + แล้วเกิน)
				SET multiply		= FLOOR(FLOOR(space / plus) * plus); # ต้อง คูณ plus จำนวนกี่ครั้ง
				SET res				= DATE_FORMAT(DATE_ADD(res, INTERVAL multiply MONTH), '%Y-%m-%d'); # คำนวณวันทีเพิ่ม
				
				# บวกแล้วไม่น้อยกว่าวันที่เช็ค
				IF res < finishDate THEN
					WHILE res  <= finishDate DO
						# SET lastD	= DAYOFMONTH(LAST_DAY(res)); # วันที่ ที่ตั้งไว้
						SET res 	= DATE_FORMAT(DATE_ADD(res, INTERVAL plus MONTH), '%Y-%m-%d');
					END WHILE;
					
					SET res			= DATE_SUB(res, INTERVAL plus MONTH);
					
					IF dateDefault > DAYOFMONTH(LAST_DAY(res)) THEN
						SET res 	= DATE_FORMAT(res, '%Y-%m-%d');
					ELSE
						SET res 	= DATE_FORMAT(CONCAT(DATE_FORMAT(res, '%Y-%m'),'-',dateDefault), '%Y-%m-%d');
					END IF;
				ELSE
					IF dateDefault > DAYOFMONTH(LAST_DAY(res)) THEN
						SET res 	= DATE_FORMAT(res, '%Y-%m-%d');
					ELSE
						SET res 	= DATE_FORMAT(CONCAT(DATE_FORMAT(res, '%Y-%m'),'-',dateDefault), '%Y-%m-%d');
					END IF;
				END IF;
			# ถ้าเป็นสัปดาห์
			WHEN 'week' THEN
				SET plus		= ROUND(plus * 7); # จำนวนวัน ต่อ plus อาทิตย์
				SET space		= FLOOR(diff); # จำนวนปีที่ห่างกัน ปัดลงตลอด
				SET multiply	= FLOOR(FLOOR(space / plus) * plus); # ต้อง คูณ plus จำนวนกี่ครั้ง
				SET res			= DATE_FORMAT(DATE_ADD(res, INTERVAL multiply DAY), '%Y-%m-%d'); # คำนวณวันทีเพิ่ม
				
				# บวกแล้วไม่น้อยกว่าวันที่เช็ค
				IF res < finishDate THEN
					WHILE res  <= finishDate DO
						SET res 	= DATE_FORMAT(DATE_ADD(res, INTERVAL plus DAY), '%Y-%m-%d');
					END WHILE;
					
					SET res 		= DATE_FORMAT(DATE_SUB(res, INTERVAL plus DAY), '%Y-%m-%d');
				END IF;
			# ถ้าเป็นวัน
			WHEN 'day' THEN
				SET space		= FLOOR(diff); # จำนวนปีที่ห่างกัน ปัดลงตลอด
				SET multiply	= FLOOR(FLOOR(space / plus) * plus); # ต้อง คูณ plus จำนวนกี่ครั้ง
				SET res			= DATE_FORMAT(DATE_ADD(res, INTERVAL multiply DAY), '%Y-%m-%d'); # คำนวณวันทีเพิ่ม
				
				# บวกแล้วไม่น้อยกว่าวันที่เช็ค
				IF res < finishDate THEN
					WHILE res  <= finishDate DO
						SET res 	= DATE_FORMAT(DATE_ADD(res, INTERVAL plus DAY), '%Y-%m-%d');
					END WHILE;
					
					SET res 		= DATE_FORMAT(DATE_SUB(res, INTERVAL plus DAY), '%Y-%m-%d');
				END IF;
		END CASE;
		
		RETURN res;
	END$$
 
DELIMITER ;