<?php
require_once("config/general.php");
require_once("function/general.php");
require_once("function/resperson_change.php");
require_once("function/ic_followcus.php");
// require_once("function/nuy.class.php");

$thisPage = "resperson_change";

/****************************
* css = css/followcus.css   *
* 	    css/nz_button.css	 *
* js  = java/followcus.js   *
****************************/

## สั่งทำงานโค้ด
$doCode = ""; 		## ทำงาน
// $doCode = "No";  ## ไม่ทำงาน แสดงโค้ด Insert, Update, Delete
// $doCode = "All"; ## ทำงาน แสดงโค้ด Select, Insert, Update, Delete

$objMain	= new manageMainDB();

if(isset($_GET['can_self_approve'])){
	$_SESSION['resperson_self_approve'] = $_GET['can_self_approve'];
}

$opt	= $_REQUEST['opt'];

Conn2DB();

## แบบเห็นพนักงาน ทั้งหมด
if($_SESSION['resperson_self_approve']=='All'){
	## หาพนักงาน ทั้งหมด ที่ต้องดูแลลูกค้า
	# function/genearl.php
	$arrGetRes			= array();
	$arrGetRes['out']	= 'Y'; ## Y แสดงคนที่ออก, N หรือค่าว่าง ไม่แสดงคนที่ออก
	$command_array		= getAllEmpResperson($arrGetRes);
## แบบเห็นพนักงาน ตามสายงาน
}else{
	# BEN Add 2013-02-06 DEV10534 
	# == กรณี จะแยกเป็นสองหน้า เห็นผู้ล็อกอินเอง แยกโดย ใส่ parameter ตัวที่สอง ให้กับฟังก์ชั่น getCommandByPosition 
	#== สถานะ ดังนี้  1 ไม่แสดงข้อมูลผู้ล็อกอิน 2 แสดงข้อมูลผู้ล็อกอิน
	# function/ic_followcus.php
	$command_array 		= getCommandByPosition($_SESSION['SESSION_Position_id'],$_SESSION['resperson_self_approve']);
}

/* echo "<pre>";
print_r($command_array);
echo "</pre>"; */

if(!empty($command_array[0])){ $emp_id_card = implode(",",$command_array[0]); }else{ $emp_id_card = ""; }
if(!empty($command_array[1])){ $emp_pos_id 	= implode(",",$command_array[1]); }else{ $emp_pos_id = ""; }

$whereEmp		= "";
## แบบเห็นพนักงาน ทั้งหมด
if($_SESSION['resperson_self_approve']=='All'){
	$whereEmp	= "";
## แบบเห็นพนักงาน ตามสายงาน
}else{
	$whereEmp	= "&where=emp_data.ID_card IN(".$emp_id_card.")";
}

switch($opt){
	case 'changeForm':
		$old_id_card 		= $_GET['id_card'];
		$old_position_code  = $_GET['position_code'];
		$whereSearch 		= "";
		$popupWidth			= "width:1200px;";
		
		if($_POST['search']!='Y'){
			$config['change_descrip']  = 'เป็นผู้รับผิดชอบลูกค้า';
			$config['change_descrip2'] = 'จำนวนลูกค้าที่รับผิดชอบ';

			$arr['id_card']	= $old_id_card;
			$arr['Being']	= "";
			$feEmp	= $objMain->getEmpData($arr);
			
			$saleName	= "";
			if($feEmp['Be']){ $saleName .= $feEmp['Be']." "; }
			if($feEmp['Name']){ $saleName .= $feEmp['Name']." "; }
			if($feEmp['Surname']){ $saleName .= $feEmp['Surname']." "; }
			if($feEmp['Nickname']){ $saleName .= "( ".$feEmp['Nickname']." )"; }
			// $saleName = select_emp_nametonickname($old_id_card);
		}else{
			if($_POST['searchGeo']){ $whereSearch .= "AND MAIN_ADDRESS.ADDR_GEOGRAPHY LIKE '".$_POST['searchGeo']."_%' "; }
			if($_POST['searchProvince']){ $whereSearch .= "AND MAIN_ADDRESS.ADDR_PROVINCE LIKE '".$_POST['searchProvince']."_%' "; }
			if($_POST['searchAmpher']){ $whereSearch .= "AND MAIN_ADDRESS.ADDR_DISTRICT LIKE '".$_POST['searchAmpher']."_%' "; }
			if($_POST['searchDistrice']){ $whereSearch .= "AND MAIN_ADDRESS.ADDR_SUB_DISTRICT LIKE '".$_POST['searchDistrice']."_%' "; }
			if($_POST['searchBU']){ $whereSearch .= "AND MAIN_RESPONSIBILITY.BUSINESS_REFS LIKE '".$_POST['searchBU']."' "; }
			// if($whereSearch){ $whereSearch .= "AND MAIN_ADDRESS.ADDR_MAIN_ACTIVE>0"; }
		}
		
		$where		= "AND MAIN_RESPONSIBILITY.RESP_IDCARD='".$old_id_card."'"; 
		
		$qstring	= "SELECT ";
		$qstring	.= "MAIN_RESPONSIBILITY.RESP_ID  AS resID, ";
		$qstring	.= "MAIN_RESPONSIBILITY.RESP_CUSNO AS CusNo, ";
		$qstring	.= "MAIN_RESPONSIBILITY.BUSINESS_REFS AS bizID, ";
		$qstring	.= "MAIN_CUS_GINFO.Be, ";
		$qstring	.= "MAIN_CUS_GINFO.Cus_Name, ";
		$qstring	.= "MAIN_CUS_GINFO.Cus_Surename ";
		$qstring	.= "FROM ".$config['db_maincus'].".MAIN_RESPONSIBILITY ";
		$qstring	.= "INNER JOIN ".$config['db_maincus'].".MAIN_CUS_GINFO ";
		$qstring	.= "ON MAIN_RESPONSIBILITY.RESP_CUSNO=MAIN_CUS_GINFO.CusNo ";
		$qstring	.= "LEFT JOIN ".$config['db_maincus'].".MAIN_ADDRESS ";
		$qstring	.= "ON MAIN_RESPONSIBILITY.RESP_CUSNO=MAIN_ADDRESS.ADDR_CUS_NO ";
		$qstring	.= "LEFT JOIN ".$config['db_maincus'].".MAIN_TELEPHONE ";
		$qstring	.= "ON MAIN_RESPONSIBILITY.RESP_CUSNO=MAIN_TELEPHONE.TEL_CUS_NO ";
		$qstring	.= "WHERE MAIN_RESPONSIBILITY.RESP_ID IS NOT NULL ".$where." ".$whereSearch." ";
		// $qstring	.= "AND MAIN_RESPONSIBILITY.RESP_CUSNO='5276431' ";
		$qstring	.= "GROUP BY MAIN_ADDRESS.ADDR_CUS_NO, MAIN_TELEPHONE.TEL_CUS_NO ";
		// echo $qstring."<br/><br/>";
		
		mysql_query('SET NAMES UTF8');
		$result = $logDb->queryAndLogSQL( $qstring, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$num_cus = mysql_num_rows($result);
		if($num_cus==0){
			$disabled = "disabled='disabled'";
			$cus_display = 'none';
			$nochange="<font color='red'><b>ไม่มีลูกค้าที่รับผิดชอบ</b></font>";
		}else {
			$cus_change_list = '';
			while($rs = mysql_fetch_assoc($result)){
				$sqlBiz				= "SELECT biz_name FROM ".$config['db_organi'].".biz_name WHERE biz_id='".$rs['bizID']."' AND status!=99 LIMIT 1";
				$queBiz				= $logDb->queryAndLogSQL( $sqlBiz, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$feBiz				= mysql_fetch_assoc($queBiz);
			
				$arrSend['resID']	= $rs['resID'];
				$arrSend['CusNo']	= $rs['CusNo'];
				$arrSend['bizID']	= $rs['bizID'];
				$sendRes			= json_encode($arrSend);
			
				$resID 				= $rs['resID'];
				$cus_no 			= $rs['CusNo'];
				$cus_name 			= $rs['Cus_Name'].' '.$rs['Cus_Surename'];
				$check['no']		= 'tel';
				
				$arrAdd['CusNo']	= $rs['CusNo']; 	## รหัสลูกค้า
				$arrAdd['type']		= $rs['ONE']; 		## ประเภท ## All=ทั้งหมด, ONE=เลือกมาอันหนึ่งตามลำดับ, CARD=บัตรประชาชน, DOC=เอกสาร, WORK=ที่ทำงาน, OTH=อื่นๆ
				
				## ค้นหา
				if($_POST['search']=='Y'){
					$arrAdd['geography'] 		= $_POST['searchGeo'];
					$arrAdd['province'] 		= $_POST['searchProvince'];
					$arrAdd['ampher'] 			= $_POST['searchAmpher'];
					$arrAdd['tambon']			= $_POST['searchDistrice'];
				}
				
				$resCusAdd 			= $objMain->getMainCusAddress($arrAdd); ## function/nuy.class.php
				$cus_addr 			= $resCusAdd['address'];
				
				$cus_change_list 	.= $tpl->tbHtml( $thisPage.'.html', 'CUS_CHANGE_LIST' );
			}// end while
			// $fixtable = "nz_modify_table({table:'tb_change_form',divHeight:'200px',bdHead:['#eee','#999'],bdBody:['#fff','#ccc']});";
		}
		
		$popupList					= $tpl->tbHtml( $thisPage.'.html', 'POPUP_LIST' );
		
		if($_POST['search']=='Y'){ echo $popupList; exit(); }
		
		$arrGeo['field_value']	= 'id';
		$optionGeo				= get_geography_option($arrGeo); ## function/general.php
		
		$optionProvince			= '<option value=""> - เลือกจังหวัด - </option>';
		$optionAmpher			= '<option value=""> - เลือกอำเภอ - </option>';
		$optionDistrict			= '<option value=""> - เลือกตำบล - </option>';
		
		$option_bu				= get_biz_option($arrBU); ## function/general.php
		
		/* $arrAmpher['field_value']	= 'id';
		$optionAmpher				= get_amphur_option($arrAmpher); ## function/general.php */
		
		/* $arrCusType['field_value']	= 'id';
		$optionCusType				= get_cusType_option($arrCusType); ## function/general.php */
		
		echo $tpl->tbHtml( $thisPage.'.html', 'CHANGE_FORM' );
	break;
	case 'change_resperson':


		/* 	########## EDIT ##########
			Array ( 
			[change] => Array ( 
				[0] => [{"resID":"6717","CusNo":"4603699","bizID":"1"}]
			) 
			[new_resperson] => พิบูลย์ศักดิ์  พิมสาร  (เอ)
			[hid_new_DataNo] => 3571000337961##RSO106102##58
			[hid_old_DataNo] => 1570500002839 ) */
			
		/* 	########## INSERT ##########
			Array
			(
				[change] => Array
					(
						[0] => {"resID":null,"CusNo":"5273013","bizID":null}
					)

				[new_resperson] => พิบูลย์ศักดิ์  พิมสาร  (เอ)
				[buID] => 2
				[hid_new_DataNo] => 3571000337961##RSO106102##58
				[hid_old_DataNo] => none
			) */
	




		list($new_id_card, $position, $department) 	= explode("##",$_POST['hid_new_DataNo']);
		$old_id_card 	= $_POST['hid_old_DataNo'];

		$cusArr		 	= $_POST['change'];
		$arrCusNo		= array();
		$BizID 			= $_POST['buID'];

			 // echo "arrRes <pre>";
				// print_r( $_POST);
				// echo "</pre>"; 
				// exit();
		//ถ้ากำหนดใหม่ old_id_card จะเท่ากับ none
		if($new_id_card!= "" && $old_id_card !="" && !empty($cusArr)){
			$cus_is_change 	= '';
			
			foreach($cusArr AS $key=>$value)
			{
				$value	= stripslashes($value);
				## value send
				$arrRes 	= json_decode($value);
				
				//  echo "arrRes <pre>";
				// print_r($arrRes);
				// echo "</pre>"; 
				
				## ถ้าเป็นการแก้ไข
				if($old_id_card!="none"){
					## insert LOG_MAIN_RESPONSIBILITY 
					$insLog		= "INSERT INTO ".$config['db_maincus'].".LOG_MAIN_RESPONSIBILITY(RESP_ID, RESP_BU_CUSNO, RESP_CUSNO, BUSINESS_REFS, DEPARTMENT_REFS, ";
					$insLog		.= "POS_EMP_CODE, RESP_IDCARD, RESP_DATE, RESP_TIME_STAMP, BU, DO) SELECT RESP_ID, RESP_BU_CUSNO, RESP_CUSNO, BUSINESS_REFS, DEPARTMENT_REFS, ";
					$insLog		.= "POS_EMP_CODE, RESP_IDCARD, RESP_DATE, RESP_TIME_STAMP, 'crm', '2' FROM ".$config['db_maincus'].".MAIN_RESPONSIBILITY ";
					$insLog		.= "WHERE RESP_ID = '".$arrRes->resID."' LIMIT 1";
					mysql_query($insLog) or die('<br/>'.__FILE__.':'.__LINE__.'<br>'.$update.'<br>'.mysql_error());
					
					## update MAIN_RESPONSIBILITY
					$update 	= "UPDATE ".$config['db_maincus'].".MAIN_RESPONSIBILITY SET RESP_IDCARD='".$new_id_card."', DEPARTMENT_REFS='".$department."', ";
					$update		.= "POS_EMP_CODE='".$position."' WHERE RESP_ID='".$arrRes->resID."' LIMIT 1";
					// echo $update."<br/><br/>";
					$logDb->queryAndLogSQL( $update, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					
					## update  ERP_maincusdata.maincusdata
					if($arrRes->bizID == 1){
						$update 	= "UPDATE ".$config['Cus'].".MainCusData SET Resperson='".$new_id_card."', Resperson_posCode='".$position."' ";
						$update		.= "WHERE CusNo='".$arrRes->CusNo."' LIMIT 1";
						$logDb->queryAndLogSQL( $update, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					}
			
				## ถ้าเป็นการเพิ่มใหม่
				}else{
					## เช็คว่ามีแล้วหรือยัง
					## select old resperson for update
					$sqlChk = "SELECT RESP_ID, RESP_IDCARD FROM ".$config['db_maincus'].".MAIN_RESPONSIBILITY WHERE RESP_CUSNO='".$arrRes->CusNo."' AND BUSINESS_REFS='".$BizID."' LIMIT 1";
					$queChk = $logDb->queryAndLogSQL( $sqlChk, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$feChk  = mysql_fetch_assoc($queChk);
				
					if(!$feChk['RESP_ID'] && $queChk){
						## insert MAIN_RESPONSIBILITY
						$insert		= "INSERT INTO ".$config['db_maincus'].".MAIN_RESPONSIBILITY (RESP_ID, RESP_BU_CUSNO, RESP_CUSNO, BUSINESS_REFS, DEPARTMENT_REFS, ";
						$insert		.= "POS_EMP_CODE, RESP_IDCARD, RESP_DATE) VALUES (NULL, '', '".$arrRes->CusNo."', '".$BizID."', '".$department."', ";
						$insert		.= "'".$position."', '".$new_id_card."', '".date('Y-m-d')."');";
						// echo $insert."<br/><br/>";
						$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					
						$idRun		= mysql_insert_id();
					
						## insert LOG_MAIN_RESPONSIBILITY
						$insLog		= "INSERT INTO ".$config['db_maincus'].".LOG_MAIN_RESPONSIBILITY(RESP_ID, RESP_BU_CUSNO, RESP_CUSNO, BUSINESS_REFS, DEPARTMENT_REFS, ";
						$insLog		.= "POS_EMP_CODE, RESP_IDCARD, RESP_DATE, RESP_TIME_STAMP, BU, DO) SELECT RESP_ID, RESP_BU_CUSNO, RESP_CUSNO, BUSINESS_REFS, DEPARTMENT_REFS, ";
						$insLog		.= "POS_EMP_CODE, RESP_IDCARD, RESP_DATE, RESP_TIME_STAMP, 'crm', '1' FROM ".$config['db_maincus'].".MAIN_RESPONSIBILITY ";
						$insLog		.= "WHERE RESP_ID = '".$idRun."' LIMIT 1";
						mysql_query($insLog) or die('<br/>'.__FILE__.':'.__LINE__.'<br>'.$update.'<br>'.mysql_error());

						## update  ERP_maincusdata.maincusdata
						if($BizID == 1){
							$update 	= "UPDATE ".$config['Cus'].".MainCusData SET Resperson='".$new_id_card."', Resperson_posCode='".$position."', respon_date = '".date('Y-m-d')."' ";
							$update		.= "WHERE CusNo='".$arrRes->CusNo."' LIMIT 1";
							$logDb->queryAndLogSQL( $update, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						}						
					}else{
						## id cancel follow_customer
						$old_id_card	= $feChk['RESP_IDCARD'];

						## insert LOG_MAIN_RESPONSIBILITY 
						$insLog		= "INSERT INTO ".$config['db_maincus'].".LOG_MAIN_RESPONSIBILITY(RESP_ID, RESP_BU_CUSNO, RESP_CUSNO, BUSINESS_REFS, DEPARTMENT_REFS, ";
						$insLog		.= "POS_EMP_CODE, RESP_IDCARD, RESP_DATE, RESP_TIME_STAMP, BU, DO) SELECT RESP_ID, RESP_BU_CUSNO, RESP_CUSNO, BUSINESS_REFS, DEPARTMENT_REFS, ";
						$insLog		.= "POS_EMP_CODE, RESP_IDCARD, RESP_DATE, RESP_TIME_STAMP, 'crm', '2' FROM ".$config['db_maincus'].".MAIN_RESPONSIBILITY ";
						$insLog		.= "WHERE RESP_CUSNO='".$arrRes->CusNo."' AND BUSINESS_REFS = '".$BizID."' LIMIT 1";
						mysql_query($insLog) or die('<br/>'.__FILE__.':'.__LINE__.'<br>'.$update.'<br>'.mysql_error());
						
						## update MAIN_RESPONSIBILITY
						$update 	= "UPDATE ".$config['db_maincus'].".MAIN_RESPONSIBILITY SET RESP_IDCARD='".$new_id_card."', DEPARTMENT_REFS='".$department."', ";
						$update		.= "POS_EMP_CODE='".$position."' WHERE RESP_CUSNO='".$arrRes->CusNo."' AND BUSINESS_REFS = '".$BizID."' LIMIT 1";
						// echo $update."<br/><br/>";
						$logDb->queryAndLogSQL( $update, " FILE : ".__FILE__." LINE : ".__LINE__."" );

						## update  ERP_maincusdata.maincusdata
						if($BizID == 1){
							$update 	= "UPDATE ".$config['Cus'].".MainCusData SET Resperson='".$new_id_card."', Resperson_posCode='".$position."' ";
							$update		.= "WHERE CusNo='".$arrRes->CusNo."' LIMIT 1";
							$logDb->queryAndLogSQL( $update, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						}
						
						## update cancel follow_customer
						// $upFollow	= "UPDATE follow_customer SET cancel_type='', remark='', status=99 WHERE ";
					}
				}
				
				#-- up follow_customer
				if($old_id_card!="none"){
					// $change_emp_sql = "UPDATE ".$config['db_base_name'].".follow_customer SET emp_id_card = '".$new_id_card."' ";
					$change_emp_sql = "UPDATE ".$config['db_base_name'].".follow_customer SET  cancel_type='stopResperson', remark='เปลี่ยนผู้ดูแล=>', status = '99' ";
					$change_emp_sql .= "WHERE emp_id_card = '".$old_id_card."' ";
					$change_emp_sql .= "AND cus_no ='".$arrRes->CusNo."' ";
					$change_emp_sql .= "AND biz_id_ref='".$arrRes->bizID."' ";
					$change_emp_sql .= "AND status IN('0','1','90') ";
					$change_emp_sql .= "AND stop_date > DATE(NOW())"; // เพิ่มสถานะ 90 ด้วย
					// echo $change_emp_sql."<br/><br/>";
					//อัพเดตเฉพาะข้อมูลที่ยังไม่ได้ทำรายการ  และยังไม่เลยกำหนด
					$result_follow = $logDb->queryAndLogSQL( $change_emp_sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				}
			}
			
			$message = "<h2 align='center' style='color:green'>เปลี่ยนผู้รับผิดชอบลูกค้า เรียบร้อย</h2>";
			// $message .= '<script type="text/javascript">setTimeout("followFormSubmit(\'list_sale\',\''.$thisPage.'.php\');$(\'#nz_popup_div\').remove();",2000);</script>';

		}else {
			if($old_id_card==""){ $miss .= '<br>* ผู้รับผิดชอบลูกค้า คนเดิม';}
			if($new_id_card==""){ $miss .= '<br>* ผู้รับผิดชอบลูกค้า คนใหม่';}
			if($cusArr==""){ $miss .= '<br>* รายชื่อลูกค้าที่ต้องการเปลี่ยนผู้รับผิดชอบ<br>';}
			$message = "<h3 align='center' style='color:red'>ส่งข้อมูลไม่ครบ <br><font style='color:blue;font-size:12px;'>$miss</font><br>กรุณาทำรายการอีกครั้งครับ!</h3>";
		}
		#-- ถ้าเป็นการค้นหาลูกค้าที่ไม่มีผู้ดูแล
		if($old_id_card=="none"){
			// $js = '<script type="text/javascript">hideObj("bt_action");</script>';
		}
		echo $message.$js;
	break;
	default:
		#-- สร้างตัวแปรตรวจสอบ
		if($_REQUEST['search']!=""){ $selectpage = $_REQUEST['search']; }else { $selectpage = 'search_sale'; }
		$blockS1 = "<!-- "; $blockS2 = " -->";
		$option_bu				= get_biz_option($arrBU);

		#-- ค้นหาชื่อลูกค้า
		if($selectpage=='search_cus'){
			$search_sale_display 	= 'none';
			$name 					= $_POST['cus_name'];
			$surname 				= $_POST['cus_surname'];
			$cus_tum 				= $_POST['cus_tum'];
			$ampure 				= $_POST['cus_aumpure'];
			$province 				= $_POST['cus_province'];
			$cus_type 				= $_POST['cus_tel'];
			$cus_bu 				= $_POST['cus_bu'];
			$cus_id 				= $_POST['cus_id'];
			$empID 					= $_POST['empID'];

			$where		= "";
			$whereRes	= "";
			$join_sell	= "";

			if($name){ $where .= "AND MAIN_CUS_GINFO.Cus_Name LIKE '".trim($name)."%' "; } ## ชื่อ
			if($surname){ $where .= "AND MAIN_CUS_GINFO.Cus_Surename LIKE '".trim($surname)."%' "; } ## นามสกุล
			if($cus_id){ $where .= "AND MAIN_CUS_GINFO.CusNo LIKE '".trim($cus_id)."%' "; } ## หมายเลขลูกค้า
			if($cus_tum){ $where .= "AND MAIN_ADDRESS.ADDR_SUB_DISTRICT LIKE '%".trim($cus_tum)."%' "; } ## ตำบล
			if($ampure){ $where .= "AND MAIN_ADDRESS.ADDR_DISTRICT LIKE '%".trim($ampure)."%' "; } ## อำเภอ
			if($province){ $where .= "AND MAIN_ADDRESS.ADDR_PROVINCE LIKE '%".trim($province)."%' "; } ## จังหวัด
			if($cus_tel){ $where .= "AND MAIN_TELEPHONE.TEL_NUM LIKE '%".trim($cus_tel)."%' "; } ## เบอร์มือถือ
			if($cus_bu){ 
				$where 		.= "AND (MAIN_RESPONSIBILITY.BUSINESS_REFS!='".trim($cus_bu)."' OR MAIN_RESPONSIBILITY.BUSINESS_REFS IS NULL) ";
				$whereRes	= "AND BUSINESS_REFS!='".trim($cus_bu)."'";
				if($cus_bu =='1'){
					$join_sell 	.= "INNER JOIN ".$config['db_easysale'].".Sell ";
					$join_sell 	.= "ON MAIN_CUS_GINFO.CusNo=SUBSTRING_INDEX(Sell.cusBuy,'_',1) ";
					// $where .= "AND MAIN_CUS_GINFO.CusNo=SUBSTRING_INDEX(Sell.cusBuy,'_',1)";
				}
			} ## เบอร์มือถือ BU
			
			$qstring	= "SELECT ";
			$qstring	.= "MAIN_CUS_GINFO.CusNo, ";
			$qstring	.= "MAIN_CUS_GINFO.Be, ";
			$qstring	.= "MAIN_CUS_GINFO.Cus_Name, ";
			$qstring	.= "MAIN_CUS_GINFO.Cus_Surename ";
			$qstring	.= "FROM ".$config['db_maincus'].".MAIN_CUS_GINFO ";
			$qstring	.= "LEFT JOIN ".$config['db_maincus'].".MAIN_RESPONSIBILITY ";
			$qstring	.= "ON MAIN_CUS_GINFO.CusNo=MAIN_RESPONSIBILITY.RESP_CUSNO ";
			$qstring	.= "LEFT JOIN ".$config['db_maincus'].".MAIN_ADDRESS ";
			$qstring	.= "ON MAIN_CUS_GINFO.CusNo=MAIN_ADDRESS.ADDR_CUS_NO ";
			$qstring	.= "LEFT JOIN ".$config['db_maincus'].".MAIN_TELEPHONE ";
			$qstring	.= "ON MAIN_CUS_GINFO.CusNo=MAIN_TELEPHONE.TEL_CUS_NO ";

			$qstring	.= $join_sell;
			$qstring	.= "WHERE 1=1 ".$where." ";
			// $qstring	.= "AND MAIN_CUS_GINFO.CusNo=5276430 ";
			$qstring	.= "GROUP BY MAIN_ADDRESS.ADDR_CUS_NO, MAIN_TELEPHONE.TEL_CUS_NO ";

			// echo $qstring."<br/><br/>";exit();
			
			//nz 2011-08-04
			$result = $logDb->queryAndLogSQL( $qstring, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$numAll = mysql_num_rows($result);

			$qstring .= "ORDER BY MAIN_CUS_GINFO.UpdateT DESC LIMIT 300";
			// echo $qstring;
			mysql_query( "SET NAMES UTF8" ) ;
			$query = $logDb->queryAndLogSQL( $qstring, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$num_cus = mysql_num_rows($query);

			$old_id_card = 'none';
			while($rs = mysql_fetch_assoc($query)){
				$cus_name		= "";
				
				$change_disabled		= "";
				$have_resperson			= "";
				$feBiz['biz_name']		= "";
				$bizName				= "";
				
				$sqlRes	= "SELECT BUSINESS_REFS AS bizID, RESP_IDCARD FROM ".$config['db_maincus'].".MAIN_RESPONSIBILITY WHERE RESP_CUSNO='".$rs['CusNo']."' ".$whereRes;
				// echo $sqlRes."<br/><br/>";
				$queRes = $logDb->queryAndLogSQL( $sqlRes, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$rowRes  = mysql_num_rows($queRes);
				
				if($rowRes>0){
					while($feRes = mysql_fetch_assoc($queRes)){
						$sqlBiz		= "SELECT biz_name FROM ".$config['db_organi'].".biz_name WHERE biz_id='".$feRes['bizID']."' AND status!=99 LIMIT 1";
						$queBiz		= $logDb->queryAndLogSQL( $sqlBiz, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						$feBiz		= mysql_fetch_assoc($queBiz);
						
						if($bizName!=""){ $bizName .= "<br/>"; }
						
						################ emp data ###################
						$arr['id_card']	= $feRes['RESP_IDCARD'];
						$arr['Being']	= "";
						$feEmp	= $objMain->getEmpData($arr);
						
						$empName	= "";
						if($feEmp['Be']){ $empName .= $feEmp['Be']." "; }
						if($feEmp['Name']){ $empName .= $feEmp['Name']." "; }
						if($feEmp['Surname']){ $empName .= $feEmp['Surname']." "; }
						// if($feEmp['Nickname']){ $empName .= "( ".$feEmp['Nickname']." )"; }
						if($empName){ $empName = "(".$empName.")"; }
						################ emp data ###################
						
						$bizName	.= $feBiz['biz_name']." ".$empName."";
					}
					
					$feBiz['biz_name']	= "<font color='#FF0000'>".$bizName."</font>";
					// $change_disabled 	= 'disabled="disabled" style="visibility:hidden"';
					// $have_resperson 	= '<span style="color:red">* มีผู้รับผิดชอบลูกค้าแล้ว</span>';
				}else{
					$feBiz['biz_name']	= "-";
				}
			
				$cus_no 			= $rs['CusNo'];
				if($rs['Be']){
					$cus_name		= $rs['Be'].' ';
				}
				$cus_name 			.= $rs['Cus_Name'].' '.$rs['Cus_Surename'];
				$check['no']		='tel';
				// $cus_addr 		= getMainCusAddress($rs,$check);
				$arrAdd['CusNo']	= $rs['CusNo']; 	## รหัสลูกค้า
				$arrAdd['type']		= $rs['ONE']; 		## ประเภท ## All=ทั้งหมด, ONE=เลือกมาอันหนึ่งตามลำดับ, CARD=บัตรประชาชน, DOC=เอกสาร, WORK=ที่ทำงาน, OTH=อื่นๆ
				$resCusAdd 			= $objMain->getMainCusAddress($arrAdd); ## function/nuy.class.php
				$cus_addr 			= $resCusAdd['address'];
				
				$arrSend['resID']	= $rs['resID'];
				$arrSend['CusNo']	= $rs['CusNo'];
				$arrSend['bizID']	= $rs['bizID'];
				$sendRes			= json_encode($arrSend);

				$cus_change_list .= $tpl->tbHtml( $thisPage.'.html', 'CUS_CHANGE_LIST' );
			}//end while
			
			$bg = '#ffffff';
			$fixtable = "nz_modify_table({table:'tb_change_form',divHeight:'200px',bdHead:['#eee','#999'],bdBody:['#fff','#ccc']});";
			$config['change_descrip'] = 'ไม่มีผู้รับผิดชอบลูกค้า';

			$config['change_descrip2'] = 'จำนวนลูกค้า ';

			//nz 2011-08-04
			$config['change_descrip3'] = ' จากจำนวนทั้งหมด <font style="color:red"><b>'.number_format($numAll,0).'</b></font> ราย';
			$display_close = 'none';
			
			$arrBU['field_value']	= "id";
			$selectBU				= '<select name="buID" id="buID">
											'.$option_bu.'
										</select>';
										
			$popupList					= $tpl->tbHtml( $thisPage.'.html', 'POPUP_LIST' );

			echo $tpl->tbHtml( $thisPage.'.html', 'CHANGE_FORM' );

		}else {#-- ค้นหาชื่อพนักงาน

			$chk	= "N";
			$search_cus_display = 'none';
			$hidden = "class=\"hidden\""; $display_none = "style=\"display:none\"";
			
			if(!$emp_id_card){
				$content = "<tr><td colspan=\"5\" style=\"text-align: center;\">ไม่พบข้อมูลที่ท่านค้นหา !! </td></tr>";
			}else{
				$noKey	= 0;
				foreach($command_array[0] AS $key=>$value){
					if($_POST['sale_idCard']){ $chk = "Y"; }
				
					if($chk=="N" || ($chk=="Y" && ($value==$_POST['sale_idCard']))){
						$arr['id_card']	= $value;
						// $arr['Being']	= "3";
						$arr['Being']	= "";
						$feEmp	= $objMain->getEmpData($arr);
						
						$empName	= "";
						if($feEmp['Be']){ $empName .= $feEmp['Be']." "; }
						if($feEmp['Name']){ $empName .= $feEmp['Name']." "; }
						if($feEmp['Surname']){ $empName .= $feEmp['Surname']." "; }
						if($feEmp['Nickname']){ $empName .= "( ".$feEmp['Nickname']." )"; }
						
						$all_team[$noKey]['PositionCode']	= $feEmp['Code'];		## position code
						$all_team[$noKey]['emp_name']		= $empName;				## emp name
						$all_team[$noKey]['id_card']		= $value;				## id card
						$all_team[$noKey]['position_name']	= $feEmp['Position'];	## position name
						$noKey++;
						########## ที่อยู่ ##########
					}
				}

				$content .= list_resperson($all_team, 1);
			}

			$listInfo = $tpl->tbHtml( $thisPage.'.html', 'SALE_LIST_HEAD' ).$content.$tpl->tbHtml( $thisPage.'.html','SALE_LIST_FOOT' );
			if(isset($_REQUEST['search'])){ echo $listInfo; }
		}//end check page

		if(!isset($_REQUEST['search'])){
			echo $tpl->tbHtml($thisPage.'.html', 'APPROVE_HEADER');  //แถบค้นหา
		}
	break;
}

CloseDB();
?>