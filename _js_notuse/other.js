/* naizan create 2011-01-11 */
var systerm_check_msg=0;
//เช็กข้อความแจ้งเตือนของระบบ เช่นตอนเปลี่ยนเวอร์ชั่น
$(document).ready(function(){
	jQuery('#main').ajaxComplete(function() {
		  if(systerm_check_msg==111){
		      if( typeof(check_systerm_warning)=='function'){
		    	  check_systerm_warning();
		      }
		  }
		  systerm_check_msg = 'delay';
		  setTimeout(function(){
			  if(systerm_check_msg!='get_progress'){systerm_check_msg = 111;}
		  },20000);//20วิ ทำงานครั้งนึง
	});
});
function check_systerm_warning(){
	if(systerm_check_msg!='get_progress'){ 
		$.get('check_systerm_warning.php', '', function(result){
			$('.system_warning_msg').remove();
			if(result.search('system_warning_msg')){//ถ้ามีคลาสข้อความแจ้งเตือน (เช็กกรณี echo ค่าว่าง)
				$('body').prepend(result);
			}
		});
	}
}

//
//-- begin customers_management การจัดการลูกค้ามุ่งหวัง
function seting_manage(){
	//เพิ่มการทำงานให้ปุ่ม ซ่อน/แสดงเมนูค้นหา
	$("#show_hide_button").click(function(){
		var txt = $(this).html();
		if(txt=="แสดงเมนูค้นหา"){
			$("#footer,#accordionApppre").hide();
			$("#saleRepareContain").show();
			hideHeadContent('show','containHead');
			//browseCheck('main','customers_management.php','');
			$(this).hide();
			$("a.button").hide();
			//$("#footer,#sugnsA,#app_five,#app_one,#app_three,#app_four,#condition_detail2,#sugnsC,#show_hide").html('');
			$("#footer,#sugnsA,#app_five,#app_one,#app_two,#app_three,#app_four,#condition_detail2,#sugnsC,#show_hide").html('');
		}
	});

	$( "#showCusDetail" ).draggable({
		handle:	'#headDetail',
		opacity: 0.8
	});

}

/**
 *ไฮไลท์แถวที่เพิ่งทำรายการเสร็จ
 * @param Integer intnum หมายเลขความสนใจ
 */
function change_active_row(intnum){
	$('.active_manage_cuslist').removeClass('active_manage_cuslist');
	$('#holdlist'+intnum).addClass('active_manage_cuslist');
	$('#intnum_active').val(intnum);
}

 //เปิดหน้าเสนอแผน
function goto_edit_condition(cusNo,page,case_name,cusInt,intTime,prepare_num){
	change_active_row(cusInt);//เปลี่ยนสีพื้นหลัง
	$("#footer,#sugnsA,#app_five,#app_one,#app_two,#app_three,#app_four,#condition_detail2,#sugnsC,#show_hide").html('');
	$('#footer').show().html(ajaxLoader);
	$('#saleRepareContain').hide();
	$("#show_hide_button").show();
	hideHeadContent('hide','containHead');
	$('#show_hide').hide();//ซ่อนปุ่มซ่อนแสดงเมนูค้นหาตัวเก่า
	$.get(page,'Edit='+cusNo+'&case_name='+case_name,function(result){
		$("#footer").html(result);
		if(case_name=="ProposedTrack"){									//## ไปหน้าเสนอแผน
			openTabs('ctwo',cusNo,cusInt,intTime);
		}else if(case_name=="SaveTracking"){                     //** ไปหน้าบันทึกผลการติดตาม
			//openSaveTrack('cthree',cusNo,cusInt,intTime);
			document.getElementById('cthree').style.display = '';selectedTab(3);
			$.get('tracking.php?PrepareHis='+cusNo+'&intNo='+cusInt+'&check=TrackRecord&TrackintTime='+intTime,'',function(result){
				$("#box_three").html(result);
				$('#box_four').html("<div style='text-align:center;color:blue;font-size:20px;'><img src=images/preload.gif><br>กรุณารอสักครู่ ระบบกำลังโหลดข้อมูล</div>");
				//alert('->'+prepare_num+'<-');
				SaveTracking(cusNo,cusInt,intTime,prepare_num);
			});
		}
	});
}
//---  อนุมัติแผน
function goto_approve_prepare(id_card,prepare_no,int_num,cus_no,plan_person){
	change_active_row(int_num);//เปลี่ยนสีพื้นหลัง
	$("#footer,#sugnsA,#app_five,#app_one,#app_two,#app_three,#app_four,#condition_detail2,#sugnsC,#show_hide").html('');
	hideHeadContent('hide','containHead');
	$('#show_hide').hide();//ซ่อนปุ่มซ่อนแสดงเมนูค้นหาตัวเก่า
	$("#show_hide_button").show();
	$('#saleRepareContain').hide();
	$('#accordionApppre,#a_prepare').show();
	$('#app_two').show().html("<div style='text-align:center;color:blue;font-size:20px;'><img src=images/preload.gif><br>กรุณารอสักครู่ ระบบกำลังโหลดข้อมูล</div>");
	showPrepareDetail(prepare_no,int_num,cus_no,plan_person);
	$('#app_one').html('');
}
//--- อนุมัติการติดตาม showRepareDetail
function goto_approve_contact(RepareNo,IntNum,cus_no,PlanPerson,PrepareNo,checkcase){
	change_active_row(IntNum);//เปลี่ยนสีพื้นหลัง
	$("#footer,#sugnsA,#app_five,#app_one,#app_two,#app_three,#app_four,#condition_detail2,#sugnsC,#show_hide").html('');
	hideHeadContent('hide','containHead');
	$('#show_hide').hide();//ซ่อนปุ่มซ่อนแสดงเมนูค้นหาตัวเก่า
	$("#show_hide_button").show();
	$('#saleRepareContain').hide();
	$('#accordionApppre').show();
	$('#app_four').show().html("<div style='text-align:center;color:blue;font-size:20px;'><img src=images/preload.gif><br>กรุณารอสักครู่ ระบบกำลังโหลดข้อมูล</div>");

	if(checkcase=='readonly'){
		var array = {readonly:'on'};
	}
	if(checkcase=='debug'){//เช็กว่าส่งตัวแปรครบหรือไม่
		alert(checkcase+' :: Rep='+ RepareNo+', IntNum='+ IntNum+', cus_no='+cus_no+', cus_no='+PlanPerson+', PrepareNo='+PrepareNo);
	}
	showRepareDetail(RepareNo,IntNum,cus_no,PlanPerson,PrepareNo,array);
}
//ตรวจสอบเงื่อนไขการนำเสนอ ในหน้า การจัดการลูกค้ามุ่งหวัง update 2011-09-28 pt
function show_condition_detail2(cus_number,elem){
	$('#condition_detail2').prepend(ajax_loader);
	$('#showCusDetail2').show();
	$.get('list_conprepare.php',{cus_no:cus_number,case_name:'from_getcus_holding'},function(data){
		$('#condition_detail2').html(data).fadeIn('slow');
		$('#close_condition_detail2').show();$('#tabs').tabs();
		$('#ajaxLoader').remove(); $('#tabs').css('margin-bottom','50px');
		$("#close_condition_detail2 img").live('click',function(){
			$('#showCusDetail2,#accordionApppre').hide();
		});
		window.scrollTo(0,100);
	});
}

//ตรวจสอบผลการนำเสนอ ในหน้า การจัดการลูกค้ามุ่งหวัง
function show_approve_detail(cus_no,check,case_name,sale_id_card){
	$("#footer,#sugnsA,#app_five,#app_one,#app_two,#app_three,#app_four,#condition_detail2,#sugnsC,#show_hide").html('');
	$('#condition_detail2').prepend(ajax_loader);
	$('#showCusDetail2').show();
	$.get('list_conprepare.php',{cus_no:cus_no,case_name:'check_track_list'},function(data){
		$('#condition_detail2').html(data).fadeIn('slow');
		$('#close_condition_detail2').show();$('#tabs').tabs();
		$('#ajaxLoader').remove(); $('#tabs').css('margin-bottom','50px');
		$("#close_condition_detail2 img").live('click',function(){
			$('#showCusDetail2,#accordionApppre').hide();
		});
		window.scrollTo(0,100);
	});
	/*
	$("#footer,#sugnsA,#app_five,#app_one,#app_two,#app_three,#app_four,#condition_detail2,#sugnsC,#show_hide").html('');
	hideHeadContent('hide','containHead');
	$("#show_hide_button").show();
	$('#saleRepareContain').hide();
	$('#accordionApppre').hide();
	$('#footer').show().html("<div style='text-align:center;color:blue;font-size:20px;'><img src=images/preload.gif><br>กรุณารอสักครู่ ระบบกำลังโหลดข้อมูล</div>");
	 //          อ้างอิง edit_cusNo(cus_no,check,case_name);//ken.js
	$.get(check+'&CheckResultsCusNo='+cus_no+'&CheckResultscase_name='+case_name,'sale_id_card='+sale_id_card,function(data){
		$('#footer').html(data);
		$('#cone').hide();
		$('#ctwo a[href="#tabs-2"]').click();
	});
	*/
}

function other_searchCus(case_name){
	switch (case_name){
		case "keyword":
		browseCheck( 'saleRepareContain',
			'customers_management.php?search=yes&SESSION_Name='+document.getElementById('SESSIONName').value
			+'&cus_name='+document.getElementById('cus_name').value
			+'&cus_surname='+document.getElementById('cus_surname').value
			+'&cus_aumpure='+document.getElementById('cus_aumpure').value
			+'&cus_province='+document.getElementById('cus_province').value
			+'&cus_type='+document.getElementById('cus_type').value
			+'&cus_tel='+document.getElementById('cus_tel').value
			+'&cus_id='+document.getElementById('cus_id').value,'');
		break;
		case "All":
			browseCheck( 'saleRepareContain','customers_management.php?search=yes');
		break;
	}
}
function setInputSearch(frmID,func){
	$('#'+frmID+' input:text').live('keyup',function(e){
		if (window.event){keycode = window.event.keyCode;}else if (e){	keycode = e.which;}
		if(keycode == 13) {
			eval(func);
		}
	});
}
function show_hide_search(param){
	if(param=="search_sale"){
		$('#search').val('search_sale');
		$('#search_cus').hide().find('input').attr('disabled',true);
		$('#search_sale').show().find('input').attr('disabled',false);
	}else if(param=="search_cus"){
		$('#search').val('search_cus');
		$('#search_sale').hide().find('input').attr('disabled',true);
		$('#search_cus').show().find('input').attr('disabled',false);
	}
}
//-- end customers_management


function runBlink(arr){
	/** สร้างตัวอักษรกระพริบ เรียกโดย runBlink('ชื่อ div');
	*	หรือแบบกำหนดค่าด้วย runBlink({div:'ชื่อ div',time:'หน่วงเวลา ms',color:['สีที่ 1','สีที่ 2','สีที่ nn']});
	*/
	if(!arr){alert('กำหนด id ของ element ที่ต้องการด้วย');return false;}
 	if(typeof(arr) == 'string'){
      	var obj = arr;
      }else{
      	var t = arr.time,obj=arr.div,num=arr.count,arrColor = arr.color;
   }
	if(document.getElementById(obj)){
		if(!t){t=700;}
		if(!num){num=0;}
		if(!arrColor){	arrColor = new Array('#FF6600','#32CD32','#FF00CC');}
		document.getElementById(obj).style.color = arrColor[num];
		num++;
		if(num==arrColor.length){num=0;}
		setTimeout(function(){runBlink({div:obj,count:num,time:t,color:arrColor});},t);
	}else{
		clearTimeout();
	}
}

//สำหรับ ซ่อนแสดง id ที่ต้องการ แบบ toggle
function nz_toggle(arr){
	if(typeof(arr) == 'string'){
      	var obj = arr;
      }else{
      	var obj=arr.div,url=arr.url,param = arr.param;
   }
   var display = $('#'+obj).css('display');
   if(display=="none"){
   	$('#'+obj).fadeIn('slow');
   	if(url){
		$('#'+obj).html(ajaxLoader);
		$.get(url,'',function(data){
			$('#'+obj).html(data);
			if(arr.callback){arr.callback();}
		});
	}
   }else{
   	$('#'+obj).fadeOut();
   }

}


//ic_change_Resperson.html
function list_change_cus(idcard,emp){
      var obj = $('#show_cus_list'+idcard);
      var objToggle = $('#emp'+idcard);

      if(objToggle.attr('title') != 'ซ่อนรายชื่อลูกค้า'){
            $('.toggleChange').removeClass('toggleChange').hide();//ซ่อนตัวก่อนหน้านี้
            objToggle.attr('title','ซ่อนรายชื่อลูกค้า');
            obj.show().addClass('toggleChange');
            browseCheck('show_cus_list'+idcard,'ic_change_Resperson.php?func=show_cus_list&sale_idcard='+idcard);
      }else{
            objToggle.attr('title','คลิกที่นี่เพื่อดูรายชื่อลูกค้าที่ '+emp+' รับผิดชอบ');
            obj.hide().html('');
      }
}
function set_auto_com(url,input_id){//autocomplete ใช้ url ของ ic_autocomplete.php
      $(input_id).autocomplete({
            source: url,minLength: 2,
            open: function() {
                  $('#hidden_'+$(this).attr('id')).val('');
            },
            select: function(event, ui) {
                  $('#hidden_'+$(this).attr('id')).val(ui.item.hidden_value);
            }
      });
}
//เปลี่ยนผู้ดูแล
function change_resperson(cusno,hidden_id,old_emp){
      var resperson = $("#"+hidden_id).val();
      if(resperson){
            $.get('ic_change_Resperson.php?func=change',{cus_no:cusno,resperson:resperson,old_emp:old_emp},function(data){
                  $('#auto'+cusno).attr('readonly','readonly');
                  $('#hidden_auto'+cusno).next().attr('disabled',true).after(data);
            });
      }else{
            alert('ยังไม่ได้ระบุชื่อรับผิดชอบครับ!');
      }
}

/**
 * แสดงสถานะเปอร์เซนต์ดาวนโหลด
 * @param html_obj ชื่อ id ของ element ที่จะใช้แสดงผล '#abcd'
 * @param ชื่อ session ตอนสร้าง progress
 */
function show_progress(html_obj,session_name){
	setTimeout(function(){
		get_progress(html_obj,session_name);
	},1000);//setTimeout();
}
function get_progress(html_obj,session_name,checknull,old_data,check_error,check_old_obj){
	systerm_check_msg = 'get_progress';//ไม่ต้องเช็กข้อความจากระบบ เพราะวนหลายรอบเกินไป
	if(!session_name){ session_name = '';}
	if(!checknull){checknull=0;}
	if(!check_error){check_error=0;}
	//$('#test').remove();//test
	$.get('show_progress.php?process=show','session_name='+session_name,function(data){
		var obj_data = '';
		if(!data){data=0;}
		if(check_old_obj){obj_data = old_data;}
		else{obj_data = data;}//ครั้งแรก
		
		var obj = 'nz_inprogress'+session_name+obj_data;
		var progress_bar = $('#'+obj).text();
		if(!check_old_obj && !progress_bar && !old_data){
			$('#'+obj).remove();
			$('#nz_main_progress').remove();
			$(html_obj).append('<div id="nz_main_progress" style="width:100%;padding:5px" align="center"><div id="'+obj+'"></div></div>');
		}
		//test
		//$('#main').append('<div id="test">nz_inprogress'+session_name+obj_data+'<div>');
		
		var progress_obj = $('#'+obj);	
		//เช็กก่อนว่า session ที่ทำงานอยู่นั้นถ้าครั้งแรกให้ทำงานเลย
		//ถ้าเป็นการทำงานรอบต่อไป ให้เช็กว่า div แสดงผลยังมีอยู่หรือไม่ถ้าไม่มีแล้ว แสดงว่า
		//มีการโหลดข้อมูลอื่นไปแล้ว ให้เลิกการแสดงผลตัวเดิม
		if(!check_old_obj || (check_old_obj!='' && progress_obj.text()!='') && $('#nz_percent'+session_name+obj_data).text()!='' ){
			//test
			//$('#main').append(progress_obj.text());		
			var top = progress_obj.offset().top;
			var left = progress_obj.offset().left;
			var bar_id = 'nz_progressbar'+session_name+data;
			var per_id = 'nz_percent'+session_name+data;
			
			var bar = '<div id="'+bar_id+'" style="height:12px;background:#cccccc">&nbsp;</div>';
				bar+= '<div id="'+per_id+'" align="center" style="width:100px;height:12px;line-height:13px;position:absolute;top:'+top+'px;">';
				//bar+= data;
				bar+= '</div>';
			progress_obj.css({
					'border':'1px solid',
					'width':'100px',
					'font-size':'10px',
					'height':'12px',
					'text-align':'left'
				})
				.html(bar)
				.attr('id','nz_inprogress'+session_name+data);
			
			$('#'+obj).show();
			$('#'+bar_id).css({'width':data+'%'});
			$('#'+per_id).html(data+' %');
			
			if(data < 100 || data < '100'){
				if(data=='' || data==0 || data=='0'){checknull++;}
				if(data==old_data){check_error++;}
				if(checknull > 10 || check_error > 30){
					old_data = null;data = null;checknull=null;check_error=null;
					setTimeout(function(){$('#nz_main_progress').remove();},1000);
					return false;//ถ้าได้ค่าว่างวนลูปครบห้ารอบ หรือได้แต่ข้อมูลเดิมให้หยุดทำงาน
				}
				check_old_obj = 'nz_progressbar'+session_name+data;
				setTimeout("get_progress('"+html_obj+"','"+session_name+"','"+checknull+"','"+data+"','"+check_error+"','"+check_old_obj+"')",800);
			}else{
				//$('#'+obj).fadeOut();
				setTimeout(function(){$('#nz_main_progress').remove();},1000);
			}
		}//check null obj
		old_data = null;data = null;checknull=null;check_error=null;
	});
}
//-end progress

// fix head table  **ไม่ใช้แล้ว เปลี่ยนไปใช้ nz_fixhead_table
function nz_modify_table(arr){
	if(typeof(arr) == 'string'){
      	var table = arr;
   }else{
      	var table=arr.table,divHeight=arr.divHeight,blankTD=arr.blankTD,theadClass=arr.theadClass,
      	tbodyClass=arr.tbodyClass,bdHead=arr.bdHead,bdBody=arr.bdBody,headTag=arr.headTag;
   }
	var tb = $('#'+table);
	if(!divHeight){var divHeight = "360px";}
   if(!blankTD){var blankTD = "10";}
   blankTD = blankTD.replace('px','');
	if(!tbodyClass){var tbodyClass = tb.attr('class');}
	if(!theadClass){var theadClass = tb.attr('class');}
	var margin = 'margin-top:'+tb.css('margin-top')+';margin-right:'+tb.css('margin-right')+';margin-bottom:'+tb.css('margin-bottom')+';margin-left:'+tb.css('margin-left');
	var newTable =  '' //'<div id="nz_fix_'+table+'" align="center" style="width: 100%;margin-top:'+marginTop+';border:1px solid #cccccc;">'
					 + '<table width="100%" cellpadding="0" cellspacing="0" border="0" algin="center" style="border:1px solid #cccccc;'+margin+';">'
					 + '<tr><td id="_td_'+table+'" style="padding:0px" ></td>'
					 + '<td id="nz_fix_blank_'+table+'" style="padding:0px;width:'+blankTD+'px"></td>'
					 + '</tr><tr><td colspan="2" style="padding:0px">'
					 + '<div id="nz_fix_div_'+table+'" style="overflow-y: scroll;overflow-x: hidden;height: '+divHeight+';width:100%;">'
					 + '</div></td></tr></table>';
					// + '</div>';
	$('#nz_fix_'+table).remove();
	tb.after(newTable);

	if(headTag==true){
		tb.find('thead tr:last').attr('id','nz_fix_tr_head_'+table);
		var firstTR = tb.find('thead').html();
		var removeTag = 'thead';
	}else{
		var firstTR = tb.find('tr:first').clone().attr('id','nz_fix_tr_head_'+table);//สร้างแถวที่เป็นหัวข้อ
		var removeTag = 'tr:first';
	}
	tb.clone().appendTo('#_td_'+table)
		.attr({'id':'nz_fix_head_'+table,'border':'0','cellspacing':'0','cellpadding':'0'})
			.css({'margin':'0px'})
				.addClass(theadClass).html(firstTR);
	tb.appendTo('#nz_fix_div_'+table)
		.attr({'border':'0','cellspacing':'0','cellpadding':'0'})
			.css({'margin':'0px'})
				.addClass(tbodyClass).find(removeTag).remove();

	var hObj = '',headW='';
	//เช็กว่าแถวแรกเป็น td หรือ th
	if($('#nz_fix_tr_head_'+table+' td').css('width')){ hObj = 'td';}else{ hObj = 'th';}
	//กำหนดความกว้างของ รายการ ตามความกว้างของ หัวข้อ
	var trHead = $('#nz_fix_tr_head_'+table+' '+hObj);
	if(!headTag){
		trHead.each(function(index,obj){
			headW = $(obj).attr('width');	// ความกว้างของหัวข้อ
			var pdl = $(obj).css('padding-left');
			var pdr = $(obj).css('padding-right');
			var pdt = $(obj).css('padding-top');
			var pdb = $(obj).css('padding-bottom');
			//if(index < 3 ){alert(hObj+' = '+ $(obj).html()+' width = '+headW+' padding= '+headP);}
			tb.find('tr:first td:eq('+index+')').attr('width',headW).css({'padding-top':pdt,'padding-right':pdr,'padding-bottom':pdb,'padding-left':pdl});//ปรับความกว้างของรายการให้เท่ากับ หัวข้อ
		});
		//if(headW==""){alert('ยังไม่ได้กำหนด ความกว้างของหัวข้อ ('+hObj+') ในตาราง '+table+' หรือกำหนดไม่ครบ \n หากไม่กำหนด ความกว้างระหว่าง หัวข้อ กับ รายการ จะไม่ตรงกัน \n อย่าลืมไปกำหนดขนาดด้วยล่ะ ^_^');}
	}
	var headTable = $('#nz_fix_head_'+table);
	var bg = headTable.find('tr:first').css('background-color');
	if(bg=="transparent"){
		bg = headTable.find(hObj+':first').css('background-color');
	}
	$('#_td_'+table).css('background-color',bg);
	//กำหนดเส้นขอบของหัวข้อ
	if(headTag){
		trHead = $('#nz_fix_head_'+table+' '+hObj);
	}
	if(bdHead){
		headTable.css('border-width','0px');
		trHead.css({
			'border-style': 'solid','border-width': '1px','border-color': bdHead[0]+' '+bdHead[1]+' '+bdHead[1]+' '+bdHead[0],
		   'padding': '5px',
		   'height': '20px'
		});
		$('#nz_fix_blank_'+table).css({
			'border-style': 'solid','border-width': '1px','border-color': bdHead[0]+' '+bdHead[1]+' '+bdHead[1]+' '+bdHead[0],
		   'background-color' : bg
		});
	}else{
		// กำหนดสี td ด้านขวาสุด
		var bdL = headTable.find('tr:first '+hObj).css('border-left-color');
		var bdT = headTable.find('tr:first '+hObj).css('border-top-color');
		var bdR = headTable.find('tr:first '+hObj).css('border-right-color');
		var bdB = headTable.find('tr:first '+hObj).css('border-bottom-color');
		$('#nz_fix_blank_'+table).css({'background' : bg ,'border-style':'solid','border-width':'1px','border-color':bdT+' '+bdR+' '+bdB+' '+bdL});
	}
	//กำหนดเส้นขอบของรายการในตาราง
	if(bdBody){
		tb.css('border-width','0px');
		$('#'+table+' td').css({
			'border-style': 'solid',
		   'border-width': '1px',
		   'border-color': bdBody[0]+' '+bdBody[1]+' '+bdBody[1]+' '+bdBody[0],
		   'padding': '5px',
		   'height': '20px'
		});
	}
}

/**
  * ซ่อน element ที่ส่งมา
  * obj = .element | #element | ตามรูปแบบ selector ของ jQuery
  * tm = เวลาที่ต้องการหน่วง
  */
function delay_close(obj,tm){
	if(!tm){tm = 3000;}
	setTimeout(function(){$(obj).fadeOut();},tm);
}

