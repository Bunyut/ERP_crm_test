/*followcus.js is javascript for Phase_F
	หน้าที่เรียกสคริปต์นี้ => headpage.html
	หน้าที่ใช้ฟังก์ชั่นจากสคริปต์นี้ => ic_followcus.html
*/
var ajaxLoader = "<div align='center'><img src='img/loader_100x100.gif'></div>";
var smallLoader = "<div align='center'><img src='img/loader_32x32.gif'></div>";

function nzCheckForm(arr){ //รับตัวแปรแบบอาร์เรย์ {formID:'id ของฟอร์ม', errorMsg:'ข้อความที่จะให้แสดง', debug:'ใช้ตรวจดู input ที่เป็นค่าว่าง'}
var n = 0, num = 0, error = 0, A= [], B= [], curName='', oldName='', curID = '',curType='',oldID, chkVal = 'start',txt='', errorMsg='',tagname,formID,formObj,obj,debug, noAlert='';

if(arr){
      if(typeof(arr) == 'string'){
             formID = arr;
      }else{
            formID = arr['formID'];
            debug = arr['debug'];
            errorMsg = arr['errorMsg'];
            noAlert = arr['noAlert'];
      }
}
if(!errorMsg){ errorMsg = 'กรอกข้อมูลในแบบฟอร์มให้ถูกต้องด้วยครับ.';}
var require 	= "<span class='require' style='color:red'>* กรอกข้อมูล</span>";
var chkrequire = "<span class='require' style='color:red'>* เลือกอย่างน้อย 1 ตัวเลือก<br></span>";

if(formID){ //เช็กฟอร์มที่ระบุ
   var checkID = $("#"+formID).attr('id');
    if(!checkID){ //กรณีที่ชื่อฟอร์มที่ส่งมา ไม่ได้กำหนดแอตทริบิว id
         $("form[name='"+formID+"']").attr('id',formID);
    }
    if($("#"+formID).attr('id')){
       formObj = $("#"+formID+" input:not(:button,:submit),#"+formID+" select,#"+formID+" textarea");
    }else{ //ถ้า id ที่ส่งมาไม่มีอยู่จริง จะเช็กทุกฟอร์ม
          formObj = $("form input:not(:button,:submit),form select,form textarea");
    }
}else{ //เช็กทุกฟอร์ม
    formObj = $("form input:not(:button,:submit),form select,form textarea");
}
$(".require").remove();
 formObj.removeClass('require_box');//ล้างกรอบสีแดง
 formObj.not('.nocheck').each(function(){
      if($(this).attr('disabled')!=true){
      num++;
        curType = this.type;
        curID	 = this.id;
        if(!curID){//ถ้าไม่มี id
            A.push(curID);	//เก็บ id เดิมไว้
            curID = curName.split('[]');
            curID = curName[0]+num;
            B.push(curID); //เก็บ id ที่สร้างใหม่
            this.id = curID;
        }
        if(curType=="checkbox" || curType=="radio"){
        	 curName = this.name;
        	 if(curName!=oldName || num==1){//checkbox,radioเป็นกลุ่ม ถ้าเปลี่ยนชื่อแสดงว่าครบทุกตัวแล้ว หากไม่ได้เช็ก ตัวแปร chkVal จะเป็นค่า ว่างๆ
	         if(chkVal==""){//ค่าของชุดที่แล้ว
                                    if(n==0){obj = oldID;n=1;}
                                    $('#'+oldID).before(chkrequire);
                                    $('input[name="'+oldName+'"]').addClass('require_box');
                                    error++;
                               }
                              oldID = curID;
                              chkVal = "";
	}
         // if(chkVal==null){chkVal="";}
          if(this.checked==true){
                chkVal += this.value;
          }

          oldName = curName;
        }else{ //type = text ,textarea หรือ  select box
				if(chkVal==""){//ต้องเพิ่ม ถ้าเป็นตัวสุดท้าย checkbox ก่อนหน้าจะไม่ทำงาน
					if(n==0){obj = oldID;n=1;}
				}
				if(this.value==""){
					tagname = $(this)[0].tagName.toLowerCase();
					txt = require;
					if(tagname=="select"){txt = "<span class='require' style='color:red'>* เลือกข้อมูล</span>";}
                	$(this).after(txt);
                	//$(this).css({'border':'1px solid #FF8247'});
                	$(this).addClass('require_box');
               	if(n==0){obj = this.id;n=1;}
                	error++;
				}
        }
      }//end if
    });//end each
    if(chkVal==""){//ตรวจสอบ checkbox , radio ตัวสุดท้าย เพราะไม่สามารถ วนไปตรวจสอบเงื่อนไข if(curID!=oldID)
    	if(n==0){obj = oldID;n=1;}
        $('#'+oldID).before(chkrequire);
        $('input[name="'+oldName+'"]').addClass('require_box');
        error++;
    }
    for(i=0;i<A.length;i++){//วนลูปคืนค่า id เดิมให้กับอีเลเมนต์ที่โดนเปลี่ยนค่า id
        $("#"+B[i]).attr('id',A[i]);
    }
    if(error>0 || $(":input").next().is(".require")==true){//ตรวจสอบว่ามี error หรือไม่ ถ้ามีแสดงว่าในฟอร์มมีค่าว่าง
            $("#"+obj).focus();
            if(!noAlert){ alert(errorMsg); }
            if(debug){alert("input ที่ไม่ผ่านคือ : "+obj);}
            return false;
    }else{
        return true;
    }
}

//begin ajax
function followFormSubmit(div,url,param,formID,callback){
	var serialData,object;
	if(formID){//ถ้า submit แบบระบุชื่อฟอร์ม จะแยกค่า checkbox ออกมาต่างหาก
		serialData = $("#"+formID).serialize();
	}else{
		serialData = $("form").serialize();
	}
	var formdata = decodeURIComponent(serialData);
	if(param){	formdata += "&"+param;	}
	//alert('FORM : '+formID +'\n\n'+'URL : '+url+'\n\n PARAMITER : '+ formdata);
	object = create_object(div);//ใช้ในกรณีที่ไม่ต้องการแสดงผลลัพธ์ แต่ต้องการ echo javascript
	$.post(url, formdata, function(result){
		object.html(result);
		if(callback){ callback(result); }//ฟังก์ชั่นที่ต้องการทำงานต่อ
	});
}
//ตรวจสอบ element ที่ใช้แสดงผล ถ้าว่าง ให้สร้าง element ใหม่ สำหรับรับค่า javascript ที่ echo ออกมา
function create_object(div){
	var obj,el,check;
	if(div){
		obj = $('#'+div);
		obj.show();
		obj.html("<div align='center'>"+ajaxLoader+"<br> กรุณารอสักครู่...");
	}else{
		el = jQuery("<div style='display:none'></div>").attr('id', 'hidden_div');
		check = $("#hidden_div").attr('id');
		if(!check){//ถ้ายังไม่ได้สร้าง
			el.appendTo(document.body);
		}
		obj = $('#hidden_div');
	}
	return obj;
}
function followGetData(div,url,param,callback){
	var object = create_object(div);//สร้างออบเจ็กต์
	$.get(url,param,function(result){
		object.html(result);
		if(callback){ callback(div,url,param,result); }//ฟังก์ชั่นที่ต้องการทำงานต่อ
	});
}
function followPostData(div,url,param,callback){
	var object = create_object(div);//สร้างออบเจ็กต์
	$.post(url,param,function(result){
		object.html(result);
		if(callback){ callback(div,url,param,result); }//ฟังก์ชั่นที่ต้องการทำงานต่อ
	});
}

//สร้างป๊อบอัพกลางจอ คลาสที่ใช้ nz_content CTwhite อยู่ใน css/nz_button.css (ใช้ตัวนี้)
function nz_popup_center(sendType,sendURL,param,callback,config){
	var popup,newElement,el,object,border='',background='',title_text='',title_color='',title_style='',title_div='';
	if(config){
		border = config.border;
		background=config.background;
          title_text = config.title;
          title_color = config.color;
	}
	popup = "nz_popup_div";
	if($('#'+popup).attr('id')){
		$('#'+popup).remove();
	}
	newElement = "<div class='nz_content CTwhite' style='padding:0px;display:none;position:absolute;z-index:1;'></div>";
	el = jQuery(newElement).attr('id', popup);
	el.appendTo(document.body);

 	object = $('#'+popup);
	object.css({'border':border,'background':background}).html("<div align='center'>"+smallLoader+"<br> กรุณารอสักครู่...");
	nzShowCenter(popup);
	sendType=sendType.toUpperCase();
     var title_bg = object.css('border-top-color');
     if(title_text){
         title_style = 'background:'+title_bg+'; color:'+title_color+'; padding-top:2px; padding-left:2px; padding-bottom:2px; height:20px;';
         title_div = '<div style="float:left;line-height:20px;font-weight:bold">'+title_text+'</div>';
     }
      var close_button = '<div id="nz_title" align="left" style="'+title_style+';cursor:move">';
          close_button += title_div;
        close_button += '<img align="absbottom" onclick="$(\'#nz_popup_div\').remove();" title="ปิด" src="img/closed.png" style="cursor:pointer;float:right" onmouseover="this.style.opacity=0.8" onmouseout="this.style.opacity=1"></div>';
	$.ajax({
	   type: sendType, url: sendURL, data: param,
	   success: function(result){
	   	object.html(close_button).append('<div style="padding:10px;">'+result+'</div>');
	   	nzShowCenter(popup);
			if(callback){ callback();}
         $("#nz_popup_div").draggable({handle:   '#nz_title',opacity: 0.8});
		}
	});
}

function pf_auto_complete(url,inputBox,hiddenBox,callback){//inputBox ส่งมาแบบ #id
	$(inputBox).autocomplete({
		delay : 500,//milliseconds #naizan 2011-05-20
		source: url,
		minLength: 2,
		select: function( event, ui ) {
			if(hiddenBox){
				$(hiddenBox).val('');
				var tag = $(hiddenBox).get(0).tagName.toLowerCase();
				if(tag=="input"){	$(hiddenBox).val(ui.item.hidden_value);}else{$(hiddenBox).html(ui.item.hidden_value);}
			}
			if(callback){callback(ui);}
		},
		search: function(event, ui) {
			if(hiddenBox){	$(hiddenBox).val('');}//เคลียร์ค่าเมื่อเริ่มค้นใหม่
		}
	});
	//ใช้กับ php ที่ echo ค่าอาร์เรย์ ในรูปแบบ json_encode($array);
}

//ถ้าจะป๊อบอัพ เรียกฟังก์ชั่น nz_popup_center() แทน ตัวนี้เลิกใช้แล้ว
function nzPopupCenter(display_el,url,param,callback){//ฟังก์ชั่นเก่า ต้องระบุ div ที่ใช้
	var object,display,newElement;
	if(display_el){
		object = $('#'+display_el);
		display = display_el;
	}else{
		newElement = "<div class='nz_content CTwhite' style='display:none;position:absolute;'></div>";
		el = jQuery(newElement).attr('id', 'div_popup');
		check = $("#div_popup").attr('id');
		if(!check){//ถ้ายังไม่ได้สร้าง
			el.appendTo(document.body);
		}
		object = $('#div_popup');
		display = "div_popup";
	}
	object.html("<div align='center'>"+smallLoader+"<br> กรุณารอสักครู่...");
	nzShowCenter(display);
	$.get(url,param,function(result){
		object.html(result);
		nzShowCenter(display);
		if(callback){ callback();}
	});
}//ตัวนี้เลิกใช้แล้ว

//end ajax

function nz_get_formdata(formID){//อ่านข้อมูลในฟอร์ม
	var formdata='';
	if(formID){//แบบระบุ id ฟอร์ม
		checkID = $("#"+formID).attr('id');
		if(checkID){ formdata = $("#"+formID).serialize();	}
		else{ alert("ไม่มี <form> ที่ตรงตาม id='"+formID+"' ที่ระบุครับ");	}
	}
	else{ formdata = $("form").serialize();}
	if(formdata){ formdata = decodeURIComponent(formdata);}
	return formdata;
}

/* ใช้งานยาก รอเปลี่ยนไปใช้ PHP รับค่าแล้ววนลูปหาเอา */
function submitAnswerForm(obj,url,param,formID){//ใช้กับแบบฟอร์มโทรหาลูกค้าเท่านั้น
	var ques_list_id;
	var arr = [];//เก็บค่าหลังจาก split
	var A= [];
	var B= [];
	var curname = '';
	var curID = '';
	var formData = '';
	var serialData;
	$('#'+formID+' .quesList').each(function(){//วนหนึ่งรอบต่อหนึ่งข้อ
		var chk = 0;//ใช้นับ checkbox
		serialData = '';

		div = this.id;
		arr = div.split('_');
		ques_list_id = arr[1];

		$("#"+div+" input:not(:disabled)").each(function(){//วนหนึ่งรอบต่อ input หนึ่งตัว
			curname = $(this).attr('name');
			curID = curname.split('[]');
	        curID = curID[0];
	        A.push(this.id); //เก็บ id เดิมไว้ บางที id="car[]" จะใ้ช้กับ jquery ไม่ได้
	        B.push(curID); //เก็บ id ที่สร้างจาก name ที่ตัด [] ออกไปแล้ว จะได้ id="car"
	        this.id = curID;
        	curname = this.id;
			inputValue = $(this).val();
			inputType = $(this).attr('type');

			sign = "&"+curname+"="+ques_list_id+"=>\'";// ใช้ => แยก id ของ คำถามออกจาก value


			if(inputType=="text"){
				if(inputValue==""){sign = '';}
				serialData += sign+inputValue;

			}else if(inputType=="checkbox"){
				if(this.checked){
					chk++;
					if(chk>1){ sign="','"; } //ถ้าเป็น checkbox ในข้อเดียวกันให้เอาทุกตัวมารวมเป็นค่าเดียวกัน name = value1,value2,value3
					serialData += sign+inputValue;
				}
			}else if(inputType=="radio"){
				if(this.checked){
					serialData += sign+inputValue;
				}
			}
			
		});

		$("#"+div+" select:not(:disabled)").each(function(){
			curname = $(this).attr('name');
			inputValue = $(this).val();
			inputType = $(this).attr('type');
			if(curname==""){curname==this.id;}
			sign = '&'+curname+'='+ques_list_id+"=>\'";
			if(inputValue==""){sign = '';}
			serialData += sign+inputValue;
		});

		$("#"+div+" textarea:not(:disabled)").each(function(){
			curname = $(this).attr('name');
			inputValue = $(this).val();
			inputType = $(this).attr('type');
			if(curname==""){curname==this.id;}
			sign = '&'+curname+'='+ques_list_id+"=>\'";
			if(inputValue==""){sign = '';}
			serialData += sign+inputValue;
		});
		
		//alert(curname);
		if(serialData!=''){
			formData += serialData + "\'";
		}
		
	});	// รูปแบบที่ได้ &textbox1=Hello&textbox2=MynameisZan&checkbox1=Pic1,Pic3,Pic9&selectbox=Animal

	for(i=0;i<A.length;i++){//วนลูปคืนค่า id เดิมให้กับอีเลเมนต์ที่โดนเปลี่ยนค่า id
        $("#"+B[i]).attr('id',A[i]);
    }
	
	//เก็บค่าคำถามที่ไม่ใช้
	var no_use="";
	$('input.nouse:checked').each(function(){
		div = this.id;
		arr = div.split('_');
		ques_list_id = arr[1];
		no_use += '&'+$(this).attr('name')+'='+ques_list_id+'=>'+$(this).val();
	});
	
	var resultData = formData;
	if(param){ resultData = formData+"&"+param;	}

	//alert('URL : '+url+'\n\n DATA : '+ formData+'\n\n DISPLAY : '+obj);//test
	//return false;//test

	if(formData){
		//followPostData(obj,url,resultData);
		$.post(url,resultData+no_use,function(myJSONtext){//บันทึกคำตอบ
			var myObject = eval('(' + myJSONtext + ')');//แปลง json เป็นอาร์เรย์
			$('#'+obj).html(myObject.message);//แสดงข้อความว่าบันทึกแล้ว
			if(myObject.postdata){
				checkNumForm(myObject.postdata );	//กรณีที่หนึ่ง id มีแบบฟอร์มคำถามมากกว่า 1 ฟอร์ม
			}
		});

	}else{
		alert('คุณยังไม่ได้กรอกข้อมูลเลยครับ.');
		return false;
	}
}

//ถ้าบันทึกการโทรหาแล้ว = submit ทุกแบบฟอร์ม ให้ซ่อนเหตุการณ์นั้น เรียกมาจาก $func == "SendAnswer"
function checkNumForm(postdata){
	var num = $("#div_question form").length;
	if(!num){//ถ้าหมดทุกแบบฟอร์มคำถามแล้ว
		url = 'ic_followcus.php?func=setStatus';
		followPostData("hidden",url,postdata);
	}
}

var old_toggle_id=null;
function follow_toggle(div,url,id){
	var obj = $("#TR"+id);
	if(id!=old_toggle_id && old_toggle_id){//ซ่อนตัวแถวที่เคยเปิดก่อนหน้านี้
		$("#TR"+old_toggle_id).attr('title','ขยาย');
		$("#cus"+old_toggle_id).hide();
		$("#detail"+id).hide();
	}
	if(obj.attr('title') == "ย่อ"){
		obj.attr('title','ขยาย');
		$("#detail"+id).toggle('blind',500);
		$("#cus"+id).fadeOut();
	}else{
		obj.attr('title','ย่อ');
		$("#cus"+id).fadeIn('fast');
		$("#detail"+id).toggle('blind',500);
		followGetData(div,url); //โหลดข้อมูลเมื่อคลิกขยาย
	}
	old_toggle_id = id;
}

function openCusEvent(div,url,follow_id){
	$.get('ic_followcus.php?func=checkProcess','follow_id='+follow_id,function(result){
		var txt = result.split("<->");
		var alertText = txt[1];
		var showText = txt[2];
		if(alertText==true){
			alert(alertText);
			$("#process"+follow_id).show().html(showText);
		}else{
			followGetData(div,url,'');
		}
	});
}

//ซ่อนแสดงปุ่ม
var oldButton,oldContent;
function changeObject(curButton,curContent){
	if(curButton != oldButton){
		$("#"+curButton).removeClass('BTwhite');
		$("#"+curButton).addClass('BTblack');
		$("#"+curContent).show();
		$("#"+oldButton).removeClass('BTblack');
		$("#"+oldButton).addClass('BTwhite');
		$("#"+oldContent).hide();
	}
	oldButton = curButton;
	oldContent = curContent;
}

//var breake=null;//ตัวแปรนี้ใช้สำหรับควบคุมฟังก์ชั่นนี้ เพราะเวลาคลิกลงบนปุ่มที่อยู่ในแถวที่ใช้ฟังก์ชั่นนี้ จะเรียกฟังก์ชั่นนี้ด้วย ดังนั้นจะใส่ breake=true เมื่อไม่ต้องการเรียกฟังก์ชั่นนี้
function showQuestionDetail(url,url2,tr){// จุดอ้างอิงที่จะแสดง , url ที่จะส่งไป , พื้นที่แสดงข้อความ
	//if(breake ==false){//ถ้าคลิกที่ปุ่ม ยกเลิกการติดตาม จะได้ค่าเป็น true
		var offset = $("#"+tr).offset(); //กำหนดจุดเริ่มแสดงผล
		var posX = Math.floor(offset.left);
		var posY = Math.floor(offset.top);
		var divMainDetail = $('#divMainDetail');
		var display = divMainDetail.css('display');
		var div = $('#div_script');
		var bt = $("#bt_script");
		div.show();
		bt.show();
		div.html(ajaxLoader);
		divMainDetail.css({left : posX, top : posY+25});
		divMainDetail.fadeIn('slow');
		$.get(url,'',function(result){
			div.html(result);
			if(url2){//ถ้าเป็นหัวหน้าเข้ามา url2 ในส่วนของคำถาม จะไม่ส่งมาด้วย
				$("#bt_question").show();
				$("#div_question").hide();
				$.get(url2,'',function(result){
					$("#div_question").html(result);
				});
			}
		});
		$("#cursor").css({left : posX, top : posY+5}).show();
		$("#blank").css({'left' : '0', 'top' : '0' ,'width' : '100%' ,'height': gWH().height}).show().click(function(){
			hideObj('divMainDetail,cursor,blank');
		});
		bt.removeClass('BTwhite');
		bt.addClass('BTblack');
	//}
}


//ซ่อนแสดงปุ่ม รายละเอียด แบบฟอร์มคำถาม
function CRchangeObject(typeShow){
	if(typeShow=="script"){
		showButton = "bt_script";
		showContent = "div_script";
		hideButton = "bt_question";
		hideContent = "div_question";
	}else if(typeShow=="question"){
		showButton = "bt_question";
		showContent = "div_question";
		hideButton = "bt_script";
		hideContent = "div_script";
	}
	$("#"+showButton).show();
	$("#"+showContent).show();
	changeClass(showButton,'BTwhite','BTblack');

	$("#"+hideContent).hide();
	$("#tooltip").hide();
}

//ใช้เฉพาะ CR เท่านั้น ******
var oldRow;
var oldType;
function CRshowQuestionDetail(tdRef,typeShow,url){
	var divContent;
	var bt;
	var offset = $("#"+tdRef).offset(); //กำหนดจุดเริ่มแสดงผล
	var posX = Math.floor(offset.left);
	var posY = Math.floor(offset.top);

	var curRow = tdRef;
	var curType = typeShow;
	var divMainDetail = $('#divMainDetail');
	var display = divMainDetail.css('display');
	if(typeShow=="script"){//ตรวจสอบว่าคลิกไอคอนไหน
		div = "div_script";
		hideButton = "bt_question";
	}else if(typeShow=="question"){
		div = "div_question";
		hideButton = "bt_script";
	}

	var h = $("#"+div).height();
	if(h==0){h=300;}

	if(curRow != oldRow || curType != oldType){ //จะโหลดเฉพาะข้อมูลใหม่ เมื่อคลิกไอคอนแถวใหม่ หรือไอคอนใหม่
		$("#bt_script").addClass('BTwhite');
		$("#bt_question").addClass('BTwhite');
		if(curRow != oldRow){ //ถ้าคลิกแถวใหม่ จะแสดงเอฟเฟกต์ขยายกล่องข้อความ
			$("#"+hideButton).hide();
			$('#div_script').hide();
			$('#div_question').hide();
			$('#div_script').html('');
			$('#div_question').html('');
			divMainDetail.css({width:10 , height:10 ,left : posX, top : posY});
			divMainDetail.animate({width: '90%', height: h, left : 25 }, 500,function(){
	  			CRchangeObject(typeShow);
			});

		}else if(display=="none"){ // แถวเดิม ไอคอนใหม่ เลเยอร์ถูกซ่อนอยู่
			divMainDetail.css({width:10 , height:10 ,left : posX, top : posY});
			divMainDetail.animate({width: '90%', height: h, left : 25 }, 500,function(){
	  				CRchangeObject(typeShow);
				});
		}else{ // แถวเดิม คลิกไอคอนใหม่ กล่องข้อความแสดงอยู่แล้ว
			CRchangeObject(typeShow);
		}
		followGetData(div,url,''); //ajax send data

	}else if(display=="none"){ // แถวเดิม คลิกไอคอนเดิม แต่กล่องข้อความถูกซ่อนอยู่  ให้สร้างเอฟเฟกต์ขยายใหม่
		if($('#'+div).html()==""){ //ถ้าไม่มีข้อมูล ให้โหลดใหม่
			followGetData(div,url,''); //ajax send data
		}
		divMainDetail.css({width:10 , height:10 ,left : posX, top : posY});
		divMainDetail.animate({width: '90%', height: h, left : 25 }, 500,function(){
	  		CRchangeObject(typeShow);
		});
	}else{//ถ้าคลิกที่เดิมให้โหลดข้อมูลใหม่อีกครั้ง ถ้าไม่ต้องการให้โหลดซ้ำ ปิดฟังก์ชั่นนี้ได้
		followGetData(div,url,'');
	}
	$("#cursor").css({left : posX-5, top : posY+5});
	$("#cursor").show();
	divMainDetail.show();
	oldRow = curRow;
	oldType = curType;
}

function showIcon(obj,e){//CR
	var icon = $("#"+obj).html();
	var display = $("#tooltip");
	var posX = 0;
	var posY = 0;
	if (!e){var e = window.event;}
	if (e.pageX || e.pageY){
		posX = e.pageX;
		posY = e.pageY;
	}else if (e.clientX || e.clientY){
		posX = e.clientX;
		posY = e.clientY;
	}
	display.css({left : posX, top : posY});
	var cls = "<img src='img/closed.png' style='cursor:pointer;' onclick=\"hide('tooltip');\" title='ปิดกล่องข้อความ'>" ;
	display.html(icon+cls);
	display.show();
}//ใช้แค่ CR เท่านั้น ******************* (รอแก้ไข)

//oeditcus.php แก้ไขข้อมูลวันที่ทะเบียนหมดอายุ วันที่จ่ายงวดแรก
function showPopup(div,url,param){
	var offset = $("#"+div).offset(); //กำหนดจุดเริ่มแสดงผล
	var posX = Math.floor(offset.left);
	var posY = Math.floor(offset.top);
	var display = $("#PopupSellUpForm");
	var w = (display.width()/2)+100;
	display.css({left : posX-w, top : posY});
	display.show();
	followGetData('PopupSellUpForm',url,param);
	$("#div_edit_button").hide();
}

function selectDate(obj){//datepicker
	var defaultDate = $("#"+obj).val().split("/");
	var defaultYear = defaultDate[2]-543;
	$("#"+obj).live('keydown', function() {return false;});//ไม่ให้พิมพ์เอง
	var dateBefore= defaultDate[0]+"/"+defaultDate[1]+"/"+defaultYear;
	//var deteBefore=null;
	$( "#"+obj ).datepicker({
		dateFormat: 'dd/mm/yy',
		changeMonth: true,
		changeYear: true,
      beforeShow:function(){
            if($(this).val()!=""){
                var arrayDate=$(this).val().split("/");
                arrayDate[2]=parseInt(arrayDate[2])-543;
                $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);
            }
            setTimeout(function(){
                $.each($(".ui-datepicker-year option"),function(j,k){
                    var textYear=parseInt($(".ui-datepicker-year option").eq(j).val())+543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            },50);

        },
        onChangeMonthYear: function(){
            setTimeout(function(){
                $.each($(".ui-datepicker-year option"),function(j,k){
                    var textYear=parseInt($(".ui-datepicker-year option").eq(j).val())+543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            },50);
        },
        onClose:function(){
            if($(this).val()!="" && $(this).val()==dateBefore){
                var arrayDate=dateBefore.split("/");
                arrayDate[2]=parseInt(arrayDate[2])+543;
                $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);
            }
        },
        onSelect: function(dateText, inst){
            dateBefore=$(this).val();
            var arrayDate=dateText.split("/");
            arrayDate[2]=parseInt(arrayDate[2])+543;
            $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);
        }
	});
}

function view_input(inputname){
	var html,opt,addBR;
	var div = $("#view_input");
	var input_type = $("#"+inputname).val();

	switch(input_type){
		case "textbox":
  			html  = "<input type='text' size='40' class='nocheck'>";
 			break;
		case "textarea":
			opt = true;
  			html  = "<textarea id='textarea' rows='3' cols='35' class='nocheck nz_textarea'></textarea>";
 			break;
 		case "checkbox":
 			opt = true;
 			addBR = true;
  			html  = "<input type='checkbox' name='ex' id='ex1' class='nocheck'>";
  			html += "<label for='ex1' style='cursor:pointer'>ตัวเลือก 1</label>";
			html += "<input type='checkbox' name='ex' id='ex2' class='nocheck'>";
			html += "<label for='ex2' style='cursor:pointer'>ตัวเลือก 2</label>";
			html += "<input type='checkbox' name='ex' id='ex3' class='nocheck'>";
			html += "<label for='ex3' style='cursor:pointer'>ตัวเลือก 3</label>";
 			break;
 		case "radio":
 			opt = true;
 			addBR = true;
  			html  = "<input type='radio' name='ex' id='ex1' class='nocheck'>";
  			html += "<label for='ex1' style='cursor:pointer'>ตัวเลือก 1</label>";
			html += "<input type='radio' name='ex' id='ex2' class='nocheck'>";
			html += "<label for='ex2' style='cursor:pointer'>ตัวเลือก 2</label>";
			html += "<input type='radio' name='ex' id='ex3' class='nocheck'>";
			html += "<label for='ex3' style='cursor:pointer'>ตัวเลือก 3</label>";
 			break;
 		case 'selectbox':
 			opt = true;
 			addBR = false;
  			html  = "<select size='0' name='ex' id='ex' style='width:150px' class='nocheck'>";
			html += "<option value='1'>ตัวเลือก 1</option>";
			html += "<option value='2'>ตัวเลือก 2</option>";
			html += "<option value='3'>ตัวเลือก 3</option>";
			html += "</select>";
 			break;
 		case 'pleasure_value':
 			opt	  = true;
 			html  = "<input type='text' style='width:30px;' class='nocheck'>";
 			break;
	}
	div.html(html);
	$("#view").show();
	$("#pleasure_check").css('background-color','').attr('readonly',false);
	$(".ans_option,.ans_option2").attr('disabled',true);//ปิด option ทั้งหมดก่อน แล้วค่อยไปเปิดทีละตัว
	if(opt){
		var option_title,div_opt;
		$(".choice").hide();
		if(input_type=="textarea"){//สำหรับ textarea
			option_title = "กำหนดความสูง";
			div_opt = $("#textarea_choice");
			$("#add_ans_opt2").attr('disabled',false);
		}else if(input_type=="pleasure_value"){//สำหรับ ช่องคะแนนความพึงพอใจ
			option_title = "กำหนดคะแนน<br>ความพึงพอใจสูงสุด";
			div_opt = $("#pleasure_choice");
			$("#add_ans_opt3").attr('disabled',false);
			$("#pleasure_check").val('0').css('background-color','#cccccc').attr('readonly',true);
		}else{
			$(".ans_option").attr('disabled',false);
			var el = "view_input";
			var option;
			var option_val = $("#answer_option").val();
			if(option_val){
				if(input_type=="selectbox"){/*ถ้าเป็น select จะเขียนลงในไอดี ของ select*/
					el = "ex";//id ของ select ที่แสดงตัวอย่าง
				}
				option = create_option(option_val,input_type);
				$("#"+el).html(option);
			}
			option_title = "ตัวเลือกของคำตอบ";
			div_opt = $("#multi_choice");
			
		}
		$("#tr_answer_option").show();
		$("#opt_title").html(option_title);
		div_opt.show();
		if(addBR==false){$("#div_add_br").hide();}else{$("#div_add_br").css('display','inline');}
	}else{
		$("#tr_answer_option").hide();
	}
}

function create_option(val,t){//สร้างรายการตามอินพุต answer_option
	var option_val = val.split(",");
	var count = option_val.length;
	var html = '';
	for(i=0;i<count;i++){
		if(t=="selectbox"){
			html += "<option value='"+option_val[i]+"'>"+option_val[i]+"</option>";
		}else {
			html += "<input type='"+t+"' name='ex' id='ex"+i+"' class='nocheck' value='"+option_val[i]+"'>";
			html += "<label for='ex"+i+"' style='cursor:pointer'>"+option_val[i]+"</label>";
		}
	}
	return html;
}

function add_answer_option(){
	var opt,txt,oldvalue,sign,add_br,br;
	var input_type = $("#div_input_type input:checked").val();
	add_br = document.getElementById('add_br');
	//เคลียร์ enter
	br = "";
	$('.ex_inp br').remove();
	//เพิ่มคำสั่งขึ้นบรรทัดใหม่ด้วย
	if(add_br.checked==true){
		br="<br>";
		$('.ex_inp').append(br);
	}
	//txt 	 = $("#add_ans_opt").val()+br;
	txt 	 = $("#add_ans_opt").val();
	oldvalue = $("#answer_option").val();
	sign 	 = ", ";
	switch(input_type){
 		case "checkbox":
  			opt  = "<span class='ex_inp'> <input type='checkbox' name='ex' id='ex1' class='nocheck'>";
  			opt += "<label for='ex1' style='cursor:pointer'>"+txt+"</label>"+br+"</span>";
 			break;
 		case "radio":
  			opt  = "<span class='ex_inp'> <input type='radio' name='ex' id='ex1' class='nocheck'>";
  			opt += "<label for='ex1' style='cursor:pointer'>"+txt+"</label>"+br+"</span>";
 			break;
 		case 'selectbox':
			opt = "<option value='"+txt+"'>"+txt+"</option>";
 			break;
	}

	if(input_type=="selectbox"){	/*ถ้าเป็น select จะเขียนลงในไอดี ของ select*/
		obj = "ex";//id ของ select ที่แสดงตัวอย่าง
	}else{
		obj = "view_input";
	}

	if(oldvalue==""){//ถ้าเป็นการเพิ่ม option ใหม่ ให้เคลียร์ค่า ตัวอย่างเดิม
		sign = "";
		$("#"+obj).html('');
	}

	$("#"+obj).append(opt);//แสดงตัวอย่าง
	//เก็บค่าที่จะบันทึก
	var result = oldvalue+sign+txt;
	$("#answer_option").val(result);
	//เปลี่ยนความสูงของ textarea ที่เก็บค่าสำหรับบันทึก
	var viewH;
	if(obj=="ex"){
		var h = result.split("<br>").length;
		viewH = h * 16;//ถ้าเป็น select box จะไม่สูงขึ้น
	}else{
		viewH = $("#view_input").height();//ความสูงของ div แสดงตัวอย่าง
	}
	if(viewH<=16){viewH=18;}
	$("#answer_option").css({height:viewH});

	//เคลียร์ค่า
	$("#add_ans_opt").val('');
	//add_br.checked = false;//ถ้าเลือกอยู่ก็ให้เลือกค้างไว้
	$("#add_ans_opt").focus();
}

function del_answer_option(){//ลบ answer option ทีละตัว
	var option_val = $("#answer_option").val().split(",");
	var count = option_val.length;
	var txt = '';
	if(count>0){
		for(i=0;i<count-1;i++){
			sign = ",";
			if(i==0){sign = "";}
			txt += sign+option_val[i];
		}
		$("#answer_option").val(txt);
		var input_type = $("#div_input_type input:checked").val();
		var option = create_option(txt,input_type);//สร้างรายการใหม่
		if(input_type=="selectbox"){
			obj = "ex";
		}else{
			obj = "view_input";
		}
		$("#"+obj).html(option);//แสดงตัวอย่างหลังจากลบแล้ว
		var viewH;
		if(obj=="ex"){
			var h = txt.split("<br>").length;
			viewH = h * 14;//ถ้าเป็น select box จะไม่สูงขึ้น
		}else{
			viewH = $("#view_input").height();//ความสูงของ div แสดงตัวอย่าง
		}
		$("#answer_option").css({height:viewH});
	}
}

// * APPROVE * //
function hideCheckRow(result){
	var check,id;
	document.getElementById('checkAll').checked = false;
	$("input.wait_approve:checked").attr('disabled',true);
	$("#tb_wait_approve tr").has('input.wait_approve:checked').fadeOut();
}

// * จัดการคำถาม manage_question
function nz_manage_question(event_id){
	/*
	$(".event_check").attr('disabled',true);
	$("#frm_event_list").animate( { height: "hide" }, 800 );
	$("#div_manage_question").animate( { left: 0 }, 200 ).animate( { height: "show" }, 700  );
	*/
	followGetData('display_event','ic_followcus_form.php?func=manage_question&event_id='+event_id);
}
function nz_back_to_event(){
	/*
	$(".event_check").attr('disabled',false);
	$("#frm_event_list").animate( { left: 0 }, 200 ).animate( { height: "show" }, 700  );
	$("#div_manage_question").animate( { height: "hide" }, 900 );
	*/
	followGetData('display_event','ic_followcus_form.php?func=');
}
function manage_question_detail(){
	var obj = $("#display_detail");
	var txt = $("#title_text").text();
	var w = obj.width()-5;
	var h = obj.height();
	hideObj('title_text,bt_edit_detail,show_manage_msg');
	showObj('bt_save_detail,edit_box');
	$("#edit_box").html('<textarea id="detail"  class="nz_textarea" style="width:'+w+'px;height:'+h+'px; padding:3px">'+ txt +'</textarea>');
	$("#detail").select();
}

//wait event
function auto_emp_name(val){
	if(val != '' && val.length > 1){
		showBeside('show_emp_name','auto_name',0,20);
		followGetData('show_emp_name','ic_followcus_form.php?func=auto_emp_name&keyword='+val);
	}
}
function follow_get_id_card(id_card,emp_name){
	$("#auto_name").val('');
	$("#show_emp_name").hide();
	$("#who_response").show().val(id_card).attr('checked',true);
	$("#emp_name").html(emp_name);
}

//EVENT FORM
function nzConfirmDel(div,url,param,formID){//popup center
	var formdata;
	if(formID){//ถ้า submit แบบระบุชื่อฟอร์ม
		formdata = $("#"+formID).serialize();
	}else{
		formdata = $("form").serialize();
	}
	$('#'+div).html("<div align='center'>"+smallLoader+"<br> กรุณารอสักครู่...");
	nzShowCenter(div);
	formdata = decodeURIComponent(formdata);
	if(param){	formdata += "&"+param;	}
	$.post(url,formdata,function(result){
		$('#'+div).html(result);
		nzShowCenter(div);
	});
}
function showEventDetail(event_id){//แสดงรายละเอียดเพิ่มเติมที่ซ่อนไว้
	var oldDiv = $(".detail_visible");
	var target = $("#detail_"+event_id);//div ที่ต้องการแสดง
	$(".detail_visible").hide().attr('class','');//ซ่อน div ที่แสดงก่อนหน้านี้
	var offset = $("#event_tr"+event_id).offset();//จุดอ้างอิง
	var hg = Math.floor(target.css('padding-top').replace("px",""));
	hg += Math.floor(target.css('padding-bottom').replace("px",""));
	hg += target.height();
	var t = offset.top - hg;
	target.attr('class','detail_visible').css({top:t}).show();
}
//เรียกใช้เช็กตอนกรอกค่าความพึงพอใจ
function checkMaxValue(objname,limit){//ใช้เช็กค่า ว่าเกินกำหนดหรือไม่
	var obj = $("#"+objname);
	var val = obj.val();
	limit = parseInt(limit);
	if(val>limit){
		alert('ตัวเลข เกินค่าที่กำหนดไว้ (ค่าที่กำหนดไว้ = '+ limit +')');
		//obj.css({'color':'#FF0000','font-weight':'bold','border':'3px solid #FF8247'});
		obj.addClass('checknumber_error');
		obj.select();
		return false;
	}else{
		//obj.css({'color':'#000000','font-weight':'200','border':'1px groove #8B7355'});
		obj.removeClass('checknumber_error');
		return true;
	}
}
//เรียกใช้เช็กตอนกรอกค่าความพึงพอใจ
function check_number(objname){
	var obj, val, len, digit, error;
		obj = $("#"+objname);
		val = obj.val(); //val ของ pleasure check ที่กรอก
	if(val){
		len = val.length;
		for(var i=0 ; i<len ; i++){
			digit = val.charAt(i);
			if(digit < "0" || digit > "9"){
				error += i+",";
			}
		}
		if(error){
			alert('รับค่าเป็นตัวเลขอย่างเดียวครับ');
			//obj.css({'color':'#FF0000','font-weight':'bold','border':'3px solid #FF8247'});
			obj.addClass('checknumber_error');
			obj.select();
			return false;
		}else{
			//obj.css({'color':'#000000','font-weight':'200','border':'1px groove #8B7355'});
			obj.removeClass('checknumber_error');
			return true;
		}
	}else{
		obj.addClass('checknumber_error');
		//obj.css({'color':'#000000','font-weight':'200','border':'1px groove #8B7355'});
		return false;
	}
}
//ฟังก์ชั่นเก่า ** จะไม่ใช้แล้ว (รออัพแก้ไขก่อน จะลบทิ้ง)
function show(element){
	var el = element.split(",");
	var num = el.length;
	for(i=0;i<num;i++){
		id = el[i];
		id = jQuery.trim(id);
		$('#'+id).fadeIn('500');
	}
}//old function

function showObj(element){
	var el = element.split(",");
	var num = el.length;
	for(i=0;i<num;i++){
		id = el[i];
		id = jQuery.trim(id);
		if(id.indexOf('.')>=0 || id.indexOf('#')>=0){
			$(id).fadeIn();
		}else{
			$('#'+id).fadeIn();
		}
	}
}
function hideObj(element){
	var el,num,id;
	el = element.split(",");
	num = el.length;
	for(i=0;i<num;i++){
		id = el[i];
		id = jQuery.trim(id);
		if(id.indexOf('.')>=0 || id.indexOf('#')>=0){
			$(id).hide();
		}else{
			$('#'+id).hide();
		}
	}
}

function enableElement(element){//รับค่าเป็น id
	var el = element.split(",");
	var num = el.length;
	for(i=0;i<num;i++){
		id = el[i];
		id = jQuery.trim(id);
		$('#'+id).attr('disabled', false);
	}
}
function disableElement(element){//รับค่าเป็น id
	var el = element.split(",");
	var num = el.length;
	for(i=0;i<num;i++){
		id = el[i];
		id = jQuery.trim(id);
		$('#'+id).attr('disabled', 'disabled');
	}
}

function nz_toggle_enable(obj,className){//toggle แบบ disable / enable เป้าหมายตามชื่อคลาสที่ส่งมา
	var numChk = $("input."+className+":disabled").length;
	var check = false;
	//ถ้า obj หรือ this ที่ส่งมาเป็น checkbox ให้อิงตามค่า checked
	if($(obj).is(':checkbox')==true){
		check = obj.checked;
	}
	//alert(numChk+' '+check);
	if(numChk > 0 || check == true){//enable ถ้ามีตัวใดตัวหนึ่งถูก disable อยู่ หรือ คลิกเช็กถูก
		$("."+className).attr('disabled',false); 	  //enable ทั้งหมด
	}else{
		$("."+className).attr('disabled','disabled');//disable ทั้งหมด
	}
}

function nz_toggle_check(obj,className){//toggle แบบ เลือกทั้งหมด/ไม่เลือกทั้งหมด เป้าหมายตามชื่อคลาสที่ส่งมา
	var numChk = $("input."+className+":checked").length;
	var check = false,chkbox=false;
	if($(obj).is(':checkbox')==true){
		check = obj.checked;
		chkbox = true;
	}
	if(numChk > 0 && check == false){//ถ้ามีตัวใดตัวหนึ่งเช็กอยู่ หรือ คลิกเครื่องหมายถูกออก
		$("input."+className).attr('checked','');		//uncheck เอาเครื่องหมายออกทั้งหมด
	}else if(chkbox==true && check==false && numChk==0){
		//ไม่ต้องทำไร
	}else{
		$("input."+className).attr('checked','checked');//check เช็กถูกทั้งหมด
	}
}

//---- * 4 ฟังก์ชั่นนี้จะไม่ใช้เพิ่มแล้ว จะเปลี่ยนไปใช้ แบบ toggle แทน----//
function enable_with_className(className){//มีที่เรียกใช้อยู่ยังลบไม่ได้ แต่จะไม่ใช้แล้ว
	$("."+className).attr('disabled',false);
}
function disable_with_className(className){//มีที่เรียกใช้อยู่ยังลบไม่ได้ แต่จะไม่ใช้แล้ว
	$("."+className).attr('disabled','disabled');
}
function checkAllBox(className){//มีที่เรียกใช้อยู่ยังลบไม่ได้ แต่จะไม่ใช้แล้ว
	$("input."+className).attr('checked','checked');
}
function unCheckAllBox(className){//มีที่เรียกใช้อยู่ยังลบไม่ได้ แต่จะไม่ใช้แล้ว
	$("input."+className).attr('checked','');
}
//---end 4 function---//


function checkBox(className){//เช็กว่าคลิกเลือกตัวใดตัวหนึ่งใน class นั้นหรือยัง
	$(".requireCheck").remove();
	var n = $("input."+className+":checked,input[name='"+className+"']").length;
	if(n>0){
		return true;
	}else{
		alert('กรุณาเลือกรายการที่ต้องการก่อนครับ');
		$("input."+className).after("<span class='requireCheck error'>*</span>");
	}
}

var win=null;
function NewWindow(url,winName,settings){
	if(!settings){
		settings = 'top=0,left=0,toolbar=yes,menubar=yes,scrollbars=yes,resizable,directories=1,fullscreen=yes';
	}
	if(win && win.name == winName){win.close();}
	if(!winName){winName = "newWindow";}
	win = window.open(url,winName,settings);
	win.focus();
}

function gWH(){  //ความกว้าง , ความสูง ของหน้าจอ (ไม่ใช่ความสูงของ document *ความสูงของ document = scrollHeight)
    var e = window, a = 'inner';
    if ( !( 'innerWidth' in window ) ){
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
}
function getScrollTop(){  //ตำแหน่งขอบบน ของจอภาพขณะนั้น
    var e = document.documentElement || document.body;
    return e.scrollTop;
}
function get_el_offset(element){
	var pos = new Array();
	var offset = $("#"+element).offset();
	pos['x'] = Math.floor(offset.left);
	pos['y'] = Math.floor(offset.top);
	return pos;
}

//popup
//เซ็ตให้ display อยู่ตรงจุดที่กำหนด (pointRefer)
//pX , pY จะเลื่อนตำแหน่งโดยอ้างอิง pointRefer ถ้า pX เป็น - จะเลื่อนไปซ้าย ถ้า + เลื่อนไปขวา / ส่วน pY เป็น - จะเลื่อนขึ้น ถ้า + จะเลื่อนลง
function showBeside(display,pointRefer,pX,pY){
	var el = $("#"+display);
	var offset = get_el_offset(pointRefer);
	if(pX){ offset.x += parseInt(pX);}
	if(pY){ offset.y += parseInt(pY);}
	el.css({left : offset.x, top : offset.y});
	el.fadeIn();
}
//เซ็ตให้ display_el อยู่ตรงกลางจอ
function nzShowCenter(display){
	var el,px,py,halfW,halfH;
	el = $("#"+display);
	halfW = gWH().width/2;////ตำแหน่งกึ่งกลางแนวนอน (จะคงที่ตลอด เพราะไม่ให้มี scroll bar ทางแนวนอน)
	halfH = (gWH().height/2) + getScrollTop();//ตำแหน่งกึ่งกลางแนวตั้ง ของหน้าจอขณะนั้น
	px = halfW - (el.width()/2);
	py = halfH - (el.height()/2) - 20;
	if(py<1){py=0;}
	el.css({left : px, top : py,'position':'absolute'});
	el.fadeIn();
}
//เซ็ตให้ display จุดที่ตัวชี้อยู่ (ตามเมาส์)
function nzShowOnPointer(display,e){//e = ฟังก์ชั่นที่ส่งมา
	var posX = 0;
	var posY = 0;
	var el = $("#"+display);
	if (!e){var e = window.event;}
	if (e.pageX || e.pageY){
		posX = e.pageX;
		posY = e.pageY;
	}else if (e.clientX || e.clientY){
		posX = e.clientX;
		posY = e.clientY;
	}
	posX += 10;
	posY += 10;
	el.css({left : posX, top : posY, 'position':'absolute'});
	el.fadeIn();
}
//end popup

function notEnter(e){
	var keycode;
	if (window.event){
		keycode = window.event.keyCode; // ใช้ IE
	}else if (e){
		keycode = e.which; // ใช้ Firefox
	}
	if(keycode=="13"){
		return false;
	}
}

var oldBtt,newCss,oldCss;
function changeClass(curBtt,remCss,addCss){
	if(typeof(curBtt)=='string'){
		var curButton = $("#"+curBtt);
	}else{
		var curButton = $(curBtt);
	}
     if(remCss && addCss){
          $('.'+addCss).removeClass(addCss).addClass(remCss);//เพิ่มใหม
     }
	curButton.removeClass(remCss).addClass(addCss);

	if(oldBtt){
		if(oldBtt != curBtt){
			if(typeof(oldBtt)=='string'){
				var oldButton = $("#"+oldBtt);
			}else{
				var oldButton = $(oldBtt);
			}

			oldButton.removeClass(newCss).addClass(oldCss);
		}
	}
	oldBtt	= curBtt;
	newCss	= addCss;
	oldCss	= remCss;
}

//ถ้าเลือก option ตัวไหนแล้ว select ที่เหลือจะเลือก option ตัวนั้นไม่ได้
var old_index = new Array();
function nz_changeSelect(obj){//จัดกลุ่ม select ด้วยชื่อ class
	var curObj = obj.id;
	var arr = $(obj).attr('class').split(" ");
	var cls = arr[0];
	var index = old_index[curObj];
	var selectIndex = obj.selectedIndex;
	$('select.'+cls).not(obj).each(function(){	//jquery วนลูปชื่อคลาส
		if(obj.value !=""){
			this.options[selectIndex].disabled = true;
		}
		if(index && index != selectIndex){
			this.options[index].disabled = false;
		}
	});
	old_index[curObj] = selectIndex;
}

function goToAnchor(nameAnchor){//โฟกัสไปที่ ID ที่ต้องการ
	if(nameAnchor){
		var anc = document.getElementById(nameAnchor);
		var top = anc.offsetTop - 10;
		//anc.className = 'anchor_hilight';
		$(anc).addClass('anchor_hilight');
		//window.location.hash=nameAnchor;
		window.scrollTo(0,top);
	}
}

function create_wait_event(event_id, prev_event_id){
	if(nzCheckForm('frm_wait_event')==true){
		var resperson = document.getElementById('who_response');
		if(document.getElementById('who_response').disabled==false && (resperson.value=='' || resperson.checked==false) ){
			alert('ระบุผู้ติดตามด้วยครับ');
			document.getElementById('auto_name').focus();
		}else{
			//followFormSubmit('wait_event_form','ic_followcus_form.php?func=add_event', 'event_id='+event_id+'&prev_event_id='+prev_event_id, 'frm_wait_event');
			var serialData = $("form#frm_wait_event").serialize();
				serialData = decodeURIComponent(serialData);
				serialData += '&event_id='+event_id+'&prev_event_id='+prev_event_id;
			$.post('ic_followcus_form.php?func=add_event', serialData, function(result){
				$('#wait_event_form').html(result);
			});
		}
	}
}

/**
 * ฟังก์ชั่นคลิกเลือกใช้/ไม่ใช้คำถามนั้นๆ
 * @param obj ตัวที่คลิกเลือก
 * @param q_list_id รหัสคำถามสำรหับหาไอดี
 */
function nz_use_question(obj,q_list_id)
{
	if(obj.checked==true){
		obj.value = 'no_use';
		$('#div_q'+q_list_id).css('background','#ececec').find('.c_title')
			.css({'text-decoration':'line-through','color':'red'});
		$('#div_q'+q_list_id).find('select,input:not(.nouse),textarea')
			.attr({'disabled':true,'readonly':true}).addClass('nocheck');
	}else{
		obj.value = '';
		$('#div_q'+q_list_id).css('background','').find('.c_title')
			.css({'text-decoration':'none', 'color':'#000000'});
		$('#div_q'+q_list_id).find('select,input:not(.nouse),textarea')
			.attr({'disabled':false,'readonly':false}).removeClass('nocheck');
	}
}

/**
 * ตรวจสอบคะแนนในช่องคำแนนความพึงพอใจ
 * กรณีที่มีคำถามเกี่ยวกับความพึงพอใจด้วย
 */
function check_pleasure(form_id)
{
	if($('#'+form_id+' .pleasure').length){
		var opt,id,err=0;
		$('#'+form_id+' .pleasure:not(.nocheck)').each(function(){
			 opt = $(this).attr('rel');
			 id =  $(this).attr('id');
			 if( checkMaxValue(id, opt)!=true || check_number(id)!=true ){
				 err++;
			 }
		});
		if(err >= 1){
			return false;
		}else{
			return true;
		}
	}else{//ถ้าไม่มีค่าความพึงพอใจ ให้ผ่านเลย
		return true;
	}
}

function add_cr_booking(cus_no, id_card, department)
{
	if(!$('#book'+cus_no).attr('id')){
		//แสดงแถบสีแหลงว่าจองแล้ว
		var div = '<div id="book'+cus_no+'">';
		div	+= '<span onclick="follow_toggle(\'detail'+cus_no+'\',\'ic_followcus.php?func=showCusEvent&id_card='+id_card+'&cus_no='+cus_no+'\',\''+cus_no+'\')" style="border:1px solid orange;background-color:yellow;">';
		div	+= '<img src="img/process_by.png">คุณได้จองไว้แล้ว คลิกที่นี่ เพื่อทำรายการต่อ</span>';
		div	+= '<img align="absbottom" style="margin-top:2px;" onclick="cancel_cr_process(\''+cus_no+'\',\'1\',\''+id_card+'\',\''+department+'\');" title="ยกเลิกการจอง" src="img/cancel_process.png"></div>';
		$('#cusname'+cus_no).append(div);
		$('#TR'+cus_no).unbind('click');//ไม่ให้ TR คลิกได้เพราะมันจะไปซ้ำกับแถบสีเหลืองที่ให้คลิก
	}
}

/**
 * ยกเลิกการจองเพื่อทำรายการลูกค้าแต่ละคน
 * @param cus_no รหัสลูกค้า
 * @param status สถานะที่มีผล
 * @param id_card ส่ง Session คนทำรายการ
 * @param department ส่งรหัสฝ่ายของคนทำรายการ
 */
function cancel_cr_process( cus_no, status, id_card, department )
{
	var url = 'ic_followcus.php?func=cancelProcess1';
	var param = 'cus_no='+cus_no+'&status='+status+'&id_card='+id_card+'&department='+department;
	$.get(url, param, function(result){
		if(result=='OK'){
			$('#book'+cus_no).remove();
			//ย่อ ซ่อนไว้ก่อนรอเปิดอีกที
			$('#TR'+cus_no).attr('title','ขยาย');
			$("#detail"+cus_no).hide();
			$("#cus"+cus_no).fadeOut();
			old_toggle_id = cus_no;
			
			//สำหรับการคลิกครั้งต่อไป
			$('#TR'+cus_no).unbind('click')
				.click(function(){
					follow_toggle('detail'+cus_no,'ic_followcus.php?func=showCusEvent&id_card='+id_card+'&cus_no='+cus_no, cus_no);
			});
		}else{
			alert('เกิดข้อผิดพลาด กรุณาทำรายการอีกครั้ง');
		}
	});
}

function update_question_order(obj,q_list_id)
{
	$.post('ic_followcus_form.php?func=update_question_list_order', 'q_list_id='+q_list_id+'&number='+obj.value, function(){
		var lf = $(obj).offset().left - 80;
		var tp = $(obj).offset().top; 
		$('#show_progress').html('<font>Saving.....</font>').css({'left':lf,'top':tp}).show(); 
		setTimeout("$('#show_progress').hide()",2000);
	});
}