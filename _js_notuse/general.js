var new_win=null;
function openNewWindow(url,winName,config){
	var height='100',width='300';
	if(config['width']){width=config['width'];}
	if(config['height']){height=config['height'];}
	var top = ((gWH().height/2)-(height/2)) ,left=((gWH().width/2)-(width/2));
	var toolbar='no',menubar='no',scrollbars='yes',resizable='no',directories='no',fullscreen='no';	
	settings = 'height='+height+',width='+width+',top='+top+',left='+left+',toolbar='+toolbar+',menubar='+menubar+',scrollbars='+scrollbars+',resizable='+resizable+',directories='+directories+',fullscreen='+fullscreen;
	if(new_win && new_win.name == winName){new_win.close();}
	if(!winName){winName = "newWindow";}
	new_win = window.open(url,winName,settings);
	new_win.focus();
}

function IsNumber(value){
	var lenvalue = value.length;
   for (i = 0 ; i < lenvalue ; i++) { 
      if ((value.charAt(i) < '0') || (value.charAt(i) > '9')) return false;
   }
   return true;
}

function browseAppend(obj, url, param, callback){
	$.get(url,param,function(return_data){
		if(obj){
			$(obj).append(return_data);
		}
		if(callback){
			callback(obj,url,return_data)
		}
	});
}

function remove_obj(obj)
{
	$(obj).remove();
}

function load_more_carAcc()
{
	var next_limit_start = $('#car_acc_load').attr('rel');
	var url = 'carAcc.php?ViewAcccar=booking3&limit_start='+next_limit_start+'&car_type='+document.getElementById('02_c_type').value;
	url += '&car_pattern='+document.getElementById('02_pattern_model_class').value;
	url += '&car_code_name='+document.getElementById('02_model_code_name').value;
	url += '&car_color='+document.getElementById('02_car_color').value;
	url += '&order_by='+document.getElementById('02_order_by').value;
	
	browseAppend('#div_caracc_booking3', url);
}

function set_car_acc_load(next_limit_start){
	$('#car_acc_load').attr('rel',next_limit_start);
}