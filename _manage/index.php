<?php
session_start();
header("Content-type: text/html; charset=utf-8");
#คอนฟิกคำอธิบาย ชื่อไฟล์ 
# การอธิบายชื่อไฟล์สามารถทำผ่านคอมเมนต์ บนสุดของไฟล์นั้นๆได้เลยครับ
# รูปแบบ 
#
# /**
#  * ข้อความ1
#  * ข้อความ2
#  */
#
#หรือ
#
# /* คอมเมนต์ */
#  
# สามารถใช้ได้ทั้งสองแบบครับ
# แปะไว้ที่ส่วนบนสุดได้เลย ไม่ต้อง $config ชื่อไฟล์แล้วครับ

$_SESSION['SESSION_username']='admin';


$config['check_cusint_no_condition.php'] = 'ตรวจสอบ  cusint ที่ไม่มี condition ';

$config['check_acc_base_format.php'] = 'ตรวจสอบและจัด format ราคาของแต่ง  ด้วยใช้ number_format() ';

$config['check_color_and_model_matching.php'] = 'แสดงข้อมูล model code ที่ matching กับสี';
$config['check_cusint_product_real_data.php'] = 'ตรวจสอบ CusInt ที่ไม่มี CusInt_Product';
$config['resperson_report.php'] = 'ตรวจสอบจำนวนลูกค้าที่ดูแลหลังการขาย ของพนักงานแต่ละคน ต่อเดือน';

$config['replace_booking_unit_model.php'] = 'ตรวจสอบ unit_model ใน booking ที่มีค่า unit model ไม่ครบและไม่เหมือนใน stock => และทำการ replace ค่าที่ถูกต้องแทน ( booking ที่ยังไม่ได้ ยกเลิก และ  มีเลขเครื่อง )';
$config['replace_booking_unit_model2.php'] = 'ตรวจสอบ unit_model ใน booking ที่มีค่า unit model ไม่ครบและไม่เหมือนใน stock => และทำการ replace ค่าที่ถูกต้องแทน ( booking ที่ยังไม่ได้ ยกเลิก  และ ไม่มีเลขเครื่อง)';


require_once("check_function.php");
require_once("function_general.php");

if($_SESSION['SESSION_username']!='admin'){
	savelogfile();
	echo '<h1 style="color:red;margin:50px" align="center"><img src="user.png"><br>??????!!</h1>';	
	echo '<meta HTTP-EQUIV="REFRESH" content="2; url=../index.php">';	
}else {
	if ($handle = opendir('./')) {
	    /* This is the correct way to loop over the directory. */
	    $fileArr = array();
	    $notuse = array('function','index.php');
	    while (false !== ($file = readdir($handle))) {
	    		if($file!='.' && $file != '..' && $file!=='index.php' && substr($file,strrpos($file,'.'))=='.php' && strpos($file,'function')===FALSE){
	    			//$date = date('Y-m-d',filemtime($file));
	    			$fileArr[] = $file;
	       	}
	    }
	    closedir($handle);
	    
	    natcasesort($fileArr);
		 foreach($fileArr as $file){
		 	$n++;
	       		//echo '<div style="line-height:28px">',$n,'. <a href="',$file,'">',$file,'</a> (<font color="green"> ',$config[$file],'</font> )</div>';
	       		if($config[$file]==''){
	       			$config[$file] = get_file_comment($file);
	       		}
	       		$tr = array(
	       						'No'=>$n,
	       						'Link'=>'<a href="javascript:void(0)" onclick="javascript:if(confirm(\'ยืนยันการรันโค๊ดจากไฟล์ '.$file.'\')==true){window.location=\''.$file.'\';}">'.$file.'</a>',
	       						'Description'=>'<font color="green"> '.$config[$file].'</font>',
	       						'LastModify'=>date('Y-m-d',filemtime($file))
	       					);
	       		$tr_option = array(
	       							'custom'=>array('bgcolor'=>'white')
	       						);
	       		$tbody .= gen_tr($tr,$tr_option);
		 }
	    
	    $th = array(
 						'No'=>'ลำดับ',
 						'Link'=>'ชื่อไฟล์',
 						'Description'=>'คำอธิบาย',
 						'LastModyfy'=>'วันที่แก้ไข'
	       	);
 		$th_option = array(
 							'id'=>'tr_head',
 							'tag'=>'th',
 							'custom'=>array('bgcolor'=>'')
 				);
 		$thead = gen_tr($th,$th_option);
	    $table_option = array(
	    								'id'=>'tb_index',
	       							'custom'=>array('bgcolor'=>'#cccccc')
	       						);
		echo '<br>';						
		$easyaccount = $config['ppang'].$config['PART_HOME'];
	    echo "<div align='center'><span float='left' style='font-size:20px;font-weight:bold;'>(".$_SESSION['SESSION_Name'].'   '.$_SESSION['SESSION_Surname'].")</span><span style='padding-left:70px;'>   <a href='".$easyaccount."'>BACK TO EASYACCOUNT</a></span></div>";
		echo '<br><br>';
		echo gen_table($thead,$tbody,$table_option);
	}
}


/*
$arr = array(
'GXZ77NAFK(SV./AMAX-A;ABS)',
'GXZ77NFH(SV./AMAX-A)',
'GXZ77NAFK(SV./ABS)',
'GXZ77NFH(SB.)',
'GVR34JH(SV./AMAX-F)',
'FXZ77QDFH(SV.)',
'FXZ77QDTH(SV.)',
'FVZ34PSDFH(SV.)',
'FVZ34PSDTH(SV.)',
'FVZ34PNDH(SV.)',
'FVM34TSH(SV.)',
'FVM34QSH(SV.)',
'FVM34TNH(SV.)',
'FVM34RNH(SV.)',
'FVM34QNH(SV.)',
'FTR34QZL(SV./NEW TRANSMISSION)',
'FTR34PZL(SV./NEW TRANSMISSION)',
'FTR34LZL(SV./NEW TRANSMISSION)',
'FTR34JZL(SV./NEW TRANSMISSION)',
'FRR90NZL(SV./NEW TRANSMISSION)',
'FRR90HZL(SV./NEW TRANSMISSION)',
'GVR86KCL(SV./AMAX-F/CNG)',
'GVR86KCL(SV./CNG)',
'FTR86QCL(SV./CNG)',
'NPR82K5CK(SV./CNG)',
'NPR82H5CK(SV./CNG)',
'NMR82H5CK(SV./CNG)',
'NQR75L5H(SV.)',
'NQR75H5H(SV.)',
'NPR75K5NH(SV.)',
'NPR75H5NH(SV.)',
'NMR85H5TH(SV.)',
'NMR85H5FH(SV.)',
'NMR85E5H(SV.)',
'NLR85E1H(SV.)',
'GVR-CNG(6X2)',
'GXZ-CNG(6X4)A-MAX',
'GXZ-CNG(6X4)'
);
foreach($arr as $model){
	$mArr = explode("(",$model);
	$newM = $mArr[0];
	$new[] = $newM;	
}
$str = '$carModel = array(';
$str.= implode("', '",$new);
$str.= "');";
echo $str;
*/
?>