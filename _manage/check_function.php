<?php

session_start();

if( ! function_exists('savelogfile') ){
	function savelogfile() {
		if( $_SESSION['SESSION_username']==''){$user = '-';}else {$user =  $_SESSION['SESSION_username'];}
		$data = '"ไดเรกทอรี่ตรวจสอบข้อมูล(_admin)","'. date('Y-m-d H:i:s') .'","'.$_SERVER["REMOTE_ADDR"].'","'.$user.'"'." \n";
		$file = '../files/log_access_admin_'. date('m_Y') .'.csv';
		if( ! file_exists($file) ){
			$data = '"PAGE","วันที่เข้ามา","IP address","Username"'." \n ".$data;	
		}
		$fp = fopen($file, 'a');
		fwrite($fp, $data);
		fclose($fp);
		unset($arr);
		unset($data);
	}
}

if( ! function_exists('get_file_comment') ){
	function get_file_comment($filename){
		$fp = fopen($filename, 'r');
		$buffer = '';
		$check = '';
		while (($buffer = fgets($fp, 4096)) !== false) {
			$text .= $buffer.'[!@]';
	  		$n++;
	   	if(strpos(trim($buffer), '*/')!==FALSE || $n > 20){
	   		break;
	   	}
	   }
	   fclose($fp);
	   if(preg_match('/\/\*(.*)\*\//s', $text, $matches)) {
			$string = $matches[1];
		}
		$arr = explode('[!@]',$string);
		foreach($arr as $key=>$val){
			if(strpos(trim($val), '*')!==FALSE ){
				$new = substr($val,strpos($val,'*')+1);
				if(trim($new)!=''){
					$comment .= $new;
				}
			}else {
				$comment .= $val;
			}
		}
		return nl2br($comment);
	}
}

if( ! function_exists('createPageView'))
	{
	function createPageView($sql, $pagenumber, $gotopage){
			global $config;
			$limit = $config['page_limit'];
			
			mysql_query( "SET NAMES UTF8" ) ;
			$result = mysql_query($sql);
			$total=mysql_num_rows($result); //จำนวนแถวทั้งหมด
			
			$totalpage=ceil($total/$limit); //จำนวนหน้าที่ได้
			$mod = $total % $limit;
			if($pagenumber>$totalpage){$pagenumber=$totalpage;}//ถ้าหน้าที่ส่งมา มากว่าจำนวนหน้าทั้งหมด แสดงว่ารายการหน้าสุดท้ายถูกลบออกไปแล้ว
			if ($pagenumber==""){$pagenumber=1;}
			
			$arr['goto']=($pagenumber-1)*$limit; //เริ่มต้นที่ 0 คือข้อมูลแถวแรก
			$rowbegin = $arr['goto']+1; //แสดงแถวเริ่มต้น
			$rowend = $pagenumber*$limit; //แสดงแถวสุดท้าย
			if($pagenumber==$totalpage){
				if($mod){ //ถ้าหารไม่ลงตัว
					$rowend = $arr['goto']+$mod;
				}else{
					$rowend = $pagenumber*$limit;
				}
			}
			if($totalpage>1){
				$limitpage = $config['page_number'];
				if($pagenumber <= $limitpage and $pagenumber < $totalpage){//ถ้าหน้าปัจจุบันเป็นตัวเลขชุดแรก
					$startpage = 1;
				}else{
					if($pagenumber % $limitpage){
						$startpage = (floor($pagenumber/$limitpage)*$limitpage)+1;
					}else{//ถ้าหารลงตัว หมายถึงหน้าปัจจุบัน เป็นเลขตัวสุดท้าย
						$startpage = ($pagenumber - $limitpage) +1; //หน้าเริ่มต้นของ เลขแถวนั้นจะเท่ากับ หน้าปัจจุบัน - จำนวนจำกัดต่อแถว
					}
				}
				$endpage = ($startpage+$limitpage)-1;
				
				if($endpage > $totalpage){ //ถ้าหน้าสุดท้ายมากกว่า จำนวนหน้าทั้งหมด
					$num = ($totalpage-$startpage) ;
					$endpage = $startpage + $num;
				}
				if(strpos($gotopage, '?')!==FALSE){ $sign = '&';}else {$sign='?';}
				for ($i=$startpage;$i<=$endpage;$i++){
					if($i==$pagenumber){$class="select";}else{$class = "nonselect";}
					$nav_pager .= "<a href='$gotopage".$sign."pagenumber=$i' class='$class'>$i</a>";
				}
	
				if($endpage < $totalpage){
					$num = $endpage + 1;
					$next = "<a href='$gotopage".$sign."pagenumber=$num' class='nav_button'>หน้าถัดไป>></a>";
				}
				if($startpage > $limitpage){
					$num = $startpage - 1;
					$prev = "<a href='$gotopage".$sign."pagenumber=$num' class='nav_button'><<หน้าที่แล้ว</a>";
				}
	
			}
			if($total > 0){
			$arr['pageview'] = "<div id='pager_nav' class='pager' style='margin-top:10px'>[หน้าที่ <span class='current_pager'>$pagenumber</span> / <font class='total_pager'>$totalpage</font> หน้า] รายการที่ <span class='begin_end_rows'>$rowbegin - $rowend</span> จากทั้งหมด <font class='total_num_rows'>$total</font> รายการ $other <br>
			$prev $nav_pager  $next </div>";
			}
			return $arr;
	}
}
?>