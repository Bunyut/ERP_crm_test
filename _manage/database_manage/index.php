<?php
	header('Content-Type: text/html; charset=utf-8');
?>
<html>
<head>
<title>โปรแกรม Import - Export ฐานข้อมูล</title>
<script type="text/javascript" >
	function goto(url){
		window.location = url;
	}
</script>
<style>
	#menu div{
		margin: 5px;
		background-color: #FFEDDF;
		cursor: pointer;
	}
</style>
</head>
<body>
<div id="menu" align="center" style="padding:10px">
<div onclick="goto('bigdump.php');" align="center" style="border:2px solid #eee;width:600px">
<a href="javascript:void(0)">โปรแกรม Big Dump (sql command)</a><br>
</div>
<div onclick="goto('bigexport.php');" align="center" style="border:2px solid #eee;width:600px">
<a href="javascript:void(0)">โปรแกรม Big Export (data only)</a>
</div>
</div>
</body>
</html>