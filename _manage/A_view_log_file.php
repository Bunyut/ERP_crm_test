<?php
header('Content-type: text/html; charset=utf-8');
/**
แสดงรายการ logfile ทั้งหมดในโฟลเดอร์ files
จะอ่านเฉพาะไฟล์/directory ที่มีคำนำหน้าว่า log_
*/
require_once("../config/general.php");
// require_once '../function/helper_person_data.php';
require_once 'function_general.php';
Conn2DB();
//config file name
$config['log_customer_management'] = 'การจัดการลูกค้ามุ่งหวัง';
$config['log_access_admin']		= 'การเข้ามาใช้ในส่วนของแอดมินฯ';

$selfpage = $_SERVER['PHP_SELF'];

function get_file_title($filename){
	global $config;
	$arr = explode('_', $filename);
	$first = explode('/',$arr[0]);
	$arr[0] = $first[count($first)-1];
	$num = count($arr) - 2;
	for($i=0;$i<$num;$i++){
		$new .= $sign.$arr[$i];
		if($sign==''){$sign='_';}
	}
	return $config[$new];
}

function my_readDir($sourceDir)
{
	$dir=opendir($sourceDir);  //เปิดไดเรกทอรีเก็บไฟล์ต้นฉบับ
	while($file=readdir($dir)){  //วนรอบอ่านสคริปต์ทีละสคริปต์ จนกว่าจะครบ
		if(is_dir( $sourceDir.'/'.$file )){
			$ext = substr($file,strrpos($file,'.'));
			$path = $sourceDir.'/'.$file;
			$getDir = false;
	    	if($sourceDir=='../files' && strpos($file,'log_')!==FALSE){//ถ้าอยู่ใน files เอาเฉพาะที่มีคำว่า log_
		    	$getDir = true;
		    }else if($sourceDir!='../files'){//ถ้าเป็นโฟลเดอร์ที่กำหนดเอง เลือกทั้งหมด
		    	$getDir = true;
		    }
			if($getDir==true && $file != '.' && $file !='..'){
			//	echo '<br><a href="'.$_SERVER['PHP_SELF'].'?dir='.$path.'">'.$path.'</a>';
				$directory[] =  $path;
				if(opendir($path)){
					$directory[] = my_readDir($path);
				}
			}
		}
	} //end while
	closedir($dir);
	return $directory;
}

function show_all_array($arr)
{
	if(gettype($arr)=='array'){
		natcasesort($arr);
		foreach($arr as $path){
			if(gettype($path)=='array'){
				show_all_array($path);
			}else {
				if($path){
					echo '<br><a href="'.$_SERVER['PHP_SELF'].'?dir='.$path.'">'.$path.'</a>';
				}
			}
		}
	}
}


echo '<h4 style="border:1px solid orange;text-align:center;color:blue">รายการ logfile <font style="color:red"> ['.$_REQUEST['dir'].$_REQUEST['log'].']','</font></h4>';

if($_GET['log']){
	echo '<a href="',$_SERVER['PHP_SELF'],'?dir='.$_GET['dir'].'" style="color:green;border:1px solid brown;background:orange;text-decoration:none;padding:5px"><= Back</a><br><br>';
	$file = $_GET['log'];
	if(($fp = @fopen($file,"r"))!== FALSE){
		$num = 0;
		$checkbox = '';
		$select_sale = '';
		$arr_num = array();
		foreach($_GET as $k=>$gt){
			$get .= $sign.$k.'='.$gt;
			$sign = '&';
		}
		echo '<form method="POST" action="'.$selfpage.'?'.$get.'">';
		echo '<table bgcolor="black" cellspacing="1" cellpadding="5" style="font-size:13px">';
		while (($arr = fgetcsv($fp, 1024, ",")) !== FALSE) {
			$num++;
			if($num==1){
				$col = '<th>';$_col='</th>';$bg='saddlebrown';$color='white';
				$numRow = 'ลำดับ';
			}else{
				$col = '<td>';$_col='</td>';$bg='white';$color='black';
				$numRow+=1;
			}

			$tr = '';
			$i=0;
			$not_in = array('-',' ','');
			foreach($arr as $val){
				$i++;
				$val = stripcslashes($val);
				$new_val = $val;
				if( strlen($val)==13 && abs($val) )
				{
					$id_card = $val;
					$emp_name = helper_get_emp_name("ID_card='$id_card'");
					$val = $id_card.'<br> '.$emp_name;
					$select_sale[$id_card] = $emp_name;
				}
				if($num==1){
					$checked = '';
					if($_POST['chk_column'] && in_array($val,$_POST['chk_column']) ){
						$arr_num[] = $i;
						$checked = 'checked="checked"';
					}
					$checkbox .= '<input type="checkbox" value="'.$val.'" name="chk_column[]" id="chk_'.$val.'" '.$checked.'><label for="chk_'.$val.'" style="cursor:pointer">'.$val.'</label>&nbsp;';
					//$tr .= $col.$val.$_col;
					if(($val=='Sent_Request_Date' || strpos(strtolower($val),'time')!==FALSE) && !in_array($val,$not_in)){
						$column_date = $i-1;
					}
				}
				if( $_POST['chk_column'] && (!in_array($val,$_POST['chk_column']) && !in_array($i,$arr_num)) ){
					//กรองข้อมูลที่ไม่แสดง
				}else{
					if($i==($column_date+1) && $num!=1){//คอลัมน์แรก และไม่ใช่แถวแรก (คือแถวที่สองเป็นต้นไป)
						if($old_action_date!='' && !in_array($new_val,$not_in)){
							$d = diff2time($old_action_date,$new_val);
							$val .= '<br>'."<font color='saddlebrown'><b>$d[d]</b></font>[D],<font color='blue'><b>$d[h]</b></font>[H],<font color='green'><b>$d[m]</b></font>[M],<font color='red'><b>$d[s]</b></font>[S].";
						}
						if(ereg("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})",$new_val,$arr)){
							$old_action_date = $new_val;//วันที่เกิดรายการ
						}
					}
					$tr .= $col.$val.$_col;
				}

			}
			if($_POST['slt_sale']!='' && $id_card!=$_POST['slt_sale'] && $num!=1){
				continue;
			}
			echo '<tr bgcolor="'.$bg.'" style="color:',$color,'">';
			echo "$col $numRow $_col".$tr;
			echo '</tr>';

			//$data = '"'. implode('","',$arr) .'"';
			//$new .= html_entity_decode($data, ENT_NOQUOTES,'UTF-8')."\n";
		}//while loop
		if($select_sale){
			$select_sale_opt = '<option value="">เลือกชื่อพนักงานที่ต้องการดูข้อมูล</option>';
			natcasesort($select_sale);
			foreach($select_sale as $id_card_sel=>$name){
				$selected = '';
				if($_POST['slt_sale']==$id_card_sel){
					$selected = 'selected="selected"';
				}
				$select_sale_opt .= '<option value="'.$id_card_sel.'" '.$selected.'>'.$name.'</option>';
			}
			$select_sale = '<select name="slt_sale">'.$select_sale_opt.'</select>';
		}
		echo '<div style="font-family:verdana;font-size:12px">
				เลือกคอลัมน์ และชื่อพนักงาน :: ',$select_sale,'<br>',$checkbox,'
				<br><input type="submit" value="           แสดงข้อมูล           ">
			</div>';

		echo '</table>';
		echo '</form>';
		unset($arr);
		fclose($fp);
	}//end open
	else
	{
		echo '<font color="red">ไม่มีข้อมูล</font>';
	}
}else if($_REQUEST['dir']){
	//$dir = "../files";
	echo '<a href="',$_SERVER['PHP_SELF'],'" style="color:green;border:1px solid brown;background:orange;text-decoration:none;padding:5px"><= Back</a><br><br>';
	$dir = $_REQUEST['dir'];
	if ($handle = opendir($dir)) {
	    /* This is the correct way to loop over the directory. */
	    $fileArr = array();
	    //เอาเฉพาะที่ขึ้นต้นด้วย log_
	    while (false !== ($file = readdir($handle))) {
	    	 $check_name = false;
	    	if($dir=='../files' && (substr($file,0,4)=='log_' || (substr($file,0,1)=='d' && substr($file,-3)=='csv' ) )){//ถ้าอยู่ใน files เอาเฉพาะที่มีคำว่า log_
		    	$check_name = true;
		    }else if($dir!='../files'){//ถ้าเป็นโฟลเดอร์ที่กำหนดเอง เลือกทั้งหมด
		    	$check_name = true;
		    }
		    $ext = substr($file,strrpos($file,'.'));
			if($check_name && $ext=='.csv' ){
	       		$fileArr[] = $file;
			}
	    }
	    closedir($handle);

	    if(!$fileArr){
	    		echo '<div style="color:red">ไม่มีรายการ</div>';
	    	}else {
		    natcasesort($fileArr);
		    foreach($fileArr as $filedata){
		    		$n++;
		    		$fp = fopen($dir.'/'.$filedata,'r');
		    		$num=0;
		    		$index='';
		    		while (($arr = fgetcsv($fp, 1024, ",")) !== FALSE) {
		    			$num++;
			    		foreach($arr as $key=>$val){
			    			if(strpos(strtolower($val),'title')!==FALSE){ $index=$key; break; }
			    		}
			    		if($index!==FALSE){ $title = $arr[$index];}
			    		if($num==2){break;}
		    		}
		    		fclose($fp);
		    		if($title==''){//ถ้าไม่มีคอลัมน์ TITLE ให้หาจากการคอนฟิกค่าไว้ในตัวแปร
		    			$title = get_file_title($filedata);
		    		}
		   		echo '<div style="line-height:28px">',$n,'. <a href="',$_SERVER['PHP_SELF'],'?log=',$dir,'/',$filedata,'&title=',$title,'&dir=',$_REQUEST['dir'],'">',$filedata,' - ( <font style="color:saddlebrown">',$title,'</font> )</a></div>';
		   	}
		}//check data
	}
}else {
	echo	'<form action="'.$_SERVER['PHP_SELF'].'" method="post">
		เลือกโฟลเดอร์เก็บไฟล์
		<input type="text" id="dirname" name="dir" value="../files" />
		<input type="submit" value="GO" />
	</form><br>';
	$dir = '../files';
 	echo '<b><font color="green">เลือกรายชื่อโฟล์เดอร์ที่เก็บไฟล์ </font></b>';
  	echo '<br><a href="'.$_SERVER['PHP_SELF'].'?dir='.$dir.'">'.$dir.'</a>';
	$arr = my_readDir($dir);
	natcasesort($arr);
	show_all_array($arr);
}
CloseDB();
?>