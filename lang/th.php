<?php
/*
* Thai - compatible v2.3
$config['charset'] =      'iso-8859-11';
*/


$lang['Short_messags'] =      'ข้อความสั้นๆ';

//$config['charset'] =      'windows-874';
$config['charset'] =      'TIS-620';
$lang['operation_sure_deletemember'] ='คุณแน่ใจนะครับที่จะลบ ( ';
$lang['operation_sure_deletemember1'] =' ) นะครับ ';
$lang['Report_AllProduct']='รายงานการสั่งแผนกและฝ่ายแสดงแผนกและฝ่ายทั้งหมด';
$lang['Report_Product']='รายงานการสั่งแผนกและฝ่ายแยกรายบุคคล';
$lang['member']='สมาชิก';
$lang['Are_You_sure']='คุณแน่ใจ ?';
$lang['other']='อื่นๆ ';
$lang['manager']='ระบบจัดการ';
$lang['manager_Member']='ระบบจัดการสมาชิก';
$lang['manager_Products_Report']='ระบบรายงานแผนกและฝ่าย';
$lang['manager_Products']='ระบบการจัดการแผนกและฝ่าย';
$lang['January']='มกราคม';
$lang['February']='กุมภาพันธ์';
$lang['March']='มีนาคม';
$lang['April']='เมษายน';
$lang['May']='พฤษภาคม';
$lang['June']='มิถุนายน';
$lang['July']='กรกฎาคม';
$lang['August']='สิงหาคม';
$lang['September']='กันยายน';
$lang['October']='ตุลาคม';
$lang['November']='พฤศจิกายน';
$lang['December']='ธันวาคม';


$lang['eer2']='เนื่องจากไม่สามารถส่งอีเมล์  หรือยืนยันอีเมล์นี้ได้ หรือ ';
$lang['eer']='  เนื่องจาก <br>&nbsp;&nbsp;&nbsp;&nbsp;  อีเมล์ หรือ ข้อมูล ของท่านผิดพลาดหรืออาจมีผู้ใช้แล้ว  &nbsp;&nbsp;&nbsp;<br>กรุณาลองใหม่อีกครั้ง';
$lang['eer1']='มีผู้ลงทะเบียนก่อนหน้านี้แล้วกรุณาลองใหม่อีกครั้ง';

/*MEMBER*/
$lang['member_id_Login'] =     'เนื่องจากท่านดำเนินการไม่เรียบร้อยกรุณาล็อกอินเข้าระบบ<br>หรือ<br>ถ้ายังไม่ได้เป็นสมาชิกกรุณาสมัครสมาชิกก่อน';
$lang['member_EmailContact'] =     'ถ้าต้องการเปลี่ยนกรุณาติดต่อผู้ดูแล';
$lang['PolicyEdit_Text'] =     'การเพิ่มช่อง ท่านสามารถเพิ่มได้ คือ กล่องข้อความ  กล่องหล่นลง กล่องกาเครื่องหมาย ปุ่มตัวเลือก รวมกันแล้วต้องไม่เกิน 14 กล่องข้อความ<br> และ การแสดงข้อความเวลาผู้ใช้งานให้ใส่ เครื่องหมาย \'ฺtype Name\' ดูวิธีใช้งานเพิ่มเติมได้ที่ได้ที่ mn9.net';

/*
* Messages
*/
$lang['Passwords_not_match'] = "พาสเวิร์ดผิดพลาด กรุณากลับไปกรอกใหม่";
$lang['orderMore']='รายการสั่งซื้อ';


$lang['Pending'] =      'ยังไม่แน่นอน';
$lang['category_not_found'] =          'ไม่พบ รายการที่ต้องการ';
$lang['Pending'] =      'ยังไม่แน่นอน';

$lang['cf_tel'] =                      'กรุณาพิมพ์หมายเลขโทรศัพท์';
$lang['cf_url'] =                      'เว็บของคุณคุณ';
$lang['cf_post_code'] =                'กรุณาพิมพ์รหัสไปรษณีย์';
$lang['cf_nip'] =                      'หมายเลขผู้เสียภาษี';
$lang['cf_pesel'] =                    'กรุณาพิมพ์ ค่าเงิน';
$lang['cf_to_small_value'] =           'ค่าน้อยเกินไป';
$lang['cf_wrong_date'] =               'วันที่ผิด';
$lang['cf_min'] =                      'น้อย';
$lang['cf_fields_checked'] =           'กรุณาตรวจสอบ';

$lang['cf_no_word'] =                  'กรุณากรอกข้อความในทุกช่อง';
$lang['cf_mail'] =                     'กรอกอีเมล์';
$lang['cf_wrong_value'] =              'กรอกถูกต้อง';
$lang['cf_txt_to_short'] =             'ข้อความสั้นไป';

$lang['operation_saved'] =                          'บันทึกข้อมูลเรียบร้อย';
$lang['operation_changed'] =                        'ปรับปรุงข้อมูลเรียบร้อย';
$lang['operation_sure_delete'] =                    'คุณต้องการลบใช่ไหม?';
$lang['operation_yes'] =                            'ใช่';
$lang['operation_no'] =                             'ไม่';
$lang['operation_deleted'] =                        'ลบข้อมูลเรียบร้อยแล้ว';
$lang['operation_completed'] =                      'ระบบจัดการเรียบร้อย';
$lang['operation_not_completed'] =                  'ระบบจัดการไม่เรียบร้อย';
$lang['operation_especify_all_required_fields'] =   'กรุณากรอกทุกช่อง';
$lang['operation_not_found'] =                      'ไม่พบข้อมูลที่ท่านต้องการ';
$lang['operation_go_back'] =                        'ย้อนกลับ';

$lang['Logged_in'] =            'ล็อกอินเข้าสู่ระบบเรียบร้อบแล้ว';
$lang['Error_login_or_pass'] =  'ล็อกอิน หรือ รหัสผ่านไม่ถูกต้อง';
$lang['Logged_out'] =           'ล็อกเอ้าท์ออกจากะระบบเรียบร้อยแล้ว';

$lang['products_not_found'] = 'ไม่พบข้อมูลที่ท่านต้องการ';
$lang['Choose_courier'] =     'เลือกการจัดส่ง';
$lang['Not_found'] =          'ไม่พบรายการที่ท่านต้องการ';
$lang['basket_is_empty'] =    'ตะกร้าไม่มีแผนกและฝ่าย';

$lang['write_topic'] =      'กรอกหัวเรื่อง';
$lang['Error_email_send'] = 'การส่งอีเมล์ผิดพลาด';
$lang['Email_send'] =       'อีเมล์ได้ส่งเรียบร้อยแล้ว ขอบคุณ';
$lang['Answer_soon'] =      'เราจะตอบคุณโดยเร็ว';

$lang['Fill_login'] =                 'กรอก ล็อกอิน';
$lang['Fill_password'] =              'กรอก รหัสผ่าน';
$lang['Fill_product_list'] =          'กรอกจำนวนแผนกและฝ่ายที่แสดงใน 1 หน้า';
$lang['Fill_admin_list'] =            'กรอกจำนวนแผนกและฝ่ายที่สั่งซื้อใน 1 หน้า';
$lang['Fill_products_photo_size'] =   'กรอกขนาภาพแผนกและฝ่าย';
$lang['Fill_categories_photo_size'] = 'กรอกขนาดภาพสำหรับข่าวพิเศษ';

/*
* Name of fields in forms
*/

$lang['Content_mail'] = 'รายละเอียด';
$lang['send'] =         '  ส่ง  ';
$lang['Logo']     = 'โลโก้';
$lang['Template'] = 'เทมเพลต';
$lang['Type'] = 'พิมพ์';
$lang['Contact_page'] = 'หน้าติดต่อสอบถาม';

/*
* Actions
*/
$lang['All'] =            'ทั้งหมด';
$lang['choose'] =         'เลือก';
$lang['Delete'] =         'ลบ';
$lang['Next'] =           'ต่อไป';

$lang['back'] =           'กลับ';
$lang['log_in'] =         'ล็อกอิน';
$lang['logout'] =         'ล็อกเอ้าท์';
$lang['delete'] =         'ลบ';
$lang['save'] =           'บันทึก';
$lang['go_back'] =        'ย้อนกลับ';

$lang['Pages'] =          'หน้า';
$lang['wellcome'] ='ยินดีต้อนรับ ผู้มีอุปการะคุณทุกท่าน';
$lang['Log_in_administration'] ='ล็อกอิน เข้าสู่ระบบ';
$lang['version'] =              'เวอร์ชั่น';


$lang['Pending'] =      'ยังไม่แน่นอน';
$lang['Processing'] =   'ดำเนินการ';
$lang['Finished'] =     'เรียบร้อย';
$lang['Canceled'] =     'ยกเลิก';

$lang['Pendings'] =     'ยังไม่แน่นอน';


/*
* HtmlEditor messages
*/

/*
* Dont change below
*/
define( 'LANG_YES_SHORT', $lang['operation_yes'] );
define( 'LANG_NO_SHORT', $lang['operation_no'] );
?>