<?php
//คำอธิบายที่มาของทีม
$lang['remark'] = '<font color="magenta">* จะต้องเลือกกำหนดเป้าก่อน ข้อมูลทีมถึงจะแสดงในหน้านี้</font>';


//ส่วนของแผนผังทีม ในสายงานองค์กร (OU)
$lang['team_not_in_system']		= 'ข้อมูลทีม ที่ดึงมาจากสายงานขององค์กร (OU)';
$lang['no_team_in_ou'] 			= 'คุณยังไม่ได้กำหนดสายงานในผังองค์กรครับ ';
$lang['confirm_transfer_right'] = 'ยืนยันการนำเข้าข้อมูลทีม ของคุณ ';

//ส่วนของแผนผังทีม ในระบบ
$lang['team_in_system']			= 'ทีมในระบบ';
$lang['no_team_from_ou'] 		= 'คุณยังไม่ได้กำหนดเป้าให้กับทีมครับ';
$lang['confirm_transfer_left'] 	= 'ยืนยันการย้ายออกข้อมูลทีม ของคุณ ';

//ข้อความแจ้งเตือนในหน้าแบ่งเป้าด
$lang['target_team_reference_warning'] = '*** รายชื่อตำแหน่งต่างๆที่จะปรากฏในหน้านี้ได้ จะต้องกำหนดสิทธิ์ให้สามารถเข้าใช้งานเมนู '.$program['menu_check_team'].' ก่อนครับ';

//แบ่งเป้า

$lang['target_level_select'] 	= 'เลือกระดับหัวหน้า';//ตัวเลือกระดับเพื่อแบ่งเป้า
$lang['target_month_select']	= 'เลือกเดือน';
$lang['target_type_description']= 'ประเภทของเป้า';

$lang['target_level_option_leader'] = 'แบ่งเป้าให้ระดับ ';
$lang['target_level_top_leader'] = 'หัวหน้าระดับบนสุด';

$lang['target_type2'] = 'เป้าคน';
$lang['target_type1'] = 'เป้าทีม';
$lang['button_submit_title']	= 'บันทึกแก้ไขทั้งหมด';
$lang['button_submit_confirm'] 	= 'ยืนยันการบันทึกการกำหนดเป้าทั้งหมด';
$lang['button_submit_confirm_one'] 	= 'ยืนยันการบันทึกการกำหนดเป้าของตำแหน่ง';
$lang['button_submit_confirm_one_person'] 	= 'ยืนยันการบันทึกการกำหนดเป้าของคุณ';

$lang['target_setup_noteam'] = 'คุณยังไม่มีลูกทีมสำหรับแบ่งเป้า กรุณาติดต่อผู้รับผิดชอบจัดสายงานในฝังองค์กรครับ';


//หมายเหตุ
$lang['target_remark1'] = '* ตัวหนังสือ <font color="red">สีแดง</font> คือรายการที่ยังไม่มีอยู่ในตารางกำหนดเป้า';
$lang['target_remark2'] = '** ตัวหนังสือ <font color="blue">สีน้ำเงิน</font> คือรายการที่มีการเปลี่ยนแปลงหัวหน้าทีม';


//จัดทีม
$lang['choose_team_title'] = 'คลิกเพื่อเลือกชื่อทีม';
$lang['choose_no_team_description'] = '<font color="red">ไม่มีชื่อทีม</font>';
$lang['team_deplicate_warning']		= 'ทีมที่เลือกซ้ำกับชื่อทีมของ';


//ข้อมูลทีม
$lang['team_emp_name_null'] = '<< ตำแหน่งว่าง >>';


//ไม่พบข้อมูล
$lang['found_not_data'] = 'ไม่พบข้อมูล';

//END OF FILE