/* General page */
var ajax_loader = "<div id='ajaxLoader' align='center'><img src='img/loader_100x100.gif'></div>";
function search_array(arr_object, variable){for(var i=0;i< arr_object.length;i++){ if (arr_object[i] == variable) return true ; } return false ;}
function setDisplayNone(id){ if(document.getElementById(id))document.getElementById(id).style.display = 'none';}
function setDisplayInline(id){if(document.getElementById(id))document.getElementById(id).style.display = 'inline';}
function setDisplay(id){if(document.getElementById(id))document.getElementById(id).style.display = '';}
function checknumber(e){
	keyPressed = e.which;
	if(((keyPressed < 48) || (keyPressed > 57)) && (keyPressed != 0) && (keyPressed != 8) && (keyPressed != 46)) keyPressed = e.preventDefault();
}
function remove_class(id,class_name){$('#'+id).removeClass(class_name);}
function remove_attr(id,attr_name){$('#'+id).removeAttr(attr_name);}
function showHide(id,chk,name){
 	/*if(chk=='Hide'){
 		var show = 'Show';
 		setDisplayNone(id);
 		document.getElementById(name).innerHTML = '<a onclick=\"showHide('+id+','+show+','+name+')\">Show</a>';
 	}else if(chk=='Show'){
 		var hide = 'Hide';
 		setDisplayInline(id);
 		document.getElementById(name).innerHTML = '<a onclick=\"showHide('+id+ ','+hide+','+name+')\">Hide</a>';
 	}*/
 }
function setReadOnly(id,downVal){document.getElementById(id).readOnly = val;}
function activeUiTab(div,num){$(div).tabs('option', 'selected', num);}
function formatDecimals(id){
	var number = ""; number = $(id).val(); number = textTOnum(number); number = parseFloat(number);
	if(number != ''|| number != '0'|| number != 0){ number = formatNumber(number,2,1); $(id).val(number); }
}
function SwitchRead(id,val){
	var val = document.getElementById(id).readOnly;if(val == true){document.getElementById(id).readOnly = false;}
	var Sub_Down = document.getElementById('Sub_Down').value;Sub_Down = parseFloat(Sub_Down);calRealDownPay('Sub_Down');calIntPerMont();
}
function calBalanceheld(){  // คำนวนยอดจัด
	var down = document.getElementById('Down').value; // เงินดาวน์
	if(down == ''){ down = 0; } down = textTOnum(down);
	var NetPrice = document.getElementById('NetPrice').value; // ราคาหลังหักส่วนลด
	NetPrice = textTOnum(NetPrice);
	var Balanceheld = parseFloat(NetPrice) - parseFloat(down);  // ยอดจัด
	Balanceheld = parseFloat(Balanceheld);return Balanceheld;
}
function calContractValue(){ // คำนวนมูลค่่าสัญญา
 var AllInterest = calAllInterest();  // ดอกเบี้ยทั้งหมด
 var Balanceheld = calBalanceheld();  // ยอดจัด
 var ContractValue = parseFloat(Balanceheld)+ parseFloat(AllInterest);ContractValue = parseFloat(ContractValue);
 return ContractValue;
}
 function calAllInterest(){  // ตำนวนดอกเบี้ยทั้งหมด
	var IntPerYear = CalIntPerYear();  // ดอกเบี้ยต่อปี
	var Time = document.getElementById('Time').value;
	if(Time == ''){ Time = '0.00'; }
	var NumOfYears = parseFloat(Time)/12;
	var AllInterest = parseFloat(IntPerYear) * parseFloat(NumOfYears);
	AllInterest = parseFloat(AllInterest);
	return AllInterest;
 }
 function CalIntPerYear(){		// คำนวนดอกเบี้ยต่อปี
	var Balanceheld = calBalanceheld(); // ยอดจัด
	var intVal = document.getElementById('Int').value; // ดอกเบี้ย
	if(intVal == ''){ intVal = 0; }
	intVal = (parseFloat(intVal)/100);

	var IntPerYear = (parseFloat(Balanceheld)*parseFloat(intVal));
	IntPerYear =  parseFloat(IntPerYear);
	return IntPerYear;
 }
 function calIntPerMont(){  // ค่างวด
	var val = document.getElementById('Time').value;
	if(val == ""){
		val = 1;
	}else if(val == 0){
		val = 1;
	}
	var ContractValue = calContractValue();
	var IntPerMont = 0;
	IntPerMont = parseFloat(ContractValue)/parseFloat(val);
	IntPerMont = parseFloat(IntPerMont);
	//IntPerMont = eval(parseInt(IntPerMont * 100) * .01); //   ปัดเสษ javascript ขึ้นเป็น  2 ตำแหน่ง
	//IntPerMont = IntPerMont.toFixed(2);
	IntPerMont = Math.ceil(IntPerMont);  // ปัดจำนวนเต็ม
	if(IntPerMont == 0){ IntPerMont = '0.00'; }
	IntPerMont = formatNumber(IntPerMont,2,1);
	document.getElementById('Installment').value = IntPerMont;


 }
function textTOnum(ams){ /*  ตัดข้อความเอา ลูกน้ำออก */
	if(!ams){ var ams = 0+',';}
	var strd='';var str= ams.split(',');
	if(str.length > 1){
	  for (var i=0;i<str.length;i++) { strd=strd+str[i]; }
		if(strd ==0) str = ams; else str = strd;
	}else str = ams;
	if(str=='.NaN') str='0.00';
	//return str;
	return parseFloat(str);
}
function formatNumber(num, decplaces,showtt) {
	num = parseFloat(num);
	if (!isNaN(num)){
		var str = "" + Math.round (eval(num) * Math.pow(10,decplaces));
		if (str.indexOf("e") != -1) {return "Out of Range";}
		while (str.length <= decplaces){ str = "0" + str;}
		var decpoint = str.length - decplaces;var tmpNum = str.substring(0,decpoint);
		//---------------Add Commas--------------------------
		var numRet = tmpNum.toString();if(showtt==1){var re = /(-?\d+)(\d{3})/;while (re.test(numRet)){numRet = numRet.replace(re, "$1,$2");}}
		//2011-06-6 NZ
		//return numRet + "." + str.substring(decpoint,str.length);
		if(!decplaces){//ถ้าไม่ส่งจำนวนทศนิยมมาด้วย
			return numRet;
		}else{
			return numRet + "." + str.substring(decpoint,str.length);
		}
		// -- modify
		/* var number = numRet + "." + str.substring(decpoint,str.length);
		if(number == '0.00'){number = "";} return number; */
	}else{ return ""; }//return "0.00";
}


function style_display_none(id){ document.getElementById(id).style.display = 'none';}
function jDateBSlash(datepicker){
	$(function(){  alert('55');  $("#Re_date").datepicker();
		//$(datepicker).datepicker({dateFormat : 'dd-mm-yy'});
	});
}
function Comma(Num){
   Num += '';
   Num = Num.replace(',' , '');Num = Num.replace(',' , '');Num = Num.replace(',' , '');
   Num = Num.replace(',' , '');Num = Num.replace(',' , '');Num = Num.replace(',' , '');
   x = Num.split('.');
   x1 = x[0];
   x2 = x.length > 1 ? '.' + x[1] : '';
   var rgx = /(\d+)(\d{3})/;
   while (rgx.test(x1))
   x1 = x1.replace(rgx, '$1' + ',' + '$2');
   return x1 + x2;
}
function ClearinnerHTML(name){$('#'+name).html('');}
function jqueryUI(Tabsname){$(function(){ $(Tabsname).tabs();});}
function TabsMouseover(Tabsname){$(function() {$(Tabsname).tabs({ event: 'mouseover'});});}
function jDate(datepicker){ $(datepicker).datepicker({dateFormat : 'dd-mm-yy',changeMonth: true,changeYear: true}); }
function jAccordion(divname){$(function() { $(divname).accordion({autoHeight:false,header: "h3"});});}
function jZoom(){document.getElementById(function() {document.getElementById('#gallery a').lightBox();}); }
function datePick(jShow){$(function(){$(jShow).datepicker({dateFormat : 'dd-mm-yy',changeMonth: true,changeYear: true});});}
function radio_set(div){ $(div).buttonset(); }
function dateMonthYear(jShow,StrRank,StpRank){
	$(function() {
		$(jShow).datepicker({
			dateFormat: "dd-mm-yy",
			changeMonth: true,
			changeYear: true,
			yearRange: StrRank+':'+StpRank,
			beforeShow:function(input, inst){
				var dt = $.datepicker.parseDate('dd-mm-yy', $(input).val());
					if(dt){dt.setYear(dt.getFullYear() - 543); //$(this).val();//$(this).val($.datepicker.formatDate('dd-mm-yy', dt));
				}
				//$(input).val($.datepicker.formatDate('dd-mm-yy', dt));

			//naizan 2010-12-13
				setTimeout(function(){
                $.each($(".ui-datepicker-year option"),function(j,k){
                    var textYear=parseInt($(".ui-datepicker-year option").eq(j).val())+543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            },50);
         },
			onChangeMonthYear: function(){
            setTimeout(function(){
                $.each($(".ui-datepicker-year option"),function(j,k){
                    var textYear=parseInt($(".ui-datepicker-year option").eq(j).val())+543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            },50);
          //end naizan

			}/* ,
			onClose: function(dateText, inst) {
				var dt = $.datepicker.parseDate('dd-mm-yy', dateText);dt.setYear(dt.getFullYear() + 543);
				$(this).val($.datepicker.formatDate('dd-mm-yy', dt));
			} */

		});
	});
}
function dialogUI(){
$(function() {
	$("#dialog").dialog("destroy");
	$("#dialog").dialog({
		bgiframe: true,
		resizable: false,
		height:140,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {
			'ตกลง': function() {
				$(this).dialog('close');
				updateCon();
			}
		}
	});
});
}












 /* --- Question use add to customer interest --- */
 	function showQList(val,coutf,refer){

		var Qgroup = document.getElementsByName('Qgroup[]');

			for(var i=0;i < Qgroup.length;i++){

			 	if(Qgroup[i].checked == true){

			 		document.getElementById('Question_list'+coutf).style.display = 'inline';
			 		browseCheck('Question_list'+coutf,'interestHistory.php?Qgraoup='+val+'&refer='+refer,'');

			 		return false;

			 	}else{
			 		document.getElementById('Question_list'+coutf).style.display = 'none';
			 	}

			}

 	}


 /* Customer Interest Histoiry Content */

 	function newType(){
 		//browseCheck('selecType','interestHistory.php?newType','');
 	}
 	function changeCusType(){
		browseCheck('changeArea','interestHistory.php?changeCusType','');
	}
	function otherData(){
			if(document.addForm.Outsite.checked == true){
				document.getElementById('CHECKBOXShow').style.display = 'inline';
			}else{
				document.getElementById('CHECKBOXShow').style.display = 'none';
			}
	}
	function exitCus(){
		var val = document.getElementById('id_cutomer').value;
		if(val == '' || val == null){
			alert('กรุณากรอกข้อมูลเพื่อค้นหา');
			return false;
		}else{
			browseCheck('box_tab','interestHistory.php?DupCus='+document.getElementById('id_cutomer').value,'');
		}
	}
	function optionModel(){
	  var option_model = document.getElementsByName('option_model[]');
   	if(option_model['8'].checked == true){
        document.getElementById('IntModelTypeDiv').style.display = 'inline';
    }else if(option_model['8'].checked == false){
        document.getElementById('IntModelTypeDiv').style.display = 'none';
    }
	}
	function showcusRecommend(name,check){
    var check = document.getElementById(check);
    if(check.checked == true){
        document.getElementById(name).style.display = 'inline';
    }else{
        document.getElementById(name).style.display = 'none';
    }
	}
	function insertNewInt(){
	  var Intbranch_name = document.getElementById('Intbranch_name').value;
	  if(Intbranch_name == ""){
	    alert('กรุณาเลือกสาขา');
	    return false;
	  }
	  if(confirm('ต้องการบันทึกข้อมูล ตรวจสอบถูกต้องแล้ว !!') == true){

	  }

	}
	function changeCaseTo(){
	  if(confirm('คุณต้องการส่งต่อเคศลูกค้า !!')==true){
	    var name = document.getElementById('input_name').value;
	    var CusNoRefer = document.getElementById('CusNoRefer').value;
	    if(name == ''){
	      alert('กรุณากรอกชื่อผู้รับเคสต่อ !! ');
	      return false;
	    }
	    document.getElementById('sentCaseArea').style.display = 'none';
	    browseCheck('ReSaleRespon','cusInterest.php?changeToName='+name+'&submittedName='+document.getElementById('SaleResponName').value+'&SaleResponNo='+document.getElementById('SaleResponNo').value+'&CusNoRefer='+CusNoRefer+'&Int_Num_Ref='+document.getElementById('Int_Num_Ref').value,'');
	    document.getElementById('input_name').value = "";
	    browseCheck('Re_CusInt_his','cusInterest.php?Re_CusInt_his='+CusNoRefer+'&case_name=sentCase','');
	    browseCheck('sent_case_record','cusInterest.php?ViewCaseNum='+document.getElementById('Int_Num_Ref').value+'&CusNo='+document.getElementById('CusNoRefer').value,'');
	   }
	}
	function IntCarPatternModel(id,number){
	//'IntCarModelCodeName_hidden'
	  document.getElementById(id).style.display = 'inline';
  browseCheck('CarModelCodeName'+number,'ic_newcusint.php?CarPatterName='+document.getElementById('IntCarPattern'+number).value+'&CarTypeName='+document.getElementById('IntCarType'+number).value+'&pattern_num='+number,'');


	}
	function IntModelGname(id,number){
	  document.getElementById(id).style.display = 'inline';
	  browseCheck('CarModelGName'+number,'ic_newcusint.php?CarModelName='+document.getElementById('IntModelCodeName'+number).value+'&CarModelPattern='+document.getElementById('IntCarPattern'+number).value+'&CarModelType='+document.getElementById('IntCarType'+number).value,'');

	}
	function groupByUnitType(val,check){
		alert('function groupByUnitType');
		for(var i=0;i<$('select[name="select_acc_use[]"]').size();i++){
			if(i == 0){ i = '';}
			var AssCarModelName = $('#IntModelCodeName').val();
			var AssCarModelPattern = $('#IntCarPattern').val();

			if(AssCarModelName == undefined){
				AssCarModelName = "";
			}else if(AssCarModelPattern == undefined){
				AssCarModelPattern = "";
			}

			var dataSet={ AssCarModelName: AssCarModelName, AssCarModelPattern:AssCarModelPattern,AssCarModelType:val };
			$.get('cusInterest.php',dataSet, function(data) {
				i = i-1;
				if(i == 0){ i = '';}
				$('#reAssType'+i).html(data);
			});

			var dataSet2={ AssNameCarModelName: AssCarModelName, AssNameCarModelPattern:AssCarModelPattern,AssNameCarModelType:val };
			$.get('cusInterest.php',dataSet2, function(data) {
				$('#reAssName'+i).html(data);
			});
		}

		var dataSet3={ AssNameCarModelTabs: AssCarModelName, AssNameCarPatternTabs:AssCarModelPattern,AssNameCarTypeTabs:val };
		$.get('cusInterest.php',dataSet3, function(data) {

			if(check != 'notClecrInner'){
				$("#All_AccUse_ForGift").html('');
				$("#All_AccUse_ForBuy").html('');
				$("#All_AccUse_ForSetPrice").html('');
			}else if(check == 'notClecrInner'){
				alert('รุ่นรถถูกเปลี่ยน เพื่อความถูกต้อง กรุณาตรวจสอบรายละเอียดอุปกรณ์ตกแต่งด้วยครับ !! ');
			}
			$("#All_AccUse_contain").hide('fast');
			$('#Reacc_use_ul').html(data);
			jqueryUI("#sunmenu_ass");
		});

	}

	function IntModelDetail(){
/*browseCheck('calCRPrice','cusInterest.php?DetailModelGName='+document.getElementById('IntModelGName').value+'&DetailModelPattern='+document.getElementById('IntCarPattern').value+'&DetailModelType='+document.getElementById('IntCarType').value+'&DetailModelCodeName='+document.getElementById('IntModelCodeName').value,''); */
	}
	function selectCRPrice(val){
		browseCheck('calCRPrice','cusInterest.php?ModelCMNumber='+val,'');
		browseCheck('CRPAndAirIncVatDIv','cusInterest.php?ModelCMNumberVal='+val,'');
	}
	function IntModelCM(id,number){
	  alert('function IntModelCM');
	 // return false;
	  //document.getElementById(id).style.display = 'inline';
browseCheck('CarModelCM'+number,'ic_newcusint.php?DetailModelGName='+document.getElementById('IntModelGName').value+'&DetailModelPattern='+document.getElementById('IntCarPattern').value+'&DetailModelType='+document.getElementById('IntCarType').value+'&DetailModelCodeName='+document.getElementById('IntModelCodeName').value,'');
	}
function CRPAndAirIncVat(id,number){
	document.getElementById(id).style.display = 'inline';
	browseCheck('CRPAndAirIncVatDIv'+number,'ic_newcusint.php?DetailModelGNameCRP='+document.getElementById('IntModelGName').value+'&DetailModelPatternCRP='+
	document.getElementById('IntCarPattern').value+'&DetailModelTypeCRP='+document.getElementById('IntCarType').value+'&DetailModelCodeNameCRP='+
	document.getElementById('IntModelCodeName').value,'');
}

function CusintSelectColor(id,number){/* piag 18-04-2011 ดึงชื่อสี */
	var url = 'ic_newcusint.php?pDetailModelGNameCRP='+document.getElementById('IntModelGName').value
			+'&DetailModelPatternCRP='+document.getElementById('IntCarPattern').value+'&DetailModelTypeCRP='
			+document.getElementById('IntCarType').value+'&DetailModelCodeNameCRP='
			+document.getElementById('IntModelCodeName').value;
	browseCheck('pColorDetail'+ number, url,'');
}

function CRPPriceValue(val,input){

	//alert('function CRPPriceValue');
	//return false;
	var value = textTOnum(val);
	value = parseFloat(value);
	value = formatNumber(value,2,1);
	$(input).val(value);
	
	CalNetPriceDiscount($("#Discount").val());
	
	calTotal_Budget();
	var IntCarType = $("#IntCarType").val(); 
	var IntModelCodeName = $("#IntCarPattern").val();
	var IntModelCodeName = $("#IntModelCodeName").val(); 
	var IntModelGName = $("#IntModelGName").val();
	var CRPAndAirIncVat = $("#CRPAndAirIncVat").val();
	
	alert(IntCarType+','+IntCarPattern+' , '+IntModelCodeName+' , '+IntModelGName+' '+CRPAndAirIncVat);
	
	browseCheck('EmtryAirConCRP','ic_newcusint.php?AirIntCarType='+IntCarType+'&AirIntCarPattern='+IntCarPattern+'&AirIntModelCodeName='+IntModelCodeName+'&AirIntModelGName='+IntModelGName+'&AirCRPAndAirIncVat='+CRPAndAirIncVat,'');

}
function MoreCompetitor(){
   var com = document.getElementById('chkMoreCompetitor').value;
   var number = parseFloat(com)+1;
   var area = 'CusInt_CusCompare_contain';
   var name = 'more_competitor'+number;

   node = document.getElementById(area);
   if (node) {
		   var myElement = document.createElement("DIV");
		   myElement.setAttribute("id",name);
		   node.appendChild(myElement);
		   myElement.innerHTML = 'document.getElementByIdCusInt_CusCompar';
		   document.getElementById(name).style.backgroundColor = '#F4F8D3';
		   document.getElementById('chkMoreCompetitor').value = number;

   }

	browseCheck(name,'cusInterest.php?MoreCompetitor='+number,'');
	subMoreComp();
}
function subMoreComp(){
   var com = document.getElementById('cpareChk').value;
   var number = parseFloat(com)+1;
   var area = 'sublist_compare_contain';
   var name = 'sublist_Compare'+number;

   node = document.getElementById(area);
   if (node) {
		   var myElement = document.createElement("li");
		   myElement.setAttribute("id",name);
		   node.appendChild(myElement);
		   myElement.innerHTML = '$CusCompare_list';
		   document.getElementById('cpareChk').value = number;
   }
  browseCheck(name,'cusInterest.php?moreSubCompare='+number,'');
}

 function removeElement(Area,divNum) {
    var d = document.getElementById(Area);
    var olddiv = document.getElementById(divNum);
    d.removeChild(olddiv);
 }
 function activeTabs(div,num){
	$(div).tabs('option', 'selected', num);
}
function checkForType(asstype,assname,AssCode){
	var name = asstype+assname;
	var chkval = $('input:radio[name=Item_ForAll]:checked').val();
	//alert(AssCode);
	if(chkval == "ลูกค้าจ่าย"){
		$('#DividePrice'+AssCode).show('fast');
	}else{
		$('#DividePrice'+AssCode).hide('fast');
	}
}

function checklist(asstype,assname,SaleAcc,AssCode,Dunit,Dname){
	var name = asstype+assname;
	var dnnName = Dname;
	var chkval = $('input:radio[name=Item_ForAll]:checked').val();  // get value buy or gift
	var amount = $('#AmountListName'+AssCode+''+Dname).val();  // จำนวน
	if(amount == ''){
		alert('กรุณาระบุจำนวนชิ้น !!');
	}else{
		//Zan@2011-11-26 เช็กตัวเลข
		if(IsNumber(amount)!=true){alert('กรุณาระบุจำนวนชิ้น เป็นตัวเลขครับ!!');return false;}//IsNumber() -> js_css_autoload/js/general.js
		$("#All_AccUse_contain").show('fast',function () {
			var div_id = ''; var setPrice = ''; var DividePrice= 0;
			if(chkval == "แถม"){
				div_id = '#All_AccUse_ForGift';
			}else if(chkval == "ซื้อ"){
				div_id = '#All_AccUse_ForBuy';
			}else if(chkval == "ลูกค้าจ่าย"){
				div_id = '#All_AccUse_ForSetPrice';
				setPrice = '<span class=\"paddingLeft\">ลูกค้าจ่าย        '+$('#DividePriceVal'+AssCode+''+Dname).val()+'     บาท        ราคาขาย        '+SaleAcc+'       บาท </span>';
				DividePrice = parseFloat(textTOnum($('#DividePriceVal'+AssCode+''+Dname).val()));
			}
			SaleAcc = textTOnum(SaleAcc);
			var value = formatNumber(((parseFloat(amount)*parseFloat(SaleAcc))-parseFloat(DividePrice)),2,1);
			var del_id = 'addId@#'+AssCode+''+Dname+'@#'+chkval;
			
			if(!document.getElementById(del_id)){
				$(div_id).append('<div id=\"addId@#'+AssCode+''+Dname+'@#'+chkval+'\" class=\"paddingLeft fontBold\" style=\"font-size:12px;margin-top:10px;margin-bottom:10px;\"><span class=\"paddingLeft\"> - '+asstype+'  ( '+assname+' )</span><span class=\"paddingLeft\">จำนวน         '+amount+'  '+Dunit+'</span><span style="color:#ff0000;" class=\"paddingLeft\">ราคารวม               '+value+'            บาท</span>'+setPrice+'<span class=\"paddingLeft\"><img src=\"temp/i_del.gif\" onclick=\"delInner(\''+del_id+'\')\" name=\"del\" class=\"cursor\"></span><span id="ItemFor@#'+AssCode+'\"><input type=\"hidden\" name=\"ItemForPer[]\" id=\"ItemForPer@#'+AssCode+''+Dname+'\" value=\"'+asstype+'@#'+AssCode+'@#'+chkval+'@#'+SaleAcc+'@#'+amount+'@#'+value+'\"/></span></div>');
			}else{
				//document.getElementById(del_id).innerHTML = '<div id=\"addId@#'+AssCode+''+Dname+'@#'+chkval+'\" class=\"paddingLeft fontBold\" style=\"font-size:12px;margin-top:10px;margin-bottom:10px;\"><span class=\"paddingLeft\"> - '+asstype+'  ( '+assname+' )</span><span class=\"paddingLeft\">จำนวน         '+amount+'  '+Dunit+'</span><span style="color:#ff0000;" class=\"paddingLeft\">ราคารวม               '+value+'            บาท</span>'+setPrice+'<span class=\"paddingLeft\"><img src=\"temp/i_del.gif\" onclick=\"delInner(\''+del_id+'\')\" name=\"del\" class=\"cursor\"></span><span id="ItemFor@#'+AssCode+'@#'+AssCode+'\"><input type=\"hidden\" name=\"ItemForPer[]\" id=\"ItemForPer@#'+AssCode+'@#'+AssCode+'\" value=\"'+asstype+'@#'+AssCode+'@#'+chkval+'@#'+SaleAcc+'@#'+amount+'@#'+value+'\"/></span>';
				
				//Zan@2011-11-26 ลบค่าเดิมออกก่อน
				var parent_div = div_id.replace("#","");
				parent_div = document.getElementById(parent_div);
				var oldChild = document.getElementById(del_id);
				parent_div.removeChild(oldChild);//ลบ
				$(div_id).append('<div id=\"addId@#'+AssCode+''+Dname+'@#'+chkval+'\" class=\"paddingLeft fontBold\" style=\"font-size:12px;margin-top:10px;margin-bottom:10px;\"><span class=\"paddingLeft\"> - '+asstype+'  ( '+assname+' )</span><span class=\"paddingLeft\">จำนวน         '+amount+'  '+Dunit+'</span><span style="color:#ff0000;" class=\"paddingLeft\">ราคารวม               '+value+'            บาท</span>'+setPrice+'<span class=\"paddingLeft\"><img src=\"temp/i_del.gif\" onclick=\"delInner(\''+del_id+'\')\" name=\"del\" class=\"cursor\"></span><span id="ItemFor@#'+AssCode+'@#'+AssCode+'\"><input type=\"hidden\" name=\"ItemForPer[]\" id=\"ItemForPer@#'+AssCode+'@#'+AssCode+'\" value=\"'+asstype+'@#'+AssCode+'@#'+chkval+'@#'+SaleAcc+'@#'+amount+'@#'+value+'\"/></span></div>');
			}
						
			$('#DividePriceVal'+AssCode+''+Dname).val(''); 
			$('#AmountListName'+AssCode+''+Dname).val(''); 
			calAllAccPrice('');
			//cal_accessory_cost();
		});
	}
}
function delInner(id){ document.getElementById(id).innerHTML = '';calAllAccPrice('');}
function calAllAccPrice(chk){
	var loop = $("input[name='ItemForPer[]']").size();
	var name = $("input[name='ItemForPer[]']");
	var val = 0; var buy = 0; var buy_value = 0; var OthCost = 0;
	for(var i = 0;i < loop; i++){
		var value = name[i].value.split('@#');
		var price = textTOnum(value[5]);				// ราคารวม
		buy = textTOnum(buy);							// ราคาที่ซื้อเอง
		if(value[2] == "แถม"){
			price = price;
		}else if(value[2] == "ซื้อ"){
			buy_value = price;
			price = 0;
		}else if(value[2] == "ลูกค้าจ่าย"){
			price = price;

			buy_value = (parseFloat(value[3])-parseFloat(price));
			price = price;
//alert(buy_value);
		}
		buy = (parseFloat(buy_value)+parseFloat(buy));
		val = ( parseFloat(price)+parseFloat(val));
		//  ex. ลายเนอร์@#1066@#แถม@#4500@#2@#2,500.00
		if(buy != 0 || buy != ''){ buy = formatNumber(buy,2,1);}
		else if(buy == 0){ buy = ''; }
	}
	if(chk == 'updateCon'){
		val = $('#AccessoryCost').val();
		val = textTOnum(val);
		buy = $('#AccessoryCostTabsHidBuy').val();
	}else{
		val = val;
		buy = buy;
	}
	if(val != 0 || val != ''){ val = formatNumber(val,2,1);}
	else if(val == 0){ val = ''; }
	$('#AccessoryCost').val(val); $('#AccessoryCostTabsHid').val(val); $('#AccessoryCostTabsHidBuy').val(buy);
	OthCost = $("#OthCostHidBuy").val();   	// ค่าใช่จ่ายอื่นๆ ที่ซื้อเอง
	if(chk != 'updateCon'){	calPurchasePrice(OthCost,buy); }	// update condition // ค่าใช้จ่ายที่ซื้อเอง
	//calIntPerMont();						// ค่างวด
	calTotal_Budget();						// งบประมาณรวมทั้งหมด

}
function checklistCampaign(Tisname,TisCode,CampaignValue,Status){
	var name = Tisname+TisCode;
	if(document.getElementById('checkCampaignName'+name).checked == true){

		// ## set value to CampaignAllValue text
		var id  = document.getElementById('CampaignCode#@'+Tisname+'#@'+TisCode).value;
		var value = id+'@#'+Tisname+'@#'+CampaignValue;
		document.getElementById('CampaignAllValue'+Tisname+TisCode).innerHTML = '<input type=\"hidden\" name=\"CampaignAllValue[]\" id=\"CampaignAllValue#@'+Tisname+'#@'+TisCode+'\" value=\"'+value+'\"/>';

		setDisplayInline(name);
		setDisplayInline('HidCampaignValue#@'+Tisname+'#@'+TisCode);
	}else{
		//setDisplayNone(name);
		setDisplayNone('HidCampaignValue#@'+Tisname+'#@'+TisCode);
		document.getElementById('CampaignAllValue'+Tisname+TisCode).innerHTML = '';
	}
	calCampaignTabs(Status);
}
function selectAssDesCode(asstype,assname){
	var dataSet={ selectAssDesName: assname, assTypeDesName: asstype, assIDDesc: asstype+assname };
	$.get('cusInterest.php',dataSet, function(data) {
		$('#'+asstype+assname).append('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+data);
		//calAccCostTabs(asstype,assname);
	});
 }
function calCampaignTabs(chk){
	var headbar = $("input[name='HeadTis[]']");
	var val = 0;
	for(var i=0; i<headbar.length;i++){
		var list = document.getElementsByName(headbar[i].value+'[]');
		for(var j=0;j<list.length;j++){
			if(list[j].checked == true){
				var value = list[j].value.split(',');

				var num = document.getElementById('CampaignValue#@'+value[0]+'#@'+value[1]).value;
				var status = document.getElementById('CampaignStatus#@'+value[0]+'#@'+value[1]).value;

				if(status == 2){			// ส่วนลด
					num = num;
				}else if(status == 1){	    // แถม
					num = 0;
				}
				if(num==''){num = 0;}
				num = textTOnum(num);
				val = parseFloat(num)+parseFloat(val);

			} /* end if */

		} /* end loop */

	} /* end loop */

	if(chk == 'updateCon'){  // if copy condition and update
		val = $('#CampaignCost').val();
		val = textTOnum(val);
		val = parseFloat(val);
	}else{
		val = val;
	}

	if(val != 0 || val != ''){ val = formatNumber(val,2,1);}
	else if(val == 0){ val = ''; }
	$('#CampaignCost').val(val);
	calTotal_Budget();
}
function AccAllTextValue(asstype,assname){			/*  ##### insert for AccAllValue text input value #### */
	var AccAllValue = document.getElementById('AccAllValue'+asstype+assname).value;		// all value --> from database
	var num2 = $('#AmountListName'+asstype+assname).val();		// จำนวน
	var Type = document.getElementById('Item_ForAll#@'+asstype+'#@'+assname).value;  // item for
	document.getElementById('AccAllValue'+asstype+assname).value = AccAllValue+'@#'+Type+'@#'+num2;   // insert for new value
}
function calAccCostTabs(asstype,assname,check){
	if(check != 'remove'){
		AccAllTextValue(asstype,assname);
	}
	var headbar = $("input[name='HeadType[]']");
	var accumu = 0; var buy = 0; var OthCost = 0; var AccAllValue = '';
	for(var i=0; i<headbar.length;i++){
		var list = document.getElementsByName(headbar[i].value+'[]');
		for(var j=0;j<list.length;j++){
			if(list[j].checked == true){
				var value = list[j].value.split(',');							// splite for name
				var val = 0;
				var num = $('#AccCostTab'+value[0]+value[1]).val();				// ราคา
				var num2 = $('#AmountListName'+value[0]+value[1]).val();		// จำนวน
				var Type = document.getElementById('Item_ForAll#@'+value[0]+'#@'+value[1]).value;  // item for

				if(Type == "แถม"){
					num = num;
					num2 = num2;
					OthCost = $("#OthCostHidBuy").val();
				}else if(Type == "ซื้อ"){
					num = textTOnum(num);
					num2 = textTOnum(num2);
					buy = textTOnum(buy);
					buy = (parseFloat(num) * parseFloat(num2))+parseFloat(buy);
					if(buy != 0 || buy != ''){ buy = formatNumber(buy,2,1);}
					else if(buy == 0){ buy = ''; }
					num = 0;
					num2 = 0;
					OthCost = $("#OthCostHidBuy").val();
				}

				if(num == ''){
					num = 0;
				}else if(num2 == ''){
					num2 = 0;
				}
				val = parseFloat((parseFloat(val)+(parseFloat(num) * parseFloat(num2))));
				accumu = parseFloat(accumu)+parseFloat(val);

			}	/* ### ---- end if ----- #### */

		}    /* ### ---- end loop -----##### */

	}  /* #### ---- end loop  ----- ##### */
	if(accumu != 0 || accumu != ''){ accumu = formatNumber(accumu,2,1);}
	else if(accumu == 0){ accumu = ''; }
	$('#AccessoryCostTabsHid').val(accumu);
	$('#AccessoryCostTabsHidBuy').val(buy);
	$('#AccessoryCost').val(accumu);
	calPurchasePrice(OthCost,buy);  // ค่าใช้จ่ายที่ซื้อเอง
	//calIntPerMont();				// ค่างวด
	calTotal_Budget();				// งบประมาณรวมทั้งหมด
}







 function CusInt_Question_Answer(){
	setDisplayNone('NegInput');
	setDisplayInline('CusInt_Question');
	var element = document.getElementsByName('select_QuestionModel[]');
	if(element.length>1){
		for( var i=0; i < element.length; i++ ){
			if(i == '0'){ i = ''; }
			setDisplayInline('Sub_CusInt_Question'+i);
		}
	}

 }
 function NegInputSaveTrack(){
	setDisplayNone('CusInt_Question');
	setDisplayInline('NegInput');
	var element = document.getElementsByName('select_NegMainGrp[]');
	if(element.length>1){
		for( var i=0; i < element.length; i++ ){
			if(i == '0'){ i = ''; }
			setDisplayInline('Sub_NegInput'+i);
		}
	}
 }
 function OtherCost(){
	if(document.getElementById('checkedCost').checked == true){setDisplayInline('OtherCost');}
	else{setDisplayNone('OtherCost');}
 }
 function selectAssName(val,id){
	browseCheck('reAssName'+id,'cusInterest.php?selectAssName='+val+'&AssNameID='+id+'&UType='+$("#IntCarType").val()+
	'&UClass='+$("#IntCarPattern").val()+'&UModel='+$("#IntModelCodeName").val(),'');

	$("#AssUseInner"+id).html(val);
 }
 function selectAssDes(val,id){
	var ass_type = document.getElementById('select_acc_use'+id).value;
	//browseCheck('reTooltip'+id,'cusInterest.php?selectAssDes='+val+'&assType='+ass_type+'&assID='+id,'')

	var dataSet={ selectAssDes: val, assType: ass_type, assID:id };
	$.get('cusInterest.php',dataSet, function(data) {
		$('#reTooltip'+id).html(data);
		calAccCost();
	});

	var inne = $("#AssUseInner"+id).html();
	$("#AssUseInner"+id).html(inne+" ("+val+")");
 }
 function CallNegSubGrp(val,id){
	browseCheck('reSunNeg'+id,'cusInterest.php?NegMainGrp='+val+'&NegMainGrpID='+id,'')
	$("#NegInner"+id).html(val);
 }
 function InnerNegSubGrp(val,id){
	var NegInner = $("#NegInner"+id).html();
	$("#NegInner"+id).html(NegInner+" ("+val+")");
 }
 function selectQlist(val,id){
	browseCheck('showQlist'+id,'cusInterest.php?QuestionModel='+val+'&Boxname='+id,'');
	$("#valQues"+id).html("กลุ่มคำถาม  "+val);
 }
function ExchangeBrand(val,id){$("#ExchangeInner"+id).html(val);}
function ExchangeBrandGen(val,id){ var BrandGen = $("#ExchangeInner"+id).html();$("#ExchangeInner"+id).html(BrandGen+" ("+val+")");}
 function addMoreAss(value){
	   var element = document.getElementsByName('AssAmount[]');
	   var com = null;
	   if(element.length > 1){
		com = (element.length)-1;
	   }else{
		com = document.getElementById('chkMoreAccUse').value;
	   }
	  // var com = document.getElementById('chkMoreAccUse').value;
	   var number = parseFloat(com)+1;
	   var area = 'CusInt_AccUse_contain';
	   var name = 'more_AccUse'+number;
	   node = document.getElementById(area);
	   if (node) {
			   var myElement = document.createElement("DIV");
			   myElement.setAttribute("id",name);
			   myElement.setAttribute("class","marginTop widthFull");
			   node.appendChild(myElement);
			   myElement.innerHTML = '$CusInt_Acc_Use';
			   document.getElementById('chkMoreAccUse').value = number;
	   }
	   if($('#IntCarType').val() != ""){
		 value = "groubByCar";
	   }
	//browseCheck(name,'cusInterest.php?MoreAss='+number+'&MoreAssCarModelName='+$('#IntModelCodeName').val()+'&MoreAssCarModelPattern='+$('#IntCarPattern').val()+'&MoreAssCarModelType='+$('#IntCarType').val()+'&CheckPageState='+value,'');

	var dataSet={ MoreAss: number, MoreAssCarModelName: $('#IntModelCodeName').val(), MoreAssCarModelPattern: $('#IntCarPattern').val(),MoreAssCarModelType:$('#IntCarType').val(),CheckPageState:value };
		$.get('cusInterest.php',dataSet, function(data) {
			$("#"+name).html(data);
			$("#checkAss_Type"+number).val("แถม");
	});
	sublist_ass(number);
 }
 function AssType(value,id){
	var checkAss_Type = "#checkAss_Type"+id;
	$(checkAss_Type).val(value);
	calAccCost();
 }
 function sublist_ass(number){
	//var com = document.getElementById('assChk').value;
    //var number = parseFloat(com)+1;
    var number = number;
	var area = 'sublist_ass_contain';
    var name = 'sublist_AccUse'+number;
    node = document.getElementById(area);
    if (node) {
		   var myElement = document.createElement("li");
		   myElement.setAttribute("id",name);
		   node.appendChild(myElement);
		   myElement.innerHTML = "$Acc_Use_list";
		   document.getElementById('assChk').value = number;
    }
	browseCheck(name,'cusInterest.php?moreSubAss='+number,'');
 }
 function addMoreProduct(){
	   var com = document.getElementById('chkVal').value;
	   var number = parseFloat(com)+1;
	   var area = 'CusInt_Product_contain';
	   var name = 'CusInt_Product_list'+number;

	   node = document.getElementById(area);
	   if (node) {
			   var myElement = document.createElement("DIV");
			   myElement.setAttribute("id",name);
			   node.appendChild(myElement);
			   myElement.innerHTML = '$cusInt_product';
			   document.getElementById('chkVal').value = number;
	   }
    browseCheck(name,'cusInterest.php?moreCusIntProduct='+number,'');
 }
 function add_more_product(){
	if(confirm('ต้องการเพิ่มเงื่อนไข !!')==true){
	   var com = document.getElementById('proChk').value;
	   var number = parseFloat(com)+1;
	   var area = 'ul_product';
	   var name = 'phtm'+number;

	   node = document.getElementById(area);
	   if (node) {
			   var myElement = document.createElement("li");

			   myElement.setAttribute("id",name);
			   node.appendChild(myElement);
			   myElement.innerHTML = '$li_product';
			   document.getElementById('proChk').value = number;
	   }
	   browseCheck(name,'cusInterest.php?moreProductCusInt='+number,'');
	}
 }
 function MoreQ(){
	   var element = document.getElementsByName('select_QuestionModel[]');
	   var com = null;
	   if(element.length > 1){
		com = (element.length)-1;
	   }else{
		com = document.getElementById('chkMoreQ').value;
	   }
	  // var com = document.getElementById('chkMoreQ').value;
	   var number = parseFloat(com)+1;
	   var area = 'CusInt_Question_contain';
	   var name = 'Sub_CusInt_Question'+number;
	   node = document.getElementById(area);
	   if (node) {
			   var myElement = document.createElement("DIV");
			   myElement.setAttribute("id",name);
			   myElement.setAttribute("class","widthFull marginTop");
			   myElement.setAttribute("style","background-color:#F4F8D3");
			   node.appendChild(myElement);
			   myElement.innerHTML = '$CusInt_Question_content';
			   document.getElementById('chkMoreQ').value = number;
	   }
	   browseCheck(name,'cusInterest.php?MoreQ='+number,'');
	   sublist_Q(number);
 }
 function sublist_Q(number){
	  //var com = document.getElementById('chkQues').value;
	  //var number = parseFloat(com)+1;
	   var name = 'sublist_rec'+number;
	   var area = 'sublist_Ques_contain';
	   var name = 'sub_question'+number;
	   node = document.getElementById(area);
	   if (node) {
			   var myElement = document.createElement("li");
			   myElement.setAttribute("id",name);
			   node.appendChild(myElement);
			   myElement.innerHTML = '$CusIntQuestion_list';
			   document.getElementById('chkQues').value = number;
	   }
	   browseCheck(name,'cusInterest.php?MoreQli='+number,'');
 }
 function MoreOthercost(condition){
	   var element = document.getElementsByName('OthCostValue[]');
	   var com = null; var number = null;
	   if(element.length > 1){
			if(condition == 'EditBooking'){
				com = ($("#sublist_OtherCost_contain li:last-child a").attr("href").split("#Sub_Condition_OtherCost")[1]);
				if(document.getElementById('sublist_OtherCost'+com)){com = +com+1;} //ver0.26
			}else{com = (element.length)-1;}
	   }else{ if(condition == 'EditBooking'){com = (element.length)-1; }else{ com = document.getElementById('chkMoreOtherCost').value;} }

	   number = parseFloat(com)+1;

	   //alert(($("#sublist_OtherCost_contain li:last-child a").attr("href").split("#Sub_Condition_OtherCost")[1]));

		if(condition == 'EditBooking'){
			if(($("#sublist_OtherCost_contain li:last-child a").attr("href").split("#Sub_Condition_OtherCost")[1]) == number){number = number+1;}
		}
	   var area = 'Condition_OtherCost_contain';
	   var name = 'Sub_Condition_OtherCost'+number;
	   node = document.getElementById(area);
	   if (node) {
			   var myElement = document.createElement("DIV");
			   myElement.setAttribute("id",name);
			   myElement.setAttribute("class","widthFull marginTop");
			   myElement.setAttribute("style","background-color:#F4F8D3");
			   node.appendChild(myElement);
			   myElement.innerHTML = '$CusInt_Condition_OtherCost';
			   document.getElementById('chkMoreOtherCost').value = number;
	   }else{document.getElementById('chkMoreOtherCost').value = number; }

	   editMoreOthercost(name,number);

	   var dataSet={ MoreOtherCost: number, ConditionVal:condition };
		$.get('cusInterest.php',dataSet, function(data) {
			$("#"+name).html(data);
			$("#checkCost_Type"+number).val("แถมลูกค้า");
			sublist_OtherCost(number,condition);
		});

 }
 function sublist_OtherCost(number,condition){
	var number = number;
	var area = 'sublist_OtherCost_contain';
	var name = 'sublist_OtherCost'+number;
	node = document.getElementById(area);
	if (node) {
		var myElement = document.createElement("li");
		myElement.setAttribute("id",name);
		node.appendChild(myElement);
		myElement.innerHTML = '$OtherCost_list';
		document.getElementById('OtherCostChk').value = number;
	}else{ document.getElementById('OtherCostChk').value = number;}
	browseCheck(name,'cusInterest.php?MoreOtherCostli='+number+'&ConditionVal='+condition,'');
 }
 function editMoreOthercost(name,number){ $('#ConOtherCost').tabs('add', '#'+name, "ค่าใช้จ่าย"+ number);}
 function loadTabs(div){
	$(function() {
		// -- variant 2: newly added tabs closable
		var $tabs = $(div).tabs({
			add: function(event, ui) {
				// --- alert('@@@@'+$("#OtherCostLi li:last-child a").attr("href").split("#")[1]);

				var input = null; var Li = null; var textChk = null;
				if(div == '#ConOtherCost'){
					input = (($("#OtherCostLi").children("li").size())-+1);
					//input = $("#OtherCostLi").children("li").size();
					Li = "#OtherCostLi";
					textChk = "textChk";

					// -- select just added tab
					$tabs.tabs('select', $("#OtherCostLi li:last-child a").attr("href"));

				}else if(div == '#CusRecommend'){
					input = (($("#CampaignsLi").children("li").size())-+1);
					//input = $("#OtherCostLi").children("li").size();
					Li = "#CampaignsLi";
					textChk = "CampChk";

					$tabs.tabs('select', $("#CampaignsLi li:last-child a").attr("href"));
				}



				// append close thingy
				$(ui.tab).parents('li:first')
				.append('<em class="ui-tabs-close" title="Close Tab">[x]</em><input type="hidden" id="'+textChk+input+'" value="'+input+'"/>')
				.find('em')
				.click(function() {
					var liName = null; var Val = $(Li).children("li.ui-state-active").children("input").val();	    // define number to remove
					$tabs.tabs('remove', $('li', $tabs).index($(this).parents('li:first')[0]));                     // remove tabs
					//$(div).tabs('remove',$(div).tabs().data("selected.tabs")); // ver0.26

					if(Val == 0){Val = '';}if(div == '#ConOtherCost'){liName = 'sublist_OtherCost';}else if(div == '#CusRecommend'){liName = 'sublist_rec';}
					$('#'+liName+Val).remove();	   // remove li content element
					calOthCost();calRecCost();  //ver0.26
				});
				// -- select just added tab
				// -- $tabs.tabs('option','selected', '#' + ui.panel.id);
			}
		});
	});

 }
 function delTab(div){
	$(div).find('em.ui-tabs-close').bind('click', function() {
		var liName = null; var Val = null; var Li = null; var size = null;
		if(div == '#ConOtherCost'){
			Li = "#OtherCostLi";liName = 'sublist_OtherCost';size = $("input[name='OthCost_Grp[]']").size();
		}else if(div == '#CusRecommend'){
			Li = "#CampaignsLi";liName = 'sublist_rec';size = $("input[name='CusNo_Ref_Rec[]']").size();
		}
		Val = $(Li).children("li.ui-state-active").children("input").val(); if(Val == 0){ Val = '';}
		if(size == 1){
			if(div == '#ConOtherCost'){
				clearValOther();
			}else if(div == '#CusRecommend'){
				clearValCampaign();
			}
			calOthCost(); calRecCost();
			return false;
		}else{
			var $tabs = $(div).tabs();
			$tabs.tabs('remove', $('li', $tabs).index($(this).parents('li:first')[0]));    // remove tabs content
			$('#'+liName+Val).remove();	   								   					// remove li content element
			calOthCost(); calRecCost();
		}
	});
 }
 function clearValOther(){
	$("input[name='OthCost_Grp[]']").val('');$("textarea[name='OthCost_Detail[]']").val('');$("input[name='OthCostValue[]']").val('');
	$("textarea[name='OthCostRemark[]']").val('');alert('เคลียร์ข้อมูลเรียบร้อยครับ!! ');calOthCost();
 }
 function clearValCampaign(){
	$("input[name='Rec_Date[]']").val('');$("input[name='CusNo_Ref_Rec[]']").val('');$("input[name='Rec_Com[]']").val('');
	$("textarea[name='Rec_Remark[]']").val('');	alert('เคลียร์ข้อมูลเรียบร้อยครับ!! ');calRecCost();
 }
 function MoreNeg(chk){
		var chk = chk;
		if(chk == 'CopyCondition'){
			chk = 'CopyCondition';
		}else if(chk == 'CreateNewCondition'){
			chk = 'CreateNewCondition';
		}else if(chk == 'SaveTracking'){
			chk = 'SaveTracking';
		}else{
			chk = 'hidden';
		}
		var com = document.getElementById('chkMoreNegInput').value;
		var number = parseFloat(com)+1;
		var area = 'NegInput_contain';
		var name = 'Sub_NegInput'+number;
		node = document.getElementById(area);
		if (node) {
			var myElement = document.createElement("DIV");
			myElement.setAttribute("id",name);
			myElement.setAttribute("class","widthFull marginTop");
			myElement.setAttribute("style","background-color:#F4F8D3");
			node.appendChild(myElement);
			myElement.innerHTML = '$CusInt_NegInput';
			document.getElementById('chkMoreNegInput').value = number;
		}
		browseCheck(name,'cusInterest.php?MoreNeg='+number+'&changeHidden='+chk,'');
		sublist_Neg(chk);
 }
 function sublist_Neg(chk){
	var chk = chk;
	if(chk == 'CopyCondition'){
		chk = 'CopyCondition';
	}else if(chk == 'CreateNewCondition'){
		chk = 'CreateNewCondition';
	}else if(chk == 'SaveTracking'){
		chk = 'SaveTracking';
	}else{
		chk = 'hidden';
	}
	var com = document.getElementById('NegInputChk').value;
	var number = parseFloat(com)+1;
	var area = 'sublist_NegInput_contain';
	var name = 'sublist_NegInput'+number;
	node = document.getElementById(area);
	if (node) {
		var myElement = document.createElement("li");
		myElement.setAttribute("id",name);
		node.appendChild(myElement);
		myElement.innerHTML = '$NegInput_list';
		document.getElementById('NegInputChk').value = number;
	}
	browseCheck(name,'cusInterest.php?MoreNegli='+number+'&changeHiddenli='+chk,'');
 }
  function MoreExchange(cus_number){
		var element = document.getElementsByName('exchange_car_brand[]');
	    var com = null;
	    if(element.length > 1){
			com = (element.length)-1;
	    }else{
			com = document.getElementById('chkMoreCarExchange').value;
	    }
		var number = parseFloat(com)+1;
		var area = 'CarExchange_contain';
		var name = 'Sub_CarExchange'+number;
		node = document.getElementById(area);
		if (node) {
			var myElement = document.createElement("DIV");
			myElement.setAttribute("id",name);
			myElement.setAttribute("class","widthFull marginTop");
			myElement.setAttribute("style","background-color:#F4F8D3");
			node.appendChild(myElement);
			myElement.innerHTML = '$CarExchange_contain';
			document.getElementById('chkMoreCarExchange').value = number;
		}
		browseCheck(name,'cusInterest.php?MoreExchange='+number+'&ExchangeCus_number='+cus_number,'');
		sublist_Exchange(number);
 }
 function sublist_Exchange(number){
	var number = number;
	var area = 'sublist_CarExchange_contain';
	var name = 'sublist_CarExchange'+number;
	node = document.getElementById(area);
	if (node) {
		var myElement = document.createElement("li");
		myElement.setAttribute("id",name);
		node.appendChild(myElement);
		myElement.innerHTML = '$CarExchange_list';
		document.getElementById('carExchangeChk').value = number;
	}
	browseCheck(name,'cusInterest.php?MoreExchangeli='+number,'');
 }
  function MoreCampaign(){
		var element = document.getElementsByName('CampaignValue[]');
		   var com = null;
		   if(element.length > 1){
			com = (element.length)-1;
		   }else{
			com = document.getElementById('chkMoreTisCampaign').value;
		   }
		//var com = document.getElementById('chkMoreTisCampaign').value;
		var number = parseFloat(com)+1;
		var area = 'TisCampaign_contain';
		var name = 'Sub_TisCampaign'+number;
		node = document.getElementById(area);
		if (node) {
			var myElement = document.createElement("DIV");
			myElement.setAttribute("id",name);
			myElement.setAttribute("class","widthFull marginTop");
			myElement.setAttribute("style","background-color:#F4F8D3");
			node.appendChild(myElement);
			myElement.innerHTML = '$CusInt_TisCampaign';
			document.getElementById('chkMoreTisCampaign').value = number;
		}
		browseCheck(name,'cusInterest.php?MoreCampaign='+number,'');
		sublist_Campaign(number);
 }
 function sublist_Campaign(number){
	//var com = document.getElementById('TisCampaignChk').value;
	//var number = parseFloat(com)+1;
	var number = number;
	var area = 'sublist_TisCampaign_contain';
	var name = 'sublist_TisCampaign'+number;
	node = document.getElementById(area);
	if (node) {
		var myElement = document.createElement("li");
		myElement.setAttribute("id",name);
		node.appendChild(myElement);
		myElement.innerHTML = '$TisCampaign_list';
		document.getElementById('TisCampaignChk').value = number;
	}
	browseCheck(name,'cusInterest.php?MoreCampaignli='+number,'');
 }
 function addMoreCusRecommend(condition){
	   var condition = condition;
	   var element = document.getElementsByName('Rec_Com[]');
	   var com = null;
	   if(element.length > 1){
			if(condition == 'EditBooking'){
				com = ($("#sublist_rec_contain li:last-child a").attr("href").split("#CusInt_CusRecommend_list")[1]);
				if(document.getElementById('CusInt_CusRecommend_list'+com)){com = +com+1;} //ver0.26
			}else{com = (element.length)-1;}
	   }else{
			if(condition == 'EditBooking'){com = (element.length)-1;}else{com = document.getElementById('chkCusRecommend').value;}
	   }
	   var number = parseFloat(com)+1;
	   if(condition == 'EditBooking'){
			if(($("#sublist_rec_contain li:last-child a").attr("href").split("#CusInt_CusRecommend_list")[1]) == number){number = number+1;}
	   }
	   var area = 'CusInt_CusRecommend_contain';
	   var name = 'CusInt_CusRecommend_list'+number;
	   node = document.getElementById(area);
	   if (node) {
		   var myElement = document.createElement("DIV");
		   myElement.setAttribute("id",name);
		   node.appendChild(myElement);
		   myElement.innerHTML = "$CusInt_CusRecommend";
		   document.getElementById('chkCusRecommend').value = number;
	   }else{
		   document.getElementById('chkCusRecommend').value = number;
	   }

		MoreCusRecommend(name,number);

		var dataSet={ moreRecommend: number, ConditionVal:condition };
		$.get('cusInterest.php',dataSet, function(data) {
			$("#"+name).html(data);
			if(condition != 'EditBooking'){
				document.getElementById(name).style.marginTop = '20px';
				document.getElementById(name).style.backgroundColor = '#F4F8D3';
			}
		});

		sublist_rec(number,condition);

	  /*browseCheck(name,'cusInterest.php?moreRecommend='+number+'&ConditionVal='+condition,'');
	    document.getElementById(name).style.backgroundColor = '#F4F8D3';
	    document.getElementById(name).style.marginTop = '20px';
	    sublist_rec(number,condition); */
 }

 function countElement(length_name,name,color){
	var element = document.getElementsByName(length_name);
	var val = element.length%2;
	if(val == '1'){ var div = document.getElementById(name); div.style.backgroundColor = color;}
 }
 function MoreCusRecommend(name,number){ $('#CusRecommend').tabs('add', '#'+name, "ลูกค้าแนะนำ"+ number); }
 function sublist_rec(number,condition){
	var number = number;
    var area = 'sublist_rec_contain';
    var name = 'sublist_rec'+number;
	var name2 = '#sublist_rec'+number;
    node = document.getElementById(area);
    if (node) {
		var myElement = document.createElement("li");
		myElement.setAttribute("id",name);
		node.appendChild(myElement);
		myElement.innerHTML = "$CusRecommend_list";
		document.getElementById('recChk').value = number;
    }else{document.getElementById('recChk').value = number;}

	/* var dataSet = {moreSubRec:number,ConditionVal:condition};
	jGet(name2,'cusInterest.php',dataSet,''); */
	browseCheck(name,'cusInterest.php?moreSubRec='+number+'&ConditionVal='+condition,'');
 }
 function subShow(id,lengthName,name){
	var element = document.getElementsByName(lengthName);
	if(id == ''){id = '0';}
	for( var i=0; i < element.length; i++ ){
		if(id == i ){
			if(name == 'CusInt_CusRecommend_list'){
				activeUiTab('#CusRecommend',i);
			}else if(name == 'Sub_Condition_OtherCost'){
				activeUiTab('#ConOtherCost',i);
			}

			if(i == '0'){ i = ''; }
			setDisplayInline(name+i);

		}else{
			if(i == '0'){ i = ''; }
			setDisplayNone(name+i);
		}
	}
 }
 function CalNetPriceDiscount(Discount){
	var Discount = Discount;
	if(Discount == ''){
		Discount = document.getElementById('Discount').value;
	}
	var IncreaseCarPrices = $("#IncreaseCarPrices").val();  // ส่วนเพิ่มราคารถ
	if(IncreaseCarPrices == ''){
		IncreaseCarPrices = 0;
	}

	IncreaseCarPrices = textTOnum(IncreaseCarPrices);  // ส่วนเพิ่มราคารถ

	Discount = textTOnum(Discount);
	var CRPPrice = document.getElementById('CRPPrice').value;
	CRPPrice = textTOnum(CRPPrice);


	if(CRPPrice == ""){ CRPPrice = '0.00';}
	var NetPrices = ((parseFloat(CRPPrice) - parseFloat(Discount))+parseFloat(IncreaseCarPrices));
	NetPrices = formatNumber(NetPrices,2,1);
	document.getElementById('NetPriceDiscount').value = NetPrices;


	//calIntPerMont(); ค่างวด
	CalNetPrice(NetPrices);
	calTotal_Budget();
 }
 function CalNetPrice(NetPriceDiscount){

 	//alert(document.getElementById("OtherBuyVal").value);
	var NetPriceDiscount = NetPriceDiscount;		// ราคาหักส่วนลด
	if(NetPriceDiscount == ''){
		NetPriceDiscount = 0;
	}
	NetPriceDiscount = $("#NetPriceDiscount").val();
	NetPriceDiscount = textTOnum(NetPriceDiscount);

	var OtherBuyVal = $("#OtherBuyVal").val(); 				// ค่าใช้จ่ายที่ซื้อเพิ่ม
	if(OtherBuyVal == ''){
		OtherBuyVal = 0;
	}
	OtherBuyVal = $("#OtherBuyVal").val();
	OtherBuyVal = textTOnum(OtherBuyVal);
	/*parseFloat(OtherBuyVal)+*/
	var NetPrice = parseFloat(NetPriceDiscount);
	NetPrice = formatNumber(NetPrice,2,1);
	$('#NetPrice').val(NetPrice);
 }
  function SwitchRead(id){
	var val = document.getElementById(id).readOnly;
	if(val == true){
		document.getElementById(id).readOnly = false;
	}
	var Sub_Down = document.getElementById('Sub_Down').value;
	Sub_Down = parseFloat(Sub_Down);

	//calRealDownPay(Sub_Down);
	//calIntPerMont();
 }
 function calRealDownPay(Sub_Down){
	if(Sub_Down == ''){ Sub_Down = 0;}
	Sub_Down = textTOnum(Sub_Down);
	var Down = document.getElementById('Down').value;
	if(Down == ''){Down = 0;}
	Down = textTOnum(Down);
	var Real_DownPay = parseFloat(Down) - parseFloat(Sub_Down);
	Real_DownPay = parseFloat(Real_DownPay);
	Real_DownPay = formatNumber(Real_DownPay,2,1);
	document.getElementById('Real_DownPay').value = Real_DownPay;
	calIntPerMont();
 }
 function calRecCost(){
	var loop = document.getElementsByName('Rec_Com[]'); var val = 0; var value = document.getElementsByName('Rec_Com[]');
	for(var i=0; i < loop.length; i++){
		 var num = value[i].value;
		 if(num==''){num = 0;}
		 num = textTOnum(num);
		 val = parseFloat(num)+parseFloat(val);
	}
	if(val != 0 || val != ''){ val = formatNumber(val,2,1);}
	else if(val == 0){ val = ''; }

	document.getElementById('RecCostHid').value  = val;
	document.getElementById('RecCost').value = val;
	calTotal_Budget();
 }
  function calRecCostkey(){
	var loop = document.getElementsByName('Rec_Com[]');
	var val = 0;
	for(var i=0; i < loop.length; i++){
		if(i == '0'){ i = ''; }
		 var num = document.getElementById('Rec_Com'+i).value;
		 if(num==''){num = 0;}
		 val = parseFloat(num)+parseFloat(val);
	}
	if(val != 0 || val != ''){ val = formatNumber(val,2,1);}
	else if(val == 0){ val = '0.00'; }
	document.getElementById('RecCostHid').value  = val;
	document.getElementById('RecCost').value = val;
	calTotal_Budget();
 }
 function calOthCost(chk){
	var loop = document.getElementsByName('OthCostValue[]');
	var cost = document.getElementsByName('OthCostValue[]');
	var Type = document.getElementsByName('checkCost_Type[]');
	var val = 0; var buy = 0; var AccessoryCost = 0;
	for(var i=0; i <loop.length; i++){
		//alert(Type[i].value);
		// j = i;// if(i == 0){ i = ''; }// var num = document.getElementById('OthCostValue'+i).value;
		var num = cost[i].value;
		if(Type[i].value == "แถมลูกค้า"){
			num = num;
		}else if(Type[i].value == "ทั่วไป"){

			if(num==''){num = 0;}
			num = textTOnum(num);

			buy = textTOnum(buy);
			buy = parseFloat(num)+parseFloat(buy);
			if(buy != 0 || buy != ''){ buy = formatNumber(buy,2,1);}
			else if(buy == 0){ buy = ''; }
			num = 0;

		}

		// check การแสดงผลเมนู
		/*if($("#swap_menu").hasClass('treeMenu') == true){AccessoryCost = $("#AccessoryCostTabsHidBuy").val();}
		else{AccessoryCost = $("#AccessoryCostBuy").val();} */

		 AccessoryCost = $("#AccessoryCostTabsHidBuy").val();

		 if(num==''){num = 0;}
		 num = textTOnum(num);
		 val = parseFloat(num)+parseFloat(val);

	}
	if(val != 0 || val != ''){ val = formatNumber(val,2,1);}
	else if(val == 0){ val = ''; }


	if(chk == 'updateCon'){  // if copy condition and update
		buy = $('#OthCostHidBuy').val();
	}else{
		buy = buy;
	}


	$('#OthCostHidBuy').val(buy);
	document.getElementById('OthCostHid').value  = val;
	document.getElementById('OthCost').value = val;

	if(chk != 'updateCon'){
		//alert(buy+","+AccessoryCost);
		calPurchasePrice(buy,AccessoryCost);
	}
	calTotal_Budget();
 }
 function CostType(value,id){

	//alert('function CostType');
	//return false;
	//var OtherBuyVal = $('#OtherBuyVal').val();
	//alert(value+","+id);

	var checkCost_Type = "#checkCost_Type"+id;

	$(checkCost_Type).val(value);

	calOthCost('');
 }
 function calPurchasePrice(OthCost,AccessoryCost){   	//ค่าใช้จ่ายที่ซื้อเพิ่ม

	if(OthCost == ''){
		OthCost = 0;
	}
	OthCost = textTOnum(OthCost);

	if(AccessoryCost == ''){
		AccessoryCost = 0;
	}
	AccessoryCost = textTOnum(AccessoryCost);



	var PurchasePrice = parseFloat(OthCost) + parseFloat(AccessoryCost);

	if(PurchasePrice != 0 || PurchasePrice != ''){ PurchasePrice = formatNumber(PurchasePrice,2,1);}
	else if(PurchasePrice == 0){ PurchasePrice = '';}
	$("#OtherBuyVal").val(PurchasePrice);
	CalNetPriceDiscount($("#Discount").val());
	//calIntPerMont(); ค่างวด
 }
 function calAccCost(){

	var loop = document.getElementsByName('AssAmount[]');
	var val = 0; var buy = 0; var OthCost = 0;
	for(var i=0; i < loop.length; i++){
		if(i == '0'){ i = ''; }
		 var num = document.getElementById('AccCost'+i).value;
		 var num2 = document.getElementById('AssAmount'+i).value;
		 var Type = document.getElementById('checkAss_Type'+i).value;

		if(Type == "แถม"){
			num = num;
			num2 = num2;
			OthCost = $("#OthCostHidBuy").val();
		}else if(Type == "ซื้อ"){
			if(num == ''){
				num = 0;
			}else if(num2 == ''){
				num2 = 0;
			}
			num = textTOnum(num);
			num2 = textTOnum(num2);
			buy = textTOnum(buy);

			buy = (parseFloat(num) * parseFloat(num2))+parseFloat(buy);
			if(buy != 0 || buy != ''){ buy = formatNumber(buy,2,1);}
			else if(buy == 0){ buy = ''; }
			num = 0;
			num2 = 0;
			OthCost = $("#OthCostHidBuy").val();
		}

		if(num == ''){
			num = 0;
		}else if(num2 == ''){
			num2 = 0;
		}
		val = (parseFloat(num) * parseFloat(num2))+parseFloat(val);
	}

	if(val != 0 || val != ''){ val = formatNumber(val,2,1);}
	else if(val == 0){ val = ''; }
	document.getElementById('AccessoryCostHid').value = val;
	$('#AccessoryCostBuy').val(buy);
	document.getElementById('AccessoryCost').value = val;
	calPurchasePrice(OthCost,buy);
	calIntPerMont();
	calTotal_Budget();
 }
 function calCampaignCost(){
	var loop = document.getElementsByName('CampaignValue[]');
	var val = 0;
	for(var i=0; i < loop.length; i++){
		if(i == '0'){ i = ''; }
			var check = $("#TisCampaignStatus"+i).val();
			var num = document.getElementById('CampaignValue'+i).value;
			if(check == "1"){
				num = 0;
			}else{
				num = num;
			}
			if(num == ''){ num = 0;}
			num = textTOnum(num);
			val = parseFloat(num)+parseFloat(val);
	}
	if(val != 0 || val != ''){ val = formatNumber(val,2,1);}
	else if(val == 0){ val = ''; }
	document.getElementById('CampaignCost').value = val;
	//calTotal_Budget();
 }
 function calTotal_Budget(){
	/* var DiscountVal = document.getElementById('Discount').value;  // ส่วนลด
	if(DiscountVal == ''){
		document.getElementById('Discount').value = '0.00';
	}
	var DownVal = document.getElementById('Down').value; //เงินดาวน์
	if(DownVal == ''){
		document.getElementById('Down').value = '0.00';
	}
	var IntVal = document.getElementById('Int').value; //ดอกเบี้ย
	if(IntVal == ''){
		document.getElementById('Int').value = '0.00';
	}
	var TimeVal = document.getElementById('Time').value; //ดอกเบี้ย
	if(TimeVal == ''){
		document.getElementById('Time').value = '0';
	}
	var Sub_Down = document.getElementById('Sub_Down').value; //  ส่วนลดเงินดาวน์(ซับดาวน์)
	if(Sub_Down == ''){
		document.getElementById('Sub_Down').value = '0.00';
	}
	var Sub_UsedCarVal = document.getElementById('Sub_UsedCar').value;  // ส่วนลดรถมือสอง (ซับรถมือสอง)
	if(Sub_UsedCarVal == ''){
		document.getElementById('Sub_UsedCar').value = '0.00';
	}
	var IncreaseCarPrices = document.getElementById('IncreaseCarPrices').value;  // ส่วนเพิ่มราคารถ
	if(IncreaseCarPrices == ''){
		document.getElementById('IncreaseCarPrices').value = '0.00';
	} */

	/* var NetPrice = document.getElementById('NetPrice').value;
	if(NetPrice == ""){ NetPrice = 0;}
	NetPrice = textTOnum(NetPrice);
	NetPrice = parseFloat(NetPrice);

	var Discount = document.getElementById('Discount').value;  // ถ้าส่วนลดเท่ากับ ค่าว่าง หรือ 0.00
	if(Discount == '0.00'){
		var CRPPrice = document.getElementById('CRPPrice').value;
		if(CRPPrice != '' && CRPPrice != '0.00'){
			CRPPrice = textTOnum(CRPPrice);
			CRPPrice = parseFloat(CRPPrice);
			NetPrice = CRPPrice;
		}
	} */
	//alert('function calTotal_Budget');
	var Discount = document.getElementById('Discount').value;  	// ส่วนลด
	Discount = textTOnum(Discount);
	Discount = parseFloat(Discount);


	var Sub_UsedCar = document.getElementById('Sub_UsedCar').value;
	if(Sub_UsedCar == ""){ Sub_UsedCar = 0; }
	Sub_UsedCar = textTOnum(Sub_UsedCar);
	Sub_UsedCar = parseFloat(Sub_UsedCar);

	var Sub_Down = document.getElementById('Sub_Down').value;
	if(Sub_Down == ""){ Sub_Down = 0;}
	Sub_Down = textTOnum(Sub_Down);
	Sub_Down = parseFloat(Sub_Down);

	var AccessoryCost = document.getElementById('AccessoryCost').value;
	if(AccessoryCost == ""){ AccessoryCost = 0; }
	AccessoryCost = textTOnum(AccessoryCost);
	AccessoryCost = parseFloat(AccessoryCost);

	var RecCost = document.getElementById('RecCost').value;
	if(RecCost == ""){ RecCost = 0; }
	RecCost = textTOnum(RecCost);
	RecCost = parseFloat(RecCost);

	var OthCost = document.getElementById('OthCost').value;
	if(OthCost == ""){ OthCost = 0; }
	OthCost = textTOnum(OthCost);
	OthCost = parseFloat(OthCost);

	var CampaignCost = document.getElementById('CampaignCost').value;
	if(CampaignCost == ""){ CampaignCost = 0; }
	CampaignCost = textTOnum(CampaignCost);
	CampaignCost = parseFloat(CampaignCost);


	var Total_Budget = (((((Discount+Sub_UsedCar)+Sub_Down)+AccessoryCost)+RecCost)+OthCost);
	Total_Budget = (Total_Budget-CampaignCost);

	Total_Budget = parseFloat(Total_Budget);
	if(Total_Budget == 0 || Total_Budget == '' ){ Total_Budget = '';}
	Total_Budget = formatNumber(Total_Budget,2,1);
	document.getElementById('Total_Budget').value = Total_Budget;

 }
 function selectCampaignCode(val,id){
	browseCheck('reCamcode'+id,'cusInterest.php?selectCampaignCode='+val+'&chkID='+id,'');
	$("#camInner"+id).html(val);
 }
 function selectCampaignDetail(val,id){
	var name = document.getElementById('select_TisName'+id).value;
	//document.getElementById('ReCampaignValue').innerHTML = "";
	browseCheck('camcodeId'+id,'cusInterest.php?selectCampaignDetail='+val+'&hid_id='+id+'&CampaignName='+name,'');
	browseCheck('ReCampaignValue'+id,'cusInterest.php?ReCampaignValue='+val+'&hid_id='+id+'&ReCampaignName='+name,'');
	var inne = $("#camInner"+id).html();
	$("#camInner"+id).html(inne+"  ("+val+")");
 }
 function saveCusInt(cus_no){
	//browseCheck('footer','cusInterest.php?&Edit='+cus_no,'');
 }
 function delAllMore(DivName,liName,Val,Count){
	var loop = document.getElementsByName(Count); var Li = null; var value = null;
	if(loop.length == '1'){
		if(DivName == 'Sub_Condition_OtherCost'){
			clearValOther();
		}else if(DivName == 'CusInt_CusRecommend_list'){
			clearValCampaign();
		}else{ alert('ไม่สามารถลบได้ หากต้องการดำเนินการลบ กรุณาแก้ไข หรือ ลบข้อมูล ภายในฟอร์มครับ!! ');}
		//del_all_option(DivName,liName,Val,Count);
		return false;
	}else{ del_all_option(DivName,liName,Val,loop.length);}
	// delete tabs example in  -- editBooking page --
	if(DivName == 'Sub_Condition_OtherCost'){
		$("#ConOtherCost").tabs('remove',$("#ConOtherCost").tabs('option', 'selected'));return false;
	}else if(DivName == 'CusInt_CusRecommend_list'){
		$("#CusRecommend").tabs('remove',$("#CusRecommend").tabs('option', 'selected'));return false;
	}
 }
 function del_all_option(DivName,liName,Val,Count){
	if(Val == 0){ Val = '';}
	if(Count > '1'){
		$('#'+liName+Val).remove();	   // remove li content element
		$('#'+DivName+Val).remove();   // remove content element
	} //$('#'+liName+Val)
	if(liName == 'sublist_rec'){calRecCost();}else{calOthCost();}
}
 function CalCurrentPriceComapre(Discount,id){
	if(id == 0){ id = "";}
	var Discount = Discount;
	if(Discount == ''){
		Discount = '0.00';
	}
	Discount = textTOnum(Discount);

	var Compare_OpenPrice = document.getElementById('Compare_OpenPrice'+id).value;
	if(Compare_OpenPrice == ""){ Compare_OpenPrice = '0.00'; }
	Compare_OpenPrice = textTOnum(Compare_OpenPrice);

	var Compare_CurrentPrice = parseFloat(Compare_OpenPrice) - parseFloat(Discount);
	Compare_CurrentPrice = formatNumber(Compare_CurrentPrice,2,1);
	document.getElementById('Compare_CurrentPrice'+id).value = Compare_CurrentPrice;

	calIntPerMontComapre(id);
	calTotal_Budget_Compare(id);

 }
 function CalComapreDiscount(CurrentPrice,id){
	if(id == 0){ id = "";}
	var CurrentPrice = CurrentPrice;
	if(CurrentPrice == ''){
		CurrentPrice = '0.00';
	}
	CurrentPrice = textTOnum(CurrentPrice);
	var Compare_OpenPrice = document.getElementById('Compare_OpenPrice'+id).value;
	if(Compare_OpenPrice == ""){ Compare_OpenPrice = '0.00'; }
	Compare_OpenPrice = textTOnum(Compare_OpenPrice);

	var Compare_Discount = parseFloat(Compare_OpenPrice) - parseFloat(CurrentPrice);
	Compare_Discount = formatNumber(Compare_Discount,2,1);
	document.getElementById('Compare_Discount'+id).value = Compare_Discount;
 }
 function CalOpenPrice(id){
	/* document.getElementById('Compare_Discount'+id).value = '0.00';
	document.getElementById('Compare_CurrentPrice'+id).value = '0.00'; */
	document.getElementById('Compare_Discount'+id).readOnly = false;
	document.getElementById('Compare_CurrentPrice'+id).readOnly = false;
 }
  function calCompareRealDownPay(Sub_Down,id){
	if(Sub_Down == ''){ Sub_Down = 0;}
	Sub_Down = textTOnum(Sub_Down);

	var Compare_Down = document.getElementById('Compare_Down'+id).value;  // เงินดาวส์
	if(Compare_Down == ''){Compare_Down = 0;}
	Compare_Down = textTOnum(Compare_Down);

	var Real_DownPay = parseFloat(Compare_Down) - parseFloat(Sub_Down);

	Real_DownPay = parseFloat(Real_DownPay);
	Real_DownPay = formatNumber(Real_DownPay,2,1);
	document.getElementById('compare_real_down'+id).value = Real_DownPay;
 }
 function ConverseRealDownPay(val,id){
	if(val == ''){ val = 0;}
	val = textTOnum(val);
	var Compare_Sub_Down = document.getElementById('Compare_Sub_Down'+id).value;
	if(Compare_Sub_Down == ''){Compare_Sub_Down = 0;}
	Compare_Sub_Down = textTOnum(Compare_Sub_Down);
	var Real_DownPay = parseFloat(val) - parseFloat(Compare_Sub_Down);
	Real_DownPay = parseFloat(Real_DownPay);
	Real_DownPay = formatNumber(Real_DownPay,2,1);
	document.getElementById('compare_real_down'+id).value = Real_DownPay;

	calBalanceheldCompare(id);
	calIntPerMontComapre(id);
 }
 function calBalanceheldCompare(id){  // คำนวนยอดจัด
	var down = document.getElementById('Compare_Down'+id).value; // เงินดาวน์
	if(down == ''){ down = 0; }
	down = textTOnum(down);

	var NetPrice = document.getElementById('Compare_CurrentPrice'+id).value; // ราคาหลังหักส่วนลด
	NetPrice = textTOnum(NetPrice);
	var Balanceheld = parseFloat(NetPrice) - parseFloat(down);  // ยอดจัด
	Balanceheld = parseFloat(Balanceheld);
	return Balanceheld;
 }
 function calContractValueCompare(id){ // คำนวนมูลค่่าสัญญา
	 var AllInterest = calAllInterestCompare(id);  // ดอกเบี้ยทั้งหมด
	 var Balanceheld = calBalanceheldCompare(id);  // ยอดจัด
	 var ContractValue = parseFloat(Balanceheld)+ parseFloat(AllInterest);
	 ContractValue = parseFloat(ContractValue);
	 return ContractValue;
 }
 function calAllInterestCompare(id){  // ตำนวนดอกเบี้ยทั้งหมด
	var IntPerYear = CalIntPerYearCompare(id);  // ดอกเบี้ยต่อปี
	var Time = document.getElementById('Compare_Time'+id).value;
	if(Time == ''){ Time = '0.00'; }
	var NumOfYears = parseFloat(Time)/12;
	var AllInterest = parseFloat(IntPerYear) * parseFloat(NumOfYears);
	AllInterest = parseFloat(AllInterest);
	return AllInterest;
 }
 function CalIntPerYearCompare(id){		// คำนวนดอกเบี้ยต่อปี
	var Balanceheld = calBalanceheldCompare(id); // ยอดจัด
	var intVal = document.getElementById('Compare_Int'+id).value; // ดอกเบี้ย
	if(intVal == ''){ intVal = 0; }
	intVal = (parseFloat(intVal)/100);

	var IntPerYear = parseFloat(Balanceheld)*parseFloat(intVal);
	IntPerYear =  parseFloat(IntPerYear);
	return IntPerYear;
 }




 function calIntPerMontComapre(id){

	var val = document.getElementById('Compare_Time'+id).value;
	if(val == ""){
		val = 1;
	}else if(val == 0){
		val = 1;
	}
	var ContractValue = calContractValueCompare(id);
	var IntPerMont = 0;
	IntPerMont = parseFloat(ContractValue)/parseFloat(val);
	IntPerMont = parseFloat(IntPerMont);
	//IntPerMont = eval(parseInt(IntPerMont * 100) * .01); //   ปัดเสษ javascript ขึ้นเป็น  2 ตำแหน่ง
	IntPerMont = IntPerMont.toFixed(2);
	if(IntPerMont == 0){ IntPerMont = '0.00'; }
	IntPerMont = formatNumber(IntPerMont,2,1);
	document.getElementById('Compare_Installment'+id).value = IntPerMont;

 }

 function calTotal_Budget_Compare(id){
	/* var Compare_DiscountVal = document.getElementById('Compare_Discount'+id).value;  // ส่วนลด
	if(Compare_DiscountVal == ''){
		document.getElementById('Compare_Discount'+id).value = '0.00';
	} */
	var DownVal = document.getElementById('Compare_Down'+id).value; //เงินดาวน์
	/* if(DownVal == ''){
		document.getElementById('Compare_Down'+id).value = '0.00';
	} */
	var IntVal = document.getElementById('Compare_Int'+id).value; //ดอกเบี้ย
	/* if(IntVal == ''){
		document.getElementById('Compare_Int'+id).value = '0.00';
	} */
	var TimeVal = document.getElementById('Compare_Time'+id).value; //ดอกเบี้ย
	/* if(TimeVal == ''){
		document.getElementById('Compare_Time'+id).value = '0';
	} */
	var Sub_Down = document.getElementById('Compare_Sub_Down'+id).value; //  ส่วนลดเงินดาวน์(ซับดาวน์)
	/* if(Sub_Down == ''){
		document.getElementById('Compare_Sub_Down'+id).value = '0.00';
	} */
	var Sub_UsedCarVal = document.getElementById('Compare_Sub_UsedCar'+id).value;  // ส่วนลดรถมือสอง (ซับรถมือสอง)
	/* if(Sub_UsedCarVal == ''){
		document.getElementById('Compare_Sub_UsedCar'+id).value = '0.00';
	} */

	var Discount = document.getElementById('Compare_Discount'+id).value;  	// ส่วนลด
	Discount = textTOnum(Discount);
	Discount = parseFloat(Discount);


	var Sub_UsedCar = document.getElementById('Compare_Sub_UsedCar'+id).value;
	if(Sub_UsedCar == ""){ Sub_UsedCar = 0; }
	Sub_UsedCar = textTOnum(Sub_UsedCar);
	Sub_UsedCar = parseFloat(Sub_UsedCar);

	var Sub_Down = document.getElementById('Compare_Sub_Down'+id).value;
	if(Sub_Down == ""){ Sub_Down = 0;}
	Sub_Down = textTOnum(Sub_Down);
	Sub_Down = parseFloat(Sub_Down);

	var AccessoryCost = document.getElementById('Compare_AccessoryCost'+id).value;
	if(AccessoryCost == ""){ AccessoryCost = 0; }
	AccessoryCost = textTOnum(AccessoryCost);
	AccessoryCost = parseFloat(AccessoryCost);

	var Total_Budget = (((Discount+Sub_UsedCar)+Sub_Down)+AccessoryCost);
	Total_Budget = parseFloat(Total_Budget);
	if(Total_Budget == 0 || Total_Budget == '' ){ Total_Budget = '0.00';}
	Total_Budget = formatNumber(Total_Budget,2,1);
	document.getElementById('Compare_Total_Budget'+id).value = Total_Budget;

 }












/* -- Check Before Submit to insert data -- */
function fncSubmit(){
	if(confirm('มีการเพิ่มข้อมูลใหม่ ตรวจสอบถูกต้องแล้ว !!')==true){
		var check = document.getElementById('Interest_pattern').value;var Guide = document.getElementById('Interest_Guide').value;
		if(Guide != ''){if(check == ''){alert('การบันทึกประวัติการสนใจไม่ถูกต้อง กรุณาเลือกรูปแบบการสนใจก่อนบันทึกข้อมูล!! ');return false;}} // end check interest page
	}else{return false;} // end confirm
}
function fncSubmitInterest(){
	if(confirm('ต้องการเพิ่มข้อมูลความสนครั้งใหม่ ตรวจสอบรายละเอียดเงื่อนไข และข้อมูลต่างๆ  ถูกต้องแล้ว !!')==true){
		if( document.getElementById('Branch_Data').value == '' ){ alert('กรุณาเลือกสาขาที่ลูกค้าสนใจ !! ');return false;}
		var name = document.getElementsByName('select_acc_use[]');
		for(var i=0;i < name.length;i++){
			if(i == 0){ i = '';}
			if(document.getElementById('Branch_Data'+i).value != '' && document.getElementById('select_acc_name'+i).value != '' ){
				var j = i+1;if(document.getElementById('AssAmount'+i).value == ''){alert('กรุณากรอกจำนวน อุปกรณ์ตกแต่งในช่องข้อมูลที่'+j+'ด้วยครับ !!');return false;}
			}
		}
		if(document.getElementById('IntCarType').value == ''){
			alert('กรุณาเลือก ชนิดรุ่น ของรถที่ลูกค้าสนใจ !! '); return false;
		}else if(document.getElementById('IntCarPattern').value == ''){
			alert('กรุณาเลือก แบบรถ ที่ลูกค้าสนใจ !! ');return false;
		}else if(document.getElementById('IntModelCodeName').value == ''){
			alert('กรุณาเลือก รหัสแบบรถ ที่ลูกค้าสนใจ !! ');return false;
		}else if(document.getElementById('IntModelGName').value == ''){
			alert('กรุณาเลือก รหัสแบบรถทั่วไป !! ');return false;
		}else if(document.getElementById('CarModelColor').value == ''){
			alert('กรุณาเลือก สี รถที่ลูกค้าสนใจ !! '); return false;
		}else if(document.getElementById('SaleRecThisTime').value == ''){
			alert('กรุณากรอกรายละเอียดผู้รับข้อมูลความสนใจ !! '); return false;
		}else if(document.getElementById('Sale_Responsibility').value == ''){
			alert('กรุณากรอกรายละเอียดผู้รับผิดชอบลูกค้า !! ');return false;
		}else if(document.getElementById('CRPPrice').value == ''){
			alert('กรุณาตรวจสอบข้อมูลมูลรถ และ รายละเอียดเงื่อนไข ราคาตั้ง !! ');return false;
		}else if(document.getElementById('BuyVolume').value == ''){
			alert('กรุณากรอก จำนวนที่สนใจซื้อ  !! ');return false;
		} var Rec_Date = document.getElementsByName('Rec_Date[]');
		for(var i=0;i < Rec_Date.length;i++){
			if(Rec_Date[i].value != ''){ var j = i;if(j == 0){ j = '';}
				if(document.getElementById('CusNo_Ref_Rec'+j).value == ''){alert('กรุณากรอกข้อมูลชื่อผู้แนะนำ  ข้อมูลลูกค้าแนะนำ ช่องที่ '+(j+1));return false;}
			}
		}	calTotal_Budget();
	}else{ return false; }
}
function dblclick_text(temp){suggcont = document.getElementById(temp);if(suggcont.style.display == "block"){count = 0; pos = 0;suggcont.style.display = "none";}}
function style_display_none(id){ document.getElementById(id).style.display = 'none';}
function checktext(obj, jtem, shoe,tshow,nshow ){ drt=obj.innerHTML;splittxt = drt.split("_");document.getElementById(shoe).value  = splittxt[1] ;
	document.getElementById(tshow).value  = splittxt[0];document.getElementById(nshow).value  = splittxt[2] ;
	suggcont = document.getElementById(jtem);suggcont.style.display = "none";document.getElementById(shoe).focus();
}
/* -- approveConprepare -- */
function searchSale(){var sale_id = document.getElementById('sale_id').value;
	var SaleSupervisor_id = document.getElementById('SaleSupervisor_id').value;var BigSupervisor_id = document.getElementById('BigSupervisor_id').value;
	browseCheck('list_sale', 'approveConPrepare.php?BigSupervisor_id='+BigSupervisor_id+'&SaleSupervisor_id='+SaleSupervisor_id+'&sale_id='+sale_id,'');
}
function appConPrepare(SessionID){ hideHeadContent('hide','containHead');
	$('#athree,#ApppreContain').show(); preload('#accessories_tabs_name'); back_to_emp();
	$.get('approveConPrepare.php',{saleSessionidHold:SessionID},function(data){$('#app_five').html(data);/* $('#accordionApppre').tabs('option','selected',0); */});
	$.get('approveConPrepare.php',{saleSessionid:SessionID},function(data){$('#app_one').html(data);$('#case_sale_idcard').val(SessionID);});
	$.get('approveConPrepare.php',{saleSessionidApp:SessionID},function(data){$('#app_three').html(data);});

}
function selectedTabPrepare(num){$('#accordionApppre').tabs('option', 'selected', num);}
function showPrepareDetail(prepare_no,int_num,cus_no,plan_person){
	$('#app_one').prepend("<div style='text-align:center;color:blue;font-size:20px;'><img src=images/preload.gif><br>กรุณารอสักครู่ ระบบกำลังเรียกข้อมูล</div>");
	$.get('approveConPrepare.php',{prepare_cus:cus_no,int_number:int_num,plan_person:plan_person,prepare_no:prepare_no,case_name:'new_prepare'},function(data){
		$('#app_two').html(data); $('#atwo').show(); selectedTabPrepare(2); $('#containAll').tabs('select',5);
		$('#preset_cond input,#preset_cond textarea').attr('readonly',true);$('#preset_cond select,#preset_cond checkbox').attr('disabled','disabled');
		$('#preset_cond .question_list,#preset_cond .hasDatepicker').attr('disabled','disabled'); element = $('.active_cond');
		id = element.attr('id').split('condition_temp')[1];
		make_condition(id.split('_')[0],element.val(),id,'new_prepare',document.getElementById(element.attr('id')));
		if($('#btn_status').val()!=1){ $('#c_action').hide(); } $('#int_num').val(int_num);$('#edit_id').val(cus_no);
	});
}
function showRepareDetail(RepareNo,IntNum,cus_no,PlanPerson,PrepareNo){
	document.getElementById('afour').style.display = '';selectedTabPrepare(4);
	/*
	browseCheck('app_four','approveConPrepare.php?RepareNo='+RepareNo+'&RepareIntNum='+IntNum+'&RepareCusNo='+cus_no+
	'&RePlanPerson='+PlanPerson+'&PrepareNo='+PrepareNo,'');
	*/
	call_overlay('#overlay-prepare');
	$('#app_four').before("<div id='will_process'><div style='text-align: center'><img src='java/img/wait1.gif'/></div><div class='fontBold textCenter' style='font-size:14px;'> will process .. <div><div>");
	$.get('approveConPrepare.php',{RepareNo:RepareNo,RepareIntNum:IntNum,RepareCusNo:cus_no,RePlanPerson:PlanPerson,PrepareNo:PrepareNo},function(data){
		$('#app_four').html(data); $('#will_process').remove(); $('#overlay-prepare').hide();
		switch($('#TodoStatus').val()){
			case 'ยุติ':	$('#oreder_status_pan1').hide(); break;
			case 'จอง':	$('#oreder_status_pan3').hide(); break;
			case 'ไม่จอง': $('#oreder_status_pan1').hide(); break;
			case 'หลุดคู่แข่ง': $('#oreder_status_pan1').hide(); break;
			default:	$('#oreder_status_pan3,#oreder_status_pan1').hide(); break;
		}

		//         เสนอแผน  ขออนุมัติจอง
		if($('#TodoStatus').val()==''){if($('#ContactTypes').val()=='ขออนุมัติจอง'){$('#oreder_status_pan1').show(); }}


		//$('#preset_cond input,#preset_cond textarea').attr('readonly',true);$('#preset_cond select,#preset_cond checkbox').attr('disabled','disabled');
		// $('#preset_cond .question_list,#preset_cond .hasDatepicker').attr('disabled','disabled');
		$('#cusint-1 input,#cusint-1 textarea').attr('readonly',true);$('#cusint-1 select,#cusint-1 checkbox').attr('disabled','disabled');
		$('#cusint-1 .hasDatepicker').attr('disabled','disabled');
		$('.carexchange'). show(); // set bottom เพิ่มลบ รถแลกเปลี่ยน ให้ show
		element = $('.active_cond');  id = element.attr('id').split('condition_temp')[1];
		// make_condition(id.split('_')[0],element.val(),id,'new_prepare',document.getElementById(element.attr('id'))); 12-01-2011
		make_condition(id.split('_')[0],element.val(),id,'approve_report',document.getElementById(element.attr('id')));
		$('#int_num').val(IntNum);$('#edit_id').val(cus_no); $('#containAll').css('border','none');
		selected_tree('6'); cusint_prepare(IntNum,cus_no,'approve_report','edit_condition');  // insert prepare record ใหม่  พร้อม  condition ต่าง
		if($('#condition_change').val()=='condition_change'){       	 // alert show เมื่อมีการแก้ไขเงื่อนไข
			$('#ifcond_change').show();
			$("#dialog").dialog("destroy");$('#dialog-message,#notic_info').show();
			$("#dialog-message").dialog({modal:true,buttons:{Ok:function(){$(this).dialog("close");}}});
		}

	});
}
function fncSubmitApppre(){ if(confirm('คุณต้องการยืนยันเงื่อนไขนี้')==true){ }else{ return false;}}
function sentPostData(cus_no,intNo,intTime){
	var loop = document.getElementsByName('Question[]'); var answer = "";
	for(var i=0; i<loop.length;i++){  var check = document.getElementsByName('A'+loop[i].value+'[]');
		for(var j=0;j<check.length;j++){ var ans = document.getElementById('B'+loop[i].value+'_'+check[j].value).value;
			answer += check[j].value+'_'+ans+'Z@Z';
		}
	}
	var negloop = document.getElementsByName('select_NegMainGrp[]');var NegMainGrp = "";  var NegSubGrp = ""; var Neg_Detail = "";
	var SolveDetail = ""; var SolverRemark = "";
	for(var i=0; i<negloop.length;i++){	var k = i;if( i == 0){ i = ""; }var j = i+1;
		var negVal = document.getElementById('select_NegMainGrp'+i).value; NegMainGrp += negVal+"@#_@#";
		var negSubVal = document.getElementById('select_NegSubGrp'+i).value; NegSubGrp += negSubVal+"@#_@#";
		var Detail = document.getElementById('Neg_Detail'+i).value; Neg_Detail += Detail+"@#_@#";
		var Solve = document.getElementById('SolveDetail'+i).value; SolveDetail += Solve+"@#_@#";
		var Remark = document.getElementById('SolverRemark'+i).value;SolverRemark += Remark+"@#_@#";
	}
	var select_CompetitorName = $("#select_CompetitorName").val();
	var select_CompetitorNickName = $("#select_CompetitorNickName").val();
	var Compare_GModelName = $("#Compare_GModelName").val();var Compare_Color = $("#Compare_Color").val();
	var Compare_OpenPrice = $("#Compare_OpenPrice").val();var Compare_Discount = $("#Compare_Discount").val();
	var Compare_CurrentPrice = $("#Compare_CurrentPrice").val();var Compare_Sub_UsedCar = $("#Compare_Sub_UsedCar").val();
	var Compare_AccessoryCost = $("#Compare_AccessoryCost").val();
	var Compare_AccessoryRemark = $("#Compare_AccessoryRemark").val();
	var Compare_Sell_Type = $("#Compare_Sell_Type").val();var Compare_Down = $("#Compare_Down").val();
	var Compare_Sub_Down = $("#Compare_Sub_Down").val();var Compare_Int = $("#Compare_Int").val();
	var Compare_Time = $("#Compare_Time").val();var Compare_Installment = $("#Compare_Installment").val();
	var Compare_Total_Budget = $("#Compare_Total_Budget").val();var Compare_Salename = $("#Compare_Salename").val();
	var Compare_Position = $("#Compare_Position").val(); var Compare_Tel = $("#Compare_Tel").val();
	var Compare_Remark = $("#Compare_Remark").val(); var RemarkTodo = $("textarea#RemarkTodo").val();
	var ConResult = $("input[name='ConResult']:checked").val();
	if(confirm('คุณต้องการบันทึกข้อมูลการติดตาม ตรวจสอบข้อมูลถูกต้องแล้ว')==true){
	var Contact_Content = document.getElementById('Contact_Content').value;
	var Contact_Person = document.getElementById('Contact_Person').value;
	var Contact_Date = document.getElementById('Contact_Date').value;
	var Contact_ReturnDate = document.getElementById('Contact_ReturnDate').value;
		if(Contact_Content == ''){
			alert('กรุณากรอกรายละเอียดผลการติดตาม');return false;
		}else if(Contact_Person == ''){
			alert('กรุณากรอกรายละเอียดชื่อผู้ติดตาม');return false;
		}else if(Contact_Date == ''){
			alert('กรุณากรอกรายละเอียดวันที่ออกติดตาม');return false;
		}else if(Contact_ReturnDate == ''){
			alert('กรุณากรอกรายละเอียดวันที่กลับมา');return false;
		}else{
			$(function(){
				var url="approveConPrepare.php";
				var dataSet={ Contact_Date: $("input#Contact_Date").val(), Contact_ReturnDate: $("input#Contact_ReturnDate").val(), Contact_Person: $("input#Contact_Person").val(), Contact_Type: $("input[name='Contact_Type']:checked").val(), Contact_Status: $("input[name='Contact_Status']:checked").val(), Contact_Content: $("textarea#Contact_Content").val(),CusNo: $("input#CusNo").val(),Int_Num: $("input#Int_Num").val(),Int_Time: $("input#Int_Time").val(), Con_prepare_Num: $("input#Con_prepare_Num").val(), Answer: answer, NegMainGrp:NegMainGrp, NegSubGrp: NegSubGrp, Neg_Detail:Neg_Detail, SolveDetail:SolveDetail, SolverRemark:SolverRemark, select_CompetitorName:select_CompetitorName, select_CompetitorNickName:select_CompetitorNickName, Compare_GModelName:Compare_GModelName, Compare_Color:Compare_Color, Compare_OpenPrice:Compare_OpenPrice,Compare_Discount:Compare_Discount,Compare_CurrentPrice:Compare_CurrentPrice,Compare_Sub_UsedCar:Compare_Sub_UsedCar,Compare_AccessoryCost:Compare_AccessoryCost, Compare_AccessoryRemark:Compare_AccessoryRemark,Compare_Sell_Type:Compare_Sell_Type, Compare_Down: Compare_Down,Compare_Sub_Down:Compare_Sub_Down,Compare_Int:Compare_Int,Compare_Time:Compare_Time,Compare_Installment:Compare_Installment,Compare_Total_Budget:Compare_Total_Budget,Compare_Salename:Compare_Salename,Compare_Position:Compare_Position,Compare_Tel:Compare_Tel,Compare_Remark:Compare_Remark, RemarkTodo:RemarkTodo,ConResult:ConResult};
				$.post(url,dataSet,function(data){selectedTab(3);
					document.getElementById('cfour').style.display = 'none';
					alert("บันทึกข้อมูล ผลการติดตามเรียบร้อยแล้ว !!");
					browseCheck('box_three','tracking.php?PrepareHis='+cus_no+'&intNo='+intNo+'&check=TrackRecord&TrackintTime='+intTime,'');
				});
			});
			document.getElementById('Contact_Content').value = '';document.getElementById('Contact_Person').value = '';
			document.getElementById('Contact_Date').value = '';document.getElementById('Contact_ReturnDate').value = '';
		}
	}else{return false;}
}
function appRepare(Contact_Person){
	if(confirm('คุณต้องการยืนยันเงื่อนไขนี้')==true){
		
		// var dataSet={ SupComment: $("textarea#SupComment").val(), SupOrder: $("textarea#SupOrder").val(),
		// Oreder_Status:$("input[name='Oreder_Status']:checked").val(),Con_Prepare_Num_Ref: $("input#Con_Prepare_Num_Ref").val(),
		// Con_Rep_Num: $("input#Con_Rep_Num").val(),CusInt_ID_Ref: $("input#CusInt_ID_Ref").val(), TodoStatus:$("input#TodoStatus").val() };
		if($('img.accept').hasClass('hidden') == false){ alert('++    กรุณากดบันทึกรายการเงื่อนไข    ++  '); return false;}
		var order_status = $("input[name='Oreder_Status']:checked").val();
		if(order_status==undefined||order_status==''){ alert('++   กรุณาเลือกความเห็นของผลการติดตาม   ++  '); return false;}
		var dataSet = jQuery('#tabsag-4').find('*').serialize();
		
		//alert(dataSet);
		
		//ตัวแปรสามตัวที่เกี่ยวกับ Prepare ที่หายไป ตอนอนุมัติผลแล้วเป็น undefined ใน Product, Condition
		//Con_Prepare_Num_Ref	28104 น่าจะเป็นค่าเดิม
		//prepare_num_id	29195 น่าจะเป็นค่าใหม่
		//prepare_num_ref	29195 น่าจะเป็นค่าใหม่
		$.post("approveConPrepare.php",dataSet,function(data){
			selectedTabPrepare(3);$('#afour').hide(); alert("ตรวจสอบ ผลการติดตามเรียบร้อยแล้ว !! ");
			//2011-06-02 nz
			browseCheck('main','customers_management.php?toggle_id_card='+$('#toggle_sale_id').val()+'&intnum_active='+$('#intnum_active').val(),'');
		});
		document.getElementById('SupComment').value = '';
		//browseCheck('main','customers_management.php','');return false;//จากหน้าจัดการลูกค้ามุ่งหวัง
		//browseCheck('app_three', 'approveConPrepare.php?saleSessionidApp='+Contact_Person+'&checkStatus=checkStatus','');
	}else{return false;}
}
function changeResponsible(Int_Num,Int_CusNo,Int_Time){
	setDisplayInline('dialogContain');
	$('#dialog').dialog({show: 'slide',hide: 'puff'});
	$('#inputDialog').append("<input type='text' style='width:170px;' id='Sale_Responsibility' name='Sale_Responsibility'onclick='javascript:this.select();' ondblclick='closechoices('tempSale_Responsibility');' autocomplete='off' maxlength='250'/>");
	$('#dialog').dialog('open');
}
function fncChangeRes(){
	var value = $('#Sale_Responsibility').val();alert(value);$('#ListResult').html('');
	$('#ListResult').append("<div id='tempSale_Responsibility' style='text-align: left; width: 205px; display: none;'><div id='sugnsSale_Responsibility' style='cursor:default;position:absolute;background-color:#fff;border:1px solid #777;'></div></div>");
	suggest('tempSale_Responsibility','sugnsSale_Responsibility','Sale_Responsibility','editcus.php?searchs=from_who&search='+value,value);
}
function ChangeRes(Int_Num,Int_CusNo,Int_Time,Sale_ResponsibilityNo){
	document.getElementById('changeResp').style.display='block';document.getElementById('sugnsC').style.display='inline';
	browseCheck('sugnsC','approveConPrepare.php?ResInt_Num='+Int_Num+'&ResInt_CusNo='+Int_CusNo+'&ResInt_Time='+Int_Time+'&Sale_ResponsibilityNo='+Sale_ResponsibilityNo,'');
	nzShowCenter('changeResp');//2011-01-10  java/followcus.js
}
function ConfirmChangeResp(Int_Num,Int_CusNo,Int_Time,OldRespIDCArd){
	if(confirm('คุณต้องการเปลี่ยนพนักงานผู้ดูแลลูกค้าคนใหม่ !!')==true){
		if($("input#Sale_Responsibility_temp").val()==''){ alert('กรุณาระบุชื่อพนักงาน !! '); return false; }  //setDisplayNone('sugnsC');
		$(function(){
			var dataSet={ NewResponsibility: $("input#Sale_Responsibility").val(), NewResInt_Num:Int_Num,
			NewResInt_CusNo:Int_CusNo,NewResInt_Time:Int_Time, OldRespIDCArd:OldRespIDCArd };
			$.post("approveConPrepare.php",dataSet,function(data){
				alert(data); setDisplayNone('changeResp');
				browseCheck('app_five', 'approveConPrepare.php?saleSessionidHold='+OldRespIDCArd,'');
				browseCheck('main','customers_management.php?toggle_id_card='+OldRespIDCArd+'&intnum_active='+$('#intnum_active').val(),'');
			});
		});
	}else{return false;}
}
 /* -- customer page content -- */
function searchcus(check,caseName,cusName,cusNo,url){//naizan add ,url
	if(url){var sendURL = url;}else{var sendURL = "customers.php";}//2010-01-04
	if(caseName == 'bookfromChkStock'){document.getElementById('carBookingStock').innerHTML = '';}
	document.getElementById("footer").style.display = 'none';
	var change_data = '';
	if(caseName == 'change_resperson'){//naizan 2011-01-26
		change_data = 'case_name=change_resperson&cus_tum='+$('#cus_tum').val()+'&car_unit_type='+$('#car_unit_type').val()+'&car_unit_model='+$('#car_unit_model').val()+'&';
	}
	browseCheck( 'list_cus', sendURL+'?'+change_data+'SESSION_Name='+document.getElementById('SESSIONName').value+'&cus_int_num='+document.getElementById('cus_int_num').value+'&cus_name='+document.getElementById('cus_name').value+'&cus_surname='+document.getElementById('cus_surname').value+'&cus_aumpure='+document.getElementById('cus_aumpure').value+'&cus_province='+document.getElementById('cus_province').value+'&cus_type='+document.getElementById('cus_type').value+'&cus_tel='+document.getElementById('cus_tel').value+'&cus_id='+document.getElementById('cus_id').value+'&page='+check+'&caseName='+caseName,'');
}
function showAllCus(){browseCheck('list_cus','interestHistory.php?changeCusType','');}
function searchStock(){browseCheck( 'list_stock', 'cheackStock.php?type='+document.getElementById('car_type').value+'&pattern='+document.getElementById('car_pattern').value+'&color='+document.getElementById('car_color').value+'&parking='+document.getElementById('car_parking').value,'');}
function searchStockKeyUp(val,chk){browseCheck( 'listKeyup', 'cheackStock.php?keyuptype='+document.getElementById('car_type').value+'&keyupColor='+document.getElementById('car_color').value+'&keyupParking='+document.getElementById('car_parking').value+'&keyupCode='+val+'&keyupClass='+document.getElementById('car_pattern').value+'&CheckChk='+chk,'');
}
function changeBox(chk){setDisplayNone('Car_code_hidde');setDisplayInline('Car_code_hidde_select');
	if(chk == 'select'){
		setDisplayNone('Car_code_hidde');setDisplayInline('Car_code_hidde_select');
		document.getElementById('Switch').innerHTML = '<a onclick=\"changeBox(\'box\')\">Switch</a>';
	}else if(chk == 'box'){
		setDisplayNone('Car_code_hidde_select');setDisplayInline('Car_code_hidde');
		document.getElementById('Switch').innerHTML = '<a onclick=\"changeBox(\'select\')\">Switch</a>';
	}
}
function selectTypeAPat(class){var car_type = document.getElementById('car_type').value;
	browseCheck('Car_code_hidde_select','cheackStock.php?Car_class='+class+'&car_type='+car_type,'');
}
function updateBody(chassNo){
	var car_parking2 = document.getElementById('car_parking2').value;
	var BodyCode = document.getElementById('BodyCode').value;
	var BodyDetail = document.getElementById('BodyDetail').value;
	var BodyCostExVat = document.getElementById('BodyCostExVat').value;
	var BodyCostVat = document.getElementById('BodyCostVat').value;
	var BodyCostIncVat = document.getElementById('BodyCostIncVat').value;
	browseCheck('edit_car_stock','cheackStock.php?chass='+chassNo+'&locate='+car_parking2+'&code='+BodyCode+'&detail='+BodyDetail+'&ExVat='+BodyCostExVat+'&Vat='+BodyCostVat+'&BodyCostIncVat='+BodyCostIncVat,'');
}
function searchStockbtn(chk,param){//naizan add param 2011-04-19
	var code = null;var style =document.getElementById('Car_code_hidde_select').style.display;
	var otherParam = '';
	if(style == 'none' || style == ''){code = document.getElementById('car_code').value; }
	else{code = document.getElementById('car_code_name').value;}
	if(param){
		otherParam = '&orderby='+param;
	}
	browseCheck('list_stock','cheackStock.php?type='+document.getElementById('car_type').value+'&pattern='+document.getElementById('car_pattern').value+'&color='+document.getElementById('car_color').value+'&parking='+document.getElementById('car_parking').value+'&code='+code+'&chkPage='+chk+'&MV_Eng_No='+$("#MV_Eng_No").val()+otherParam,'');
}
function searchStockbtn2(chk,param){//pt add function 2011-10-03   use in cheackStock.php
	browseCheck('main','cheackStock.php?CheckBookingcheng='+document.getElementById('h_CheckBookingcheng').value+'&bookIntNum2='+document.getElementById('h_bookIntNum2').value+'&page='+document.getElementById('h_page').value+'&editBooking='+document.getElementById('h_editBooking').value+'&cusnoEdit='+document.getElementById('h_cusnoEdit').value+'&changeBooking='+document.getElementById('h_changeBooking').value+'&type='+document.getElementById('h_type').value+'&class='+document.getElementById('h_class').value+'&gname='+document.getElementById('h_gname').value+'&color='+document.getElementById('h_color').value+'&B_Unit_Model='+document.getElementById('h_B_Unit_Model').value+'&FromCase='+document.getElementById('h_FromCase').value+'&check_casename='+document.getElementById('h_check_casename').value+'&pattern='+document.getElementById('h_pattern').value+'&orderby='+param);
}
function cusDetail(){browseCheck('main','test.php?CusNo=1','');}
function listCus(){browseCheck('list_cus_defaul','customer.php?','');}
function show_all_cus(check,caseName,ssc){
  if(caseName == 'bookfromChkStock'){ document.getElementById('carBookingStock').innerHTML = '';}
  else if(caseName=='') caseName=ssc;document.getElementById("footer").style.display = 'none';
  browseCheck('list_cus','customers.php?show=2&page='+check+'&caseName='+caseName+'','');
}
function edit_cusNo(cus_no,check,case_name){
	if(case_name == 'bookfromChkStock'){$('#carBookingStock').html('');} $("#footer").show(); hideHeadContent('hide','containHead');
	if(case_name == 'CheckResults'){
		browseCheck('footer',check+'&CheckResultsCusNo='+cus_no+'&CheckResultscase_name='+case_name,'');
	}else if(case_name == 'createNew'){
		$.get(check,{Edit:cus_no,case_name:case_name},function(data){
			$('#footer').html(data); $('#tabs3').tabs('option','selected',1);
			if($('#cusint_exited').val()=='cusint_exited'){
				$("#dialog").dialog("destroy"); $('#dialog-confirm-cusint').show();
				$( "#dialog-confirm-cusint" ).dialog({resizable: false,height:140,closeText:'hide',modal: true,
					buttons: {"ยืนยัน":function(){$(this).dialog("close");},
					"ยกเลิก":function(){$(this).dialog("close"); hideHeadContent('show','containHead'); $('#footer').html(''); }}
				});
				$('a.ui-dialog-titlebar-close').hide();
			}
		});
	}else if(case_name == 'list_cusint'){
		$('#footer').prepend(ajax_loader);
		$.get(check,{cus_no:cus_no,case_name:case_name},function(data){ $('#ajaxLoader').remove();  $('#footer').html(data);$('#tabs').tabs(); });

	}else if(case_name == 'change_resperson'){
		//naizan 2011-01-25 โฟกัสที่ผู้รับผิดชอบลูกค้าหลังการขาย
		$('#footer').prepend(ajax_loader);
		$.get(check,{Edit:cus_no,case_name:case_name},function(data){
			$('#ajaxLoader').remove();	$('#footer').html(data);$('#tabs3').tabs('option', 'selected', 7);$('#Resperson').focus();
		});
	}else{
		//alert('');
		browseCheck('footer',check+'&Edit='+cus_no+'&case_name='+case_name,'');
	}
}
function call_track_condition(cus_number,int_number,prepare_number){
	$('#footer').prepend(ajax_loader);
	$.get('tracking.php',{prepare_cus:cus_number,check_result:'call_track_condition',prepare_no:prepare_number,int_number:int_number,case_name:'new_prepare'},
	function(data){
		//$('#prepare_contain').hide(); // ตอนเสนอแผนให้ลัดขั้นตอน
		$('#show_cond_detail,#condition_detail').show();
		$('#condition_detail').html(data).fadeIn('slow');
		$('#close_condition_detail').show(); $('#ajaxLoader').remove();
		$('#preset_cond input,#preset_cond textarea').attr('readonly',true);$('#preset_cond select,#preset_cond checkbox').attr('disabled','disabled');
		$('#preset_cond .question_list,#preset_cond .hasDatepicker').attr('disabled','disabled'); var element = $('.active_cond');
		var id = element.attr('id').split('condition_temp')[1];
		make_condition(id.split('_')[0],element.val(),id,'new_prepare',document.getElementById(element.attr('id')));
		$('#int_num').val(int_number);$('#edit_id').val(cus_number);

		// -- update 07-01-2010 หน้าบันทึกผลการติดตาม
		if($('#btnCallCond').val()=='แก้ไขเงื่อนไข'){ selected_tree('6');  }
	});
}
function show_condition_detail(cus_number,elem){
	$('#accordionApppre').prepend(ajax_loader);
	$.get('list_conprepare.php',{cus_no:cus_number,case_name:'from_getcus_holding'},function(data){
		$('#accordionApppre').hide(); $('#showCusDetail').show();$('#condition_detail').html(data).fadeIn('slow');
		$('#close_condition_detail').show();$('#tabs').tabs(); $('#ajaxLoader').remove(); $('#tabs').css('margin-bottom','50px');
	});
}
function back_to_emp(case_name){
	switch(case_name){
		case 'call_track_condition':
			$('#show_cond_detail,#close_condition_detail,#condition_detail').hide(); $('#condition_detail').html(''); $('#prepare_contain').fadeIn('slow');
			break;
		case 'show_condition_detail':
			$('#close_condition_detail,#condition_detail').hide(); $('#accordionApppre').show(); $('#condition_detail').html('');
			break;
	}
}
function hideHeadContent(chk,div){
	if(chk == 'show'){
		document.getElementById(div).style.display = 'inline';
		document.getElementById('show_hide').innerHTML = '<table width=\"100%\"><tr><td align=\"right\"><a class=\"button\" onclick=\"hideHeadContent(\'hide\',\'containHead\')\"><span>ซ่อนเมนูค้นหา</span></a></td></tr></table>';
	}else if(chk == 'hide'){
		document.getElementById(div).style.display = 'none';
		document.getElementById('show_hide').innerHTML = '<table width=\"100%\"><tr><td align=\"right\"><a class=\"button\" onclick=\"hideHeadContent(\'show\',\'containHead\')\"><span>แสดงเมนูค้นหา</span></td></tr></table>';
	}
}
function hideHeadEditbOOK(chk,div){
	if(chk == 'show'){
		document.getElementById(div).style.display = '';
		document.getElementById('show_hide').innerHTML = '<table width=\"100%\"><tr><td align=\"right\"><a class=\"button\" onclick=\"hideHeadEditbOOK(\'hide\',\'containHead\')\"><span>ซ่อนรายการจอง</span></a></td></tr></table>';
	}else if(chk == 'hide'){
		document.getElementById(div).style.display = 'none';
		document.getElementById('show_hide').innerHTML = '<table width=\"100%\"><tr><td align=\"right\"><a class=\"button\" onclick=\"hideHeadEditbOOK(\'show\',\'containHead\')\"><span>&nbsp;&nbsp;&nbsp;แสดงรายการจอง</span></td></tr></table>';
	}
}
function switchSearchInfo(chk,divshow,divhide){
	if(chk == 'show'){
		document.getElementById(divshow).style.display = 'inline';
		document.getElementById(divhide).style.display = 'none';
		document.getElementById('switchSearchInfo').innerHTML = '<span id=\"switchSearchInfo\" class=\"fontBold cursor\" OnMouseOver=\"this.style.color=\'#6B6730\'\" OnMouseOut=\"this.style.color=\'#ff0000\'\" style=\"font-size:14px;color:#ff0000;\" onclick=\"switchSearchInfo(\'hide\',\'key_search_car\',\'key_search_cus\')\">ค้นหาจากข้อมูลรถ</span>';
	}else if(chk == 'hide'){
		document.getElementById(divshow).style.display = 'inline';
		document.getElementById(divhide).style.display = 'none';
		document.getElementById('switchSearchInfo').innerHTML = '<span id=\"switchSearchInfo\" class=\"fontBold cursor\" OnMouseOver=\"this.style.color=\'#6B6730\'\" OnMouseOut=\"this.style.color=\'#ff0000\'\" style=\"font-size:14px;color:#ff0000;\" onclick=\"switchSearchInfo(\'show\',\'key_search_cus\',\'key_search_car\')\">ค้นหาจากข้อมูลลูกค้า</span>';
	}
}
function hideCondition(chk){
	if(chk == 'show'){
		document.getElementById('TreeIntContain').style.display = 'inline';
		document.getElementById('hideCodition').innerHTML = '<ul id=\"icons\" class=\"ui-widget ui-helper-clearfix\"><li class=\"ui-state-default ui-corner-all\" title=\".ui-icon-carat-1-n\" onclick=\"hideCondition(\'hide\')\"><span class=\"ui-icon ui-icon-carat-1-n\"></span></li></ul>';
	}else if(chk == 'hide'){
		document.getElementById('TreeIntContain').style.display = 'none';
		document.getElementById('hideCodition').innerHTML = '<ul id=\"icons\" class=\"ui-widget ui-helper-clearfix\"><li class=\"ui-state-default ui-corner-all\" title=\".ui-icon-carat-1-s\" onclick=\"hideCondition(\'show\')\"><span class=\"ui-icon ui-icon-carat-1-s\"></span></li></ul>';
	}
}
function clear_all_cus(){}//  document.getElementById("footer").style.display = 'none';}
function showCusDetail(cusNo){
	document.getElementById('showCusDetail').style.display='block';browseCheck('sugnsA','customers.php?ViewCusDetail='+cusNo,'');
}

function addEmpRelat(){
	var cont = document.getElementsByName('check_cus_more2[]');	var conts = '';
	for (var i=0;i<cont.length;i++) {if(cont[i].checked == true) {conts += cont[i].value+"_";}	}
	var relate = document.getElementsByName('Rel_Level2[]');var relates = '';
	for (var i=0;i<relate.length;i++) {relates += relate[i].value+"_";}
	var exten = document.getElementsByName('Rel_Grp2[]'); var extens = '';
	for (var i=0;i<exten.length;i++) {extens += exten[i].value+"_";}
	var note = document.getElementsByName('Rel_Remark2[]'); var notes = '';
	for (var i=0;i<note.length;i++) {notes += note[i].value+"_";}
	var cus_no = document.getElementById('serial').value;
	for (var i=0;i<relate.length;i++) {if(relate[i].value == ''){var j = i+1;alert('กรุณาเลือกความสัมพันธ์กับพนักงาน  คนที่'+j+' !! ');return false;}}
	browseCheck('ReleEmpContain','oeditcus.php?relateEmp='+conts+'&relate='+relates+'&Rel_Grp='+extens+'&Note='+notes+'&cus_no='+cus_no,'');
	document.getElementById('sHowCus').innerHTML = '';
}
function addCusRelat(){
	var cont = document.getElementsByName('check_cus_more[]');var refCus_no = '';
	for (var i=0;i<cont.length;i++) {if(cont[i].checked == true) {refCus_no += cont[i].value+"_";}}
	var relate = document.getElementsByName('Rel_Level[]');var relates = '';
	for (var i=0;i<relate.length;i++) {relates += relate[i].value+"_";}
	var exten = document.getElementsByName('Rel_Grp[]'); var extens = '';
	for (var i=0;i<exten.length;i++) {extens += exten[i].value+"_";}
	var note = document.getElementsByName('Rel_Remark[]'); var notes = '';
	for (var i=0;i<note.length;i++) {notes += note[i].value+"_";}
	var cus_no = document.getElementById('serial').value;
	for (var i=0;i<relate.length;i++) {if(relate[i].value == ''){var j = i+1;alert('กรุณาเลือกความสัมพันธ์กับลูกค้า  คนที่'+j+' !! ');return false;}}
	browseCheck('RelateCus','oeditcus.php?relateCus='+refCus_no+'&relate='+relates+'&Rel_Grp='+extens+'&Note='+notes+'&cus_no='+cus_no,'');
	document.getElementById('sHowCusd').innerHTML = '';
}
function CalBodyCostIntVat(val){
	var value = textTOnum(val);value = parseFloat(value);var vat  = value*0.07;var formatVat = formatNumber(value*0.07,2,1);
	document.getElementById('BodyCostVat').value= formatVat;
	var BodyCostIncVat = formatNumber((value + parseFloat(vat)),2,1);document.getElementById('BodyCostIncVat').value= BodyCostIncVat;
}
function CalBodyCostExVat(val){
	var value = textTOnum(val); value = parseFloat(value); var vat  = value*0.07;
	var formatVat = formatNumber(value*0.07,2,1);document.getElementById('BodyCostVat').value= formatVat;
	var BodyCostIncVat = formatNumber((value - parseFloat(vat)),2,1);document.getElementById('BodyCostExVat').value= BodyCostIncVat;
}

	/* --  Manage Main data Content -- */

	function addProvince(){

		var province = document.getElementById('province').value;
		var geo = document.getElementById('GEO').value;
		if(province == '' || geo == ''){
			alert('กรุณากรอกชื่อจังหวัด หรือเลือกภาค');
			return false;
		}else{
			browseCheck('message','manageMainData.php?NewProvince='+province+'&GEO_NAME='+geo,'');
			browseCheck('ProvinceGroup','manageMainData.php?Prolist='+geo,'');
		}
	}
	function provinceGroup(geo){
		browseCheck('ProvinceGroup','manageMainData.php?GEO_Amp_Edit='+geo,'');
	}
	function SearchPro(){
			var geo = document.getElementById('GEO2').value;
			var province = document.getElementById('pro_search').value;
			browseCheck('ProvinceGroup','manageMainData.php?searchPro='+province+'&searchGEO='+geo,'');
	}
	function editProvince(province,geo,geo_name){
			browseCheck('editProvince','manageMainData.php?editProvince='+province+'&OldGEO='+geo+'&geoName='+geo_name,'');
			document.getElementById('editProvince').style.display = '';
	}
	function delProvince(province,geo_id){
		if(confirm('คุณต้องการลบข้อมูล !!')==true){
			browseCheck('message','manageMainData.php?delProvince='+province+'&delGEO='+geo_id,'');
			browseCheck('ProvinceGroup','manageMainData.php?Prolist='+geo_id,'');
		}else{
			return false;
		}
	}
function SaveEditPro(){
	var geo = document.getElementById('GEO_Edit').value;var pro = document.getElementById('province_edit').value;
	var pro_id = document.getElementById('pro_id').value;
	browseCheck('editProvince','manageMainData.php?SaveEditPro='+pro+'&SEditGEO='+geo+'&updateId='+pro_id,'');
	browseCheck('ProvinceGroup','manageMainData.php?Prolist='+geo,'');
}
function GroupPro(geo_name,GroupPro,check){browseCheck(GroupPro,'manageMainData.php?GroupPro='+geo_name+'&check='+check,'');			}
	function addAmpure(){
		var geo = document.getElementById('ampure_geo').value;
		var provinceName = document.getElementById('provinceName').value;
		var ampure = document.getElementById('ampure').value;
		var ampCode = document.getElementById('ampureCode').value;
		var postcode = document.getElementById('PostCode').value;
		var postOff = document.getElementById('postOff').value;
		var note = document.getElementById('noteAmp').value;


		if(provinceName == ''){
			alert('กรุณาเลือกจังหวัด');
		}

browseCheck('editAmp','manageMainData.php?AddAmp='+ampure+'&AddAmpCode='+ampCode+'&AddGEO='+geo+'&AddPostCode='+postcode+'&AddProName='+provinceName+'&AddPostOff='+postOff+'&AddNote='+note,'');
		browseCheck('AmpGroup','manageMainData.php?Ampurelist='+geo+'&proAmp='+provinceName,'');


	}
	function editAmp(geo,pro,amp,post){
		browseCheck('editAmp','manageMainData.php?EditAmp='+amp+'&EditPro='+pro+'&EditGEO='+geo+'&EditPost='+post,'');
		document.getElementById('editAmp').style.display = '';
	}
	function viewAmp(geo,pro,amp,post){
		browseCheck('editAmp','manageMainData.php?viewGEO='+geo+'&viewPro='+pro+'&viewAmp='+amp+'&viewPost='+post,'');
	}
function delAmp(geo,pro,amp,post,id){
	if(confirm('คุณต้องการลบข้อมูล !!')==true){
		browseCheck('editAmp','manageMainData.php?delAmp='+amp+'&delPro='+pro+'&delGEO='+geo+'&delPost='+post+'&AmpId='+id,'');
		browseCheck('AmpGroup','manageMainData.php?Ampurelist='+geo+'&proAmp='+pro,'');
	}else{
		return false;
	}
}

function GroupAmp(geo_name,pro_name,div,check){
	browseCheck(div,'manageMainData.php?GroupAmp_geo='+geo_name+'&GroupAmp_pro='+pro_name+'&check='+check,'');	    // GroupAmp
}
	function SearchAmp(){
		var geo = document.getElementById('ampure_geo_search').value; var pro = document.getElementById('ampure_pro_search').value; var Ampure2 = document.getElementById('Ampure2').value;
		var postCode2 = document.getElementById('postCode2').value;
		if(Ampure2 == null){
			Ampure2 = "";
		}
		if(postCode2 == null){
			postCode2 = "";
		}
		browseCheck('AmpGroup','manageMainData.php?SearchAmp='+Ampure2+'&SearchPost='+postCode2+'&SearchPro='+pro+'&SearchGEO='+geo,'');
	}
	function SaveEditAmp(){
		var pro = document.getElementById('Pro_Edit_Amp').value;
		var geo = document.getElementById('GEO_Edit_Amp').value;

		browseCheck('editAmp','manageMainData.php?SaveEditAmp='+document.getElementById('editAmpName2').value+'&SaveEditCode='+document.getElementById('editAmpCode2').value+'&SaveEditPost='+document.getElementById('editAmpPost2').value+'&SaveEditOffice='+document.getElementById('editAmpOffice2').value+'&SaveEditNote='+document.getElementById('editAmpNote2').value+'&SaveEditProvin='+document.getElementById('Pro_Edit_Amp').value+'&SaveEditGeo='+document.getElementById('GEO_Edit_Amp').value+'&editAmphur_id='+document.getElementById('editAmphur_id2').value,'');
		browseCheck('AmpGroup','manageMainData.php?Ampurelist='+geo+'&proAmp='+pro,'');
	}

	function addTumbon(){

		var geo = document.getElementById('tumbon_geo').value;			 // geo name
		var province = document.getElementById('tumbon_pro').value;   // province name
		var ampure = document.getElementById('tumbon_amp').value;		 // ampure name
		var tumbon = document.getElementById('tumbon').value;
		var tumCode = document.getElementById('tumCode').value;

		browseCheck('editTum','manageMainData.php?NewTumbon='+tumbon+'&TumbonAmpure='+ampure+'&TumbonProvince='+province+'&TumbonGEO='+geo+'&TumCode='+tumCode,'');
		browseCheck('TumbonGroup','manageMainData.php?GeoTumbonlist='+geo+'&proTum='+province+'&ampTum='+ampure,'');

	}

	function viewTum(district_id,district_name){
		browseCheck('editTum','manageMainData.php?viewTum='+district_id+'&districtName='+district_name,'');  // Define districtName for check edit or view
	}
	function editTum(district_id,district_name){
		browseCheck('editTum','manageMainData.php?EditTum='+district_id+'&districtName='+district_name,'');  // Define districtName for check edit or view
		document.getElementById('editTum').style.display = '';
	}
	function delTum(district_id,geo,province,ampure){
		if(confirm('คุณต้องการลบข้อมูล')==true){
			browseCheck('editTum','manageMainData.php?delTum='+district_id,'');
			browseCheck('TumbonGroup','manageMainData.php?GeoTumbonlist='+geo+'&proTum='+province+'&ampTum='+ampure,'');
			// Receive id
		}else{
			return false;
		}
	}
	function SearchTum(){
		var geo = document.getElementById('tum_geo_search').value;  		//  geo_name
		var pro = document.getElementById('tum_pro_search').value;			//	 pro_name
		var amp = document.getElementById('tum_amp_search').value;			//	 amp_name
		var tumbon = document.getElementById('tumSearch').value;			//  tum_name

		browseCheck('TumbonGroup','manageMainData.php?searchTumGeo='+geo+'&searchTumPro='+pro+'&searchTumAmp='+amp+'&searchTum='+tumbon,'');
	}
	function SaveEditTum(){
		var geo = document.getElementById('tum_geo_edit').value;  		//  geo_name
		var pro = document.getElementById('tum_pro_edit').value;			//	 pro_name
		var amp = document.getElementById('tum_amp_edit').value;			//	 amp_name
		var tumbon = document.getElementById('tumbonEdit').value;			//  tum_name

		browseCheck('editTum','manageMainData.php?SaveEditTum_GEO='+geo+'&SaveEditTum_Pro='+pro+'&SaveEditTum_Amp='+amp+'&STum_Name='+tumbon+'&SaveEditTum_Code='+document.getElementById('tumCodeEdit').value+'&SaveEditTum_ID='+document.getElementById('tumEdit_id').value,'');

		browseCheck('TumbonGroup','manageMainData.php?GeoTumbonlist='+geo+'&proTum='+pro+'&ampTum='+amp,'');   // Receive name


	}

					/* --- JOB CONTENT  -----*/


function addJob(){
	var NewJob = $('#cusJob').val(); var TIS_ID_REF = $('#TIS_ID_REF').val(); var TYPE_OF_CUS = $("input[name='TYPE_OF_CUS']:checked").val();
	if(NewJob==''){ alert("กรุณาระบุอาชีพ "); return false; }
	// if(TIS_ID_REF==''){ alert("กรุณาระบุรหัสอ้างอิง TIS "); return false; }
	// if(TYPE_OF_CUS==''){ alert("กรุณาระบุประเภทลูกค้า  "); return false; }
	$.get('manageMainData.php',{NewJob:NewJob,JobDesc:$('#JobDes').val(),TIS_ID_REF:TIS_ID_REF,TYPE_OF_CUS:TYPE_OF_CUS},function(data){
		$('#box_two').html(data); browseCheck('Job_list','manageMainData.php?Joblist','');
	});
}
function editJob(job){call_dialog('manageMainData.php?editJob='+job,'divShowForm');ShowCenter('divShowForm');}
function SaveEdit(){
	var SJob = $('#EJob').val();if(SJob==''){ alert("กรุณาระบุอาชีพ "); return false; }
	$.get('manageMainData.php',{SJob:SJob,SJobDes:$('#EJobDes').val(),JobID:$('#JobID').val(),TIS_ID_REF:$('#TIS_ID_REF').val(),
	TYPE_OF_CUS:$("input[name='TYPE_OF_CUS']:checked").val()},function(data){
		close_dialog(); alert('แก้ไขข้อมูลเรียบร้อย ');browseCheck('Job_list','manageMainData.php?Joblist','');
	});

}
function delJob(job){
	if(confirm('คุณต้องการลบข้อมูล !!')==true){
		browseCheck('editJob','manageMainData.php?delJob='+job,'');
		browseCheck('Job_list','manageMainData.php?Joblist','');
   }else{
		return false;
   }
}




	/* Question Content */

	function addQGroup(){
		var QgroupName = document.getElementById('txtQgroup').value;
		var txtQuestionModel = document.getElementById('txtQuestionModel').value;

		if(confirm('คุณต้องการเพิ่มข้อมูล')==true){

	      browseCheck('showmsgQgroup','manageMainData.php?QgroupName='+QgroupName+'&txtQuestionModel='+txtQuestionModel+'&txtQuestionStp='+document.getElementById('txtQuestionStp').value+'&txtRemark='+document.getElementById('txtRemark').value,'');
	      browseCheck('QGroupList','manageMainData.php?Qgrouplist','');
	      browseCheck('reSubQgroup','manageMainData.php?ReMainQgroup','');       // refresh select Qgroup in box Question
	   }else{
			return false;
	   }
	}
	function delQgroup(delID){
	   if(confirm('คุณต้องการลบข้อมูล')==true){
	      browseCheck('delMsgQgroup','manageMainData.php?delQgroup='+delID,'');
	      browseCheck('QGroupList','manageMainData.php?Qgrouplist','');
	      browseCheck('reSubQgroup','manageMainData.php?ReMainQgroup','');       // refresh select Qgroup in box Question
	   }else{
			return false;
	   }
	}
	function editQgroup(editID){
	    browseCheck('delMsgQgroup','manageMainData.php?editQgroup='+editID+'chkStatus=','');
	    document.getElementById('delMsgQgroup').style.display='inline';
	}
	function SaveEditQgroup(){
	   var QGroupName = document.getElementById('txtQgroup_edit').value;
	   var QGroupID = document.getElementById('QgroupID_edit').value;

	   if(confirm('คุณต้องการแก้ไขข้อมูล')==true){
	      browseCheck('delMsgQgroup','manageMainData.php?SaveEditQgroup='+QGroupName+'&QGroupID='+QGroupID,'');
	      browseCheck('QGroupList','manageMainData.php?Qgrouplist','');
	      browseCheck('reSubQgroup','manageMainData.php?ReMainQgroup','');       // refresh select Qgroup in box Question
	   }else{
			return false;
	   }
	}
	function addQuestion(){
	   var Qgroup_select = document.getElementById('Qgroup_select').value; var sub_question = document.getElementById('sub_question').value;
	   var sub_QuestionModel = document.getElementById('sub_QuestionModel').value; var sub_QuestionStp = document.getElementById('sub_QuestionStp').value;
	   var sub_Remark = document.getElementById('sub_Remark').value;
		if(Qgroup_select == ''){
		   alert('กรุณาเลือกกลุ่มคำถาม !!');
		}else{
		   if(confirm('คุณต้องการเพิ่มข้อมูล')==true){

	         browseCheck('QuestionMsg','manageMainData.php?Qgroup_select='+Qgroup_select+'&sub_question='+sub_question+'&sub_QuestionModel='+sub_QuestionModel+'&sub_QuestionStp='+sub_QuestionStp+'&sub_Remark='+sub_Remark,'');
	         browseCheck('QuestionList','manageMainData.php?Questionlist='+Qgroup_select,'');
	      }else{
			return false;
		  }
	   }
	}
	function delQuestion(delID){
	   if(confirm('คุณต้องการลบข้อมูล')==true){
	      browseCheck('ManageQuestionMsg','manageMainData.php?delSubQuestion='+delID,'');
	      browseCheck('QuestionList','manageMainData.php?Questionlist','');
	   }else{
			return false;
	   }
	}
	function editQuestion(editID,check){
	      browseCheck('ManageQuestionMsg','manageMainData.php?editQuestion='+editID+'&check='+check,'');
	      document.getElementById('ManageQuestionMsg').style.display = 'inline';
	}
	function SaveEditQuestion(){
         var QuestionTrcNo_edit = document.getElementById('QuestionTrcNo_edit').value; var Qgroup_select_edit = document.getElementById('Qgroup_select_edit').value;
         var sub_question_edit = document.getElementById('sub_question_edit').value; var sub_QuestionModel_edit = document.getElementById('sub_QuestionModel_edit').value;
         var sub_QuestionStp_edit = document.getElementById('sub_QuestionStp_edit').value; var sub_Remark_edit = document.getElementById('sub_Remark_edit').value;
         browseCheck('ManageQuestionMsg','manageMainData.php?QuestionTrcNo_edit='+QuestionTrcNo_edit+'&Qgroup_select_edit='+Qgroup_select_edit+'&sub_question_edit='+sub_question_edit+'&sub_QuestionModel_edit='+sub_QuestionModel_edit+'&sub_QuestionStp_edit='+sub_QuestionStp_edit+'&sub_Remark_edit='+sub_Remark_edit,'');
          browseCheck('QuestionList','manageMainData.php?Questionlist='+Qgroup_select_edit,'');
	}
	function viewQuestion(Viewid,check){
       browseCheck('viewQuestion','manageMainData.php?viewQuestion='+Viewid+'&check='+check,'');
	}



	/* Questionniare content */

	function addQuestionForm(name){


	    var topic = document.getElementById('Qgroup_naire').value;
	    var newDiv = document.getElementById('wrapper_table').innerHTML;          // qlist select to add new div
	    var grou_id = document.getElementById('txtQgroupValue').value;           // define Qgroup id
	    var num_check = document.getElementById('num_check').value;
	    var number = parseFloat(num_check)+1;

	    var area = 'wrapper_area';
	    var divname = topic;
	    var Qgroup_id = document.getElementById('txtQgroupValue').value;


	    if(!document.getElementById(divname)){
  	    node = document.getElementById(area);
  	    if (node) {
  			  var myElement = document.createElement("div");
  			  myElement.setAttribute("id",divname);
  			  myElement.style.paddingLeft = "15px";
  			  myElement.style.backgroundColor = "#fcfbc3";
			    myElement.style.fontWeight = "bold";
  	      node.appendChild(myElement);
  			  myElement.innerHTML = '<span>'+topic+'<input type=\"hidden\" name=\"topicName[]\" value=\"'+topic+'\"><input type=\"hidden\" name=\"QgroupVal_id[]\" value=\"'+Qgroup_id+'\"/></span>';

  			  if(document.getElementById(divname)){

  			  var loop = document.getElementsByName(name);


    		  for(var i=0;i<loop.length;i++){

    			     if(loop[i].checked == true){
    			        var id = i+1;
    			    	  var inner_val = document.getElementById('QuesString'+id).innerHTML;
    			    	  var hidden_val = document.getElementById('hiddenQlist_id'+id).value;
    			        var divnamelist = inner_val;
    			        var myElementList = document.createElement("table");
            			myElementList.setAttribute("id",divnamelist);
            			myElementList.style.width = "100%";
            			myElementList.style.paddingLeft = "30px";
            			myElementList.style.backgroundColor = "#cce8d1";
            	    node.appendChild(myElementList);
            			myElementList.innerHTML = '<tr><td><span>'+divnamelist+'</span><span><input type=\"hidden\" name=\"'+divnamelist+'\" value=\"'+divnamelist+'\"/> <input type=\"hidden\" name=\"Qlist[]\" value=\"'+hidden_val+'_'+Qgroup_id+'\"/></span></td><td style=\"float:right;margin-right:20px;cursor: pointer; \"><a onclick=\"delqlist('+divnamelist+')\"><img name=\"del\" src=\"temp/i_del.gif\"/></a></td></tr>';
                  //browseCheck('Qfooter','questionnaire.php?Qstatus='+inner_val+'&topic='+topic,'');
    		       }
    		    }
  			 }
  	   }
  	    var num_check = number;
  	    browseCheck('Qfooter','questionnaire.php?Qstatus='+inner_val+'&topic='+grou_id,'');
	  }

	  document.getElementById('QuestionList').innerHTML = '';
	  document.getElementById('wrapper_table').innerHTML = '';
	  document.getElementById('QuestionList_more').innerHTML = '';
	  document.getElementById('wrapper_table_head').style.display = 'none';

	  browseCheck('Qfooter','questionnaire.php?Qfooter='+num_check,'');

	}
	function createQues(){
	    var val = document.getElementById('txtQname').value;
	    if(val == ''){
	       alert('กรุณากรอกชื่อแบบสอบถาม');
	    }else{
	      browseCheck('wrapper','questionnaire.php?createQuestitle='+val,'');
	    }
	}
  function SubmitQlist(){
    var name = document.getElementsByName('question[]');
		var tick = 0;
			for(var i=0;i < name.length;i++){
				if(name[i].checked==true){
					tick=1;
				}
			}
			if(tick==0){
					alert('การบันทึกข้อมูล ต้องเลือกคำถามอย่างน้อย 1 ข้อ');
					return false;
			}else{
			 // if(confirm('ตรวจสอบข้อมูลถูกต้องแล้ว')==true){
			     addQuestionForm('question[]');
			 // }
			}
  }
  function addQuestionMore(num_check){

     browseCheck('Qfooter','questionnaire.php?addQuestionMore='+num_check,'');
  }
  function selectQgroup(value,check){       // check define default or more group
     var QuestionList = '';
     if(check == 'default'){
        QuestionList = 'QuestionList';
     }else if(check == 'more'){
       QuestionList = 'QuestionList_more';
     }
     browseCheck(QuestionList,'questionnaire.php?Qgroup_naire='+value,'');
     //document.getElementById('wrapper_table_head').style.display = '';
  }
  function delqlist(div_id){
    //alert(div_id);
    document.getElementById(div_id).innerHTML = '';
  }
  function searchQuestionnaire(val){
     browseCheck('exited_questionnaire','questionnaire.php?searchQuestionnaire='+val,'');
  }
  function showAllNairelist(){
     browseCheck('exited_questionnaire','questionnaire.php?showAllNairelist','');
  }
  function delQnaire(Delid){
    if(confirm('ต้องการลบข้อมูล')==true){
      browseCheck('exited_questionnaire','questionnaire.php?DelQnaire='+delid,'');
      browseCheck('exited_questionnaire','questionnaire.php?showAllNairelist','');
    }
  }
  function ViewQnaire(Viewid){
     browseCheck('Qfooter','questionnaire.php?ViewQnaire='+Viewid,'');
  }
  function editQnaire(Editid){
     browseCheck('Qfooter','questionnaire.php?editQnaire='+Editid,'');
  }
  function copyQnaire(Copyid){
    alert('Copyid');
  }

  /* campaign content */
  function clearTextValue(id){
	document.getElementById(id).value = '';
  }
  function btnAddCampaign(){
	document.getElementById('TisName').value = ''; document.getElementById('CampaignCode').value = '';
	document.getElementById('CampaignValue').value = ''; document.getElementById('Detail').value = '';
	document.getElementById('StrDate').value = ''; document.getElementById('StpDate').value = '';
	document.getElementById('CamRemark').value = '';
	setDisplayInline('manageAllCampaign');
	browseCheck('addEditCam','manageMainData.php?CaseAdd=btnAddCampaign','');
  }
  function addCampaign(){
	if(confirm('ต้องการบันทึกข้อมูลแคมเปญ ตรวจสอบรายละเอียดถูกต้องแล้ว !!')==true){
	 var TisName = document.getElementById('TisName').value;  var CampaignCode = document.getElementById('CampaignCode').value;
	 var CampaignValue = document.getElementById('CampaignValue').value;  var Detail = document.getElementById('Detail').value;
	 var StrDate = document.getElementById('StrDate').value;  var StpDate = document.getElementById('StpDate').value;
	 var CamRemark = document.getElementById('CamRemark').value;  var paignStatus = $('input[name=paignStatus]:checked').val();
	 if(TisName == ''){
		alert('กรุณากรอกชื่อแคมเปญ ');
		return false;
	 }else if(CampaignCode == ''){
		alert('กรุณากรอกรหัสแคมเปญ  ');
		return false;
	 }else if(CampaignValue == ''){
		alert('กรุณากรอกมูลค่าแคมเปญ  ');
		return false;
	 }else if(StpDate == ''){
		alert('กรุณาระบุวันที่เลิกใช้งาน ');
		return false;
	 }
	 browseCheck('messageCampaign','manageMainData.php?NewTisName='+TisName+'&NewCampaignCode='+
	 CampaignCode+'&NewCampaignValue='+CampaignValue+'&NewDetail='+Detail+'&NewStrDate='+StrDate+
	 '&NewStpDate='+StpDate+'&NewCamRemark='+CamRemark+'&paignStatus='+paignStatus,'');
	 document.getElementById('TisName').value = ''; document.getElementById('CampaignCode').value = '';
	 document.getElementById('CampaignValue').value = ''; document.getElementById('Detail').value = '';
	 document.getElementById('StrDate').value = ''; document.getElementById('StpDate').value = '';
	 document.getElementById('CamRemark').value = '';
	 browseCheck('reTisName','manageMainData.php?RefreshSelect=Campaign','');
	 browseCheck('Re_list_comapign','manageMainData.php?RelistComapign','');
	}else{
		return false;
	}
 }
 function editCampaign(id){
	setDisplayInline('manageAllCampaign');
	browseCheck('addEditCam','manageMainData.php?EditCampaignId='+id,'');
 }
 function saveEditCampaign(){
	if(confirm('ต้องการแก้ไขข้อมูลแคมเปญ ตรวจสอบรายละเอียดถูกต้องแล้ว !!')==true){
	 var TisName = document.getElementById('TisName').value;  var CampaignCode = document.getElementById('CampaignCode').value;
	 var CampaignValue = document.getElementById('CampaignValue').value;  var Detail = document.getElementById('Detail').value;
	 var StrDate = document.getElementById('StrDate').value;  var StpDate = document.getElementById('StpDate').value;
	 var CamRemark = document.getElementById('CamRemark').value;  var CamID = document.getElementById('CamID').value;
	 var paignStatus = $('input[name=paignStatus]:checked').val();
	if(TisName == ''){
		alert('กรุณากรอกชื่อแคมเปญ ');
		return false;
	 }else if(CampaignCode == ''){
		alert('กรุณากรอกรหัสแคมเปญ  ');
		return false;
	 }else if(CampaignValue == ''){
		alert('กรุณากรอกมูลค่าแคมเปญ  ');
		return false;
	 }else if(StpDate == ''){
		alert('กรุณาระบุวันที่เลิกใช้งาน ');
		return false;
	 }
	 browseCheck('messageCampaign','manageMainData.php?EditTisName='+TisName+'&EditCampaignCode='+
	 CampaignCode+'&EditCampaignValue='+CampaignValue+'&EditDetail='+Detail+'&EditStrDate='+StrDate+
	 '&EditStpDate='+StpDate+'&EditCamRemark='+CamRemark+'&CamID='+CamID+'&paignStatus='+paignStatus,'');
	 browseCheck('reTisName','manageMainData.php?RefreshSelect=Campaign','');
	 browseCheck('Re_list_comapign','manageMainData.php?RelistComapign','');
	 setDisplayNone('manageAllCampaign');
	}else{
		return false;
	}
 }
 function delCampaign(id){
	if(confirm('ต้องการลบข้อมูลแคมเปญ หมายเลขที่  '+id+'  !!')==true){
	 browseCheck('messageCampaign','manageMainData.php?DelCampaignId='+id,'');
	 browseCheck('reTisName','manageMainData.php?RefreshSelect=Campaign','');
	 browseCheck('Re_list_comapign','manageMainData.php?RelistComapign','');
	}else{
		return false;
	}
 }
 function searchCampaign(){
	var select_TisName = document.getElementById('select_TisName').value;
	//alert("TisName:"+select_TisName);
	var shrCode = document.getElementById('shrCode').value;
	browseCheck('Re_list_comapign','manageMainData.php?SearchTisName='+select_TisName+'&shrCode='+shrCode,'');
 }
 function ListAllCampaign(){
	 browseCheck('Re_list_comapign','manageMainData.php?RelistComapign','');
 }
 function btnAddAcc(){
	 document.getElementById('AssType').value = '';  document.getElementById('AssName').value = '';
	 document.getElementById('BuyFrom').value = '';  document.getElementById('AssDes').value = '';
	 document.getElementById('StartDate').value = ''; document.getElementById('StopDate').value = '';
	 document.getElementById('ItemCost_ExVat').value = ''; document.getElementById('ItemCost_IncVat').value = '';
	 document.getElementById('LaborCost_ExVat').value = ''; document.getElementById('LaborCost_IncVat').value = '';
	 document.getElementById('TotalCost_IncVat').value = '';document.getElementById('Remark').value = '';

	 setDisplayInline('manageAllAcc');
	 browseCheck('addEditAcc','manageMainData.php?CaseAdd=btnAddAcc','');
  }

  function calAccIncVat(val){
	var ItemCost_ExVat = textTOnum(val);
	var vat = 0.07;
	var IncVat = parseFloat(ItemCost_ExVat) * parseFloat(vat);
	var ItemCost_Vat = formatNumber(IncVat,2,1);
	document.getElementById('ItemCost_Vat').value = ItemCost_Vat;

	var ItemCost_IncVat = parseFloat(ItemCost_ExVat) + parseFloat(IncVat);
	ItemCost_IncVat = formatNumber(ItemCost_IncVat,2,1);
	document.getElementById('ItemCost_IncVat').value = ItemCost_IncVat;
	calTotalCostIncVat();
  }
  function calAccExVat(val){
	var ItemCost_InVat = textTOnum(val);
	var vat = 0.07;
	var IncVat = parseFloat(ItemCost_InVat) * parseFloat(vat);
	var ItemCost_Vat = formatNumber(IncVat,2,1);
	document.getElementById('ItemCost_Vat').value = ItemCost_Vat;

	var ItemCost_ExVat = parseFloat(ItemCost_InVat) - parseFloat(IncVat);
	ItemCost_ExVat = formatNumber(ItemCost_ExVat,2,1);
	document.getElementById('ItemCost_ExVat').value = ItemCost_ExVat;
	calTotalCostIncVat();

  }
  function CalLaborCostIncVat(val){
	var LaborCost_ExVat = textTOnum(val);
	var vat = 0.07;
	var IncVat = parseFloat(LaborCost_ExVat) * parseFloat(vat);
	var LaborCost_Vat = formatNumber(IncVat,2,1);
	document.getElementById('LaborCost_Vat').value = LaborCost_Vat;

	var LaborCost_IncVat = parseFloat(LaborCost_ExVat) + parseFloat(IncVat);
	LaborCost_IncVat = formatNumber(LaborCost_IncVat,2,1);
	document.getElementById('LaborCost_IncVat').value = LaborCost_IncVat;
	calTotalCostIncVat();
  }
  function CalLaborCostExVat(val){
	var LaborCost_InVat = textTOnum(val);
	var vat = 0.07;
	var IncVat = parseFloat(LaborCost_InVat) * parseFloat(vat);
	var LaborCost_Vat = formatNumber(IncVat,2,1);
	document.getElementById('LaborCost_Vat').value = LaborCost_Vat;

	var LaborCost_ExVat = parseFloat(LaborCost_InVat) - parseFloat(IncVat);
	LaborCost_ExVat = formatNumber(LaborCost_ExVat,2,1);
	document.getElementById('LaborCost_ExVat').value = LaborCost_ExVat;
	calTotalCostIncVat();

  }
  function calTotalCostIncVat(){
	var ItemCost_IncVat = document.getElementById('ItemCost_IncVat').value;
	if(ItemCost_IncVat == ''){ ItemCost_IncVat = 0; }
	ItemCost_IncVat = textTOnum(ItemCost_IncVat);
	var LaborCost_IncVat = document.getElementById('LaborCost_IncVat').value;
	if(LaborCost_IncVat == ''){ LaborCost_IncVat = 0; }

	var ItemCost_ExVat = document.getElementById('ItemCost_ExVat').value;
	if(ItemCost_ExVat == ''){ ItemCost_ExVat = 0; }
	ItemCost_ExVat = textTOnum(ItemCost_ExVat);

	var LaborCost_ExVat = document.getElementById('LaborCost_ExVat').value;
	if(LaborCost_ExVat == ''){ LaborCost_ExVat = 0; }
	LaborCost_ExVat = textTOnum(LaborCost_ExVat);

	var TotalCost_ExVat = parseFloat(ItemCost_ExVat) + parseFloat(LaborCost_ExVat);
	TotalCost_ExVat = formatNumber(TotalCost_ExVat,2,1);
	document.getElementById('TotalCost_ExVat').value = TotalCost_ExVat;


	var ItemCost_Vat = document.getElementById('ItemCost_Vat').value;
	if(ItemCost_Vat == ''){ ItemCost_Vat = 0; }
	ItemCost_Vat = textTOnum(ItemCost_Vat);

	var LaborCost_Vat = document.getElementById('LaborCost_Vat').value;
	if(LaborCost_Vat == ''){ LaborCost_Vat = 0; }
	LaborCost_Vat = textTOnum(LaborCost_Vat);

	var TotalCost_Vat = parseFloat(ItemCost_Vat) + parseFloat(LaborCost_Vat);
	TotalCost_Vat = formatNumber(TotalCost_Vat,2,1);
	document.getElementById('TotalCost_Vat').value = TotalCost_Vat;

	LaborCost_IncVat = textTOnum(LaborCost_IncVat);
	var TotalCost_IncVat = parseFloat(ItemCost_IncVat) + parseFloat(LaborCost_IncVat);
	TotalCost_IncVat = formatNumber(TotalCost_IncVat,2,1);
	document.getElementById('TotalCost_IncVat').value = TotalCost_IncVat;
  }
  function delAccBase(id){
	if(confirm('ต้องการลบข้อมูลของแต่ง !!')==true){
	 browseCheck('messageAcc','manageMainData.php?DelAccId='+id,'');
	 browseCheck('ReAssType','manageMainData.php?RefreshSelect=AccBase','');
	 browseCheck('Re_list_Acc','manageMainData.php?RelistAcc','');
	}else{  return false; }
 }
 function editAccBase(id){
	setDisplayInline('manageAllAcc');

	/* var dataSet={ EditAccId: id  };
	$.get('manageMainData.php',dataSet, function(data) {
		$('#addEditAcc').html(data);
	}); */

	browseCheck('addEditAcc','manageMainData.php?EditAccId='+id,'');

 }
  function addAcc(){
	if(confirm('ต้องการบันทึกข้อมูลของแต่ง ตรวจสอบรายละเอียดถูกต้องแล้ว !!')==true){
		var AssType = document.getElementById('AssType').value;
		var AssName = document.getElementById('AssName').value;

	 if(AssType == ''){
		alert('กรุณากรอกหมวด ');
		return false;
	 }else if(AssName == ''){
		alert('กรุณากรอกชื่อของแต่ง   ');
		return false;
	 }
	/*  browseCheck('messageAcc','manageMainData.php?NewAssType='+AssType+'&NewAssName='+
	 AssName+'&NewBuyFrom='+BuyFrom+'&NewAssDes='+AssDes+'&NewStartDate='+StartDate+
	 '&NewStopDate='+StopDate+'&ItemCost_ExVat='+ItemCost_ExVat+'&ItemCost_IncVat='+
	 ItemCost_IncVat+'&LaborCost_ExVat='+LaborCost_ExVat+'&LaborCost_IncVat='+LaborCost_IncVat+
	'&SaleAcc='+SaleAcc+'&TotalCost_IncVat='+TotalCost_IncVat+'&Remark='+Remark+'&ItemCost_Vat='+ItemCost_Vat+'&LaborCost_Vat='+
	 LaborCost_Vat+'&TotalCost_ExVat='+TotalCost_ExVat+'&TotalCost_Vat='+TotalCost_Vat+'&IntCarType='+IntCarType+
	 '&IntCarPattern='+IntCarPattern+'&IntModelCodeName='+IntModelCodeName,''); */

	$(function(){
			var dataSet = jQuery("#myForm").find('input, textarea, select').serialize();
			$.post("manageMainData.php",dataSet,function(data){
				if(data){alert(data);};
			});
	});


	 document.getElementById('AssType').value = '';  document.getElementById('AssName').value = '';
	 document.getElementById('BuyFrom').value = '';  document.getElementById('AssDes').value = '';
	 document.getElementById('StartDate').value = ''; document.getElementById('StopDate').value = '';
	 document.getElementById('ItemCost_ExVat').value = ''; document.getElementById('ItemCost_IncVat').value = '';
	 document.getElementById('LaborCost_ExVat').value = ''; document.getElementById('LaborCost_IncVat').value = '';
	 document.getElementById('TotalCost_IncVat').value = '';document.getElementById('Remark').value = '';
	 document.getElementById('ItemCost_Vat').value = '';document.getElementById('LaborCost_Vat').value = '';
	 document.getElementById('TotalCost_ExVat').value = '';document.getElementById('TotalCost_Vat').value = '';
	 document.getElementById('SaleAcc').value = ''; $("#data2").html(''); $("#AssUnit").val('');


	 browseCheck('ReAssType','manageMainData.php?RefreshSelect=AccBase','');
	 browseCheck('Re_list_Acc','manageMainData.php?RelistAcc','');
	}else{
		return false;
	}
  }
 function saveEditAccBase(){
	if(confirm('ต้องการแก้ไขข้อมูลของแต่ง ตรวจสอบรายละเอียดถูกต้องแล้ว !!')==true){
	 var AssType = document.getElementById('AssType').value;
	 var AssName = document.getElementById('AssName').value;
	 if(AssType == ''){
		alert('กรุณากรอกหมวด ');
		return false;
	 }else if(AssName == ''){
		alert('กรุณากรอกชื่อของแต่ง   ');
		return false;
	 }

	 $(function(){
			var dataSet = jQuery("#myForm").find('input, textarea, select').serialize();
			$.post("manageMainData.php",dataSet,function(data){
				alert(data);
			});
	});

	 document.getElementById('AssType').value = '';  document.getElementById('AssName').value = '';
	 document.getElementById('BuyFrom').value = '';  document.getElementById('AssDes').value = '';
	 document.getElementById('StartDate').value = ''; document.getElementById('StopDate').value = '';
	 document.getElementById('ItemCost_ExVat').value = ''; document.getElementById('ItemCost_IncVat').value = '';
	 document.getElementById('LaborCost_ExVat').value = ''; document.getElementById('LaborCost_IncVat').value = '';
	 document.getElementById('TotalCost_IncVat').value = '';document.getElementById('Remark').value = '';
	 document.getElementById('ItemCost_Vat').value = '';document.getElementById('LaborCost_Vat').value = '';
	 document.getElementById('TotalCost_ExVat').value = '';document.getElementById('TotalCost_Vat').value = '';
	 document.getElementById('AssCode').value = ''; $("#data2").html('');


	browseCheck('ReAssType','manageMainData.php?RefreshSelect=AccBase','');
	 browseCheck('Re_list_Acc','manageMainData.php?RelistAcc','');
	 setDisplayNone('manageAllAcc');
	}else{
		return false;
	}
 }
 function searchAssBase(){
	var select_acc_use = document.getElementById('select_acc_use').value;
	var shrAssName = document.getElementById('shrAssName').value;
	browseCheck('Re_list_Acc','manageMainData.php?SearchAssType='+select_acc_use+'&shrAssName='+shrAssName,'');
 }
 function ListAllAcc(){
	 browseCheck('Re_list_Acc','manageMainData.php?RelistAccBase','');
 }
 function btnAddNeg(){
	 document.getElementById('Neg_MainGrp').value = '';  document.getElementById('Neg_SubGrp').value = '';
	 document.getElementById('Neg_Detail').value = '';  document.getElementById('Neg_Remark').value = '';
	 setDisplayInline('manageAllNeg');
	 browseCheck('addEditNeg','manageMainData.php?CaseAdd=btnAddNeg','');
 }
 function addNeg(){
	if(confirm('ต้องการบันทึกข้อมูล Neg ตรวจสอบรายละเอียดถูกต้องแล้ว !!')==true){
	  var Neg_MainGrp = document.getElementById('Neg_MainGrp').value;
	  var Neg_SubGrp = document.getElementById('Neg_SubGrp').value;
	  var Neg_Detail = document.getElementById('Neg_Detail').value;
	  var Neg_Remark = document.getElementById('Neg_Remark').value;
	 if(Neg_MainGrp == ''){
		alert('กรุณากรอกรายละเอียด กลุ่มหลัก  ');
		return false;
	 }else if(Neg_SubGrp == ''){
		alert('กรุณากรอกรายละเอียด กลุ่มรอง ');
		return false;
	 }
	 browseCheck('messageNeg','manageMainData.php?NewNeg_MainGrp='+Neg_MainGrp+'&NewNeg_SubGrp='+
	 Neg_SubGrp+'&NewNeg_Detail='+Neg_Detail+'&NewNeg_Remark='+Neg_Remark,'');

	 document.getElementById('Neg_MainGrp').value = ''; document.getElementById('Neg_SubGrp').value = '';
	 document.getElementById('Neg_Detail').value = '';  document.getElementById('Neg_Remark').value = '';

	 browseCheck('NegMainGrp','manageMainData.php?RefreshSelect=MainNeg','');
	 browseCheck('Re_list_Neg','manageMainData.php?RelistNeg','');
	}else{
		return false;
	}
 }
 function DelNeg(id){
	if(confirm('ต้องการลบข้อมูล  Neg !!')==true){
	 browseCheck('messageNeg','manageMainData.php?DelNegId='+id,'');
	 browseCheck('NegMainGrp','manageMainData.php?RefreshSelect=MainNeg','');
	 browseCheck('Re_list_Neg','manageMainData.php?RelistNeg','');
	}else{  return false; }
 }
 function editNeg(id){
	setDisplayInline('manageAllNeg');
	browseCheck('addEditNeg','manageMainData.php?EditNegId='+id,'');
 }
 function saveEditNeg(){
	if(confirm('ต้องการแก้ไขข้อมูล Neg ตรวจสอบรายละเอียดถูกต้องแล้ว !!')==true){
	  var Neg_MainGrp = document.getElementById('Neg_MainGrp').value;
	  var Neg_SubGrp = document.getElementById('Neg_SubGrp').value;
	  var Neg_Detail = document.getElementById('Neg_Detail').value;
	  var Neg_Remark = document.getElementById('Neg_Remark').value; var Neg_ID = document.getElementById('Neg_ID').value;

		 if(Neg_MainGrp == ''){
			alert('กรุณากรอกรายละเอียด กลุ่มหลัก  ');
			return false;
		 }else if(Neg_SubGrp == ''){
			alert('กรุณากรอกรายละเอียด กลุ่มรอง ');
			return false;
		 }
	 browseCheck('messageNeg','manageMainData.php?EditNeg_MainGrp='+Neg_MainGrp+'&EditNeg_SubGrp='+
	 Neg_SubGrp+'&EditNeg_Detail='+Neg_Detail+'&EditNeg_Remark='+Neg_Remark+'&Neg_ID='+Neg_ID,'');
	 Neg_MainGrp = document.getElementById('Neg_MainGrp').value = '';
	 Neg_SubGrp = document.getElementById('Neg_SubGrp').value = '';
	 Neg_Detail = document.getElementById('Neg_Detail').value = '';
	 Neg_Remark = document.getElementById('Neg_Remark').value = ''; document.getElementById('Neg_ID').value = '';
	 browseCheck('NegMainGrp','manageMainData.php?RefreshSelect=MainNeg','');
	 browseCheck('Re_list_Neg','manageMainData.php?RelistNeg','');
	 setDisplayNone('manageAllNeg');
	}else{
		return false;
	}
 }
 function ListAllNeg(){
	  document.getElementById('select_NegMainGrp').value = ''; document.getElementById('shrNegSubGrp').value = '';
	  browseCheck('Re_list_Neg','manageMainData.php?RelistNeg','');
 }
 function searchNeg(){
	var select_NegMainGrp = document.getElementById('select_NegMainGrp').value;
	var shrNegSubGrp = document.getElementById('shrNegSubGrp').value;
	browseCheck('Re_list_Neg','manageMainData.php?SearchNegMainGrp='+select_NegMainGrp+'&shrNegSubGrp='+shrNegSubGrp,'');
 }
 function btnAddQues(){
	 document.getElementById('QuestionModel').value = ''; document.getElementById('Question').value = '';
	 document.getElementById('Remark').value = '';  document.getElementById('QuetionStr').value = '';
	 document.getElementById('QuestionStp').value = '';
	 setDisplayInline('manageAllQues');
	 browseCheck('addEditQues','manageMainData.php?CaseAdd=btnAddQues','');
 }
 function addQues(){
	if(confirm('ต้องการบันทึกข้อมูล คำถาม ตรวจสอบรายละเอียดถูกต้องแล้ว !!')==true){
	  var QuestionModel = document.getElementById('QuestionModel').value;
	  var Question = document.getElementById('Question').value;
	  var Remark = document.getElementById('RemarkQ').value;
	  var QuetionStr = document.getElementById('QuetionStr').value;
	  var QuestionStp = document.getElementById('QuestionStp').value;

	 if(QuestionModel == ''){
		alert('กรุณากรอกรายละเอียด กลุ่มคำถาม ');
		return false;
	 }else if(Question == ''){
		alert('กรุณากรอกรายละเอียดคำถาม ');
		return false;
	 }
	 browseCheck('messageQues','manageMainData.php?NewQuestionModel='+QuestionModel+'&NewQuestion='+
	 Question+'&NewRemark='+Remark+'&NewQuetionStr='+QuetionStr+'&NewQuestionStp='+QuestionStp,'');

	 document.getElementById('QuestionModel').value = ''; document.getElementById('Question').value = '';
	 document.getElementById('RemarkQ').value = '';  document.getElementById('QuetionStr').value = '';
	 document.getElementById('QuestionStp').value = '';
	 browseCheck('QuestionModelVal','manageMainData.php?RefreshSelect=Question','');
	 browseCheck('Re_list_Ques','manageMainData.php?RelistQues','');
	}else{
		return false;
	}
 }
 function DelQues(id){
	if(confirm('ต้องการลบข้อมูล คำถาม !!')==true){
	 browseCheck('messageQues','manageMainData.php?DelQuesId='+id,'');
	 browseCheck('QuestionModelVal','manageMainData.php?RefreshSelect=Question','');
	 browseCheck('Re_list_Ques','manageMainData.php?RelistQues','');
	}else{  return false; }
 }
 function ListAllQues(){
	 // document.getElementById('select_NegMainGrp').value = ''; document.getElementById('shrNegSubGrp').value = '';
	   browseCheck('Re_list_Ques','manageMainData.php?RelistQues','');
 }
 function editQues(id){
	setDisplayInline('manageAllQues');
	browseCheck('addEditQues','manageMainData.php?EditQuesId='+id,'');
 }
 function saveEditQues(){
	if(confirm('ต้องการแก้ไขข้อมูล คำถาม ตรวจสอบรายละเอียดถูกต้องแล้ว !!')==true){
	  var QuestionModel = document.getElementById('QuestionModel').value;
	  var Question = document.getElementById('Question').value;
	  var Remark = document.getElementById('RemarkQ').value;
	  var QuetionStr = document.getElementById('QuetionStr').value;
	  var QuestionStp = document.getElementById('QuestionStp').value;
	  var QuestionMainTrcNo = document.getElementById('QuestionMainTrcNo').value;
		if(QuestionModel == ''){
			alert('กรุณากรอกรายละเอียด กลุ่มคำถาม ');
			return false;
		 }else if(Question == ''){
			alert('กรุณากรอกรายละเอียดคำถาม ');
			return false;
		 }
		 browseCheck('messageQues','manageMainData.php?EditQuestionModel='+QuestionModel+'&EditQuestion='+
		 Question+'&EditRemark='+Remark+'&EditQuetionStr='+QuetionStr+'&EditQuestionStp='+QuestionStp+
		 '&QuestionMainTrcNo='+QuestionMainTrcNo,'');

		 document.getElementById('QuestionModel').value = ''; document.getElementById('Question').value = '';
		 document.getElementById('RemarkQ').value = '';  document.getElementById('QuetionStr').value = '';
		 document.getElementById('QuestionStp').value = ''; document.getElementById('QuestionMainTrcNo').value = '';
		 browseCheck('QuestionModelVal','manageMainData.php?RefreshSelect=Question','');
		 browseCheck('Re_list_Ques','manageMainData.php?RelistQues','');
		 setDisplayNone('manageAllQues');
	}else{
		return false;
	}
 }
 function searchQues(){
	var QuestionModel = document.getElementById('select_QuestionModel').value;
	var shrQuestion = document.getElementById('shrQuestion').value;
	//alert(QuestionModel);
	//alert(shrQuestion);
	browseCheck('Re_list_Ques','manageMainData.php?SearchQuestionModel='+QuestionModel+'&shrQuestion='+shrQuestion,'');
 }

 function btnAddCompetitor(){
	 setDisplayInline('manageAllCompetitor');
	 browseCheck('addEditCompetitor','manageMainData.php?CaseAdd=btnAddCompetitor','');
 }
 function AddCompetitor(){
	if(confirm('ต้องการบันทึกข้อมูล บริษัทคู่แข่ง ตรวจสอบรายละเอียดถูกต้องแล้ว !!')==true){
	  var CompetitorName = document.getElementById('CompetitorName').value;
	  var CompetitorNickName = document.getElementById('CompetitorNickName').value;
	  var CompetitorRemark = document.getElementById('CompetitorRemark').value;

	 if(CompetitorName == ''){
		alert('กรุณากรอกรายละเอียด บริษัท   ');
		return false;
	 }else if(CompetitorNickName == ''){
		alert('กรุณากรอกรายละเอียด ชื่อบริษัทคู่แข่ง  ');
		return false;
	 }
	 browseCheck('messageCompetitor','manageMainData.php?NewCompetitorName='+CompetitorName+'&NewCompetitorNickName='+
	 CompetitorNickName+'&NewCompetitorRemark='+CompetitorRemark,'');

	 document.getElementById('CompetitorName').value = ''; document.getElementById('CompetitorNickName').value = '';
	 document.getElementById('CompetitorRemark').value = '';

	browseCheck('CompetitorNameVal','manageMainData.php?RefreshSelect=Competitor','');
	browseCheck('Re_list_competi','manageMainData.php?RelistCompetitor','');
	}else{
		return false;
	}
 }
 function DelCompetitor(id){
	if(confirm('ต้องการลบข้อมูล บริษัทคู่แข่ง !!')==true){
	 browseCheck('messageQues','manageMainData.php?DelCompetitorId='+id,'');
	 browseCheck('CompetitorNameVal','manageMainData.php?RefreshSelect=Competitor','');
	 browseCheck('Re_list_competi','manageMainData.php?RelistCompetitor','');
	}else{  return false; }
 }
 function editCompetitor(id){
	setDisplayInline('manageAllCompetitor');
	browseCheck('addEditCompetitor','manageMainData.php?EditCompetitorId='+id,'');
 }
 function saveEditCompettor(){
	if(confirm('ต้องการแก้ไขข้อมูล บริษัทคู่แข่ง ตรวจสอบรายละเอียดถูกต้องแล้ว !!')==true){
	  var CompetitorName = document.getElementById('CompetitorName').value;
	  var CompetitorNickName = document.getElementById('CompetitorNickName').value;
	  var CompetitorRemark = document.getElementById('CompetitorRemark').value;
	  var CompetitorStatus = document.getElementById('CompetitorStatus').value;
	  var CompetitorID = document.getElementById('CompetitorID').value;

		if(CompetitorName == ''){
			alert('กรุณากรอกรายละเอียด บริษัท   ');
			return false;
		}else if(CompetitorNickName == ''){
			alert('กรุณากรอกรายละเอียด ชื่อบริษัทคู่แข่ง  ');
			return false;
		}
		browseCheck('messageCompetitor','manageMainData.php?EditCompetitorName='+CompetitorName+'&EditCompetitorNickName='+
		CompetitorNickName+'&EditCompetitorRemark='+CompetitorRemark+'&EditCompetitorStatus='+CompetitorStatus+
		'&CompetitorID='+CompetitorID,'');

		 document.getElementById('CompetitorName').value = ''; document.getElementById('CompetitorNickName').value = '';
		 document.getElementById('CompetitorRemark').value = '';  document.getElementById('CompetitorStatus').value = '';
		 document.getElementById('CompetitorID').value = '';
		 browseCheck('CompetitorNameVal','manageMainData.php?RefreshSelect=Competitor','');
		 browseCheck('Re_list_competi','manageMainData.php?RelistCompetitor','');
		 setDisplayNone('manageAllCompetitor');
	}else{
		return false;
	}
 }
 function ListAllCompetitor(){
	  browseCheck('Re_list_competi','manageMainData.php?RelistCompetitor','');
 }
 function searchCompetitor(){
	browseCheck('Re_list_competi','manageMainData.php?SelectCompetitorName='+$('select#select_CompetitorName').val()+
	'&shrCompetitor='+document.getElementById('shrCompetitor').value,'');
 }
function selectCompetitorNickName(val,id){
	browseCheck('ReCompetitorNickName'+id,'cusInterest.php?CompareCompetitorName='+val+'&CompareCompetitorID='+id,''); $("#compareInner"+id).html(val);
}
function get_compet_nickname(val,id){
	browseCheck('ReCompetitorNickName'+id,'cusInterest.php?CompareCompetitorName='+val+'&CompareCompetitorID='+id+'&result=save_track_record','');
	$("#compareInner"+id).html(val);
}
function selectCompetitorDesc(val,id){
var inne = $("#compareInner"+id).html();
$("#compareInner"+id).html(inne+" ("+val+")");
}
 function btnAddCustype(){
	$("#Cus_Type").val('');
	$("#CusType_Remark").val('');
	setDisplayInline('manageAllCusType');
	browseCheck('addEditCusType','manageMainData.php?CaseAdd=btnAddCustype','');
  }
  function btnAddCustype(){
	$("#Cus_Type").val('');
	$("#CusType_Remark").val('');
	setDisplayInline('manageAllCusType')
	browseCheck('addEditCusType','manageMainData.php?CaseAdd=btnAddCustype','');
  }
  function AddCusType(){
	if(confirm('ต้องการบันทึกข้อมูล ประเภทลูกค้า ตรวจสอบรายละเอียดถูกต้องแล้ว !!')==true){
	  var Cus_Type = document.getElementById('Cus_Type').value;
	  var CusType_Remark = document.getElementById('CusType_Remark').value;

	 if(Cus_Type == ''){
		alert('กรุณากรอกรายละเอียด ชื่อประเภทลูกค้า  !!');
		return false;
	 }
	 browseCheck('messageCompetitor','manageMainData.php?NewCusType='+Cus_Type+'&NewCusTypeRemark='+CusType_Remark,'');
	 document.getElementById('Cus_Type').value = '';
	 document.getElementById('CusType_Remark').value = '';
	 browseCheck('Re_list_CusType','manageMainData.php?RelistCusType','');
	}else{
		return false;
	}
  }

  function DelCusType(id){
	if(confirm('ต้องการลบข้อมูล ประเภทลูกค้า !!')==true){
		 browseCheck('messageCusType','manageMainData.php?DelCusTypeId='+id,'');
		 browseCheck('Re_list_CusType','manageMainData.php?RelistCusType','');
	}else{  return false; }
 }
 function editCusType(id){
	setDisplayInline('manageAllCusType');
	browseCheck('addEditCusType','manageMainData.php?EditCusTypeID='+id,'');
 }
  function saveEditCusType(){
	if(confirm('ต้องการแก้ไขข้อมูล ประเภทลูกค้า ตรวจสอบรายละเอียดถูกต้องแล้ว !!')==true){
		var Cus_Type = document.getElementById('Cus_Type').value;
		var CusType_Remark = document.getElementById('CusType_Remark').value;
		var CusTypeID = document.getElementById('CusTypeID').value;
		if(Cus_Type == ''){
			alert('กรุณากรอกรายละเอียด ชื่อประเภทลูกค้า  !!');
			return false;
		}
		browseCheck('messageCompetitor','manageMainData.php?EditCusType='+Cus_Type+'&EditCusTypeRemark='+CusType_Remark+
		'&EditCusTypeID='+CusTypeID,'');
		document.getElementById('Cus_Type').value = '';
		document.getElementById('CusType_Remark').value = '';
		document.getElementById('CusTypeID').value = '';
		browseCheck('Re_list_CusType','manageMainData.php?RelistCusType','');
		setDisplayNone('manageAllCusType');
	}else{
		return false;
	}
 }
 function ListAllCustype(){
	 browseCheck('Re_list_CusType','manageMainData.php?RelistCusType','');
 }
 function searchCustype(){
	 browseCheck('Re_list_CusType','manageMainData.php?shrCustype='+$("#shrCustype").val(),'');
 }
 function btnAddworkBranch(){
	 setDisplayInline('manageAllworkBranch');
	 browseCheck('addEditworkBranch','manageMainData.php?CaseAdd=btnAddworkBranch','');
 }
 function AddWorkBranch(){
	if(confirm('ต้องการเิพิ่มข้อมูล  ตรวจสอบข้อมูลแล้ว !!') == true){
		$(function(){
			/*
			var dataSet = jQuery("#myForm").find('input, textarea, select').serialize();
			*/
			var dataSet = "ParkingId="+$("#ParkingId").val()+"&code_name="+$("#code_name").val()+"&short_name="+$("#short_name").val()+"&name="+$("#name").val()+"&address="+$("#address").val()+"&inv_number="+$("#inv_number").val()+"&Remark="+$("#Remark").val();

			$.post("manageMainData.php",dataSet,function(data){
				btnAddworkBranch();
				browseCheck('reShort_name','manageMainData.php?RefreshSelect=WorkBranch','');
				browseCheck('Re_list_work','manageMainData.php?Refreshlist=WorkBranch','');
				alert("บันทึกข้อมูลสำเร็จ");
			});
		});
	}else{
		return false;
	}
}
function SaveEditParkBranch(){
	if(confirm('ต้องการแก้ไขข้อมูล  ตรวจสอบข้อมูลแล้ว !!') == true){
		$(function(){
			//var dataSet = jQuery("#myForm").find('input, textarea, select').serialize(); //naizan edit [15-10-2010]
			var dataSet = "ParkingId="+$("#ParkingId").val()+"&code_name="+$("#code_name").val()+"&short_name="+$("#short_name").val()+"&name="+$("#name").val()+"&address="+$("#address").val()+"&inv_number="+$("#inv_number").val()+"&Remark="+$("#Remark").val();
			$.post("manageMainData.php",dataSet,function(data){
				setDisplayNone('manageAllworkBranch');
				browseCheck('reShort_name','manageMainData.php?RefreshSelect=WorkBranch','');
				browseCheck('Re_list_work','manageMainData.php?Refreshlist=WorkBranch','');
				alert("บันทึกข้อมูลสำเร็จ");
			});
		});
	}else{
		return false;
	}
}

 function editBranch(id){
	setDisplayInline('manageAllworkBranch');
	browseCheck('addEditworkBranch','manageMainData.php?editBranchId='+id,'');
 }

 function DelBranch(id){
	browseCheck('Re_list_work','manageMainData.php?DelDataCase=WorkBranch&DELID='+id,'');
	browseCheck('reShort_name','manageMainData.php?RefreshSelect=WorkBranch','');
	browseCheck('Re_list_work','manageMainData.php?Refreshlist=WorkBranch','');
 }
 function ListAllworkBranch(){
	browseCheck('Re_list_work','manageMainData.php?Refreshlist=WorkBranch','');
 }
 function searchworkBranch(){
	browseCheck('Re_list_work','manageMainData.php?SearchString=WorkBranch&Select_shortName='+$("#Select_shortName").val()+
	'&shrParkBranch='+$("#shrParkBranch").val(),'');
 }


 /* function AddCompetitor(){
	if(confirm('ต้องการบันทึกข้อมูล สาขา ตรวจสอบรายละเอียดถูกต้องแล้ว !!')==true){

	 if($("#code_name").val() == ''){
		alert('กรุณากรอกรายละเอียด รหัสโค้ดเนม   ');
		return false;
	 }else if($("#short_name").val() == ''){
		alert('กรุณากรอกรายละเอียด ชื่อรหัสบริษัท   ');
		return false;
	 }
	 }else if($("#short_name").val() == ''){
		alert('กรุณากรอกรายละเอียด ชื่อรหัสบริษัท   ');
		return false;
	 }
	 browseCheck('messageCompetitor','manageMainData.php?NewCompetitorName='+CompetitorName+'&NewCompetitorNickName='+
	 CompetitorNickName+'&NewCompetitorRemark='+CompetitorRemark,'')

	 document.getElementById('CompetitorName').value = ''; document.getElementById('CompetitorNickName').value = '';
	 document.getElementById('CompetitorRemark').value = '';
	 browseCheck('Re_list_competi','manageMainData.php?RelistCompetitor','');
	}else{
		return false;
	}
 }
 function AddCompetitor(){
	if(confirm('ต้องการบันทึกข้อมูล สาขา ตรวจสอบรายละเอียดถูกต้องแล้ว !!')==true){

	 if($("#code_name").val() == ''){
		alert('กรุณากรอกรายละเอียด รหัสโค้ดเนม   ');
		return false;
	 }else if($("#short_name").val() == ''){
		alert('กรุณากรอกรายละเอียด ชื่อรหัสบริษัท   ');
		return false;
	 }
	 }else if($("#short_name").val() == ''){
		alert('กรุณากรอกรายละเอียด ชื่อรหัสบริษัท   ');
		return false;
	 }
	 browseCheck('messageCompetitor','manageMainData.php?NewCompetitorName='+CompetitorName+'&NewCompetitorNickName='+
	 CompetitorNickName+'&NewCompetitorRemark='+CompetitorRemark,'')

	 document.getElementById('CompetitorName').value = ''; document.getElementById('CompetitorNickName').value = '';
	 document.getElementById('CompetitorRemark').value = '';
	 browseCheck('Re_list_competi','manageMainData.php?RelistCompetitor','');
	}else{
		return false;
	}
 } */















	function Check_Object(){
	  if(window.XMLHttpRequest){
		  xmlHttp = new XMLHttpRequest();	//alert("bbbb");
	  }else{
	    alert("กรุณาใช้ Fire fox เท่านั้น");
	    return false;
	  }
  }










	function checkall_question(j,name){        // j=1=check j=0=none
	var name = document.getElementsByName(name);
	for(var i=0;i<name.length;i++){
			if(j==1){
				name[i].checked = true;
			}else if(j==0){
				name[i].checked = false;
			}
		}
	}


	function addQuestionList(i){

	   if(document.getElementById('question'+i).checked == true){
	      var QgroupName = document.getElementById('Qgroup_naire').value;
	      var num_check = document.getElementById('num_check').value;

	      addQgroupContent(QgroupName,num_check);
	   }
	}



	/* Car Booking content */
	function bookingCar(){
		var Cusdata = prompt("กรอกหมายเลขลูกค้า หรือ ชื่อลูกค้า? ", "")
		alert('browseCheck',Cusdata);
		//browseCheck('footer','customers.php?page=carBooking.php&Edit='+Cusdata,'')
	}
	function intTime(num){
		document.getElementById('branch_Data').style.display = 'inline';
		browseCheck('branch_Data_val','carBooking.php?intNum='+num,'')
	}
	function fncSubmitBooking(){
		if(confirm('ต้องการบันทึกข้อมูลการจองครั้งใหม่ ตรวจสอบรายละเอียดถูกต้องแล้ว !!')==true){
			if(document.getElementById('intSelection').value == ''){
				alert('กรุณาเลือกครั้งที่สนใจ !!');
				return false;
			}
			/*	else if(document.getElementById('B_Unit_Type').value == ''){
				alert('กรุณากรอกข้อมูลชนิดรถ');
				return false;
			}else if(document.getElementById('B_Unit_Model').value == ''){
				alert('กรุณากรอกข้อมูลแบบรถ');
				return false;
			}else if(document.getElementById('Eng_No_B').value == ''){
				alert('กรุณากรอกข้อมูลเลขเครื่อง');
				return false;
			}else if(document.getElementById('B_Unit_Color').value == ''){
				alert('กรุณากรอกข้อมูลสีถ');
				return false;
			} */

		}else{
			return false;
		}
	}
	function editBooking(num,book_no,cus_no,int_time,book_date,arr){
		changeTab('Int','2','3');
		var new_arr = arr.split("-");
	browseCheck('box_contentInt2','carBooking.php?editBookNum='+num+'&editbookNo='+book_no+'&editCusNo='+cus_no+'&editIntTime='+int_time+'&book_date='+book_date+'&book_type='+new_arr[0]+'&book_model='+new_arr[1]+'&book_code='+new_arr[2]+'&book_gname='+new_arr[3]+'&book_chass='+new_arr[4]+'&book_color='+new_arr[5]+'&model_CM='+new_arr[6],'');
	}

	function BookCarPatternModel(){
	  document.getElementById('BookModelCodeName_hidden').style.display = 'inline';
	  browseCheck('BookModelCodeName','carBooking.php?B_Unit_Model='+document.getElementById('B_Unit_Model').value+'&B_Unit_Type='+document.getElementById('B_Unit_Type').value,'');
	}
	function BookModelGname(){
	  document.getElementById('BookModelGName_hidden').style.display = 'inline';
	  browseCheck('BookModelGName','carBooking.php?BCarType='+document.getElementById('B_Unit_Type').value+'&BCarPattern='+document.getElementById('B_Unit_Model').value+'&BpatternCode='+document.getElementById('B_Model_CodeName').value,'');
	}
	function selectModelCM(){
	  document.getElementById('ModelCM_hidden').style.display = 'inline';
	  browseCheck('ModelCM','carBooking.php?Model_GName='+document.getElementById('B_Model_GName').value+'&CMType='+document.getElementById('B_Unit_Type').value+'&CMPattern='+document.getElementById('B_Unit_Model').value+'&CMCode='+document.getElementById('B_Model_CodeName').value,'');
	}
	function selectCassNo(){
	  document.getElementById('ChassNo_hidden').style.display = 'inline';
	  browseCheck('ChassNo','carBooking.php?ModelCM='+document.getElementById('Model_C_M').value,'');
	}
	function VehicleColor(){
	  document.getElementById('VehicleColor_hidden').style.display = 'inline';
	  browseCheck('VehicleColor','carBooking.php?VehicleColor='+document.getElementById('Chass_No_B').value,'');
	}
	function popUpWindow(URL, N, W, H, S,CH) { // name, width, height, scrollbars, chass no
		var ID = CH;
		var winleft	=	(screen.width - W) / 2;
		var winup	=	(screen.height - H) / 2;
		winProp		=	'width='+W+',height='+H+',left='+winleft+',top='+winup+',scrollbars='+S+',resizable' + ',status=yes';
		var type_val = 'S_unit_type'+ID;
		var type	= 	document.getElementById(type_val).value;
		var class_val = 'S_model_class'+ID;
		var class 	=	document.getElementById(class_val).value;
		var code_val =  'S_Model_CodeName'+ID;
		var code	= 	document.getElementById(code_val).value;
		var gname_val = 'S_Model_GName'+ID;
		var gname	=   document.getElementById(gname_val).value;
		var cm_val 	= 'S_Model_C_M'+ID;
		var cm		= 	document.getElementById(cm_val).value;
		var chass_val = 'S_Chass_No'+ID;
		var chass	=   document.getElementById(chass_val).value;
		var color_val = 'S_Color'+ID;
		var color 	=   document.getElementById(color_val).value;
	URL =  URL+'?popBooking&type='+type+'&class='+class+'&code='+code+'&gname='+gname+'&cm='+cm+'&chass='+chass+'&color='+color;
		Win			=	window.open(URL, N, winProp);
	if (parseInt(navigator.appVersion) >= 4) { Win.window.focus(); }
	}
	function popUpTextArea(chass) {
		if(confirm('คุณต้องการจองรถ !! ') == true){
			popUpWindow("cheackStock.php",'SH1',420,200,'no',chass);
		}
	}
	function directBook(){
		var info = document.getElementById('book_Cusinfo').value;
		//alert(info);
		info = info.split("_");
		var cusNo = info[0];
		var cusName = info[1];
		var page = "carBooking.php?";
		var caseName = "booking";
		var chkPage = "booking";
		var status = 'fromCheckStock';
		var sugg = '';

		window.opener.browseCheck('main','customers.php?page='+page+'&caseName=bookfromChkStock&case=bookfromChkStock&CusInfo='+info+'&bookstatus='+status+'&s_type='+document.getElementById('s_type').value+'&s_class='+document.getElementById('s_class').value+'&s_code='+document.getElementById('s_code').value+'&s_gname='+document.getElementById('s_gname').value+'&s_cm='+document.getElementById('s_cm').value+'&s_chass='+document.getElementById('s_chass').value+'&s_color='+document.getElementById('s_color').value+'&Edit='+cusNo,'');


		//browseCheck('footer','customers.php&Edit='+cusNo,'');
		//alert('66666');
    	//searchcus(page,caseName,chkPage,'',cusNo)
		//alert('5555');
		window.close();

	}
	function QueBooklist(book_id,CusNo,Int_Num,B_Unit_Type){
		var dataSet={ Edit_book_id : book_id, Edit_book_CusNo : CusNo, Edit_book_IntNum : Int_Num ,B_Unit_Type : B_Unit_Type, intNo : Int_Num, PreintTime : '', bookIntNum2 : Int_Num};
		$.get('carAcc.php',dataSet, function(data) {
			$("#edit_booking_contain").html(data);
			$("#btnSubmitEditContain").show('fast');
		});
	}
	function SubmitEditBook(check_value){
		if(confirm('ต้องการแก้ไขข้อมูล รายการการจองรถ  ตรวจสอบถูกต้องแล้ว!!') == true){
			$(function(){
				var dataSet = jQuery("#fncEditBookForm").find('input, textarea, select').serialize();
				$.post("Editbooking.php",dataSet,function(data){
					alert(data);
					$("#containHead").show('fast', function(){
						$("#edit_booking_contain").html('');
						//$("#btnSubmitEditContain").hide('fast');
						var param='';
						if(check_value=='saleRemark'){param='&value=saleRemark'}
						browseCheck('main','Editbooking.php?case_name=today'+param,'');
					});
				});
			});
		}else{
			return false;
		}
	}
	function toggleDiv(div){
		$('#changeCar').toggle(function() {
			setDisplayNone('BookNewCar');
		}, function() {
			setDisplayInline('BookNewCar');
		});
	}
	function returnCar(){
		if(confirm('ต้องการคืนรถให้กับสต็อค') == true){
			// input value
			$('#Eng_No_B').val('');
			$('#Chassi_No_B').val('');
			$('#web_order').val('');

			$('#Cheak_ChangeCar').val('Changing_vehicle_information');

			// inner html value
			$('#nerEng_No_B').html('');
			$('#nerChassi_No_B').html('');
			$('#nerWeb_order').html('');

			alert('เรียบร้อยครับ การคืนรถให้กับสต็อคจะสมบูรณ์เมื่อบันทึกการแก้ไขเพื่อยืนยันเงื่อนไข ++');
		}else{
			return false;
		}
	}
function changeCar(){
	var Unit_Model = $("#B_Unit_Model").val().split("<BR>");
	var dataSet = { B_Unit_Type: $("#B_Unit_Type").val(), B_Model_Class : $("#B_Model_Class").val(), B_Unit_Model : Unit_Model[0],
	B_Model_GName : $("#B_Model_GName").val(), Eng_No_B : $("#Eng_No_B").val(), Chassi_No_B : $("#Chassi_No_B").val(), B_Unit_Color :
	$("#B_Unit_Color").val(),Price_E : $("#Price_E").val(),web_order : $("#web_order").val() }
	/* $.get('Editbooking.php',dataSet,function(data){
		if(data == "EMTRY"){
			var cond = { condition : 'emtry_car'};
			$.extend(true, dataSet, cond);							// -- Merge two objects
			jPost('#car-forchange','Editbooking.php',dataSet,'Edit-Book-Emtry');
			dialogConfirm('emtry_car');										// ---- call dialog dialogConfirm
			$("#dialog-confirm").show('slide');
		}else{ jPost('#car-forchange','Editbooking.php',dataSet,'Edit-Book-have');}
	}); */
	var cond = { condition : 'emtry_car'}; $.extend(true, dataSet, cond);							// -- Merge two objects
	//jPost('#car-forchange','Editbooking.php',dataSet,'Edit-Book-Emtry');
	// EDIT WITH IC
	$.post('Editbooking.php',dataSet,function(data){
		//dialogForm('emtry_car','');  		 // ---- call dialog
		$("#car-forchange-overlay").show().addClass('ui-widget-overlay');
		$("#car-forchange-overlay").css({width:"100%" ,height:$(document).height(),opacity:.3});
		$("#car-forchange").show('clip').html(data); $('.car-forchange').show('clip');
	});
}
	function dialogConfirm(condition){
		$(function() {
			$("#dialog-confirm").dialog("destroy");
			$("#dialog-confirm").dialog({
				resizable: false,
				height:140,
				modal: true,
				buttons: {
					'ตกลง': function() {
						$(this).dialog('close');

						dialogForm(condition,'');  		 // ---- call dialog
						$("#car-forchange").show('clip');
					},
					'ยกเลิก': function() {
						$(this).dialog('close');
					},
					'สั่งจองรถในสต็อค': function() {
						if(confirm('ไม่พบข้อมูลรถภายในสต็อค ยืนยันการสั่งจองรถภายในสต็อค') == true){
							$("#dialog-confirm").dialog("destroy");
							var Unit_Model = $("#B_Unit_Model").val().split("<BR>");
							var dataSet = { B_Unit_Type: $("#B_Unit_Type").val(), B_Model_Class : $("#B_Model_Class").val(), B_Unit_Model : Unit_Model[0],
							B_Model_GName : $("#B_Model_GName").val(), Eng_No_B : $("#Eng_No_B").val(), Chassi_No_B : $("#Chassi_No_B").val(), B_Unit_Color :
							$("#B_Unit_Color").val(),Price_E : $("#Price_E").val()}
							jPost('#car-forchange','Editbooking.php',dataSet,'Edit-Book-have');
							
						}else{
							return false; 
						}
					}
				}
			});
		});
	}
	function dialogForm(condition,case_name){
		$(function() {
			//$("#car-forchange").html('');
			// EDIT WITH IC
			switch(case_name){
				case 'ตกลง': if(confirm('ตรวจสอบข้อมูลถูกต้องแล้ว') == true){ checkValue(condition);}else{return false;}	break;
				case 'ยกเลิก': $('#car-forchange').hide().html(''); $('#car-forchange-overlay').hide(); break;
			}

			/* $("#car-forchange").dialog("destroy");
			$("#car-forchange").dialog({
				position: 'center',
				height: 400,
				width: 450,
				modal: true,
				buttons: {
					'ตกลง': function() {if(confirm('ตรวจสอบข้อมูลถูกต้องแล้ว') == true){ checkValue(condition);}else{return false;}},
					'ยกเลิก': function() {$(this).dialog('close');}
				},
				close: function() {$(this).dialog('close');}
			}); */
		});
	}
	function UnitType(val,table_name){
		var dataSet = {table_name:table_name,AUnit_Type:val};
		$.get('Editbooking.php',dataSet, function(data) {
			$('#IntCar_pattern').html(data);
		});
	}
	function model_class(val,table_name){  // ModelClass
		var dataSet = {table_name:table_name,BUnit_Type:$('#Unit_Type').val(),BModelClass:val};
		$.get('Editbooking.php',dataSet, function(data) {$('#CarModelCodeName').html(data);});
	}
	function model_codename(val,table_name){  //ModelCodeName
		var dataSet = {table_name:table_name,Unit_Type:$('#Unit_Type').val(),ModelClass:$('#ModelClass').val(),ModelCodeName:val};
		$.get('Editbooking.php',dataSet, function(data) {$('#CarModelGName').html(data);});
	}
	function model_gname(val,table_name){	// ModelGName
		var dataSet = { table_name:table_name,CUnit_Type:$('#Unit_Type').val(),CModelClass:$('#ModelClass').val(),CModelCodeName:$('#ModelCodeName').val(),CModelGName:val};
		var dataSet2 = {table_name:table_name,DUnit_Type:$('#Unit_Type').val(),DModelClass:$('#ModelClass').val(),DModelCodeName:$('#ModelCodeName').val(),DModelGName:val};
		$.get('Editbooking.php',dataSet, function(data) {	$('#CRPAndAirIncVatDIv').html(data);});
		$.get('Editbooking.php',dataSet2, function(data) {	$('#CarModelCM').html(data); });
	}
	function mv_engno(val){			// MVEngNo
		var dataSet = {Unit_Type:$('#Unit_Type').val(),ModelClass:$('#ModelClass').val(),ModelCodeName:$('#ModelCodeName').val(),
		ModelGName:$('#ModelGName').val(),MVEngNo:val};
		$.get('Editbooking.php',dataSet, function(data) {$('#ChassiNoB').html(data);});

		var cond = { condition : 'CarCard'};
		$.extend(true, dataSet, cond);
		$.get('Editbooking.php',dataSet, function(data) {$('#CarCardShow').html(data);});
		$('#CarCardShow').show('fast');
	}
	function checkValue(condition){
		if($('#Unit_Type').val() == ''){
			alert('กรุณาเลือก ชนิดรุ่น ');
			return false;
		}else if( $('#ModelClass').val() == ''){
			alert('กรุณาเลือก รุ่นปี  ');
			return false;
		}else if( $('#ModelCodeName').val() == ''){
			alert('กรุณาเลือก รหัสแบบรถ   ');
			return false;
		}else if( $('#ModelGName').val() == ''){
			alert('กรุณาเลือก รหัสแบบรถทั่วไป    ');
			return false;
		}else if( $('#PriceIncVat').val() == ''){
			alert('กรุณาเลือก ราคา');
			return false;
		}else if(condition != 'emtry_car'){     // -- check condition before
			if( $('#MVEngNo').val() == ''){
				alert('กรุณาเลือก เลขเครื่อง    ');
				return false;
			}else if( $('#ChassiNoB').html() == ''){
				alert('กรุณาเลือก เลขแชสสี     ');
				return false;
			}
		}else if( $('#CarModelColor').val() == ''){
			alert('กรุณาเลือก สี      ');
			return false;
		}
		Replace(condition);
		$('#car-forchange').hide().html(''); $('#car-forchange-overlay').hide(); // EDIT WITH IC
	}

	function Replace(condition){
		var price = formatNumber($('#PriceIncVat').val(),2,1);

		// --- input value
		$('#B_Unit_Type').val($('#Unit_Type').val());
		$('#B_Model_Class').val($('#ModelClass').val());
		$('#B_Unit_Model').val($('#ModelCodeName').val());
		$('#B_Model_GName').val($('#ModelGName').val());
		//$('#Eng_No_B').val($('#MVEngNo').val());
		$('#Eng_No_B').val('');
		//$('#Chassi_No_B').val($('#ChassiNoB').html());
		$('#Chassi_No_B').val('');
		$('#web_order').val('');

		$('#B_Unit_Color').val($('#CarModelColor').val());
		$('#Price_E').val($('#PriceIncVat').val());

		// --- inner html
		$('#nerB_Unit_Type').html($('#Unit_Type').val());
		$('#nerB_Model_Class').html($('#ModelClass').val());
		$('#nerB_Unit_Model').html($('#ModelCodeName').val());
		$('#nerB_Model_GName').html($('#ModelGName').val());

		//$('#nerEng_No_B').html($('#MVEngNo').val());
		$('#nerEng_No_B').html('');
		//$('#nerChassi_No_B').html($('#ChassiNoB').html());
		$('#nerChassi_No_B').html('');
		$('#nerWeb_order').html('');

		$('#nerB_Unit_Color').html($('#CarModelColor').val());
		$('#nerPrice_E').html(price);


		// --- replace in car info
		$('#Cheak_ChangeCar').val('Changing_vehicle_information')
		if(condition != 'emtry_car'){
			$('#CarCardInner').html($('#CarCardVal').html()); 		// set value
			$('#CarCard').val($('#CarCardVal').html()); 				// set value
			$('#CarCard_hide').show('fast');

			$('#DLR_IDInner').html($('#DLR_IDVal').html()); 		// set value
			$('#DLR_ID').val($('#DLR_IDVal').html()); 			// set value
			$('#DLR_ID_hide').show('fast');
		}

		// --- replace in condition
		$('#CRPPrice').val(price);


		//--- reset div (target) of accessories  -- เคลียร์ของแต่ง
		var dataSet = { ReAccessory: 'ReAccessory', ReUnitType : $('#Unit_Type').val() };
		jGet('#Reacc_use_ul','Editbooking.php',dataSet,'ReAccessory');
		$('#All_AccUse_ForGift').html('');
		$('#All_AccUse_ForBuy').html('');
		$('#All_AccUse_ForSetPrice').html('');
		$('#All_AccUse_contain').hide('fast');
		$('#AccessoryCost').val('');   // ราคาอุปกรณ์ตกแต่ง(รวม) --> รายละเอียดเงื่อนไข



		// -- reset div (target) of campaign -- clear campaign
		var dataSet = { ReCampaign: 'ReCampaign', ReCampaignUType : $('#Unit_Type').val() };
		jGet('#CusInt_TisCampaign','Editbooking.php',dataSet,'ReCampaign');
		$('#CampaignCost').val('');   // ราคาแคมเปญ(รวม) --> รายละเอียดเงื่อนไข


		// --- re calculated
		calTotal_Budget();		    // คำนวนงบประมาณทั้งหมดใหม่
	    calIntPerMont(); 		    // คำนวนค่างวดใหม่    
		alert('แทนที่ข้อมูลเรียบร้อยแล้ว !!');
		$('#Discount').keyup();	//คำนวณส่วนลด และราคาหักส่วนลด

	}
	function showChangeArea(){
		dialogForm(); 		// call dialog
		$("#car-for-change").show('clip');
	}
	function jPost(jShow,target,dataSet,condition){
		$.post(target,dataSet,function(data){
			if(condition == 'Edit-Book-have'){
				$(jShow).html(data);
				showChangeArea();
			}else{
				$(jShow).html(data);
			}
		});
	}
	function jGet(jShow,target,dataSet,condition){
		$.get(target,dataSet, function(data) {
			if(condition == 'ReAccessory'){
				$(jShow).html(data);
				jqueryUI('#sunmenu_ass');
			}else if(condition == 'ReCampaign'){
				$(jShow).html(data);
				jqueryUI('#sunmenu_campaign');
			}else{
				$(jShow).html(data);
			}
		});
	}
	function check_sell_type(chk_case){
		var Sell_type = $("input[name='Sell_type_con']:checked").val();
		if(Sell_type == 'สด'){
			$('#Down').attr("readonly", true);
			$('#Int').attr("readonly", true);
			$('#Finance').attr("readonly", true);
			$('#Time').attr("readonly", true);
		}else if(Sell_type == 'ผ่อน'){
			$('#Down').removeAttr("readonly");
			$('#Int').removeAttr("readonly");
			$('#Finance').removeAttr("readonly");
			$('#Time').removeAttr("readonly");
		}
		//Zan@2011-11-17 Sale อ่านอย่างเดียว
		if(chk_case=='saleRemark'){
			$('#Down').attr("readonly", true);
			$('#Int').attr("readonly", true);
			$('#Finance').attr("readonly", true);
			$('#Time').attr("readonly", true);
		}
	}


	/* stock content */
	function changeParking(chassNo,cheak_value,Parking){
		if(cheak_value == 'notequal'){
			alert('ไม่่สามารถย้ายสถานที่จอดรถได้เนื่องจากปัจจุบันรถคันนี้จอดที่    '+ Parking);
			return false;
		}
		document.getElementById('changePark').style.display='block';
		document.getElementById('sugnsC').style.display='inline';
		browseCheck('sugnsC','cheackStock.php?Vehicle='+chassNo,'');
	}
	function ReceiveCar(MV_Chass_No,HQ){
		if (confirm('            ++ คุณต้องการรับรถ  ++'  )) {
			if(HQ == 'HQ'){ cghangeParkingReAll(MV_Chass_No);}else{ changeParkingRe(MV_Chass_No,'');}
		}else{ return false;}
	}
	function cghangeParkingReAll(MV_Chass_No){
		$(function() {
			$("#dialog-form").dialog("destroy");
			$("#dialog-form").dialog({
				position: 'center',
				height: 150,
				width: 300,
				modal: true,
				buttons: {
					'ตกลง': function() {
						changeParkingRe(MV_Chass_No,'$("#car_parking3").val()');
					},
					'ยกเลิก': function() {$(this).dialog('close');}
				},
				close: function() {$(this).dialog('close');}
			});
		});
	}
	function changeParkingRe(chassNo,Parking){
		var dataSet={ ParkReceiveName: 1, chass_no: chassNo, Parking:Parking };
		$.get('cheackStock.php',dataSet, function(data) {
			alert(data);
			browseCheck('main','cheackStock.php?page=changeParking?','')
		});
	}
	function confirmChkPark(ChassNo,Eng_No_Re){
		if(confirm('คุณต้องการแก้ไขสถานที่จอดรถ   ตรวจสอบข้อมูลถูกต้องแล้ว!! ') == true){
			if(document.getElementById('car_parking3').value == ''){
				alert('กรุณาเลือกสถานที่จอด !!');
				return false;
			}else{
				var park = document.getElementById('car_parking3').value;
				document.getElementById('changePark').style.display='none';
				//var Re_date = document.getElementById('Re_date').value;
				//var dataSet={ ParkName: park, chass_no: ChassNo, Eng_No_Re: Eng_No_Re, Re_date: Re_date };

				var dataSet={ ParkName: park, chass_no: ChassNo, Eng_No_Re: Eng_No_Re,CaseName:'changeParking'};
				$.get('cheackStock.php',dataSet, function(data) {alert(data);});

				//browseCheck('changePark','cheackStock.php?ParkName='+park+'&chass_no='+ChassNo+'&Eng_No_Re='+Eng_No_Re,'');

				browseCheck('main','cheackStock.php?page=changeParking?','');
			}
		}
	}
	function EditMainModelCode(Tranc_No){
		var style = document.getElementById('footer').style.display;
		if(style == 'none' || style == ''){
			code = document.getElementById('footer').style.display = 'inline';
		}
		hideHeadContent('hide','containHead');
		browseCheck('footer','cheackStock.php?ModelCodeTrancNo='+Tranc_No,'');
	}

	function SendSaveModel(){
		if(confirm('ต้องการแก้ไขข้อมูล  Model Code ตรวจสอบข้อมูลแล้ว !!') == true){
			$(function(){
				var dataSet = jQuery("#myForm").find('input, textarea, select').serialize();
				$.post("cheackStock.php",dataSet,function(data){
					$("#footer").hide();
					alert('แก้ไขข้อมูลเรียบร้อยแล้ว.')
					$("#list_stock").html('');
					browseCheck('list_stock','cheackStock.php?RefreshModellist=RefreshModellist','');
					$("#containHead").show('slow');

				});
			});
		}else{
			return false;
		}
	}
function delModel(id,Model_C_M,case_name){
	if(case_name=='del_all'){
		data_set = $("#listKeyup").find('input:checkbox').serialize();
	}else{
		$('#'+id).attr('checked','checked'); $('#'+id).addClass('hilight_rows');data_set = 'model_id[]='+id;
	}
	if(confirm('คุณต้องการลบข้อมูล modelcode  <รหัสรุ่นรถ '+Model_C_M+'>   ตรวจสอบข้อมูลแล้ว !!') == true){
		$.get('cheackStock.php',data_set,function(data){ alert(data); browseCheck('list_stock','cheackStock.php?RefreshModellist=RefreshModellist','');});
		//browseCheck('footer','cheackStock.php?DelModelid='+id+'&model_id='+$('input[name=model_id\\[\\]]:checked').val(),'');
		//browseCheck('list_stock','cheackStock.php?RefreshModellist=RefreshModellist','');
	}
}
function searchStockModel(chk){
	var code = null;var style =document.getElementById('Car_code_hidde_select').style.display;
	if(style == 'none' || style == ''){code = document.getElementById('car_code').value; }else{code = document.getElementById('car_code_name').value;}
	browseCheck('list_stock','cheackStock.php?Codetype='+document.getElementById('car_type').value+'&Codepattern='+document.getElementById('car_pattern').value+'&codeModel='+code+'&ModelchkPage='+chk,'');
}
function selectCodeName(class,chk){
	browseCheck('Car_codeBook','cheackStock.php?ThisNameModel=car_pattern&nameSelect=car_codeName&carTypeModel='+$("#car_type").val()+'&carPatternModel='+class,'');
}
function selectCodeNameVehicle(class,chk){
	browseCheck('code_hidde_select','cheackStock.php?VehicleThisNameModel=car_pattern&VehiclenameSelect=car_codeName&carTypeModel='+$("#car_type").val()+'&carPatternModel='+class,'');
}












	/* update database field any table */
	function updateStatus(){
		var status = document.getElementById('status_val').value;
		browseCheck('updateSuccess','updateStatus.php?status_val='+status,'');
	}
	function updateCodeRefer(){
		browseCheck('updateSuccess','updateStatus.php?updateCodeRefer=','');
	}
	function updateParking(){
		browseCheck('updateSuccess','updateStatus.php?updateParking=','');
	}









	/* Follow history content */

	function QListSell(val){
	   document.getElementById('QlistSell').style.display = 'inline';
	  // browseCheck('QlistSell','questionnaire.php?QListSell='+val,'')
	}
	function TrackingMore(){
	   var com = document.getElementById('chkvalue').value;
	   var number = parseFloat(com)+1;
	   var area = 'boxtwo_trackCon';
	   var name = 'tracking'+number;

	   node = document.getElementById(area);
	   if (node) {
			   var myElement = document.createElement("DIV");

			   myElement.setAttribute("id",name);
			   node.appendChild(myElement);
			   myElement.innerHTML = 'document.getElementByIdMoretrackCon';
			   document.getElementById('chkvalue').value = number;

	   }

    browseCheck(name,'tracking.php?MoretrackCon='+number,'');
	}
function Contact_Prepare_detail(cusNo,intNum,Con_prepare_Num){
	document.getElementById('sshowConPre').style.display='block';
	browseCheck('sugnsConPre','tracking.php?ViewConpreDetail='+cusNo+'&ConPreintNum='+intNum+'&Con_prepare_Num='+Con_prepare_Num,'');
}
//เปิดหน้าเสนอการติดตาม
function openTabs(display,cus_no,intNo,intTime){
	document.getElementById(display).style.display = '';selectedTab(2);document.getElementById('cfive').style.display = '';
	$('#box_two').html(ajax_loader);//naizan 2011-01-11
	$.get('ic_newprepare.php',{prepare_cus:cus_no,int_number:intNo,pre_inttime:intTime,case_name:'new_prepare'},function(data){
	    $('#box_two').html(data);
		$('#tabsa-3 input,textarea').attr('readonly',true);$('#tabsa-3 select,checkbox').attr('disabled','disabled');
		if($('#tabsa-3 .question_list')){$('#tabsa-3 .question_list').attr('disabled','disabled');}
		if($('#tabsa-3 .hasDatepicker')){$('#tabsa-3 .hasDatepicker').attr('disabled','disabled');}
		element = $('.active_cond');
		id = element.attr('id').split('condition_temp')[1];
		make_condition(id.split('_')[0],element.val(),id,'new_prepare',document.getElementById(element.attr('id')));
		$('#c_save_plan').hide();
		$('#c_action').fadeIn("slow");
		$('#int_num').val(intNo);
		$('#edit_id').val(cus_no);
	});
	//browseCheck('box_five','tracking.php?report_history='+cus_no+'&int_number='+intNo+'&check=ProposedTrack','');
	browseCheck('box_five','tracking.php?PrepareHis='+cus_no+'&intNo='+intNo+'&check=ProposedTrack','');
}

function selectedTab(num){$('#tabsPrepare').tabs('option', 'selected', num);}
function openSaveTrack(display,cus_no,intNo,intTime){
	document.getElementById(display).style.display = '';selectedTab(3);
	browseCheck('box_three','tracking.php?PrepareHis='+cus_no+'&intNo='+intNo+'&check=TrackRecord&TrackintTime='+intTime,'');
}
function SaveTracking(cus_no,intNo,intTime,ConPrepareNum){
	document.getElementById('cfour').style.display = ''; selectedTab(4);
	// browseCheck('box_four','tracking.php?SaveTrack='+cus_no+'&SaveIntNo='+intNo+'&TrackintTime='+intTime+'&ConPrepareNum='+ConPrepareNum,'');
	// if($('#Con_prepare_Num').val()){
		// alert('condition_detail');
	// }else{
		// alert('no condition_detail');
	// }
	// $.ajax({
		// url: 'tracking.php?SaveTrack='+cus_no+'&SaveIntNo='+intNo+'&TrackintTime='+intTime+'&ConPrepareNum='+ConPrepareNum,
		// cache: false,
		// success: function(data){
		// $('#box_four').html(data);
		// call_track_condition(cus_no,intTime,$('#Con_prepare_Num').val());
		// }
	// });

	$.get('tracking.php',{SaveTrack:cus_no,SaveIntNo:intNo,TrackintTime:intTime,ConPrepareNum:ConPrepareNum},function(data){
		 $('#box_four').html(data); call_track_condition(cus_no,intNo,$('#Con_prepare_Num').val());
	});
}

function openCheckResults(display,cus_no,int_no,int_time,con_pre){
	var data_set = {CheckPrepare:cus_no,CheckIntNo:int_no,CheckPreintTime:int_time,CheckConPrepare:con_pre}
	$.get('tracking.php',data_set,function(data){
		var cond = {check_prepare_cond:'prepare_condition'};
		$('#box_six').html(data); $('#'+display).show(); selectedTab(2);
		$.get('tracking.php',$.extend(true,data_set,cond),function(data){
			$('#tabs-6').html(data);$('#cfive').show();
			$('#cusint_product_content input,textarea,select').attr({disabled:'disabled',readonly:'readonly'});
			element = $('.active_cond'); id = element.attr('id').split('condition_temp')[1];
			make_condition(id.split('_')[0],element.val(),id,'new_prepare',document.getElementById(element.attr('id')));
			 $('.i_del,.i_add_m,.i_del,.iHelp').addClass('hidden');
		});
	});
}
function open_prepare_cond(display,cus_no,int_no,int_time,con_pre){
	$.get('tracking.php',{CheckPrepare:cus_no,CheckIntNo:int_no,CheckPreintTime:int_time,CheckConPrepare:con_pre},function(data){
		$('#box_six').html(data); $('#cusint_num_ref').val(int_no)
		$.get('tracking.php',{check_prepare_cond:'prepare_condition',CheckPrepare:cus_no,CheckIntNo:int_no,CheckPreintTime:int_time,CheckConPrepare:con_pre},
		function(data){
			$('#tabs-6').html(data);$('#cfive').show();
			$('#cusint_product_content input,textarea,select').attr({disabled:'disabled',readonly:'readonly'});
			element = $('.active_cond'); id = element.attr('id').split('condition_temp')[1];
			make_condition(id.split('_')[0],element.val(),id,'new_prepare',document.getElementById(element.attr('id')));
			$('.i_del,.i_add_m,.i_del,.iHelp').addClass('hidden');
		});
	});
}
function check_result(cus_number,case_name,arr){ var inner = '';
	//-- naizan add arr 2011-02-12
	var param='';
	if(arr){
		param = '?sale_id_card='+arr.sale_id_card;
	}

	$.get('tracking.php'+param,{check_result:case_name,cus_number:cus_number},function(data){
		if(case_name=='repare'){ preload('#Repare_CusInt_his');$('#Repare_CusInt_his').html(data);}else{ preload('#Re_CusInt_his');$('#Re_CusInt_his').html(data);}
	});
}
function openCheckResultsCusin(display,cus_no,intNo,intTime,ConPre,Con_prepare_Num){
	document.getElementById(display).style.display = '';selectedTab(5);document.getElementById('cfourd').style.display = '';
	browseCheck('box_five','tracking.php?fgdVew=1&ContactPrepare='+cus_no+'&intNo='+intNo+'&PreintTime='+intTime+'&Con_Time='+Con_prepare_Num+'&ConPre='+ConPre,'');
}
function openCheckResultsRepare(display,cus_no,intNo,intTime,ConPre,arr){ //naizan add arr 2011-02-12
	$('#'+display).show();
	$.get('tracking.php',{CheckRepare:cus_no,CheckReIntNo:intNo,CheckReintTime:intTime,CheckRePrepare:ConPre},
	function(data){$('#box_four').html(data); selectedTab(3);});
}
function activeElement(div,box){$(function(){$(div).accordion('activate' , box, 'animated', "bounceslide");  });	}
function jTab(){$(function(){$('#tabs3').tabs({ selected: 1})});}
function NewCusCompare(){ $('#CusInt_CusCompare').html(''); $('#CusInt_CusCompare').html('$CusCompare_content');}
function fncSubmitContact(){alert('กรุณาให้เจ้าตัวทำรายการเองนะครับ.......');return false;}
function copyInt(cusno,intnum,intTime){
	if(confirm('ต้องการคัดลอกรายละเอียดเงื่อนไข ตรวจสอบข้อมูลแล้ว !!') == true){
		var Int_Product_TrcNo = document.getElementById('Int_Product_TrcNo').value;
		var Int_Con_TrcNo = document.getElementById('Int_Con_TrcNo').value;
		var RecComd = document.getElementsByName('RecComd[]');
		var RecComend = '';
		for (var i=0;i<RecComd.length;i++) {
			if(RecComd[i].checked == true) {
				RecComend += RecComd[i].value+"_";
			}
		}
		var Acc = document.getElementsByName('AccUse[]');
		var AccUse = '';
		for (var i=0;i<Acc.length;i++) {
			if(Acc[i].checked == true) {
				AccUse += Acc[i].value+"_";
			}
		}
		var OtherCost = document.getElementsByName('OtherCost[]');
		var OtherCostVal = '';
		for (var i=0;i<OtherCost.length;i++) {
			if(OtherCost[i].checked == true) {
				OtherCostVal += OtherCost[i].value+"_";
			}
		}
		var Campaign = document.getElementsByName('Campaign[]');
		var CampaignVal = '';
		for(var i=0;i<Campaign.length;i++) {
			if(Campaign[i].checked == true){
				CampaignVal += Campaign[i].value+"_";
			}
		}

		var Question = document.getElementsByName('Question[]');
		var QuestionModelVal = '';
		for(var i=0;i<Question.length;i++) {
			if(Question[i].checked == true){
				QuestionModelVal += Question[i].value+"_";
			}
		}

		var NegIn = document.getElementsByName('NegIn[]');
		var Neg = '';
		for (var i=0;i<NegIn.length;i++) {
			if(NegIn[i].checked == true) {
				Neg += NegIn[i].value+"_";
			}
		}

		/* browseCheck('box_two','tracking.php?Recommended='+RecComend+'&Accessory='+AccUse+'&OtherCost='
		+OtherCostVal+'&Campaign='+CampaignVal+'&Question='+QuestionModelVal+'&Neg='+Neg+'&Int_Product_TrcNo='+
		Int_Product_TrcNo+'&Int_Con_TrcNo='+Int_Con_TrcNo+'&case_name=CopyCondition&copyCusNo='+cusno+
		'&copyIntnum='+intnum+'&v='+intTime,''); */

		var dataSet={ Recommended: RecComend, Accessory:AccUse,OtherCost:OtherCostVal,Campaign:CampaignVal,Question:QuestionModelVal,Neg:Neg,
		Int_Product_TrcNo:Int_Product_TrcNo,Int_Con_TrcNo:Int_Con_TrcNo,case_name:'CopyCondition',copyCusNo:cusno,copyIntnum:intnum,v:intTime };
		$.get('tracking.php',dataSet, function(data) {
			$('#box_two').html(data);
			setDisplayInline('dialog');
			showswap();
			dialogUI();
		});


	}else{
		return false;
	}
}
function createNew(cus_no,intNumRef){
	if(confirm('ต้องการสร้างรายละเอียดเงื่อนไขใหม่  ตรวจสอบข้อมูลแล้ว!!') == true){
		var NegIn = document.getElementsByName('NegIn[]');var Neg = '';
		for(var i=0;i<NegIn.length;i++) {if(NegIn[i].checked == true) {Neg += NegIn[i].value+"_";}}
		var dataSet={ Edit: cus_no, case_name:'contactPrepare',intNumRef:intNumRef, NegCreNew:Neg};
		$.get('cusInterest.php',dataSet, function(data) {$('#box_two').html(data);showswap();});
	}
}
function showswap(){
	$("#swap_menu").show('fast',function() {
		$("#swap_menu").addClass('tabMenu'); $("#selectItemFor").show('fast');
		setDisplayNone('CusInt_AccUse_contain');		// tab acc
		setDisplayNone('TisCampaign_contain');	// tab campaign
	});
}
	function fncSubmitTracking(){
		if(confirm('ต้องการบันทึกข้อมูลเพื่อนำเสนอการติดตาม  ตรวจสอบข้อมูลถูกต้องแล้ว!!') == true){
			/* var name = document.getElementsByName('select_acc_use[]');
			for(var i=0;i < name.length;i++){
				if(i == 0){ i = '';}
		if(document.getElementById('Branch_Data'+i).value != '' && document.getElementById('select_acc_name'+i).value != '' ){
					var j = i+1;
					if(document.getElementById('AssAmount'+i).value == ''){
						alert('กรุณากรอกจำนวน อุปกรณ์ตกแต่งในช่องข้อมูลที่'+j+'ด้วยครับ !!');
						return false;
					}
				}
			} */
			if(document.getElementById('IntCarType').value == ''){
			   alert('กรุณาเลือก ชนิดรุ่น ของรถที่ลูกค้าสนใจ !! ');
			   return false;
			}else if(document.getElementById('IntCarPattern').value == ''){
			   alert('กรุณาเลือก แบบรถ ที่ลูกค้าสนใจ !! ');
			   return false;
			}else if(document.getElementById('IntModelCodeName').value == ''){
			   alert('กรุณาเลือก รหัสแบบรถ ที่ลูกค้าสนใจ !! ');
			   return false;
			}else if(document.getElementById('IntModelGName').value == ''){
			   alert('กรุณาเลือก รหัสแบบรถทั่วไป !! ');
			   return false;
			}else if(document.getElementById('CarModelColor').value == ''){
			   alert('กรุณาเลือก สี รถที่ลูกค้าสนใจ !! ');
			   return false;
			}else if(document.getElementById('CRPPrice').value == ''){
			   alert('กรุณาตรวจสอบข้อมูลมูลรถ และ รายละเอียดเงื่อนไข ราคาตั้ง !! ');
			   return false;
			}else if(document.getElementById('Plandetail').value == ''){
			   alert('กรุณากรอกรายละเอียดการวางแผนออกติดตาม  !! ');
			   return false;
			}
			/* var Neg_Detail = document.getElementsByName('Neg_Detail[]');
			for(var i=0;i < Neg_Detail.length;i++){
				if(i == 0){ i = '';}
				if(document.getElementById('select_NegMainGrp'+i).value != '' || document.getElementById('select_NegSubGrp'+i).value != '' ){
					var j = i+1;
					if(document.getElementById('SolveDetail'+i).value == ''){
						alert('กรุณากรอกรายละเอียดการแก้ไข Negในช่องข้อมูลที่'+j+'ด้วยครับ !!');
						return false;
					}
				}
			} */

			calRecCost();
			calOthCost('');
			//	calAccCost();  ## old function
			calAllAccPrice('');
			calCampaignTabs('');
			calTotal_Budget();

		}else{
			return false;
		}
	}
	function updateCon(){
		calRecCost();
		// calAccCost();  ## old function  Accessory

		calAllAccPrice('updateCon');   //Accessory
		calOthCost('updateCon');		// other Cost


		var AccessoryCost = $('#AccessoryCostTabsHidBuy').val();
		var OthCost = $('#OthCostHidBuy').val();
		calPurchasePrice(OthCost,AccessoryCost);    // buy value
		calCampaignTabs('updateCon');
		calTotal_Budget();	   // total budget
		setDisplayNone('updateCon');

	}
	function otherCostInner(val,id,chk){
		if(chk = 'EditBook'){$('#OtherCostLi li.ui-state-active span').text(val);$('li#sublist_OtherCost'+id+' a').text('กลุ่มรายการ    '+val);
		}else{$("#otherCostInner"+id).html("กลุ่มรายการ    "+val);}

		/* var OtherCost = document.getElementsByName("OtherCost[]");
		var chk = $("#OthCost_Grp"+i).value()
		for(var i = 0;i<OtherCost.length;i++){
			if(i == 0){ i = "";}
			if( chk == ""){
				$("#otherCostInner"+i).html("ค่าใช้จ้ายอื่นๆ  "+i);
			}else{
				$("#otherCostInner"+i).html("กลุ่มรายการ    "+val);
			}
		} */
	}
	function SHCompetitors(chk){
		if(chk == 'HIDE'){
			setDisplayNone('Unsuccessful');
			document.getElementById('SHCompetitors').innerHTML = '<a style=\"color:#ff0000;\" class=\"cursor\" onclick=\"SHCompetitors(\'SHOW\')\">แสดง รายละเอียดข้อมูลคู่แข่งต่างยี่ห้อ</a>';
		}else if(chk == 'SHOW'){
		    setDisplayInline('Unsuccessful');
			document.getElementById('SHCompetitors').innerHTML = '<a style=\"color:#ff0000;\" class=\"cursor\" onclick=\"SHCompetitors(\'HIDE\')\">ซ่อน รายละเอียดข้อมูลคู่แข่งต่างยี่ห้อ</a>';
		}
	}
function setemtryValue(){$('#Compare_OpenPrice').val('0.00');}
/* jQuery plugin */
function TreeView(){
	$(document).ready(function(){
		$("#TreeInt").tree({
			rules : { draggable : false,use_max_children : false,use_max_depth : false,multitree : true,multiple : true},callback : {},
			/* ui : {theme_name : "checkbox"}, */
			plugins : { /* hotkeys : {functions : {"s" : function () {alert($.tree.focused().container.attr("id"));}}} */}
		});//selected_tree();  // external function
	});
}
function MenuTree(div){$(document).ready(function(){$(div).tree({rules : {use_max_children : false,use_max_depth : false,multitree : true,multiple : true}});});}
function Enlarge(){
	$(document).ready(function(){
		textInSearch("#cus_moreMem","ค้นหาจากชื่อลูกค้า");textInSearch("#cus_more","ค้นหาจากชื่อพนักงาน")
		var oWidth = $('img.resize').width(); //get the width
		var oHeight = $('img.resize').height(); //get the height
		var mpx = (oWidth / oHeight); //run a function when the image is hovered over
		$('img.resize')
			.hover(function(){ 	//mouseOver effect
				$(this) 		//take the currently targeted img
					.stop() 	//stops the event from happening in case of an abrupt mouseOut
					.animate({	//custom animation effect to change the width and height of the img
						width: (oWidth * 3.13) +'px',height: (oHeight * 2.73) +'px' //take the original width/height X multipler and tag on the 'px'
					},1000);	//space the animation out over 1 sec (deals in milliseconds)
			},
			//this is just like a mouseOut effect to take the img back to the original size
			function(){
				$(this)			//stops the event from happening in case of an abrupt mouseOut
					.stop()
					.animate({width: oWidth +'px',height: oHeight +'px'},1000);
			});
	});
}
function Citizen(){$(function(){$("input[name^='citizen_']").keyup(function(){if($(this).val().length==$(this).attr("maxLength")){$(this).next("input").focus();}});});}
function textInSearch(objSet,objSetText){$(function(){if($(objSet).val()==""){$(objSet).val(objSetText);}
$(objSet).focus(function(){if($(objSet).val()==objSetText){$(objSet).val("");}}).blur(function(){if($(objSet).val()==""){$(objSet).val(objSetText);}});});}
function Watermark(objKey){ $(function(){if($(objKey).val()==""){$(objKey).addClass("watermark");}
$(objKey).focus(function(){$(objKey).removeClass("watermark");}).blur(function(){if($(objKey).val()==""){$(objKey).addClass("watermark");}});});}
function Overlay(){$(function() {$('#overlay').fadeIn('fast',function(){$('#box').animate({'top':'160px'},500);});
$('#boxclose').click(function(){$('#box').animate({'top':'-400px'},500,function(){$('#overlay').fadeOut('fast');});}); });
}
function Calculator(hide){
	if(hide != ""){ $(hide).hide(); }
	$(function(){ var cal_result=0;var resultD="";
		$("div.iDigit > div").click(function(){
			var pressD=$(this).text();var pressID_C=encodeURIComponent(pressD);
			if(pressD=="C"){resultD=0;}else{
				if(cal_result==1){resultD=pressD; cal_result=0;	}else{if($("div.iResult").text()=="0"){resultD=pressD;}else{resultD+=pressD;}}
			}if(pressID_C=="%3D"){resultD=eval($("div.iResult").text());cal_result=1;}$("div.iResult").text(resultD);
		});
	});
}
function enterSearch(check,caseName,cusName,cusNo,url){//naizan add ,url
	$(document).ready(function(){
		$('#cus_name').keyup(function(e){if(e.keyCode == 13) {searchcus(check,caseName,cusName,cusNo,url);}});
		$('#cus_surname').keyup(function(e){if(e.keyCode == 13) {searchcus(check,caseName,cusName,cusNo,url);}});
		$('#cus_id').keyup(function(e) {if(e.keyCode == 13) {searchcus(check,caseName,cusName,cusNo,url);}});
		$('#cus_aumpure').keyup(function(e) {if(e.keyCode == 13) {searchcus(check,caseName,cusName,cusNo,url);}});
		$('#cus_province').keyup(function(e) {if(e.keyCode == 13) {searchcus(check,caseName,cusName,cusNo,url);}});
		$('#cus_tel').keyup(function(e) {if(e.keyCode == 13) {searchcus(check,caseName,cusName,cusNo,url);}});
	});
}
function selectMulti(){
	$(function(){
		$("#data1").dblclick(function(){ var numOp=$("#data1 option:selected").length;
			if(numOp>0){for(i=0;i<numOp;i++){$("#data2").prepend($("#data1 option:selected").eq(i));}}
		});
		$("#data2").dblclick(function(){ var numOp=$("#data2 option").length;
			if(numOp>0){for(i=0;i<numOp;i++){$("#data1").prepend($("#data2 option:selected").eq(i));}$("#data2").find("*").attr("selected", "selected");	}
		});
	});
}






/* ---------------  new use general -----------------*/
function auto_com(target,input_id,hidden_val,shoe){
	$(function() {
		$(hidden_val).val("");
		$(input_id).autocomplete({
			delay : 500,//milliseconds #naizan 2011-05-20
			source: target,
			minLength: 2,
			select: function(event, ui) {
				$(hidden_val).val(ui.item.hidden_value);
				if(shoe=="changeResperson"){ check_resperson($(hidden_val).val());}
			}
		});
	});
}
function preview(what) {
	if(jQuery.browser.msie) { document.getElementById("preview-photo").src=what.value; return;}
	else if(jQuery.browser.safari) {document.getElementById("preview-photo").src=what.value; return;}
	document.getElementById("preview-photo").src=what.files[0].getAsDataURL();
	var h = jQuery("#preview-photo").height();  var w = jQuery("#preview-photo").width();
	if ((h > 100) && (w > 100)){
		if (h > w){jQuery("#preview-photo").css("height", "150px"); jQuery("#preview-photo").css("width", "auto");}
		else { jQuery("#preview-photo").css("width", "150px"); jQuery("#preview-photo").css("height", "auto"); }
	}
}
function Tooltip(id){$(function(){$(".product_temp").hover(function(event){callTooltip("#infoTool",event,$(this).find("textarea").attr("title"),id);},
function(){$("#infoTool").hide();});});}
function callTooltip(obj,event,strText,id){ 	// ฟังก์ ชั่นแสดงกล่องข้อความ Tooltip
	//var locateX=event.pageX; var locateY=event.pageY; //locateX=400;locateY=10;
	var offset = $("#"+id).offset(); //กำหนดจุดเริ่มแสดงผล
	var locateX = Math.floor(offset.left);
	var locateY = Math.floor(offset.top); locateY = locateY -180;
	if(strText != " "){$(obj).show().css({left:locateX,top:locateY}).addClass('ui-corner-all').html(strText);	}
}
function callTool(obj,event,target){ 	// ฟังก์ ชั่นแสดงกล่องข้อความ Tooltip
	var locateX=event.pageX; var locateY=event.pageY; locateX = locateX-120; locateY=locateY-200;
	if($('#'+obj).val() != ""){$('#'+target).css({left:locateX,top:locateY}).addClass('ui-corner-all').html($('#'+obj).val()).show();}
}
function preload(div){ $(div).html( "<div style='text-align: center'><img src='java/img/wait1.gif'/></div>");}
function toggleDetail(div,imgsrc){
	$("#"+div).slideToggle('fast'); if(($("#"+imgsrc).attr('src')) == "temp/collapse.gif"){ $("#"+imgsrc).attr('src','temp/expand.gif');}
	else{ $("#"+imgsrc).attr('src','temp/collapse.gif'); preload(div); }
	if(div.substring(0,15)=='blind_condition'){	click_producttemp($('#cusint_product_temp'+div.substring(16)+' ul#ul_product_temp')); }
}
function is_numeric(data){return parseFloat(data)==data;}//return ((typeof data === typeof 1) && (null !== data) && isFinite(data));}//
function format_number(num, decplaces,showtt){
	num = parseFloat(num);
	if (!isNaN(num)){
		var str = "" + Math.round (eval(num) * Math.pow(10,decplaces));
		if (str.indexOf("e") != -1) {return "Out of Range";}
		while (str.length <= decplaces){ str = "0" + str;}
		var decpoint = str.length - decplaces;var tmpNum = str.substring(0,decpoint);
		//---------------Add Commas--------------------------
		var numRet = tmpNum.toString();if(showtt==1){var re = /(-?\d+)(\d{3})/;while (re.test(numRet)){numRet = numRet.replace(re, "$1,$2");}}
		// -- modify
		var number = numRet + "." + str.substring(decpoint,str.length);
		if(number == '0.00'){number = "";} return number;
	}else{ return ""; }
}




/* -- -------------- ic_newcusint gerneral function --------------- */
function cal_rec_cost(){
	var size = null; var rec_com_value = 0; var rec_type = $("input[name=rec_type\\[\\]]"); var rec_com = $("input[name=rec_com\\[\\]]");
	size = $("input[name=rec_com\\[\\]]").size();for(var i=0;i<size;i++){if(rec_type[i].value == "แนะนำและค่าคอม"){rec_com_value += +textTOnum(rec_com[i].value);}}
	$('#RecCost').val(format_number(rec_com_value,2,1)); cal_all_cost();
}
function cal_accessory_cost(){
	var loop = $("input[name='ItemForPer[]']").size();
	var name = $("input[name='ItemForPer[]']");
	var val = 0; var buy = 0; var buy_value = 0; var OthCost = 0;
	for(var i = 0;i < loop; i++){
		var value = name[i].value.split('@#');
		var price = textTOnum(value[5]);				// ราคารวม
		buy = textTOnum(buy);							// ราคาที่ซื้อเอง
		if(value[2] == "แถม"){
			price = price;
		}else if(value[2] == "ซื้อ"){
			buy_value = price;
			price = 0;
		}else if(value[2] == "ลูกค้าจ่าย"){
			price = price;
			buy_value = (parseFloat(value[3])-parseFloat(price));
			price = price;
		}
		buy = (parseFloat(buy_value)+parseFloat(buy));
		val = ( parseFloat(price)+parseFloat(val));
		//  ex. ลายเนอร์@#1066@#แถม@#4500@#2@#2,500.00
		if(buy != 0 || buy != ''){ buy = format_number(buy,2,1);}else if(buy == 0){ buy = ''; }
	}
	if(val != 0 || val != ''){ val = format_number(val,2,1);}else if(val == 0){ val = ''; }
	$('#AccessoryCost').val(val); $('#for_buy_val').val(buy); cal_cus_buy(); cal_all_cost();
}
function cal_othercost(){
	var loop = $("input[name='othcost_value[]']").size(); var val = $("input[name='othcost_value[]']");
	var name = $("input[name='othcost_type[]']"); var gift = 0; var buy = 0;
	for(var i=0;i<loop;i++){ if(val[i].value == ""){val[i].value = 0;}
	if(name[i].value == "แถมลูกค้า"){gift += +textTOnum(val[i].value);}else{buy += +textTOnum(val[i].value);}
	}$('#other_buy').val(buy); cal_cus_buy(); $('#OthCost').val(format_number(gift,2,1));
	cal_all_cost();
}
function cal_cus_buy(){
	//alert("cal_cus_buy");
	//alert(document.getElementById("OtherBuyVal").value);
	var other_cost = textTOnum($('#other_buy').val()); var acessory_cost = textTOnum($('#for_buy_val').val());
    result = format_number((parseFloat(other_cost)+parseFloat(acessory_cost)),2,1);if(result == '0.00'){ result = '';}
	$('#Cost_Buy_Option').val(result); cal_all_cost();   // $('#OtherBuyVal').val(result); สำหรับ function ใหม่

}
function cal_all_cost(total_id){//naizan add  total_id 16-10-2010
	var discount = textTOnum($('#Discount').val());var usecar = textTOnum($('#Sub_UsedCar').val());var sub_down = textTOnum($('#Sub_Down').val());
	var rec_cost = textTOnum($('#RecCost').val());var other_cost = textTOnum($('#OthCost').val());var accessory = textTOnum($('#AccessoryCost').val());
	var campaign = textTOnum($('#CampaignCost').val()); var result = (((((discount+usecar)+sub_down)+rec_cost)+other_cost)+accessory)-campaign;
	$('#Total_Budget').val(format_number(result,2,1));
	$('#sp_total_budget'+total_id).html(format_number(result,2,1));
}
function cal_installment(total_id){
	/* -- cal netprice -- */
	var increase_car = textTOnum($('#IncreaseCarPrices').val());var discount = textTOnum($('#Discount').val());
	var crp_price = textTOnum($('#CRPPrice').val()); $('#NetPrice').val(format_number(((crp_price - discount)+increase_car),2,1));
	/* -- cal down_payment -- */
	var down = textTOnum($('#Down').val());var sub_down = textTOnum($('#Sub_Down').val());$('#Real_DownPay').val(format_number((down-sub_down),2,1));
	/* -- cal balancing -- ยอดจัด */
	var balancing = textTOnum($('#NetPrice').val())-textTOnum($('#Down').val());
	/* -- ดอกเบี้ยทั้งหมด  -- */
	var interest = (balancing*(textTOnum($('#IntCon').val())/100))*(textTOnum($('#Time').val())/12);
	var time = textTOnum($('#Time').val()); if(time == '0.00' || time == '0' || time == ''){time = 1;}
	$('#Installment').val(format_number(Math.ceil((balancing+interest)/(time)),2,1));
	cal_all_cost(total_id);//naizan add  total_id 16-10-2010
}
function cal_campaign(check){
	var headbar = $("input[name='HeadTis[]']");
	var val = 0;
	for(var i=0; i<headbar.length;i++){
		var list = document.getElementsByName(headbar[i].value+'[]');
		for(var j=0;j<list.length;j++){
			if(list[j].checked == true){
				var value = list[j].value.split(',');
				var num = document.getElementById('CampaignValue#@'+value[0]+'#@'+value[1]).value;
				var status = document.getElementById('CampaignStatus#@'+value[0]+'#@'+value[1]).value;
				if(status == 2){ num = num;		// ส่วนลด
				}else if(status == 1){ num = 0;}     // แถม
				if(num==''){num = 0;} val = parseFloat(textTOnum(num))+parseFloat(val);
			} /* end if */
		} /* end loop */
	} /* end loop */
	// if copy condition and update
	if(check == 'updateCon'){ val = $('#CampaignCost').val(); val = textTOnum(val); val = parseFloat(val); }else{ val = val;}
	if(val != 0 || val != ''){ val = formatNumber(val,2,1);}else if(val == 0){ val = ''; }
	$('#CampaignCost').val(val); cal_all_cost();
}
function add_accessory_list(asstype,assname,SaleAcc,AssCode,Dunit,Dname){
	var chkval = $('input:radio[name=Item_ForAll]:checked').val();  // get value buy or gift
	var amount = $('#AmountListName'+AssCode+''+Dname).val();  		// จำนวน
	if(amount == ''){
		alert('กรุณาระบุุจำนวนชิ้น !!');
	}else{
		$("#All_AccUse_contain").show('fast',function () {
			var div_id = ''; var setPrice = ''; var DividePrice= 0;
			if(chkval == "แถม"){
				div_id = '#All_AccUse_ForGift';
			}else if(chkval == "ซื้อ"){
				div_id = '#All_AccUse_ForBuy';
			}else if(chkval == "ลูกค้าจ่าย"){
				div_id = '#All_AccUse_ForSetPrice';
				setPrice = '<span class=\"paddingLeft\">ลูกค้าจ่าย        '+$('#DividePriceVal'+AssCode+''+Dname).val()+'     บาท        ราคาขาย        '+SaleAcc+'       บาท </span>'
				DividePrice = parseFloat(textTOnum($('#DividePriceVal'+AssCode+''+Dname).val())); $('#DividePrice'+AssCode+''+Dname).hide();
			}
			SaleAcc = textTOnum(SaleAcc);
			var value = formatNumber(((parseFloat(amount)*parseFloat(SaleAcc))-parseFloat(DividePrice)),2,1);
			var del_id = 'addId@#'+AssCode+''+Dname+'@#'+chkval;
			
			if(!document.getElementById(del_id)){
				$(div_id).append('<div id=\"addId@#'+AssCode+''+Dname+'@#'+chkval+'\" class=\"paddingLeft fontBold\" style=\"font-size:12px;margin-top:10px;margin-bottom:10px;\"><span class=\"paddingLeft\"> - '+asstype+'  ( '+assname+' )</span><span class=\"paddingLeft\">จำนวน         '+amount+'  '+Dunit+'</span><span style="color:#ff0000;" class=\"paddingLeft\">ราคารวม               '+value+'            บาท</span>'+setPrice+'<span class=\"paddingLeft\"><input type=\"hidden\" name=\"ass_transac_no[]\" value=\"\"/><img src=\"temp/i_del.gif\" onclick=\"del_accessory_list(\''+del_id+'\')\" name=\"del\" class=\"cursor\"></span><span id="ItemFor@#'+AssCode+'\"><input type=\"hidden\" name=\"ItemForPer[]\" id=\"ItemForPer@#'+AssCode+''+Dname+'\" value=\"'+asstype+'@#'+AssCode+'@#'+chkval+'@#'+SaleAcc+'@#'+amount+'@#'+value+'\"/></span></div>');
			}else{
				//document.getElementById(del_id).innerHTML = '<div id=\"addId@#'+AssCode+''+Dname+'@#'+chkval+'\" class=\"paddingLeft fontBold\" style=\"font-size:12px;margin-top:10px;margin-bottom:10px;\"><span class=\"paddingLeft\"> - '+asstype+'  ( '+assname+' )</span><span class=\"paddingLeft\">จำนวน         '+amount+'  '+Dunit+'</span><span style="color:#ff0000;" class=\"paddingLeft\">ราคารวม               '+value+'            บาท</span>'+setPrice+'<span class=\"paddingLeft\"><input type=\"hidden\" name=\"ass_transac_no[]\" value=\"\"/><img src=\"temp/i_del.gif\" onclick=\"del_accessory_list(\''+del_id+'\')\" name=\"del\" class=\"cursor\"></span><span id="ItemFor@#'+AssCode+'@#'+AssCode+'\"><input type=\"hidden\" name=\"ItemForPer[]\" id=\"ItemForPer@#'+AssCode+'@#'+AssCode+'\" value=\"'+asstype+'@#'+AssCode+'@#'+chkval+'@#'+SaleAcc+'@#'+amount+'@#'+value+'\"/></span>';
				//ลบของแต่งเดิมก่อน (ถ้าเป็นของแต่งไอดีเดียวกัน) แล้วค่อยเพิ่มใหม่ ไม่เช่นนั้นจะมีไอดีเดียวอัน inner อีเลเมนต์ไอดีเดียวกันเพิ่มขึ้นเรื่อยๆ (ข้อสังเกตข้อความจะเยื้องไปทางขวาเรื่อยๆ)
				//ใช้ jQuery ลบไม่ได้ เพราะ del_id มี # หลายตัว ทำให้ใช้อ้างอิงไม่ได้
				var parent_div = div_id.replace("#","");
					parent_div = document.getElementById(parent_div);
				var oldChild = document.getElementById(del_id);
				parent_div.removeChild(oldChild);//ลบ
				$(div_id).append('<div id=\"addId@#'+AssCode+''+Dname+'@#'+chkval+'\" class=\"paddingLeft fontBold\" style=\"font-size:12px;margin-top:10px;margin-bottom:10px;\"><span class=\"paddingLeft\"> - '+asstype+'  ( '+assname+' )</span><span class=\"paddingLeft\">จำนวน         '+amount+'  '+Dunit+'</span><span style="color:#ff0000;" class=\"paddingLeft\">ราคารวม               '+value+'            บาท</span>'+setPrice+'<span class=\"paddingLeft\"><input type=\"hidden\" name=\"ass_transac_no[]\" value=\"\"/><img src=\"temp/i_del.gif\" onclick=\"del_accessory_list(\''+del_id+'\')\" name=\"del\" class=\"cursor\"></span><span id="ItemFor@#'+AssCode+'\"><input type=\"hidden\" name=\"ItemForPer[]\" id=\"ItemForPer@#'+AssCode+''+Dname+'\" value=\"'+asstype+'@#'+AssCode+'@#'+chkval+'@#'+SaleAcc+'@#'+amount+'@#'+value+'\"/></span></div>');
				//--
			}

			
			$('#DividePriceVal'+AssCode+''+Dname).val(''); $('#AmountListName'+AssCode+''+Dname).val('');
			cal_accessory_cost();
		});
	}
}
function del_accessory_list(id){document.getElementById(id).innerHTML = '';cal_accessory_cost();}
function select_campaign_list(Tisname,TisCode,CampaignValue,Status){
	if(document.getElementById('checkCampaignName'+(Tisname+TisCode)).checked == true){
		var id  = document.getElementById('CampaignCode#@'+Tisname+'#@'+TisCode).value;var value = id+'@#'+Tisname+'@#'+CampaignValue;
		document.getElementById('CampaignAllValue'+Tisname+TisCode).innerHTML = '<input type=\"hidden\" name=\"CampaignAllValue[]\" id=\"CampaignAllValue#@'+Tisname+'#@'+TisCode+'\" value=\"'+value+'\"/>';
		setDisplayInline(Tisname+TisCode);setDisplayInline('HidCampaignValue#@'+Tisname+'#@'+TisCode);
	}else{setDisplayNone('HidCampaignValue#@'+Tisname+'#@'+TisCode);document.getElementById('CampaignAllValue'+Tisname+TisCode).innerHTML = '';}
	cal_campaign(Status);
}
function call_overlay(div){ $(div).show().addClass('ui-widget-overlay').css({width:"100%" ,height:$(document).height(),opacity:.3});}



/* -- -------------- ic_newcusint -- ------------- */
function sell_type_cusint(){
	if($("input[name=Sell_Type]:checked").val() == "สด"){
		$('#Down').attr('readonly','readonly'); $('#Sub_Down').attr('readonly','readonly');
		$('#IntCon').attr('readonly','readonly'); $('#Time').attr('readonly','readonly');
	}else{
		if($('img#cond_edit').hasClass('hidden')){
			$('#Down').removeAttr('readonly'); $('#Sub_Down').removeAttr('readonly'); $('#IntCon').removeAttr('readonly'); $('#Time').removeAttr('readonly');
			return false;
		}else if($('img#accedit').hasClass('hidden') == false){
			$('#Down').removeAttr('readonly'); $('#Sub_Down').removeAttr('readonly');$('#IntCon').removeAttr('readonly'); $('#Time').removeAttr('readonly');
			return false;
		}
	}
}
function call_cusint_his(cusno,case_name){
	$.get("ic_newcusint.php",{call_cusint_his:'call_cusint_his',cusint_number:cusno,cusint_checkpage:case_name},function(data){
		$('#Re_CusInt_his').html(data); $('#InterTotal').val($('#interested_numer').val());
	});
}
function prv_next(tabname){
	$(function() {
		var $tabs = $(tabname).tabs();
		$(".cusint-tabs-panel").each(function(i){ var totalSize = $(".cusint-tabs-panel").size() - 1;// if($('#ctwo').text()=='นำเสนอการติดตาม'){totalSize=totalSize+1;}
		  if (i != totalSize) { next = i + 2; if(next == 7){ hide = 'hidden'; }else{ hide = ''}
		  $(this).append("<a href='#' class=' " + hide +" next-tab mover' rel='" + next + "'>Next Page &#187;</a>");}
		  if (i != 0) { prev = i; $(this).append("<a href='#' class='prev-tab mover' rel='" + prev + "' >&#171; Prev Page</a>"); }
		});
		$('.next-tab, .prev-tab').click(function() { $tabs.tabs('select', $(this).attr("rel"));
		if(($(this).attr("rel")) == 2){ savetemp_cusint();}
		return false; });
	});
}
function savetemp_cusint(){
	if(($('#cusint_num_ref').val()) == ""){
		$.get("ic_newcusint_action.php",{deletes:'unsuccessful'},function(data){
			$.post("ic_newcusint_action.php",(jQuery('#cusint-1').find('input,input[type=hidden],radio,check,checkbox,button,select,textarea').serialize())+
			'&insert=cus_int&cus_no='+$('#edit_id').val(),function(data){ 
				if((data * 1) && (data != 0)){ 
					$('#cusint_num_ref').val(data);$('#add_product').show();
				}else{
					alert('เกิดข้อผิดพลาดบางอย่าง กรุณาเรียกหน้าโปรแกรมใหม่และทำรายการใหม่!');
				}
			});
		});
	}
}
function selected_tree(num){  /* $('#hierarchy-'+($tabs.tabs('option', 'selected')+1)).addClass('clicked'); */
	$('#containAll').tabs('option', 'selected', num);
	/* for(i=0;i<$(count_val).size();i++){ $('#'+div+i).show('fast');}  */
}
function selected_sub_tree(count_val,div,num,sub_num){ selected_tree(num); if(sub_num == ''){ sub_num = 0;}
	for(i=0;i<$(count_val).size();i++){if(sub_num == i){ if(i == 0){ i = '';} $('#'+div+i).show('fast');}else{if(i == 0){ i = '';} $('#'+div+i).hide('fast');}}
}
function more_car_exchange(chk,cus_number){
	var size = $(chk).val();if(size<1){ size = $('input[name="more_carexchange[]"]').size();}else{size= +size+1;}
	var content = "<div id='cusint_carexchange_content"+size+"' class='marginBottom'></div>";
	$('#cusint_carexchange_area').append(content); $(chk).val(size);
	browseCheck('cusint_carexchange_content'+size,'ic_newcusint.php?MoreExchange='+size+'&ExchangeCus_number='+cus_number,'');
	sub_more_all(size,'sublist_carexchange','#sublist_carexchange_contain','MoreExchangeli');
}
function all_delinner(contain,li,class){ if($('.'+class).size() == 1){ return false; }else{  $('#'+contain).remove(); $('#'+li).remove();} }
function remove_all(div){ $(div).remove();}
function del_producttemp(id,temp_num,del_case){  var size = $('#blind_condition_'+temp_num+' .cond_text input[name=condition_temp\\[\\]]').size();
	var value = $('#blind_condition_'+temp_num+' .cond_text input[name=condition_temp\\[\\]]'); var b_check = 1; var con_num = '';
	for(var i =0;i<size;i++){ if(b_check != 1){ con_num += ","; } con_num += $(value[i]).val(); b_check = 2;}
	$.get("ic_newcusint_action.php",{deletes:'cusint_product',del_id:id,cond_id:con_num,cusint_num_ref:$('#cusint_num_ref').val()},
	function(data){remove_all('#cusint_product_temp'+temp_num);remove_all('#blind_condition_'+temp_num);});
}
function del_database(table,id){$.get("ic_newcusint_action.php",{deletes:table,del_id:id});}
function edit_temp(div,reset,inhtml,number,show,hide,product_id,id_ref){
	if($('#prepare_num_id').val()){ }  // case สำหรับ หน้านำเสนอการติดตาม
	$.get("ic_newcusint.php",(jQuery(div).find('input,input[type=hidden],select,textarea').serialize())+'&check_number='+number+'&reset='+
	reset+'&product_id='+product_id+'&con_id_refer='+id_ref,function(data){
		$(inhtml).html(data); $(show).show(); $(hide).hide();
		if($('#prepare_num_id').val()){ $('#prepare_num_ref').val($('#prepare_num_id').val()); }  // case สำหรับ หน้านำเสนอการติดตาม
	});
}
function edit_database(inhtml,show,hide,id,case_name){
	$.get("ic_newcusint.php",{reset:case_name,edit_id:id},function(data){ $(inhtml).html(data);$(show).show(); $(hide).hide();  });
}
function mouseover_protemp(div){$(div).hover(function(event){$(this).css("background-color","#f9dc49");},function(){ $(this).removeAttr('style');}); }
function more_all(chk,csize,ccontent,cappend,variable,sub_content,sub_append,sub_variable){
	var size = $(chk).val();if(size<1){ size = $('input[name="'+csize+'"]').size();}else{size= +size+1;}
	var content = "<div id='"+ccontent+size+"' class='marginBottom'></div>"; $(cappend).append(content); $(chk).val(size);
	browseCheck(ccontent+size,'ic_newcusint.php?'+variable+'='+size,'');
	sub_more_all(size,sub_content,sub_append,sub_variable);
}
function more_condition(temp_num,price){
	if(confirm('ต้องการเพิ่มเงื่อนไข สำหรับข้อมูลรถคันนี้ !!') == true){
		$.post("ic_newcusint_action.php",{add_case:'condition',temp_num:temp_num,price:price,append_num:$('#temp_numcheck'+temp_num).val(),
		product_insertid:$('#product_insertid'+temp_num).val(),cusint_num_ref:$('#cusint_num_ref').val(),cus_no:$('#edit_id').val(),
		prepare_num_ref:$('#prepare_num_ref').val()},function(data){ $('#blind_condition_'+temp_num).prepend(data);
			var num = (+($('#temp_numcheck'+temp_num).val())+1);$('#temp_numcheck'+temp_num).val(num);sublist_pro_contain(num,temp_num);
			$('#blind_condition_'+temp_num).show(); $("#toggle_pic"+temp_num).attr('src','temp/collapse.gif');
		});
	}
}
function sublist_pro_contain(num,temp_num){
	$.get("ic_newcusint_action.php",{create:'sublist_blind_cond',condition_num:num,temp_num:temp_num},function(data){
	$('#sublist_pro_contain'+temp_num).append(data);});
}
function make_condition(temp_product_num,condition_id,checkbox,case_name,thishtm){
	var size = $("input[name=condition_temp\\[\\]]").size();
	$("input[name=condition_temp\\[\\]]").attr('checked',false);
	$('.sp_active_remark').addClass('hidden');		 // remove remark ว่ากำลังเลือกใช้เงื่อนไขนี้
	if(case_name != 'refresh'){
	    $("#"+thishtm.id).attr('checked',true);
	    $('#sp_active_remark'+checkbox).removeClass('hidden');
	} // show remark ว่ากำลังเลือกใช้เงื่อนไขนี้
	$('.cond_text').removeClass('orange_border');
	if(document.getElementById('refresh_cond')){var temp_num = $('#refresh_cond').val().split('@#');}$('.cond_detail').hide(); $('.cond_detail').html('');
	switch(case_name){
		case 'refresh': //jQuery('#frm_'+checkbox).get(0).reset();
			$.get("ic_newcusint_action.php",{create:'blind_condition',condition_id:temp_num[1],unit_type:$('#int_cartype'+temp_num[0]).val(),
			cusint_num_ref:$('#cusint_num_ref').val(),checkbox:checkbox},function(data){ $('#condition_detail_'+checkbox).show('fast');
			$('#condition_detail_'+checkbox).html(data); jqueryUI('#blind_cond'+$('#condition_id_value').val()); var pro_num = checkbox.split('_');
				var crp = $('#int_crp'+pro_num[0]).val().split('_'); if(crp[1]){$('#CRPPrice').val(format_number(crp[1],2,1));}
				$('#blind_cond'+pro_num[0]).tabs('select', 4); $('#refresh_cond').val(temp_product_num+'@#'+condition_id);
				$('#condition_temp'+checkbox).attr('checked',true); $('#sp_active_remark'+checkbox).removeClass('hidden');
			});
		break;
		case 'new_prepare': // create new prepare to present
			$('#cond_text_'+checkbox).addClass('orange_border');
			$.get("ic_newcusint_action.php",{create:'blind_condition',condition_id:condition_id,unit_type:$('#int_cartype'+temp_product_num).val(),
			cusint_num_ref:$('#cusint_num_ref').val(),checkbox:checkbox,chk_case:'new_prepare'},function(data){ $('#condition_detail_'+checkbox).html(data);
				$('#condition_detail_'+checkbox).show('fast'); jqueryUI('#blind_cond'+condition_id);//$('#blind_cond'+temp_product_num).tabs('select', 4);
				$('#blind_cond'+condition_id).tabs('select', 4);

				$('.iHelp,.i_del,.i_add_m,.edit,.refresh,.i_reset').addClass('hidden');set_none_input();

				// -- update 07-01-2010 หน้าบันทึกผลการติดตาม พนังงานขาย
				if($('#btnCallCond').val()=='แก้ไขเงื่อนไข'){
					$.post('ic_newprepare_action.php',{insert:'prepare',case_name:'sup_edit',int_number:$('#Int_Num').val(),prepare_cus:$('#CusNo').val(),
					int_time:$('#Interest_Time').val(),old_prepare_num:$('#Con_prepare_Num').val()},function(data){
						data = jQuery.trim(data);
						arr = data.split('#@');
						$('#prepare_num_ref').val(arr[0]);
						$('#prepare_num_id').val(arr[0]);
						$('#overlay-prepare').hide();
						// case หลังจากไปติดตามแล้วบันทึกผลและมีการเปลี่ยนแปลงเงื่อนไข
						//if(check_case=='edit_condition'){ remove_class('prepare-foot','hidden');}
						// case บันทึกผลการติดตาม    08-01-2011
						if($('#btnCallCond').val()=='แก้ไขเงื่อนไข'){  $('#summit_approve').hide(); }
						$.get('ic_newprepare.php',{prepare_cus:$('#CusNo').val(),int_number:$('#Int_Num').val(),case_name:'copyint_prepare',
						prepare_num_ref:$('#prepare_num_ref').val()},function(data){ prepare_copyold_after(data); });
					});
				}
				//
			});
		break;
		case 'approve_report':
			$('#cond_text_'+checkbox).addClass('orange_border');
			$.get("ic_newcusint_action.php",{create:'blind_condition',condition_id:condition_id,unit_type:$('#int_cartype'+temp_product_num).val(),
			cusint_num_ref:$('#cusint_num_ref').val(),checkbox:checkbox,chk_case:'new_prepare',case_name:case_name},function(data){ $('#condition_detail_'+checkbox).html(data);
				$('#condition_detail_'+checkbox).show('fast'); jqueryUI('#blind_cond'+condition_id);//$('#blind_cond'+temp_product_num).tabs('select', 4);
				$('#blind_cond'+condition_id).tabs('select', 4);
				//$('.iHelp,.i_del,.i_add_m,.edit,.refresh,.i_reset').addClass('hidden');
				set_none_input();

			});
		break;
		default:
			if((document.getElementById('condition_temp'+checkbox).checked) == true){
				$('#cond_text_'+checkbox).addClass('orange_border');
				$('#sp_active_remark'+checkbox).removeClass('hidden');  // show remark ว่ากำลังเลือกใช้เงื่อนไขนี้
				$.get("ic_newcusint_action.php",{create:'blind_condition',condition_id:condition_id,unit_type:$('#int_cartype'+temp_product_num).val(),
				cusint_num_ref:$('#cusint_num_ref').val(),checkbox:checkbox,chk_case:case_name},function(data){ $('#condition_detail_'+checkbox).show('fast');
				$('#condition_detail_'+checkbox).html(data); jqueryUI('#blind_cond'+condition_id); var crp = $('#int_crp'+temp_product_num).val().split('_');
					if(crp[1]){$('#CRPPrice').val(format_number(crp[1],2,1));} 	$('#blind_cond'+temp_product_num).tabs('select', 4);
					$('#refresh_cond').val(temp_product_num+'@#'+condition_id);
					if(case_name=='copy_prepare'){ $('.iCond').addClass('hidden');  /*  $('.can_read').removeAttr('readonly'); */ }
				});
			}else{  $('#condition_detail_'+checkbox).hide('fast'); $('#cond_text_'+checkbox).removeClass('orange_border');	}
		break;
	}
	//return condition_id;
}
function sub_more_all(size,sub_content,sub_append,sub_variable){
	var content = "<li id='"+sub_content+size+"'>"+sub_variable+"</li>"; $(sub_append).append(content);
	browseCheckNoPre(sub_content+size,'ic_newcusint.php?'+sub_variable+'='+size,'');
}
function change_con_title(num,case_name){if(case_name == "show"){$('#con_title_'+num).hide();$('#conval_detail_'+num).show();}else{$('#con_title_'+num).show();$('#conval_detail_'+num).hide(); $('#con_title_'+num).html($('#con_val_'+num).val());}
}
function click_producttemp(div){
	for(i=1;i<=$("#make_condition_list ul").size();i++){
		if($("#cusint_product_temp"+i+" ul").hasClass("bg_pink")){ $("#cusint_product_temp"+i+" ul").removeClass("bg_pink");}
	} $(div).addClass("bg_pink"); /* $('#cusint-6 a').removeClass('hidden'); $('#cusint-6 a').attr('onclick','call_accessories();call_campaign()');*/
	/* if($('#cusint-6 a').attr('rel') ==7){ $('#cusint-6 a').removeClass('hidden'); }; */
}
function reset_ele(div,para){ var add_case = '';
	if(para=='reset_product@#'){
		if($('#prepare_num_ref').val()==''){prepare_num_ref = $('#prepare_num_ref').val();}else{prepare_num_ref =  $('#prepare_num_id').val();}
		add_case = "new_cusint&prepare_num_ref="+prepare_num_ref;
	}else if(para=='reset_product@#new_prepare'){
		add_case = "new_prepare&prepare_num_ref="+$('#prepare_num_ref').val();
	}else if(para=='reset_product@#new_prepare_condition'){
		if($('#prepare_num_ref').val()==''){prepare_num_ref = $('#prepare_num_ref').val();}else{prepare_num_ref =  $('#prepare_num_id').val();}
		add_case = "new_prepare&prepare_num_ref="+prepare_num_ref;
	}
	browseCheck(div,'ic_newcusint.php?reset='+para.split('@#')[0]+'&add_case='+add_case,'');
}
function add_product(add_case){
	if($('#IntCarType').val()==''){alert('กรุณาเลือกข้อมูลรถ');return false;}
	if($('#IntCarPattern').val()==''){alert('กรุณาเลือกรุ่นปี');return false;}
	if($('#IntModelCodeName').val()==''){alert('กรุณาเลือก รหัสแบบรถ');return false;}
	if($('#IntModelGName').val()==''){alert('กรุณาเลือก รหัสแบบรถทั่วไป');return false;}
	if($('#CRPAndAirIncVat').val()==''){alert('กรุณาเลือกราคารถ');return false;}
	if($('#CarModelColor').val()==''){alert('กรุณาเลือก สี');return false;}
	if($('#BuyVolume').val()==''){alert('จำนวนที่สนใจซื้อ ');return false;}

	var prepare_num_ref = '';
	if($('#prepare_num_ref').val()==''){prepare_num_ref = $('#prepare_num_id').val();}else{prepare_num_ref = $('#prepare_num_ref').val();}
	if(prepare_num_ref==undefined){ prepare_num_ref = '';}

	$.post("ic_newcusint.php",(jQuery('#cusint_product_content').find('input, textarea, select').serialize())+'&product_temp='+$('#product_temp').val()+
	'&cusint_num_ref='+$('#cusint_num_ref').val()+'&cus_no='+$('#edit_id').val()+'&add_case='+add_case+'&prepare_num_ref='+prepare_num_ref,
	function(data){
	    sublist_product($('#product_temp').val());
		$('#product_temp').val((+($('#product_temp').val())+1));
		$('#make_condition_list').prepend(data);
		$("#make_condition ul").each(function (index){ $('div .product_temp').addClass("product_temp_ul");})
		reset_ele('cusint_product_content','reset_product@#'+add_case);
		// if$(.product_temp_ul).count();< 1 $(div).addClass("bg_pink");
	});

}
function sublist_product(number){$.get("ic_newcusint_action.php",{create:'sublist_product',product_number:number},function(data){ $('#ul_product').append(data);});}
function add_pro_con(field,content_left,content_right,temp,list_div,reset,bind){
	if($('ul.bg_pink').size() > 0){
		var product_id =  $('ul.bg_pink > li').find('input[type=hidden]').val();$(field).removeClass('default_box_size'); var case_name = null;
		switch(content_left){
			case 'recomend_content_left':	case_name = '&add_case=recomend';	break;
			case 'othercost_content_left':	case_name = '&add_case=othercost';	break;
			default: case_name = '';
		}
		$.post("ic_newcusint_action.php",(jQuery('#'+content_left).find('input,input[type=hidden],select,textarea').serialize())+'&rec_num='+$('#'+temp).val()+
		case_name+'&product_id='+product_id+'&cusint_num_ref='+$('#cusint_num_ref').val()+'&condition_no_ref='+$('div.orange_border input:checked').val()+
		'&cus_no='+$('#edit_id').val(),function(data){ $('#'+temp).val((+($('#'+temp).val())+1));    /*   if(($('#bind_product_cond_'+product_id)){  */
			if(!document.getElementById(bind+product_id)){$('#'+content_right).append("<div id='"+bind+product_id+"'></div>") ;}
			$('#'+bind+product_id).append(data);
			$('#'+content_right+" ul").each(function(index){$('div .'+list_div).addClass("product_temp_ul"); }); reset_ele(content_left,reset);
			switch(reset){
				case 'reset_recomd': cal_rec_cost(); break;
				case 'reset_othercost': cal_othercost(); break;
			}
		});
	}else{ alert(' กรุณาเลือกข้อมูลรถ ที่ต้องการทำเงื่อนไขด้วยครับ  ! ');}
}
function save_edit_pro(target,content_left,jshow,case_name,reset,number,product_id){   var all_refer_id = null;
	if(case_name == 'recomend'){ all_refer_id = $('#rec_ref_id').val();}else if(case_name == 'othercost'){all_refer_id = $('#other_ref_id').val();}
	$.post(target,(jQuery('#'+content_left).find('input,input[type=hidden],select,textarea').serialize())+'&edit_case='+case_name+
	'&rec_num='+number+'&product_id='+product_id+'&cusint_num_ref='+$('#cusint_num_ref').val()+'&condition_no_ref='+$('div.orange_border input:checked').val()+
	'&all_refer_id='+all_refer_id,
	function(data){ $('#'+jshow).html(data); reset_ele(content_left,reset)
	if($('#blind_condition_'+number).attr('style') != 'display: block;'){ $('#toggle_pic'+number).attr('src','temp/expand.gif');}
		if((case_name == 'product') && (document.getElementById('CRPPrice'))){ var crp = $('#int_crp'+number).val().split('_');
			if(crp[1]){$('#CRPPrice').val(format_number(crp[1],2,1));} $('#cusint_product_temp'+number+' ul#ul_product_temp').addClass('bg_pink');
			call_accessories('edit'); // เรียก accessory ใหม่
			cal_installment(); 		  // คำนวนส่วนลดราคารถ
		}else if(case_name == 'recomend'){ cal_rec_cost();
		}else if(case_name == 'othercost'){ cal_othercost();}
	});
}
function call_accessories(case_name){
	if($('img #cond_edit').hasClass('hidden')){ $('.iCond').addClass('hidden'); $("input[name=AmountListName\\[\\]]").attr('readonly',true);}
	switch(case_name){
		case 'edit':
			preload('#accessories_tabs_name');$.get('ic_newcusint_action.php',{call:'call_accessories',unittype:$('ul.bg_pink input[name="int_cartype[]"]').val()},
			function(data){ $('#accessories_tabs_name').html(data).delay(800).fadeIn(400); jqueryUI('#sunmenu_ass'); $('#selectItemFor').show('fast'); });
		break;
		case 'new_prepare':
			if(!document.getElementById('sunmenu_ass')){
				preload('#accessories_tabs_name');$.get('ic_newcusint_action.php',{call:'call_accessories',unittype:$('ul.bg_pink input[name="int_cartype[]"]').val()},
				function(data){ $('#accessories_tabs_name').html(data).delay(800).fadeIn(400); jqueryUI('#sunmenu_ass'); $('#selectItemFor').show('fast');
				$('.iCond').addClass('hidden'); $("input[name=AmountListName\\[\\]]").attr('readonly',true); });
			}else{ return false;}
		break;
		default:
			if(!document.getElementById('sunmenu_ass')){
				preload('#accessories_tabs_name');$.get('ic_newcusint_action.php',{call:'call_accessories',unittype:$('ul.bg_pink input[name="int_cartype[]"]').val()},
				function(data){ $('#accessories_tabs_name').html(data).delay(800).fadeIn(400); jqueryUI('#sunmenu_ass'); $('#selectItemFor').show('fast');
					if($('#cond_edit').hasClass('hidden') == false){
						$("input[name=AmountListName\\[\\]]").attr('readonly',true);$('#All_AccUse_contain img').addClass('hidden');
					}
					if(case_name=='copy_prepare'){ $('#All_AccUse_contain img').addClass('hidden'); }
				});
			}else{ return false;}
	}
}
function call_campaign(call){
	if(!document.getElementById('sunmenu_campaign')){
		preload('#tiscampaign_content_right');$.get('ic_newcusint_action.php',{call:call,condition_id:$('#condition_id_value').val(),
		cus_int_numref:$('#cusint_num_ref_value').val()},function(data){
			$('#tiscampaign_content_right').html(data).delay(800).fadeIn(400); jqueryUI('#sunmenu_campaign');
			if($('#cond_edit').hasClass('hidden') == false){$("#sunmenu_campaign input[type=checkbox]").attr('disabled','disabled');}
			if($('#cond_edit').hasClass('new_prepare')){$("#sunmenu_campaign input[type=checkbox]").attr('disabled','disabled');}
		});
	}else{ return false;}
}
function save_condition(element,case_name,id){
	$.post('ic_newcusint_action.php',(jQuery('#pro_cond_'+id).find('input,input[type=hidden],select,textarea').serialize())+'&'+case_name+'=save_condition&cus_no='+$('#edit_id').val(),function(data){alert(data); $(element).addClass('hidden'); $(element).next().removeClass('hidden');  $('.iCond').addClass('hidden'); set_none_input();
	});
}
function edit_condition(element,id,number){
	var arr = number.split("|||");
	if(confirm('ต้องการแก้ไขรายละเอียดเงื่อนไข !!') == true){
		var cusint_num = $('#cusint_num_ref_value').val();
		$.get('ic_newcusint_action.php',{condition_id:$('#condition_id_value').val(),call:'temp_condition',checkbox:arr[0],
			cusint_num_ref:cusint_num},function(data){$('#'+$(id).parent('.cond_detail').attr('id')).html(data);
			jqueryUI('#blind_cond'+$('#condition_id_value').val());$(id).tabs('select',4);
			// if(arr[1]=='approve_report'){
				// call_overlay('#overlay-prepare');
				// $('#containAll').before("<div id='will_process'><div style='text-align: center'><img src='java/img/wait1.gif'/></div><div class='fontBold textCenter' style='font-size:14px;'> will process .. <div><div>");
				// cusint_prepare(cusint_num,$('#cus_no').val(),arr[1],'edit_condition');
			// }

			// if(arr[1]=='approve_report'){
				// call_overlay('#overlay-prepare');
				// $('#containAll').before("<div id='will_process'><div style='text-align: center'><img src='java/img/wait1.gif'/></div><div class='fontBold textCenter' style='font-size:14px;'> will process .. <div><div>");
				// var prepare_num = '';
				// if($('#prepare_num_id').val()==''){ prepare_num = $('#prepare_num_ref').val(); }else{ prepare_num = $('#prepare_num_id').val();}
				// $.post('ic_newprepare_action.php',{insert:'prepare',case_name:'approve_report',int_number:$('#cusint_num_ref').val(),prepare_cus:$('#cus_no').val(),
				// int_time:$('#Interest_Time').val(),old_prepare_num:prepare_num},function(data){
					// arr = data.split('#@'); alert(arr[1]); $('#prepare_num_ref').val(arr[0]); $('#prepare_num_id').val(arr[0]);
					// $('#will_process').remove(); $('#overlay-prepare').hide();


					// $.get('ic_newprepare.php',{prepare_cus:$('#cus_no').val(),int_number:$('#cusint_num_ref').val(),case_name:'copyint_prepare',
					// prepare_num_ref:$('#prepare_num_ref').val()},
					// function(data){
						// prepare_copyold_after(data);
					// });
				// });
			 // }
		});
	}
}
function save_all_cusint(){
	if(confirm('ต้องการบันทึกข้อมูลลูกค้าสนใจ ตรวจสอบความถูกต้องแล้ว !!') == true){
		if($('img.accept').hasClass('hidden') == false){ alert('กรุณากดบันทึกรายการเงื่อนไข'); return false;}
		if($('#Int_Date').val()==''){ alert('กรุณาเลือกวันที่สนใจ'); return false;}
		
//update pt 13-08-2011  เชควันที่สนใจไม่ให้กรอกเกินวันทีปัจจุบัน
		var brokenstring=$('#Int_Date').val().split('-'); 
		var int_date = brokenstring[2]+brokenstring[1]+brokenstring[0];
		if(int_date > $('#today_date').val()){ alert('ไม่สามารถกรอกวันที่สนใจล่วงหน้าได้'); return false;}
	
		if($('#Branch_Data').val()==''){ alert('กรุณาเลือกสาขา '); return false;}
		if($('#SaleRecThisTime_temp').val()==''){ alert('กรุณากรอกข้อมูลผู้รับข้อมูลความสนใจ  '); return false;}
		if($('#Sale_Responsibility_temp').val()==''){ alert('กรุณากรอกข้อมูลผผู้รับผิดชอบลูกค้า '); return false;}

		if($('#SaleRecThisTime').val()==''){
			alert('กรุณากเลือกชื่อผู้รับข้อมูล จากรายชื่อในรายการที่ได้จากการค้นหาเมื่อพิมพ์ในช่อง ผู้รับข้อมูลความสนใจ ');
			$('#SaleRecThisTime_temp').select();
			return false;
		}//PIAG
		if($('#Sale_Responsibility').val()==''){
			alert('กรุณาเลือกชื่อผู้รับผิดชอบ จากรายชื่อในรายการที่ได้จากการค้นหาเมื่อพิมพ์ในช่อง ผู้รับผิดชอบลูกค้า');
			$('#Sale_Responsibility_temp').select();return false;
		}//PIAG

		$.post('ic_newcusint_action.php',(jQuery('#tabs3').find('*').serialize())+'&insert=save_all_cusint',function(data){
			alert(data); 
			browseCheck('main','customers.php?&page=ic_newcusint.php?&caseName=createNew','');
		});
	}
}
function save_all_prepare(){
	if(confirm('ต้องการนำเสนอเงื่อนไขตามรายการที่สร้าง ตรวจสอบความถูกต้องแล้ว !!') == true){
		if($('img.accept').hasClass('hidden') == false){ alert('กรุณากดบันทึกรายการเงื่อนไข'); return false;}
		if($('#Plantogodate').val()==''){alert('กรุณาเลือกวันที่จะออกติดตาม ');return false;}
		else if($('#Plandetail').val()==''){alert('กรุณากรอกรายละเอียดแผน '); return false;}
		$.post('ic_newprepare_action.php',(jQuery('#tabsa-3').find('*').serialize())+'&insert=save_all_prepare', function(data){
			//browseCheck('main','customers.php?&page=tracking.php?&case_name=ProposedTrack','')
			browseCheck('main','customers_management.php?toggle_id_card='+$('#toggle_sale_id').val()+'&intnum_active='+$('#intnum_active').val(),'');//การจัดการลูกค้ามุ่งหวัง naizan 2011-01-23
		});

	}
}
function approve_all_prepare(){
	if(confirm('ยืนยันผลการตรวจสอบตามเงื่อนไขนี้ ตรวจสอบความถูกต้องแล้ว !!') == true){
		if($('#c_action').hasClass('hidden')){ var has_edit = 'has_edit';  }
		if($('img.accept').hasClass('hidden') == false){ alert('กรุณากดบันทึกรายการเงื่อนไข'); return false;}
		$.post('approveConPrepare.php',(jQuery('#app_two').find('*').serialize())+'&insert=save_app_prepare&has_edit='+has_edit,
		function(data){ alert(data);
			$('#app_one').prepend("<div style='text-align:center;color:blue;font-size:20px;'><img src=images/preload.gif><br>กรุณารอสักครู่ ระบบกำลังเรียกข้อมูล</div>");
			$.get('approveConPrepare.php',{saleSessionid:$('#case_sale_idcard').val()},function(data){
				$('#atwo').hide(); $('#app_one').html(data); selectedTabPrepare(1);
				//-- 2011-01-27 # naizan redirect to การจัดการลูกค้ามุ่งหวัง
				// browseCheck('main','customers_management.php','');
				browseCheck('main','customers_management.php?toggle_id_card='+$('#toggle_sale_id').val()+'&intnum_active='+$('#intnum_active').val(),'');
			});
		});
	}
}
function save_tracking_cond(){
	if(confirm('ยืนยันการเปลี่ยนแปลงตามเงื่อนไขนี้ ตรวจสอบความถูกต้องแล้ว !!') == true){
		if($('img.accept').hasClass('hidden') == false){ alert('กรุณากดบันทึกรายการเงื่อนไข'); return false;}
		$('#preset_cond').prepend(ajax_loader); $.post('approveConPrepare.php',(jQuery('#preset_cond').find('*').serialize())+
		'&insert=save_tracking_condition&has_edit=has_edit',function(data){ var arr = data.split("@#"); alert(arr[1]);
		/*set ค่า  Con_prepare_Num เพื่อใช้ค่าไป update status ให้เป้น 3*/$('#Con_prepare_Num').val(arr[0]);$('#ajaxLoader').remove(); back_to_emp('call_track_condition'); });
	}
}
function set_none_input(){
	$("#sunmenu_campaign input[type=checkbox]").attr('disabled','disabled');$("input[name=AmountListName\\[\\]]").attr('readonly',true);
	$('#cusint_condition_content input,#othercost_content_left input,#recomend_content_left input').attr('readonly',true);
	$('#cusint_condition_content textarea,#othercost_content_left textarea,#recomend_content_left textarea').attr('readonly',true);
	$("input[name=OthCost_Type_Val\\[\\]],input[name=RecType\\[\\]],#CusRec_Relation").attr('disabled','disabled');
	$("#accessories_content_right img").addClass('hidden');
}
function cusint_prepare(int_num,cus_no,case_name,check_case){  //alert(int_num+','+cus_no+','+case_name+','+check_case);
	switch(case_name){
		case 'sup_cnew':if(confirm('ต้องการสร้างเงื่อนไขใหม่ !!') == true){ call_overlay('#overlay-prepare');
						$('#containAll').before("<div id='will_process'><div style='text-align: center'><img src='java/img/wait1.gif'/></div><div class='fontBold textCenter' style='font-size:14px;'> will process .. <div><div>");
						$.post('ic_newprepare_action.php',{insert:'prepare',case_name:case_name,int_number:int_num,prepare_cus:cus_no,
						int_time:$('#Interest_Time').val()},function(data){
							arr = data.split('#@'); alert(arr[1]); $('#prepare_num_ref').val(arr[0]); $('#prepare_num_id').val(arr[0]);
							$('#will_process').remove(); $('#overlay-prepare').hide(); reset_ele('cusint_product_content','reset_product@#new_prepare_condition');
							$('#make_condition_list,#ul_product').html(''); $('#product_temp').val('1'); selected_tree('6');
							$('#c_action').fadeOut('slow',function(){ $('#c_save_plan').fadeIn('slow');$('#not_approve').hide();
							$('#btn_appove').attr('checked',true); });
							$('#cusint_plandetail_content input,#cusint_plandetail_content textarea').removeAttr('readonly').removeAttr('disabled');
							// -- update 05-10-53
							$('#cusint_compare_area input,#cusint_compare_area textarea,#cusint_compare_area select').removeAttr('readonly').removeAttr('disabled');
							$('.btn_cusint_compare').show(); $('.btn_cusint_carexchange').show();
							$('#cusint_carexchange_area input,#cusint_carexchange_area textarea,#cusint_carexchange_area select').removeAttr('readonly')
							.removeAttr('disabled');
							// case หลังจากไปติดตามแล้วบันทึกผลและมีการเปลี่ยนแปลงเงื่อนไข
							if(check_case=='create_new'){ remove_class('prepare-foot','hidden');}
							//
						});
					} break;
		case 'sup_edit':if(confirm('ต้องการแก้ไขเงื่อนไขการนำเสนอ !!') == true){ call_overlay('#overlay-prepare');
						$('#containAll').before("<div id='will_process'><div style='text-align: center'><img src='java/img/wait1.gif'/></div><div class='fontBold textCenter' style='font-size:14px;'> will process .. <div><div>");
						$.post('ic_newprepare_action.php',{insert:'prepare',case_name:case_name,int_number:int_num,prepare_cus:cus_no,
						int_time:$('#Interest_Time').val(),old_prepare_num:$('#Con_prepare_Num').val()},function(data){
						    data = jQuery.trim(data);
							arr = data.split('#@'); alert(arr[1]);
							$('#prepare_num_ref').val(arr[0]);
							$('#prepare_num_id').val(arr[0]);
							$('#will_process').remove(); $('#overlay-prepare').hide();
							// case หลังจากไปติดตามแล้วบันทึกผลและมีการเปลี่ยนแปลงเงื่อนไข
							if(check_case=='edit_condition'){ remove_class('prepare-foot','hidden');}
							// case บันทึกผลการติดตาม 08-01-2011
							//if(check_case=='recorded'){ alert('x');}// remove_class('prepare-foot','hidden');}
							if($('#btnCallCond').val()=='แก้ไขเงื่อนไข'){  $('#summit_approve').hide(); }
							//
							$.get('ic_newprepare.php',{prepare_cus:cus_no,int_number:int_num,case_name:'copyint_prepare',prepare_num_ref:$('#prepare_num_ref').val()},
							function(data){ prepare_copyold_after(data); });
						});
					} break;
		case 'approve_report'://if(confirm('ต้องการแก้ไขเงื่อนไขการนำเสนอ !!') == true){
							var prepare_num = '';
							if($('#prepare_num_id').val()==''){ prepare_num = $('#prepare_num_ref').val(); }else{ prepare_num = $('#prepare_num_id').val();}
							$.post('ic_newprepare_action.php',{insert:'prepare',case_name:case_name,int_number:int_num,prepare_cus:cus_no,
							int_time:$('#Interest_Time').val(),old_prepare_num:prepare_num},function(data){
							    data = jQuery.trim(data);
								arr = data.split('#@'); /* alert(arr[1]) */ ;
								$('#prepare_num_ref').val(arr[0]);
								$('#prepare_num_id').val(arr[0]);
								$('#will_process').remove(); $('#overlay-prepare').hide();
								if(check_case=='edit_condition'){ remove_class('prepare-foot','hidden');}
								if($('#btnCallCond').val()=='แก้ไขเงื่อนไข'){  $('#summit_approve').hide(); }
								$.get('ic_newprepare.php',{prepare_cus:cus_no,int_number:int_num,case_name:'copyint_prepare',prepare_num_ref:$('#prepare_num_ref').val()},
								function(data){ prepare_copyold_after(data); });
							});
						//}
						break;
		case 'cnew':if(confirm('ต้องการสร้างเงื่อนไขใหม่ !!') == true){ call_overlay('#overlay-prepare'); prv_next('#containAll');
						$('#containAll').before("<div id='will_process'><div style='text-align: center'><img src='java/img/wait1.gif'/></div><div class='fontBold textCenter' style='font-size:14px;'> will process .. <div><div>");
						$('#cusint_plandetail_content input,#cusint_plandetail_content textarea,#Plantogodate,#Plandetail').removeAttr('readonly').
						removeAttr('disabled');
						$.post('ic_newprepare_action.php',{insert:'prepare',case_name:case_name,int_number:int_num,prepare_cus:cus_no,
						int_time:$('#Interest_Time').val()},function(data){	  prepare_cnew_after(data); });
					} break;
		case 'copy':if(confirm('ต้องการคัดลอกเงื่อนไข !!') == true){ call_overlay('#overlay-prepare'); prv_next('#containAll');
						$('#containAll').before("<div id='will_process'><div style='text-align: center'><img src='java/img/wait1.gif'/></div><div class='fontBold 	textCenter' style='font-size:14px;'> will process .. <div><div>");
						$('#cusint_plandetail_content input,#cusint_plandetail_content textarea,#Plantogodate,#Plandetail').removeAttr('readonly').
						removeAttr('disabled');
						$.post('ic_newprepare_action.php',{insert:'prepare',case_name:case_name,int_number:int_num,prepare_cus:cus_no,
						int_time:$('#Interest_Time').val()},function(data){
						    data = jQuery.trim(data);
						    arr = data.split('#@'); alert(arr[1]);
						    $('#prepare_num_ref').val(arr[0]);
						    $('#prepare_num_id').val(arr[0]);
							$('#will_process').remove(); $('#overlay-prepare').hide(); // show plan detail
							$.get('ic_newprepare.php',{prepare_cus:cus_no,int_number:int_num,case_name:'copyint_prepare',prepare_num_ref:$('#prepare_num_ref').val()},
							function(data){ prepare_copy_after(data); });
						});
					} break;
		default: 	if(confirm('ต้องการคัดลอกเงื่อนไข !!') == true){ call_overlay('#overlay-prepare'); prv_next('#containAll'); alert('default');
						$('#containAll').before("<div id='will_process'><div style='text-align: center'><img src='java/img/wait1.gif'/></div><div class='fontBold 	textCenter' style='font-size:14px;'> will process .. <div><div>");
						$('#cusint_plandetail_content input,#cusint_plandetail_content textarea').removeAttr('readonly').removeAttr('disabled');
						$.post('ic_newprepare_action.php',{insert:'prepare',case_name:case_name,int_number:int_num,prepare_cus:cus_no,
						int_time:$('#Interest_Time').val()},function(data){
						    data = jQuery.trim(data);
						    arr = data.split('#@');
						    alert(arr[1]);
						    $('#prepare_num_ref').val(arr[0]); $('#prepare_num_id').val(arr[0]);
							$('#will_process').remove(); $('#overlay-prepare').hide(); // show plan detail
							$.get('ic_newprepare.php',{prepare_cus:cus_no,int_number:int_num,case_name:'copyint_prepare',prepare_num_ref:$('#prepare_num_ref').val()},
							function(data){ prepare_copy_after(data); });
						});
					} break;
	}
}
function prepare_cnew_after(data){
	arr = data.split('#@'); alert(arr[1]);
	$('#prepare_num_ref').val(arr[0]); $('#prepare_num_id').val(arr[0]); $('a[rel="7"]').removeClass('hidden');
	$('#will_process').remove(); $('#overlay-prepare').hide(); $('#phtm_prepar_con').removeClass('hidden'); // show plan detail
	reset_ele('cusint_product_content','reset_product@#new_prepare_condition');$('#make_condition_list,#ul_product').html('');
	$('#product_temp').val('1'); selected_tree('7'); $('#c_action').fadeOut("slow"); $('#c_save_plan').fadeIn();
	// -- update 05-10-53
	$('#cusint_compare_area input,#cusint_compare_area textarea,#cusint_compare_area select').removeAttr('readonly').removeAttr('disabled');
	$('.btn_cusint_compare').show(); $('.btn_cusint_carexchange').show();
	$('#cusint_carexchange_area input,#cusint_carexchange_area textarea,#cusint_carexchange_area select').removeAttr('readonly')
	.removeAttr('disabled');
	//

	//$('#cusint_plandetail_content input,#cusint_plandetail_content textarea').removeAttr('readonly').removeAttr('disabled');
}
function prepare_copy_after(data){
	$('#phtm_prepar_con').removeClass('hidden'); $('a[rel="7"]').removeClass('hidden');
	$('#cusint_product_content select,#cusint_product_content input,#cusint_product_content textarea').removeAttr('readonly')
	.removeAttr('disabled');
	$('#make_condition_list').html('');
	$('#make_condition_list').html(data);
	element = $('.active_cond');id = element.attr('id').split('condition_temp')[1];
	make_condition(id.split('_')[0],element.val(),id,'copy_prepare',document.getElementById(element.attr('id')));
	selected_tree('7'); $('#c_action').fadeOut("slow"); $('#action_car').removeClass('hidden'); $('#c_save_plan').fadeIn();

	// -- update 05-10-53
	$('#cusint_compare_area input,#cusint_compare_area textarea,#cusint_compare_area select').removeAttr('readonly').removeAttr('disabled');
	$('.btn_cusint_compare').show(); $('.btn_cusint_carexchange').show();
	$('#cusint_carexchange_area input,#cusint_carexchange_area textarea,#cusint_carexchange_area select').removeAttr('readonly')
	.removeAttr('disabled');
	//

	//$('#cusint_plandetail_content input,#cusint_plandetail_content textarea').removeAttr('readonly').removeAttr('disabled');
}
function prepare_copyold_after(data){
	$('#cusint_product_content select,#cusint_product_content input,#cusint_product_content textarea').removeAttr('readonly')
	.removeAttr('disabled');

	$('#make_condition_list').html('');
	$('#make_condition_list').html(data);
	element = $('.active_cond');id = element.attr('id').split('condition_temp')[1];
	make_condition(id.split('_')[0],element.val(),id,'copy_prepare',document.getElementById(element.attr('id')));
	selected_tree('6'); $('#c_action').fadeOut('slow',function(){ $('#c_save_plan').fadeIn('slow');$('#not_approve').hide();
	$('#btn_appove').attr('checked',true); }); $('#action_car').show(); // show add or reset product button
	$('#cusint_plandetail_content input,#cusint_plandetail_content textarea').removeAttr('readonly').removeAttr('disabled');
	// -- update 05-10-53
	$('#cusint_compare_area input,#cusint_compare_area textarea,#cusint_compare_area select').removeAttr('readonly').removeAttr('disabled');
	$('.btn_cusint_compare').show(); $('.btn_cusint_carexchange').show();
	$('#cusint_carexchange_area input,#cusint_carexchange_area textarea,#cusint_carexchange_area select').removeAttr('readonly')
	.removeAttr('disabled');
	//
}
function tracking_record(){
	jDate("#Contact_Date");jDate("#Contact_ReturnDate");auto_com('ic_autocomplete.php?event=emp','#Contact_Person_temp','#Contact_Person','');
	radio_set("#contact_type_value");radio_set("#contact_result");radio_set("#contact_conresult");
}
function call_compare(){
	if(!document.getElementById('cusint_compare')){
		$.get('tracking.php',{check_result:'call_compare'},function(data){
			$('#tbl_trackrec').hide();$("#competing_brands").show('clip').html(data); $('.competing_brands').show('clip');
		});
	}else{
		$('#tbl_trackrec').hide();$("#competing_brands").show('clip'); $('.competing_brands').show('clip');
	}
}
function call_compare2(id_value){ //  หน้า Sale บันทึกผลการติดตาม
	if(!document.getElementById('cusint_compare')){
		$.get('tracking.php',{check_result:'call_compare',id_value:id_value},function(data){
			$('#tbl_trackrec').hide();$('#tracking_record_content').hide();
			$("#competing_brands").show('clip').html(data); $('.competing_brands').show('clip');
		});
	}else{
		$('#tbl_trackrec').hide();$('#tracking_record_content').hide();
		$("#competing_brands").show('clip'); $('.competing_brands').show('clip');
	}
}
function save_track_record(cus_number,int_number,int_time){
	if(confirm('คุณต้องการบันทึกข้อมูลการติดตาม ตรวจสอบข้อมูลถูกต้องแล้ว')==true){
		if($('#Contact_Content').val() == ''){ alert('กรุณากรอกรายละเอียดผลการติดตาม'); return false; }
		if($('#Contact_Person_temp').val() == ''){alert('กรุณากรอกรายละเอียดชื่อผู้ติดตาม'); return false;}
		if($('#Contact_Date').val() == ''){alert('กรุณากรอกรายละเอียดวันที่ออกติดตาม');return false;}
		if($('#Contact_ReturnDate').val() == ''){ alert('กรุณากรอกรายละเอียดวันที่กลับมา');return false;}
		//alert('save_tracking_cond()');
		//if(confirm('ยืนยันการเปลี่ยนแปลงตามเงื่อนไขนี้ ตรวจสอบความถูกต้องแล้ว !!') == true){
			if($('img.accept').hasClass('hidden') == false){ alert('กรุณากดบันทึกรายการเงื่อนไข'); return false;}
			$('#preset_cond').prepend(ajax_loader); $.post('approveConPrepare.php',(jQuery('#preset_cond').find('*').serialize())+
			'&insert=save_tracking_condition&has_edit=has_edit',function(data){ var arr = data.split("@#"); //alert(arr[1]);
			/*set ค่า  Con_prepare_Num เพื่อใช้ค่าไป update status ให้เป้น 3*/$('#Con_prepare_Num').val(arr[0]);$('#ajaxLoader').remove(); //back_to_emp('call_track_condition');
				
			//pt@2011-12-14
			lose_competition_date = $('#lose_competition_date').val();
			
			// update 08-01-2010  //pt@2011-12-14  add +'&lose_competition_date='+lose_competition_date
				$.post('tracking.php',(jQuery('#tabsa-5').find('*').serialize())+'&insert=save_track_record&int_number='+int_number+'&int_time='+int_time+'&lose_competition_date='+lose_competition_date,
				function(data){ selectedTab(3);$('#cfour').hide(); alert(data);
					browseCheck('box_three','tracking.php?PrepareHis='+cus_number+'&intNo='+int_number+'&check=TrackRecord&TrackintTime='+int_time,'');
					//-- 2011-01-27 # naizan redirect to การจัดการลูกค้ามุ่งหวัง
					browseCheck('main','customers_management.php?toggle_id_card='+$('#toggle_sale_id').val()+'&intnum_active='+$('#intnum_active').val(),'');
				});
			});
		//}
	}
}
function get_prepare_list(int_number,cus_number,case_name){
	$.get('list_conprepare.php',{int_number:int_number,cus_number:cus_number,list_case:'prepare_list',case_name:case_name},function(data){
		$('#prepare_list').html(data); $('#tb2').show(); $('#all_num_rows').val($('#pre_num_all').val()); $('#tabs').tabs('option', 'selected', 1);
	});
}
function show_prepare_cond(prepare_no,int_num,cus_no,plan_person){
	$('#tabs-2').prepend("<div id='show_preload' style='text-align:center;color:blue;font-size:20px;'><img src=images/preload.gif><br>กรุณารอสักครู่ ระบบกำลังเรียกข้อมูล</div>");
	$.get('approveConPrepare.php',{prepare_cus:cus_no,int_number:int_num,plan_person:plan_person,prepare_no:prepare_no,case_name:'new_prepare',access:'readonly'},
	function(data){
		$('#tabs-3').html(data); $('#tb3').show(); $('#tabs').tabs('option', 'selected', 2); $('#containAll').tabs('select',5);
		$('#preset_cond input,#preset_cond textarea').attr('readonly',true);$('#preset_cond select,#preset_cond checkbox').attr('disabled','disabled');
		$('#preset_cond .question_list,#preset_cond .hasDatepicker').attr('disabled','disabled');
		element = $('.active_cond');
		id = element.attr('id').split('condition_temp')[1]; $('#show_preload').remove();
		//make_condition(temp_product_num,condition_id,checkbox,case_name,thishtm)
		make_condition(id.split('_')[0],element.val(),id,'new_prepare',document.getElementById(element.attr('id')));
		if($('#btn_status').val()!=1){ $('#c_action').hide(); } $('#int_num').val(int_num);$('#edit_id').val(cus_no);
	});
}
function show_report_cond(prepare_no,int_num,cus_no,plan_person){
	$('#tabs-2').prepend("<div id='show_preload' style='text-align:center;color:blue;font-size:20px;'><img src=images/preload.gif><br>กรุณารอสักครู่ ระบบกำลังเรียกข้อมูล</div>");
	$.get('approveConPrepare.php',{prepare_cus:cus_no,int_number:int_num,plan_person:plan_person,prepare_no:prepare_no,case_name:'new_prepare',access:'readonly',show_cond:'show_report_cond'},
	function(data){
		$('#tabs-3').html(data); $('#tb3').show(); $('#tabs').tabs('option', 'selected', 2); $('#containAll').tabs('select',5);
		$('#preset_cond input,#preset_cond textarea').attr('readonly',true);$('#preset_cond select,#preset_cond checkbox').attr('disabled','disabled');
		$('#preset_cond .question_list,#preset_cond .hasDatepicker').attr('disabled','disabled'); element = $('.active_cond');
		id = element.attr('id').split('condition_temp')[1]; $('#show_preload').remove();
		//make_condition(temp_product_num,condition_id,checkbox,case_name,thishtm)
		make_condition(id.split('_')[0],element.val(),id,'new_prepare',document.getElementById(element.attr('id')));
		if($('#btn_status').val()!=1){ $('#c_action').hide(); } $('#int_num').val(int_num);$('#edit_id').val(cus_no);
	});
}
/* EDIT WITH IC VER 0.25 */
function event_search_date(){
	$(function() {
		var dates = $('#from, #to').datepicker({
			defaultDate: "+1w",changeMonth: true,changeYear: true,numberOfMonths: 1,dateFormat : 'dd-mm-yy',
			onSelect: function(selectedDate) {
				var option = this.id == "from" ? "minDate" : "maxDate";var instance = $(this).data("datepicker");
				var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
				dates.not(this).datepicker("option", option, date);
			}
		});
	});
}
function open_export(condition){
		switch (condition) {
			case "booking": case_name = '&str_date='+$('#from').val()+'&stp_date='+$('#to').val()+'&define_date='+$('#define_date').val()+
							'&all_status='+$("input[name='all_status']:checked").val(); 				break;
			case "saleremark": case_name = '&str_date='+$('#from').val()+'&stp_date='+$('#to').val()+'&define_date='+$('#define_date').val()+
							'&all_status='+$("input[name='all_status']:checked").val(); 				break;
			case "pre_booking": case_name = '&str_date='+$('#from').val()+'&stp_date='+$('#to').val();  break; // 20-08-53
			case "sell_cr": case_name = '&str_date='+$('#from').val()+'&stp_date='+$('#to').val()+'&after_the_sale='+$('#after_the_sale').val();  break; // 20-08-53
		}
		window.open('ic_exel.php?export='+condition+case_name,'newwindow','height=50,width=50,top=0,left=0,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes,directories=no,status=no,titlebar=no');
}
function refresh_div(div){$(div+' :input').val('');}
function call_toggle(div){$(div).slideToggle();}
function accordion_fill(divname){$(function() {$(divname).accordion({autoHeight: false, header: "h3",fillSpace:true});});}
function checked_all_byname(names,case_name,target,css_proper){
	//checked_all_byname('prebook_id\\[\\]','checked','.sum_model','hilight_rows')
	$("input[name='"+ names +"']:checkbox").attr("checked",case_name);
	if(case_name=='checked'){$(target).addClass(css_proper);}else{ $(target).removeClass(css_proper);}
}
function checked_model_code(names,checked,target,css_proper,case_name){
	if(case_name=='check_all'){
		if(document.getElementById('control_checked').checked==true){
			$("input[name='"+ names +"']:checkbox").attr("checked",checked); $(target).addClass(css_proper);
		}else{
			$("input[name='"+ names +"']:checkbox").removeAttr("checked");$(target).removeClass(css_proper);
		}
	}else{
		if(names.checked==true){$(target).addClass(css_proper);}else{ $(target).removeClass(css_proper);}
	}
}
function hilight_rows(this_val,names,css_proper,case_name){if(this_val.checked==true){$(names).addClass(css_proper);}else{ $(names).removeClass(css_proper);}}
function search_stock(case_name){
	preload('#scroll_list'); var param = '';
	if(case_name != 'all'){ param = jQuery("#frm_search_stock").find('input, textarea, select').serialize()+'&';}else{ param = 'case_name='+case_name+'&'; }
	$.get("ic_all_action.php",param+'search=car_stock',function(data){
		$('#scroll_list').html(data); $('#pre_book_list_num').val($("input[name='stock_num']").val());
	});
}
function search_book_queue(case_name){
	if($("#queue_order_by").val()==''){
		alert('กรุณาเลือกรูปแบบการจัดเรียงก่อนครับ');
		return false;
	}
	preload('#scroll_list');
	var param = ''; 
	if(case_name != 'all'){ 
		param = jQuery("#frm_search_book").find('input, textarea, select').serialize()+'&';
	}else{
		param = 'order_by='+$("#queue_order_by").val()+'&';
	}
	$.get("ic_all_action.php",param+'search=book_queue',function(data){ $('#scroll_list').html(data);});
}

function prebook_process(eng_no,chass_no,car_od,car_id){
	/* if(confirm('ต้องการจองรถ   (เลขเครื่อง  '+ eng_no+ ')  นี้ไว้ก่อนหรือไม่  ?') == true){ */
	if(eng_no!=''){ car_value = '< เลขเครื่อง   '+eng_no+'>'; }else{ car_value = '< เลข OD '+car_od+'>'; }
	if(confirm('ต้องการจองรถ   '+car_value+'  นี้ไว้ก่อนหรือไม่  ?') == true){
		$.post('ic_all_action.php',{show:'pre_book_popup',chass_no:chass_no,eng_no:eng_no,car_id:car_id},function(data){
			$("#car-stock-overlay").show().addClass('ui-widget-overlay').css({width:"100%" ,height:$(document).height(),opacity:.3});
			$("#car-stock").show('clip').html(data); $('.car-stock').show('clip');
		});
	}
}
function prebook_info(case_name,val){
	switch(case_name){
		case 'close_switch_car':
			$('#car-stock').hide().html('');$('#car-stock-overlay').hide();$('#prebook-id-'+val).attr('checked','');$('#TRLIST-'+val).removeClass('hilight_rows');
			break;
		case 'close':
			$('#car-stock').hide().html(''); $('#car-stock-overlay').hide();
			break;
		case 'ok':
			if($('#emp_prebook').val()==''){ alert('กรุณาใส่ชื่อพนักงานที่ต้องการจอง  !!');  return false;}
			$.get('ic_all_action.php',{search:'employee',emp_id:$('#emp_prebook').val().split('_')[0]},function(data){
				if(data.trim()=='not_found'){alert('ไม่พบข้อมูลพนักงานในระบบ กรุณาตรวจสอบ !! '); return false;}
				$.post('ic_all_action.php',(jQuery("#pre_book_info").find('input, textarea, select').serialize())+'&edit=pre_book_popup',function(data){
					alert(data); search_stock('all'); $('#car-stock').hide().html(''); $('#car-stock-overlay').hide();
				});
			});

			break;
	}
}
/**
 * ซ่อนแสดงกล่องข้อความ เพื่อระบุหมายเหตุการยกเลิก pre_book
 * @param Object obj ชื่อเช็กบ็อกที่ใช้เลือก
 * @param Integer pre_id รหัส pre_book
 * @return ซ่อนแสดงช่องระบุเหตุผล และเปลี่ยนคลาสเป็นเช็ก/ไม่เช็กค่าว่าง
 */
function change_prebook_remark(obj,pre_id,all){
	if(all){
		if(all=='selectAll'){
			$('.tr_pre_remark').show();
			$('input.prebook_remark').removeClass('nocheck');
		}else if(all=='unSelectAll'){
			$('.tr_pre_remark').hide();
			$('input.prebook_remark').addClass('nocheck');
		}
	}else{
		if(obj.checked==true){
			$('#trlist_'+pre_id).show();
			$('#trlist_'+pre_id+' input.prebook_remark').removeClass('nocheck');
		}else{
			$('#trlist_'+pre_id).hide();
			$('#trlist_'+pre_id+' input.prebook_remark').addClass('nocheck');
		}
	}
}

function del_prebook(c_name){
	var data_set = '',cancel_remark='';
	if(c_name=='all'){
		data_set = $("#tbreport_new").find('input:checkbox').serialize();
		$('.prebook_remark').each(function(){//naizan 2011-05-12
			if(this.value!=''){cancel_remark += '&'+this.name+'='+this.value;}
		});
		//เช็กก่อนว่ามีการระบุเหตุผลหรือไม่
		if(nzCheckForm({errorMsg:'ระบุเหตุผลการยกเลิก',noAlert:true})==false){
			cancel_remark = '';
		}
	}else{
		$('#prebook-id-'+c_name).attr('checked','checked');
		$('#TRLIST-'+c_name).addClass('hilight_rows');
		data_set = 'prebook_id[]='+c_name;
		//--- naizan 2011-05-12
		var remark = $('#txt_remark_'+c_name).val();
		if(remark){
			cancel_remark = 'remark['+c_name+']='+ remark;
		}
		//-
	}
	if(cancel_remark!=''){//naizan 2011-05-12
		if(confirm('ต้องการยกเลิกรายการ Pre Booking ตรวจสอบถูกต้องแล้ว !!') == true){
			$.post('ic_all_action.php',data_set+'&delete=pre_booking_data&'+cancel_remark ,function(data){
				alert(data);browseCheck('main','ic_manage_prebooking.php','');
			});
			$('#nz_popup_div').hide();
		}
	}else{//naizan 2011-05-12
		alert('กรุณาระบุเหตุผลในการขอยกเลิกด้วยครับ!');
	}
}
function prebook_switch_car(c_name,val){
	if(confirm('ต้องการสลับรถในสต๊อค ?') == true){
		$('#prebook-id-'+val).attr('checked','checked'); $('#TRLIST-'+val).addClass('hilight_rows');
		if(c_name == 'ok'){
			$.post('ic_all_action.php',{edit:'prebook_switch_car',switch_id:val,MV_Eng_No:$('#MV_Eng_No').val()},function(data){
				$('#car-stock').hide().html(''); $('#car-stock-overlay').hide(); data = jQuery.trim(data);
				if(data=='have_data'){
					alert(" ++ สลับรถสำเร็จ ++ "); browseCheck('main','ic_manage_prebooking.php','');
				}else if(data=='have_book'){
					alert(" เลขเครื่องนี้ถูกจองแล้ว ไม่สามารถสลับได้ !! ");
				}else if(data=='no_engno'){
					alert(" ไม่มีเลขเครื่องนี้ในสต็อค !! ");
				}
			});
		}else{
			$.post('ic_all_action.php',{show:'prebook_switch_car',switch_id:val},function(data){
				$("#car-stock-overlay").show().addClass('ui-widget-overlay').css({width:"100%" ,height:$(document).height(),opacity:.3});
				$("#car-stock").show('clip').html(data,function(){$('#MV_Eng_No').focus();});$('.car-stock').show('clip');
			});
		}
	}
}

function open_remark_history(url,name,windowWidth,windowHeight,id){
	mytop = ''; myleft = '';
	properties = "'resizable=no,menubar=no,status=yes,toolbar=no,location=no,width="+windowWidth+",height="+windowHeight+",top="+mytop+",left="+myleft;
	window.open(url,name,properties);
}
function back_page(page,case_name){
	switch(case_name){
		case 'active_3':
			$.post(page,function(data){ $('#main').html(data);});
		break;
	}
}
function disable_volume(int_number,cus_info,element){
	
	$.post('ic_all_action','int_number='+int_number+'&delete=chk_bookingvolume',function(data2){
		var chkk = data2;
		if(chkk != 0){
			if(confirm('ยืนยันการยกเลิกจำนวนรถที่สนใจจองที่เหลือทั้งหมดของ '+cus_info+' ') == true){
				$.post('ic_all_action','int_number='+int_number+'&delete=disable_buyvolume',function(data){ alert(data);
				browseCheck('main','cheackStockCus.php?case_name=reservation_car','');
				});
			}else{element.checked = false;}
		}else{ alert('ไม่สามารถยกเลิกจำนวนรถที่สนใจจองนี้ได้ \nเนื่องจากท่านต้องทำรายการจองอย่างน้อย 1 คัน');
			element.checked = false;
		}	
	});
}

function cond_before_book(int_number){ 
	$.get('carBooking.php',{condition:'before_book',int_number:int_number,case_name:'new_prepare'},function(data){
		$('#cusint_product_area').remove(); $('#BookInfo').before(data).delay('800'); $('#cusint_product_area input,textarea').attr('readonly',true);
		$('#cusint_product_area select,checkbox').attr('disabled','disabled'); $('#cusint_num_ref').val(int_number);
		element = $('.active_cond'); id = element.attr('id').split('condition_temp')[1]; $('#condition_num_ref').val(element.val());
		
		// last update 29-10-2554  เมนูบันทึกการจอง รถ เมื่อ เลือก รายการที่จะจอง  จะแสดงเงื่อนไขก่อน จอง เอา  icon ลบ  ออก  ตอนแรก hide ไว้above
		// c แต่สามารถลบได้อยู่  ทำให้เงื่อนไขอาจหายไปได้ก่อนลบข้อมูล   by.k
		$('#del_producttemp_book,#more_condition_book').remove();
		$('#edit_temp_book').removeAttr("onclick");

		// end
		
		//var condition_id = make_condition
		make_condition(id.split('_')[0],element.val(),id,'new_prepare',document.getElementById(element.attr('id')));
		$('#int_num').val(int_number); $("input[name='condition_temp\\[\\]']").attr('disabled','disabled'); //$('#edit_id').val(cus_no);
		//if(condition_id){ call_accessories('new_prepare'); call_campaign('call_edit_campaign');}
	});
}




/* version 0.35 */
function search_del_stock(case_name){ preload('#scroll_list');
	var param = ''; if(case_name != 'all'){ param = jQuery("#frm_search_stock").find('input, textarea, select').serialize()+'&';}
	$.get("ic_all_action.php",param+'search=del_car_stock',function(data){
		$('#scroll_list').html(data); $('#pre_book_list_num').val($("input[name='stock_num']").val());
	});
}
function delete_stock(eng_no,od,id,case_name){
	switch(case_name){
		case 'show_popup':
			if(eng_no==''){value = od; text = 'OD'; }else{ value = eng_no; text = 'เลขเครื่อง';}
			if(confirm('ต้องการลบ   ('+text+'  '+ value+'  ) ออกจากสต๊อค  ?') == true){
				$.post('ic_all_action.php',{show:'delete_stock_popup',eng_no:eng_no,od:od,car_id:id},function(data){
					$("#car-stock-overlay").show().addClass('ui-widget-overlay').css({width:"100%" ,height:$(document).height(),opacity:.3});
					$("#car-stock").show('clip').html(data); $('.car-stock').show('clip');
				});
			}
		break;
		case 'confirm':
			if(confirm('ตรวจสอบข้อมูลถูกต้องแล้ว  ?') == true){
				$.get('ic_all_action.php','delete=delete_car_stock&eng_no='+eng_no+'&car_od='+od+'&stock_id='+id+'&cancle_reason='+
				$('#cancle_reason').val(),function(data){
					if(data){ $('#car-stock').hide().html(''); $('#car-stock-overlay').hide();alert(data);search_del_stock('all');}
				});
			}
		break;
		case 'close':
			$('#car-stock').hide().html(''); $('#car-stock-overlay').hide();
		break;
	}
}
function search_manage_car(case_name){ preload('#scroll_list');
	var param = ''; if(case_name != 'all'){ param = jQuery("#frm_search_stock").find('input, textarea, select').serialize()+'&';}
	$.get("ic_all_action.php",param+'search=manage_car_stock',function(data){
		var number = $("input[name='stock_num']").val();if(number==undefined){ number = '0';}
		$('#scroll_list').html(data); $('#car_stock_list_num').val(number);
	});
}
function manage_car_enable(id,case_name,button_case){
	if(confirm('ต้องการปล่อยรถเข้าในสต๊อค    ?') == true){
		$('#prebook-id-'+id).attr('checked','checked'); $('#TRLIST-'+id).addClass('hilight_rows');
		if(button_case=='all'){data_set = $("#scroll_list").find('input:checkbox').serialize();}else{data_set = 'prebook_id[]='+id;}
		$.post('ic_all_action.php',data_set+'&edit=manage_car_enable&case_name='+case_name,function(data){
			alert(data); search_manage_car('all');
		});
	}
}


/* ---------------------------------------------------------------------------------------------*/
function open_exel(target,condition){
	switch (condition){
		case "int_per_occupation": case_name = '&request=cusint&term=search&num_year='+$('#year').val()+'&car_type='+$('#car_type').val()+'&car_class='+$('#car_class').val()+'&car_model='+$('#car_model').val();
		break;
		case "int_per_type": case_name = '&request=cusint&term=search&num_year='+$('#year').val()+'&car_type='+$('#car_type').val()+'&car_class='+$('#car_class').val()+'&car_model='+$('#car_model').val();
		break;
		case "int_per_date": case_name = '&request=cusint&term=search&num_year='+$('#year').val()+'&car_type='+$('#car_type').val()+'&car_class='+$('#car_class').val()+'&car_model='+$('#car_model').val();
		break;
		case "sell_per_occupation": case_name = '&request=sell&term=search&num_year='+$('#year').val()+'&car_type='+$('#car_type').val()+'&car_class='+$('#car_class').val()+'&car_model='+$('#car_model').val();
		break;
		case "sell_per_date": case_name = '&request=sell&term=search&num_year='+$('#year').val()+'&car_type='+$('#car_type').val()+'&car_class='+$('#car_class').val()+'&car_model='+$('#car_model').val();
		break;
		case "sell_purchase_type": case_name = '&request=sell&term=search&num_year='+$('#year').val()+'&car_type='+$('#car_type').val()+'&car_class='+$('#car_class').val()+'&car_model='+$('#car_model').val();
		break;
		case "sell_area_amphur": case_name = '&request=sell&term=search&num_year='+$('#year').val()+'&branch_id='+$('#branch_id').val()+'&province='+$('#province').val()+'&amphur='+$('#amphur').val();
		break;
		case "sell_area_tumbon": case_name = '&request=sell&term=search&num_year='+$('#year').val()+'&branch_id='+$('#branch_id').val()+'&province='+$('#province').val()+'&amphur='+$('#amphur').val();
		break;
		case "sell_per_intType": case_name = '&request=cusint&term=search&num_year='+$('#year').val()+'&car_type='+$('#car_type').val()+'&car_class='+$('#car_class').val()+'&car_model='+$('#car_model').val();  //naizan 2010-11-30
		break;
	}
	window.open(target+'?export='+condition+case_name,'newwindow','height=50,width=50,top=0,left=0,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes,directories=no,status=no,titlebar=no');
}


function add_class(ele_name,class_name){$('.'+ele_name).toggleClass(class_name);}
//function reload_data(case_name){
function reload_data(case_name,subcase){//naizan 2010-11-27
	switch (case_name){
		case "int_per_type":
			if(confirm('ต้องการอัพเดทข้อมูลใหม่  ?') == true){
				$('#main_content').prepend("<div id='show_preload' style='text-align:center;color:blue;font-size:20px;'><img src=images/preload.gif><br>กรุณารอสักครู่ ระบบกำลังเรียกข้อมูล</div>");
				$.get('ic_all_action.php',{show:case_name},function(data){
					$('#main_content').remove();
					alert('อัพเดทข้อมูลสำเร็จ');
					browseCheck('main','report_cusint_inttype.php?request=cusint&term=search','');
				});
			}
		break;
		case "sell_area_amphur":
			if(confirm('ต้องการอัพเดทข้อมูลใหม่  ?') == true){
				$('#main_content').prepend("<div id='show_preload' style='text-align:center;color:blue;font-size:20px;'><img src=images/preload.gif><br>กรุณารอสักครู่ ระบบกำลังเรียกข้อมูล</div>");
				$.get('ic_all_action.php',{show:case_name},function(data){
					$('#main_content').remove();
					alert('อัพเดทข้อมูลสำเร็จ');
					if(subcase=="tumbol"){//naizan 2010-12-01
						browseCheck('main','report_sell_area_tumbon.php?request=sell&term=search','');
					}else{
						browseCheck('main','report_sell_area_amphur.php?request=sell&term=search','');
					}
				});
			}
		break;
	}
}

//pt2011-11-12
function reason_book_cancle(type){
	switch(type){
		case 'competitor':
			$('#reason_other').html(''); $('#topic_reason').html('หมายเหตุ');		
			browseCheck('reason_compare','carAcc.php?reasonCompetitor=1','');
		break;
		case 'other':
			$('#reason_compare').html('<table width="400px"></table> ');
			$('#topic_reason').html('เหตุผลที่ยกเลิก');
		break;
	}
}
//modify pt2011-11-12
function cancle_book(book_no,sale_bookno,int_number,chass_no,start_month,book_model,case_switch){
	switch(case_switch){
		case 'show':
			$('#book_no').val(book_no);$('#book_carmodel').val(book_model); $('#sale_bookno').val(sale_bookno);
			$('#int_number').val(int_number);$('#chass_no').val(chass_no);  $('#start_month').val(start_month);
			
			$('#cancel_reason').val(''); $('.cancle_reason').show();
			$('.tb_compare_f').hide();
			window.scrollTo(0,100);//NZ 2011-05-21
		break;
		case 'hide':
			$('.cancle_reason').hide();
		break;
		case 'confirm':
			var book_no=$('#book_no').val();
			
			var select_CompetitorName=$('#select_CompetitorName').val();
			var select_CompetitorNickName=$('#select_CompetitorNickName').val();
			var Compare_GModelName=$('#Compare_GModelName').val();
			var Compare_Color=$('#Compare_Color').val();
			var Compare_OpenPrice=$('#Compare_OpenPrice').val();
			var Compare_Discount=$('#Compare_Discount').val();
			var Compare_CurrentPrice=$('#Compare_CurrentPrice').val();
			var Compare_Sub_UsedCar=$('#Compare_Sub_UsedCar').val();
			var Compare_AccessoryCost=$('#Compare_AccessoryCost').val();
			var Compare_AccessoryRemark=$('#Compare_AccessoryRemark').val();
			var Compare_Sell_Type=$('#Compare_Sell_Type').val();
			var Compare_Down=$('#Compare_Down').val();
			var Compare_Sub_Down=$('#Compare_Sub_Down').val();
			var Compare_Int=$('#Compare_Int').val();
			var Compare_Time=$('#Compare_Time').val();
			var Compare_Installment=$('#Compare_Installment').val();
			var Compare_Total_Budget=$('#Compare_Total_Budget').val();
			var Compare_Salename=$('#Compare_Salename').val();
			var Compare_Position=$('#Compare_Position').val();
			var Compare_Tel=$('#Compare_Tel').val();
			var cancel_reason=$('#cancel_reason').val();
				 
			if(document.getElementById('radio_competitor').checked == true){
				if (confirm('ต้องการยกเลิกรายการจองเลขที่   '+book_no+'    !!  ')){
					
				param  = '&select_CompetitorName='+select_CompetitorName+'&select_CompetitorNickName='+select_CompetitorNickName+'&Compare_GModelName='+Compare_GModelName+'&Compare_Color='+Compare_Color;
				param += '&Compare_OpenPrice='+Compare_OpenPrice+'&Compare_Discount='+Compare_Discount+'&Compare_CurrentPrice='+Compare_CurrentPrice+'&Compare_Sub_UsedCar='+Compare_Sub_UsedCar;
				param += '&Compare_AccessoryCost='+Compare_AccessoryCost+'&Compare_AccessoryRemark='+Compare_AccessoryRemark+'&Compare_Sell_Type='+Compare_Sell_Type+'&Compare_Down='+Compare_Down;
				param += '&Compare_Sub_Down='+Compare_Sub_Down+'&Compare_Int='+Compare_Int+'&Compare_Time='+Compare_Time+'&Compare_Installment='+Compare_Installment;
				param += '&Compare_Total_Budget='+Compare_Total_Budget+'&Compare_Salename='+Compare_Salename+'&Compare_Position='+Compare_Position+'&Compare_Tel='+Compare_Tel;
				
				//if($('#cancel_reason').val()==''){  alert(' ++ กรุณาระบุเหตุผลที่ยกเลิก   ++  ');  return false;}
				$.post('carAcc.php?Delete_with_compet=1&Delete='+book_no+'&MemberEdit='+$('#sale_bookno').val()+'&B_Int_Num='+$('#int_number').val()+'&MV_Chass_No='+$('#chass_no').val()+'&start_month='+$('#start_month').val()+'&cancel_reason='+cancel_reason,param,function(data){
					hideHeadEditbOOK('hide','containHead');
					$("#main").html(data);
				});
				//browseCheck('main','carAcc.php?Delete_with_compet=1&Delete='+book_no+'&MemberEdit='+$('#sale_bookno').val()+'&B_Int_Num='+$('#int_number').val()+'&MV_Chass_No='+$('#chass_no').val()+'&start_month='+$('#start_month').val()+param,'');
				}
			}else{
				if (confirm('ต้องการยกเลิกรายการจองเลขที่   '+book_no+'    !!  ')){
					if(cancel_reason==''){  alert(' ++ กรุณาระบุเหตุผลที่ยกเลิก   ++  ');  return false;}
					
					hideHeadEditbOOK('hide','containHead');
					browseCheck('main','carAcc.php?Delete='+book_no+'&MemberEdit='+$('#sale_bookno').val()+'&B_Int_Num='+$('#int_number').val()+'&MV_Chass_No='+$('#chass_no').val()+'&start_month='+$('#start_month').val()+'&cancel_reason='+$('#cancel_reason').val(),'');
				}
			}
		break;
	}
}

function show_cancle_book(page,para){
	var OverlayObj="<div class='overlay'></div><div class='alertBox' id='alertBox'></div>";$(document.body).prepend(OverlayObj);
	$("div.overlay").css({position:"absolute",top:0,left:0,background:"#000",width:"100%",height:$(document).height(),   opacity:.7			});
	$("div.alertBox").animate(500).html("<div><a onclick=\"$('div.overlay,.alertBox').fadeOut('fast')\" class=\"boxcloseModel\" id=\"boxclose\"></a><table align=\"center\" style='padding-top:20px;'><tr><td><label for=\"from\">จากวันที่</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"text\" id=\"from\" name=\"from\"/></td><td><label for=\"to\">ถึงวันที่</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"text\" id=\"to\" name=\"to\"/></td><td>&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' id='search_cancle' value='ค้นหา'/></td></tr></table><div align='right' style='margin-top:50px;margin-right:30px;'><input type='button' id='cancle_all' value='แสดงทั้งหมด'/></div>");
	nzShowCenter('alertBox');
	var dates = $( "#from, #to" ).datepicker({
		defaultDate: "+1w",changeMonth:true,numberOfMonths: 1,dateFormat : 'dd-mm-yy',
		onSelect: function(selectedDate){
			var option = this.id == "from" ? "minDate" : "maxDate",instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(instance.settings.dateFormat ||$.datepicker._defaults.dateFormat,selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		}
	});
	$("#search_cancle").bind('click',function(){
		$('div.overlay,.alertBox').fadeOut('fast')
		browseCheck('main',page+'?page_name='+page+'&booking_cancel=list_all&'+para+'&from='+$("#from").val()+'&to='+$("#to").val(),'');
	});
	$("#cancle_all").bind('click',function(){
		$('div.overlay,.alertBox').fadeOut('fast')
		browseCheck('main',page+'?page_name='+page+'&booking_cancel=list_all&'+para,'');
	});
}
function uibutton(){$(".uibutton").button();}
function uitab(id){$(id).tabs();}
function save_bind_color(){
	$.post('color_bind_modelcar.php',$("#box-table-a").find('input,input:checkbox').serialize()+'&operation=save_data',function(data){
		alert('       บันทึกข้อมูลเรียบร้อย   +++ ');
		browseCheck('main','color_bind_modelcar.php?car_type='+$('#car_type').val()+'&car_class='+$('#car_class').val()+'&car_model='+$('#car_model').val(),'');
	});
}
function close_dialog(){$("#popup_overlay").addClass("displayNone");$("#divShowForm").addClass("displayNone");}
function call_dialog(url,showFormDiv){
	$("#divShowForm").draggable();
	browseCheck(showFormDiv,url,'');
	$("#popup_overlay").removeClass("displayNone");
	$("#divShowForm").removeClass("displayNone");
}
function can_edit_able(target,id,case_name){
	var id_name = 'CARCARD_INPUT_'+id;
	if(case_name=='fill'){
		$(target).show();  $('#CARD_CON_'+id).removeAttr('onclick');
		$(target).html('<input type="text" id="'+id_name+'" onblur="can_edit_able(\''+target+'\',\''+id+'\',\'save\')" style="width:50px" name="CARCARD_INPUT[]" value="'+$('#CARCARD_VAL_'+id).val()+'" />'); //onkeyup="can_edit_able(\''+target+'\',\''+id+'\',\'save\')"
	}else if(case_name=='save'){
		//$(target).hide(); //alert($('#'+id_name).val());
		var value = $('#'+id_name).val();
		$.get('ic_prebook.php',{car_id:id,carcard_number:value},function(respond){
			if(respond=='success'){
				$(target).fadeIn('slow',function(){
					$('#CARCARD_VAL_'+id).val(value);
					$(target).html("<img src='temp/loader.gif'></div>").delay(1000).html(value);
				});
			}
		});
		//$('#CARD_CON_'+id).click(function(){ can_edit_able(target,id,'fill'); });
		document.getElementById('CARD_CON_'+id).setAttribute("onclick","can_edit_able('"+target+"','"+id+"','fill')");
	}
}



function submit_book(){
	alert('checkStockCus.html foote>');
	/* alert("line1\r\nline2\r\nline3");
 	if($('#intSelection').val()==''){ var msg1 = 'ผิดพลาดกรุณาไปทำรายการลูกค้าสนใจมาใหม่นะครับ';}
	if($('#Price_E').val()==''){ 	  var msg2 = 'กรุณาระบุ ราคารถด้วยครับ';}
	if($('#Booking_Paid').val()==''){ var msg3 = 'กรุณาระบุ ราคารถด้วยครับ';}
	if($('#date_booking').val()==''){ var msg3 = กรุณาระบุ วันที่จอง ด้วยครับ'}
	if($('#Est_Delivery').val()==''){ var msg4 = 'กรุณาระบุ วันที่ออกรถ ด้วยครับ';}
	if($('#Time_Delivery').val()==''){ var msg5 = 'กรุณาระบุ เวลาที่ออกรถ ด้วยครับ';}
	if($('#SaleBookingNo').val()==''){ var msg6 = 'กรุณาระบุ พนง.ขายที่รับจอง ด้วยครับ';} */
	return false;
}







function verify_form(array){
	alert(array);
	//'['Booking_Paid','date_booking','Est_Delivery','Time_Delivery','SaleBookingNo']'
	return false;
   /* if($('#').val()==''){ alert('')}
	Array('Price_E','simple','กรุณาระบุ ราคารถด้วยครับ')
	,Array('Booking_Paid','simple','กรุณาระบุ เงินจอง ด้วยครับ')
	,Array('date_booking','simple','กรุณาระบุ วันที่จอง ด้วยครับ' )
	,Array('Est_Delivery','simple','กรุณาระบุ วันที่ออกรถ ด้วยครับ')
	,Array('Time_Delivery','simple','กรุณาระบุ เวลาที่ออกรถ ด้วยครับ' )
	,Array('SaleBookingNo', 'simple','กรุณาระบุ พนง.ขายที่รับจอง ด้วยครับ' )
	*/
}








function progress_bar(){
	//if(val != '10'){val = val*5;}else{val=val;}
	$("#progressbar").progressbar({value: 20});
	//var value = $( "#progressbar" ).progressbar( "option", "value" );
}
function test55(){
	alert('44');var dataSet = {test:'ttss'}
	$.post("XX.php",dataSet,function(data){alert(data);});
}
function more_all_test(chk,csize,ccontent,cappend,variable,sub_content,sub_append,sub_variable){
	/* alert(variable);variable = variable.split('##');alert(variable.length);
	if(variable.length>1){for(i=0;i<variable.length;i++){alert(variable[i]);para = variable[i].split('@@');alert(para[1]);
	if(para[1]){variable[i] = para[0]+'='+para[1]};alert(variable[i]);}} */return false;
}
function test_all(){
	$.post('ic_newcusint_action.php',(jQuery('#testXX').find('input,input[type=hidden],select,textarea').serialize()),
	function(data){ $('#field_recomend').html(data); });
}
function CusTempAttain_Sell(){
	//var url = $('#formsCusReport').serialize(true);
	url = "operator=MakeSelectSub&List=Sell&CutMonths="+document.getElementById('formsCusReport').CutMonthsReport.value+'&CutYears='+document.getElementById('formsCusReport').CutYearsReport.value;
	$.get('CusTempAttain.php',url,function(data){
			$('#spanCutTempCheckMoresdCar').html(data);
			url = url = "operator=Sell&"+$('#formsCusReport').serialize(true);
			$('#SaleTempReportd1').html("1");
			browseCheck('SaleTempReportd1' , 'CusTempAttain.php?operator=Sell&CutMonths='+document.getElementById('formsCusReport').CutMonthsReport.value+'&CutYears='+document.getElementById('formsCusReport').CutYearsReport.value+'&CutTempCheckMore='+document.getElementById('formsCusReport').CutTempCheckMoresdCar.value);
		}
	);
}
function CusTempAttain_SaleDataView(){
	url = 'operator=MakeSelectSub&List=SaleDataView&CutMonths='+document.getElementById('formsCus').CutMonths.value+'&CutYears='+document.getElementById('formsCus').CutYears.value;
	$.get('CusTempAttain.php',url,function(data){
		$('#spanCutTempCheck').html(data);
		browseCheck('TempCut' ,'CusTempAttain.php?operator=SaleDataView&CutTempCheck='+document.getElementById('formsCus').CutTempCheck.value+'&CutMonths='+document.getElementById('formsCus').CutMonths.value+'&CutYears='+document.getElementById('formsCus').CutYears.value);
	});
}


function CusTempAttain_ProspectsSummary(){
	url = 'operator=MakeSelectSub&List=prospects_summary&CutMonths='+document.getElementById('formsCus2').CutMonths.value+'&CutYears='+document.getElementById('formsCus2').CutYears.value;
	$.get('CusTempAttain.php',url,function(data){
		$('#spanCutTempCheck2').html(data);
		browseCheck('sale_prospects_summary' ,'CusTempAttain.php?operator=sale_prospects_summary&CutTempCheck='+document.getElementById('formsCus2').CutTempCheck.value+'&CutMonths='+document.getElementById('formsCus2').CutMonths.value+'&CutYears='+document.getElementById('formsCus2').CutYears.value);
	});
}
//ตรวจสอบข้อมูลก่อนส่งมอบรถ 2011-02-08
var errorCusData = '';
function check_cus_data(cusBuy,param){
	if(cusBuy){ //ตรวจสอบข้อมูลว่าครบหรือยัง
		$.get("carAcc.php", "check_cusBuy_profile="+cusBuy ,function(error){
			errorCusData = error;
			if(error){alert(error);}//ห้ามแก้ไขฟังก์ชั่นนี้ ให้เขียนใหม่ เพราะการเช้กไม่ได้เช้ก true/false แต่เป็นการเช็กค่าว่าง
		});
	}
}
//เหมือนฟังก์ชั่นด้านบน แต่ใช้ตอนคลิกเข้าไปหน้าบันทึกการจอง
function check_customer(cusno, browse_case, arr){
	if(cusno){ //ตรวจสอบข้อมูลว่าครบหรือยัง
		$.get("carAcc.php", "check_cusBuy_profile="+cusno+'&booking=1' ,function(error){
			if(error){
				alert(error);return false;
			}else{
				if(browse_case=='have_car'){//บันทึกจองแบบมีรถ
					var param = 'CheckBookingcheng='+arr.CheckBookingcheng+'&page=&cusnoEdit='+cusno+'&bookIntNum2='+arr.Int_Num_Ref;
					param += '&editBooking=&changeBooking=&caseName=bookfromChkStock&case=bookfromChkStock&intNo='+arr.Int_Num_Ref;
					param += '&PreintTime='+arr.Int_Time+'&CusInfo='+arr.info+'&bookstatus=fromCheckStock&s_type='+arr.Unit_Type+'&s_class='+arr.Model_Class;
					param += '&s_code='+arr.MV_Model_CodeName+'&s_gname='+arr.Model_GName+'&ModelCM_Value='+arr.Model_C_M+'&s_cm='+arr.MV_Eng_No;
					param += '&Dnet_IncVat='+arr.CRPAndAirIncVat+'&s_chass='+arr.MV_Chass_No+'&ContactPrepare='+arr.Int_CusNo+'&s_color='+arr.Color+'&Edit='+arr.cusno;
					browseCheck('BookInfo','cheackStockCus.php?'+param);
					document.getElementById('ViewCusForStock').style.display = 'none';
					document.getElementById('cusnoEdit').value = cusno;
					document.getElementById('bookIntNum2').value = arr.Int_Num_Ref;
					document.getElementById('CusInfo').value = arr.info;
					document.getElementById('editBooking').value = arr.Int_Num_Ref;
					hideHeadContent('hide','containHead');
				}else if(browse_case=='no_car'){//บันทึกจองแบบไม่มีรถ (รอรถใหม่)
					var param = 'page=&cusnoEdit='+cusno+'&bookIntNum2='+arr.Int_Num_Ref+'&editBooking=&changeBooking=';
					param += '&caseName=bookfromChkStock&case=bookfromChkStock&intNo='+arr.Int_Num_Ref+'&PreintTime='+arr.Int_Time+'&CusInfo='+arr.info;
					param += '&bookstatus=fromCheckStock&s_type='+arr.Int_Unit_Type_1C+'&s_class='+arr.Int_Model_Class_1C;
					param += '&s_code='+arr.Int_Model_CodeName+'&s_gname='+arr.Int_Model_GName+'&s_cm=&ModelCM_Value='+arr.Model_C_M;
					param += '&Dnet_IncVat=&s_chass=&ContactPrepare='+arr.Int_CusNo+'&s_color='+arr.Color+'&Edit='+arr.Int_CusNo;
					browseCheck('BookInfo','carBooking.php?'+param);
					hideHeadContent('hide','containHead');
					cond_before_book(arr.Int_Num_Ref);
				}
			}
		});
	}
}


//ค้นหาผู้รับผิดชอบการขายก่อน บันทึกการจอง
function check_resperson(cusBuy){
	if(cusBuy){
		$.get("carBooking.php", "check_cus_resperson="+cusBuy ,function(cus){
			var obj = jQuery.parseJSON(cus);
			if(obj){
				$('#resperson').val(obj.emp_name).attr({'readonly':'readonly','style':'','class':''});
				$('#resperson_id').val(obj.emp_data_no);
			}else{
				$('#resperson,#resperson_id').val('').attr('readonly',false);
			}
		});
		check_cus_data(cusBuy);//ตรวจสอบประวัติลูกค้า
	}
}

// FUNCTION PAGE StockDeliveryDealer การนำรถจาก Dealer อื่นเข้าสู่ stock
function fullpageScroll(){
	var div = $('#data');
	var divHeight = $(window).height()-150;
	div.css("height",divHeight+'px');
}
// note การนำรถจาก Dealer อื่นเข้าสู่ stock  
function MoreDeliveryDealer(check_id){
	var size = $('.more-rows').size(); var msg = 'โปรดตรวจสอบความถูกต้องด้วยครับ \n\n'; 	var empty = false;   
	$.each( $('.dl-rows-'+check_id) , function(){ 
		if($(this).attr('name')!='carcard[]'){ 
			if( $(this).val() == '' ){ empty=true; } 
		}
	});
	if( empty ){msg += 'มีช่องป้อนข้อมูลบางตัวว่างอยู่  ในกลุ่มข้อมูลชุดที่ '+check_id; alert( msg );return false;}
	
	$.get('StockDeliveryDealer.php','method=MoreDeliveryDealer&size='+size,function(rows){
		$('#box-table-a').append(rows);
	});
}
function DelDeliveryDealer(remove_id){  var size = $('.more-rows').size();  if(size>1){ $(remove_id).remove(); }  }
function SaveStockDeliveryDealer(){
	var size = $('.more-rows').size(); var msg = 'โปรดตรวจสอบความถูกต้องด้วยครับ \n\n'; 	var empty = false;   
	$.each( $('.dl-rows-'+size) , function(){
		if($(this).attr('name')!='carcard[]'){  if( $(this).val() == '' ){ empty=true; }  }
	});
	if( empty ){msg += 'มีช่องป้อนข้อมูลบางตัวว่างอยู่  ในกลุ่มข้อมูลชุดที่ '+size; alert( msg );return false;}
	
	
	$.post('StockDeliveryDealer.php',$("#StockDeliveryDealer").find('input, textarea, select').serialize()+'&method=SaveStockDeliveryDealer',function(data){
		alert(data);  browseCheck('main','StockDeliveryDealer.php','');
	});
}
function SelectDeliveryDealer(this_value,target_id,select_case,id_num){
	var dataSet = ''; var str = this_value.split('||'); var length = str.length;
	for(var i=0;i<length;i++) { if(str[i]!=''){	dataSet += '&'+str[i]+'='+$('#'+(str[i]+id_num)).val(); } }
	$.get('StockDeliveryDealer.php','method=SelectDeliveryDealer&select_case='+select_case+dataSet,function(data){
		if(select_case=='model_name'){ 
			$(target_id).val(data);
			SelectDeliveryDealer(this_value,'#model_gname'+id_num,'model_gname',id_num); 
		}else if(select_case=='model_gname'){
			$(target_id).val(data);
			SelectDeliveryDealer(this_value+'||model_name||model_gname','#color'+id_num,'color_model',id_num); 
		}else{
			$(target_id).html(data);
		}
	});
}


//browseCheck('TempCut' , 'CusTempAttain.php?operator=SaleDataView&dQuery=1&CutTempCheck=$send_id&CutMonths='+document.getElementById('formsCus').CutMonths.value+'&CutYears='+document.getElementById('formsCus').CutYears.value);
//browseCheck('spanCutTempCheck' , 'CusTempAttain.php?operator=MakeSelectSub_Manager&dQuery=1&CutMonths='+document.getElementById('formsCus').CutMonths.value+'&CutYears='+document.getElementById('formsCus').CutYears.value);
