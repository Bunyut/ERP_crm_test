<?php
	class ReadFiles{
		private $config;
		private $logDb;

		function __construct(){
			global $config, $logDb;
			
			$this->config	= $config;
			$this->logDb	= $logDb;
		}
	
		public function readExcel($arr){
	
			$file_path = $arr['filename'];

			// $file_path ='libraries/excel_reader/test.xls';
			require_once 'libraries/PHPExcel/PHPExcel/IOFactory.php';
			$objPHPExcel = PHPExcel_IOFactory::load( $file_path );

			$DataExcel 	= array();
			$arrKey		= array();

			$value = '';

			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
				$worksheetTitle  	= $worksheet->getTitle();
				$objWorksheet 		= $objPHPExcel->getActiveSheet();
				$highestRow 		= $objWorksheet->getHighestRow(); // e.g. 10
				$highestColumn 		= $objWorksheet->getHighestColumn(); // e.g 'F'
				$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5

				for ($row = 1; $row <= $highestRow; $row ++ ) {
					$chk_null = true;

					for ($Colum = 0; $Colum <  $highestColumnIndex; $Colum ++ ) {  
						$cell = $worksheet->getCellByColumnAndRow($Colum,$row);

						if($row!=1 && $cell->getValue()!=''){
							$chk_null = false;
						}

						if($cell->getValue() instanceof PHPExcel_RichText){
							$value = $cell->getValue()->getPlainText();
						}else{
							$value = $cell->getValue();
						}

						$keyArr = $row - 2;

						if( $row == 1 ){
							$arrKey[$Colum]	= $value;
						}else if($arrKey[$Colum]!=''){
							$DataExcel[$keyArr][$arrKey[$Colum]] = $value;
						}
					}

					if($chk_null == true){
						unset($DataExcel[$keyArr]);
					}
				}
				
				break;
			}
			
			return $DataExcel;
		}
		
		public function readExcel2007($arr){
		
			$file_path = $arr['filename'];

			// $file_path ='libraries/excel_reader/test.xls';
			require_once 'libraries/PHPExcel/PHPExcel/IOFactory.php';
			$objReader 		= PHPExcel_IOFactory::createReader('Excel2007');
			$objReader->setReadDataOnly('true');
			
			$objPHPExcel 	= PHPExcel_IOFactory::load( $file_path );

			$DataExcel 	= array();
			$arrKey		= array();

			$value = '';

			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
				$worksheetTitle  	= $worksheet->getTitle();
				$objWorksheet 		= $objPHPExcel->getActiveSheet();
				$highestRow 		= $objWorksheet->getHighestRow(); // e.g. 10
				$highestColumn 		= $objWorksheet->getHighestColumn(); // e.g 'F'
				$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5

				for ($row = 1; $row <= $highestRow; $row ++ ) {
					$chk_null = true;

					for ($Colum = 0; $Colum <  $highestColumnIndex; $Colum ++ ) {  
						$cell = $worksheet->getCellByColumnAndRow($Colum,$row);

						if($row!=1 && $cell->getValue()!=''){
							$chk_null = false;
						}

						if($cell->getValue() instanceof PHPExcel_RichText){
							$value = $cell->getValue()->getPlainText();
						}
						else{
							$value = $cell->getValue();
						}

						$keyArr = $row - 2;

						if( $row == 1 ){
							$arrKey[$Colum]	= $value;
						}else if($arrKey[$Colum]!=''){
							$DataExcel[$keyArr][$arrKey[$Colum]] = $value;
						}
					}

					if($chk_null == true){
						unset($DataExcel[$keyArr]);
					}
				}
				
				break;
			}
			
			return $DataExcel;
		}
		
		public function read_file($file_path)
		{
			$handle = fopen($file_path,'r');
			$output = fread($handle, filesize($file_path));
			fclose($handle);
			
			return$output;
		}
		
		public function readCSV($arr){
			/* $file_path 	= $arr['filename'];
			$charset	= iconv_function($file_path); // function/general.php
			
			echo "if(".$charset." == 'UTF-8'){\n";
			if($charset == 'UTF-8'){
				setlocale ( LC_ALL, 'en_US.UTF-8' );
			}else{
				setlocale ( LC_ALL, 'en_US.UTF-8' );
			}
			
			if (($handle = fopen($file_path, "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 0, ',')) !== FALSE) {
					$num 	= count($data);
					
					// echo "<p> $num fields in line $row: <br /></p>\n";
					$row++;
					for ($c=0; $c < $num; $c++) {
						if($charset == 'UTF-8'){
							$data[$c] = $data[$c];
						}else{
							$data[$c] = iconv( 'windows-874', 'UTF-8', $data[$c]);
							// $data[$c] = htmlentities(iconv( 'TIS-620', 'UTF-8', trim($data[$c])), ENT_QUOTES, "UTF-8");
						}
					}
					
					echo '<pre>';
					print_r($data);
					echo '</pre>';
				}
				fclose($handle);
			} */
			
			## มีบัค column สุดท้าย มี enter จะรวมข้อความผิด
			## มีบัค column มีเครื่องหมาย comma
			$file_path 		= $arr['filename'];
			// $rowData 		= file($file_path);
			$charset		= iconv_function($file_path); // function/general.php
			$output			= $this->read_file($file_path);
			$rowData 		= array_filter(explode( "\r\n", $output ));
			$numRow			= count($rowData);

			if($numRow <= 1){
				$rowData 	= array_filter(explode( "\n", $output ));
				$numRow		= count($rowData);
			}
			
			$DataExcel 		= array();
			$arrKey			= array();
			
			$i 				= 0;
			$keyArr			= 0;
			## วนจำนวนแถว
			while($i < $numRow){
			
				$vRow			= explode(',',$rowData[$i]);
				$keyArr 		= $i;	
				
				if(count($vRow) > 0){
					## วนจำนวน column
					foreach($vRow AS $keyC=>$valueC){
						$keyC		= trim($keyC);
						$valueC		= trim($valueC);
					
						## ถ้าเป็นแถวแรก
						if( $i == 0 ){
							if($charset == 'UTF-8'){
								$arrKey[$keyC]						= $valueC;
							}else{
								$arrKey[$keyC]						= iconv( 'TIS-620', 'UTF-8', $valueC);
							}
						## ถ้าไม่ใช่แถวแรก
						}else if($arrKey[$keyC] != ''){
							if($charset == 'UTF-8'){
								$DataExcel[$keyArr][$arrKey[$keyC]] = $valueC;
							}else{
								$DataExcel[$keyArr][$arrKey[$keyC]] = iconv( 'TIS-620', 'UTF-8', $valueC);
							}
						}
					}
				}
				
				$i++;
			}

			/* // $file_path ='libraries/excel_reader/test.xls';
			$file_path 	= $arr['filename'];
			require_once 'libraries/PHPExcel/PHPExcel/IOFactory.php';
			$objReader 		= PHPExcel_IOFactory::createReader('CSV');
			// $objReader->setReadDataOnly('true');
			
			// $objReader->setDelimiter("|");
			$objReader->setInputEncoding('UTF-8');
			// $objReader->setInputEncoding('TIS-620');
			
			$objPHPExcel 	= PHPExcel_IOFactory::load( $file_path );

			$DataExcel 	= array();
			$arrKey		= array();

			$value = '';

			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
				$worksheetTitle  	= $worksheet->getTitle();
				$objWorksheet 		= $objPHPExcel->getActiveSheet();
				$highestRow 		= $objWorksheet->getHighestRow(); // e.g. 10
				$highestColumn 		= $objWorksheet->getHighestColumn(); // e.g 'F'
				$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5

				for ($row = 1; $row <= $highestRow; $row ++ ) {
					$chk_null = true;

					for ($Colum = 0; $Colum <  $highestColumnIndex; $Colum ++ ) {  
						$cell = $worksheet->getCellByColumnAndRow($Colum,$row);

						if($row!=1 && $cell->getValue()!=''){
							$chk_null = false;
						}

						if($cell->getValue() instanceof PHPExcel_RichText){
							$value = $cell->getValue()->getPlainText();
						}
						else{
							$value = $cell->getValue();
						}
						
						$value	= htmlentities(iconv( 'TIS-620', 'UTF-8', $value), ENT_QUOTES, "UTF-8");

						$keyArr = $row - 2;

						if( $row == 1 ){
							$arrKey[$Colum]	= $value;
						}else if($arrKey[$Colum]!=''){
							$DataExcel[$keyArr][$arrKey[$Colum]] = $value;
						}
					}

					if($chk_null == true){
						unset($DataExcel[$keyArr]);
					}
				}
				
				break;
			} */
			
			// echo 'DataExcel >> <pre>';
			// print_r($DataExcel);
			// echo '</pre>';
			
			return $DataExcel;
		}
		
		public function checkFieldFiles($check, $arr){
			$res['status'] 	= true;
			
			$diff			= array_diff_key($check, $arr);
			
			if(!empty($diff)){
				$res['status'] 	= false;
				$res['error']	= $diff;
			}
			
			return $res;
		}
		
		public function fieldClist(){
			$arr 	= array('FollowDate'		=>'FollowDate', 
							'PersonName'		=>'PersonName', 
							'CompanyName'		=>'CompanyName', 
							'FollowType'		=>'FollowType', 
							'FollowPeriod'		=>'FollowPeriod',
							'ScName'			=>'ScName', 
							'ActionDate'		=>'ActionDate', 
							'DealerName'		=>'DealerName', 
							'BranchName'		=>'BranchName', 
							'VIN'				=>'VIN', 
							'InterestMarketName'=>'InterestMarketName', 
							'Engine'			=>'Engine', 
							'DeliveryDate'		=>'DeliveryDate', 
							'Address'			=>'Address', 
							'Phone'				=>'Phone', 
							'Remark'			=>'Remark', 
							'ContactDetail'		=>'ContactDetail', 
							'SQ1Score'			=>'SQ1Score', 
							'SQ2Score'			=>'SQ2Score', 
							'StatusId'			=>'StatusId', 
							'FollowUpStatus'	=>'FollowUpStatus', 
							'ModelGroup'		=>'ModelGroup', 
							'IsUnContact'		=>'IsUnContact', 
							'UnContactReason'	=>'UnContactReason', 
							'IsReferral'		=>'IsReferral', 
							'IsRePurchase'		=>'IsRePurchase', 
							'IsAdditional'		=>'IsAdditional', 
							'SalesOppNo'		=>'SalesOppNo', 
							'ActionDealerName'	=> 'ActionDealerName', 
							'ActionBranchName'	=> 'ActionBranchName', 
							'ActionScName'		=> 'ActionScName');
			
			return $arr;
		}
	}
?>
