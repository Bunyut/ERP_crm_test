<?php
function getFieldPatternDate($id=null) {
	global $config, $logDb;
	
	$sql  	= "SELECT id,pattern_name FROM $config[db_base_name].follow_field_pattern WHERE status != '99'";
	$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	while($rs = mysql_fetch_assoc($result)) {
		$selected = "";
		if($rs['id']==$id){$selected = "selected='selected'";}
		$option .= "<option $selected value='$rs[id]' class='text-left'>$rs[pattern_name]</option>";
	}
	if($id=='xx'){$option .= "<option selected='selected' value='xx' class='text-left'>สร้างกิจกรรมแบบไม่ระบุวันที่</option>";}
	return $option;
}

function getDepartment($opt=null,$func=null) {//รอ OU เพราะลิสต์แผนกออกมาไม่ได้ select box
	global $config,$depart;
	foreach($depart as $key=>$val){
		$selected = "";
		if($func=="" && $key=="Leader"){$a++;continue;}#-- ถ้าเป็นหน้าแบบฟอร์มสร้างเหตุการณ์ไม่ต้องแสดงหัวหน้า
		if($key==$opt){$selected = "selected='selected'";}
	//	echo '<br>'.$opt."-".$key;
		$option .= "<option $selected value='".$key."'>".$val."</option>";
	}
	return $option;
}

function get_department_from_cut($cutDepartment){//ชื่อแผนก
	global $depart;

	//$sql = "SELECT department FROM ...... WHERE MID(member_id,5,2) = '$cutDepartment' ";
	//$result = mysql_query($sql);
	//$rs = mysql_fetch_assoc($result);
	//return $rs['department'];

	$rs['department'] = $depart;
	return $rs['department'][$cutDepartment];
}

//ลิสต์รายชื่อฐานข้อมูลที่อยุ่ใน $config ที่ส่งมา
function getDatabaseName($opt=null){  
	global $config, $db_comment,$fix_db, $logDb;
	$sql = "SHOW DATABASES";
	$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	while($row = mysql_fetch_assoc($result)) {
		//if(in_array($row['Database'], $config)){ //แสดงฐานข้อมูลทุกตัวที่อยู่ใน $config
		if(in_array($row['Database'], $fix_db)){//[fix database] เอาเฉพาะฐานข้อมูล ฝ่ายขาย และ ฐานข้อมูล ลูกค้า
			$a++;
			if($a==1){
				$color = "#FDFDFD";
			}else{
				$color = "#ECECEC";
				$a=0;
			}
			$selected = "";
			if($opt==$row['Database']){ $selected = "selected='selected'";}
			$comment = $db_comment[$row['Database']]['comment'];
			if($comment==""){$comment="ไม่มีคำอธิบาย";}
			$db .= "<option $selected style='background-color:$color;height:20px' value='$row[Database]' class='text-left'>&nbsp;&nbsp;$row[Database] ($comment)</option>";
		}
	}
	mysql_free_result($result);
	return $db;
}

//ลิสต์รายชื่อ "ตาราง" ข้อมูลที่อยู่ในฐานข้อมูล ที่ส่งมา
function getTableName($dbname,$opt=null){
	global $config,$fix_tb,$logDb;
	$sql = "SHOW TABLE STATUS FROM $dbname";
	$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	if($result){
		$inum = 0;
		while($row = mysql_fetch_assoc($result)){
			//หาเฉพาะตารางที่มีฟิลด์เป็นวันที่
			$have_date = false;
			// if($row['Name']=="Sell" || $row['Name'] == "MainCusData"){//fix table เพราะยังไม่ได้เขียนให้รองรับทุกตาราง
			if(!empty($fix_tb)){
				if(in_array($row['Name'],$fix_tb)){//$fix_tb = array fix table
					if(!strpos($row['Name']," ")){//ชื่อตารางต้องไม่มีค่าว่าง
						$have_sql = "SHOW FULL COLUMNS FROM $dbname.$row[Name] ";
						$have_result = $logDb->queryAndLogSQL( $have_sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						while($have_row = mysql_fetch_assoc($have_result)) {
							$str_field   = strtolower($have_row['Field']);
							$str_comment = strtolower($have_row['Comment']);
							if(strstr($str_field,"date" ) || strstr($str_field,"day" )  || strstr($str_comment,"วัน")){
								$have_date = true;
								break;//ถ้าวันที่แล้วให้หยุดค้นหา
							}
						}//วนลูปหาฟิลด์ที่เก็บวันที่
					}
				}
			}else{
				if(!strpos($row['Name']," ")){//ชื่อตารางต้องไม่มีค่าว่าง
					$have_sql = "SHOW FULL COLUMNS FROM $dbname.$row[Name] ";
					$have_result = $logDb->queryAndLogSQL( $have_sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					while($have_row = @mysql_fetch_assoc($have_result)) {
						$str_field = strtolower($have_row['Field']);
						$str_comment = strtolower($have_row['Comment']);
						/* if($row['Name']=='lease_detail_data'){
							echo '1',strstr($str_field,"date" ),' || 2',strstr($str_field,"day" ),' || 3',strstr($str_comment,"วัน"),"\n";
						} */
						if(strstr($str_field,"date" ) || strstr($str_field,"day" )  || strstr($str_comment,"วัน")){
							$have_date = true;
							break;//ถ้าวันที่แล้วให้หยุดค้นหา
						}
					}//วนลูปหาฟิลด์ที่เก็บวันที่
				}
			}
			//end fix table
			  
			if($have_date==true){
				if($row['Comment']==""){ $row['Comment'] = "ไม่มีคำอธิบาย!!";}
				$a++;
				if($a==1){
					$color = "#FDFDFD";
				}else{
					$color = "#ECECEC";
					$a=0;
				}
				$selected = "";
				if($opt==$row['Name']){ $selected = "selected='selected'";}
				
				if($inum==0){
					$table .= "<option value='' class='text-left'>&nbsp;&nbsp;เลือกชื่อตารางข้อมูล </option>";
				}
				$table .= "<option $selected style='background-color:$color;height:20px' class='text-left' value='$row[Name]'>&nbsp;&nbsp;$row[Name]  ( $row[Comment] )</option>";
			}
		
			$inum++;
		
		}//while
		mysql_free_result($result);
	}//if
	return $table; 
}

//ลิสต์รายชื่อ "ฟิลด์" ข้อมูลที่อยู่ในตาราง ที่ส่งมา
function getFieldName($tbname,$opt=null,$chk=''){
	global $config, $logDb;
	if($tbname){
		$sql 	= "SHOW FULL COLUMNS FROM $tbname";
		$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		if($result)
		{
			while($row = mysql_fetch_assoc($result)) {
				$str_field 		= trim(strtolower($row['Field']));
				$str_comment 	= trim(strtolower($row['Comment']));
				//เอาเฉพาะฟิลด์ที่มีวันที่
				
				
				if($chk == 'NotCheck'){
				
					$a++;
					if($a==1){
						$color = "#FDFDFD";
					}else{
						$color = "#ECECEC";
						$a=0;
					}
					$selected = "";
					//echo "$opt==$row[Field]<br>";
					if($opt==$row['Field']){ $selected = "selected='selected'";}
					if($row['Comment']==""){ $row['Comment'] = "ไม่มีคำอธิบาย!!";}
					$field .= "<option $selected style='background-color:$color;height:20px' value='$row[Field]' class='text-left'>&nbsp;&nbsp;$row[Field]  ( $row[Comment] )</option>";
				
					
				}else{
				
					if(strstr($str_field,"date" ) || strstr($str_field,"day" )  || strstr($str_comment,"วัน")){
						$a++;
						if($a==1){
							$color = "#FDFDFD";
						}else{
							$color = "#ECECEC";
							$a=0;
						}
						$selected = "";
						//echo "$opt==$row[Field]<br>";
						if($opt==$row['Field']){ $selected = "selected='selected'";}
						if($row['Comment']==""){ $row['Comment'] = "ไม่มีคำอธิบาย!!";}
						$field .= "<option $selected style='background-color:$color;height:20px' value='$row[Field]' class='text-left'>&nbsp;&nbsp;$row[Field]  ( $row[Comment] )</option>";
					}
				}
				
			}//while
			mysql_free_result($result);
		}//if
	}//end if
	return $field;
}

//ลิสต์รายชื่อ "ฟิลด์" ข้อมูลที่อยู่ในตาราง ที่ส่งมา    field ข้อมูล  cusno รหัส ลูกค้า
function getFieldNameCus($tbname,$opt=null){
	global $config, $logDb;
	if($tbname){
		$sql = "SHOW FULL COLUMNS FROM $tbname";
		$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		if($result){
		
			$arrayOption = array();
			$field		 = '';
			$iNum		 = 0;
			
			while($row = mysql_fetch_assoc($result)) {
				$str_field	 = trim(strtolower($row['Field']));
				$str_comment = trim(strtolower($row['Comment']));
				//เอาเฉพาะฟิลด์ที่มีวันที่
				if(strstr($str_field,"crmข้อมูลคน" ) || strstr($str_comment,"crm_ref_cusno") || strstr($str_comment,"crmข้อมูลคน")){
					
					$a++;
					$iNum++;
					
					if($a==1){
						$color = "#FDFDFD";
					}else{
						$color = "#ECECEC";
						$a=0;
					}
					$selected = "";
					//echo "$opt==$row[Field]<br>";
					if($opt==$row['Field']){ 
						$selected = "selected='selected'";
					}
					
					if($row['Comment']==""){ 
						$row['Comment'] = "ไม่มีคำอธิบาย!!";
					}
					
					$field .= "<option $selected style='background-color:$color;height:20px' value='$row[Field]' class='text-left'>&nbsp;&nbsp;$row[Field]  ( $row[Comment] )</option>";
				}
				
				//$arrayOption[$iNum]['Comment'] = $row['Comment'];
				//$arrayOption[$iNum]['Field']   = $row['Field'];
		
				$arrayOption[] = array(
					'Field' 	=> $row['Field'], 
					'Comment' 	=> $row['Comment'],
				);

			}//while
			mysql_free_result($result);
		}//if
	}//end if
	
	
	
	if($field == ''){
		if(!empty($arrayOption)){
			$arrayOption = array_filter($arrayOption);
			foreach($arrayOption as $keys => $vals){
				$b++;
				
				if( $b == 1 ){
				
					$color 	= 	"#FDFDFD";
					
				}else{
				
					$color 	= 	"#ECECEC";
					$b		=	0;
				}
				
				$selected = "";
				
				if($opt==$vals['Field']){ 
					$selected = "selected='selected'";
				}
				
				if($vals['Comment']==""){ 
					$vals['Comment'] = "ไม่มีคำอธิบาย!!";
				}
				
				$field .= "<option $selected style='background-color:$color;height:20px' value='".$vals['Field']."' class='text-left'>&nbsp;&nbsp;$vals[Field]  ( $vals[Comment] )</option>";
			
			}
		}
	}
	
	
	return $field;
}

//ลิสต์รายชื่อ "ฟิลด์" ข้อมูลที่อยู่ในตาราง ที่ส่งมา    field ข้อมูล  vehicle รหัส  refer id MainVehicle
function getFieldNameVehicle($tbname,$opt=null){
	global $config, $logDb;
	if($tbname){
		$sql = "SHOW FULL COLUMNS FROM $tbname";
		$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		if($result)
		{
			while($row = mysql_fetch_assoc($result)) {
				$str_field = trim(strtolower($row['Field']));
				$str_comment = trim(strtolower($row['Comment']));
				//เอาเฉพาะฟิลด์ที่มีวันที่
				if(strstr($str_field,"crmข้อมูลรถ" )  || strstr($str_comment,"crm_ref_vehicle") || strstr($str_comment,"crmข้อมูลรถ")){
					$a++;
					if($a==1){
						$color = "#FDFDFD";
					}else{
						$color = "#ECECEC";
						$a=0;
					}
					$selected = "";
					if($opt==$row['Field']){ $selected = "selected='selected'";}
					if($row['Comment']==""){ $row['Comment'] = "ไม่มีคำอธิบาย!!";}
					$field .= "<option $selected style='background-color:$color;height:20px' value='$row[Field]' class='text-left'>&nbsp;&nbsp;$row[Field]  ( $row[Comment] )</option>";
				}
			}//while
			mysql_free_result($result);
		}//if
	}//end if
	return $field;
}

//ลิสต์รายชื่อ "ฟิลด์" ข้อมูลที่อยู่ในตาราง ที่ส่งมา    field ข้อมูล  vehicle รหัส  refer id MainVehicle
function getFieldNameBrand($tbname,$opt=null){
	global $config, $logDb;
	if($tbname){
		$sql = "SHOW FULL COLUMNS FROM $tbname";
		$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		if($result)
		{
			while($row = mysql_fetch_assoc($result)) {
				$str_field = trim(strtolower($row['Field']));
				$str_comment = trim(strtolower($row['Comment']));
				//เอาเฉพาะฟิลด์ที่มีวันที่
				if(strstr($str_field,"branch" )  || strstr($str_comment,"branch") || strstr($str_comment,"สาขา")){
					$a++;
					if($a==1){
						$color = "#FDFDFD";
					}else{
						$color = "#ECECEC";
						$a=0;
					}
					$selected = "";
					if($opt==$row['Field']){ $selected = "selected='selected'";}
					if($row['Comment']==""){ $row['Comment'] = "ไม่มีคำอธิบาย!!";}
					$field .= "<option $selected style='background-color:$color;height:20px' value='$row[Field]' class='text-left'>&nbsp;&nbsp;$row[Field]  ( $row[Comment] )</option>";
				}
			}//while
			mysql_free_result($result);
		}//if
	}//end if
	return $field;
}

//ลิสต์รายชื่อ "ฟิลด์" ข้อมูลที่อยู่ในตาราง ที่ส่งมา    field ข้อมูล  vehicle รหัส  refer id MainVehicle
function getFieldNamePrimary($tbname,$opt=null){
	global $config, $logDb;
	if($tbname){
		$sql = "SHOW FULL COLUMNS FROM $tbname";
		$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		if($result)
		{
			while($row = mysql_fetch_assoc($result)) {
				$str_field = trim(strtolower($row['Field']));
				$str_comment = trim(strtolower($row['Comment']));
				//เอาเฉพาะฟิลด์ที่มีวันที่
				// if(strstr($str_field,"branch" )  || strstr($str_comment,"branch") || strstr($str_comment,"สาขา")){
					$a++;
					if($a==1){
						$color = "#FDFDFD";
					}else{
						$color = "#ECECEC";
						$a=0;
					}
					$selected = "";
					if($opt==$row['Field']){ $selected = "selected='selected'";}
					if($row['Comment']==""){ $row['Comment'] = "ไม่มีคำอธิบาย!!";}
					$field .= "<option $selected style='background-color:$color;height:20px' value='$row[Field]' class='text-left'>&nbsp;&nbsp;$row[Field]  ( $row[Comment] )</option>";
				// }
			}//while
			mysql_free_result($result);
		}//if
	}//end if
	return $field;
}

function get_question_list($main_question_id) {
	global $config,$tpl,$row_fol,$bg,$colspan,$tr_list,$manage_question,$main_question_id,$logDb;
	$class[1] = "event_tr1";
	$class[2] = "event_tr2";
	$event_id = query("SELECT event_id_ref FROM $config[db_base_name].follow_main_question WHERE id = '$main_question_id' ");
	$manage_question = "event_id=$event_id";
	$sql_list  = "SELECT id,main_question_id,list_title,input_type,answer_option,pleasure_check,status,order_number
				FROM $config[db_base_name].follow_question_list
				WHERE main_question_id = '$main_question_id' AND status != '99' ORDER by order_number ";
	$result_list = $logDb->queryAndLogSQL( $sql_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	$tr_list=null;//ต้องเคลียร์ค่า $tr_list ก่อนใช้งาน เพราะเป็น global
	while($row_fol = mysql_fetch_assoc($result_list)) {
		$a++;
		if($a>2){$a=1;}
		$bg = $class[$a];
		$answer_option	= stripcslashes($row_fol['answer_option']);
		$answer_option	= str_replace("','", "', '", $answer_option);//แทรกช่องว่างระหว่างตัวเลือก เพื่อให้ขึ้นบันทัดใหม่ได้ในกรณียาวเกินไป
		$answer_option	= str_replace("\n","",$answer_option);//ตัดการขึ้นบรรทัดทิ้งไป
		$answer_option	= str_replace("<br>',", "',<br>",$answer_option);//ขึ้นบันทัดใหม่
		$row_fol['answer_option'] = str_replace("<br>'", "'<br>",$answer_option);//ขึ้นบันทัดใหม่ของ option ตัวสุดท้าย
		$tr_list .= $tpl->tbHtml('ic_followcus_form.html','QUESTION_LIST');
	}
	mysql_free_result($result_list);
	if($tr_list==""){
		$colspan = 	8;
		$config['null'] = "ยังไม่มีรายการคำถามย่อย";
		$tr_list = $tpl->tbHtml('ic_followcus.html','NULL');
	}
	$question_list = $tpl->tbHtml('ic_followcus_form.html','QUESTION_LIST_HEAD');
	return $question_list;
}

function follow_auto_emp_name($keyword) {
	global $config,$logDb;
	$sign = "";
	$strSQL = "SELECT Name,Surname,Nickname,ID_card FROM $config[Member].emp_data ";
	$strSQL .= "WHERE Name LIKE '%".$keyword."%' OR Surname LIKE '%".$keyword."%' ORDER by Name,Surname,Nickname";
	mysql_query( "SET NAMES utf8" ) or die (mysql_error());
	$result= $logDb->queryAndLogSQL( $strSQL, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	$Num_Rows = mysql_num_rows($result);
	if($Num_Rows){
		while($rs = mysql_fetch_array($result)){
			if($sign==""){$sign="|";}
			$display .= $sign.$rs['ID_card']."_".$rs['Name'].' '.$rs['Surname'].' ('.$rs['Nickname'].")";
		}
	}
	return $display;
}

//ลิสต์รายชื่อฐานข้อมูลที่อยุ่ใน $config ที่ส่งมา
function get_db_name($opt=null){  
	global $config,$db_comment,$logDb;
	$sql = "SHOW DATABASES";
	$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	while($row = mysql_fetch_assoc($result)) {
		$a++;
		if($a==1){
			$color = "#FDFDFD";
		}else{
			$color = "#ECECEC";
			$a=0;
		}
		$selected = "";
		if($opt==$row['Database']){ $selected = "selected='selected'";}
		$comment = $db_comment[$row['Database']]['comment'];
		if($comment==""){$comment="ไม่มีคำอธิบาย";}
		$db .= "<option $selected style='background-color:$color;height:20px' value='$row[Database]' class='text-left'>&nbsp;&nbsp;$row[Database] ($comment)</option>";
	}
	mysql_free_result($result);
	return $db;
}

function getCancelPatternDate($id=null) {
	global $config,$logDb;
	$sql  = "SELECT id,pattern_name FROM $config[db_base_name].follow_cancel_pattern WHERE status != '99'";
	$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	while($rs = mysql_fetch_assoc($result)) {
		$selected = "";
		if($rs['id']==$id){$selected = "selected='selected'";}
		$option .= "<option $selected value='$rs[id]' class='text-left'>$rs[pattern_name]</option>";
	}
	if($id=='xx'){$option .= "<option selected='selected' value='xx' class='text-left'>สร้างกิจกรรมแบบไม่ระบุวันที่</option>";}
	return $option;
}

function getFieldNameActSpecificate(){
	global $config,$logDb;
	$sql  = "SELECT id,pattern_name FROM $config[db_base_name].follow_cancel_pattern WHERE status != '99'";
	$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	while($rs = mysql_fetch_assoc($result)) {
		$selected = "";
		if($rs['id']==$id){$selected = "selected='selected'";}
		$option .= "<option $selected value='$rs[id]' class='text-left'>$rs[pattern_name]</option>";
	}
	if($id=='xx'){$option .= "<option selected='selected' value='xx' class='text-left'>สร้างกิจกรรมแบบไม่ระบุวันที่</option>";}
	return $option;
}
?>