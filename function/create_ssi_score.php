<?php
//create date 11-8-2011 pt

//คำนวน แบบคำนวนชนิด A
//10-08-2011 เพิ่มคำนวนกรณีที่ คำถามไม่ถูกถาม
function compute_score_a($weight,$answer,$use_status=NULL){
	$count = count($weight);
	$i=0;
	$unuse = 0;
	$sum_weight = 0;
	$arr = array();
	$real_weight = array();

	for($i2=0;$i2<$count;$i2++){//หาผลรวมน้ำหนักทั้งหมด ที่ถูกตอบ
		if(is_string($weight[$i2])){$real_weight = parseInt($weight[$i2]);}
		if($use_status[$i2] !='no'){$sum_weight += $weight[$i2];}
	}

	if($sum_weight==0){$sum_weight = 100;}
	while($i < $count){
		if($use_status[$i] != 'no'){
			$real_ans = array();
			$real_weight = array();
			$real_weight = parseInt($weight[$i]);

			if (is_float($answer[$i])){
				$arr[] = ((($real_weight*100/$sum_weight)*$answer[$i])/100);
			}else {
				$real_ans = parseInt($answer[$i]);
				$arr[] = ((($real_weight*100/$sum_weight)*$real_ans)/100);
			}
		}else {$arr[] = 0;}
		$i++;
	}
	return $arr;
}
//คำนวน แบบคำนวนชนิด B
function compute_score_b($option_count,$answer,$use_status){
	$n=array();
	for($i=0;$i<count($option_count);$i++){
		if($use_status[$i]!= 'no'){
			$real_ans = array();
			$real_ans = explode("'", $answer[$i]);//05/08/2011

			if($real_ans[1] == ''){ //กรณี คำตอบไม่มี '' ;
				$real_ans[1] = $real_ans[0];
			}
			if ($option_count[$i]== $real_ans[1]){
				$n[] = 1;
			}else {
				$n[] = 0;
			}
		}
	}
	return $n;
}
function compute_title_b($title_w,$array_sum_list){
	$n=array();
	for($i=0;$i<count($title_w);$i++){

		$count_a = count($array_sum_list[$i]);
		if($count_a != 0){
			$point_a = array_sum($array_sum_list[$i]);
			$n[$i] = $point_a*100/$count_a;
		}
	}
	return $n;
}
function compute_form_b($title_w,$array_sum_list,$use_status){
	for($i=0;$i<count($title_w);$i++){
		$count_a += count($array_sum_list[$i]);
		if($count_a != 0){
			$point_a += array_sum($array_sum_list[$i]);
		}
	}
	if($count_a != 0){
		$n = $point_a*100/$count_a;
	}
	return $n;
}

function parseInt($string) { // MODIFYให้ ใช้กับทศนิยม ได้ 2011-11-08 PETE  * แก้ไขปัญหา การคำนวนพลาด
	$expString = explode(".",$string);
	//"[+-]?\\d+(.\\d+)?"  <--- isNumeric ในจาวา
	if(count($expString)>1){
		if(preg_match('/(\d+.\d+)/', $string, $array)) {
			return $array[1];
		} else {
			return 0;
		}
	}else{
		if(preg_match('/(\d+)/', $string, $array)) {
			return $array[1];
		} else {
			return 0;
		}
	}
}

//ของเดิม ไม่ใช่แล้ว pt 2011-11-08
function OLD_parseInt($string) {
	if(preg_match('/(\d+)/', $string, $array)) {
		return $array[1];
	} else {
		return 0;
	}
}
?>