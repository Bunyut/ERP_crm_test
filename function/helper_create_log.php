<?php
/**
  * ฟังก์ชั่นเก็บ Log การทำงาน มีทั้งฟังก์ชั่นเขียนไฟล์ และ insert ลงในตาราง ZLogEditAllTable
  * ถ้าจะติดต่อฐานข้อมูล จะต้องอยู่ภายใต้ไฟล์คอนฟิกของระบบ เช่นหากไฟล์คอนฟิกอยู่ที่ include/config/config.php
  * ให้เขียนดังนี้
  * require_once('include/config/config.php');
  * require_once('function/helper_create_log.php');
  *
  * $config[db_base_name] คือ คอนฟิกฐานข้อมูลที่เก็บเทเบิล ZLogEditAllTable
  */

session_start();

if( ! function_exists('show_error') ){
	/**
	 * แสดงข้อความผิดพลาด และหยุดการทำงาน
	 * @param String $file ส่งชื่อไฟล์เพื่อให้รู้ว่าเป็นไฟล์ไหน __FILE__
	 * @param String $line ส่งค่าบรรทัดที่เกิดข้อผิดพลาด __LINE__
	 * @param String $sql ส่งตัวแปร SQL เพื่อแสดงคำสั่ง sql ที่ใช้
	 */
	function show_error($file,$line=null,$sql=null){
		echo '<div style="font-family:verdana,arial,tahoma;color:saddlebrown;margin:10px;padding:10px;
		border:1px solid #cccccc;background-color:#FFFFCC;font-size:12px;">
		<font color="red"><b>ERROR</font> :: '.$file.' line '.$line.'</b><br>
		<font color="red">Message :: '.mysql_error().'</font><br>';
		if($sql){echo 'SQL :: [ <font color="green">'.nl2br($sql).'</font> ]';}
		echo '</div>';
		exit();
	}
}//<-//

#-- back up & record log data ด้วยฟังก์ชั่น insert_log()
if(!function_exists('insert_log')){
	/**
 	*
	*สำหรับ บันทึกข้อมูลลงในฐานข้อมูล ZLogEditAllTable
	*@param Array $arrayData = array(
  					'table_name'=>'',//ชื่อตาราง
  					'primary_id'=>'',//รหัสอ้างอิง
  					'user_session'=>'',//ผู้ใช้ที่ต้องการเก็บล็อก
  					'array_data'=>'',//ข้อมูลที่ต้องการเก็บ
  					'remark'=>'',//ข้อความเพิ่มเติม
  					'status'=>''//สถานะ 1=แก้ไข, 2=ลบ
  				);
   */
	function insert_log($arrayData)
	{
		global $conn,$config;
		if(!$conn && $config['ServerName']!=''){//มีคอนฟิก แต่ไม่ได้เชื่อมต่อฐานข้อมูล
			$conn = mysql_connect( $config['ServerName'], $config['UserName'], $config['UserPassword'] );
			mysql_select_db( $config['db_base_name'], $conn )	or die(  $config['db_base_name'].'  ติดต่อไม่ได้ ในระบบ Conn2DB '.  mysql_error() );
		}
		if($conn){//ถ้ามีการเชื่อมต่อแล้ว
		    //ข้อมูลที่จำเป็นต้องมี
		    if($arrayData['table_name']!='' && $arrayData['primary_id']!='' && $arrayData['user_session'] && $arrayData['array_data']!=''){
		        $arrayData[array_data] = addslashes($arrayData[array_data]);
			    $logInsertSQL = "INSERT INTO $config[db_base_name].ZLogEditAllTable (TableName, table_id_ref, Fill_Id, Edit_Arry, Time_date, remark, status)VALUES('$arrayData[table_name]', '$arrayData[primary_id]', '$arrayData[user_session]', '$arrayData[array_data]', NOW(), '$arrayData[remark]', '$arrayData[status]') ";
			    @mysql_query("SET NAMES UTF8");
			    @mysql_query($logInsertSQL) or die('บันทึกข้อมูลไม่ได้ อาจเกิดจากยังไม่ได้เรียกคำสั่งเชื่อมต่อฐานข้อมูล หรือข้อมูลที่ส่งมามีอักขระพิเศษอยู่ด้วย<br>'. mysql_error());
			    unset($logInsertSQL);
			}
		}
		unset($arrayData);
	}
}

if( ! function_exists('insert_backup_from_sql') )
{
	/**
	 * สร้างข้อมูลแบ็กอัพ ใส่ไว้ใน ZLogEditPhaseF
	 * @param Array $arr[table] ชื่อตารางที่ทำการแบ็กอัพ
	 * 					[where] เลือกเรคอร์ดที่จะแบ็กอัพ (เงื่อนไขเดียวกับเรคอร์ดที่ลบ/แก้ไข)
	 * 					[line] บรรทัดที่เรียกใช้ฟังก์ชั่น กรณีเกิดข้อผิดพลาดจะได้รู้ว่าอยู่ส่วนไหน (__LINE__)
	 * 					[remark] คำอธิบายเพิ่มเติม
	 * 					[action_type] ประเภทการเปลี่ยนแปลงข้อมูล 1 = edit/update 2 = delete
	 */
	function insert_backup_from_sql($arr, $table_log=null )
	{
		global $conn,$config;
		if(!$conn && $config['ServerName']!=''){//มีคอนฟิก แต่ไม่ได้เชื่อมต่อฐานข้อมูล
			$conn = mysql_connect( $config['ServerName'], $config['UserName'], $config['UserPassword'] );
			mysql_select_db( $config['db_base_name'], $conn )	or die(  $config['db_base_name'].'  ติดต่อไม่ได้ ในระบบ Conn2DB '.  mysql_error() );
		}
		$line = $arr['line'];
		if(empty($table_log)){
			$table_log="$config[db_base_name].ZLogEditPhaseF";
		}
		
		mysql_query('SET NAMES UTF8');

		//หาคีย์ที่เป็น PRIMARY
		$sql = "SHOW KEYS FROM $arr[table] WHERE Key_name = 'PRIMARY'";
		$result = mysql_query($sql) or show_error($_SERVER['PHP_SELF'], $line.'/'.__LINE__);
		$rs = mysql_fetch_assoc($result);
		$keys_name = $rs['Column_name'];

		//วนลูปบันทึกลง ตาราง BACKUP
		$who_edit = $_SESSION['SESSION_ID_card'];
		$sql = "SELECT * FROM $arr[table] WHERE $arr[where] ";
		$result = mysql_query($sql) or show_error($_SERVER['PHP_SELF'], $line.'/'.__LINE__);
		while($rec = mysql_fetch_assoc($result))//วนลูปบันทึกตาม SQL ที่ส่งมา
		{
			$json_data = addslashes(json_encode($rec));
			$sql = "INSERT INTO $table_log
			    	(table_name, 	primary_fieldname, 	primary_id, 		old_data, 		who_edit, 	action_date, remark, 		action_type)VALUES
			    	('$arr[table]', '$keys_name', 		'$rec[$keys_name]', '$json_data', 	'$who_edit', NOW(), 	'$arr[remark]', '$arr[action_type]') ";
			mysql_query("SET NAMES UTF8");
			mysql_query($sql) or show_error($_SERVER['PHP_SELF'], $line.'/'.__LINE__, $sql);
		}
	}
}

if( ! function_exists('insert_log_history') )
{
	/**
	 * สร้างข้อมูลแบ็กอัพ ใส่ไว้ใน ZLogHistory
	 * @param Array $arr[table] ชื่อตารางที่ทำการแบ็กอัพ
	 * 					[where] เลือกเรคอร์ดที่จะแบ็กอัพ (เงื่อนไขเดียวกับเรคอร์ดที่ลบ/แก้ไข)
	 * 					[line] บรรทัดที่เรียกใช้ฟังก์ชั่น กรณีเกิดข้อผิดพลาดจะได้รู้ว่าอยู่ส่วนไหน (__LINE__)
	 * 					[remark] คำอธิบายเพิ่มเติม
	 * 					[action_type] ประเภทการเปลี่ยนแปลงข้อมูล 1 = edit/update 2 = delete
	 */
	function insert_log_history($arr)
	{
		global $conn,$config;
		if(!$conn && $config['ServerName']!=''){//มีคอนฟิก แต่ไม่ได้เชื่อมต่อฐานข้อมูล
			$conn = mysql_connect( $config['ServerName'], $config['UserName'], $config['UserPassword'] );
			mysql_select_db( $config['db_base_name'], $conn )	or die(  $config['db_base_name'].'  ติดต่อไม่ได้ ในระบบ Conn2DB '.  mysql_error() );
		}

		if(gettype($arr['old_data'])=='array'){
			$arr['old_data'] = json_encode($arr['old_data']);
		}
		$old_data = addslashes($arr['old_data']);
		$who_edit = $_SESSION['SESSION_ID_Card'];
		$sql = "INSERT INTO $config[db_base_name].ZLogHistory
		    	(table_name, 	primary_fieldname, 				primary_id, 		old_data, 		who_edit, 		action_date,	 remark, 		action_type)VALUES
		    	('$arr[table_name]', '$arr[primary_fieldname]', '$arr[primary_id]', '$old_data', 	'$who_edit', 	NOW(), 			'$arr[remark]', '$arr[action_type]') ";
		mysql_query("SET NAMES UTF8");
		mysql_query($sql) or show_error($_SERVER['PHP_SELF'], __LINE__, $sql);

	}
}

if( ! function_exists('create_log_file') ){
/**
  * ใช้สำหรับเขียนข้อมูล Array ลงในไฟล์ CSV
  * @param string $filename ชื่อไดเรกทอรี่/ชื่อไฟล์ ที่จะเขียนเก็บไว้
  * @param Array $dataArray ข้อมูลที่จะบันทึกเก็บเป็นคอลัมน์
   			$dataArray = array(
   						'TITLE'	=> 'คำอธิบาย',///** ถ้ามีจะง่ายในการอ่าน Log
						'DATA1' => 'ทดสอบ1',
						'DATA2' => 'ทดสอบ2',
						'DATA....' => 'ทดสอบ....',
						'FILE' 	=> __FILE__,//*** จำเป็นต้องมี ไว้เป็นตัวท้ายๆจะดูดีกว่า
						'LINE'	=> __LINE__,
					);
	@param String $mainpath ชื่อไดเรกทอรี่หลักที่ใช้เก็บไฟล์ ค่าเริ่มต้น files/log_csv
  * @return สร้างไฟล์ CSV ตามค่าที่ส่งมาในโฟลเดอร์ files/log_csv/yyyy-mm-dd/$filename.csv
  */
	function create_log_file($filename=null,$dataArray=null,$mainpath=null)
	{
		if($filename!='' && substr($dataArray['FILE'],strrpos($dataArray['FILE'],'.'))=='.php'){//FILE คือชื่อไฟล์ที่ฝังสคริปต์ไว้ เพื่อให้รุ้ว่าฝัง log ไว้ที่ไหนบ้าง
			//บังคับให้เขียนลงในโฟลเดอร์ files
			if($mainpath==''){$mainpath = 'files/log_csv';}
			if( ! is_dir($mainpath) ){
				mkdir($mainpath, 0777, true);
				chmod($mainpath, 0777);
			}
			//$file = 'files/log_csv/'.$filename.'_'. date('m_Y') .'.csv';
			$file =  $mainpath.'/'.date('Y-m-d').'/'.$filename.'.csv';
			$title = '';
			$data = '';
			if( ! file_exists($file) ){//ถ้าเขียนไฟล์นี้ครั้งแรก
				if($dataArray){
					foreach($dataArray as $key=>$val){
						if( ! abs($key) ){$title .= $tsign.'"'.$key.'"';} //ถ้าไม่ใช่ตัวเลข Index จะเขียนหัวข้อไว้แถวแรกสุด
						if($tsign==''){$tsign=',';}
					}
				}

				$path = substr($file, 0, strrpos($file, '/' ));
				if( ! is_dir($path) ){
					mkdir($path, 0777, true);
					chmod($path, 0777);
				}

				$fp = @fopen($file, 'a');
				if($title){@fwrite($fp, $title."\n");}
				@fclose($fp);
			}
			$fp = @fopen($file, 'a');
			$data = '"'. implode('","',$dataArray) .'"'."\n";
			$data = html_entity_decode($data, ENT_NOQUOTES, 'UTF-8');
			@fwrite($fp, $data);
			@fclose($fp);
			unset($data);
			unset($dataArray);
		}else {
			if($_SESSION['SESSION_username']=='admin'){//แสดงข้อผิดพลาดให้เฉพาะ admin เห็น
				echo '<p style="color:red">ข้อมูลไม่ครบ <br>';
				if($filename==''){ echo '$filename = ชื่อไฟล์สำหรับเขียนข้อมูล log file<br>';}
				if($dataArray['FILE']==''){ echo '$dataArray[FILE] = ชื่อไฟล์ที่ฝังสคริปต์เก็บ LOG (ไม่มีส่งมาด้วย)<br>';}
				echo 'หรือ FILE = "'. substr($dataArray['FILE'],strrpos($dataArray['FILE'],'.')).'" [X] ไม่ใช่ชื่อสคริปต์';
				echo '</p>';
			}
		}
	}
}

if( !function_exists('my_diff2time')){
	/**
	 * คำนวณหาเวลาที่ใช้ไป
	 * @param Datetime $time1 เวลาเริ่ม yyyy-mm-dd H:i:s
	 * @param Datetime $time2 เวลาสิ้นสุด yyyy-mm-dd H:i:s
	 */
	function my_diff2time($time1, $time2, $array=null)
	{
		if($time1!='' && $time2!=''){
		    $now_time1=strtotime($time1);
		    $now_time2=strtotime($time2);
		    $time_diff=abs($now_time2-$now_time1);
		    $day = floor($time_diff/86400);				//จำนวนวัน
		    $time_diff_h=floor(($time_diff/3600)%24); 	// จำนวนชั่วโมงที่ต่างกัน
		    $time_diff_m=floor(($time_diff%3600)/60); 	// จำนวนนาทีที่ต่างกัน
		    $time_diff_s=($time_diff%3600)%60; 			// จำนวนวินาทีที่ต่างกัน
		    if($array==true){
		    	return array('day'=>$day, 'hour'=>$time_diff_h, 'min'=>$time_diff_m, 'sec'=>$time_diff_s);
		    }else{
		   		return "$day [วัน], $time_diff_h [ชั่วโมง], $time_diff_m [นาที], $time_diff_s [วินาที]";
		    }
		}
	}
}

if( ! function_exists('log_user_access') ){
/**
  * ใช้สำหรับสร้าง LOG file เก็บไว้ในไดเรกทอรี่ /files/log_csv/yyyy-mm-dd/log_user_access.csv
  * เก็บเป็นข้อมูล Browser ที่ใช้งานเท่านั้น หากต้องการเก็บรายละเอียดปลีกย่อยเพิ่มเติมให้ใช้ฟังก์ชั่น create_log_file() ต่างหาก
  * @param string $path พาธไดเรกทอรี่/ชื่อไฟล์ที่จะเขียน ใส่แค่ log_user_access ผลลัพธ์เก็บไว้ที่ /files/log_csv/yyyy-mm-dd/log_user_access.csv
  * @param string $title คำอธิบาย
  * @param string $file เก็บจุดที่วางฟังก์ชั่นเก็บล็อกจะเป็นนค่าจาก __FILE__
  * @param String $getType กำหนดว่าจะดึงค่า Browser เป็นอาร์เรย์ หรือ String
  */
	function log_user_access($path=null,$title='-',$file=null,$getType=null)
	{
		$time = my_diff2time($_SESSION['cus_manage_time'], date('Y-m-d H:i:s'), true);
		if($_SESSION['LOG_SESS_ID'] != session_id() || $time['min'] >= 5 ){//จะไม่เก็บค่าจาก session เดิม ถ้าไม่ถึง 5 นาที
				$LogArray['FILE']		= $file;//*** Require
				$LogArray['TITLE'] 		= $title;
				$LogArray['USER']		= $_SESSION['SESSION_Name'].' '.$_SESSION['SESSION_Surname'].'('.$_SESSION['SESSION_Nickname'].') - '.iconv('tis-620','utf-8',$_SESSION['SESSION_username']);
				$LogArray['ACCESS_TIME']	= date('Y-m-d H:i:s');
				$LogArray['IP_ADDRESS']	= $_SERVER["REMOTE_ADDR"];
				$LogArray['BROWSER']	= getBrowser($getType);	//this file
				create_log_file($path, $LogArray);				//this file
				$_SESSION['LOG_SESS_ID'] = session_id();
				$_SESSION['cus_manage_time'] = date('Y-m-d H:i:s');
		}
	}
}

// แปลง &#3623;&#3636;&#3594;&#3640;&#3604;&#3634; เป็นภาษาไทย
// ใช้เหมือนกับ html_entity_decode($string,ENT_NOQUOTES,'UTF-8')
if( ! function_exists('html_entity_decode')){
	function html_entity_decode($str,$type=null,$charset=null) {
		function unichr($dec) {
		  if ($dec < 128) {
		    $utf = chr($dec);
		  } else if ($dec < 2048) {
		    $utf = chr(192 + (($dec - ($dec % 64)) / 64));
		    $utf .= chr(128 + ($dec % 64));
		  } else {
		    $utf = chr(224 + (($dec - ($dec % 4096)) / 4096));
		    $utf .= chr(128 + ((($dec % 4096) - ($dec % 64)) / 64));
		    $utf .= chr(128 + ($dec % 64));
		  }
		  return $utf;
		}
		return preg_replace("/&#(\d{2,5});/e", "unichr($1);", $str);
	}
}



//--------LOG MySQL Error-------//
function mysql_error_log($log_error_title='บันทึกข้อมูลความผิดพลาด ระหว่างคิวรี่ MySQL',$sql=null,$line='--')
{
		$LogArray = array();
		$LogArray['FILE']			= __FILE__;
		$LogArray['SELF']			= $_SERVER['PHP_SELF'].' line '.$line;
		$LogArray['TITLE'] 			= $log_error_title;
		$LogArray['ERROR_TIME']  	= date('Y-m-d H:i:s');
		$LogArray['ERROR_MSG']  	= addslashes(mysql_error().' => <font color="red">'.$sql.'</font>');
		create_log_file('MySQL_Error', $LogArray);
		unset($logArray);
}
//------------------------------//

/**
  * @param string ถ้าต้องการคืนค่าแบบอาร์เรย์ ให้กำหนดเป็นคำว่า 'array'
  *			ถ้าต้องการข้อมูลทั้งหมด กำหนดเป็น 'all'
  *			ถ้าไม่กำหนดค่าเลย จะตัดเฉพาะส่วนของเบราเซอร์ เวอร์ชั่น และแพลตฟอร์มเท่านั้น
  */
if( ! function_exists('getBrowser')){
	function getBrowser($type=null)
	{
		$u_agent = $_SERVER['HTTP_USER_AGENT'];
		$arr = '';
		if($type=='all'){//ดูทั้งหมดไม่ต้องตัด
	    		$arr = $u_agent;
	   	}else{//ตัดเฉพาะส่วนที่ต้องการ
		    $bname = 'Unknown';
		    $platform = 'Unknown';
		    $version= "";
		    //First get the platform?
		    if (preg_match('/linux/i', $u_agent)) {
		        $platform = 'Linux';
		    }
		    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
		        $platform = 'Mac';
		    }
		    elseif (preg_match('/windows|win32/i', $u_agent)) {
		        $platform = 'Windows';
		    }

		    // Next get the name of the useragent yes seperately and for good reason
		    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
		    {
		        $bname = 'Internet Explorer';
		        $ub = "MSIE";
		    }
		    elseif(preg_match('/Firefox/i',$u_agent))
		    {
		        $bname = 'Mozilla Firefox';
		        $ub = "Firefox";
		    }
		    elseif(preg_match('/Chrome/i',$u_agent))
		    {
		        $bname = 'Google Chrome';
		        $ub = "Chrome";
		    }
		    elseif(preg_match('/Safari/i',$u_agent))
		    {
		        $bname = 'Apple Safari';
		        $ub = "Safari";
		    }
		    elseif(preg_match('/Opera/i',$u_agent))
		    {
		        $bname = 'Opera';
		        $ub = "Opera";
		    }
		    elseif(preg_match('/Netscape/i',$u_agent))
		    {
		        $bname = 'Netscape';
		        $ub = "Netscape";
		    }

		    // finally get the correct version number
		    $known = array('Version', $ub, 'other');
		    $pattern = '#(?<browser>' . join('|', $known) .
		    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
		    if (!preg_match_all($pattern, $u_agent, $matches)) {
		        // we have no matching number just continue
		    }

		    // see how many we have
		    $i = count($matches['browser']);
		    if ($i != 1) {
		        //we will have two since we are not using 'other' argument yet
		        //see if version is before or after the name
		        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
		            $version= $matches['version'][0];
		        }
		        else {
		            $version= $matches['version'][1];
		        }
		    }
		    else {
		        $version= $matches['version'][0];
		    }

		    // check if we have a number
		    if ($version==null || $version=="") {$version="?";}

		    if($type=='array'){
		    		$arr['browser'] = $bname;
		   		$arr['version'] = $version;
		   		$arr['platform'] = $platform;
		   		$arr['all'] = $u_agent;
		    }else {
		    		$arr = $bname.' version '.$version.' ('.$platform.')';
		    }
		}//end all
	   return $arr;
	}
}

?>