<?php
	class importClist{
		private $config;
		private $logDb;

		function __construct(){
			global $config, $logDb;
			
			$this->config	= $config;
			$this->logDb	= $logDb;
		}
		
		## สร้างคำสั่ง sql where เฉพาะ AND
		public function genWhere($arr){
			$res = array('txt' => '');
		
			foreach($arr AS $key=>$value){
				$res['txt']	.= "AND ".$key." = '".$value."' ";
			}
			
			return $res;
		}
		
		## เปลี่ยนคำสั่ง query ให้เป็นรูปแบบ array
		private function convertQueryToArray($que){
			$res	= array();
			
			while($fe = mysql_fetch_assoc($que)){
				$res[]	= $fe;
			}
			
			return $res;
		}
		
		## ดึงข้อมูล Clist
		public function selectClistIOS($arr){
			$config	= $this->config;
			$logDb	= $this->logDb;
			
			$where	= $this->genWhere($arr['where']);
			
			$sql = "SELECT ".$arr['field']." ";
			$sql .= "FROM ".$config['db_easysale'].".CList_IOS ";
			$sql .= "WHERE 1=1 ";
			$sql .= " ".$where['txt']." ";
			$sql .= " ".$arr['other_where']." ";
			$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$fe  = $this->convertQueryToArray($que);
			
			return $fe;
		}
		
		## ดึงข้อมูลการขาย
		public function selectSell($arr){
			$config	= $this->config;
			$logDb	= $this->logDb;
			
			$where	= $this->genWhere($arr['where']);
			
			$sql = "SELECT ".$arr['field']." ";
			$sql .= "FROM ".$config['db_easysale'].".Sell ";
			$sql .= "WHERE 1=1 ";
			$sql .= " ".$where['txt']." ";
			$sql .= " ".$arr['other_where']." ";
			$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$fe  = $this->convertQueryToArray($que);
			
			return $fe;
		}
		
		## ดึงข้อมูลลูกค้า
		public function selectCustomer($arr){
			$config	= $this->config;
			$logDb	= $this->logDb;
			
			$where	= $this->genWhere($arr['where']);
			
			$sql = "SELECT ".$arr['field']." ";
			$sql .= "FROM ".$config['Cus'].".MainCusData ";
			$sql .= "WHERE 1=1 ";
			$sql .= " ".$where['txt']." ";
			$sql .= " ".$arr['other_where']." ";
			$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$fe  = $this->convertQueryToArray($que);
			
			return $fe;
		}
		
		## ดึงข้อมูลพนักงาน
		public function selectEmp($arr){
			$config	= $this->config;
			$logDb	= $this->logDb;
			
			$where	= $this->genWhere($arr['where']);
			
			$sql = "SELECT ".$arr['field']." ";
			$sql .= "FROM ".$config['db_emp'].".emp_data ";
			$sql .= "INNER JOIN ".$config['db_organi'].".position ";
			$sql .= "ON TRIM(emp_data.Code) = TRIM(position.PositionCode) ";
			$sql .= "WHERE 1=1 ";
			$sql .= " ".$where['txt']." ";
			$sql .= " ".$arr['other_where']." ";
			$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$fe  = $this->convertQueryToArray($que);
			
			return $fe;
		}
		
		## เช็คประวัติขาย
		public function checkHistorySale($arr){
			$config	= $this->config;
			$logDb	= $this->logDb;
			
			## ค้นหาประวัติขาย
			$arrChk['field']				= 'Sell_No, SUBSTRING_INDEX(cusBuy, "_", 1) AS cusNo';
			$arrChk['where']['Eng_No_S']	= $arr['Engine'];
			$arrChk['where']['Chassi_No_S']	= $arr['VIN'];
			$arrChk['other_where']			= 'LIMIT 1';
			$resSell						= $this->selectSell($arrChk);
			
			if(!empty($resSell)){
				$resSell['res_status']	= true;
			}else{
				$resSell['res_status']	= false;
			}
			
			return $resSell;
		}
	
		## เช็คว่ามีการแก้ไขผู้รับชอบหรือไม่
		public function checkRespMainCus($arr){
			$config	= $this->config;
			$logDb	= $this->logDb;
			
			// $sql = "SELECT RESP_ID ";
			// $sql .= "FROM ".$config['db_maincus'].".MAIN_RESPONSIBILITY ";
			// $sql .= "WHERE RESP_CUSNO = '".$arr['cus_no']."' ";
			// $sql .= "AND RESP_IDCARD = '".$arr['emp_no']."' ";
			// $sql .= "AND RESP_DATE = '".$arr['resp_date']."' LIMIT 1";
			$sql = "SELECT CusNo ";
			$sql .= "FROM ".$config['Cus'].".MainCusData ";
			$sql .= "WHERE CusNo = '".$arr['cus_no']."' ";
			$sql .= "AND Resperson = '".$arr['emp_no']."' ";
			$sql .= "AND Resperson_posCode = '".$arr['position']."' ";
			$sql .= "AND respon_date = '".$arr['resp_date']."' LIMIT 1";
			$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$row = mysql_fetch_assoc($que);
			
			if($row > 0){
				return true;
			}else{
				return false;
			}
		}
		
		## option พนักงาน
		public function optionEmp($arr){
			$optionEmp 	= '';
			$selectBox	= '';
		
			$resEmp		= $arr;
			unset($resEmp['no']);
			unset($resEmp['default']);
			unset($resEmp['name']);

			foreach($resEmp AS $kEmp => $vEmp){
				$arrVal 	= array('ID_Card'=>$vEmp['ID_Card'], 'ScName' => $vEmp['TIS_id'], 'Code' => $vEmp['Code']);
				$value		= json_encode($arrVal);
				
				$empName	= $vEmp['Name'].' '.$vEmp['Surname'];
				if($arr['default']	== $empName){
					$selected	= 'selected';
				}else{
					$selected	= '';
				}
			
				$optionEmp	.= '<option value=\''.$value.'\' '.$selected.'>'.$vEmp['TIS_id'].' ('.$vEmp['position'].') : '.$empName.'</option>';
			}
			
			if($optionEmp){
				$selectBox	.= '<select id="'.$arr['name'].'_'.$arr['no'].'" name="'.$arr['name'].'[]" style="width: 250px;">';
				$selectBox	.= $optionEmp;
				$selectBox	.= '</select>';
			}else{
				$selectBox	= '';
			}
			
			return $selectBox;
		}

		## บันทึกลงข้อมูลลูกค้า
		public function saveReperson($arr){
			$config	= $this->config;
			$logDb	= $this->logDb;
			
			## บันทึกลง main_cusdata
			$update	= "UPDATE ".$config['Cus'].".MainCusData ";
			$update	.= "SET Resperson = '".$arr['emp_no']."', ";
			$update	.= "Resperson_posCode = '".$arr['position']."', ";
			$update	.= "respon_date = '".$arr['resp_date']."' ";
			$update	.= "WHERE ";
			$update .= "CusNo = '".$arr['cus_no']."' ";
			$update .= "LIMIT 1 ";
			$logDb->queryAndLogSQL( $update, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			
			if($arr['sync'] == true){
				## sync ฐานข้อมูลลูกค้ากลาง
				$ve = new manageMain_data('crm');
				$ve->manageCustomer($arr['cus_no']);
			}
		}
		
		## ลบข้อมูลการติดตาม
		public function delFollowCustomerResp($arr){
			$config	= $this->config;
			$logDb	= $this->logDb;
			
			## ลบข้อมูลตาราง follow_customer
			$update = "UPDATE ".$config['db_base_name'].".follow_customer ";
			$update .= "SET status = 99 ";
			$update .= "WHERE ";
			$update .= "cus_no = '".$arr['cus_no']."' ";
			$update .= "AND emp_id_card = '".$arr['emp_no']."' ";
			$update .= "AND emp_posCode = '".$arr['position']."' ";
			$update .= "AND status IN(0,1) ";
			$logDb->queryAndLogSQL( $update, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		}
	}
?>