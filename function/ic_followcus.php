<?php
//function for ic_followcus
//config นี้ย้ายไปไว้ที่อื่นไม่ได้ เพราะมีการเรียกจากหลายหน้า
$config['phF_checkDepartment']  = substr($_SESSION['SESSION_member_id'], $config['start_event'], $config['cut_event']);
$config['phF_CheckSale']		= substr($_SESSION['SESSION_member_id'],$config['startCheckSALE'],$config['cutCheckSALE']);


//fix cut department for sale ไม่งั้นรายการที่ส่งต่อไปแผนกขายจะไม่สามารถแสดงขึ้นมาได้
if($config['phF_CheckSale']=="1"){
	$config['phF_checkDepartment'] = "1";
}

if (!function_exists('set_order')) {
	function set_order($session) {//สำหรับตาราง follow_customer
		global $config;
		//$cusName = "IF(SUBSTR(cus_name,1,1) in ('เ','แ','ไ','ใ','โ'),CONCAT(SUBSTR(cus_name,2,1),SUBSTR(cus_name,1,1),SUBSTR(cus_name,3)),cus_name)";
		$cusName	= "CONVERT(cus_name USING TIS620)";
		#-- $empName ต้องใช้กับการ JOIN แล้วทำการ CONCAT_WS(' ',Name,Surname,'(',Nickname,')') as emp_name ข้อมูลในตาราง emp_data ในฐานข้อมูลพนักงาน
		//$empName = "IF(SUBSTR(emp_name,1,1) in ('เ','แ','ไ','ใ','โ'),CONCAT(SUBSTR(emp_name,2,1),SUBSTR(emp_name,1,1),SUBSTR(emp_name,3)),emp_name)";
		$empName = "CONVERT(emp_name USING TIS620)";
		switch($session){
			case "cusno_asc" 		: $order = "cus_no ,stop_date";		break;
			case "cusno_desc" 		: $order = "cus_no DESC,stop_date";	break;
			case "cusname_asc" 		: $order = "$cusName ,stop_date";		break;
			case "cusname_desc" 		: $order = "$cusName DESC,stop_date";	break;
			case "expire_date_asc" 	: $order = "stop_date,$cusName"; 		break;
			case "expire_date_desc" : $order = "stop_date DESC,$cusName"; 	break;
			case "process_date_asc" : $order = "process_date,$cusName ,stop_date"; 	break;
			case "process_date_desc": $order = "process_date DESC,$cusName ,stop_date";break;
			case "empname_asc" 		: $order = "$empName ,stop_date";		break;
			case "empname_desc" 		: $order = "$empName DESC,stop_date";	break;
			default 						: $order = "stop_date,$cusName"; break;//default = รายการที่ต้องทำก่อน และชื่อลูกค้า ก - ฮ
		}
		return $order;
	}
}
if (!function_exists('setDateFormat')) {
	function setDateFormat($date){//สร้างรูปแบบของวันที่ yyyy-mm-dd
		if(ereg("([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})",$date,$arr) || ereg("([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})",$date,$arr) ){
			//ถ้าเป็น xx-xx-yyyy หรือ xx/xx/yyyy
			$y = $arr[3];
			$m = sprintf("%02d",$arr[2]);
			$d = sprintf("%02d",$arr[1]);
		}else if(ereg("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})",$date,$arr) || ereg("([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})",$date,$arr)){
			//ถ้าเป็น yyyy-xx-xx หรือ yyyy/xx/xx
			$y = $arr[1];
			$m = sprintf("%02d",$arr[2]);
			$d = sprintf("%02d",$arr[3]);
		}
		if(($y!="" && $m != "" && $d != "") and ($y!= '0000' && $m != '00' && $d != '00')){
			if(checkdate($m,$d,$y)==true){
				$newdate = $y."-".$m."-".$d;
			}else {
				$newdate = "วันที่นี้ไม่มีอยู่จริง $y-$m-$d yyyy/mm/dd";
			}
			return $newdate;//yyyy-mm-dd
		}
	}
}

// ESC097-002 : new popup : create by Pichid : 2015 Sep 21
// เรียกใช้จาก /ERP_crm/inc/crm_popup.php -> getFollow_crm();
if (!function_exists('getNumFollowCusEventNew')) {
	function getNumFollowCusEventNew($dayExpire = 0) {
		global $config, $logDb;

		randomCus();//สุ่มรายชื่อลูกค้าของ id_card ตัวเอง

		$whereOu = condition_sortOU(select_empOu($_SESSION['SESSION_ID_card'],'session'),''); //function/general.php

		# P'Add 2012-11-28 ให้แสดงรายการตามสายงาน
		$emp_id_card = getEmp_follower($_SESSION['SESSION_ID_card']); //function/general.php
		if($emp_id_card){ 
			// $condition = "AND (process_by IN($emp_id_card) OR emp_id_card IN($emp_id_card))"; 
			$condition  = "AND (process_by NOT IN('updateExpire99','updateExpire98','updateExpire95','updateExpire1','updateExpire') OR emp_id_card IN($emp_id_card))"; 

			#$condition ="AND (process_by = '' OR process_by = '-') AND follow_main_event.follow_up_activities = '2' AND emp_id_card = '' "; 

			$join = "INNER JOIN $config[db_base_name].follow_main_event ";
			$join .= "ON $config[db_base_name].follow_customer.event_id_ref = $config[db_base_name].follow_main_event.id ";
			
		}
		# End P'Add 2012-11-28 ให้แสดงรายการตามสายงาน
		else {
			$today = date('Y-m-d');
			$condition = "AND (start_date <= '$today' AND stop_date >= '$today') ";
		}

		$whereExpire = "AND (follow_customer.stop_date BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d') AND DATE_FORMAT(DATE_ADD(NOW(), INTERVAL ".abs(intval($dayExpire - 1))." DAY), '%Y-%m-%d')) ";

		$sql = "SELECT TB_EVT.event_title, TB_EVT.id, COUNT(TB_EVT.id) AS CNT_CUST 
			FROM (
				SELECT follow_main_event.id, follow_main_event.event_title, follow_customer.stop_date
				FROM $config[db_base_name].follow_main_event 
				INNER JOIN $config[db_base_name].follow_customer ON follow_customer.event_id_ref = follow_main_event.id 
				WHERE follow_main_event.`status` != '99' 
				AND follow_customer.`status` = '1'
				AND $whereOu $condition $whereExpire 
				ORDER BY follow_customer.stop_date ASC 
			) AS TB_EVT
			GROUP BY TB_EVT.id
			ORDER BY TB_EVT.stop_date ASC, TB_EVT.id ASC 
			LIMIT 5";

		// echo "<br />",$sql;

		$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$result = array();
		while ($rs = mysql_fetch_assoc($re)) {
			$result[] = $rs;
		}
		mysql_free_result($re);
		return $result;
	}
}
if (!function_exists('condition_sortOU')) {
	function condition_sortOU($arr,$db=null){
		if($db){ $db .= "."; }
		$return .= "(";
		foreach($arr AS $key=>$value){
			if($return!="(" && $key!="code"){ $return .= " OR "; }
			switch($key){
				case 'id_card':
					$return .= $db."emp_id_card='".$value."'";
				break;
				case 'position':
					$return .= $db."follow_resps_position='".$value."'";
				break;
				case 'section':
					$return .= $db."follow_resps_section='".$value."'";
				break;
				case 'department':
					$return .= $db."follow_resps_department='".$value."'";
				break;
				case 'company':
					$return .= $db."follow_resps_comp='".$value."'";
				break;
			}		
		}
		$return .= ")";
		
		return $return;
	}
}
if (!function_exists('select_empOu')) {
	function select_empOu($id_card,$status=null){
		global $config, $logDb;
		
		if($status=='session'){
			if($_SESSION['SESSION_ID_card']){
				$return['id_card'] = $_SESSION['SESSION_ID_card'];
				$return['position'] = $_SESSION['SESSION_Position_id'];
				$return['section'] = $_SESSION['SESSION_Section'];
				$return['department'] = $_SESSION['SESSION_Department'];
				$return['company'] = $_SESSION['SESSION_Working_Company'];
				$return['code'] = $_SESSION['SESSION_member_id'];
			}else{
				$return['id_card'] = 'noSession';
				$return['position'] = 'noSession';
				$return['section'] = 'noSession';
				$return['department'] = 'noSession';
				$return['company'] = 'noSession';
				$return['code'] = 'noSession';
			}
		}else{
		
			$sql  = "SELECT rel.id_card,pos.id AS position,pos.Section AS section, ";
			$sql .= "pos.Department AS department,pos.WorkCompany AS company,pos.PositionCode AS code ";
			$sql .= "FROM ".$config['db_organi'].".relation_position rel ";
			$sql .= "LEFT JOIN ".$config['db_organi'].".position pos ON rel.position_id=pos.id ";
			$sql .= "WHERE ";
			// $sql .= "rel.id_card='".$id_card."' AND pos.id='".$_SESSION['SESSION_Position_id']."' AND rel.status!='99' AND pos.status!='99' LIMIT 1";
			$sql .= "rel.id_card='".$id_card."' AND rel.status!='99' AND pos.status!='99' LIMIT 1";
			$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$return = mysql_fetch_assoc($que);
		}
		
		return $return;
	}
}
if (!function_exists('getEmp_follower')) {
	// ตามสิทธิ OU
	function getEmp_follower($idCard){
		// echo $_SESSION['listData']."<br/>";

		if($_SESSION['listData']=='OU'){
			// echo $_SESSION['listData']." :: 1<br/>";
		
			$arrEmp = get_underling_position();
			$arrEmp[] = "'".$idCard."'";
			$empID = implode(',',$arrEmp);
		}else{
			// echo $_SESSION['listData']." :: 2<br/>";
		
			$arr = searchEmp_multiPosition($idCard);
			$arrEmp = loopEmp_follower($arr);
			$arrEmp[] = "'".$idCard."'";
			$empID = implode(',',$arrEmp);
		}
		
		// echo "END<br/><br/>";
		return $empID;
	}
}
if (!function_exists('searchEmp_multiPosition')) {
	// หาตำแหน่งงาน ทั้งหมดของคนนี้
	function searchEmp_multiPosition($idCard){
		global $config, $logDb;
		$arr	= array();
		
		$sqlPo = "SELECT position_id FROM $config[db_organi].relation_position WHERE id_card='".$idCard."' AND status!='99' GROUP BY position_id";
		// echo "<font color='#0000FF'>FILE :: ".__FILE__." >> LINE :: ".__LINE__."</font><br/>".$sqlPo."<br/><br/>";
		$quePo = $logDb->queryAndLogSQL( $sqlPo, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		// วนลูปเพราะอาจจมีหลายตำแหน่ง
		while($fePo = mysql_fetch_assoc($quePo)){
			$arr[] = "'".$fePo['position_id']."'";
		}
		
		if(!empty($arr)){
			return $arr = array_unique($arr);
		}else{
			return $arr;
		}
	}
}
if (!function_exists('loopEmp_follower')) {
	// หาลูกน้องทั้งหมด
	function loopEmp_follower($arr){
		global $config, $logDb;
		$chk = false; $chkPo = $chkCard = array();
		
		$no = 0;
		while($chk==false){
			
			//print_r($arr); echo "<br/><br/>";

			$where = $wherePo = '';
			if(count($arr)>0){
				foreach($arr AS $key=>$value){
					//echo $value."<br/>";
					//print_r($chkPo); echo "<br/><br/>";
					
					if(!in_array($value, $chkPo)){
						if($wherePo!=''){ $wherePo .= ","; }
						$wherePo .= $value;
						$chkPo[] = $value;
						
						$chkPo = array_unique($chkPo);
					}
				}
			}
			
			if($wherePo!=''){
				$where = "command.Leader IN(".$wherePo.")";
				
				$sql = "SELECT rel.id_card FROM $config[db_organi].command INNER JOIN $config[db_organi].relation_position rel ON command.Follower=rel.position_id ";
				$sql .= "WHERE $where AND command.Status!='99' AND rel.status!='99' GROUP BY rel.id_card ORDER BY rel.position_id,rel.NameUse ASC";
				// echo "<font color='#0000FF'>FILE :: ".__FILE__." >> LINE :: ".__LINE__."</font><br/>".$sql."<br/><br/>";
				$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$row = mysql_num_rows($que);
				
				if(abs($row)>0){
					unset($arr);
					
					while($fe = mysql_fetch_assoc($que)){
						$arrID[] = "'".$fe['id_card']."'";
						
						if(!in_array("'".$fe['id_card']."'", $chkCard)){
							$arrPo = searchEmp_multiPosition($fe['id_card']);
							//print_r($arrPo); echo "<br/><br/>";
							if(count($arr)>0){
								$arr = array_merge($arr,$arrPo);
							}else{
								$arr = $arrPo;
							}
						
							//print_r($arr); echo "<br/><br/>";
							$arr = array_unique($arr);
						}
						
						$chkCard[] = "'".$fe['id_card']."'";
					}
					
				}else{
					$chk = true;
				}
				
				//if($no==1){ $chk = true; }
				$no++;
			}else{
				$chk = true;
			}
		}
		
		if($arrID){
			return $arrID = array_values(array_unique($arrID));
		}else{
			$arrID = '';
			return $arrID;
		}
	}
}
// ESC097-002 : new popup : create by Pichid : 2015 Sep 21

//ใช้ร่วมกับ todolist , InBox.php ด้วย
if (!function_exists('getNumFollowCusEvent')) {
	function getNumFollowCusEvent($date=null,$array=null){//จำนวนลูกค้าที่ต้องโทรหา  $date='yyyy-mm-dd'
		global $config, $logDb;
		randomCus();//สุ่มรายชื่อลูกค้าของ id_card ตัวเอง
		if($array['id_card']){ 
			$id_card = $array['id_card'];
			$empOu = select_empOu($id_card); //function/general.php
		}else {	
			$id_card = $_SESSION['SESSION_ID_card'];
			$empOu = select_empOu($id_card,'session'); //function/general.php
		}
		
		$whereOu = condition_sortOU($empOu,''); //function/general.php

		if($date){//หาวันที่ติดตาม เท่ากับ $date หรือ $condition = "AND '$follow_date' = '$date' ";
			$join  = "INNER JOIN $config[db_base_name].follow_main_event ";
			$join .= "ON $config[db_base_name].follow_customer.event_id_ref = $config[db_base_name].follow_main_event.id ";
			//หาวันที่ติดตามจริง $follow_date จะใช้ (stop_date - เงื่อนไขใน main event) = $date
			$condition  = "AND IF(stop_unit='day',DATE_SUB(stop_date,INTERVAL stop_value DAY), ";
			$condition .= "IF(stop_unit='month',DATE_SUB(stop_date,INTERVAL stop_value MONTH), ";
			$condition .= "IF(stop_unit='week',DATE_SUB(stop_date,INTERVAL stop_value WEEK), ";
			$condition .= "IF(stop_unit='year',DATE_SUB(stop_date,INTERVAL stop_value YEAR),stop_date)))) = '$date' ";
		}else{//ที่ต้องแจ้งเตือนในวันนี้
			$today = date('Y-m-d');
			$condition = "AND (start_date <= '$today' AND stop_date >= '$today') ";
		}
		
		# P'Add 2012-11-28 ให้แสดงรายการตามสายงาน
		$emp_id_card = getEmp_follower($_SESSION['SESSION_ID_card']); //function/general.php
		
		if($emp_id_card){ 
			$condition = "AND (process_by IN($emp_id_card) OR emp_id_card IN($emp_id_card))"; 
			$condition  = "AND (process_by NOT IN('updateExpire99','updateExpire98','updateExpire95', ";
			$condition .= "'updateExpire1','updateExpire') OR emp_id_card IN($emp_id_card))"; 

			#$condition ="AND (process_by = '' OR process_by = '-') AND follow_main_event.follow_up_activities = '2' AND emp_id_card = '' "; 

			$join = "INNER JOIN $config[db_base_name].follow_main_event ";
			$join .= "ON $config[db_base_name].follow_customer.event_id_ref = $config[db_base_name].follow_main_event.id ";
			
		}
		# End P'Add 2012-11-28 ให้แสดงรายการตามสายงาน
		
		$sql = "SELECT COUNT(DISTINCT(cus_no)) FROM $config[db_base_name].follow_customer $join ";
		$sql .= "WHERE $whereOu AND follow_customer.status ='1' $condition ";

		// echo '<br>',$sql;
		$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$rs=mysql_fetch_array($re);
		mysql_free_result($re);
		$_SESSION['numFollow'] = $rs[0];
		return $rs[0];
	}
}

if (!function_exists('getNumFollowCusEventByFlow')) {
	function getNumFollowCusEventByFlow($date=null,$array=null){
		global $config, $logDb;
		randomCus();//สุ่มรายชื่อลูกค้าของ id_card ตัวเอง
		if($array['id_card']){ 
			$id_card = $array['id_card'];
			$empOu = select_empOu($id_card); //function/general.php
		}else {	
			$id_card = $_SESSION['SESSION_ID_card'];
			$empOu = select_empOu($id_card,'session'); //function/general.php
		}
		
		$whereOu = condition_sortOU($empOu,''); //function/general.php

		if($date){//หาวันที่ติดตาม เท่ากับ $date หรือ $condition = "AND '$follow_date' = '$date' ";
			$join  = "INNER JOIN $config[db_base_name].follow_main_event ";
			$join .= "ON $config[db_base_name].follow_customer.event_id_ref = $config[db_base_name].follow_main_event.id ";
			//หาวันที่ติดตามจริง $follow_date จะใช้ (stop_date - เงื่อนไขใน main event) = $date
			$condition  = "AND IF(stop_unit='day',DATE_SUB(stop_date,INTERVAL stop_value DAY), ";
			$condition .= "IF(stop_unit='month',DATE_SUB(stop_date,INTERVAL stop_value MONTH), ";
			$condition .= "IF(stop_unit='week',DATE_SUB(stop_date,INTERVAL stop_value WEEK), ";
			$condition .= "IF(stop_unit='year',DATE_SUB(stop_date,INTERVAL stop_value YEAR),stop_date)))) = '$date' ";
		}else{//ที่ต้องแจ้งเตือนในวันนี้
			$today = date('Y-m-d');
			$condition = "AND (start_date <= '$today' AND stop_date >= '$today') ";
		}
		
		# P'Add 2012-11-28 ให้แสดงรายการตามสายงาน
		$emp_id_card = getEmp_follower($_SESSION['SESSION_ID_card']); //function/general.php
		
		if($emp_id_card){ 
			#$condition = "AND (process_by IN($emp_id_card) OR emp_id_card IN($emp_id_card))"; 
			#$condition  = "AND (process_by NOT IN('updateExpire99','updateExpire98','updateExpire95', ";
			#$condition .= "'updateExpire1','updateExpire') OR emp_id_card IN($emp_id_card))"; 

			$condition .= "AND (process_by = '' OR process_by = '-') AND follow_main_event.follow_up_activities = '2' AND emp_id_card = '' "; 

			$join = "INNER JOIN $config[db_base_name].follow_main_event ";
			$join .= "ON $config[db_base_name].follow_customer.event_id_ref = $config[db_base_name].follow_main_event.id ";
			
		}
		# End P'Add 2012-11-28 ให้แสดงรายการตามสายงาน
		
		$sql = "SELECT COUNT(DISTINCT(cus_no)) FROM $config[db_base_name].follow_customer $join ";
		$sql .= "WHERE $whereOu AND follow_customer.status ='1' $condition ";
		// echo '<br><br/>ลูกค้าที่กำลังติดตาม (โครงสร้าง)',$sql;

		$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$rs=mysql_fetch_array($re);
		mysql_free_result($re);
		$_SESSION['numFollow'] = $rs[0];
		return $rs[0];	
	}
}

if (!function_exists('getNumFollowCusEvent2')) {
	function getNumFollowCusEvent2($date=null,$array=null, $emp_id_card2){//ลูกค้าที่ดูแลเป็นรายคน
		global $config, $logDb;
		randomCus();//สุ่มรายชื่อลูกค้าของ id_card ตัวเอง
		if($array['id_card']){ 
			$id_card = $array['id_card'];
			$empOu = select_empOu($id_card); //function/general.php
		}else {	
			$id_card = $_SESSION['SESSION_ID_card'];
			$empOu = select_empOu($id_card,'session'); //function/general.php
		}
		
		$whereOu = condition_sortOU($empOu,''); //function/general.php

		if($date){//หาวันที่ติดตาม เท่ากับ $date หรือ $condition = "AND '$follow_date' = '$date' ";
			$join  = "LEFT JOIN $config[db_base_name].follow_main_event ";
			$join .= "ON $config[db_base_name].follow_customer.event_id_ref = $config[db_base_name].follow_main_event.id ";
			//หาวันที่ติดตามจริง $follow_date จะใช้ (stop_date - เงื่อนไขใน main event) = $date
			$condition  = "AND IF(stop_unit='day',DATE_SUB(stop_date,INTERVAL stop_value DAY), ";
			$condition .= "IF(stop_unit='month',DATE_SUB(stop_date,INTERVAL stop_value MONTH), ";
			$condition .= "IF(stop_unit='week',DATE_SUB(stop_date,INTERVAL stop_value WEEK), ";
			$condition .= "IF(stop_unit='year',DATE_SUB(stop_date,INTERVAL stop_value YEAR),stop_date)))) = '$date' ";
		}else{//ที่ต้องแจ้งเตือนในวันนี้
			$today = date('Y-m-d');
			$condition = "AND (start_date <= '$today' AND stop_date >= '$today') ";
		}
		
		# P'Add 2012-11-28 ให้แสดงรายการตามสายงาน
		$emp_id_card = getEmp_follower($_SESSION['SESSION_ID_card']); //function/general.php
		
		if($emp_id_card){ 
			//$condition = "AND (process_by IN($emp_id_card) OR emp_id_card IN($emp_id_card))"; 
			/*$condition  = "AND (process_by NOT IN('updateExpire99','updateExpire98','updateExpire95', ";
			$condition .= "'updateExpire1','updateExpire') OR emp_id_card IN($emp_id_card))"; */

			$condition ="AND (process_by = ".$emp_id_card2."  OR emp_id_card =".$emp_id_card2.") "; 
			
		}
		# End P'Add 2012-11-28 ให้แสดงรายการตามสายงาน
		
		$sql = "SELECT COUNT(DISTINCT(cus_no)) FROM $config[db_base_name].follow_customer $join ";
		$sql .= "WHERE $whereOu AND follow_customer.status ='1' $condition ";
		#echo '<br> <br>ลูกค้าที่กำลังติดตาม (คน)',$sql;
		
		$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$rs=mysql_fetch_array($re);
		mysql_free_result($re);
		$_SESSION['numFollow'] = $rs[0];
		return $rs[0];
	}
}

//ใช้ร่วมกับ todolist ด้วย
if (!function_exists('getNumInterviewEvent')) {
	function getNumInterviewEvent($date=null){//จำนวนลูกค้าที่รอสัมภาษณ์
		global $config, $logDb;
		random_customer();//สุ่มรายชื่อลูกค้าตาม department

		if($date){$condition = "AND start_date = '$date' ";}

		$sql = "SELECT COUNT(DISTINCT(follow_customer.cus_no)) FROM $config[db_base_name].follow_customer ";
		$sql.= "LEFT JOIN $config[db_base_name].follow_main_event ON follow_customer.event_id_ref = follow_main_event.id ";
		$sql.= "WHERE follow_customer.status = '4'
		AND (
			follow_main_event.department='".$config['phF_checkDepartment']."'
			OR follow_main_event.who_response = '$_SESSION[SESSION_ID_card]'
			OR (follow_customer.emp_id_card LIKE '%".$_SESSION['SESSION_ID_card']."%' AND follow_main_event.department = 'Leader')
		)
		AND follow_main_event.pattern_id_ref = '' AND follow_main_event.status !='99'
		AND follow_main_event.visible !='no' $condition ";

		$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$rs=mysql_fetch_array($re);
		mysql_free_result($re);
		$_SESSION['numInterview'] = $rs[0];
		$_SESSION['checkInterview'] = "wait";
		return $rs[0];
	}
}

if (!function_exists('phf_getNumResperson')) {
	function phf_getNumResperson($array,$db=null){
		global $logDb;

		if($db==''){
			global $config;
		}else{
			$config['db_maincus'] = $db;
		}
		$id_card=$array['id_card'];
		//$sql = "SELECT COUNT(CusNo) FROM $config[db_maincus].MainCusData WHERE Resperson = '$id_card' AND Resperson!='' ";
		$sql = "SELECT COUNT(RESP_CUSNO) FROM $config[db_maincus].MAIN_RESPONSIBILITY WHERE RESP_IDCARD='".$id_card."'";
		$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$rs=mysql_fetch_array($re);
		mysql_free_result($re);
		return $rs[0];
	}
}

/**
 * จำนวนที่ต้องอนุมัติยุติการติดตาม หลังการขาย follow_customer
 * @param String $date วันที่เสนออนุมัติยกเลิก
 */
if (!function_exists('get_approve_num')) {
	function get_approve_num($date=null) {
		global $config, $logDb;
		$CheckManager = substr($_SESSION['SESSION_member_id'],$config['startManager'],$config['cutManager']);  		// ex
		$CheckLevel = substr($_SESSION['SESSION_member_id'],$config['startla_level'],$config['cutla_level']);
		//if($CheckManager==$config['Manager'] || $CheckLevel <= $config['la_level'] && ($CheckManager != $config['sale_admin']) && $CheckLevel!=""){
			$approve_num = 0;$numEmp=0;
			$team_id_card = get_emp_saleTemp($_SESSION['SESSION_ID_card'],'id_card');
			if($team_id_card){
				if($CheckManager==$config['Manager']){
					$where="";
				}else {
					$where = " AND emp_id_card IN($team_id_card) ";
				}
				if($date){
					$where.= " AND DATE(process_date) = '$date'";
				}
				//$sql = "SELECT count(id) FROM $config[db_base_name].follow_customer WHERE status = '90' AND DATE(process_date) = '$date' $where ";
				//$re = mysql_query($sql);
				//$approve_num = mysql_result($re,0,0);
				//$approve_num = mysql_fetch_array($re);

				$sql = "SELECT id FROM $config[db_base_name].follow_customer
						WHERE status = '90' $where
						GROUP by cus_no,cancel_type";
				$result_app = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$approve_num = mysql_num_rows($result_app);
			}
			return $approve_num;
		//}
	}
}
/**
 * นับจำนวนรายการที่ต้องออกไปติดตาม Contact_Prepare
 * @param String $date วันที่ๆ จะออกไปติดตาม
 */
if (!function_exists('get_tracking_num')) {
	function get_tracking_num($date=null) {
		global $config, $logDb;
		$tracking_num = array();
		# เลือกเฉพาะที่อนุมัติแล้ว status=2 และเป็นของตัวเอง จัดกลุ่มตามรหัส Int_Num
		$sql = "SELECT count(Con_prepare_Num) FROM $config[db_base_name].CusInt ";
		$sql.= "LEFT JOIN $config[db_base_name].Contact_Prepare ON CusInt.Int_Num = Contact_Prepare.Int_Num ";
		$sql.= "WHERE CusInt.Status IN('1','2','3') AND Plantogodate = '$date' AND Contact_Prepare.Status = '2'
				AND CusInt.Sale_ResponsibilityNo  = '".$_SESSION['SESSION_ID_card']."' ";
		$sql.= " GROUP by Contact_Prepare.Int_Num ";
		$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		//$tracking_num = mysql_result($re,0,0);
		$tracking_num = mysql_fetch_array($re);
		print_r($tracking_num);
		return $tracking_num[0];
	}
}

//เหตุการณ์สำคัญ
if (!function_exists('get_TelCusBirthday')) {
	function get_TelCusBirthday(){
		global $config, $logDb;
		$CheckLevel=substr($_SESSION['SESSION_member_id'],$config['startla_level'],$config['cutla_level']);
		$CheckDepartment=substr($_SESSION['SESSION_member_id'],$config['startDepartment'],$config['cutDepartment']);
		if($CheckDepartment!=$config['Department'] and $CheckLevel > $config['la_level']){	//ถ้าไม่ใช่ sale admin
			$recordby=" and  substring(Recorder,1,13)='$_SESSION[SESSION_ID_card]' ";
		}
		$nowMY	=	date('n/').(date('Y')+543);
		$sql_event="SELECT Cus_No,Record_date,substring(Recorder,1,13)
					FROM  $config[db_base_name].TelCusBirthday WHERE status!='99' and alert_date='$nowMY' $recordby ";
		$rs_event= $logDb->queryAndLogSQL( $sql_event, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$num_event = mysql_num_rows($rs_event);
		return $num_event;
	}
}

if (!function_exists('getBeforeDate')) {
	function getBeforeDate($date,$value,$unit) {//หาค่าวันที่นับจากวันที่ระบุ
		global $config;
		$dayArr = explode("-", $date);
		$day = $dayArr[2];
		$month = $dayArr[1];
		$year = $dayArr[0];
		switch($unit){
			case "day":
				$day = $day-$value;
				break;
			case "week":
				$day = $day-($value*7);
				break;
			case "month":
				$month = $month-$value;
				break;
			case "year":
				$year = $year-$value;
				break;
		}
		$beforeTime = mktime(0,0,0,$month, $day , $year);
		$beforeDate = date('Y-m-d',$beforeTime);
		return $beforeDate;
	}
}

if (!function_exists('getAfterDate')) {
	function getAfterDate($date,$value,$unit) {//หาค่าวันที่ ตามค่าที่กำหนด
		global $config;
		$dayArr = explode("-", $date);
		$day = $dayArr[2];
		$month = $dayArr[1];
		$year = $dayArr[0];
		switch($unit){
			case "day":
				$day = $day+$value;
				break;
			case "week":
				$day = $day+($value*7);
				break;
			case "month":
				$month = $month+$value;
				break;
			case "year":
				$year = $year+$value;
				break;
		}
		$afterTime = mktime(0,0,0,$month, $day , $year);
		$afterDate = date('Y-m-d',$afterTime);
		return $afterDate;
	}
}

if(!function_exists('standard2thaidate')){
	function standard2thaidate($date,$sign,$returnSign='/'){ //เปลี่ยนวันที่ ค.ศ. เป็น พ.ศ. , yyyy-mm-dd => dd/mm/yyyy คืนค่า เป็น array 'number', 'text'
		$thmonth = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$exp = explode($sign,$date);
		$thyear = $exp[0]+543;
		$thaidate['number'] = $exp[2].$returnSign.$exp[1].$returnSign.$thyear;
		$m = sprintf("%01d", $exp[1]);
		$thaidate['text'] = $exp[2]." ".$thmonth[$m]." ".$thyear;
		return $thaidate; //ค่าที่ส่งคืนจะประกอบด้วย อาร์เรย์ที่เป็นตัวเลข และตัวหนังสือ
	}
}

if (!function_exists('numDiffDate')) {
	function numDiffDate($date1,$date2){
		$d1  = explode("-",$date1);
		$d2  = explode("-",$date2);
		$time1 = mktime(0, 0, 0, $d1[1], $d1[2], $d1[0]);
		$time2 = mktime(0, 0, 0, $d2[1], $d2[2], $d2[0]);
		//หาจำนวนวันแบบนับได้
		$result = $time2 - $time1;
		$diff_date = intval($result/86400);
		return $diff_date;
	}
}

if (!function_exists('genSelectYear')) {
	function genSelectYear($year=null){
		$thisyear = date('Y');
		$num = $year - 10;
		for($i=$thisyear;$i>$num;$i--){
			$text = $i+543;
			$result .= "<option value='$i' ";
			if($i==$year){$result .= "selected='selected'";}
			$result .= ">$text</option>";
		}
		return $result;
	}
}

if (!function_exists('genSelectMonth')) {
	function genSelectMonth($m=null){
		$thmonth = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		for( $i=1 ; $i<= 12 ; $i++ ){
			$month .= "<option value='$i' ";
			if($i==$m){
				$month .= "selected='selected'";
			}
			$month .= ">$thmonth[$i]</option>";
		}
		return $month;
	}
}

if (!function_exists('getMainCusAddress')) {
	function getMainCusAddress($cusdataArr,$check=null,$export=null) {//รับค่า Array ข้อมูลลูกค้า ที่ดึงจากฐานข้อมูลแล้ว
		if($export == 'export'){
			//modify by pt
		//ใช้ที่อยู่ สำหรับส่งเอกสาร
		$addr = '<td>'.$cusdataArr[Cus_Add].'</td>';

		//if($cusdataArr['Cus_Vill']){$addr .= '<td>'.$cusdataArr[Cus_Vill].'</td>';}
		$tum = splite_reg($cusdataArr['Cus_Tum'],'_','1');
		$amp = splite_reg($cusdataArr['Cus_Amp'],'_','1');
		$pro = splite_reg($cusdataArr['Cus_Pro'],'_','1');
		$addr .= '<td>'.$tum.'</td>';
		$addr .= '<td>'.$amp.'</td>';
		$addr .= '<td>'.$pro.'</td>';
		$addr .= '<td>'.$cusdataArr[Cus_Code].'</td>';

		if(strlen($addr) < 60 && !$cusdataArr[Cus_Add]){//ถ้าไม่มี ใช้ที่อยู่ตามทะเบียนบ้าน
			$addr = '<td>'.$cusdataArr[C_Add].'</td>';
			//if($cusdataArr['C_Vill']){$addr .= " บ้าน".$cusdataArr['C_Vill'];}
			$tum = splite_reg($cusdataArr['C_Tum'],'_','1');
			$amp = splite_reg($cusdataArr['C_Amp'],'_','1');
			$pro = splite_reg($cusdataArr['C_Pro'],'_','1');
			$addr .= '<td>'.$tum.'</td>';
			$addr .= '<td>'.$amp.'</td>';
			$addr .= '<td>'.$pro.'</td>';
			$addr .= '<td>'.$cusdataArr[C_Code].'</td>';
		}

			$addr .= '<td> โทรศัพท์';
			if($cusdataArr['H_Tel']){$addr .= 'บ้าน '.$cusdataArr['H_Tel'];}
			if($cusdataArr['W_Tel']){$addr .= ', ที่ทำงาน '.$cusdataArr['W_Tel'];}
			if($cusdataArr['M_Tel']){$addr .= ', มือถือ '.$cusdataArr['M_Tel'];}
			$addr .= '</td>';

		}else{
		//ใช้ที่อยู่ สำหรับส่งเอกสาร
		if($cusdataArr['Cus_Add']){$addr = $cusdataArr['Cus_Add'];}
		if($cusdataArr['Cus_Vill']){$addr .= " บ้าน".$cusdataArr['Cus_Vill'];}
		$tum = splite_reg($cusdataArr['Cus_Tum'],'_','1');
		$amp = splite_reg($cusdataArr['Cus_Amp'],'_','1');
		$pro = splite_reg($cusdataArr['Cus_Pro'],'_','1');
		if($tum){$addr .= " ต.".$tum;}
		if($amp){$addr .= " อ.".$amp;}
		if($pro){$addr .= " จ.".$pro;}
		if($cusdataArr['Cus_Code']){$addr .= " ".$cusdataArr['Cus_Code'];}

		if($addr==""){//ถ้าไม่มี ใช้ที่อยู่ตามทะเบียนบ้าน
			if($cusdataArr['C_Add']){$addr = $cusdataArr['C_Add'];}
			if($cusdataArr['C_Vill']){$addr .= " บ้าน".$cusdataArr['C_Vill'];}
			$tum = splite_reg($cusdataArr['C_Tum'],'_','1');
			$amp = splite_reg($cusdataArr['C_Amp'],'_','1');
			$pro = splite_reg($cusdataArr['C_Pro'],'_','1');
			if($tum){$addr .= " ต.".$tum;}
			if($amp){$addr .= " อ.".$amp;}
			if($pro){$addr .= " จ.".$pro;}
			if($cusdataArr['C_Code']){$addr .= " ".$cusdataArr['C_Code'];}
		}
		if($check['no']!="tel"){
			if($cusdataArr['H_Tel'] || $cusdataArr['W_Tel'] || $cusdataArr['M_Tel']){
				if($check == 'NO_BR'){
					$addr .= "         โทรศัพท์  ";
				}else{
					$addr .= " <br><b>โทรศัพท์  </b>";
				}
			}
			if($cusdataArr['H_Tel']){$addr .= " บ้าน ".$cusdataArr['H_Tel'];}
			if($cusdataArr['W_Tel']){$addr .= ", ที่ทำงาน ".$cusdataArr['W_Tel'];}
			if($cusdataArr['M_Tel']){$addr .= ", มือถือ ".$cusdataArr['M_Tel'];}
		}

		} // end else // modify by pt

		return $addr;

	}
}

if (!function_exists('genAnswerOption')) {
	function genAnswerOption($list_id,$input_type,$answer_option) {
		$ans_opt	= stripcslashes($answer_option);
		#-- ถ้ามี ' มาด้วย (คือ option มากกว่า 1 ตัว)
		if(strpos($ans_opt,"'")!== FALSE){
			//$ans_opt	= substr($ans_opt,1);
			//$ans_opt	= substr($ans_opt,0,-1);
			$exp = explode("','", $ans_opt); //แยกคำออกจากกัน     [1]abc [2] def [3]ghi [4]"jk" 55
			$count = count($exp);
		}
		switch($input_type) {
			case "textbox":
				$input = "<input type='text' name='text".$list_id."' style='width:98%;margin-left:20px' value=''>";
			break;
			case "textarea":
				$input = "<textarea name='textArea".$list_id."'  class='nz_textarea' style='width:98%;margin-left:20px' rows='$ans_opt'></textarea>";
			break;
			case "checkbox":
				$input = "<p style='margin-left:16px'>";
				for($i=0;$i<$count;$i++) {
					//$val = $exp[$i];
					$val = str_replace("'","",$exp[$i]);
					$val2 = str_replace("<br>", "", $val);
					$input .= "<input type='checkbox' id='chk_".$list_id."_".$i."' name='check".$list_id."[]' value=\"$val2\"><label for='chk_".$list_id."_".$i."'>$val</label> ";
				}
				$input .= "</p>";
			break;
			case "radio":
				$input = "<p style='margin-left:16px'>";
				for($i=0;$i<$count;$i++) {
					//$val = $exp[$i];
					$val = str_replace("'","",$exp[$i]);
					$val2 = str_replace("<br>", "", $val);
					$input .= "<input type='radio' id='rdo_".$list_id."_".$i."' name='radio".$list_id."' value=\"$val2\"><label for='rdo_".$list_id."_".$i."'>$val</label> ";
				}
				$input .= "</p>";
			break;
			case "selectbox":
				$input = "<select name='select".$list_id."' style='margin-left:20px'>";
				$input .= "<option value=''> เลือกคำตอบ </option>";
				for($i=0;$i<$count;$i++) {
					//$val = $exp[$i];
					$val = str_replace("'","",$exp[$i]);
					$input .= "<option value='".$val."'> $val </option>";
				}
				$input .= "</select>";
			break;
			case "pleasure_value":
				//มีได้หลายๆอัน
				$ans_opt = str_replace("'", "", $ans_opt);
				$input = "<input type='text' name='pleasure".$list_id."' class='pleasure' id='pleasure".$list_id."' rel='$ans_opt' style='width:30px;margin-left:20px;' onkeyup=\"check_number('pleasure".$list_id."')\" onblur=\"checkMaxValue('pleasure".$list_id."',$ans_opt)\"> <span style='color:blue'>* กรอกเป็นตัวเลข ค่าสูงสุดคือ $ans_opt</span>";
				//$input = "<input type='text' name='pleasure".$list_id."' id='pleasure".$list_id."' style='width:30px;margin-left:20px;' name='pleasure' onkeyup=\"check_number('pleasure".$list_id."')\" onblur=\"checkMaxValue('pleasure".$list_id."',$ans_opt)\"> <span style='color:blue'>* กรอกเป็นตัวเลข ค่าสูงสุดคือ $ans_opt</span>";
			break;
		}
		return $input;
	}
}

if(!function_exists("get_emp_saleTemp")){
	function get_emp_saleTemp($session_id_card,$select=null) {//หาลูกน้องใน SaleTemp
		global $config, $logDb;
		if(empty($select)){
			$select = "id";//ค่าที่จะ return
		}
		$this_month = (date('Y')+543).date('m');
		/* ตรวจสอบระดับ ก่อนว่าอยู่ระดับไหน*/
		$CheckManager=substr($_SESSION['SESSION_member_id'],$config['startManager'],$config['cutManager']);  		// EX
		if($CheckManager==$config['Manager']){//ถ้าเป็น Manager && Admin จะเห็นทั้งหมด
			$sql = "SELECT $select FROM $config[db_base_name].SaleTemp WHERE status !='99' AND start_month='$this_month' ";//รายชื่อทีมทั้งหมด
			$team = sql_in($sql);//เก็บค่า return / function/ic_followcus.php
		}else {
			$sql = "SELECT id,SaleLevel FROM $config[db_base_name].SaleTemp ";
			$sql .= "WHERE id_card LIKE '%$session_id_card%' AND status !='99' AND start_month='$this_month' "; // 1  //pt@2012-01-04
			$sql .= "ORDER by id desc LIMIT 1";
			$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$status = mysql_fetch_assoc($result);
			/* ทำการค้นหาบัตรประชาชนของลูกน้อง */
			switch($status['SaleLevel']){
				case 'Sale':
					$team = "'$status[id]'";  //pt@2012-01-07 
					break;
				case 'Supervisor':
					//pt@2012-01-07 add  OR id='$status[id]')
					$sql = "SELECT $select FROM $config[db_base_name].SaleTemp WHERE (SaleSupervisor_id = '".$status['id']."' OR id='$status[id]') AND status !='99'  AND start_month='$this_month' ";//หา Sale
					$team = sql_in($sql);//เก็บค่า return ของลูกน้องระดับ sale
				break;
				case 'BigSupervisor':
					//pt@2012-01-07 add  OR id = '$status[id]')
					$big_sql = "SELECT $select FROM $config[db_base_name].SaleTemp WHERE (BigSupervisor_id='".$status['id']."' OR id = '$status[id]') AND status !='99'  AND start_month='$this_month' ";//หา Sale supervisor
					$sale_super = sql_in($big_sql);//เก็บค่า return ของลูกน้องระดับ sale super

					$bigSQL = "SELECT id FROM $config[db_base_name].SaleTemp WHERE BigSupervisor_id='".$status['id']."' AND status !='99'  AND start_month='$this_month' ";//หา id ของ sale super ที่เป็นลูกน้อง
					$SaleSupervisor_id_in = sql_in($bigSQL);//id ของ sale super

					$saleSQL = "SELECT $select FROM $config[db_base_name].SaleTemp WHERE SaleSupervisor_id IN($SaleSupervisor_id_in) AND status !='99' AND start_month='$this_month' ";// หา Sale
					$sale = sql_in($saleSQL); //เก็บค่า return ของลูกน้องระดับ sale

					if($sale){ $sale = ",".$sale;}
					$team = $sale_super.$sale;
				break;
			}
		}//end check
		return $team;
	}
}

if (!function_exists('getNumEvent')) {
	function getNumEvent($cus_no){//นับเหตุการณ์ของลูกค้ารายนั้น
		global $config, $logDb;
		$today = date('Y-m-d');
		$checkSQL = "SELECT count(id) FROM $config[db_base_name].follow_customer ";
		$checkSQL .= "WHERE cus_no = '$cus_no' AND status = '1' AND (start_date <= '$today' AND stop_date >= '$today') ";
		$checkResult = $logDb->queryAndLogSQL( $checkSQL, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$numCheck = mysql_fetch_array($checkResult);
		return $numCheck[0];
	}
}

if (!function_exists('checkNull')) {
	function checkNull($string) {//เช็กค่าว่าง
		if($string!=""){$data = $string;}
		else{$data = "-" ;	}
		return $data;
	}
}

if(!function_exists('get_num_miss_call')){
	function get_num_miss_call($emp_id_card){//รายการที่ไม่ได้โทร
		global $logDb;
	
		$sql = "SELECT COUNT(cus_no) FROM $config[db_base_name].follow_customer ";
		$sql .= "WHERE emp_id_card = '$emp_id_card' AND status = '98' ";
		$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$rs = mysql_fetch_array($result);
		return	$rs[0];
	}
}

if (!function_exists('get_leader_idCard')) {
	function get_leader_idCard($idCard){
		$sale = select_sell_temp($idCard);
		if($sale['SaleLevel']=="Sale"){
			$ida = select_sell_temp($sale['SaleSupervisor_id'],'id');
			$idb = select_sell_temp($ida['BigSupervisor_id'],'id');
			$leaderIDcard = $ida['id_card'].",".$idb['id_card'];
		}else if($rs['SaleLevel']=="Supervisor") {
			$ida = select_sell_temp($sale['BigSupervisor_id'],'id');
			$leaderIDcard = $ida['id_card'];
		}else if($rs['SaleLevel']=="BigSupervisor"){
			$leaderIDcard = $idCard;
		}
		return $leaderIDcard;
	}
}

/**
 * สร้าง option ให้กับ selectbox โดยลิสต์รายการตาม SQL ที่ส่งมา
 * @param String $sql คำสั่งในการดึงข้อมูลมาสร้างลิสต์
 * @param Array $arr ส่งมาเป็น อาร์เรย์ เพิ่มกำหนดค่าต่างๆเพิ่มเติม
 * 					[value_field] ฟิลด์ที่จะใช้เป็น value ของ option
 * 					[title_field] ฟิลด์ที่จะใช้เป็น text ของ option
 * 					[select_value] ค่าจากตัวเลือกเพิม เพื่มทำการเลือกเป็น default
 * @return เป็น <option></option>
 */
if (!function_exists('get_event_dropdown')) {
	function get_event_dropdown($sql, $arr=null){
		global $config, $logDb;
		$value_field = $arr['value_field'];
		$text_field = $arr['text_field'];
		$select_value = $arr['select_value'];
		$default_text = $arr['default_text'];
		if($default_text==''){$default_text = '- เลือกกิจกรรมที่ต้องการ - (แสดงทั้งหมด) -';}
		//ดึงมาเฉพาะเหตุการณ์รอสัมภาษณ์ของ CR pattern_id_ref ต้องเป็นค่าว่าง
		$event_dropdown = '<option value="">- '.$default_text.'</option>';
		$event_result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while( $eventArr = mysql_fetch_assoc($event_result) )
		{
			$evn_selected='';
			if($eventArr[$value_field] == $select_value && $select_value!=''){//ถ้า value ตรงกันให้ select
				$evn_selected='selected="selected"';
			}
			//if($eventArr[$value_field]==''){$eventArr[$value_field] = '%';}
			$event_dropdown .= '<option '.$evn_selected.' value="'.$eventArr[$value_field].'">'.$eventArr[$text_field].'</option>';
		}
		return $event_dropdown;
	}
}

/**
 * สร้าง option ให้กับ selectbox โดยลิสต์รายการตาม SQL ที่ส่งมา
 * @param String $sql คำสั่งในการดึงข้อมูลมาสร้างลิสต์
 * @param Array $arr ส่งมาเป็น อาร์เรย์ เพิ่มกำหนดค่าต่างๆเพิ่มเติม
 * 					[value_field] ฟิลด์ที่จะใช้เป็น value ของ option
 * 					[title_field] ฟิลด์ที่จะใช้เป็น text ของ option
 * 					[select_value] ค่าจากตัวเลือกเพิม เพื่มทำการเลือกเป็น default
 * @return array[dropdown] เป็น <option></option>, array[id] เป็น array[id]
 */
if (!function_exists('get_event_dropdowns')) {
	function get_event_dropdowns($sql, $arr=null){
		global $config, $logDb;
		$value_field 	= $arr['value_field'];
		$text_field 	= $arr['text_field'];
		$select_value 	= $arr['select_value'];
		$default_text 	= $arr['default_text'];
		
		if($default_text==''){
			$default_text = '- เลือกกิจกรรมที่ต้องการ - (แสดงทั้งหมด) -';
		}
		
		//ดึงมาเฉพาะเหตุการณ์รอสัมภาษณ์ของ CR pattern_id_ref ต้องเป็นค่าว่าง
		$event_dropdown = '<option value="">- '.$default_text.'</option>';
		$event_result 	= $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		$arrValue = array();
		while( $eventArr = mysql_fetch_assoc($event_result) )
		{
			foreach($eventArr AS $key => $value){
				$arrValue[$key][] = $value;
			}
			
			$evn_selected='';
			if($eventArr[$value_field] == $select_value && $select_value!=''){//ถ้า value ตรงกันให้ select
				$evn_selected='selected="selected"';
			}
			//if($eventArr[$value_field]==''){$eventArr[$value_field] = '%';}
			$event_dropdown .= '<option '.$evn_selected.' value="'.$eventArr[$value_field].'">'.$eventArr[$text_field].'</option>';
		}

		$return['dropdown'] = $event_dropdown;
		$return = array_merge($return,$arrValue);
		
		return $return;
	}
}

/**
 * หาจำนวนลูกค้าทั้งหมด ตามเงื่อนไขที่ส่งมา
 * @param String $condition เงื่อนไขในการนับจำนวน
 * @return Integer จำนวนกิจกรรมตามเงื่อนไข
 */
if (!function_exists('get_num_event_by')) {
	function get_num_event_by($condition){
		global $config, $logDb;
		$numSQL = "SELECT count(id) FROM $config[db_base_name].follow_customer WHERE  $condition ";
		$checkResult = $logDb->queryAndLogSQL( $numSQL, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$numArr = mysql_fetch_array($checkResult);
		return $numArr[0];
	}
}

//แบ่งหน้าแสดงผล --- ไม่ได้ใช้ เก็บไว้ก่อน
if(!function_exists('createPageView')){
	function createPageView($sql,$page,$onclick){
		global $config, $logDb;
		$limit = $config['page_limit'];

		mysql_query( "SET NAMES UTF8" ) ;
		$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$total=mysql_num_rows($result); //จำนวนแถวทั้งหมด

		$totalpage=ceil($total/$limit); //จำนวนหน้าที่ได้
		$mod = $total % $limit;
		if($page>$totalpage){$page=$totalpage;}//ถ้าหน้าที่ส่งมา มากว่าจำนวนหน้าทั้งหมด แสดงว่ารายการหน้าสุดท้ายถูกลบออกไปแล้ว
		if ($page==""){$page=1;}

		$arr['goto']=($page-1)*$limit; //เริ่มต้นที่ 0 คือข้อมูลแถวแรก
		$rowbegin = $arr['goto']+1; //แสดงแถวเริ่มต้น
		$rowend = $page*$limit; //แสดงแถวสุดท้าย
		if($page==$totalpage){
			if($mod){ //ถ้าหารไม่ลงตัว
				$rowend = $arr['goto']+$mod;
			}else{
				$rowend = $page*$limit;
			}
		}
		if($totalpage>1){
			$limitpage = $config['page_number'];
			if($page <= $limitpage and $page < $totalpage){//ถ้าหน้าปัจจุบันเป็นตัวเลขชุดแรก
				$startpage = 1;
			}else{
				if($page % $limitpage){
					$startpage = (floor($page/$limitpage)*$limitpage)+1;
				}else{//ถ้าหารลงตัว หมายถึงหน้าปัจจุบัน เป็นเลขตัวสุดท้าย
					$startpage = ($page - $limitpage) +1; //หน้าเริ่มต้นของ เลขแถวนั้นจะเท่ากับ หน้าปัจจุบัน - จำนวนจำกัดต่อแถว
				}
			}
			$endpage = ($startpage+$limitpage)-1;

			if($endpage > $totalpage){ //ถ้าหน้าสุดท้ายมากกว่า จำนวนหน้าทั้งหมด
				$num = ($totalpage-$startpage) ;
				$endpage = $startpage + $num;
			}

			for ($i=$startpage;$i<=$endpage;$i++){
				if($i==$page){$class="select";}else{$class = "nonselect";}
				$nav_pager .= "<a href='javascript:void(0)' onclick=\"browseCheck('$onclick[div]','$onclick[url]&pageNumber=$i&month=$_GET[month]&year=$_GET[year]')\" class='$class'>$i</a>";

			}

			if($endpage < $totalpage){
				$num = $endpage + 1;
				$next = "<a href='javascript:void(0)' onclick=\"browseCheck('$onclick[div]','$onclick[url]&pageNumber=$num&month=$_GET[month]&year=$_GET[year]')\" class='nav_button'>หน้าถัดไป>></a>";
			}
			if($startpage > $limitpage){
				$num = $startpage - 1;
				$prev = "<a href='javascript:void(0)' onclick=\"browseCheck('$onclick[div]','$onclick[url]&pageNumber=$num&month=$_GET[month]&year=$_GET[year]')\" class='nav_button'><<หน้าที่แล้ว</a>";
			}

		}
		if($total > 0){
		$arr['pageview'] = "<div id='pager_nav' class='pager'>$prev $nav_pager  $next [หน้าที่ <span class='current_pager'>$page</span> / <font class='total_pager'>$totalpage</font> หน้า] รายการที่ <span class='begin_end_rows'>$rowbegin - $rowend</span> จากทั้งหมด <font class='total_num_rows'>$total</font> รายการ $other</div>";
		}
		return $arr;
	}
}

if (!function_exists('random_customer')) {
	function random_customer(){      //department's event

		/* --- c ไม่ใช้งาน function นี้แล้ว   ใช้งาน  function randomCus() แทน c -----
		* --------------------------------------------------------------- */
		return false;
		/* ---------------------- last edit 2012-05-22 ------------------
		* --------------------------------------------------------------- */

		global $config, $logDb;
		updateExpire();		//อัพเดต status ของรายการที่เลยกำหนดแล้ว
		
		$sql  = "SELECT id,random_display FROM $config[db_base_name].follow_main_event ";
		$sql .= "WHERE (department='".$config['phF_checkDepartment']."' OR who_response = '$_SESSION[SESSION_ID_card]') AND pattern_id_ref = '' ";
		$sql .= "AND '".date('Y-m-d')."' != random_date AND status !='99' AND visible !='no'";
		$re_fl_ev = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($rs_fl_ev=mysql_fetch_array($re_fl_ev)){
			$sql = "SELECT DISTINCT(emp_id_card) FROM $config[db_base_name].follow_customer WHERE event_id_ref='".$rs_fl_ev['id']."' AND status='3'";
			$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			while($rs=mysql_fetch_array($re)){
				/* หาจำนวนทั้งหมด */
				$sqlNum	= "SELECT COUNT(id) FROM $config[db_base_name].follow_customer WHERE event_id_ref='".$rs_fl_ev['id']."' AND emp_id_card='".$rs['emp_id_card']."' AND status='3' GROUP BY emp_id_card";
				$reNum = $logDb->queryAndLogSQL( $sqlNum, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$rsNum=mysql_fetch_array($reNum);
				$num = $rsNum[0];

				/* หาจำนวนที่ต้องสุ่มมา */
				if($num > 1){//ต้องโทรหาลูกค้ามากกว่า 1 คนจึงจะสุ่ม
					$num = round($num * ($rs_fl_ev['random_display']/100));//หารแบบปัดเศษตามค่าที่ใกล้เคียง , floor ปัดลง,ceil ปัดขึ้น
	//echo "|random = $num|";
					if($num>=1){	//หาจำนวนตามเปอร์เว็นต์แล้วต้องได้ 1 ขึ้นไป
						/* หาไอดีที่ต้องอัพสเตตัส 4 */
						$id_in = sql_in( "SELECT id FROM $config[db_base_name].follow_customer WHERE event_id_ref='".$rs_fl_ev['id']."' AND emp_id_card='".$rs['emp_id_card']."' AND status='3' ORDER BY RAND() LIMIT ".$num);
						$sql = "UPDATE $config[db_base_name].follow_customer SET status='4' WHERE id IN($id_in)";
						$logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						/* หาไอดีที่ต้องอัพสเตตัส 99 */
						$id_in2 = sql_in("SELECT id FROM $config[db_base_name].follow_customer WHERE event_id_ref='".$rs_fl_ev['id']."' AND emp_id_card='".$rs['emp_id_card']."' AND status='3' ");
						if($id_in2){
							checkRemindType($rs_fl_ev['id'],$id_in2);//ส่ง event_id ไปตรวจสอบว่าเป็นชนิดที่ต่อเนื่องหรือไม่
							$sql = "UPDATE $config[db_base_name].follow_customer SET status='99',process_by = 'random_customer',process_date=NOW() WHERE id IN($id_in2)";
							$logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						}
						/* อัพเดทว่าสุ่มแล้ว */
						$sql = "UPDATE $config[db_base_name].follow_main_event SET random_date='".date('Y-m-d')."' WHERE id='".$rs_fl_ev['id']."'";
						$logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					}
				}
			}
		}

	}// end function
}

if (!function_exists('randomCus')) {
	function randomCus() {           //emp's event
		global $config, $logDb;
		updateExpire();		//อัพเดต status ของรายการที่เลยกำหนดแล้ว
		//$id_card 	= $_SESSION['SESSION_ID_card'];
		$today 		= date('Y-m-d');

		$sqlQuery = "SELECT DISTINCT(emp_id_card) AS emp_id_card FROM $config[db_base_name].follow_customer 
		WHERE (start_date <= '$today' AND stop_date >= '$today') AND status ='0' ";
		$objQuery = $logDb->queryAndLogSQL( $sqlQuery, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$numRow = mysql_num_rows($objQuery);
		$arrEmp = array();
		while($sr = mysql_fetch_assoc($objQuery)){
			$arrEmp[] = $sr['emp_id_card'];	
		} // end while
		
		for($numLoop=0;$numLoop<$numRow;$numLoop++){
			$id_card = $arrEmp[$numLoop];
		
			$sql 	= "SELECT DISTINCT(event_id_ref) FROM $config[db_base_name].follow_customer ";
			$sql   .= "WHERE emp_id_card IN('$id_card') AND (start_date <= '$today' AND stop_date >= '$today') AND status ='0' ";//ยังไม่ได้สุ่ม
			$event_in_id = sql_in($sql);//หาไอดี ของ event

			if($event_in_id){
				$sql = "SELECT id,follow_type,remind_type,remind_value,remind_unit,random_display FROM 
				$config[db_base_name].follow_main_event WHERE id IN($event_in_id) AND visible != 'no' ";   //สุ่มเฉพาะเหตุการณ์ที่ให้แสดงเท่านั้น
				$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" ); //ดึงค่าใน main_event มาใช้
				$arr_event = array();
				$num_event = mysql_num_rows($result);
				while ($rs = mysql_fetch_assoc($result)){   		//วนลูปเพื่อหาเงื่อนไขการอัพเดต
					// $event_id 		= $rs['id'];
					// $remind_type	= $rs['remind_type']; 			//รูปแบบการแจ้งเตือน
					// $remind_value	= $rs['remind_value'];		// จำนวน 1,2,3,...,n
					// $remind_unit	= $rs['remind_unit'];			//หน่วย วัน สัปดาห์ เดือน ปี
					// $random_display	= $rs['random_display'];	//เปอร์เซ็นต์การสุ่ม

					$arr_event['event_id'][] 		= 	$rs['id'];
					$arr_event['remind_type'][] 	=	$rs['remind_type'];
					$arr_event['remind_value'][] 	=  	$rs['remind_value'];
					$arr_event['remind_unit'][] 	=  	$rs['remind_unit'];
					$arr_event['random_display'][] 	=  	$rs['random_display'];

				}//while loop
			
			
				for($iList=0;$iList<$num_event;$iList++){  
					$sql = "SELECT COUNT(id) FROM $config[db_base_name].follow_customer
							WHERE event_id_ref='".$arr_event['event_id'][$iList]."' AND emp_id_card IN('$id_card')
							AND (start_date <= '$today' AND stop_date >= '$today') AND status='0'
							GROUP BY event_id_ref";
					$cusNum = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$rsNum	= mysql_fetch_array($cusNum);
					$num 	= $rsNum[0];

					//if($num > 1){//ต้องมีลูกค้าในเหตุการณ์นั้นมากกว่า 1 คนจึงจะสุ่ม !!! ใช้สำหรับ สุมสัมภาษณ์เท่านั้น  2010-11-15
					$num = round($num * ($random_display/100));//หารแบบปัดเศษตามค่าที่ใกล้เคียง , floor ปัดลง,ceil ปัดขึ้น

					if($num>=1){	//หาจำนวนตามเปอร์เซ็นต์แล้วต้องได้ 1 ขึ้นไป
						/* หาไอดีที่ต้องอัพสเตตัส 1 */
						$randSQL = "SELECT id FROM $config[db_base_name].follow_customer WHERE 
						event_id_ref='".$arr_event['event_id'][$iList]."' AND emp_id_card IN('$id_card') 
						AND (start_date <= '$today' AND stop_date >= '$today') AND status='0' ORDER BY RAND() 
						LIMIT ".$num;
						$id_in = sql_in($randSQL);
						$sql = "UPDATE $config[db_base_name].follow_customer SET status='1' WHERE id IN($id_in)";
						$logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" ); //อัพเดต status ของการติดตามที่ถูกสุ่มขึ้นมา

						/* หาไอดีที่ต้องอัพสเตตัส 99 */
						$id_in99 = sql_in("SELECT id FROM $config[db_base_name].follow_customer WHERE 
						event_id_ref='".$arr_event['event_id'][$iList]."' AND emp_id_card IN('$id_card') 
						AND (start_date <= '$today' AND stop_date >= '$today') AND status='0' ");
						//echo '<br>',$id_in99;
						if($id_in99){
							// checkRemindType($arr_event['event_id'][$iList],$id_in99); //ส่ง event_id ไปตรวจสอบว่าเป็นชนิดที่ต่อเนื่องหรือไม่
							$sql = "UPDATE $config[db_base_name].follow_customer SET status='99',process_by='randomCus',process_date=NOW() WHERE id IN($id_in99)";
							$logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" ); //อัพเดต status ของการติดตามที่ไม่ได้สุ่มขึ้นมา
						}
					
					} // end if($num>=1){
					
				} // end for loop
			
			} // end if($event_in_id){
		
		}  //for($numLoop=0;$numLoop<$numRow;$numLoop++){
		
		
		/* ---------------------- Note random ตาม ผู้รับผิดชอบตาม OU -----------
		* --------------------------------------------------------------- */
		$ResponList = array();
		
		$sqlResponQuery = "SELECT follow_event_responsible.main_event_id,follow_main_event.remind_type,follow_main_event.remind_value ,
		follow_main_event.remind_unit,follow_main_event.random_display FROM $config[db_base_name].follow_event_responsible 
		Inner Join $config[db_base_name].follow_main_event ON follow_event_responsible.main_event_id = follow_main_event.id 
		WHERE follow_main_event.follow_up_activities = '2' AND follow_main_event.visible <> 'no' GROUP BY main_event_id";
		$objResponQuery = $logDb->queryAndLogSQL( $sqlResponQuery, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($res = mysql_fetch_assoc($objResponQuery)){
			
			$sqlResponListQuery = "SELECT * FROM $config[db_base_name].follow_event_responsible 
			WHERE main_event_id = '".$res['main_event_id']."' ";
			$objResponListQuery = $logDb->queryAndLogSQL( $sqlResponListQuery, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			
			while($resList = mysql_fetch_assoc($objResponListQuery)){
				if($resList['resps_bu']){ 		 
					$ResponList[$res['main_event_id']]['resps_bu'][] 	    = $resList['resps_bu'];         
				}		
				if($resList['resps_comp']){		 
					$ResponList[$res['main_event_id']]['resps_comp'][] 	    = $resList['resps_comp'];       
				}
				if($resList['resps_department']){ 
					$ResponList[$res['main_event_id']]['resps_department'][]= $resList['resps_department']; 
				}
				if($resList['resps_section']){ 	 
					$ResponList[$res['main_event_id']]['resps_section'][] 	= $resList['resps_section'];   
				}
				if($resList['resps_position']){   
					$ResponList[$res['main_event_id']]['resps_position'][]  = $resList['resps_position'];   
				}
			
			}
			
			
			if($res['remind_type']){   
				$ResponList['remind_type#@'.$res['main_event_id']][]  = $res['remind_type'];   
			}
			if($res['remind_value']){   
				$ResponList['remind_value#@'.$res['main_event_id']][]  = $res['remind_value'];   
			}
			if($res['remind_unit']){   
				$ResponList['remind_unit#@'.$res['main_event_id']][]  = $res['remind_unit'];   
			}
			if($res['random_display']){   
				$ResponList['random_display#@'.$res['main_event_id']][]  = $res['random_display'];   
			}
			
		}
		
		
		// $sql 	= "SELECT DISTINCT(event_id_ref) FROM $config[db_base_name].follow_customer ";
		// $sql   .= "WHERE emp_id_card IN('$id_card') AND (start_date <= '$today' AND stop_date >= '$today') AND status ='0' ";//ยังไม่ได้สุ่ม
		// $event_in_id = sql_in($sql);//หาไอดี ของ event

		
		
		// echo "<br> <br><p style='color:#ff5463;'>function randomCus </p><textarea>";
		// print_r($ResponList);
		// echo "</textarea>";
		
		// echo "<br><br>";
		if(!empty($ResponList)){
			foreach($ResponList as $eventId => $arrayValue){ 
				// echo "<br><br><br>";
			
				if(abs($eventId)){
					$numLimit = 0;
					// c status 0 ยังไม่ได้สุ่ม
					// echo "SELECT id FROM $config[db_base_name].follow_customer WHERE 
					// event_id_ref = '".$eventId."' AND (start_date <= '$today' AND stop_date >= '$today') AND status ='0' ";
					
					$sqlFollwerQuery = "SELECT id FROM $config[db_base_name].follow_customer WHERE 
					event_id_ref = '".$eventId."' AND (start_date <= '$today' AND stop_date >= '$today') AND status ='0' ";
					$objFollwerQuery = $logDb->queryAndLogSQL( $sqlFollwerQuery, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					// echo "numRows >>", $numRows = mysql_num_rows($objFollwerQuery);
					
					// echo "<br><font color='#FF8000'>random_display</font> >> ",$ResponList['random_display#@'.$eventId][0];
					$random_display = $ResponList['random_display#@'.$eventId][0];
					//if($numRows > 1){//ต้องมีลูกค้าในเหตุการณ์นั้นมากกว่า 1 คนจึงจะสุ่ม !!! ใช้สำหรับ สุมสัมภาษณ์เท่านั้น  2010-11-15
					$numRows = round($numRows * ($random_display/100));//หารแบบปัดเศษตามค่าที่ใกล้เคียง , floor ปัดลง,ceil ปัดขึ้น

					if($numRows>=1){	//หาจำนวนตามเปอร์เซ็นต์แล้วต้องได้ 1 ขึ้นไป
						/* หาไอดีที่ต้องอัพสเตตัส 1 */
						$randSQL = "SELECT id FROM $config[db_base_name].follow_customer WHERE event_id_ref='".$eventId."' AND (start_date <= '$today' AND stop_date >= '$today') 
						AND status='0' ORDER BY RAND() LIMIT ".$numRows;
						$id_in = sql_in($randSQL);
						$sql = "UPDATE $config[db_base_name].follow_customer SET status='1' WHERE id IN($id_in)";
						$logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" ); //อัพเดต status ของการติดตามที่ถูกสุ่มขึ้นมา

						/* หาไอดีที่ต้องอัพสเตตัส 99 */
						$id_in99 = sql_in("SELECT id FROM $config[db_base_name].follow_customer WHERE event_id_ref='".$eventId."'  
						AND (start_date <= '$today' AND stop_date >= '$today') AND status='0' ");
						//echo '<br>',$id_in99;
						if($id_in99){
							// checkRemindType($eventId,$id_in99); //ส่ง event_id ไปตรวจสอบว่าเป็นชนิดที่ต่อเนื่องหรือไม่
							$sql = "UPDATE $config[db_base_name].follow_customer SET status='99',process_by='randomCus',process_date=NOW() WHERE id IN($id_in99)";
							$logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" ); //อัพเดต status ของการติดตามที่ไม่ได้สุ่มขึ้นมา
						}
					
					} // end if($numRows>=1){
				 
				 
				 
				 
					/* 
					echo "<br><br>eventId >>",$eventId,'       :::::     ';  
					echo "numArray>>",$numArray = count($ResponList[$eventId]);
					if($numArray>0){
					
						for($i=0;$i<$numArray;$i++){
						
							echo "<br>numLimit >> ",$numLimit    = round($numRows/$numArray);
							
							echo "<br>",
							$objRandomQuery = "SELECT id FROM $config[db_base_name].follow_customer WHERE 
							event_id_ref = '".$eventId."' AND (start_date <= '$today' AND stop_date >= '$today') 
							AND status ='0' LIMIT $numLimit";
							$id_in_all = sql_in($objRandomQuery);
							
							//    หาจำนวนการ random display
							echo "<br>random_display >>",$random_display = $ResponList['random_display#@'.$eventId][0];
							
							echo "<br>iNum >>",$iNum = round($numLimit * ($random_display/100));//หารแบบปัดเศษตามค่าที่ใกล้เคียง , floor ปัดลง,ceil ปัดขึ้น

							if($iNum>0){  
								// หาไอดีที่ต้องอัพสเตตัส 1 
								echo "<br>randSQL",$randSQL = "SELECT id FROM $config[db_base_name].follow_customer WHERE event_id_ref='".$eventId."' 
								AND id IN($id_in_all) ORDER BY RAND() LIMIT ".$iNum;
								$id_in = sql_in($randSQL);
								echo "<br>id_in",$id_in;
								//อัพเดต status ของการติดตามที่ถูกสุ่มขึ้นมา
								mysql_query("UPDATE $config[db_base_name].follow_customer SET status='1' WHERE id IN($id_in)");

								//หาไอดีที่ต้องอัพสเตตัส 99 
								$id_in99 = sql_in("SELECT id FROM $config[db_base_name].follow_customer WHERE 
								event_id_ref='".$eventId."' AND id IN($id_in_all) AND status='0' ");
								
								echo '<br>',$id_in99;
								if($id_in99){
									
									//ส่ง event_id ไปตรวจสอบว่าเป็นชนิดที่ต่อเนื่องหรือไม่
									checkRemindType($eventId,$id_in99); 
									$sql = "UPDATE $config[db_base_name].follow_customer SET status='99',process_by='randomCus',process_date=NOW() WHERE id IN($id_in99)";
									mysql_query($sql);//อัพเดต status ของการติดตามที่ไม่ได้สุ่มขึ้นมา
								
								} // end if($id_in99){

							}  // end if($iNum>0){  
							
						
							$numRows = $numRows-$numLimit;
						
						} // end for($i=0;$i<$numArray;$i++){
					
					} // end if($numArray>0){
					
					echo "<br><br>"; 
					
					*/
					
					
					
				}  // end if(abs($eventId)){
			}
		}
		
		
		
		// if(!empty($ResponList['resps_bu'])){
			// foreach($ResponList['resps_bu'] as $keys => $value){
				
			// }
		// }
	}
}

if (!function_exists('updateExpire')) {
	function updateExpire() {        //อัพเดตรายการที่เลยกำหนดทำ
		global $config, $logDb;
		//อัพเดตรายการที่เลยกำหนด
		if($_SESSION['updateExpire']!=session_id()){//ตรวจสอบว่า session นี้เคยอัพเดตหรือยัง
		    //status 1 = สุ่มแล้วรอโทรหา, 4 = สุ่มแล้วรอสัมภาษณ์
			$f_sql = "SELECT DISTINCT(event_id_ref) FROM $config[db_base_name].follow_customer WHERE status IN('0','1','3','4') AND stop_date < '".date('Y-m-d')."'";
			$in_id = sql_in($f_sql);//หาไอดี ของ eventของแถวที่หมดอายุการติดตามแล้ว
			if($in_id){
				$sql = "SELECT id,remind_type,remind_value,remind_unit,target_count,department,pattern_id_ref,status FROM $config[db_base_name].follow_main_event ";
				$sql.= "WHERE id IN($in_id) ";
				$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" ); //หา event ตามไอดีที่ได้มา
				while ($rs = mysql_fetch_assoc($result)){//วนลูปเพื่อหาเงื่อนไขการอัพเดต
					$id 	= $rs['id']; //ไอดี เหตุการณ์
					$type	= $rs['remind_type']; //รูปแบบการแจ้งเตือน
					$value  = $rs['remind_value'];// จำนวน 1,2,3,...,n
					$unit	= $rs['remind_unit'];//หน่วย วัน สัปดาห์ เดือน ปี
					//สร้างอาร์เรย์
					$event[$id]['type']  = $type;
					$event[$id]['value'] = $value;
					$event[$id]['unit']	 = $unit;
					$event[$id]['status']= $rs['status'];

					//2011-08-29 ใช้อ้างอิงตอนสร้างการติดตามครั้งต่อไป
					$event[$id]['department'] 		 = $rs['department'];
					$event[$id]['pattern_id_ref']	 = $rs['pattern_id_ref'];

					if($rs['target_count']!='no'){//เหตุการณ์ที่ ให้ทำ target_count!='no'
						$event_todo_id .= $sign.$id;
						if($sign==""){$sign="','";}
					}elseif($rs['target_count']=='no') {//เหตุการณ์ที่ให้แสดงอย่างเดียว
						$event_show_only .= $t.$id;
						if($t==""){$t="','";}
					}
				}
			}
			//$event_id = 1','2','3','4
			//ค้นหารายการที่ต้องสร้างการติดตามครั้งใหม่
			$sql = "SELECT id,event_id_ref FROM $config[db_base_name].follow_customer WHERE status IN('0','1','3','4') 
			AND stop_date < '".date('Y-m-d')."'";
			$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			while ($fl = mysql_fetch_assoc($result)){

				$follow_id    = $fl['id'];
				$event_id     = $fl['event_id_ref'];//ไอดี เหตุการณ์

				$remind_type  = $event[$event_id]['type'];
				$remind_value = $event[$event_id]['value'];
				$remind_unit  = $event[$event_id]['unit'];



				if($remind_type=="alway"){	//ตรวจสอบประเภทของเหตุการณ์ว่าเป็นแบบ ต่อเนื่องหรือไม่
					createNextRemind($follow_id,$remind_value,$remind_unit, $event[$event_id]);
				}
			}
			
			//อัพเดตรายการที่เลยกำหนด และต้องทำ
			if($event_todo_id){
				$sql1 = "UPDATE $config[db_base_name].follow_customer SET status='98',process_by='updateExpire98',process_date=NOW() WHERE event_id_ref IN('$event_todo_id') AND status IN('1','4') AND stop_date < '".date('Y-m-d')."'";
				$logDb->queryAndLogSQL( $sql1, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			}
			//รายการที่เลยกำหนด แต่ไม่ต้องทำ
			if($event_show_only){
				$sql2 = "UPDATE $config[db_base_name].follow_customer SET status='99',process_by='updateExpire99',process_date=NOW() WHERE event_id_ref IN('$event_show_only') AND status IN('1','4') AND stop_date < '".date('Y-m-d')."'";
				$logDb->queryAndLogSQL( $sql2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			}

			//เหตุการณ์ที่เลยกำหนดแล้ว แต่ไม่ได้เปิดดู
			$sql3 = "UPDATE $config[db_base_name].follow_customer SET status='95',process_by='updateExpire95',process_date=NOW() WHERE status IN('0','3') AND stop_date < '".date('Y-m-d')."'";
			$logDb->queryAndLogSQL( $sql3, " FILE : ".__FILE__." LINE : ".__LINE__."" );

			$_SESSION['updateExpire'] = session_id();
		}
	}
}

if(!function_exists('sql_in')){
	//echo "<br>sql_in >> ",$sql;
	function sql_in($sql){//จัดเรียง ข้อมูลที select ให้อยู่ในรุปแบบ 'a1','a2',...,'an'
		global $logDb;
	
		$a = 0;
		$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		if($re){
			while($rs=mysql_fetch_array($re)){
				if($a>0){$t .= ",";}
				$t .= $rs[0];
				$a++;
			}
			mysql_free_result($re);
		}
		if($t){$text = "'".str_replace(",","','",$t)."'";}
		
		//echo "<br> text >>",$text;
		
		return $text;
	}
}

/**
 *
 * สร้างการติดตามครั้งต่อไป
 * @param Number $follow_id รหัสการติดตามครั้งก่อน
 * @param Number $remind_value	ตัวเลขกำหนดระยะการติดตาม
 * @param String $remind_unit	หน่วยของระยะการติดตาม
 * @param Array $arr	อาร์เรย์ของกิจกรรมที่ดึงมา เพื่อใช้ตรวจสอบข้อกำหนดของกิจกรรม
 */
if (!function_exists('createNextRemind')) {
	function createNextRemind($follow_id,$remind_value,$remind_unit,$arr=null) {//สร้างการติดตามครั้งต่อไป (ไม่ใช่สร้าง event refer!!)
		global $config, $logDb;

		/* if($arr['status']!='10' && $arr['status']!='99'){//กิจกรรมพิเศษ กับที่ยกเลิกไปแล้ว จะไม่สร้างใหม่

			// ดึงข้อมูลจาก เรคอร์ดเดิม มาบวกวันที่เพิ่มเข้าไป
			mysql_query('SET NAMES UTF8');
			$sqlFl = "SELECT * FROM $config[db_base_name].follow_customer WHERE id = '$follow_id'";
			$fl = $logDb->queryAndLogSQL( $sqlFl, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$rs = mysql_fetch_assoc($fl);

			$event_id_ref	=	$rs['event_id_ref'];
			$follow_type	=	$rs['follow_type'];
			$chassi_no_s	=	$rs['chassi_no_s'];
			$cus_no			=	$rs['cus_no'];
			$cus_name		=	$rs['cus_name'];
			$start_date		=	$rs['start_date'];
			$stop_date		=	$rs['stop_date'];
			$status			= 	$rs['status'];
			$bu_id			=	$rs['biz_id_ref'];

			//$sql = "SELECT Cus_Name,Cus_Surename,Resperson FROM $config[db_maincus].MainCusData WHERE CusNo = '$cus_no'";
			$sql = "SELECT cus.Cus_Name,cus.Cus_Surename,resp.RESP_IDCARD AS Resperson FROM $config[db_maincus].MAIN_CUS_GINFO 
			cus LEFT JOIN $config[db_maincus].MAIN_RESPONSIBILITY resp ON cus.CusNo=resp.RESP_CUSNO WHERE CusNo='".$cus_no."' 
			AND resp.RESP_CUSNO='".$cus_no."' AND resp.BUSINESS_REFS='".$bu_id."' LIMIT 1";
			mysql_query('SET NAMES UTF8');
			$resperSQL = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$main_cus = mysql_fetch_assoc($resperSQL);

			//ถ้าเป็นเหตุการณ์ของ Sale และไม่ใช่รายการรอสัมภาณ์
			//หาผู้รับผิดชอบหลังการขาย (กรณีเปลี่ยนผู้รับผิดชอบไปแล้ว แต่เรคอรดนี้ไม่ถูกอัพด้วยเพราะไม่อยู่ในเงื่อนไข ณ ขณะนั้น)
			// if($arr['department']==$config['phF_CheckSale'] && $arr['pattern_id_ref']!='')
			// {
				// $emp_id_card = $main_cus['Resperson'];
				// if($emp_id_card==''){
					// return;//แสดงว่าถูกยกเลิกการดูแลไปแล้ว ไม่ต้อง INSERT อีก
				// }
			// }
			
			if($arr['pattern_id_ref']!=''){
				$emp_id_card = $main_cus['Resperson'];
				if($emp_id_card==''){
					return;//แสดงว่าถูกยกเลิกการดูแลไปแล้ว ไม่ต้อง INSERT อีก
				}
			}

			//เผื่อลูกค้าเปลี่ยนชื่อ
			$cus_name		= $main_cus['Cus_Name'].' '.$main_cus['Cus_Surename'];
			$nextStartDate  = getAfterDate($start_date,$remind_value,$remind_unit);//เพิ่มวันที่เริ่มโทร
			$nextStopDate 	= getAfterDate($stop_date,$remind_value,$remind_unit);//เพิ่มวันที่เลยกำหนดการโทร

			$nextStatus		= createNewStatus($status);//สร้างสถานะการติดตามใหม่

			//ข้อกำหนดกิจกรรม TIS
			$sql   =	"SELECT TIS_Event_Group, TIS_Event_Period, TIS_Event_PeriodCode, send_to_tis
						FROM $config[db_base_name].follow_main_event WHERE id = '$event_id_ref'";
			$res   = 	$logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$event = 	mysql_fetch_assoc($res);

			//บันทึกข้อมูล
			$nextsql = "INSERT INTO $config[db_base_name].follow_customer ";
			$nextsql .= "( event_id_ref,		follow_type,		chassi_no_s,		cus_no,			cus_name,
							emp_id_card,		start_date,			stop_date,		process_by,	process_date,	status,
							TIS_Event_Group, TIS_Event_Period, TIS_Event_PeriodCode, send_to_tis";
			$nextsql .= ")VALUES(";
			$nextsql .= " '$event_id_ref',		'$follow_type',		'$chassi_no_s',		'$cus_no',		'$cus_name',
						  '$emp_id_card',		'$nextStartDate',	'$nextStopDate',	'nextRemind',	NOW(),	'$nextStatus',
						  '$event[TIS_Event_Group]', '$event[TIS_Event_Period]', '$event[TIS_Event_PeriodCode]',
						  '$event[send_to_tis]')";
			mysql_query("SET NAMES UTF8");
			$logDb->queryAndLogSQL( $nextsql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		}//if status */
	}//func
}

if (!function_exists('createNewStatus')) {
	function createNewStatus($status) {//อัพเดตสถานะ
		if($status=="1" or $status=="0"){
			$nextStatus = "0";//0 = รอสุ่ม จะเริ่มติดตามใหม่ (กรณีเป็นการติดตามแบบต่อเนื่อง alway)
		}else if($status=="4" or $status=="3") {
			$nextStatus = "3";	//3 = รอสัมภาษณ์ (กรณี รอสัมภาษณ์ ต้องทำแบบต่อเนื่อง alway)
		}
		return $nextStatus;
	}
}

if (!function_exists('checkRemindType')) {	
	function checkRemindType($event_id,$follow_id_in){//ตรวจสอบรูปแบบการแจ้งเตือนก่อน update สถานะ
		global $config, $logDb;
		$sql = "SELECT remind_type,remind_value,remind_unit,department,pattern_id_ref,status FROM 
		$config[db_base_name].follow_main_event WHERE id = '$event_id' ";
		$result 		= $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$rs 			= mysql_fetch_assoc($result);
		// $remind_type	= $rs['remind_type'];  			//รูปแบบการแจ้งเตือน
		// $remind_value	= $rs['remind_value']; 		// จำนวน 1,2,3,...,n
		// $remind_unit	= $rs['remind_unit'];  			//หน่วย วัน สัปดาห์ เดือน ปี

		$follow_cus_id  = str_replace("'","",$follow_id_in);//$follow_id_in จะส่งมาแบบ '1','2','5','90',...,'n'
		$follow_id 		= explode(",",$follow_cus_id);

		if($rs['remind_type']=="alway"){	//ถ้าต่อเนื่อง ให้สร้างการติดตามครั้งใหม่
			foreach($follow_id as $fl_id){
				createNextRemind($fl_id, $rs['remind_value'], $rs['remind_unit'], $rs);
			}
		}
	}
}

//ประเภทการยกเลิก
//ic_followcus_show.php
//ic_followcus_show_special.php
//ic_followcus2.php
if (!function_exists('follow_TypeCancel')) {
	function follow_TypeCancel($value=null){
		global $config, $logDb;
		
		$sql = "SELECT type_id, type_group, type_name FROM $config[db_base_name].cancel_type WHERE status!='99' ORDER BY type_name ASC";
		$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		$option = '<option value="">-- เลือกเหตุผลในการยกเลิก --</option>';
		while($fe = mysql_fetch_assoc($que)){
			$cutTGroup = explode(',',$fe['type_group']);
			
			foreach($cutTGroup AS $key => $tgroup){
				if($tgroup!=1){
					$selected = '';
					$display = 'style="display: none;"';
				}else{
					if($fe['type_id']==$value){ $selected = 'selected="selected"'; }else{ $selected = ''; }
					$display = '';
				}
				//if($fe['type_name']==$name){ $selected = 'selected="selected"'; }else if(!$selected){ $selected = ''; }
				$option .= '<option value="'.$fe['type_id'].'" tgroup="'.$tgroup.'" '.$selected.' '.$display.'>'.$fe['type_name'].'</option>';
			}
		}
		
		return $option;
	}
}

/* P'Add 2012-11-28 ดึงชื่อลูกน้องตามตำแหน่ง */
if (!function_exists('get_underling_position')) {
	function get_underling_position(){
		global $config, $logDb;
		/* $whereS = "command.Leader IN(".$_SESSION['SESSION_Position_id'].")";
		$sqlS = "SELECT rel.id_card FROM $config[db_organi].command LEFT JOIN $config[db_organi].relation_position rel ON command.Follower=rel.position_id ";
		$sqlS .= "WHERE $whereS AND command.Status!='99' AND rel.status!='99' GROUP BY rel.id_card ORDER BY rel.position_id,rel.NameUse ASC";
		// echo '<font color="#0000FF">FILE :: '.__FILE__.' >> LINE :: '.__LINE__.'</font><br/>'.$sqlS.'<br/><br/>';
		$queS = mysql_query($sqlS) or die("<br>".$sql."<br>Error : ".__FILE__." Line : ".__LINE__."<br>".mysql_error());
		$arr_id_card = array();
		while($arrS = mysql_fetch_assoc($queS)){array_push($arr_id_card,$arrS['id_card']);} */
		
		$sqlMap 	= "SELECT WorkCompany, Department, Section, Level FROM ".$config['db_organi'].".position WHERE Status!=99 AND id=".$_SESSION['SESSION_Position_id']." LIMIT 1";
		$queMap 	= $logDb->queryAndLogSQL( $sqlMap, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$feMap  	= mysql_fetch_assoc($queMap);
		
		$sqlUnder 	= "SELECT rel.id_card FROM ".$config['db_organi'].".position INNER JOIN ".$config['db_organi'].".relation_position rel ON position.id=rel.position_id ";
		$sqlUnder	.= "WHERE position.Status!=99 AND position.WorkCompany='".$feMap['WorkCompany']."' AND position.Department='".$feMap['Department']."' AND ";
		$sqlUnder	.= "position.Level>".abs($feMap['Level'])." AND rel.status!=99";
		$queUnder 	= $logDb->queryAndLogSQL( $sqlUnder, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		$arr_id_card = array();
		while($feUnder = mysql_fetch_assoc($queUnder)){
			$arr_id_card[]	= $feUnder['id_card'];
		}
		
		/* echo "<pre>";
		print_r($arr_id_card);
		echo "</pre>"; */
		
		return $arr_id_card;
	}
}

if (!function_exists('getCommandByPosition')) {
	/** 
		BEN Add 2013-02-06 
		Description : ดึงลูกน้องตามสายงาน เอาเฉพาะ under ตำแหน่งที่ log in เข้าระบบ
		$include_leader =  1 ไม่รวมคนล็อกอิน 2 รวมคนล็อกอิน
	*/
	$follower_result = array();
	$follower_id_card = array();
	$follower_pos = array();
	function getCommandByPosition($login_position , $include_leader=1){
		global $config ,$follower_result , $follower_id_card , $follower_pos, $logDb;

		if($include_leader == 2){
			$follower_id_card[] = $_SESSION['SESSION_ID_card'];
			$follower_pos[]	= $login_position;
		}
		
		$check_follower = "SELECT Follower,Status FROM $config[db_organi].command ";
		$check_follower .= " WHERE Leader = '$login_position'  ";//AND command.Status != 99
		// echo $check_follower."<br/><br/>";
		$check_follower_result = $logDb->queryAndLogSQL( $check_follower, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$check_follwer_num_rows = mysql_num_rows($check_follower_result);
		if($check_follwer_num_rows > 0){
			while($follower_rows = mysql_fetch_object($check_follower_result)){
				if($follower_rows->Status != 99 ) {
					$emp_query = "SELECT id_card,position_id FROM $config[db_organi].relation_position WHERE position_id = '".$follower_rows->Follower."' AND status != 99";
					// echo $emp_query."<br/><br/>";
					$emp_result = $logDb->queryAndLogSQL( $emp_query, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					while($emp_row = mysql_fetch_object($emp_result)){
						$follower_id_card[] 	= $emp_row->id_card;
						$follower_pos[]			= $emp_row->position_id;
					}
					getCommandByPosition($follower_rows->Follower);
				}
				
			}
		}
		$follower_result[0] = $follower_id_card;
		$follower_result[1] = $follower_pos;
		return $follower_result;
	}
	/********************************* END FUNCTION ********************************/
}

?>