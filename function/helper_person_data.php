<?php
// 2011-06-09
//ใช้สำหรับดึงข้อมูลบุคคล ทำงานภายใต้คอนฟิกระบบ และการเชื่อมต่อฐานข้อมูลแล้ว
// ฟังก์ชั่น ข้อมูลพนักงาน ข้อมูลลูกค้า ข้อมูลลูกทีม

$config[charset] = 'UTF8';

/**
 * ใช้สำหรับดึงข้อมูลพนักงาน สามารถกำหนดฟิลด์ที่ต้องการ และเงื่อนไขในการดึงข้อมูลได้
 *
 * @param Array $where เงื่อนไขที่ต้องการ Data_No=10 AND ID_card = 1234567890123
 * @return CONCAT(Name,' ',Surname,'(',Nickname,')')
 */
function helper_get_emp_name($where){
	global $config;
	$sql = "SELECT CONCAT(Name,' ',Surname,'(',Nickname,')') as emp_name FROM $config[Member].emp_data WHERE $where LIMIT 1";
	mysql_query("SET NAMES $config[charset]");
	$result = mysql_query($sql) or die(__FILE__.' line '.__LINE__.'<br>'.mysql_error().'<br>'.$sql);
	$arr = mysql_fetch_assoc($result);
	return $arr['emp_name'];
}

/**
 * ดึงข้อมูลผู้ดูแลหลังการขาย
 * @param String $dbase ชื่อฐานข้อมูล
 * @param Number $cusno รหัสลูกค้า
 */
function helper_get_cus_resperson($dbase, $cusno)
{
	$sql = "SELECT Resperson FROM $dbase WHERE CusNo = '$cusno' ";
	mysql_query("SET NAMES $config[charset]");
	$result = mysql_query($sql) or die(__FILE__.' line '.__LINE__.'<br>'.mysql_error().'<br>'.$sql);
	$arr = mysql_fetch_assoc($result);
	return $arr['Resperson'];
}


#### ยังไม่ใช้ แค่ลองเขียนดู แต่ไม่รู้ว่าจะเขียนต่อยังไง
function check_department($id_card)
{
	global $config;
	$depart_ment = '';
	# ถ้าใช้กับ OU ให้เปลี่ยน $case ให้ตรงตามแผนก
	switch($case)
	{
		case 'SA': $depart_ment = $config['sale']; break;		//ขาย
		case 'SD': $depart_ment = $config['sale_admin']; break;	//บริหารการขาย
		case 'MA': $depart_ment = $config['cr']; break;			//CR, การตลาด
		case 'EX': $depart_ment = $config['Manager']; break;	//ผู้บริหาร, admin
	}
	return $depart_ment;
}

?>