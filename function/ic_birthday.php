<?php
	function list_birthday($arr){
		global $config, $logDb, $objMain;
		
		if($arr['sDB']<1){
			unset($_SESSION['birthDayExcel']);
			$_SESSION['birthDayExcel'] = array();
		}
		
		if(!$arr['month']){ $arr['month'] = date('m'); }
		if(strlen($arr['month'])=="1"){ $arr['month'] = "0".$arr['month']; };
		if(!$arr['start']){ $arr['start'] = date('d'); } ## วันที่เริ่มต้น
		if(!$arr['stop']){ $arr['stop'] = date('d'); } ## วันที่สิ้นสุด
		
		$return	= $arr;
		$return['html']	= '';
		$sql = "SELECT ";
		$sql .= "MAIN_CUS_GINFO.DateOfBirth AS month, ";
		$sql .= "MAIN_CUS_GINFO.CusNo, ";
		$sql .= "MAIN_CUS_GINFO.Be, ";
		$sql .= "MAIN_CUS_GINFO.Cus_Name, ";
		$sql .= "MAIN_CUS_GINFO.Cus_Surename,";
		$sql .= "AMIAN_CUS_TYPE.CUS_TYPE_LEVEL_DESC ";
		$sql .= "FROM ";
		$sql .= "".$config['db_maincus'].".MAIN_CUS_GINFO ";
		$sql .= "INNER JOIN ".$config['db_maincus'].".MIAN_CUS_TYPE_REF ON MAIN_CUS_GINFO.CusNo = MIAN_CUS_TYPE_REF.CTYPE_REF_CUSNO "; 
		$sql .= "LEFT JOIN ".$config['db_maincus'].".AMIAN_CUS_TYPE ON MIAN_CUS_TYPE_REF.CTYPE_GRADE_REF = AMIAN_CUS_TYPE.CUS_TYPE_ID "; 
		$sql .= "WHERE MAIN_CUS_GINFO.DateOfBirth LIKE '_____".$arr['month']."%' "; 
		$sql .= "AND RIGHT(MAIN_CUS_GINFO.DateOfBirth,2) BETWEEN '".$arr['start']."' AND '".$arr['stop']."' ";
		$sql .= "AND MIAN_CUS_TYPE_REF.CTYPE_GRADE_REF!=0 ";
		$sql .= "AND TRIM(AMIAN_CUS_TYPE.CUS_TYPE_LEVEL_DESC) NOT IN('NoType','Dead','Dead','RegData','57000','VIP.C','Service.B','MassMedia','Old','') "; 
		// $sql .= "AND MAIN_CUS_GINFO.CusNo=4300148 ";
		$sql .= "GROUP BY MIAN_CUS_TYPE_REF.CTYPE_REF_CUSNO, MIAN_CUS_TYPE_REF.CTYPE_GRADE_REF ";
		$sql .= "ORDER BY SUBSTRING_INDEX(MAIN_CUS_GINFO.DateOfBirth,'-',-1) ASC, CONCAT_WS(' ',MAIN_CUS_GINFO.Cus_Name, MAIN_CUS_GINFO.Cus_Surename) ASC ";
		$sql .= "LIMIT ".$arr['sDB'].", ".$arr['nDB']."";
		// $return['html'] = $sql;
			
		mysql_query("SET NAMES utf8");
		$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		// progress nz 2011-06-18
		$total_num = mysql_num_rows($re);
		// $progress = 0;
		
		/* if($arr['sDB']<1){
			if(!$total_num){//เพิ่มสำหรับเช็กข้อมูลวันเกิด
				$sql = "SELECT
						MAIN_CUS_GINFO.CusNo
						FROM 
						".$config['db_maincus'].".MAIN_CUS_GINFO 
						LEFT JOIN ".$config['db_maincus'].".MIAN_CUS_TYPE_REF ON MAIN_CUS_GINFO.CusNo = MIAN_CUS_TYPE_REF.CTYPE_REF_CUSNO 
						WHERE MAIN_CUS_GINFO.DateOfBirth LIKE '_____".$arr['month']."%' 
						AND MIAN_CUS_TYPE_REF.CTYPE_GRADE_REF!=0 
						AND TRIM(MIAN_CUS_TYPE_REF.CTYPE_REF_CUSNO) IS NULL 
						GROUP BY MIAN_CUS_TYPE_REF.CTYPE_REF_CUSNO";
				$res = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$num_all = mysql_num_rows($res);
				if($num_all){
					echo '<h4 align="center">
							<div style="border:1px dashed green;width:50%;background-color:#CCFF99;color:green;padding:5px;margin:10px">
							มีข้อมูลวันเกิดลูกค้า '.number_format($num_all).' ราย ที่ไม่ได้ระบุ "ระดับลูกค้า" (เกรดลูกค้า) </div>
						</h4>';
				}
			}
		} */
		
		while($rs=mysql_fetch_array($re)){
		
			$mainType 			= array();
		
			$arrAdd['CusNo']	= $rs['CusNo'];
			$mainAdd			= $objMain->getMainCusAddress($arrAdd); ## ที่อยู่
			
			$arrTel['type']		= '1,2,3'; ## 1=มือถือ,2=ที่ทำงาน,3=บ้าน,4=fax,5=อื่นๆ
			$arrTel['CusNo']	= $rs['CusNo'];
			$mainTel			= $objMain->getTelePhoneNumber($arrTel); ## เบอร์โทรศัพท์
			
			//modify pt 13-08-2011
			$sql = "SELECT COUNT(Int_Num) AS s
			FROM ".$config['db_easysale'].".CusInt
			WHERE Int_CusNo = '$rs[CusNo]'";
			$ci = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$total_ci = mysql_fetch_array($ci);

			$sql = "SELECT COUNT(Booking_no) AS b
			FROM ".$config['db_easysale'].".Booking
			WHERE B_CusNo = '$rs[CusNo]'";
			$bk = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$total_bk = mysql_fetch_array($bk);
			
			// progress nz 2011-06-18
			// $progress++;
			// create_progress($progress, $total_num, 'ic_birthday');

			$sql = "SELECT COUNT(Sell_No) AS total_buy FROM ".$config['db_easysale'].".Sell WHERE substring_index(cusBuy,'_',1) = '$rs[CusNo]' ";
			$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$total_buy = mysql_fetch_array($result);
			if($total_buy['total_buy']){ $sales_history = "YES"; }else{ $sales_history = "NO"; }
			//$obj = mysql_fetch_object($result);
			//if(!empty($obj->Sell_No)){ $sales_history = "YES"; }else{ $sales_history = "NO"; }
		
			mysql_free_result($result);

			$dateMonth	= dateChange($rs['month'],'Th','-','/','-');
			
			$return['html'] .= '<tr><td>'.$dateMonth.'</td><td>'.$rs['CusNo'].'</td><td>'.$rs['Be'].'</td><td>'.
			$rs['Cus_Name'].' '.$rs['Cus_Surename'].'</td><td>'.$mainAdd['add'].'</td><td>'.$mainAdd['tum'].'</td><td>'.
			$mainAdd['amp'].'</td><td>'.$mainAdd['pro'].'</td><td>'.$mainAdd['code'].'</td><td>'.$rs['CUS_TYPE_LEVEL_DESC'].'</td><td>'.$mainTel[3]['TEL_NUM'].'</td><td>'.
			$mainTel[2]['TEL_NUM'].'</td><td>'.$mainTel[1]['TEL_NUM'].'</td><td>'.$sales_history.'</td><td>'.$total_ci['s'].'</td><td>'.$total_bk['b'].'</td><td>'.$total_buy['total_buy'].'</td></tr>';
		}
		
		$_SESSION['birthDayExcel'][] = $return['html'];
		
		$return['sDB']		= $arr['sDB'] + $arr['nDB']; 	## ค่าเริ่มต้นใหม่
		$return['status']	= ($total_num>0)? 'OK' : 'LIMIT' ;
		// $return['status']	= 'LIMIT' ;
		
		return $return;
	}
?>