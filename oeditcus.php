<?php
require_once ("config/general.php");
require_once ("inc/oeditcus.php");
require_once ("inc/Thailand.php");
require_once ("classes/tc_calendar.php");
require_once ("classes/dbManagement.php");
require_once ("function/editcus.php");
require_once ("function/general.php");
include_once ("function/helper_create_log.php");//ZAN @ 2012-02-14

require_once DIR_LIBRARIES.'files.php';
require_once DIR_LIBRARIES.'files-ff.php';
require_once DIR_LIBRARIES.'FileJobs.php';
require_once DIR_LIBRARIES.'FotoJobs.php';

$CheckManager=substr($_SESSION['SESSION_member_id'],$config['startManager'],$config['cutManager']);  // EX
$CheckDepartment=substr($_SESSION['SESSION_member_id'],$config['startDepartment'],$config['cutDepartment']);
$CheckLevel=substr($_SESSION['SESSION_member_id'],$config['startla_level'],$config['cutla_level']);
$CheckSale=substr($_SESSION['SESSION_member_id'],$config['startCheckSALE'],$config['cutCheckSALE']);
$CodeDivision=substr($_SESSION['SESSION_member_id'],$config['startCheckSALE'],$config['cutCheckSALE']);

$DepartmentArray = array($config['Department'],$config['cr_department']);
$inDepartment = in_array($CheckDepartment,$DepartmentArray);

# Define Default Variable
$oFoto  = new FileJobs;
$interestCheck =$interest =$title = $val =	$vview = null;
Conn2DB();

// Piag $pPostData ใช้เก็บคำสั่งเพื่อเช็คค่าเลขบัตรประชาชนของลูกค้าว่ามีอยู่แล้วหรือยัง ใน even onblur ของ Cus_IDNo ในกรณีที่เพิ่มลูกค้าใหม่
if( $_GET['link']=="new"){$pPostData="followPostData('','oeditcus.php','pCheckIdCard=pCheckIdCard&pIdCard='+this.value)";}
// Piag End $pPostData
// piag ให้แสดงตัวเลือกอาชีพตามประเภทลูกค้า 18-04-2011
if($_GET['pSelectCusType']){
	//$cus_job = CusData_Job('',$_GET['pSelectCusType']);
	$cus_job = CusData_Job($_GET['CusJob'],$_GET['pSelectCusType']);//2011-06-09 nz
	echo $tpl->tbHtml( 'oeditcus.html', 'CUSJOBTYPE' );
	exit();
}

if(isset($_GET['link'])){
	$case = $_GET['link'];
}
//naizan add  สำหรับบันทึกข้อมูล ทะเบียนหมดอายุ จ่ายงวดแรก งวดสุดท้าย ปิดค่างวด ทะเบียนรถ วันทีจดทะเบียน วันที่ทะเบียนหมดอายุ
if(isset($_GET['UP_Sell_data'])){
	$process = $_GET['UP_Sell_data'];
	if($process=="createForm"){
		$field_target = $_GET['field_target'];
		$chassi_no_s = $_GET['chassi_no_s'];
		$olddata			 = $_GET['data'];

		if(isset($_GET['readonly'])){
			$disabled = "disabled='disabled'";
			$style	 = "style='line-height:60px;'";
			$msg		 = "<span style='color:blue'>*คุณไม่ได้รับสิทธิ์ให้แก้ไขข้อมูลในส่วนนี้ครับ</span>";
		}

		if($field_target=="reg_no"){ $label = "เลขทะเบียนรถ";}
		elseif($field_target=="reg_date"){ $label="วันที่จดทะเบียน";}
		elseif($field_target=="reg_exp_date"){	$label="วันที่ทะเบียนหมดอายุ";}
		elseif($field_target=="first_installment") {$label="วันที่จ่ายค่างวด งวดแรก";}
		elseif($field_target=="last_installment") {$label="วันที่จ่ายค่างวด งวดสุดท้าย";}
		elseif($field_target=="end_installment") {$label="วันที่ปิดค่างวด";}
		if($field_target!="reg_no"){
			$txt_name = 'newdate';//ชื่ออินพุต
			$tyear	= 	"(ปี พ.ศ)";
			$datePicker = "<script type='text/javascript'>selectDate('newdate');</script>";//function in java/followcus.js
		}else {
			$txt_name = 'regno';//ชื่ออินพุต
		}
		//แบบฟอร์ม Popup
		$popup_form = "<div style='float:right;height:33px' onclick=\"hideObj('PopupSellUpForm');show('div_edit_button');\" onmouseover=\"this.style.cursor='pointer';\"><img src='img/closed.png' align='top' title='ปิดกล่องข้อความ'></div>";
		$popup_form .= "<div style='line-height:40px;'>$msg</div>";
		$popup_form .= "<div id='display' $style>$label <input type='text' name='$txt_name' id='$txt_name' value='$olddata' style='width:150px' $disabled> <input type='button' value='บันทึก' onclick=\"javascript:if(confirm('กรุณาตรวจดูให้แน่ใจว่า '+document.getElementById('$txt_name').value+' $tyear เป็นข้อมูลที่ถูกต้อง?')==true && document.getElementById('$txt_name').value!=''){browseCheck('display','oeditcus.php?UP_Sell_data=updateData&chassi_no_s=$chassi_no_s&field=$field_target&$txt_name='+document.getElementById('$txt_name').value,'');}else{document.getElementById('$txt_name').focus();}\" $disabled>";
		echo $popup_form;
		echo $datePicker;

	}else if($process=="updateData") {//บันทึกข้อมูลที่รับเข้ามาจากแบบฟอร์ม ป๊อบอัพ
		$field = $_GET['field'];
		$newdate = $_GET['newdate'];
		$chassi_no_s = $_GET['chassi_no_s'];

		if($chassi_no_s){
			
			//-- BACKUP Z@2012-02-14
			$backup = array(
				'table' => " $config[db_easysale].Sell ",
				'where' => " Chassi_No_S = '$chassi_no_s' ",
	 			'line' => __LINE__,
	 			'remark' => 'แก้ไขประวัติขาย เลขทะเบียน วันที่ต่างๆ',
	 			'action_type' => '1',
	 		);
			insert_backup_from_sql($backup, "$config[db_easysale].ZLogHistory");
			//-- BACKUP 				
			
			if($field != "reg_no"){	//จะเป็นข้อมูลวันที่

				$date = thai2standard($newdate);//function/general.php

				if($field=='end_installment'){
					mysql_query("UPDATE $config[db_easysale].Sell SET first_offer_date=DATE_ADD(DATE_FORMAT(STR_TO_DATE(Date_Deliver,'%d-%m-%Y'),'%Y-%m-%d'),INTERVAL 3 YEAR) WHERE '$date' <= DATE_ADD(DATE_FORMAT(STR_TO_DATE(Date_Deliver,'%d-%m-%Y'),'%Y-%m-%d'),INTERVAL 3 YEAR) AND Chassi_No_S = '$chassi_no_s' ");
				}else if($field=='first_installment'){
					$time_result 	= mysql_query("SELECT Time_S FROM $config[db_easysale].Sell WHERE Chassi_No_S = '$chassi_no_s' ");
					$time_s 			= mysql_fetch_assoc($time_result);
					$time_s			= $time_s['Time_S']-1;
					if($time_s=="" || $time_s < 0){$time_s='0';}
					$last_installment  = afterDate($date,$time_s,'month');//function/general.php
					$other	= ",last_installment = '$last_installment'";
					$lastFunction = "document.getElementById('last_installment".$chassi_no_s."').innerHTML = '".$newdate."';";
				}
				$data = $date;//ถ้าไม่ใช่เลขทะเบียน ให้รับค่าเป็นวันที่
			}else if($field == "reg_no"){
				$data = $_GET['regno'];
			}

			$sql = "UPDATE $config[db_easysale].Sell SET $field = '$data'".$other." WHERE Chassi_No_S = '$chassi_no_s' ";

			mysql_query("SET NAMES UTF8");
			$result = mysql_query($sql) or die( mysql_error());
			if($result){
				echo "<h3 style='color:green'>บันทึกข้อมูลเรียบร้อย</h3>";
				echo "<script type='text/javascript'>
						show('div_edit_button');
						document.getElementById('".$field.$chassi_no_s."').innerHTML = '".$data."';
						$lastFunction
						setTimeout(\"hideObj('PopupSellUpForm')\",1000);
					</script>";//function in java/followcus.js
			}
		}else {
			echo "<h3 style='color:red'>ไม่มีหมายเลข Chassi ที่ต้องการอัพเดต.</h3>";
		}
	}
	exit();
}
// Piag Check Id Card

else if(isset($_POST[pCheckIdCard])){

	if($_POST[pIdCard]!=""){
		$strSQL = "SELECT Cus_IDNo,Cus_Name,Cus_Surename FROM $config[Cus].MainCusData ";
		$strSQL .=  " WHERE Cus_IDNo = '$_POST[pIdCard]' ";
		mysql_query( "SET NAMES utf8" ) or die (mysql_error());
		$result= mysql_query( $strSQL , $conn);
		$Num_Rows = mysql_num_rows( $result );
		$cusDetail=mysql_fetch_assoc($result);
		if($Num_Rows){
			echo "<script type='text/javascript'>
				alert('รหัสบัตรประชาชน $_POST[pIdCard] นี้ เป็นของคุณ $cusDetail[Cus_Name] $cusDetail[Cus_Surename]  \\n กรุณาตรวจสอบรหัสบัตรประชาชนอีกครั้ง หรือ ใช้ข้อมูลที่มีอยู่แล้ว	');
				document.addForm2.Cus_IDNo.select();
				</script>";
		}
	}
	exit();
}
// Piag End Check Id Card

//end naizan add

if(isset($_GET['relateCus'])){
	echo $househistory = addrelateCus($_GET);
	exit();
}elseif(isset($_GET['relateEmp'])){
	echo $househistory2 = addrelateEmp($_GET);
	exit();
}elseif(isset($_GET['QListSell'])){

}elseif(isset($_POST['Save'])){

	$saves=SaveFILE($_POST);
	/*########### golf edit 10-11-53 ########*/
	switch($_POST['page_name']){
		case 'add_reginfo':$link_val = "index.php?BackPage=reg_car.php?cus_number=".$saves.$_POST['gggg']; break;
		default: $link_val = "index.php?BackPage=oeditcus.php?Edit=".$saves."&t=".time(); break;
	}
	//$link_val = "index.php?BackPage=oeditcus.php?Edit=".$saves."&t=".time();
	$lang['operation_saved']="ระบบจัดเก็บเรียบร้อยกรุณารอสักครู่ ....";
	$link = $link_val;
	echo $content .= $tpl->tbHtml('messages.html', 'SAVED');
}elseif(isset($_GET['close'])){
	echo '<body onLoad="javascript:window.close();" onclick="javascript:window.close();">คลิกที่นี้เพื่อปิดหน้าต่าง';
}elseif(isset($_GET['getCus2'])){
	$Relation = Relation();
	$strSQL = "SELECT * FROM $config[Member].emp_data ";
	$strSQL .=  "WHERE DataNo = '".$_GET['getCus2']."'  ";
	mysql_query( "SET NAMES utf8" ) or die (mysql_error());
	$result= mysql_query( $strSQL , $conn);
	$Num_Rows = mysql_num_rows( $result );
	$rs = mysql_fetch_array( $result );
	$emp_info = $rs[DataNo]."_".$rs[Name]."_".$rs[Surname]."_".$rs[Nickname];
	if($rs['Nickname'] != ""){
		$rs['Nickname'] = " (".$rs['Nickname'].")";
	}
	if(isset($_GET['shoe']) && $_GET['shoe'] == "new" ){
		$hiddenValue = "class=\"hidden\"";
	}
	echo $tpl->tbHtml('oeditcus.html', 'CUSLIST2' );
}elseif(isset($_GET['getCus']) ){
	$Relation = Relation();
	$strSQL = "SELECT * FROM $config[Cus].MainCusData ";
	$strSQL .=  " WHERE CusNo = '$_GET[getCus]' ";
	mysql_query( "SET NAMES utf8" ) or die (mysql_error());
	$result= mysql_query( $strSQL , $conn);
	$Num_Rows = mysql_num_rows( $result );
	$rs = mysql_fetch_array( $result );
	$cus_info = $rs[CusNo]."_".$rs[Cus_Name]."_".$rs[Cus_Surename]."_".$rs[Cus_Nickname];
	if($rs['Cus_Nickname'] != ""){
		$rs['Cus_Nickname'] = " (".$rs['Cus_Nickname'].")";
	}
	if(isset($_GET['shoe']) && $_GET['shoe'] == "new" ){
		$hiddenValue = "class=\"hidden\"";
	}
	echo $tpl->tbHtml( 'oeditcus.html', 'CUSLIST' );
}elseif(isset($_GET['searchs']) and $_GET['searchs']=='mary_id'){
	$dr=null;
	$strSQL = "SELECT * FROM $config[Cus].MainCusData ";
	$strSQL .=  " WHERE Cus_Name LIKE '%".trim($_GET['search'])."%' OR Cus_Surename LIKE '%".trim($_GET['search'])."%'  OR CusNo LIKE '%".$_GET
	['search']."%' ";

	mysql_query( "SET NAMES utf8" ) or die (mysql_error());
	$result= mysql_query( $strSQL , $conn);
	$Num_Rows = mysql_num_rows( $result );

	if($Num_Rows){
		echo '<font size="2" color="#660000">ค้นหาคำเหมือน [ <i><u>';
		echo ($_GET['search']);
		echo	'</u></i> ] กรุณาเลือกตามรายการด้านล่าง</font><br>';
	}else{
		echo "ไม่มีตามที่ท่านต้องการค้นหา";
	}
	while($rs = mysql_fetch_array( $result )){
		echo ($rs['CusNo'].'_'.$rs['Be'].' '.$rs['Cus_Name'].' '.$rs['Cus_Surename']."  ".$rs['Cus_Nickname']);
		if($dr!=($Num_Rows-1)){
			echo "<br>";
			$dr++;
		}
	}
}elseif(isset($_GET['q'])){
	$dr=null;
	$strSQL = "SELECT * FROM $config[Member].emp_data ";
	$strSQL .=  " WHERE Name LIKE '%".$_GET['q']."%' ORDER BY DataNo DESC LIMIT 30 ";
	mysql_query( "SET NAMES utf8" ) or die (mysql_error());
	$result= mysql_query( $strSQL , $conn);
	$Num_Rows = mysql_num_rows( $result );
	if($Num_Rows){
		echo 'ค้นหาคำเหมือน';
		echo ($_GET['search']);
		echo	"กรุณาเลือกตามรายการด้านล่าง\n";
	}else{
		echo "ไม่มีตามที่ท่านต้องการค้นหา\n";
	}
	while($rs = mysql_fetch_array( $result )){
				echo ($rs['DataNo'].'_'.$rs['Be'].'_'.$rs['Name'].'_'.$rs['Surname']."(".$rs['Nickname'].")\n");
	}

}elseif(isset($_GET['searchs']) and $_GET['searchs']=='member_id'){
	$dr=null;
	$strSQL = "SELECT * FROM $config[Member].emp_data ";
	$strSQL .=  " WHERE Name LIKE '%".trim($_GET['search'])."%' ORDER BY DataNo DESC LIMIT 30 ";
	mysql_query( "SET NAMES utf8" ) or die (mysql_error());
	$result= mysql_query( $strSQL , $conn);
	$Num_Rows = mysql_num_rows( $result );
	if($Num_Rows){
		echo '<font size="2" color="#660000">ค้นหาคำเหมือน [ <i><u>';
		echo ($_GET['search']);
		echo	'</u></i> ] กรุณาเลือกตามรายการด้านล่าง</font><br>';
	}else{
		echo "ไม่มีตามที่ท่านต้องการค้นหา";
	}
	while($rs = mysql_fetch_array( $result )){
		echo ($rs['DataNo'].'_'.$rs['Be'].' '.$rs['Name'].' '.$rs['Surname']."  ".$rs['Nickname']);
		if($dr!=($Num_Rows-1)){
			echo "<br>";
			$dr++;
		}
	}
}elseif(isset($_GET['searchs']) and $_GET['searchs']=='from_who'){
	echo $_GET['search'];
	$dr=null;
	$strSQL = "SELECT * FROM $config[Member].emp_data ";
	$strSQL .=  " WHERE Name LIKE '%".trim($_GET['search'])."%' ORDER BY DataNo DESC ";
	mysql_query( "SET NAMES utf8" ) or die (mysql_error());
	$result= mysql_query( $strSQL , $conn);
	$Num_Rows = mysql_num_rows( $result );

	if($Num_Rows){
		echo '<font size="2" color="#660000">ค้นหาคำเหมือน [ <i><u>';
		echo ($_GET['search']);
		echo	'</u></i> ] กรุณาเลือกตามรายการด้านล่าง</font><br>';
	}
	while(	$rs = mysql_fetch_array( $result )){
		echo ($rs['DataNo'].'_'.$rs['Name'].' '.$rs['Surname'].' ('.$rs['Nickname'].")");
		if($dr!=($Num_Rows-1)){
			echo "<br>";
			$dr++;
		}
	}
}elseif(isset($_GET['searchs']) and $_GET['searchs']=='cus_recommend'){
	echo $_GET['search'];
	$dr=null;
	$strSQL = "SELECT * FROM $config[Cus].MainCusData ";
	$strSQL .=  " WHERE Cus_Name LIKE '%".trim($_GET['search'])."%'  ";
	mysql_query( "SET NAMES utf8" ) or die (mysql_error());
	$result= mysql_query( $strSQL , $conn);
	$Num_Rows = mysql_num_rows( $result );
	if($Num_Rows){
		echo '<font size="2" color="#660000">ค้นหาคำเหมือน [ <i><u>';
		echo ($_GET['search']);
		echo	'</u></i> ] กรุณาเลือกตามรายการด้านล่าง</font><br>';
	}
	while(	$rs = mysql_fetch_array( $result )){
		echo ($rs['CusNo'].'_'.$rs['Cus_Name'].' '.$rs['Cus_Surename'].' ('.$rs['	Cus_Nickname'].")");
		if($dr!=($Num_Rows-1)){
			echo "<br>";
			$dr++;
		}
	}

}elseif(isset($_GET['SGEO'])){
	$Provinces=GETProvince('',$_GET['SGEO']);
	echo $tpl->tbHtml( 'thailand.htm', 'SGEO' );
	exit;
}elseif(isset($_GET['Postcode'])){
	echo PostCode($_GET['Province'],$_GET['Amphur'],$_GET['SubDistrictsy']);
	exit;
}elseif(isset($_GET['SubDistrictsy'])){
	$SubDistrictsy =GETDistrict($_GET['Province'],$_GET['Amphur'],$_GET['SubDistrictsy']);
	echo $tpl->tbHtml( 'thailand.htm', 'SubDistrict' );
	exit;
}elseif(isset($_GET['Amphur'])){
	$Amphur =GETAmphur($_GET['Province'],$_GET['Amphur']);
	echo $tpl->tbHtml( 'thailand.htm', 'Amphurs' );
	exit;
}elseif(isset($_GET['SGEO1'])){
	$Provinces1=GETProvince('',$_GET['SGEO1']);
	echo $tpl->tbHtml( 'thailand.htm', 'SGEO1' );
	exit;
}elseif(isset($_GET['Postcode1'])){
	echo PostCode($_GET['Province1'],$_GET['Amphur1'],$_GET['SubDistrictsy1'],'1');
	exit;
}elseif(isset($_GET['SubDistrictsy1'])){
	$SubDistrictsy1 = GETDistrict($_GET['Province1'],$_GET['Amphur1'],$_GET['SubDistrictsy1']);
	echo $tpl->tbHtml( 'thailand.htm', 'SubDistrict1' );
	exit;
}elseif(isset($_GET['Amphur1'])){
	$Amphur1 =GETAmphur($_GET['Province1'],$_GET['Amphur1']);
	echo $tpl->tbHtml( 'thailand.htm', 'Amphurs1' );
	exit;
}elseif(isset($_GET['SGEO2'])){
	$Provinces2=GETProvince('',$_GET['SGEO2']);
	echo $tpl->tbHtml( 'thailand.htm', 'SGEO2' );
	exit;
}elseif(isset($_GET['Postcode2'])){
	echo PostCode($_GET['Province2'],$_GET['Amphur2'],$_GET['SubDistrictsy2'],'2');
	exit;
}elseif(isset($_GET['SubDistrictsy2'])){
	$SubDistrictsy2 =GETDistrict($_GET['Province2'],$_GET['Amphur2'],$_GET['SubDistrictsy2']);
	echo $tpl->tbHtml( 'thailand.htm', 'SubDistrict2' );
	exit;
}elseif(isset($_GET['Amphur2'])){
	$Amphur2 =GETAmphur($_GET['Province2'],$_GET['Amphur2']);
	echo $tpl->tbHtml( 'thailand.htm', 'Amphurs2' );
	exit;
}else{
	//echo $_GET['Edit']=20100512001;
	$hidden = null;
	if(isset($_GET['Edit'])){
		//แก้ไขข้อมูลลูกค้า
		//if($CheckLevel >=$config['la_level'] && $CheckDepartment != $config['Department'] || isset($_GET['readonly'])){
		if($CheckLevel >=$config['la_level'] && $inDepartment == false  || isset($_GET['readonly'])){
			$hidden='hidden';
			$button='hidden';
			$submit='submit';
			//naizan edit >> ปิดวันเกิด และรหัสประชาชน
			$noedit = "disabled";
			//naizan edit >> ให้ sale submit ได้
			//if(isset($_GET['readonly'])){ $submit='hidden';}
		}else{
			//if($CheckLevel <= $config['la_level'] || $CheckDepartment == $config['Department']  ){
			if($CheckLevel <= $config['la_level'] || $inDepartment == true  ){
				$hidden='text';
				$button='button';
				$submit='submit';
			}else{
				$hidden='hidden';
				$button='hidden';
				$submit='submit';
				$noedit = "disabled"; //naizan edit ปิดวันเกิด และรหัสประชาชน
				if(isset($_GET['readonly'])){ $submit='hidden'; }
			}
		}
		$readonly=$_GET['readonly'];
		$cus_number = $_GET['Edit'];
		$title = 'รายละเอียดข้อมูลลูกค้าส่วนบุคคล';
		$warning = '* ท่านสามารถแก้ไขข้อมูลจากส่วนนี้ได้ โดยพิมพ์ข้อมูลใหม่แทนที่และทำการกดบันทึก';
		$val = 'บันทึก';
		$save = 'Edit';
		$title_style = "class=\"marginLeft\" ";
		$hiddentext = "hidden";
		$image = 'CusPicture.php?id='.$_GET['Edit'].'&time='.time();
		$cus_num_variable = $tpl->tbHtml('oeditcus.html', 'CUS_NUMBER' );  # Define customer number show when edit customer data

		###### golf  14-12-53 เพิ่มแก้ editcus ########
		switch($_POST['page_name']){
			case 'add_reginfo':$link_val = "index.php?BackPage=reg_car.php?cus_number=".$saves.$_POST['gggg']; break;
			default: $link_val = "index.php?BackPage=oeditcus.php?Edit=".$saves."&t=".time(); break;
		}
		####################
	}else{
		//บันทึกข้อมูลลูกค้าใหม่
			//if($CheckLevel >=$config['la_level'] && $CheckDepartment != $config['Department'] || isset($_GET['readonly'])){
			if($CheckLevel >=$config['la_level'] && $inDepartment == false || isset($_GET['readonly'])){
				$hidden='hidden';
				$button='hidden';
				$submit='submit';
				$noedit = "disabled"; //naizan edit ปิดวันเกิด และรหัสประชาชน
				if(isset($_GET['readonly'])){ $submit='hidden'; }
			}else{
				//if($CheckLevel <= $config['la_level'] || $CheckDepartment == $config['Department']){
				if($CheckLevel <= $config['la_level'] || $inDepartment == true){
					$hidden='text';
					$button='button';
					$submit='submit';
				}else{
					$hidden='hidden';
					$button='hidden';
					$submit='submit';
					$noedit = "disabled";//naizan edit ปิดวันเกิด และรหัสประชาชน
					if(isset($_GET['readonly'])){ $submit='hidden'; }
				}
			}
			$readonly=$_GET['readonly'];
			$atab = "3";													# Define tab number <sub tab -- new interest>
			$title = 'เพิ่มข้อมูลลูกค้าใหม่';
			$image='temp/avatar-man.gif';
			$val = 'บันทึก';
			$save='New';
			$title_style = "class=\"marginTop marginLeft\" ";
			$hiddentext = "";
			/* $style_hidden = "style=\"display:inline;\""; */
	}

	$qstring = "SELECT * FROM $config[Cus].MainCusData where CusNo='$_GET[Edit]'  ";     # execute when edit
	mysql_query( "SET NAMES UTF8" );
	$query = mysql_query($qstring) ;
	$Num_Rows = mysql_num_rows( $query );
	$recdd = mysql_fetch_array($query);

	$selectBe = getFirstName($recdd['Be']);
	if($recdd['Be'] != "นาย" AND $recdd['Be'] != "นาง" AND $recdd['Be']  != "นางสาว"){ $otherBe = "style=\"display:inline;\""; }

	if($recdd['Be'] == "นาย" ){
		$select = "checked=\"checked\"";
	}elseif($recdd['Be'] == "นาง" ){
		$select1 = "checked=\"checked\"";
	}elseif($recdd['Be'] == "นางสาว" ){
		$select2 = "checked=\"checked\"";
	}else{
		$select3 = "checked=\"checked\"";
	}
	//echo InterestPersonel($_GET['Edit']);

	if($recdd['Cus_Coppy'] == "WorkOk" ){
		$copyAddress21 = "checked=\"checked\"";
	}elseif($recdd['Cus_Coppy'] == "HomeOk" ){
		$copyAddress22 = "checked=\"checked\"";
	}else
		$copyAddress20 = "checked=\"checked\"";
		if($recdd['W_Coppy'] == "letterOk" ){
		$copyAddress11 = "checked=\"checked\"";
	}else if($recdd['W_Coppy'] == "HomeOk" ){
		$copyAddress12 = "checked=\"checked\"";
	}else
		$copyAddress10 = "checked=\"checked\"";
	if($recdd['C_copy'] == "letterOk" ){
		$copyAddress01 = "checked=\"checked\"";
	}else if($recdd['C_copy'] == "WorkOk" ){
		$copyAddress02 = "checked=\"checked\"";
	}else
		$copyAddress00 = "checked=\"checked\"";


	$date = $intr['Int_Date'];
	$checked = null;
	$checked1 = null;
	$checked2 = null;
	$checked3 = null;
	$checked4 = null;
	$checked5 = null;
	$checked6 = null;
	$checked7 = null;
	$checked8 = null;
	$checked9 = null;
	$checked10 = null;
	$checked11 = null;
	$checked12 = null;
	$checked13 = null;

	$style_hiddenValue = null;
	if($recdd['C_Amp'] == ""){ $style_hiddenValue = "style=\"display:inline;\"";}
	else{$style_hiddenValue = "style=\"display:none;\"";}
	if($recdd['C_Tum'] == ""){$style_hiddenValue = "style=\"display:inline;\"";
	}else{$style_hiddenValue = "style=\"display:none;\"";}
	$hiddentext = null;
	if($recdd['Cus_Amp'] == ""){$hiddentext = ""; }
	else{$hiddentext = "hidden";}
	if($recdd['Cus_Tum'] == ""){$hiddentext = ""; }
	else{$$hiddentext = "hidden"; }
	$style_hiddenW = null;
	if($recdd['W_Amp'] == ""){$style_hiddenW = "style=\"display:inline;\"";
	}else{$$style_hiddenW = "style=\"display:none;\"";}
	if($recdd['W_Tum'] == ""){$style_hiddenW = "style=\"display:inline;\"";
	}else{$style_hiddenW = "style=\"display:none;\"";}

	if($recdd['Sex']== 'ชาย') $checked = 'checked';
	else if($recdd['Sex']== 'ผู้หญิง') $checked1 = 'checked';
	else if($recdd['Sex']== 'สาวประเภทสอง') $checked2 = 'checked';
	//else $checked = 'checked';  # -- edit sex default

	if($recdd['Status']== 'โสด') $checked3 = 'checked';
	else if($recdd['Status']== 'หม้าย') $checked4 = 'checked';
	else if($recdd['Status']== 'อย่าร้าง') $checked5 = 'checked';
	else if($recdd['Status']== 'สมรส') $checked6 = 'checked';
//	else $checked3 = 'checked'; naizan 2011-02-11

	if($recdd['Cus_Type']== 'Low') $checked7 = 'checked';
	else if($recdd['Cus_Type']== 'Normal') $checked8 = 'checked';
	else if($recdd['Cus_Type']== 'VIP.C') $checked9 = 'checked';
	else if($recdd['Cus_Type']== 'VIP.B') $checked10 = 'checked';
	else if($recdd['Cus_Type']== 'VIP.A') $checked11 = 'checked';
	else $checked8 = 'checked';

	if($recdd['NotMail']== '1') $checked12 = 'checked';
	else $checked13 = 'checked';

	if($recdd['Cus_Add']){
		$style='display: inline;';
		$Outsite ='checked';  				# Define for more The shipping documents <checked>
	}else{
		$style='display: none;';
	}

	if($recdd['Date_Receive']){
		$aExp = explode( '-', $recdd['Date_Receive']  );
		$dday=$aExp[2];
		$dmonth=$aExp[1];
		$dyear=$aExp[0];
	}else{
		$dday=date('d');
		$dmonth=date('m');
		$dyear=date('Y');
	}

	if($recdd['DateOfBirth']){
		$aExp = explode( '-', $recdd['DateOfBirth']  );
		$ddayBirth=$aExp[2];
		$dmonthBirth=$aExp[1];
		$dyearBirth=$aExp[0];
	}
	$action="oeditcus.php";

	if(isset($_GET['Edit'])){
		$househistory = househistory($_GET['Edit']);
		$househistory2 = househistory2($_GET['Edit']);
	}
	//$cus_job = CusData_Job($recdd['Job'],'');
	$cus_job = CusData_Job($recdd['Job'],$recdd['BeType']);//2011-06-09

	$myCalendar = new tc_calendar("start_date",1);
	$myCalendar->setIcon("images/iconCalendar.gif");
	$myCalendar->setDate($dday,$dmonth,$dyear);
	$myCalendar->startMonday();
	$start_date = 	$myCalendar->writeScript();

	$myCalendar = new tc_calendar("DateOfBirth",1);
	$myCalendar->setIcon("images/iconCalendar.gif");
	$myCalendar->setDate($ddayBirth,$dmonthBirth,$dyearBirth);
	$myCalendar->startMonday();

	if($recdd['DateOfBirth'] != ""){
		$DateOfBirth = explode("-",$recdd['DateOfBirth']);
		$recdd['DateOfBirth'] = $DateOfBirth[2]."-".$DateOfBirth[1]."-".$DateOfBirth[0];
	}
	//$cus_birth = "<input id=\"DateOfBirth\" type=\"text\" name=\"DateOfBirth\" style=\"width:140px;\" value=\"$recdd[DateOfBirth]\">";
	$cus_birth = "<input id=\"DateOfBirth\" type=\"text\" name=\"DateOfBirth\" style=\"width:140px;\" value=\"$recdd[DateOfBirth]\" $noedit>"; //naizan edit

	#-- naizan 2011-02-08
	if($recdd['BeType']== '1'){
		$BeType1 = 'checked="checked"';
	}else if($recdd['BeType']== '2'){
		$BeType2 = 'checked="checked"';
	}

	$rCus_GEO = explode( '_', $recdd['Cus_GEO']  );
	$recdd['Cus_GEO']=$rCus_GEO[0];
	$rCus_GEO = explode( '_', $recdd['Cus_Pro']  );
	$recdd['Cus_Pro']=$rCus_GEO[0];
	$rCus_GEO = explode( '_', $recdd['Cus_Amp']  );
	$recdd['Cus_Amp']=$rCus_GEO[0];
	$rCus_GEO = explode( '_', $recdd['Cus_Tum']  );
	$recdd['Cus_Tum']=$rCus_GEO[0];

	$rCus_GEO = explode( '_', $recdd['C_GEO']  );
	$recdd['C_GEO']=$rCus_GEO[0];
	$rCus_GEO = explode( '_', $recdd['C_Pro']  );
	$recdd['C_Pro']=$rCus_GEO[0];
	$rCus_GEO = explode( '_', $recdd['C_Pro']  );
	$recdd['C_Pro']=$rCus_GEO[0];
	$rCus_GEO = explode( '_', $recdd['C_Tum']  );
	$recdd['C_Tum']=$rCus_GEO[0];

	$rCus_GEO = explode( '_', $recdd['W_GEO'] );
	$recdd['W_GEO']=$rCus_GEO[0];
	$rCus_GEO = explode( '_', $recdd['W_Pro'] );
	$recdd['W_Pro']=$rCus_GEO[0];
	$rCus_GEO = explode( '_', $recdd['W_Amp'] );
	$recdd['W_Amp']=$rCus_GEO[0];
	$rCus_GEO = explode( '_', $recdd['W_Tum'] );
	$recdd['W_Tum']=$rCus_GEO[0];

	$cus_number = $_GET['Edit'];
	$viewWorkpla='display: none;';
	# --- ถ้าบันทึกลุกค้าใหม่มาจากหน้า จดทะเบียนรถ
	if(empty($recdd['CusNo'])){  if($_GET['page_name']=='add_reginfo'){  $recdd['Cus_Type'] = 'RegData'; } }
	$cus_class = customerClass($recdd['Cus_Type']);
	if(isset($_GET['Edit'])){

		//if($CheckLevel >=$config['la_level'] && $CheckDepartment != $config['Department'] || isset($_GET['readonly'])){
		if($CheckLevel >=$config['la_level'] && $inDepartment == false || isset($_GET['readonly'])){
			if($checked)$sexBox='<input type="hidden" name="Sex" value="ชาย">ชาย';
			else if($checked1)$sexBox='<input type="hidden" name="Sex" value="ผู้หญิง">ผู้หญิง';
			//else if($checked1)$sexBox='<input type="hidden" name="Sex" value="สาวประเภทสอง">สาวประเภทสอง';
			else if($checked2)$sexBox='<input type="hidden" name="Sex" value="สาวประเภทสอง">สาวประเภทสอง'; //naizan edit
			else $sexBox='ไม่ได้ระบุ';

			//if($recdd['Cus_IDNo'])
			//$Cus_IDNoBox='readonly';
			$Cus_IDNoBox='readonly';//naizan edit

			if($checked3)$mary='<input type="hidden" name="mary" value="โสด">โสด';
			else if($checked4)$mary='<input type="hidden" name="mary" value="หม้าย">หม้าย';
			else if($checked5)$mary='<input type="hidden" name="mary" value="อย่าร้าง">อย่าร้าง';
			else if($checked6)$mary='<input type="hidden" name="mary" value="สมรส">สมรส';

			$Data_Received=$recdd['Data_Received'];
			$start_date='<input type="hidden" name="start_date" value="'.$dyear.'-'.$dmonth.'-'.$dday.'"/>'.$dday.'/'.$dmonth.'/'.$dyear;
			if($recdd['DateOfBirth'])
			//$cus_birth='<input type="hidden" name="start_date" value="'.$dyearBirth.'-'.$dmonthBirth.'-'.$ddayBirth.'"/>'.$ddayBirth.'/'.$dmonthBirth.'/'.($dyearBirth);
			$cus_birth='<input type="hidden" name="DateOfBirth" value="'.$ddayBirth.'-'.$dmonthBirth.'-'.$dyearBirth.'"/>'.$ddayBirth.'/'.$dmonthBirth.'/'.($dyearBirth);	//naizan edit

		}else{
			//if($CheckLevel <= $config['la_level'] || $CodeDivision == $config['Department']  ){
			if($CheckLevel <= $config['la_level'] || $inDepartment == true){	//naizan edit
			# -- แก้ไขข้อมูลลูกค้า  user sale admin
			$sexBox='<table width="100%"><tr><td><div align="left"><input type="RADIO" name="Sex" $checked value="ชาย"/>ชาย</div></td><td><div align="left"><input type="RADIO" name="Sex" $checked1 value="ผู้หญิง"/> ผู้หญิง</div></td><td><div align="left"><input type="RADIO" $checked2 name="Sex" value="สาวประเภทสอง"/>สาวประเภทสอง</div></td></tr></table>';
			$mary='<input type="RADIO" name="mary" value="โสด" $checked3 > โสด <br><input type="RADIO" name="mary" value="หม้าย" $checked4 > หม้าย  <br><input type="RADIO" name="mary" value="อย่าร้าง" $checked5 > อย่าร้าง  <br><input type="RADIO" name="mary" value="สมรส" $checked6 >สมรส';
			}else{
//				echo "33333333333333333333333333333";

				if($checked)$sexBox='<input type="hidden" name="Sex" value="ชาย">ชาย';
				else if($checked1)$sexBox='<input type="hidden" name="Sex" value="ผู้หญิง">ผู้หญิง';
				//else if($checked1)$sexBox='<input type="hidden" name="Sex" value="สาวประเภทสอง">สาวประเภทสอง';
				else if($checked2)$sexBox='<input type="hidden" name="Sex" value="สาวประเภทสอง">สาวประเภทสอง'; //naizan edit
				else $sexBox='ไม่ได้ระบุ';
				if($recdd['Cus_IDNo'])
				$Cus_IDNoBox='readonly';

				if($checked3)$mary='<input type="hidden" name="mary" value="โสด">โสด';
				else if($checked4)$mary='<input type="hidden" name="mary" value="หม้าย">หม้าย';
				else if($checked5)$mary='<input type="hidden" name="mary" value="อย่าร้าง">อย่าร้าง';
				else if($checked6)$mary='<input type="hidden" name="mary" value="สมรส">สมรส';

				$Data_Received=$recdd['Data_Received'];
				$start_date='<input type="hidden" name="start_date" value="'.$dyear.'-'.$dmonth.'-'.$dday.'"/>'.$dday.'/'.$dmonth.'/'.$dyear;
				if($recdd['DateOfBirth'])

				if(isset($_GET['Edit'])){ $type_val = "text";}else{ $type_val = "hidden";}
				if($recdd['DateOfBirth']){ $DateValue = $ddayBirth.'-'.$dmonthBirth.'-'.$dyearBirth;}
				$cus_birth='<input type="'.$type_val.'" id="DateOfBirth" name="DateOfBirth" value="'.$DateValue.'" style="width:143px;"/>';
			}
		}

		if($recdd['Cus_Pro'] and $recdd['Cus_Amp'] and $recdd['Cus_Tum'] ){
			$SubDistrictsy2 = GETDistrict($recdd['Cus_Pro'],$recdd['Cus_Amp'],$recdd['Cus_Tum']);
			$SubDistricts2= $tpl->tbHtml( 'thailand.htm', 'SubDistrict2' );
			$Amphur2 = GETAmphur($recdd['Cus_Pro'],$recdd['Cus_Amp']);
			$Amphurs2 = $tpl->tbHtml( 'thailand.htm', 'Amphurs2' );
			$viewWorkpla='display: inline;';
			$cck='checked';

		}
		if($recdd['C_Pro'] and $recdd['C_Amp'] and $recdd['C_Tum'] ){
			$SubDistrictsy = GETDistrict($recdd['C_Pro'],$recdd['C_Amp'],$recdd['C_Tum']);
			$SubDistricts= $tpl->tbHtml( 'thailand.htm', 'SubDistrict' );
			$Amphur =GETAmphur($recdd['C_Pro'],$recdd['C_Amp']);
			$Amphurs= $tpl->tbHtml( 'thailand.htm', 'Amphurs' );
		}
		if($recdd['W_Pro'] and $recdd['W_Amp'] and $recdd['W_Tum'] ){
			$SubDistrictsy1 = GETDistrict($recdd['W_Pro'],$recdd['W_Amp'],$recdd['W_Tum']);
			$SubDistricts1= $tpl->tbHtml( 'thailand.htm', 'SubDistrict1' );
			$Amphur1 = GETAmphur($recdd['W_Pro'],$recdd['W_Amp']);
			$Amphurs1= $tpl->tbHtml( 'thailand.htm', 'Amphurs1' );

		}

		//ป.การสนใจ
		//ZAN@2011-12-12  เพิ่มแสดงทุกสถานะ

		//$sql .= "SELECT * FROM $config[db_easysale].CusInt LEFT JOIN $config[db_easysale].CusInt_Product ON CusInt.Int_Num = CusInt_Product.Int_Num_Ref
		//WHERE CusInt.Int_CusNo = '$cus_number' and CusInt.Status !='99' AND Grouping1_1 != 'insert_temp' GROUP BY $config[db_easysale].CusInt.Int_Num DESC
		//ORDER BY Int_Num DESC ";
		$sql .= "SELECT * FROM $config[db_easysale].CusInt
		LEFT JOIN $config[db_easysale].CusInt_Product ON CusInt.Int_Num = CusInt_Product.Int_Num_Ref
		WHERE CusInt.Int_CusNo = '$cus_number' AND Grouping1_1 != 'insert_temp'
		GROUP BY $config[db_easysale].CusInt.Int_Num DESC ORDER BY Int_Num DESC ";
		$dfgs=null;
		mysql_query( "SET NAMES UTF8" ) ;
		$query = mysql_query($sql) or die(mysql_error()) ;
		$Num_Rows = @mysql_num_rows($query);
		$InterTotal = $Num_Rows;
		if(!$Num_Rows){ $dfgs = "<tr style=\"border:0px;\"><td colspan=\"13\"><div align=\"center\" class=\"fontBold marginTop marginBottom\"
		style=\"font-size:14px;color:#ff0000;\">ไม่มีข้อมูลประวัติการสนใจ</div></td></tr>";}
		while($intr = @mysql_fetch_array($query)) {
			$style=' background-color:#f3f3f3;';
			$dbClick=null;
			$dates = $intr['Int_Date'];
			list($date, $times) = explode("-", $dates);
			if($intr['Int_Date'] != "" && $intr['Int_Date'] != null){
				$em_arr = explode(" ",$intr['Int_Date']);
				$arr    = explode("-",$em_arr[0]);
				$intr['Int_Date'] = $arr[2]."-".$arr[1]."-".$arr[0];
			}

			$status = null;

			$intr['font_color']='';
			if($intr['Status'] == '1'){		$status = "กำลังติดตาม";$intr['font_color']='black';}
			elseif($intr['Status'] == '2'){	$status = "นำเสนอการติดตาม";}
			elseif($intr['Status'] == '3'){	$status = "อนุมัติและออกติดตาม";}
			elseif($intr['Status'] == '4'){	$status = "ติดตามสำเร็จ";$intr['font_color']='blue';}
			elseif($intr['Status'] == '5'){	$status = "กำลังแต่งรถ";$intr['font_color']='blue';}
			elseif($intr['Status'] == '6'){	$status = "รอสั่งรถใหม่";$intr['font_color']='blue';}
			elseif($intr['Status'] == '99'){$status = "ยกเลิกการจอง";$intr['font_color'] = 'red';}
			elseif($intr['Status'] == 'IC'){$status = "ออกรถแล้ว";$intr['font_color'] = 'green';}
			else{$status = $intr['Status'];$intr['font_color'] = 'red';}

			//$status = $status.'  '.$intr['Status_Remark'];

			$dfgs .= $tpl->tbHtml( 'oeditcus.html', 'EDIT_CUSTOMER_LIST_HIST' );
		}

		$Eight = $tpl->tbHtml('oeditcus.html', 'BOX_ONE_CUSINT');

		//ป.การจอง

		$booking=null;$booking1=null;$booking2=null;$booking3=null;
		//ZAN@2011-12-12	#หากมีชื่อในผู้จองหรือชื่อที่ใช้ออกรถ ให้นำข้อมูลมาแสดงทั้งหมด
		//$SQL = "SELECT * FROM $config[db_easysale].Booking WHERE status != '99' and CusNo = '$cus_number'   order by Booking_No DESC, Booking_Date , status   ";
		$SQL = "SELECT * FROM $config[db_easysale].Booking WHERE '$cus_number' IN (CusNo,B_cusBuy)   order by Booking_No DESC, Booking_Date , status   ";
		mysql_query( "SET NAMES UTF8" ) ;
		$SQLquery = mysql_query($SQL) ;
		$Num_Rowss = mysql_num_rows( $SQLquery );
		if($Num_Rowss){
			$say=0;
			while($rsd = mysql_fetch_array($SQLquery)){
				$say++;$over="#990000";
				if($say%2){$out="#336600";}else{$out="#669900";}
				$MainCusData = getMainCusDataFormCusNo($rsd['CusNo']);
				$rsd['CusName'] = $MainCusData['Cus_Name']." ".$MainCusData['Cus_Surename'];

				$MainCusDataSend = getMainCusDataFormCusNo($rsd['B_cusBuy']);// pt2011-11-22
				$rsd['CusNameSend'] = $MainCusDataSend['Cus_Name']." ".$MainCusDataSend['Cus_Surename'];

				$rsd['CusNoName']  = "หมายเลขลูกค้า (".$MainCusData['CusNo'].") ".$MainCusData['Be']." ".$MainCusData['Cus_Name']." ".$MainCusData['Cus_Surename'];
				$CusInfo  = $MainCusData['CusNo']."_".$MainCusData['Cus_Name']."(".$MainCusData['Cus_Surename'].')';

				$SaleOfferVal= getEmpIDData($rsd['SaleBookingNo'],"ID_card");
				$rsd['SaleName']= $SaleOfferVal['Name']." ".$SaleOfferVal['Surname']." ".$SaleOfferVal['Nickname'];
				# Booking_Date
				if($rsd['Booking_Date']){
					$book_date = explode(" ",$rsd['Booking_Date']);
					$rsd['Booking_Date'] = explode_date($book_date[0]);
				}
				# Est_Delivery
				$Est = explode(" ",$rsd['Est_Delivery']);
				$Delivery = explode("-",$Est[0]);
				$rsd['Est_Delivery'] = $Delivery[2]."-".$Delivery[1]."-".$Delivery[0]." ".$Est[1];

				/* if($rsd['status'] == '0' or !$rsd['Chassi_No_B']   ){
					$bookingtt="booking";
					if($rsd['status']=='0') $rsd['BookingImg']="รอการแต่ง";
					else if($rsd['status']=='1') $rsd['BookingImg']="กำลังแต่งรถ";
					else if($rsd['status']=='2') $rsd['BookingImg']="กำลังแต่งรถ";
					else if($rsd['status']=='9') $rsd['BookingImg']="เรียบร้อย";
					else if($rsd['status']=='99') $rsd['BookingImg']="ถูกยกเลิกเพราะเปลี่ยนรถ";
					$booking .= $tpl->tbHtml('oeditcus.html', 'LIST');
				} */
				$rsd['font_color'] = '';
				if($rsd['status'] == '0' or !($rsd['Chassi_No_B']) ){$bookingtt="booking";}
				if($rsd['status']=='0') $rsd['BookingImg']="รอการแต่ง";
				elseif($rsd['status']=='1') $rsd['BookingImg']="กำลังแต่งรถ";
				elseif($rsd['status']=='2') $rsd['BookingImg']="กำลังแต่งรถ";
				elseif($rsd['status']=='9') $rsd['BookingImg']="เรียบร้อย";
				elseif($rsd['status']=='99'){
					$rsd['BookingImg']="ยกเลิกจอง";
					$rsd['font_color'] = 'color:red';
				}

				//ถ้าเป็น admin / owner
				//javascript : display_car_condition();// --> js_css_autoload/js/oeditcus.js บล็อก HEAD2
				if($CheckManager==$config['Manager'] || $_SESSION['SESSION_username']=='admin'){
				    $rsd['condition_td'] = "<td align='center' style='border-left:1px' title='ข้อมูลการจอง'><img src='temp/magnifier.png' style='cursor:pointer' onclick=\"display_car_condition('$rsd[Booking_No]','edit_booking_contain');\"></td>";
                }
				if($rsd['Remark']){ $rsd['Remark'] = 'หมายเหตุ : '.$rsd['Remark'];}//เพิ่มคำว่าหมายเหตุ

				$booking .= $tpl->tbHtml('oeditcus.html', 'LIST'); # PT 2011-11-22 เพิ่มชื่อผู้ออกรถ
			}//while
			$booking_form =null;

			//ถ้าเป็น admin / owner
			if($CheckManager==$config['Manager'] || $_SESSION['SESSION_username']=='admin'){
               $rsd['condition_th'] = '<th align="center">&nbsp;</th>';
            }

			$nine =  $tpl->tbHtml('oeditcus.html', 'HEAD2'). $booking .$tpl->tbHtml('oeditcus.html', 'FOOT_HEAD2');
		}else{
			$nine = "<div align=\"center\"><b><h1>ไม่มีประวัติ</h1></b></div>";
		}

		$cTemp='';



		//naizan edit [21-10-2010]
		//$SQLSaleFocus = "SELECT * FROM $config[db_easysale].Sell WHERE  CusNo= '$cus_number' order by Sell_No ";
		$SQLSaleFocus  = "SELECT * FROM $config[db_easysale].Sell ";
		$SQLSaleFocus .= "WHERE  IF(SUBSTRING_INDEX(cusBuy, '_','1') !='', SUBSTRING_INDEX(cusBuy, '_','1'),CusNo) = '$cus_number' ";
		$SQLSaleFocus .= "ORDER by Sell_No ";
		mysql_query( "SET NAMES utf8" );
		$SQLSaleQuery= mysql_query( $SQLSaleFocus , $conn);
		$Num_Rows = mysql_num_rows( $SQLSaleQuery );
		while($rs = mysql_fetch_array( $SQLSaleQuery )){
			$ddf++;
			if($ddf %2)
				$rs['bgcolor']='#339900';
			else $rs['bgcolor']='#336600';
				$rs['Price_CRP']=str_replace(',', "",$rs['Price_CRP']);
				$rs['Price_Discount']=str_replace(',', "",$rs['Price_Discount']);
				$rs['Price_Sell']=str_replace(',', "",$rs['Price_Sell']);
				$rs['Sub_UsedCar_S']=str_replace(',', "",$rs['Sub_UsedCar_S']);
				$rs['AccessoryCost_S']=str_replace(',', "",$rs['AccessoryCost_S']);
				$rs['Real_DownPay_S']=str_replace(',', "",$rs['Real_DownPay_S']);
				$rs['RecCost_S']=str_replace(',', "",$rs['RecCost_S']);
				$rs['OthCost_S']=str_replace(',', "",$rs['OthCost_S']);
				$rs['Total_Budget_S']=str_replace(',', "",$rs['Total_Budget_S']);

				$SQLSalef = "SELECT * FROM Main_Vehicle WHERE MV_Chass_No  LIKE '$rs[Chassi_No_S]'  order by id DESC ";
				mysql_query( "SET NAMES utf8" );
				$SQLQuery= mysql_query( $SQLSalef , $conn);
				$rsMain = @mysql_fetch_array( $SQLQuery );
				$RefRec = getMainCusDataFormCusNo($rs['CusNo']);
				$rs[Cusname] = $RefRec['Cus_Name']." ".$RefRec['Cus_Surename']."";

				if($rs['Sale_Sell']){//naizan update [16-10-2010] เปลี่ยน $rs['SaleName'] เป็น $rs['Sale_Sell']
					$update = getEmpIDData($rs['Sale_Sell'],"ID_card");
					$rs['Sale_Sell'] = $update['Name']." ".$update['Surname']."  (".$update['Nickname'].")";
				}

				if($rs['cusBuy'] != ""){ $arr_cus = explode("_",$rs['cusBuy']); $rs['cusBuy'] = $arr_cus[1];}
				$rs['PF']=(( ($rs['Price_CRP'] - $rsMain['Dnet_IncVat']) -$rs['Total_Budget_S'])+$rsMain['CutOff'] );
			if($rs['PFCheckPerson']==''){
				$rs['Checked'] .='<input type="checkbox"  name="PFCheckPerson[]" value="'.$rs[Sell_No].'" > PF<input type="text"  name="PF[]" size=10 value="" maxlength="255">';
			}else{
				$update = getEmpIDData($_SESSION['SESSION_ID_card'],"ID_card");
				$Updater = $update['Name']." ".$update['Surname']."  (".$update['Nickname'].")";
				$rs['Checked']='วันที่ '.date("d/m/",$rs['PFCheckDate']).(date("Y",$rs['PFCheckDate'])).' ';
			}

			if($rs['PF'])
				$rs['PF'] 				= number_format($rs['PF'],2 );

			if($rs['Price_CRP'])
				$rs['Price_CRP']		= number_format($rs['Price_CRP'] ,2);

			if($rs['Price_Discount'])
				$rs['Price_Discount'] 	= number_format($rs['Price_Discount'] ,2);

			if($rs['Price_Sell'])
				$rs['Price_Sell'] 		= number_format($rs['Price_Sell'] ,2);

			if($rs['Sub_UsedCar_S'])
				$rs['Sub_UsedCar_S']  	= number_format($rs['Sub_UsedCar_S'] ,2);

			if($rs['AccessoryCost_S'])
				$rs['AccessoryCost_S'] 	= number_format($rs['AccessoryCost_S'],2 );

			if($rs['Real_DownPay_S'])
				$rs['Real_DownPay_S'] 	= number_format($rs['Real_DownPay_S'] ,2);

			if($rs['RecCost_S'])
				$rs['RecCost_S'] 		= number_format($rs['RecCost_S'],2 );

			if($rs['OthCost_S'])
				$rs['OthCost_S'] 		= number_format($rs['OthCost_S'] ,2);

			if($rs['Total_Budget_S'])
				$rs['Total_Budget_S'] 	= number_format($rs['Total_Budget_S'],2 );

			$rs['Sell_Date']	= standard2thai($rs['Sell_Date']);//naizan edit

			$rs['Model_GName'] = $rsMain['Model_GName'];
			$rs['MV_Model_CodeName'] = $rsMain['MV_Model_CodeName'];
			//naizan add
			if($_GET['readonly']=="readonly"){
				$readonly = "readonly=readonly";
			}



			if($rs['first_installment']=="" || $rs['first_installment']=='0000-00-00'){
				$first_installment = create_popup_edit($rs['Chassi_No_S'],'first_installment','','[เพิ่มข้อมูล]','#ff0000');
			}else {
				$first_installment  = standard2thai($rs['first_installment']);
				$date = $first_installment;
				$text = $first_installment;
				$color = "#0000ff";
				$first_installment = create_popup_edit($rs['Chassi_No_S'],'first_installment',$date,$text,$color);
			}

			if($rs['last_installment']=="" || $rs['last_installment'] == '0000-00-00'){
				$last_installment = create_popup_edit($rs['Chassi_No_S'],'last_installment','','[เพิ่มข้อมูล]','#ff0000');
			}else {
				$last_installment	= standard2thai($rs['last_installment']);
				$date = $last_installment;
				$text = $last_installment;
				$color = "#0000ff";
				$last_installment = create_popup_edit($rs['Chassi_No_S'],'last_installment',$date,$text,$color);
			}

			if($rs['end_installment']=="" || $rs['end_installment']=='0000-00-00'){
				$end_installment = create_popup_edit($rs['Chassi_No_S'],'end_installment','','[เพิ่มข้อมูล]','#ff0000');
			}else {
				$end_installment	= standard2thai($rs['end_installment']);
				$date = $end_installment;
				$text = $end_installment;
				$color = "#0000ff";
				$end_installment = create_popup_edit($rs['Chassi_No_S'],'end_installment',$date,$text,$color);
			}

			if($rs['reg_date']=="" || $rs['reg_date']=='0000-00-00'){
				$reg_date = create_popup_edit($rs['Chassi_No_S'],'reg_date','','[เพิ่มข้อมูล]','#ff0000');
			}else {
				$reg_date = standard2thai($rs['reg_date']);
				$date = $reg_date;
				$text = $reg_date;
				$color = "#0000ff";
				$reg_date = create_popup_edit($rs['Chassi_No_S'],'reg_date',$date,$text,$color);
			}

			if($rs['reg_exp_date']=="" || $rs['reg_exp_date']=='0000-00-00'){
				$reg_exp_date = create_popup_edit($rs['Chassi_No_S'],'reg_exp_date','','[เพิ่มข้อมูล]','#ff0000');
			}else {
				$reg_exp_date = standard2thai($rs['reg_exp_date']);
				$date = $reg_exp_date;
				$text = $reg_exp_date;
				$color = "#0000ff";
				$reg_exp_date = create_popup_edit($rs['Chassi_No_S'],'reg_exp_date',$date,$text,$color);
			}

			if($rs['reg_no']==""){
				$reg_no = create_popup_edit($rs['Chassi_No_S'],'reg_no','','[เพิ่มข้อมูล]','#ff0000');
			}else {
				$reg_no	= $rs['reg_no'];
				$text = $reg_no;
				$color = "#0000ff";
				$reg_no = create_popup_edit($rs['Chassi_No_S'],'reg_no',$reg_no,$text,$color);
			}

			echo "<div id='PopupSellUpForm' class='nz_content CTwhite' style='position:absolute;z-index:1;display:none;width:450px;height:100px;line-height:100px'></div>";
			//end naizan


			//ถ้าเป็น admin / owner
			//javascript : display_car_condition();// --> js_css_autoload/js/oeditcus.js บล็อก HEAD2
			if($CheckManager==$config['Manager'] || $_SESSION['SESSION_username']=='admin'){
				   $rs['condition_td'] = "<td align='center' style='border-left:1px' title='ข้อมูลการจอง'><img src='temp/magnifier.png' style='cursor:pointer' onclick=\"display_car_condition('$rs[S_Booking_No]','show_sell_contain');\"></td>";
            }

            #pt2011-11-22 เพิ่มชื่อจอง
            $MainCusDataSend = getMainCusDataFormCusNo($rs['CusNo']);// pt2011-11-22
			$rs['CusNameBook'] = $MainCusDataSend['Cus_Name']." ".$MainCusDataSend['Cus_Surename'];

			$cTemp .= $tpl->tbHtml( 'oeditcus.html', 'REPORTLIST' );

		}//while

        //ถ้าเป็น admin / owner
		if($CheckManager==$config['Manager'] || $_SESSION['SESSION_username']=='admin'){
             $rs['condition_th'] = '<th align="center">&nbsp;</th>';
        }

		if($cTemp)
			$ten = $tpl->tbHtml( 'oeditcus.html', 'REPORTHEAD' ).$cTemp .$tpl->tbHtml( 'oeditcus.html', 'REPORTFOOT' );
		else $ten = "<div align=\"center\"><b><h1>ไม่มีประวัติ</h1></b></div>";

	############
	}else{
		//if($CheckLevel >=$config['la_level'] && $CheckDepartment != $config['Department'] || isset($_GET['readonly'])){
		if($CheckLevel >=$config['la_level'] && $inDepartment == false || isset($_GET['readonly'])){
			///echo "4444444444444444444444444";

			if($checked)$sexBox='<input type="hidden" name="Sex" value="ชาย">ชาย';
			else if($checked1)$sexBox='<input type="hidden" name="Sex" value="ผู้หญิง">ผู้หญิง';
			//else if($checked1)$sexBox='<input type="hidden" name="Sex" value="สาวประเภทสอง">สาวประเภทสอง';
			else if($checked2)$sexBox='<input type="hidden" name="Sex" value="สาวประเภทสอง">สาวประเภทสอง'; //naizan edit
			else $sexBox='ไม่ได้ระบุ';
			if($recdd['Cus_IDNo'])
			$Cus_IDNoBox='readonly';

			if($checked3)$mary='<input type="hidden" name="mary" value="โสด">โสด';
			else if($checked4)$mary='<input type="hidden" name="mary" value="หม้าย">หม้าย';
			else if($checked5)$mary='<input type="hidden" name="mary" value="อย่าร้าง">อย่าร้าง';
			else if($checked6)$mary='<input type="hidden" name="mary" value="สมรส">สมรส';

			$Data_Received=$recdd['Data_Received'];
			$start_date='<input type="hidden" name="start_date" value="'.$dyear.'-'.$dmonth.'-'.$dday.'"/>'.$dday.'/'.$dmonth.'/'.$dyear;
			//$cus_birth='<input type="hidden" name="start_date" value="'.$dyearBirth.'-'.$dmonthBirth.'-'.$ddayBirth.'"/>'.$ddayBirth.'/'.$dmonthBirth.'/'.($dyearBirth);
			$cus_birth='<input type="hidden" name="DateOfBirth" value="'.$ddayBirth.'-'.$dmonthBirth.'-'.$dyearBirth.'"/>'.$ddayBirth.'/'.$dmonthBirth.'/'.($dyearBirth);//naizan edit

		}else{
			//if($CheckLevel <= $config['la_level'] || $CodeDivision == $config['Department']  ){
			if($CheckLevel <= $config['la_level'] || $inDepartment == true){		 //naizan edit
			$sexBox='<table width="100%"><tr><td><div align="left"><input type="RADIO" name="Sex" $checked value="ชาย"/>ชาย</div></td><td><div align="left"><input type="RADIO" name="Sex" $checked1 value="ผู้หญิง"/> ผู้หญิง</div></td><td><div align="left"><input type="RADIO" $checked2 name="Sex" value="สาวประเภทสอง"/>สาวประเภทสอง</div></td></tr></table>';
			$mary='<input type="RADIO" name="mary" value="โสด" $checked3 > โสด <br><input type="RADIO" name="mary" value="หม้าย" $checked4 > หม้าย  <br><input type="RADIO" name="mary" value="อย่าร้าง" $checked5 > อย่าร้าง  <br><input type="RADIO" name="mary" value="สมรส" $checked6 >สมรส';
			}else{
			/// echo "666666666666666666666666666666";

				if($checked)$sexBox='<input type="hidden" name="Sex" value="ชาย">ชาย';
				else if($checked1)$sexBox='<input type="hidden" name="Sex" value="ผู้หญิง">ผู้หญิง';
				//else if($checked1)$sexBox='<input type="hidden" name="Sex" value="สาวประเภทสอง">สาวประเภทสอง';
				else if($checked2)$sexBox='<input type="hidden" name="Sex" value="สาวประเภทสอง">สาวประเภทสอง'; //naizan edit
				else $sexBox='ไม่ได้ระบุ';
				if($recdd['Cus_IDNo'])
				$Cus_IDNoBox='readonly';

				if($checked3)$mary='<input type="hidden" name="mary" value="โสด">โสด';
				else if($checked4)$mary='<input type="hidden" name="mary" value="หม้าย">หม้าย';
				else if($checked5)$mary='<input type="hidden" name="mary" value="อย่าร้าง">อย่าร้าง';
				else if($checked6)$mary='<input type="hidden" name="mary" value="สมรส">สมรส';

				$Data_Received=$recdd['Data_Received'];
				$start_date='<input type="hidden" name="start_date" value="'.$dyear.'-'.$dmonth.'-'.$dday.'"/>'.$dday.'/'.$dmonth.'/'.$dyear;
				if(!isset($_GET['link']) == "new"){
					//$cus_birth='<input type="hidden" name="start_date" value="'.$dyearBirth.'-'.$dmonthBirth.'-'.$ddayBirth.'"/>'.$ddayBirth.'/'.$dmonthBirth.'/'.($dyearBirth);
					$cus_birth='<input type="hidden" name="DateOfBirth" value="'.$ddayBirth.'-'.$dmonthBirth.'-'.$dyearBirth.'"/>'.$ddayBirth.'/'.$dmonthBirth.'/'.($dyearBirth);//naizan edit
				}else{
					$cus_birth='<input type="text" name="DateOfBirth" id="DateOfBirth" value="" style="width:143px;"/>';
				}
			}
		}
		########
	}

	//naizan edit GEO DEFAUL
	if(!$recdd['Cus_GEO']){$recdd['Cus_GEO']='1';}
	if(!$recdd['C_GEO']){$recdd['C_GEO']='1';}
	if(!$recdd['W_GEO']){$recdd['W_GEO']='1';}
/*
	if(!$recdd['Cus_GEO'])
	$recdd['Cus_GEO']='3';

	if(!$recdd['C_GEO'])
	$recdd['C_GEO']='3';

	if(!$recdd['W_GEO'])
	$recdd['W_GEO']='3';
*/

	//////// ที่อยู่ตามบัตรประชาชน
	$SGEO = GETGEO($recdd['C_GEO']);
	$Provinces=GETProvince($recdd['C_Pro'],$recdd['C_GEO']);
	if($recdd['C_Pro'] && $Amphurs==''){ //2011-03-06
		echo '<script type="text/javascript">document.getElementById("Province").onchange();</script>';
	}

	/////// ที่ทำงาน
	$SGEO1 = GETGEO($recdd['W_GEO']);
	$Provinces1 = GETProvince($recdd['W_Pro'],$recdd['W_GEO']);
	if($recdd['W_Pro'] && $Amphurs1==''){ //2011-03-06
		echo '<script type="text/javascript">document.getElementById("Province1").onchange();</script>';
	}

	//////ที่จัดส่งเอกสาร
	$SGEO2 = GETGEO($recdd['Cus_GEO']);
	$Provinces2 = GETProvince($recdd['Cus_Pro'],$recdd['Cus_GEO']);
	if($recdd['Cus_Pro'] && $Amphurs2==''){ //2011-03-06
		echo '<script type="text/javascript">document.getElementById("Province2").onchange();</script>';
	}

	# other tabs
	if($recdd['Date_Receive'] && $recdd['Date_Receive'] != ""){
		$Date_Receive = explode("-",$recdd['Date_Receive']);
		$recdd['Date_Receive'] = "  วันที่    ".$Date_Receive[2]."-".$Date_Receive[1]."-".$Date_Receive[0];
	}
	if($recdd['UpdateT'] && $recdd['UpdateT'] != ""){
		$UpdateT = explode(" ",$recdd['UpdateT']);
		$UpdateTT = explode("-",$UpdateT[0]);
		$UpdateT[0] = "  วันที่    ".$UpdateTT[2]."-".$UpdateTT[1]."-".$UpdateTT[0];
		$recdd['UpdateT'] = $UpdateT[0]." เวลา ".$UpdateT[1];
	}
	if($recdd['Resperson'] && $recdd['Resperson'] != ""){
		$Resperson_ID_card = $recdd['Resperson'];//naizan 2010-11-19
		$Resperson  = getEmpIDData($recdd['Resperson'],"ID_card");
		$recdd['Resperson'] = $Resperson['DataNo']."_".$Resperson['Name']." ".$Resperson['Surname']." (".
		$Resperson['Nickname'].")";
	}
	//naizan add

	if($recdd['respon_date']!='' && $recdd['respon_date']!='0000-00-00'){
		$res_date 	 = explode("-", $recdd['respon_date']);
		$res_Year 	 = $res_date[0]+543;
		$res_date = $res_date[2]."/".$res_date[1]."/".$res_Year;
	}else {
		//$res_Year	 = date('Y')+543;
		//$res_date = date('d') ."/". date('m') ."/". $res_Year;
	}
	/* 2011-02-08 ดูได้อย่างเดียว
	if($_GET['readonly']=="readonly"){
		$respon_date = "<input type='hidden' id='respon_date' name='respon_date' value='$res_date'>";
		$respon_date .= "<input type='text' id='show_date' name='show_date' value='$res_date' disabled='disabled'>";
	}else{
		$respon_date = "<input type='text' id='respon_date' name='respon_date' value='$res_date'>";
	}
	*/

	$OfBirth = explode("-",date("d-m-Y")); # use rank in datepicker ex. 1900 -2010
	$StrDateOfBirth = $OfBirth[2]-75;
	$StpDateOfBirth = $OfBirth[2];
	//echo $tpl->tbHtml( 'oeditcus.html', 'MANAGE_CUS' ).$vview;             # default main content

	$one = $tpl->tbHtml( 'oeditcus.html', 'ONE' );
	$two = $tpl->tbHtml( 'oeditcus.html', 'TWO' );
	$three = $tpl->tbHtml( 'oeditcus.html', 'THREE');
	$four = $tpl->tbHtml( 'oeditcus.html', 'FOUR' );
	$five = $tpl->tbHtml( 'oeditcus.html', 'FIVE' );
	$six = $tpl->tbHtml( 'oeditcus.html', 'SIX' );
	$seven = $tpl->tbHtml( 'oeditcus.html', 'SEVEN');

	$header = $tpl->tbHtml( 'oeditcus.html', 'MANAGE_CUST_HEADER');
	$footer = $tpl->tbHtml( 'oeditcus.html', 'MANAGE_CUST_FOOTER');
	echo $header.$tpl->tbHtml( 'oeditcus.html', 'MANAGE_CUST' ).$vview.$footer;

}


CloseDB();
?>

