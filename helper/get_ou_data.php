<?php

if(!function_exists('get_code_name')){
	/**
	 * get_code_name
	 * ใช้หา code_name บริษัท แผนก ฝ่าย จาก position_id
	 * @return $code_name['working_company']
	 * @return $code_name['department']
	 * @return $code_name['section']
	 */
	function get_code_name(){
		$code_name['working_company'] 	= get_company_code_name();
		$code_name['department'] 		= get_department_code_name();
		$code_name['section'] 			= get_section_code_name();
		return $code_name;
	}
}

if(!function_exists('isAdmin')){
	/**
	 *  isAdmin() เช็คว่าเข้ารหัสเป็น admin หรือไม่
	 *
	 *  @param ชื่อล็อคอิน(username) defualt = blank
	 *
	 * @return true=เป็นadmin  ,  false=ไม่ใช่admin
	 */
	function isAdmin($username = ''){
		global $user_config;
		$username = ($username == '') ? $_SESSION['SESSION_username'] : $username;
		return in_array( $username , $user_config['AdminUser'] );
	}
}

if(!function_exists('get_company_code_name')){
	/**
	 * get_company_code_name
	 * ใช้หารหัสบริษัท working_company.code_name จาก position_id
	 * @return code_name     จาก working_company
	 */
	function get_company_code_name(){
		global $db;
		$sql_pos = "SELECT working_company.code_name FROM $db[DataBaseOU].position LEFT JOIN $db[DataBaseOU].working_company
	ON position.WorkCompany = working_company.id WHERE position.PositionCode = '$_SESSION[SESSION_Position]' ";
		$res_pos = mysql_query($sql_pos)or die('error on file:'.__FILE__.' Line:'.__LINE__);
		$fet_pos = mysql_fetch_assoc($res_pos);
		return $fet_pos['code_name'];
	}
}

if(!function_exists('get_department_code_name')){
	/**
	 * get_department_code_name
	 * ใช้หารหัสแผนก department.code_name จาก position_id
	 * @return code_name     จาก department
	 */
	function get_department_code_name(){
		global $db;
		$sql_dep = "SELECT department.code_name FROM $db[DataBaseOU].position LEFT JOIN $db[DataBaseOU].department
	ON position.Department = department.id WHERE position.PositionCode = '$_SESSION[SESSION_Position]' ";
		$res_dep = mysql_query($sql_dep)or die('error on file:'.__FILE__.' Line:'.__LINE__);
		$fet_dep = mysql_fetch_assoc($res_dep);
		return $fet_dep['code_name'];
	}
}

if(!function_exists('get_section_code_name')){
	/**
	 * get_section_code_name
	 * ใช้หารหัสฝ่าย section.code_name จาก position_id
	 * @return code_name     จาก section
	 */
	function get_section_code_name(){
		global $db;
		$sql_sec = "SELECT section.code_name FROM $db[DataBaseOU].position LEFT JOIN $db[DataBaseOU].section
	ON position.Section = section.id WHERE position.PositionCode = '$_SESSION[SESSION_Position]' ";
		$res_sec = mysql_query($sql_sec)or die('error on file:'.__FILE__.' Line:'.__LINE__);
		$fet_sec = mysql_fetch_assoc($res_sec);
		return $fet_sec['code_name'];
	}
}

if(!function_exists('check_access_permission'))
{
	/**
	 * ตรวจสอบสิทธิ์การใช้งานในแต่ละหน้าเมนูที่คลิกเข้าไปอีกที โดยเอาฟิงก์ชั่นนี้ไปแปะไว้ หากไม่ส่งพารามิเตอร์มาด้วยจะเป็นการเช็กผู้ใช้ขณะนั้น
	 * @param Number $id_card รหัสบัตรประชาชน ถ้าส่งต้องส่งมาพร้อมกับ $position_code
	 * @param String $position_code รหัสตำแหน่ง หากส่งจะส่งพร้อมกับ $id_card
	 * @return ถ้าเป็นแอดมินจะได้ $conf['skill_write'] เสมอ ถ้าเป็น ผู้ใช้อื่นๆ จะได้ตามสิทธิ์ที่กำหนดไว้ ถ้าไม่กำหนดไว้จะเป็นค่าว่าง ถ้าไม่ได้รับสิทธิ์จะมีข้อความแจ้งเตือน และเปลี่ยนไปหน้าหลัก
	 */
	function check_access_permission($id_card=null,$position_code=null,$debug=null)
	{
		global $db,$conf;
		if(isAdmin()){return $conf['skill_write'];}//END
		if( !empty($conf['check_permission'])){//ถ้าเปิดไว้ และไม่ใช่ ผู้ดูแลระบบ
			if($_GET[$conf['check_main_menu']]){//is main menu ถ้าส่งจากเมนูแถบบนสุดของหน้าเพจ

				$check_permission = $_GET[$conf['check_permission']];
				$check_p = substr($_SERVER['PHP_SELF'],strrpos($_SERVER['PHP_SELF'],'/')+1);
				$check_p = explode('.php',$check_p);
				$check_page = $check_p[0].'.php';
				$menu_id = $_SESSION['SESSION_menu_permission'][$check_page][$check_permission];

				if(substr($conf['part_home'],-1)!='/'){ $conf['part_home'].='/';}
				$url = $conf['ppang'].$conf['part_home']."index.php";
				$page_not_permission = '<html>
						<head>
						<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
						<meta http-equiv="refresh" content="2 URL='.$url.'" />
						</head>
						<body>
							<div style="background:#C0ECCC; line-height:30px; color: #944419; padding:50px;width:40%; margin:auto">
							<center>
							<i><font size="3" color="#0000ff">คุณยังไม่ได้รับสิทธิ์ในการเข้าถึงหน้านี้<br>กรุณารอสักครู่ กำลังย้ายไปยังหน้าหลัก.............</font></i>
							</center>
							</div>
							<script type="text/javascript" >setTimeout("window.location=\''.$url.'\';",2000);</script>
						</body>
						</html>';

				if($menu_id!=''){//if menu's session is define

					$sql_permit = "SELECT skill FROM $db[Member].menu_manage
					WHERE menu_id='$menu_id' AND menu_manage.status != '99' AND ( menu_manage.working_company_id = '$_SESSION[SESSION_Working_Company]'
					OR menu_manage.id_card = '$_SESSION[idcard]' OR menu_manage.position_id = '$_SESSION[SESSION_Position_id]'
					OR menu_manage.section_id = '$_SESSION[SESSION_Section]'
					OR menu_manage.department_id = '$_SESSION[SESSION_Department]' ) ";
					$result = mysql_query($sql_permit) or die(mysql_error().' <br>'.$sql_permit);
					$allowed = mysql_fetch_assoc($result);
					if( !$allowed ){//ถ้าไม่ได้รับอนุญาติ
						echo $page_not_permission;
						exit();
					}else{
						switch($allowed['skill']){
							case $conf['skill_write']: $skill = 'w'; break;
							case $conf['skill_read']: $skill = 'r'; break;
						}
						return $skill;//r/w
					}

				}else{//if this page doesn't have menu session
					if(!$debug){
						echo $page_not_permission;
					}else{
						echo '<h3 style="color:blue"><pre>
						ERROR:: อย่าลืมส่งค่า $_GET[\''.$conf['check_permission'].'\'] เพื่อเอามาเช็กสิทธิ์ด้วยครับ

						หน้าที่เข้าถึงนี้ ใช้เซสชั่นสำหรับอ้างอิงสิทธิ์คือ
						<font color="saddlebrown">$_SESSION[\'SESSION_menu_permission\'][<font color="green">\''.$check_page.'\'</font>][<font color="green">\''.$check_permission.'\'</font>]</font>
						ซึ่งไม่มีในรายการเมนูที่ผู้ใช้งานสามารถเข้าถึงได้</pre>
						</h3>';
					}
					exit();
				}
			}//if main_menu
		}//check open

	}//function
}//if


if(!function_exists('haveFollower')){
	/**
	 * haveFollower
	 * เช็คว่ามีลูกน้องหรือไม่
	 * @return arr=มีลูกน้อง  ,  false=ไม่มีลูกน้อง
	 */
	function haveFollower($positionId,$idCard=null,$workCompany=null,$department=null,$section=null){
		global $db,$conf;
		$arr = array();
		if(!$workCompany){		$workCompany = $conf['working_company'];		}
		if(!$department){		$department = '%';		}
		if(!$section){			$section = '%';		}
		$sql_ = "SELECT DISTINCT(command.Follower), relation_position.id_card
	FROM $db[DataBaseOU].position
	LEFT JOIN $db[DataBaseOU].command ON position.id = command.Follower
	LEFT JOIN $db[DataBaseOU].relation_position ON position.id = relation_position.position_id
	WHERE position.Status != '99' AND command.Status != '99' AND relation_position.status != '99'
	AND position.WorkCompany LIKE '$workCompany' AND position.Department LIKE '$department' AND position.Section LIKE '$section'
	AND command.Leader = '$positionId' ";

		$res_ = mysql_query($sql_)or halt(__FILE__,__LINE__);
		if(mysql_num_rows($res_)){
			while($fet_ = mysql_fetch_assoc($res_)){
				$text = $fet_['Follower'].'_'.$fet_['id_card'];
				if(!in_array($text, $arr)){
					$arr[] .= $text;
				}
			}
			return $arr;
		}else {
			return false;
		}
	}
}
if(!function_exists('haveLeader')){
	/**
	 * haveLeader
	 * เช็คว่ามีหัวหน้าหรือไม่
	 * @return arr=มีหัวหน้า ,  false=ไม่มีหัวหน้า
	 */
	function haveLeader($positionId,$idCard=null,$workCompany=null,$department=null,$section=null){
		global $db,$conf;
		if(!$workCompany){		$workCompany = $conf['working_company'];		}
		if(!$department){		$department = '%';		}
		if(!$section){			$section = '%';		}
		$sql_ = "SELECT DISTINCT(command.Follower), relation_position.id_card
	FROM $db[DataBaseOU].position
	LEFT JOIN $db[DataBaseOU].command ON position.id = command.Follower
	LEFT JOIN $db[DataBaseOU].relation_position ON position.id = relation_position.position_id
	WHERE position.Status != '99' AND command.Status != '99' AND relation_position.status != '99'
	AND position.WorkCompany LIKE '$workCompany' AND position.Department LIKE '$department' AND position.Section LIKE '$section'
	AND command.Leader = '' ";
		return ;
	}
}

if(!function_exists('find_Leader')){
	/**
	 * find_Leader
	 * เช็คว่ามีหัวหน้าหรือไม่
	 * @return true=มีหัวหน้า ,  false=ไม่มีหัวหน้า
	 */
	function find_Leader($posIdAsFollow,$id_card){
		global $db,$conf;
		if(isAdmin()){
			$workingcompany = $conf['working_company'];
		}else{
			$workingcompany = $_SESSION['SESSION_Working_Company'];
		}

		//หาคนที่เป็น หัวหน้า
		$sql_Notfollow = "SELECT DISTINCT(command.Leader), relation_position.id_card
	FROM $db[DataBaseOU].position
	LEFT JOIN $db[DataBaseOU].command ON position.id = command.Leader
	LEFT JOIN $db[DataBaseOU].relation_position ON position.id = relation_position.position_id
	WHERE position.Status != '99' AND command.Status != '99' AND relation_position.status != '99' AND position.WorkCompany LIKE '$workingcompany' AND command.Follower = '$posIdAsFollow'";
		$res_Notfollow = mysql_query($sql_Notfollow)or halt(__FILE__,__LINE__.$sql_Notfollow);

		$sign = $T = '';

		if(mysql_num_rows($res_Notfollow)){//ถ้าเจอว่ายังมีหัวหน้าอยู่ให้เข้าไปหาต่อ
			while($fet_NotFollow = mysql_fetch_assoc($res_Notfollow)){
				$T .= $sign.find_Leader($fet_NotFollow['Leader'],$fet_NotFollow['id_card']);$sign = '|and|';
			}
		}else{
			$T .= $sign.$posIdAsFollow.'_'.$id_card;$sign = '|and|';
		}
		return $T;
	}
}

if(!function_exists('find_topLeader')){
	/**
	 * find_topLeader
	 * เช็คว่ามีหัวหน้าหรือไม่
	 * @return arr=มีหัวหน้า ,  false=ไม่มีหัวหน้า
	 */
	function find_topLeader($posId=null,$id_card=null){
		global $db,$conf;

		$workingcompany = $conf['working_company'];
		$topHead= array();
		#- ดึงข้อมูล พนักงาน ที่อยู่ใน อีซูซุเชียงราย(2002)ออกมา
		$sql_1 = "SELECT position.id, position.PositionCode, position.Name, position.Status,  relation_position.id_card, relation_position.NameUse
		FROM $db[DataBaseOU].position RIGHT JOIN $db[DataBaseOU].relation_position ON position.id = relation_position.position_id
		WHERE position.WorkCompany = '$db[working_company]' AND position.Status != '99' AND relation_position.status != '99'";
		$res_1 = mysql_query($sql_1)or halt(__FILE__,__LINE__);

		while($fet_1 = mysql_fetch_assoc($res_1)){
			#-เช็คว่า$fet_1[id]เป็นหัวหน้าใครอยู่มั๊ย
			$sql_2 = "SELECT command.id FROM $db[DataBaseOU].command WHERE command.Status != '99' AND command.Leader = '$fet_1[id]'";
			$res_2 = mysql_query($sql_2)or halt(__FILE__,__LINE__);
			if(mysql_num_rows($res_2)){//ถ้า$fet_1[id] กำลังเป็นหัวหน้าอยู่

				#- หาหัวหน้าของ$fet_1[id]
				$sql_3 = "SELECT command.id, command.Leader FROM $db[DataBaseOU].command WHERE command.Status != '99' AND command.Follower = '$fet_1[id]'";//ใช้$fet_1[id]
				$res_3 = mysql_query($sql_3)or halt(__FILE__,__LINE__);
				if(mysql_num_rows($res_3)){


					$fet_3 = mysql_fetch_assoc($res_3);
					#- ตอนนี้ $fet_3[Leader] เป็นหัวหน้า $fet_1[id]
					#- หาหัวหน้าของ$fet_3[Leader] ถ้าไม่เจอหัวของ$fet_3[Leader] แสดงว่า$fet_3[Leader]เป็น BigSupervisor ตามหลักโครงสร้างเก่า
					$sql_4 = "SELECT command.id, command.Leader FROM $db[DataBaseOU].command WHERE command.Status != '99' AND command.Follower = '$fet_3[Leader]'";
					$res_4 = mysql_query($sql_4)or halt(__FILE__,__LINE__);
					if(mysql_num_rows($res_4)){
						$fet_4 = mysql_fetch_assoc($res_4);
						#-ถ้ายังเจือกมีหัวหน้าอีกนี่ แสดงว่าจัดทีมลึกมาก งงแล้วนะ
						$deBug .= ",'".$fet_4['Leader']."'";
						$countTTTTT ++;
					}else{//ถ้าไม่เจอหัวหน้าของ$fet_3['Leader'] แสดงว่า $fet_3['Leader'] อยู่ชั้นบนสุดละ แสดงว่าเป็น เป็น Big ขึ้นไป
						$card_3 = getidCardFromPositionId($fet_3['Leader']);
						if(!in_array($fet_3['Leader'].'_'.$card_3, $topHead)){
							$topHead[] .= $fet_3['Leader'].'_'.$card_3;
							$countTTTT ++;
						}
					}

				}else{//ถ้าไม่เจอหัวหน้าก็เก็บข้อมูลนี้ไว้ แสดงว่า$fet_1[id]อยู่ชั้นบนสุด
					if(!in_array($fet_1['id'].'_'.$fet_1['id_card'], $topHead)){
						$topHead[] .= $fet_1['id'].'_'.$fet_1['id_card'];
						$countTTT ++;
					}
				}
				$countTT ++;
			}

		}
		return '<br>พนักงานขายทั้งหมดรวมบริหารขาย='.mysql_num_rows($res_1).'<br>พ.น.ง.ที่มีลูกน้อง='.$countTT .'<br>คัดพนักงานที่ไม่มีหัวหน้าแล้วออกมาได้(รอบ1)='.$countTTT.'<br>คัดพนักงานที่ไม่มีหัวหน้าแล้วออกมาได้(รอบ2)='.$countTTTT.'<br>พ.น.ง.ที่ยังมีหัวหน้าหลังจากคัดแล้ว2รอบ='.$countTTTTT. '<br>Position_id ที่เหลือ='.$deBug .'<br>แป่วววว';
	}
}//

function getidCardFromPositionId($posID){
	global $db;
	$sql = "SELECT relation_position.id_card FROM $db[DataBaseOU].relation_position WHERE relation_position.status != '99' AND relation_position.position_id = '$posID'";
	$res = mysql_query($sql)or halt(__FILE__,__LINE__);
	$fet = mysql_fetch_assoc($res);
	return $fet['id_card'];
}

function getPositionCodeOU($positionID){ //ใช้งาน
	global $db;
	$sql_posCode = "SELECT position.PositionCode FROM $db[DataBaseOU].position WHERE position.id = '$positionID' ";
	$res_posCode = mysql_query($sql_posCode)or halt(__FILE__,__LINE__);
	$fet_posCode = mysql_fetch_assoc($res_posCode);
	return $fet_posCode['PositionCode'];
}

if(!function_exists('get_all_follow_idcard_poscode')){ //ใช้งาน
	/**
	* get_all_follow_idcard_poscode
	* เช็คว่าถ้ามีลูกน้อง จะreturn pos_code_idCard ลูกน้องทุกชั้นลึก รวมกัน
	* @return array( poscode_idCard, poscode_idCard... ) ,  false=ไม่มีลูกน้อง
	*/
	function get_all_follow_idcard_poscode($pos_id){
		if($F1 = haveFollower($pos_id)){
			//pt@2012-02-29
			$arr_posCode_idCard = array();
			foreach($F1 as $F1_val){
				$exp_id1 = explode("_",$F1_val);
				$pos_code1 = getPositionCodeOU($exp_id1[0]);
				$text1 = $pos_code1.'_'.$exp_id1[1];
				if(!in_array($text1, $arr_posCode_idCard)){
					$arr_posCode_idCard[] .= $text1;
					//เช็คต่อว่ามีลูกน้องต่ออีกมั๊ย
					if($F2 = haveFollower($exp_id1[0])){
						foreach($F2 as $F2_val){
							$exp_id2 = explode("_",$F2_val);
							$pos_code2 = getPositionCodeOU($exp_id2[0]);
							$text2 = $pos_code2.'_'.$exp_id2[1];
							if(!in_array($text2, $arr_posCode_idCard)){
								$arr_posCode_idCard[] .= $text2;
							}
						}
					}
				}
			}
			return $arr_posCode_idCard;
		}else{
			return false;
		}
	}
}
?>