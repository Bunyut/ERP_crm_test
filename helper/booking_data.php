<?php
/**
 * สำหรับดึงข้อมูลจากตาราง Booking
 */

/**
 * สำหรับดึงข้อมูลวันแรกที่บันทึกลงการจอง
 * @param String $table ชื่อฐานข้อมูล "$config[DataBaseName].Booking"
 * @param Number $all_booking_no หมายเลขการจอง
 * @return คืนค่าเป็น Array [1111111] => '2011-11-10' โดยมีคีย์เป็นเลขการจอง
 */
function get_booking_input_date($table, $all_booking_no) {
	$all_booking_no = implode("','", $all_booking_no);
	$sql = "SELECT Booking_No, DATE( SUBSTRING_INDEX( `B_Input_Time` , '@#' , -1 )) AS B_Input_Time FROM $table WHERE Booking_No IN ('$all_booking_no') ";
	$result = mysql_query($sql) or die(mysql_errno().' '.$_SERVER['PHP_SELF'].' line '.__LINE__);
	while($rs = mysql_fetch_assoc($result)){
		$arr[$rs['Booking_No']] = $rs['B_Input_Time'];
	}
	return $arr;
}


?>