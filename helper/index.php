<?php
header("Content-type: text/html; charset=utf-8");
$info = <<<TEXT
ฟังก์ชั่นที่สามารถนำกลับไปใช้ใหม่ได้ เช่น
- ฟังก์ชั่นเกี่ยวกับสูตรคำนวณ
- ฟังก์ชั่นตรวจสอบตัวเลข ข้อความ รหัสประชาชน
- ฟังก์ชั่นสร้างไฟล์ สร้าง LOG 
- ฟังก์ชั่น บันทึก LOG ลงฐานข้อมูล 
- อื่นๆ ที่สามารถนำไปใช้ได้อีก

รูปแบบการตั้งชื่อไฟล์
1. หากเป็นฟังก์ชั่นที่เกี่ยวกับฐานข้อมูล อาจจะตั้งชื่อไฟล์ตามชื่อตารางข้อมูลนั้นๆ เพื่อง่ายต่อการค้นหา
2. หากเป็นฟังก์ชั่นที่ไม่เกี่ยวกับฐานข้อมูล หรือฟังก์ชั่นที่ติดต่อกับฐานข้อมูลหลายตัว หรือมีการแต่งข้อมูลก่อนคืนค่า ให้สร้างไฟล์ในชื่อที่บอกถึงหมวดหมู่ของข้อมูล
	เช่น ฟังก์ชั่นเกี่ยวกับพนักงาน, ลูกค้า, รถ, รายงาน, excel ,ไฟล์ ,LOG ,String ,Number 
TEXT;
echo nl2br($info);
?>