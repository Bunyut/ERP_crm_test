<?php
include_once 'lang/th/team_setup.php';//ใช้ include_once เผื่อหน้าไหนไม่ได้ include เข้ามาด้วย

/**
 * คืนค่าข้อมูลแบบแบ่งตามระดับชั้น และนับจำนวน Sub Level ของข้อมูลชุดที่ส่งมา (เป็นข้อมูลอาร์เรย์ทีม)
 * @param Array $array_data เป็นข้อมูลอาร์เรย์แยกตามทีม
 * @param Number $num ใช้นับระดับชั้นจะส่งค่ามาเมื่อมีการเรียกตัวเองจากในฟังก์ชั่น
 * @param Array $sub_data เป็นข้อมูลจัดเรียงใหม่ที่ส่างมาด้วยเมื่อพบว่ามีลูกทีม จะทำการเรียกตัวเองอีกครั้ง
 * @param Array $array_option ข้อมูลอื่นๆสำหรับปรับแต่งการทำงานของฟังก์ชั่น [all_level]เลือกหรือไม่เลือกชั้นสุดท้าย
 */
function split_team_level($array_data, $num=0, $sub_data=null, $array_option=array() )
{
	if(empty($array_data)){return 0;}
	$sub = ($num+1);//default = 1 (จะไม่เปลี่ยนแปลง จนกว่าจะส่งเข้ามาในฟังก์ชั่นอีกที)
	$num_sub = $sub;//default = 1 (จะเปลี่ยนไปตามลูป)
	$data_level = $sub_data;//เซ็ตตัวแปรให้เท่ากับค่าที่ส่งมา
	foreach ($array_data as $data) {
		$leader_id = $data['Leader'];
		if($array_option['all_level']==false){//จะเอาเฉพาะเลเวลที่มีลูกทีม
			if(!empty($data['team'])){
				#STEP : เก็บข้อมูลใน level นี้ (เก็บเฉพาะที่มีลูกน้อง)
				$data_no_team = $data;
				unset($data_no_team['team']);//เอาทีมออกก่อน
				//$data_level[$sub][] = $data_no_team;
				$data_level[$sub][$leader_id][]= $data_no_team;//แยกตามตำแหน่งด้วย

				#STEP : นับซับย่อยลงไปอีก
				$sub_data = split_team_level($data['team'], $sub, $data_level, $array_option);
				$sub_count = $sub_data['level'];
				$data_level = $sub_data['data'];//รวมเพื่อให้คีย์เรียงกัน
			}
		}else if($array_option['all_level']==true){//ถึงไม่มีทีมก็จัดกลุ่มเลเวลสุดท้ายด้วย
			#STEP : เก็บข้อมูลใน level นี้ (ไม่มีลูกน้องก็รวมด้วย จะได้ถึงเลเวลสุดท้าย)
			$data_no_team = $data;
			unset($data_no_team['team']);//เอาทีมออกก่อน
			$data_level[$sub][$leader_id][] = $data_no_team;

			if(!empty($data['team'])){
				#STEP : นับซับย่อยลงไปอีก
				$sub_data = split_team_level($data['team'], $sub, $data_level, $array_option);
				$sub_count = $sub_data['level'];
				$data_level = $sub_data['data'];//รวมเพื่อให้คีย์เรียงกัน
			}
		}

		$num_sub = max($num_sub,$sub_count);//จะได้ค่าที่มากที่สุด
	}
	if($num==0 && $array_option['all_level']===false){//กรณีต้องการทั้งหมด
		$num_sub -= 1;//ลบชั้นสุดท้ายออก เพราะเป็นลูกน้องไม่ใช่หัวหน้า
	}
	return array( 'level'=>$num_sub, 'data'=>$data_level );
}

/**
 * ตัวเลือกสำหรับแบ่งเป้า
 * @param Array $all_data_level ข้อมูลที่จัดกลุ่มระดับชั้นแล้ว
 * @param Array $array สำหรับปรับแต่งฟังก์ชั่น [in_salefocus], [in_team_target]
 */
function gen_select_level( $all_data_level, $array=array() )
{
	global $lang, $db;
	$levelOpt = '';
	$option = '';
	$section = $array['section_data'];//หาชื่อฝ่าย ถ้าไม่ส่งอาร์เรย์ข้อมูลฝ่ายมาด้วยจะไม่เห็นว่าอยู่ฝ่ายไหน

	#STEP : หาข้อมูลตำแหน่งทั้งหมด
	if(empty($array['all_position_data'])){//ถ้าไม่ได้ส่งข้อมูลตำแหน่งทั้งมหดมาด้วย
		$position = array();
		$sql  = "SELECT id,PositionCode,Name,Section FROM $db[DataBaseOU].`position` WHERE status != '99' ";
		$result = mysql_query($sql) or die(show_sql_error($sql,$_SERVER['PHP_SELF']));
		while($rs = mysql_fetch_assoc($result)) {
			$rs['Section'] = $section[$rs['Section']]['name'];
			$position[$rs['id']] = $rs;
		}
		$array['all_position_data'] = $position;//ได้ข้อมูลตำแหน่งทั้งหมด (ด้วยเวลาไม่ถึง 1 วินาที)
	}
	$position_data = $array['all_position_data'];

	#STEP : สร้างรายการให้เลือก
	foreach ($all_data_level as $Level=>$data) {//วนลูป Level
		$option .= '<optgroup class="target_level_group" label="Level '.$Level.'">';
		foreach ($data as $Leader_id=>$all_data_level){//วนลูปหัวหน้าแต่ละคน
			if($Leader_id==''){
				$leader_data = $lang['target_level_top_leader'];
			}else{
				$leader_data = $position_data[$Leader_id]['Name'].' : '.$position_data[$Leader_id]['Section'].' ['.$position_data[$Leader_id]['PositionCode'].']';
			}
			$option .= '<option class="target_level_option" value="'.$Level.'_'.$Leader_id.'">'.$lang['target_level_option_leader'].' '.$leader_data.'</option>';
			foreach ($all_data_level as $all_data){//วนลูปข้อมูลแต่ละตำแหน่ง
				if($array['position']==true){//ตำแหน่ง
					#STEP : หา PositionCode ตำแหน่งที่กำหนดเป้าแล้ว
					$opt_class = '';
					if(isset($array['in_team_target']) && !in_array($all_data['PositionCode'], $array['in_team_target'])){ $opt_class = 'no_target';}
					$option .= '<option disabled="disabled" '.$selected.' value="'.$all_data['position_id'].'_'.$all_data['PositionCode'].'"
					 class="target_level_member '.$opt_class.'"> '.$all_data['PositionCode'].' : '.$all_data['position_name'].' : '.$section[$all_data['Section']]['name'].'</option>';
				}else{//คน
					#STEP : หา id_card ที่กำหนดเป้าแล้ว
					$opt_class = '';
					if(isset($array['in_salefocus']) && !in_array($all_data['id_card'], $array['in_salefocus'])){ $opt_class = 'no_target';}
					$option .= '<option disabled="disabled" '.$selected.' value="'.$all_data['position_id'].'_'.$all_data['PositionCode'].'_'.$all_data['id_card'].'"
					  class="target_level_member '.$opt_class.'">'.$all_data['emp_name'].' ['.$all_data['PositionCode'].']</option>';
				}
			}//foreach 3
		}//foreach 2
		$option .= '</optgroup>';
	}//foreach1
	return $levelOpt.$option;
}


/**
 * สร้างรายการ option ให้กับ select box ให้กับทีมแบบคน
 * @param Array $all_data ข้อมูลทีมทั้งหมดที่มาจากฟังก์ชั่น ....follow_team....
 * @param Number $sub ถ้าเป็นการเรียกจากฟังก์ชั่นเองเมื่อมี ย่อยลิงไปอีก จะมีตัวเลขส่งมาให้ด้วย
 * @param Array $array เป็นตัวเลือกเพิ่มเติม (กรณีต้องการแค่ระดับหัวหน้า) [position] true จะเป็นตำแปน่ง/false เป็นแบบคน ,[leader_only] เอาเฉพาะที่เป็นหัวหน้า
 */
function gen_select_team( $all_data, $sub=0, $array=null )
{
	if(empty($all_data['position_id'])){ //ถ้าส่งมาแบบ หัวหน้าที่อยู่บนสุด ข้อมูลจะเป็นอาร์เรย์ก่อน
		return gen_select_team_array($all_data, $sub, $array);
	}else{//ส่งแบบข้อมูลของทีมตัวเอง ข้อมูลจะ position_id จะมีอยู่
		$option = '';
		$style = "margin-left:".($sub*10)."px";
		if($sub > 0 ){
			$class = 'icons_tree';
		}

		//เลือก
		$selected = '';//ถ้ามีการส่งตัวเช็กมาด้วย อย่างใดอย่างหนึ่ง หรือทั้งสองอย่าง
		if( (isset($array['position_selected']) && $array['position_selected']==$all_data['PositionCode'])
			|| (isset($array['id_card_selected']) && $array['id_card_selected']==$all_data['id_card']) ){
			$selected = 'selected="selected"';
		}

		if($array['position']==true){//ตำแหน่ง
			$section = $array['section_data'];//หาชื่อฝ่าย
			$option .= '<option '.$selected.' value="'.$all_data['position_id'].'_'.$all_data['PositionCode'].'" class="'.$class.'" style="'.$style.'"> '.$all_data['PositionCode'].' : '.$all_data['position_name'].' : '.$section[$all_data['Section']]['name'].'</option>';
		}else{//คน
			$option .= '<option '.$selected.' value="'.$all_data['position_id'].'_'.$all_data['PositionCode'].'_'.$all_data['id_card'].'" class="'.$class.'" style="'.$style.'">'.$all_data['emp_name'].' ['.$all_data['PositionCode'].']</option>';
		}

		if(!empty($all_data['team']))
		{
			$sub++;
			$style = "margin-left:".($sub*10)."px";
			foreach ($all_data['team'] as $data) {
				if(!empty($data['team']))
				{
					$option .= gen_select_team($data, $sub, $array);
				}else if($array['leader_only']==false){
					$selected = '';
					if( ($data['PositionCode']!='' && $data['PositionCode']==$array['selected'])
						|| ( $data['id_card']!='' && $data['id_card']==$array['selected'] ) ){
						$selected = 'selected="selected"';
					}
					if($array['position']==true){//ตำแหน่ง
						$option .= '<option '.$selected.' value="'.$data['position_id'].'_'.$data['PositionCode'].'" class="icons_tree" style="'.$style.'">'.$data['PositionCode'].' : '.$data['position_name'].' : '.$section[$data['Section']]['name'].'</option>';
					}else{//คน
						$option .= '<option '.$selected.' value="'.$data['position_id'].'_'.$data['PositionCode'].'_'.$data['id_card'].'" class="icons_tree" style="'.$style.'">'.$data['emp_name'].' ['.$data['PositionCode'].']</option>';
					}
				}
			}
		}
		return $option;
	}//if array
}
//สร้างข้อมูลให้กับ อาร์เรย์ทั้งหมด ต้องวนลูปไปทีละทีม
function gen_select_team_array($all_team_data, $sub=0, $array=null){
	$select_option = '';
	if(empty($all_team_data)){return ;}
	foreach($all_team_data as $team_data){
		$select_option .= gen_select_team($team_data, $sub, $array);
	}
	return $select_option;
}


/**
 * หัวหน้าทั้งหมดในตารางทีม ซึ่งแล้วแต่ case ที่ส่งมา
 * @param Array $array ตัวเลือกที่ส่งมาเพิ่มเติม [position_where],[all_leader],[group_by],[YearMonth]
 * @param String $case ถ้าไม่ส่งจะเป็น ทีมขาย ถ้าส่งคำว่า OU จะดึงทีมของ OU
 * @param Boolean $debug ถ้าส่งค่า true จะแสดงข้อมูลสำหรับแก้ไข
 * @return เป็นค่า Array ประกอบด้วยคีย์[position_id]=> [position_code] ของหัวหน้าทั้งหมด
 */
function get_all_leader($array=null, $case=null, $debug=null)
{
	global $db;

	if($case=='OU'){//ทีมใน OU (จะได้ position_id)
		$team_table = "$db[DataBaseOU].command";
		$join_position_field = 'position.id';//ฟิลด์ที่ใช้ JOIN
	}else{//ทีมในขาย	(จะได้ PositionCode)
		$team_table = "$db[DataBaseName].team_OU_target";
		$check_year = "$team_table.YearMonth = '$array[YearMonth]' AND";
		$join_position_field = 'position.PositionCode';//ฟิลด์ที่ใช้ JOIN
	}

	//ถ้ามีการเลือกเฉพาะในบริษัทนั้น หรือเงื่อนไขอื่นๆ (อาจจะมีหัวหน้าแต่เป็นบริษัทอื่น นับเป็นสูงสุดในบรํษัทนั้นๆ)
	if($array['position_where']){
		$position_where = "AND $array[position_where]";
	}

	//ถ้าส่งข้อมูล position_code ของหัวหน้ามาด้วย
	if($array['all_leader']){
		$all_leader_in = "'".implode("', '", $array['all_leader'])."'";
		$where_all_position_leader = "AND $team_table.Leader IN ($all_leader_in)";
	}

	if($array['group_by']==''){//สำหรับ GROUP by
		$array['group_by'] = 'Leader';
	}

	$leader_position_id = array();
	$leader_position_code = array();
	$sql = "SELECT position.id, position.PositionCode FROM $team_table
	INNER JOIN $db[DataBaseOU].`position` ON $team_table.Leader = $join_position_field AND position.status != '99'
	WHERE $check_year $team_table.status != '99' $position_where $where_all_position_leader
	GROUP by $array[group_by] ";
	//echo '<br>'.$sql;
	$result = mysql_query($sql) or die(show_sql_error($sql,__FILE__,__LINE__));
	while($rs = mysql_fetch_assoc($result)){
		$leader_position_code[$rs['id']] = $rs['PositionCode'];//$array['234'] = 'CSS012343';
	}
	return $leader_position_code;
}

/**
 * หาหัวหน้าที่ยังเป็นลูกน้องคนอื่นอยู่ (ที่อยู่ในหัวหน้าชุดเดียวกัน)
 * @param Array $leader_position หัวหน้าที่ต้องการตรวจสอบ ส่งมาเป็น array ถ้าเป็น OU จะส่งเป็น id มาให้
 * @param String $YearMonth ใช้สำหรับระบุเดือนด้วยกรณีดึงจากทีม easysale
 * @return Array จะเป็น position_id หรือ position_code ของหัวหน้าที่ยังเป็นลูกน้องอยู่ ข้อมูลที่ได้จะขึ้นอยู่กับเงื่อนไขตารางที่เลือกว่าเป็นตารางไหน
 */
function get_leader_in_follower($leader_position, $YearMonth=null, $case=null)
{
	global $db;
	$not_top_leader = array();
	$all_position_in = "'".implode("', '", $leader_position)."'";//PositionCode
	if($case=='OU'){//ทีมของ OU (จะไม่มีวันที่)  จะได้ position_id
		$sql = "SELECT Follower FROM $db[DataBaseOU].command
		WHERE status != '99' AND Follower IN ($all_position_in)
		AND Leader IN ($all_position_in) GROUP by Leader,Follower ";
	}else{//ทีมของขาย ที่ดึงมาจาก OU โดยมีวันที่กำกับ จะได้ PositionCode
		$sql = "SELECT position_code AS Follower FROM $db[DataBaseName].team_OU_target
		WHERE YearMonth = '$YearMonth' AND status != '99' AND position_code IN ($all_position_in)
		AND Leader IN ($all_position_in) GROUP by Leader,position_code ";
	}
	$result = mysql_query($sql) or die(show_sql_error($sql,__FILE__,__LINE__));
	while($rs = mysql_fetch_assoc($result)){
		$not_top_leader[] = $rs['Follower'];//ไอดี หัวหน้า ที่ยังเป็นลูกน้องคนอื่นอีก
	}
	return $not_top_leader;
}


//------------- BEGIN OU -------------
/**
 * ข้อมูลทีมในสายงาน OU
 * @param Number $position_id รหัสไอดีตำแหน่ง
 * @param Array $array_option เงื่อนไขอื่นๆ [position_where],[check_position]
 * @param unknown_type $debug
 */
function get_all_follow_team_ou($position_id=null,$array_option=null,$debug=null)
{
	global $db;
	$data = array();

	#STEP 1 :: ถ้าเป็นค่าว่างหมายถึงของเซสชั่นนั้น แต่ถ้า check_position คือต้องไม่เป็นค่าว่าง
	if($position_id=='' && $array_option['check_position']==false){
		$position_id=$_SESSION['SESSION_Position_id'];
	}else if($position_id==''){//ถ้าเป็นค่าว่าง และมีการเช็กค่าว่างด้วยให้คืนค่าว่าง
		return ;
	}
	//--

	#STEP 2 :: ถ้าส่งเงื่อนไขมาด้วย
	if($array_option['position_where']){
		//ถ้ามีการเลือกเฉพาะในบริษัทนั้น หรือเงื่อนไขอื่นๆ (อาจจะมีหัวหน้าแต่เป็นบริษัทอื่น นับเป็นสูงสุดในบรํษัทนั้นๆ)
		$position_where = "AND $array_option[position_where] ";
	}

	#STEP 3 //FOLLOW ลูกน้องทั้งหมดของตำแหน่งนี้ ตามเงื่อนไข $position_where ด้วย
	$all_position_id = array();
	$sql = "SELECT position.id,position.PositionCode FROM $db[DataBaseOU].command
	INNER JOIN $db[DataBaseOU].`position` ON command.Follower = position.id AND position.status != '99' $position_where
	WHERE command.Leader = '$position_id' AND command.status != '99' ORDER by position.PositionCode ";
	$result = mysql_query($sql) or die(show_sql_error($sql,__FILE__,__LINE__));
	while($rs = mysql_fetch_assoc($result)){
		$all_position_id[] = $rs['id'];
	}//while
	if(empty($all_position_id)){return false;}//ไม่มีข้อมูล ให้คืนค่าว่าง

	#STEP : เช็กก่อนว่าอยู่ใน position_id ที่ส่งมาด้วยหรือไม่
	if(!empty($array_option['check_in_position_id'])){
		$all_position_id = array_intersect( $all_position_id, $array_option['check_in_position_id'] );
	}

	//อยู่ในทีม
	//if($position_id=='594'){echo '<pre>'.print_r($all_position_id, true).'</pre><hr>';}

	#STEP 5 จะได้เป็นอาร์เรย์ข้อมูลทั้งหมด ของลูกทีมชุดนี้
	//$data = get_position_data( $all_position_id, true, $position_option );	#-- ย้าย
	#STEP 6 // หาลูกทีมของลูกทีมอีกทีนึง
	$data = array();
	$array_option['check_position']=true;
	$i = 0;//เปลี่ยนจาก position_id เป็นเลขลำดับแทน 1 position อาจจมีข้อมูลมากกว่า 1 คน
	foreach ($all_position_id as $Follower_id) {
		$my_data = get_position_data( $Follower_id, true, $position_option );	#-- ย้าย
		$data[$i] = $my_data;//ข้อมูลหัวหน้า
		$data[$i]['Leader'] = $position_id;//รหัสหัวหน้า เป็นไอดี
		if( $team_data = get_all_follow_team_ou( $Follower_id, $array_option) ){
			$data[$i]['team'] = $team_data;//ข้อมูลลูกทีม
		}
		$i++;
	}//foreach



	if($debug==true){echo '<hr><pre>'.print_r($data,true).'</pre>';}
	return $data;
}//function


//OU
if( ! function_exists('get_all_top_leader_no_target') )
{
	/**
	 * หารายชื่อหัวหน้าระดับบนสุด จากสายงานผังองค์กร เป็นข้อมูลที่ยังไม่ได้กำหนดเป้า
	 * @param Array $array ถ้าส่งมาด้วยจะหาหัวหน้าสูงสุดของค่าที่ส่งมา ประกอบด้วย [company_id],[department_id],[section_id]
	 * @return Array เป็นไอดีสูงสุดของหัวหน้าในบริษัทนั้นๆ
	 */
	function get_all_top_leader_no_target($array=null,$debug=null)
	{
		global $db;

		#STEP 1 //-- หาที่เป็นหัวหน้าทั้งหมด (position.id) ใน OU
		$leader_position_code = get_all_leader($array, 'OU');//จากสายงานใน OU
		$leader_position_id = array_keys($leader_position_code);
		if(empty($leader_position_id)){return false;}//ไม่มีข้อมูลหัวหน้าเลย ให้หยุดการทำงาน

		#STEP 2 //หาหัวหน้าที่ยังเป็นลูกน้องคนอื่นอยู่ (ที่อยู่ในหัวหน้าชุดเดียวกัน) ได้ position_id
		$not_top_leader = get_leader_in_follower($leader_position_id, '', 'OU');//จากสายงานใน OU

		#STEP 3 //คัดเฉพาะหัวหน้าที่ไม่เป็นลูกน้องของใคร position_code
		if(empty($not_top_leader)){
			$data = $leader_position_id;//ถ้าไม่เป็นลูกน้องใคร แสดงว่าหัวหน้าที่เซ็ตเป้าไว้แล้ว เป็นหัวหน้าสูงสุด
		}else{//ถ้ามีัหัวหน้าที่ยังเป็นลูกน้องหัวหน้าคนอื่นอยู่
			$data = array_diff( $leader_position_id, $not_top_leader );//เอาไอดี ที่ยังเป็นลูกน้องอยู่ออกไป
		}

		#STEP 6 วนลูปเก็บข้อมูลลูกน้องทั้งหมด ของหัวหน้าที่อยู่บนสุด ทีละคน
		$all_team = array();
		$i = 0;
		$array['check_position'] = true;
		foreach ($data as $position_id) {//หัวหน้าที่ไม่เป็นลูกน้องใคร
			$all_team[$i] = get_position_data( $position_id, true );//ข้อมูลตัวเอง ต้องใช้ position.id
			if($team_data = get_all_follow_team_ou( $position_id, $array ) ){
				$all_team[$i]['team'] = $team_data;//ข้อมูลทีม array หลายมิติ
			}
			$i++;
		}
		//--

		//Debug
		if($debug){
			echo '<hr>ALL LEADER Position ID:: '.count($leader_position_id).'<br>';print_r($leader_position_id);//STEP 2
			echo '<hr>NOT_top_leader PositionCode:: '.count($not_top_leader).'<br>';print_r($not_top_leader,true);//STEP 3
		}
		return $all_team;
	}//function
}//if


//OU
//OU
//OU

//------------- END OU -------------




//-------------------------- EASYSALE -------------------------//
if( ! function_exists('get_all_top_leader_in_target') )
{
	/**
	 * หารายชื่อหัวหน้าระดับบนสุด ของแต่ละบริษัท ซึ่งถ้ามีการส่งข้อมูลบริษัทมาด้วย จะดึงมาแสดงเฉพาะที่ส่งมาเท่านั้น
	 * @param Array $array ถ้าส่งมาด้วยจะหาหัวหน้าสูงสุดของค่าที่ส่งมา ประกอบด้วย ['position_where'] , ['all_leader']=>array(PositionCode), ['YearMonth']=>ปีเดือน ค.ศ.
	 * @return Array คีย์ที่ได้ 	[id_card] => เลข 13 หลัก
				            [PositionCode] => IIA106101
				            [position_name] => ผู้จัดการ
				            [emp_name] => มยุรี เทพสาร (รีย์)
				            [position_id] => 525
				            [team]=> ถ้ามีทีม จะได้เป็น Array
	 */
	function get_all_top_leader_in_target($array=null,$debug=null)
	{
		global $db;
		if(!empty($array)){//ถ้าส่งค่ามา แบบเลือกเฉพาะบรฺษัท สาขา แผนก
			$YearMonth = $array['YearMonth'];
		}
		if($YearMonth==''){ $YearMonth = (date('Y')+543).date('m');}//ปี พ.ศ.
		//--
		#Position ID ทั้งหมด ของหัวหน้า
		if(!empty($array['all_leader']) && array_filter($array['all_leader'])){
			$all_leader_in = "'".implode("', '", $array['all_leader'])."'";//PositionCode
			$all_leader_in_where = "AND team_OU_target.Leader IN ($all_leader_in)";
			$position_in_where = "AND PositionCode IN ($all_leader_in)";
		}
		//--

		//ถ้ามีการเลือกเฉพาะในบริษัทนั้น หรือเงื่อนไขอื่นๆ (อาจจะมีหัวหน้าแต่เป็นบริษัทอื่น นับเป็นสูงสุดในบรํษัทนั้นๆ)
		if($array['position_where']){
			$position_where = "AND $array[position_where]";
		}

		#STEP 2 //เลือกเฉพาะหัวหน้าที่มีการ เซ็ตข้อมูลเป้าไว้แล้ว (จะได้ position id ที่ตั้งเป้าทีมไว้แล้ว)
		//****** เพิ่มเช็ก WHERE $position_where อาจจะเป็น company_id หรือ department_id , etc., แล้วแต่ส่งมา
		$sql  = "SELECT position.id AS position_id, PositionCode FROM $db[DataBaseOU].`position`
		WHERE status != '99' $position_in_where $position_where ";
		$position_all = return_query($sql,true, array('key_index'=>'PositionCode') );

		#STEP : หารหัสตำแหน่งของหัวหน้า
		$team_position_id = array();
		$team_position_code = array();
		$sql  = "SELECT DISTINCT(Leader) FROM $db[DataBaseName].`team_OU_target`
		WHERE YearMonth = '$YearMonth' AND Leader != '' AND status != '99' $all_leader_in_where  ORDER by Leader";
		$result = mysql_query($sql) or die(show_sql_error($sql,$_SERVER['PHP_SELF']));
		while($team = mysql_fetch_assoc($result)) {
			$pos_id = $position_all[$team['Leader']]['position_id'];
			if($pos_id){
				$team_position_id[$team['Leader']] = $pos_id;//$array['CSS012343'] = '234';
				$team_position_code[$team['Leader']] = $team['Leader'];
			}
		}
		#STEP : หารหัสตำแหน่งของหัวหน้าเป็นค่าว่าง ใช้รหัสพนักงานเป็นหัวหน้า (ไม่มีหัวหน้า)
		$strSQLs = "SELECT DISTINCT(position_code) FROM $db[DataBaseName].team_OU_target
		WHERE YearMonth = '$YearMonth' AND Leader = '' AND status != '99' ORDER by position_code ";
		$qry = mysql_query($strSQLs) or die(mysql_error());
		while($rss2 = mysql_fetch_assoc($qry)){
			$pos_id = $position_all[$rss2['position_code']]['position_id'];
			if($pos_id){
				$team_position_id[$rss2['position_code']] = $pos_id;
				$team_position_code[$rss2['position_code']] = $rss2['position_code'];
			}
		}
		if($debug){
			echo '<hr>ALL team_OU_target POSITION CODE :: '.count($team_position_code).'<br>';print_r($team_position_code);echo '<br>'.$sql;
		}
		if(empty($team_position_code)){return false;}//ไม่มีการตั้งเป้าทีม ให้หยุดการทำงาน

		#PositionCode ทั้งหมด ของหัวหน้าที่ตั้งเป้าไว้แล้ว
		$all_position_code_in_target = "'".implode("', '", $team_position_code)."'";//ID
		//--

		#STEP 3 //หาหัวหน้าที่ยังเป็นลูกน้องคนอื่นอยู่
		$not_top_leader = array();
		$sql = "SELECT position_code FROM $db[DataBaseName].team_OU_target
		WHERE YearMonth = '$YearMonth' AND status != '99' AND position_code IN ($all_position_code_in_target)
		AND Leader IN ($all_position_code_in_target) GROUP by Leader,position_code ";
		$result = mysql_query($sql) or die(show_sql_error($sql,$_SERVER['PHP_SELF']));
		while($rs = mysql_fetch_assoc($result)){
			$not_top_leader[] = $rs['position_code'];//ไอดี หัวหน้า ที่ยังเป็นลูกน้องคนอื่นอีก
		}
		//--
		if($debug){
			echo '<hr>NOT TOP LEADDER :: '.count($not_top_leader).'<br>';print_r($not_top_leader);echo '<br>'.$sql;
		}

		#STEP 4 //คัดเฉพาะหัวหน้าที่ไม่เป็นลูกน้องของใคร
		if(empty($not_top_leader)){
			$data = $team_position_code;//ถ้าไม่เป็นลูกน้องใคร แสดงว่าหัวหน้าที่เซ็ตเป้าไว้แล้ว เป็นหัวหน้าสูงสุด
		}else{//ถ้ามีัหัวหน้าที่ยังเป็นลูกน้องหัวหน้าคนอื่นอยู่
			$data = array_diff( $team_position_code, $not_top_leader );//เอาไอดี ที่ยังเป็นลูกน้องอยู่ออกไป
		}
		if($debug){echo '<hr>IS TOP LEADER :: '.count($data).'<br>';print_r($data);}

		#STEP 5 วนลูปเก็บข้อมูลลูกน้องทั้งหมด ของหัวหน้าที่อยู่บนสุด ทีละคน
		$all_team = array();
		$arr_option['YearMonth'] = $YearMonth;
		$arr_option['check_position'] = true;
		$num = 0;
		foreach ($data as $position_code) {//หัวหน้าที่ไม่เป็นลูกน้องใคร
			$position_id = $team_position_id[$position_code];
			$all_team[$num] = get_position_data($position_id);//ข้อมูลตัวเอง ใช้ position.id
			$all_team[$num]['team'] = get_all_follow_team($position_code, $arr_option);//ข้อมูลทีม array หลายมิติ ใช้ PositionCode
			$num++;
		}
		//--
		return $all_team;
	}//function
}//if


if( ! function_exists('get_all_follow_team') )
{
	/**
	 * หาข้อมูลของลูกทีม หากไม่ส่งค่าไอดีของตาราง position มาด้วย จะใช้ SESSION_Position_id ผู้ใช้ขณะนั้น
	 * @param Number $position_code รหัสของตำแหน่งพนักงาน
	 * @param Array $array_option เป็นค่ากำหนดอื่นๆ [YearMonth] กำหนดช่วงเวลาของทีม [all_position_code],[position_where]
	 * @return Array โดยคีย์ 	[id_card] => 3529900217641
				            [PositionCode] => IIA106101
				            [position_name] => ผู้จัดการ
				            [emp_name] => มยุรี เทพสาร (รีย์)
				            [position_id] => 525
				            [team]=> ถ้ามีทีม จะได้เป็น Array
	 */
	function get_all_follow_team($position_code=null,$array_option=null,$debug=null)
	{
		global $db;
		$data = array();

		//ถ้าเป็นค่าว่างหมายถึงของเซสชั่นนั้น
		if($position_code=='' && $array_option['check_position']==false){
			$position_code=$_SESSION['SESSION_Position'];
		}else if($position_code==''){//ถ้าเป็นค่าว่าง และมีการเช็กค่าว่างด้วยให้คืนค่าว่าง
			return ;
		}

		#STEP 1 //FOLLOW ทั้งหมดของตำแหน่งนี้
		if($array_option['all_position_code']){
			$where_in = " position_code IN ('".implode("', '", $array_option['all_position_code'])."') AND";
		}
		if($array_option['position_where']){//ถ้าส่งเงื่อนไขมาด้วย
			$position_option['where'] = $array_option['position_where'];
			//ถ้ามีการเลือกเฉพาะในบริษัทนั้น หรือเงื่อนไขอื่นๆ (อาจจะมีหัวหน้าแต่เป็นบริษัทอื่น นับเป็นสูงสุดในบรํษัทนั้นๆ)
			$position_where = "AND $array_option[position_where]";
		}

		if($array_option['YearMonth']!=''){
			$YearMonth = $array_option['YearMonth'];//ปี ค.ศ.
		}else{
			$YearMonth = (date('Y')+543).date('m');//ปี ค.ศ.
		}

		$sql = "SELECT position.id,position.PositionCode FROM $db[DataBaseName].team_OU_target
		INNER JOIN $db[DataBaseOU].`position` ON team_OU_target.position_code = position.PositionCode AND position.status != '99' $position_where
		WHERE $where_in team_OU_target.YearMonth = '$YearMonth' AND team_OU_target.Leader = '$position_code'
		AND team_OU_target.status != '99' ORDER by PositionCode ";
		$all_position_code  = array();
		$result = mysql_query($sql) or die(show_sql_error($sql,__FILE__,__LINE__));
		while($rs = mysql_fetch_assoc($result)){
			$all_position_code[$rs['id']] = $rs['PositionCode'];
		}//while
		if(empty($all_position_code)){return false;}//ไม่มีข้อมูล ให้คืนค่าว่าง
			#STEP 2 //หาข้อมูลของลูกน้อง
		//-- จะได้ขอมูลของลูกทีมทั้งหมดของ position id นี้

		//ต้องใช้ position_id
		//จะได้เป็นอาร์เรย์ของหัวหน้าทั้งหมด
		//$data = get_position_data( $all_position_id, true, $position_option );	#-- ส่งไปเป็นอาร์เรย์

		#STEP 3 // หาลูกทีมของลูกทีมอีกทีนึง ต้องใช้ position_code
		$array_option['check_position']=true;
		$data = array();
		$i=0;//เปลี่ยนจากใช้ position
		foreach ($all_position_code as $position_id=>$Follower_Code) {
			$my_data = get_position_data( $position_id, true, $position_option );	#-- ส่งไปเป็นอาร์เรย์
			$data[$i] = $my_data;//ข้อมูลหัวหน้า
			$data[$i]['Leader'] = $position_code;//รหัสหัวหน้า เป็นรหัสตำแหน่ง
			if( $team_data = get_all_follow_team( $Follower_Code, $array_option) ){
				$data[$i]['team'] = $team_data;//ข้อมูลลูกทีม
			}
			$i++;
		}//foreach
		if($debug){echo '<hr><pre>'.print_r($data,true).'</pre>';}
		return $data;
	}//function

}//if



if(!function_exists('get_position_data'))
{
	/**
	 * Enter description here ...
	 * @param Number $position_id รหัสไอดี ตำแหน่ง (auto id) ส่งเป็น Array ก็ได้
	 * @param Boolean $require_position_id ถ้าเป็น true จะต้องส่ง position_id มาด้วยเสมอ
	 * @param Array $option ใช้กำหนดค่าอื่นๆที่ต้องการ ที่ใช้อยู่ ณ ปัจจุบันคือค่า [select] เลือกฟิลด์ที่ต้องการ
	 * @return คือค่าตามค่า select ที่ส่งมาหรือค่าที่กำหนดไว้อยู่แล้ว
	 */
	function get_position_data( $position_id=null, $require_position_id=null, $option=null )
	{
		global $db,$conf_db,$lang;
		$query_case = false;

		//-- WHERE
		if($require_position_id==true && empty($position_id)){	//ถ้ามีการบังคับส่ง Position ID มาด้วย แต่ไม่ได้ส่ง คืนค่าว่างกลับไป
			return false;
		}else if( $require_position_id!=true && empty($position_id)){//ถ้าเรียกโดยไม่ส่งพารามิเตอร์ใดๆ เท่ากับเรียกของตัวเอง
			$where_position_id = " position.id = '$_SESSION[SESSION_Position_id]'";
		}else if(gettype($position_id)=='array'){//ถ้าส่งมาเป็นอาร์เรย์
			$where_position_id = "position.id IN ('".implode("', '", $position_id)."')";
			$query_case = true;
		}else if($position_id != ''){//ถ้าไม่ใช่อาร์เรย์
			$where_position_id = "position.id = '$position_id' ";
		}
		//--
		$select = "position.id AS position_id,relation_position.id_card,
		position.PositionCode, position.Name AS position_name, position.Section,
		CONCAT(emp_data.Name,' ',emp_data.Surname,' (',emp_data.Nickname,')') AS emp_name";
		if($option['select']){
			$select = $option['select'].",CONCAT(emp_data.Name,' ',emp_data.Surname,' (',emp_data.Nickname,')') AS emp_name";
		}
		if($option['where']){
			$where_other = "AND $option[where]";
		}

		//POSTION
		//$check_out_off = "AND emp_data.Prosonnal_Being != '3'";//เช็กลาออก ถ้าใช้จะทำให้ทีมที่หัวหน้าลาออกไม่มีชื่อ
		//เปลี่ยนจาก INNER เป็น LEFT JOIN กรณีลูกน้องเป็นแค่ตำแหน่ง ไม่มีพนักงานจริง
		$sql = "SELECT $select FROM $db[DataBaseOU].`position`
		LEFT JOIN $db[DataBaseOU].`relation_position` ON  position.id = relation_position.position_id AND relation_position.status != '99'
		LEFT JOIN $db[Member].`emp_data` ON relation_position.id_card = emp_data.ID_Card $check_out_off
		WHERE $where_position_id $where_other AND position.status != '99' ORDER by PositionCode ASC	";
		//if($position_id=='809'){echo '<pre>'.print_r($sql, true).'</pre><hr>';}
		//$array_option = array('key_index'=>'position_id');//คีย์เป็นรหัสไอดีตำแหน่ง
		//return return_query($sql,$query_case,$array_option);//ถ้าส่ง position_id มาเป็นอาร์เรย์ จะคืนค่าเป็นอาร์เรย์หลายมิติ ถ้าส่งค่าเดี่ยวจะได้ของคนเดียว
		mysql_query("SET NAMES $conf_db[charset]");
		$result = mysql_query($sql) or die(show_sql_error($sql,$_SERVER['PHP_SELF']));
		$arr = array();
		if($query_case===true){//multi record
			while($rs = mysql_fetch_assoc($result)){
				if(trim($rs['emp_name'])==''){
					$rs['emp_name'] = $lang['team_emp_name_null'];
				}
				$arr[] = $rs;
			}
		}else{//single record
			$arr = mysql_fetch_assoc($result);
			if(trim($arr['emp_name'])==''){
				$arr['emp_name'] = $lang['team_emp_name_null'];
			}
		}
		return $arr;
	}//function
}//if

/**
 * ข้อมูลตำแหน่งทั้งหมด สำหรับใช้กับ Section ข้อมูลที่ได้จะเป็นอาร์เรย์ ฝ่าย (Secition id)
 */
function get_all_position()
{
	global $db;
	$position = array();
	$sql  = "SELECT id,PositionCode,Section,Name FROM $db[DataBaseOU].`position` WHERE status != '99' ORDER by Level, runNumber ";
	$result = mysql_query($sql) or die(show_sql_error($sql,$_SERVER['PHP_SELF']));
	while($rs = mysql_fetch_assoc($result)) {
		$position[$rs['Section']][$rs['id']] = $rs;
	}
	return $position;
}

/**
 * หาข้อมูลฝ่ายทั้งหมด สำหรับใช้กับ Department จะได้เป็นอาร์เรย์ Department ID
 * @param Array $position ข้อมูลตำแหน่งทั้งหมด ที่คีย์เป็นรหัส section
 * @param Array $option ตัวเลือกเพิ่มเติม เช่น ['check_position'] คือต้องมี ตำแหน่ง  ถึงจะเก็บค่าแผนกนี้ไว้
 */
function get_all_section($position=null, $option=null){
	global $db;
	$section = array();
	$sql_sec  = "SELECT id,code_name,department,name FROM $db[DataBaseOU].`section` WHERE status != '99' ";
	$result_sec = mysql_query($sql_sec) or die(show_sql_error($sql_sec,$_SERVER['PHP_SELF']));
	while($rs = mysql_fetch_assoc($result_sec)) {
		if(!empty($position)){ $rs['position'] = $position[$rs['id']]; }
		if(empty($rs['position']) && $option['check_position']==true){
			continue;
		}else{
			if($option['key_index']!=''){
				if($option['key_value']!=''){
					$value = $rs[$option['key_value']];//ฟิลด์ที่ต้องการค่า แบบระบุฟิลด์
				}else{
					$value = $rs;//ถ้าไม่ได้ระบุจะเก็บอาร์เรย์ทั้งหมด
				}
				$section[$rs[$option['key_index']]] = $value;
			}else{
				$section[$rs['department']][$rs['id']] = $rs;
			}
		}
	}
	return $section;
}

/**
 * หาข้อมูลแผนกทั้งหมด โดยส่งค่า ฝ่ายมาด้วย ใช้กับ WorkingCompany จะได้เป็นอาร์เรย์ WorkingCompany ID
 * @param Array $section ข้อมูลฝ่ายทั้งหมด ที่คีย์เป็นรหัส department
 * @param Array $option ตัวเลือกเพิ่มเติม เช่น ['check_position'] คือต้องมี ตำแหน่ง  ถึงจะเก็บค่าแผนกนี้ไว้
 */
function get_all_department($section=null,$option=null){
	global $db;
	$select = $option['select'];
	if($select==''){
		$select = 'id,code_name,short_name,name';
	}
	$sql  = "SELECT $select FROM $db[DataBaseOU].`department` WHERE status != '99' $option[where] $option[sql_condition] ";
	$result = mysql_query($sql) or die(show_sql_error($sql,$_SERVER['PHP_SELF']));
	while($rs = mysql_fetch_assoc($result)) {
		$rs['section'] = $section[$rs['id']];
		if(empty($rs['section']) && $option['check_position']==true){
			continue;
		}else{
			$department[$rs['company']][$rs['id']] = $rs;
		}
	}
	return $department;
}

/**
 * บริษัททั้งหมด
 * @param Array $department ข้อมูลแผน่กทั้งหมด ที่คีย์เป็นรหัส company_id
 * @param Array $option ตัวเลือกเพิ่มเติม เช่น ['check_position'] คือต้องมี ตำแหน่ง  ถึงจะเก็บค่าแผนกนี้ไว้
 */
function get_all_company($department=null, $option=null){
	global $db,$table_config;
	$select = $option['select'];
	if($option['where']){$where = "AND $option[where]";}
	$company = array();
	if($select!=''){
		$select = str_replace(' ','',$select);
		$exp = explode(',',$select);
		if(!in_array('id', $exp)){
			$select .= ',id';//ใช้อ้างอิงชุดอาร์เรย์ที่ต้องการ
		}
	}else{
		$select = 'id,code_name,short_name,name';
	}
	$sql = "SELECT $select FROM $table_config[working_company] WHERE status != '99'
	$where ORDER by CONVERT(name USING TIS620)";
	$result = mysql_query($sql) or die(show_sql_error($sql,$_SERVER['PHP_SELF']));
	while($rs = mysql_fetch_assoc($result)){
		$rs['department'] = $department[$rs['id']];//จะได้ department ทั้งหมด ของ company นี้
		if(empty($rs['department']) && $option['check_position']==true){
			continue;
		}else{
			$company[$rs['id']] = $rs;
		}
	}
	return $company;
}

if(!function_exists('company_structure_team'))
{
	/**
	 * สำหรับดึงข้อมูลบริษัท หากไม่ส่งค่าใดๆ จะได้ข้อมูลทุกบริษัทที่ยัง active อยู่
	 * @param Array $option ฟิลด์บริษัทที่ต้องการ [select] เงื่อนไข [where] หากต้องการคัดเฉพาะที่มีตำแหน่งพนักงาน [check_position]
	 * @return จะคืนค่าเป็นข้อมูลอาร์เรย์ โดยคีย์จะใช้ไอดีของแต่ละบริษัท แต่ละตำแนห่งเรียงกันไป $array_compay[50][department][60][section][162][position][729][Name] => ผู้จัดการ
	 */
	function company_structure_team($option=null)
	{
		global $db,$table_config;

		//--POSITION เก็บไว้ในอาร์เรย์ section จะได้ข้อมูล 1 section มีหลาย position
		$position = get_all_position();
		//--

		//-- SECTION เก็บไว้ในอาร์เรย์ department จะได้ข้อมูล 1 department มีหลาย section
		$section = get_all_section($position,$option);
		//--

		//-- DEPARTMENT เก็บไว้ในอาร์เรย์ company จะได้ข้อมูล 1 company มีหลาย department
		$department = get_all_department($section,$option);
		//--

		//-- COMPANY ข้อมูลบริษัททั้งหมด
		$company = get_all_company($department, $option);
		//--

		return $company;
	}//function
}//if

//ใช้แล้วมีปัญหา ยกเลิกใช้
if(!function_exists('all_follow_position_idcard'))
{
	/**
	 * ดึงข้อมูล รหัสตำแหน่ง และ รหัสประชาชน ของลูกทีมทั้งหมด
	 * @param Number $position_code รหัสตำแหน่งของหัวหน้า
	 * @param Array $array_option สำหรับส่งค่าเพิ่มเติมอื่นๆ ['team_format'] แยกใครเป็นหัวหน้าใคร
	 * @param Boolean $debug ส่งค่า true เพื่อดูข้อมูลที่ได้
	 */
	function all_follow_position_idcard($position_code=null,$array_option=null,$debug=null,$call_back=0)
	{
		global $db, $lang;
		$data = array();

		//----------------------------CHECK OVER CALLBACK---------------------------------
		$over_load = 1000;
		if($position_code == $array_option['old_position_code'] && ++$call_back > $over_load){
			echo '<div style="color:red">ERROR :: Last position_code  "'.$position_code.'" is over '.$over_load.' LOOP !!!</div>';
			exit();
		}
		//--------------------------------------------------------------------------------

		# ถ้าไม่ส่ง POSITION ID มาด้วย จะเป็นการดึงลูกทีมของผู้ใช้ ณ ขณะนั้น
		if(empty($position_code) && $array_option['check_position']!=true){
			$position_code = $_SESSION['SESSION_Position'];
		}
		#STEP 1 //FOLLOW ทั้งหมดของตำแหน่งนี้
		$all_position_code = array();

		if($array_option['YearMonth'] =='') $array_option[YearMonth] = (date('Y')+543).date('m');

		$sql = "SELECT position_code AS Follower FROM $db[DataBaseName].team_OU_target
		WHERE YearMonth = '$array_option[YearMonth]' AND Leader = '$position_code' AND team_OU_target.status != '99' ";
		if($debug){ echo '<br>'.$sql; }
		$result = mysql_query($sql) or die( show_sql_error($sql, __FILE__, __LINE__) );
		while($rs = mysql_fetch_assoc($result)){
			$all_position_code[] = $rs['Follower'];
		}//while


		if(empty($all_position_code)){return '';}//$lang['found_not_data'];}//ไม่มีข้อมูล ให้คืนค่าว่าง
		$all_position_code_in = "'".implode("', '",$all_position_code)."'";

		if($debug){ echo '<br>'.$all_position_code_in; }

		//All My Data
		$select = "relation_position.position_id,relation_position.id_card, position.PositionCode ";
		$sql = "SELECT $select FROM $db[DataBaseOU].`position`
		INNER JOIN $db[DataBaseOU].`relation_position` ON position.id = relation_position.position_id AND relation_position.status != '99'
		WHERE position.PositionCode IN ($all_position_code_in) AND position.status != '99' ORDER by position.PositionCode ASC	";
		$result = mysql_query($sql) or die( show_sql_error($sql, __FILE__, __LINE__) );
		while($rs = mysql_fetch_assoc($result)){
			$all_data[] = $rs;
		}//while

		#STEP 2 // หาลูกทีมของลูกทีมอีกทีนึง
		$array_option['check_position'] = true;
		$array_option['old_position_code'] = $position_code;
		if($array_option['team_format']==true){//แบบแยกว่าใครเป็นหัวหน้าลูกน้องใคร
			$new_data = array();
			foreach ($all_data as $data) {
				$new_data[$data['position_id']]['position_id'] = $data['position_id'];
				$new_data[$data['position_id']]['id_card'] = $data['id_card'];
				$new_data[$data['position_id']]['PositionCode'] = $data['PositionCode'] ;
				if( $team_data = all_follow_position_idcard( $data['PositionCode'], $array_option, $call_back) ){
					$new_data[$data['position_id']]['team'] = $team_data;
				}
			}//foreach
			$all_data = $new_data;
		}else{//แบบ Position_id ต่อกับ id_card ไม่แยกหัวหน้าลูกน้อง
			foreach ($all_position_code as $Follower) {
				if(!$Follower){continue;}
				if($team_data = all_follow_position_idcard( $Follower, $array_option, $debug, $call_back) ){
					$all_data = array_merge($all_data,$team_data);//ถ้าในทีมมีลูกทีมจะเก็บเพิ่มเป็นอาร์เรย์ต่อไป
				}
			}//foreach

		}
		return $all_data;
	}
}//if

if(!function_exists('split_position_data'))
{
	/**
	 * ใช้สำหรับจัดกลุ่มตัวแปร id_card, position_id, PositionCode
	 * @param Array $array ข้อมูลของพนักงานแต่ละคน
	 * @return จะได้ค่าอาร์เรย์ 3 ตัวคือ [id_card],[position_id],[position_code]
	 */
	function split_position_data($array,$new_array=array(),$nsub=0) {
		$sub = 0;
		foreach ($array as $rs){
			$sub++;
			$index = $nsub.'_'.$sub;
			$new_array['position_id'][$index] = $rs['position_id'];
			$new_array['position_code'][$index] = $rs['PositionCode'];
			$new_array['id_card'][$index] = $rs['id_card'];
			if(!empty($rs['team']))//ถ้าเป็นแบบทีม
			{
				$new_array = split_position_data( $rs['team'], $new_array, $nsub+1 );
			}
		}
		return $new_array;
	}
}

/**
 * หารหัสไอดีของตำแหน่งที่เข้าใช้งานเมนูของขายได้
 * @param unknown_type $param
 */
function get_position_allow_set_team($param=null) {
	global $db,$program;
	#STEP 1 : ได้ไอดีของเมนู การจัดการลูกค้ามุ่งหวัง
	$sql = "SELECT id FROM $db[Member].`menu`
	WHERE place_id = '$program[place_id]' AND (`link` LIKE '$program[link_check_team]%' OR name LIKE '%$program[menu_check_team]%') AND link != '' ";
	$result = return_query( $sql, 'serial', array('key_index'=>'id') );//ได้เป็นอาร์เรย์ชั้นเดียว
	$all_menu_id_in = "'".implode("', '", $result)."'";
//echo '<br>1 '.$sql;
	#STEP 2 : หาผู้ใช้ทั้งหมดที่สามารถใช้เมนูนี้ได้
	$sql = "SELECT working_company_id, department_id, section_id, position_id, id_card
	FROM $db[Member].`menu_manage` WHERE menu_id IN ($all_menu_id_in) AND status != '99'
	GROUP BY working_company_id, department_id, section_id, position_id";
	$all_menu_allow = return_query($sql, true);//ได้เป็นอาร์เรย์ 2 มิติ
//echo '<br>2 '.$sql;
	//--ข้อมูลอ้างอิง
	#STEP 3 : หารหัสไอดีของตำแหน่งทั้งหมด ของพนักงานแต่ละคน
	$sql3 = "SELECT position_id,id_card FROM $db[DataBaseOU].`relation_position` WHERE status != '99'";
	$id_all = mysql_query($sql3) or die( show_sql_error($sql3,$_SERVER['PHP_SELF'],__LINE__) );
	while($idc = mysql_fetch_assoc($id_all)){
		$IDcard[$idc['id_card']][] = $idc['position_id'];//$id[1155700136238][0] = 25;
	}//while
	#STEP 4 : หารหัสไอดีของตำแหน่งทั้งหมด ของแต่ละบริษัท แต่ละแผนก แต่ละฝ่าย เอาไปใช้เทียบกับเมนู
	$sql = "SELECT id,WorkCompany,Department,Section FROM $db[DataBaseOU].`position` WHERE status != '99'";
	$position_all = mysql_query($sql) or die( show_sql_error($sql,$_SERVER['PHP_SELF'],__LINE__) );
	while($pos = mysql_fetch_assoc($position_all)){
		$comp[$pos['WorkCompany']][] = $pos['id'];//$comp[42][0] = 1;
		$depart[$pos['Department']][] = $pos['id'];//$depart[142][0] = 1;
		$sect[$pos['Section']][] = $pos['id'];//$sect[1102][0] = 1;
	}//while

	#STEP 5 : หารหัสไอดีตำแหน่งที่สามารถใช้เมนูได้ จากการกำหนดแบบทั้งบริษัท ทั้งแผนก หรือทั้งฝ่าย หรือรายบุคคล
	$position = array();
	if(!empty($all_menu_allow)){
		foreach($all_menu_allow as $rs){
			if($rs['position_id']){$position[] = $rs['position_id'];}//เก็บ position id ทีละตัว

			if($rs['working_company_id']){//เก็บ position id ทั้งหมดของบริษัทนี้
				$position = array_merge($position, $comp[$rs['working_company_id']]);
			}
			if($rs['department_id']){//เก็บ position id ทั้งหมดของแผนกนี้
				$position = array_merge($position, $depart[$rs['department_id']]);
			}
			if($rs['section_id']){//เก็บ position id ทั้งหมดของฝ่ายนี้
				$position = array_merge($position, $sect[$rs['section_id']]);
			}
			if($rs['id_card']){$position[] = $IDcard[$rs['id_card']];}//เก็บ position id ของพนักงานคนนี้่
		}//foreach
	}//if

	#STEP 6 : เช็กค่าว่าง
	$position = array_unique($position);

	//ทดสอบการไม่ได้กำหนดสิทธิ์ให้ใช้งานเมนู้ การจัดการลูกค้ามุ่งหวัง (ตามค่าคอนฟิกที่แสดงใน SQL ด้านบนสุดนั้น)
	//unset($position[3]);//ลองเอาหัวหน้าออก ลูกน้องจะไม่ปรากฏด้วย [3]=>589
	//unset($position[9]);//ลองเอาลูกน้องออก จะไม่ปรากฏด้วย [9]=>608
	return $position;
}

//END OF FILE