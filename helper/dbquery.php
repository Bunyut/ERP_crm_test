<?php
/**
 * @2012-02-22
 * ใช้สำหรับเก็บฟังก์ชั่นลดเวลาการเขียนโค๊ดติดต่อฐานข้อมูล
 */

if(!function_exists('show_sql_error'))
{
	function show_sql_error($sql,$file=null,$line=null){
		if($line){$line = ' line '.$line;}
		echo '<div style="color:blue;background-color:#F7F2F0;border:1px solid #CC9A87;padding:10px;width:70%;margin:auto;line-height:24px;">'
			.'<font color="red">'.mysql_error().' </font> '.$file.$line.'<br><font color="green">'.$sql.'</font></div>';
		exit();
	}
}

if(!function_exists('return_query'))
{
	/**
	 * ฟังก์ชั่น Query คำสั่ง SQL โดยเลือกคืนค่าแบบอาร์เรย์เดียว หรืออาร์เรย์สองมิติ
	 * @param String $sql คำสั่ง SQL
	 * @param Boolean $case ส่งค่าง true เพื่อคืนค่าแบบอาร์เรย์หลายมิติ
	 * @param Array $option_array ส่งค่าตัวเลือกอื่นๆ เพื่อกำหนดการดึงข้อมูลเพิ่มเติม ['key_index'] ถ้าส่งมาจะใช้ฟิลด์ที่กำหนดเป็นคีย์ของอาร์เรย์แต่ละแถว
	 * @return Array
	 */
	function return_query($sql,$case=null,$option_array=null)
	{
		global $conf_db;

		$key_index = $option_array['key_index'];//ฟิลด์ที่จะใช้เป็น key ของอาร์เรย์ทั้งหมด

		mysql_query("SET NAMES $conf_db[charset]");
		if(strpos($sql,'LIMIT')===false && strpos($sql,'limit')===false && $case==null){ $sql .= ' LIMIT 1'; }
		$result = mysql_query($sql) or die(show_sql_error($sql,$_SERVER['PHP_SELF']));
		if($case==='serial'){//ถ้าไม่ใช้ === 3 ตัวจะเข้าเคสนี้ตลอด
			if($option_array['target_field']==''){
				$target_field = $key_index;//ถ้าเป็นการเรียกแบบเดิมๆ จะส่ง key_index มาด้วยแต่ใช้อ้างอิงฟิลด์ที่ต้องการข้อมูล จึงอาจทำให้สับสน
			}else{
				$target_field = $option_array['target_field'];
			}
			$arr = array();
			if($target_field==''){//ถ้าไม่ได้ระบุฟิลด์ที่ต้องการข้อมูล จะใช้ฟิลด์แรก
				while($rs = mysql_fetch_array($result)){
					$arr[] = $rs[0];
				}
			}else{
				while($rs = mysql_fetch_assoc($result)){
					$arr[] = $rs[$target_field];
				}
			}
			return $arr;
		}elseif($case===true){
			$arr = array();
			$index = 0;
			while($rs = mysql_fetch_assoc($result)){
				if($rs[$key_index]==''){//ถ้าไม่ได้กำหนด key_index หรือกำหนดแต่ไม่ได้ select ด้วย จะได้ค่าว่าง
					$index++;//ใช้ index แบบนับไปเรื่อยๆ
				}else{//แต่ถ้ากำหนด key_index ให้ใช้ค่าจากฟิลด์นั้น
					$index = $rs[$key_index];
				}
				$arr[$index] = $rs;
			}
			return $arr;
		}else{
			return mysql_fetch_assoc($result);
		}
	}
}

if(!function_exists('run_query'))
{
	/**
	 * ใช้รันคิวรี่คำสั่ง INSERT UPDATE DELETE ที่ไม่มีการคืนค่า แต่ถ้าส่งพารามิเตอร์ตัวที่สองมาด้วย จะคืนค่าไอดีล่าสุดกรณีเป็นการ INSERT ข้อมูล
	 * @param String $sql คำสั่ง SQL ที่จะรัน
	 * @param Boolean $case ส่งค่า true เพื่อคืนค่า last_insert_id()
	 */
	function run_query($sql,$case=null){
		global $conf_db;
		mysql_query("SET NAMES $conf_db[charset]");
		mysql_query($sql) or die(show_sql_error($sql,$_SERVER['PHP_SELF']));
		if($case==true){
			return mysql_insert_id();
		}
	}
}


if( ! function_exists('where_and_condition') )
{
	/**
	 * สร้างเงื่อนไข WHERE แบบ AND ทุกตัวในอาร์เรย์ที่ส่งมา ใช้กรณีต้องการตรวจสอบข้อมูลทั้งหมด
	 * @param Array $arr_data อาร์เรย์ที่มีชื่อ คีย์เป็นฟิลด์=>ด้านขวาเป็นค่าที่จะบันทึก
	 * @return condition1 AND condition2 ....
	 */
	function where_and_condition($arr_data)
	{
		if($arr_data)
		{
			$where_and = '';
			foreach($arr_data as $field=>$value)
			{
				if($value){
					$where_and .= $sign."$field = '$value'";
					if($sign==''){ $sign = ' AND ';}
				}
			}
			return $where_and;
		}
	}
}

if(! function_exists('insert_sql')){
	/**
	 *
	 * ฟังก์ชั่นสำหรับสร้างคำสั่ง insert ทำงานภายใต้ไฟล์ config ระบบและการเชื่อมต่อฐานข้อมูลแล้ว
	 * @param String $dbname ชื่อฐานข้อมูล.ตาราง ที่ใช้
	 * @param Array $arr_data อาร์เรย์ข้อมูลที่ key=ชื่อฟิลด์ในตาราง ส่วน value = ข้อมูลที่จะบันทึก
	 * @return Sting ที่เป็นคำสั่ง SQL
	 */
	function insert_sql($dbname,$arr_data)
	{
		$key_sign = '';
		$val_sign = '';
		foreach($arr_data as $key=>$val)
		{
			$field_name .= $key_sign.$key;
			$field_value .= $val_sign.$val;
			if($key_sign==''){ $key_sign = ', ';}
			if($val_sign==''){ $val_sign = "', '";}
		}
		return  "INSERT INTO $dbname ($field_name) VALUES ('$field_value')";
	}//function
}//if

if(! function_exists('update_sql')){
	/**
	 *
	 * ฟังก์ชั่นสำหรับสร้างคำสั่ง update ทำงานภายใต้ไฟล์ config
	 * @param String $dbname ชื่อฐานข้อมูล.ตาราง ที่ใช้
	 * @param Array $arr_data อาร์เรย์ข้อมูล key=ชื่อฟิลด์ที่จะอัพเดต ส่วน value = ข้อมูลใหม่
	 * @param String $where_condition เงื่อนไขการอัพเดต
	 * @return Sting ที่เป็นคำสั่ง SQL
	 */
	function update_sql($dbname,$arr_data,$where_condition){
		if($dbname!='' && count($arr_data) && $where_condition!=''){
			$sign='';
			foreach($arr_data as $field=>$value)
			{
				$set_field .= $sign."$field='$value'";
				if($sign==''){ $sign = ', ';}
			}
			return "UPDATE $dbname SET $set_field WHERE $where_condition";
		}
	}
}

if(!function_exists('get_enum_data'))
{
	/**
	 *
	 * สร้างอาร์เรย์ข้อมูล เพื่อไปใช้สร้าง Select box
	 * @param String $table ชื่อตาราง $config[xxxx].xxxx  ตัวอย่างการเรียกใช้ $arrAttr = get_enum_data($config[DataBaseName].'.CusInt','CusInt_Rank');
	 * @param String $field ฟิลด์ที่เป็น enum หรือ set
	 * @example ตัวอย่างการเรียกใช้ $arrAttr = get_enum_data($config[DataBaseName].'.CusInt','CusInt_Rank');
	 * @return คืนค่าเป็น Array
	 */
	function get_enum_data($table,$field) {
		mysql_query("SET NAMES UTF8");
	    $result=mysql_query("SHOW COLUMNS FROM $table LIKE '$field'");
	    if(mysql_num_rows($result)>0){
	        $row=mysql_fetch_row($result);
	        $options=explode("','", preg_replace("/(enum|set)\('(.+?)'\)/","\\2", $row[1]));
	        $options2 = array();
	        foreach ($options as $value) {
	            $options2[] = array(
	                'value' => $value,
	                'display' => htmlentities($value)
	            	//'display' => $value
	            );
	        }
	        $options = $options2;
	    } else {
	        $options=array();
	    }
	    return $options;
	}//function
}


//END OF FILE