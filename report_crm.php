<?php
header('Content-type: text/html; charset=utf-8');

#region require_once 
require_once 'helper/dbquery.php';
require_once("config/general.php");
require_once("function/general.php");
require_once("function/report_crm.php");

$action  			= $_REQUEST['action'];
$dateNow 			= date('Y-m-d H:i:s');
$cur_page_html    	= "report_crm.html";
$array_check_date 	= array('','0000-00-00','00-00-0000','0000-00-00 00:00:00');	#-- ใช้สำหรับ 

/** call to connect database
*/
Conn2DB();

mysql_query( "SET NAMES UTF8" );

/** 
*	Description : ดึงลูกน้องตามสายงาน เอาเฉพาะ under ตำแหน่งที่ log in เข้าระบบ
*	$include_leader =  1 ไม่รวมคนล็อกอิน  2 รวมคนล็อกอิน
*/
$login_position_id = $_SESSION['SESSION_Position_id'];
$login_position = $_SESSION['SESSION_Position'];
$login_company_id = $_SESSION['SESSION_Working_Company'];
$login_id_card = $_SESSION['SESSION_ID_card'];
$include_leader = 2;
$yearView=(date('Y')+543);
$monthView=date('m');

/** main ajax interface 
*/
switch($action){
	case 'searchReportAll':
		$Excel = $_GET['Excel_export'];
		$startDateReport = ($_GET['y_start_search'] - 543)."-".$_GET['m_start_search']."-01";

		$endDateReport = ($_GET['y_start_search'] - 543)."-".$_GET['m_start_search']."-".date('t', mktime(0, 0, 0, $_GET['m_start_search'] , 1, $_GET['y_start_search'] ));
		$trData = "<td>0</td>
        <td>0</td>
        <td>0</td>
        <td>0</td>
        <td>0</td>
        <td>0</td>
        <td>0</td>
        <td>0</td>
        <td>0</td>";
        $whereCompany = "";
        $whereOU = "";
        if($_GET['reportType'] == 'branch'){
	        if(!empty($login_company_id)){
	        	$whereCompany = " AND position.WorkCompany = '".$login_company_id."' ";
	    	}
        }else if($_GET['reportType'] == 'ou'){
			$my_team = get_all_follow_team($login_position_id);
			$arr_id_crad = $login_id_card ;
			if(count($my_team) > 0 ){
				foreach ($my_team as $key_my_team => $value_my_team) {
					$arr_id_crad .= ",".$value_my_team;
				}
			}

			$whereCompany = " AND follow_customer.emp_id_card IN (".$arr_id_crad.") ";
			$whereOU = $whereCompany;
        }else{
        	if($_GET['company_search'] != 'all'){
        		$whereCompany = " AND position.WorkCompany = '".$_GET['company_search']."' ";
        	}
        }

       	$str_SQL ="SELECT follow_customer.emp_id_card,follow_customer.emp_posCode,position.WorkCompany,position.Department,position.Section,follow_customer.stop_date
			FROM
				$config[db_base_name].follow_customer
			INNER JOIN $config[db_organi].position ON follow_customer.emp_posCode = position.PositionCode
			WHERE
				position.Status <> '99' AND follow_customer.stop_date >= '".$startDateReport."' AND follow_customer.stop_date <= '".$endDateReport."' ".$whereCompany." 
			GROUP BY
				position.WorkCompany,
				position.Department,
				position.Section,
				follow_customer.emp_id_card,
				follow_customer.emp_posCode
			ORDER BY 
				position.WorkCompany,
				position.Department,
				position.Section,
				follow_customer.emp_posCode ";
		$result = mysql_query($str_SQL) or die(__FILE__ .'&nbsp; LINE :: '.__LINE__ .'<br>'.mysql_error().'<br>'.$str_SQL);	
		while($rs = mysql_fetch_assoc($result) ){
			$sql_Booking="SELECT Booking_No FROM $config[db_easysale].Booking WHERE pre_respond = '".$rs['emp_id_card']."'";
			$result_Booking = mysql_query($sql_Booking) or die(__FILE__ .'&nbsp; LINE :: '.__LINE__ .'<br>'.mysql_error().'<br>'.$sql_Booking);
			if(mysql_num_rows($result_Booking) > 0 ){
				$sql_emp="SELECT Be,`Name`,Surname,Nickname FROM $config[db_emp].emp_data WHERE ID_Card = '".$rs['emp_id_card']."' AND Prosonnal_Being <> '3' ";
				$result_emp = mysql_query($sql_emp) or die(__FILE__ .'&nbsp; LINE :: '.__LINE__ .'<br>'.mysql_error().'<br>'.$sql_emp);
				//if($rs['emp_id_card'] == '3571000450511'){echo $sql_emp."<br>".$result_emp;die();}
				if(mysql_num_rows($result_emp) > 0 ){
					$rs_emp = mysql_fetch_assoc($result_emp);
					$termData =  $rs;
					$emp_name = $rs_emp['Be']." ".$rs_emp['Name']." ".$rs_emp['Surname'];
			        if(!empty($rs_emp['Nickname'])){
			            $emp_name .=" ( ".$rs_emp['Nickname']." )";
			        }
					$data_Report_OU[$rs['WorkCompany']][$rs['Department']][$rs['Section']][$rs['emp_posCode']][$rs['emp_id_card']][$emp_name] = count_data_tr($termData,$whereOU,$startDateReport,$endDateReport);
					$data_Report_OU[$rs['WorkCompany']][$rs['Department']][$rs['Section']][$rs['emp_posCode']]['ALL'] = $data_Report_OU[$rs['WorkCompany']][$rs['Department']][$rs['Section']][$rs['emp_posCode']]['ALL'];
					$data_Report_OU[$rs['WorkCompany']][$rs['Department']][$rs['Section']]['ALL'] = $data_Report_OU[$rs['WorkCompany']][$rs['Department']][$rs['Section']]['ALL'];
					$data_Report_OU[$rs['WorkCompany']][$rs['Department']]['ALL'] = $data_Report_OU[$rs['WorkCompany']][$rs['Department']]['ALL'];
					$data_Report_OU[$rs['WorkCompany']]['ALL'] = $data_Report_OU[$rs['WorkCompany']]['ALL'];
					$data_Report_OU['ALL'] = $data_Report_OU['ALL'];
				}
			}
		}
		unset($result);
		unset($rs);
		unset($termData);
		$tremData_Report_OU =$data_Report_OU;
		$count_cusNo = 0;$count_event = 0;$count_wait = 0;$count_made = 0;$count_expire = 0;$count_cancel = 0;$count_waitCancel = 0;$count_all = 0;
		if($tremData_Report_OU !=''){
			foreach ($tremData_Report_OU as $workCompany => $valueWorkCompany){
				if( $workCompany!='ALL'){
					foreach ($valueWorkCompany as $department => $valueDepartment) {
						if( $department!='ALL'){
							foreach ($valueDepartment as $section => $valueSection) {
								if( $section!='ALL'){
									foreach ($valueSection as $posCode => $valuePosCode) {
										if( $posCode!='ALL'){
											foreach ($valuePosCode as $emp_id => $valueEmp_id) {
												if( $emp_id!='ALL'){
													foreach ($valueEmp_id as $emp_name => $valueEmp_name) {
														if(!empty($valueEmp_name)){
															$count_cusNo += $valueEmp_name['count_cusNo'];
															$count_event += $valueEmp_name['count_event'];
															$count_wait += $valueEmp_name['count_wait'];
															$count_made += $valueEmp_name['count_made'];
															$count_expire += $valueEmp_name['count_expire'];
															$count_cancel += $valueEmp_name['count_cancel'];
															$count_waitCancel += $valueEmp_name['count_waitCancel'];
															$count_all += $valueEmp_name['count_all'];
															
														}
													}
												
													$data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_cusNo'] += $count_cusNo;
													$data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_event'] += $count_event;
													$data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_wait'] += $count_wait;
													$data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_made'] += $count_made;
													$data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_expire'] += $count_expire;
													$data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_cancel'] += $count_cancel;
													$data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_waitCancel'] += $count_waitCancel;
													$data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_all'] += $count_all;
													
													$count_cusNo = 0;$count_event = 0;$count_wait = 0;$count_made = 0;$count_expire = 0;$count_cancel = 0;$count_waitCancel = 0;$count_all = 0;
												}
											}
											
											$data_Report_OU[$workCompany][$department][$section]['ALL']['count_cusNo'] 	+= $data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_cusNo'];
											$data_Report_OU[$workCompany][$department][$section]['ALL']['count_event'] 		+= $data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_event'];
											$data_Report_OU[$workCompany][$department][$section]['ALL']['count_wait'] 	+= $data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_wait'];
											$data_Report_OU[$workCompany][$department][$section]['ALL']['count_made'] 	+= $data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_made'];
											$data_Report_OU[$workCompany][$department][$section]['ALL']['count_expire'] 	+= $data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_expire'];
											$data_Report_OU[$workCompany][$department][$section]['ALL']['count_cancel'] 	+= $data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_cancel'];
											$data_Report_OU[$workCompany][$department][$section]['ALL']['count_waitCancel'] 	+= $data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_waitCancel'];
											$data_Report_OU[$workCompany][$department][$section]['ALL']['count_all'] 	+= $data_Report_OU[$workCompany][$department][$section][$posCode]['ALL']['count_all'];
											
										}
									}
								
									$data_Report_OU[$workCompany][$department]['ALL']['count_cusNo'] 	+= $data_Report_OU[$workCompany][$department][$section]['ALL']['count_cusNo'];
									$data_Report_OU[$workCompany][$department]['ALL']['count_event'] 		+= $data_Report_OU[$workCompany][$department][$section]['ALL']['count_event'];
									$data_Report_OU[$workCompany][$department]['ALL']['count_wait'] 	+= $data_Report_OU[$workCompany][$department][$section]['ALL']['count_wait'];
									$data_Report_OU[$workCompany][$department]['ALL']['count_made'] 	+= $data_Report_OU[$workCompany][$department][$section]['ALL']['count_made'];
									$data_Report_OU[$workCompany][$department]['ALL']['count_expire'] 	+= $data_Report_OU[$workCompany][$department][$section]['ALL']['count_expire'];
									$data_Report_OU[$workCompany][$department]['ALL']['count_cancel'] 	+= $data_Report_OU[$workCompany][$department][$section]['ALL']['count_cancel'];
									$data_Report_OU[$workCompany][$department]['ALL']['count_waitCancel'] 	+= $data_Report_OU[$workCompany][$department][$section]['ALL']['count_waitCancel'];
									$data_Report_OU[$workCompany][$department]['ALL']['count_all'] 	+= $data_Report_OU[$workCompany][$department][$section]['ALL']['count_all'];
									
								}
							}
							$data_Report_OU[$workCompany]['ALL']['count_cusNo'] 	+= $data_Report_OU[$workCompany][$department]['ALL']['count_cusNo'];
							$data_Report_OU[$workCompany]['ALL']['count_event'] 	+= $data_Report_OU[$workCompany][$department]['ALL']['count_event'];
							$data_Report_OU[$workCompany]['ALL']['count_wait'] 	+= $data_Report_OU[$workCompany][$department]['ALL']['count_wait'];
							$data_Report_OU[$workCompany]['ALL']['count_made'] 	+= $data_Report_OU[$workCompany][$department]['ALL']['count_made'];
							$data_Report_OU[$workCompany]['ALL']['count_expire'] 	+= $data_Report_OU[$workCompany][$department]['ALL']['count_expire'];
							$data_Report_OU[$workCompany]['ALL']['count_cancel'] 	+= $data_Report_OU[$workCompany][$department]['ALL']['count_cancel'];
							$data_Report_OU[$workCompany]['ALL']['count_waitCancel'] 	+= $data_Report_OU[$workCompany][$department]['ALL']['count_waitCancel'];
							$data_Report_OU[$workCompany]['ALL']['count_all'] 	+= $data_Report_OU[$workCompany][$department]['ALL']['count_all'];
							
						}
					}
					$data_Report_OU['ALL']['count_cusNo'] 	+= $data_Report_OU[$workCompany]['ALL']['count_cusNo'];
					$data_Report_OU['ALL']['count_event'] 	+= $data_Report_OU[$workCompany]['ALL']['count_event'];
					$data_Report_OU['ALL']['count_wait'] += $data_Report_OU[$workCompany]['ALL']['count_wait'];
					$data_Report_OU['ALL']['count_made'] 	+= $data_Report_OU[$workCompany]['ALL']['count_made'];
					$data_Report_OU['ALL']['count_expire'] 	+= $data_Report_OU[$workCompany]['ALL']['count_expire'];
					$data_Report_OU['ALL']['count_cancel'] 	+= $data_Report_OU[$workCompany]['ALL']['count_cancel'];
					$data_Report_OU['ALL']['count_waitCancel'] 	+= $data_Report_OU[$workCompany]['ALL']['count_waitCancel'];
					$data_Report_OU['ALL']['count_all'] 	+= $data_Report_OU[$workCompany]['ALL']['count_all'];
					
				}
			}
		}
		unset($tremData_Report_OU);
		unset($valueWorkCompany);
		unset($valueDepartment);
		unset($valueSection);
		unset($valuePosCode);
		unset($valueEmp_id);
		unset($valueEmp_name);
		if(empty($data_Report_OU)){
			echo "<tr><td colspan='15' align='center'  style='color:#ff0000;'><h1>ไม่มีรายการดังกล่าว</h1></td></tr>";
			exit();
		}
		
		foreach ($data_Report_OU as $keyWorkCompany => $valueWorkCompany) {
			if( $keyWorkCompany=='ALL'){
				$sumData = countData_all($valueWorkCompany);
			}else{
				foreach ($valueWorkCompany as $keyDepartment => $valueDepartment) {
					$id_tr_begen = "TR_".$keyWorkCompany;
					if( $keyDepartment=='ALL'){
						$trData = prospects_create_tr($valueDepartment);
						$TrKeyIntput = "TR_".$keyWorkCompany;
						$TrKeyIntputSub1 = $TrKeyIntput."_";
						$showHideKey = "Img_".$keyWorkCompany;
						if(!empty($keyWorkCompany)){
							$LV1_name = getCompany_name($keyWorkCompany);
						}else{
							$LV1_name = 'ไม่พบข้อมูลบริษัท';
						}
						$LV1_WorkCompany .= $tpl->tbHtml($cur_page_html, "LIST_OU_4_WORKINGCOM_LEVEL_1");
					
					}else{
						foreach ($valueDepartment as $keySection => $valueSection) {
							if( $keySection=='ALL'){
								$trData = prospects_create_tr($valueSection);
								$TrKeyIntput = "TR_".$keyWorkCompany."_".$keyDepartment;
								$TrKeyIntputSub2 = $TrKeyIntput."_";
								$showHideKey = "Img_".$keyWorkCompany."_".$keyDepartment;
								//$LV1_name = $valueSection['name'];
								if(!empty($keyDepartment)){
									$LV1_name = getDepartment_name($keyDepartment);
								}else{
									$LV1_name = 'ไม่พบข้อมูลแผนก';
								}
								$LV1_department .= $tpl->tbHtml($cur_page_html, "LIST_OU_4_WORKINGCOM_LEVEL_2");
							}else{
								foreach ($valueSection as $keyPositionCode => $valuePositionCode) {
									if( $keyPositionCode=='ALL'){
										$trData = prospects_create_tr($valuePositionCode);
										$TrKeyIntput = "TR_".$keyWorkCompany."_".$keyDepartment."_".$keySection;
										$TrKeyIntputSub3 = $TrKeyIntput."_";
										$showHideKey = "Img_".$keyWorkCompany."_".$keyDepartment."_".$keySection;
										//$LV1_name = $valuePositionCode['name'];
										if(!empty($keySection)){
											$LV1_name = getSection_name($keySection);
										}else{
											$LV1_name = 'ไม่พบข้อมูลฝ่าย';
										}
										$LV1_Section .= $tpl->tbHtml($cur_page_html, "LIST_OU_4_WORKINGCOM_LEVEL_3");
									}else{
										foreach ($valuePositionCode as $keyIDCard => $valueIDCard) {
											if ($keyIDCard=='ALL') {
												$trData = prospects_create_tr($valueIDCard);
												$TrKeyIntput = "TR_".$keyWorkCompany."_".$keyDepartment."_".$keySection."_".$keyPositionCode;
												$TrKeyIntputSub4 = $TrKeyIntput."_";
												$showHideKey = "Img_".$keyWorkCompany."_".$keyDepartment."_".$keySection."_".$keyPositionCode;
												//$LV1_name = $valueIDCard['name']."(".$keyPositionCode.")";
												if(!empty($keyPositionCode)){
													$positionName = getPosition_name($keyPositionCode);
													if(empty($positionName)){
														$positionName='ไม่พบข้อมูลตำแหน่ง';
													}
													$LV1_name = $positionName."(".$keyPositionCode.")";
												}else{
													$LV1_name = 'ไม่พบข้อมูลตำแหน่ง';
												}
												$LV1_PositionCode .= $tpl->tbHtml($cur_page_html, "LIST_OU_4_WORKINGCOM_LEVEL_4");
											}else{
												foreach ($valueIDCard as $keyEmpName => $valueEmpName) {
													$trData = prospects_create_tr($valueEmpName);
													$TrKeyIntput = "TR_".$keyWorkCompany."_".$keyDepartment."_".$keySection."_".$keyPositionCode."_".$keyIDCard;
													$showHideKey = "Img_".$keyWorkCompany."_".$keyDepartment."_".$keySection."_".$keyPositionCode."_".$keyIDCard;
													if(!empty($keyEmpName)){
														$LV1_name = $keyEmpName;
													}else{
														$LV1_name = 'ไม่พบข้อมูลพนักงาน';
													}
												}
												
												$LV2_IDCard .= $tpl->tbHtml($cur_page_html, "LIST_OU_4_WORKINGCOM_LEVEL_5");
											}
										}
										$LV2_Section .= $LV1_PositionCode.$LV2_IDCard;
										$LV1_PositionCode='';
										$LV2_IDCard = '';
									}
								}
								$LV2_department .= $LV1_Section.$LV2_Section;
								$LV1_Section ='';
								$LV2_Section ='';
							}

						}
						$LV1_WorkCompanySub .=$LV1_department.$LV2_department	;	
						$LV1_department='';
						$LV2_department='';
					}

					
				}
				$data_Report_Output .=$LV1_WorkCompany.$LV1_WorkCompanySub	;
				$LV1_WorkCompany='';
				$LV1_WorkCompanySub="";
			}	
		}
		$data_Report_Output .=$sumData	;
		if($Excel!=""){
			$m_y_report = $_GET['m_start_search']."_".$_GET['y_start_search'];
			header("Content-Type: application/vnd.ms-excel");
			header('Content-Disposition: attachment; filename=REPORT_CRM_'.$m_y_report.'.xls');
			$content = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">
						<HTML><HEAD><meta http-equiv="Content-type" content="text/html;charset=UTF-8" /></HEAD><BODY>';
			$list_excel=preg_replace("/<img[^>]+\>/i", "", $data_Report_Output);
			$list_excel = preg_replace('/style=(["\'])[^\1]*?\1/i', '', $list_excel);
			$content .= $tpl->tbHtml($cur_page_html, 'HEAD_EXCEL');
			$content .= '</BODY></HTML>';
			echo  $content;
		}else{
			echo $data_Report_Output;
		}

	break;

	case 'all':
		$titleReport = "รายงานการดูแลลูกค้าหลังการขาย (ALL)";
		$companyData = "<select name='company_search' id='company_search' style='width: 260px;'><option value='all'> -- เลือกบริษัททั้งหมด -- </option>";
		$sql  = "SELECT id,name FROM $config[db_organi].working_company WHERE status != 99 ORDER BY name";
		$result = mysql_query($sql) or die(__FILE__ .'&nbsp; LINE :: '.__LINE__ .'<br>'.mysql_error().'<br>'.$sql);		
		while($rs = mysql_fetch_assoc($result) ){
			$companyData  .= "<option value='".$rs['id']."'>- ".$rs['name']."</option>";		
		}
		$companyData .= "</select>";
		$monthOption = runMonth(date('m'));
		$yearOption = runYear((date('Y')+543));
		$reportType = "all";
		echo $tpl->tbHtml($cur_page_html, 'REPORT_CRM_ALL_HEADER');
	break;

	default:
		if($action == 'ou'){
			$titleReport = "รายงานการดูแลลูกค้าหลังการขาย (OU)";
			$reportType = "ou";
		}else{
			$titleReport = "รายงานการดูแลลูกค้าหลังการขาย (สาขา)";
			$reportType = "branch";
		}
		$sql  = "SELECT id,name FROM $config[db_organi].working_company WHERE id = ".$login_company_id." AND status != 99 ";
		$result = mysql_query($sql) or die(__FILE__ .'&nbsp; LINE :: '.__LINE__ .'<br>'.mysql_error().'<br>'.$sql);		
		$rs = mysql_fetch_assoc($result);
		$companyData  = "<b>".$rs['name']."</b><input type='hidden' id='company_search' value=".$rs['id'].">";
		
		$monthOption = runMonth(date('m'));
		$yearOption = runYear((date('Y')+543));
		echo $tpl->tbHtml($cur_page_html, 'REPORT_CRM_ALL_HEADER');
	break;
}

CloseDB();
?>
	
