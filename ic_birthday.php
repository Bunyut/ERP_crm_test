<?php
require_once ("config/general.php");
require_once ("function/general.php");
require_once ("function/ic_birthday.php");

//ส่วนของหัวหน้า sale , รอสัมภาษณ์ , รออนุมัติยกเลิกการติดตาม
/****************************
* css = css/followcus.css   *
* 	    css/nz_button.css	*
* js  = java/followcus.js   *
****************************/

## สั่งทำงานโค้ด
$doCode = ""; 		## ทำงาน
// $doCode = "No";  ## ไม่ทำงาน แสดงโค้ด Insert, Update, Delete
// $doCode = "All"; ## ทำงาน แสดงโค้ด Select, Insert, Update, Delete

Conn2DB();

$thisPage = "ic_birthday";

$operator = $_GET['operator'].$_POST['operator'];
if(!isset($_GET['month'])) $_GET['month'] = date('m');

switch($operator){
	case 'changeDay':
		/* [operator] => changeDay
		[month_bd] => 02
		[start_day] => 
		[stop_day] => */
		
		$return['beginDay']		= '01';
		$return['lastDay']		= cal_days_in_month(CAL_GREGORIAN, $_POST['month_bd'], 2008);
		
		$return['start']		= "<option value=''> -- เลือกวันที่ -- </option>";
		$return['stop']			= "<option value=''> -- เลือกวันที่ -- </option>";
		for($i=intval($return['beginDay']);$i<=intval($return['lastDay']);$i++){
			$dayOption			= sprintf("%02d",$i);
			$selectStart		= ($return['beginDay']==$dayOption)? "selected='selected'" : "";
			$selectStop			= ($return['lastDay']==$dayOption)? "selected='selected'" : "";
			
			$return['start']	.= "<option value='".$dayOption."' ".$selectStart.">".$dayOption."</option>";
			$return['stop']		.= "<option value='".$dayOption."' ".$selectStop.">".$dayOption."</option>";
		}
		
		echo json_encode($return);
	break;
	case 'excel':
		/* echo "<pre>";
		print_r($_SESSION['birthDayExcel']);
		echo "</pre>"; */
		
		$tagHTML = '<table>';
		$tagHTML .= '<tr style="background-color:#FFFFCC;color:#3300CC;" align="center">
						<td>วันที่เกิด</td>
						<td>หมายเลขลูกค้า</td>
						<td>คำนำหน้า</td>
						<td>ชื่อ - สกุล</td>
						<td>ที่อยู่</td>
						<td>ตำบล</td>
						<td>อำเภอ</td>
						<td>จังหวัด</td>
						<td>รหัสไปรษณีย์</td>
						<td>เกรด ลูกค้า</td>
						<td>เบอร์โทรบ้าน</td>
						<td>เบอร์โทรที่ทำงาน</td>
						<td>เบอร์โทรมือถือ</td>
						<td>ประวัติการขาย</td>
						<td>ประวัติการสนใจ(ครั้ง)</td>
						<td>ประวัติการจอง(คัน)</td>
						<td>ประวัติการขาย(คัน)</td>
					</tr>';
		foreach($_SESSION['birthDayExcel'] AS $key=>$value){
			$tagHTML .= $value;
		}
		$tagHTML .= '<table>';
		// echo htmlspecialchars($tagHTML);
		
		header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment; filename="birthDay_'.$_GET['month'].'.xls"');#ชื่อไฟล
		$html = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"
		xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv="Content-type" content="text/html;charset=UTF-8"></head><body>';
		$html .= $tagHTML;
		$html .= '</body></html>';
		echo $html;
	break;
	default:
	
		$objMain		= new manageMainDB();

		$_POST['month_bd'] = ($_GET['month_bd'])? $_GET['month_bd'] : $_POST['month_bd'] ; ## เดือน
		$vJson 			= ($_POST['vJson'])? json_decode(stripslashes($_POST['vJson'])) : '' ; ## เดือน
		
		/* echo "<pre>";
		print_r($vJson);
		echo "</pre>"; */
	
		$arr['tag']			= 'tbody#showData';
		$arr['form']		= 'form#searchDB';
		$arr['path']		= $thisPage.'.php';
		$arr['value']		= '';
		$arr['toggle']		= 'div.boxToggle';
		$arr['bLoad']		= 'div#tagData';
		$arr['run'] 		= ($vJson->run)? $vJson->run : '' ; ## สถานะ
		$arr['sDB'] 		= ($vJson->sDB)? $vJson->sDB : 0 ; ## ค่าเริ่มต้น
		$arr['nDB'] 		= ($vJson->nDB)? $vJson->nDB : 200 ; ## จำนวนที่จะต้องแสดง
		
		$arr['month'] 		= ($_POST['month_bd'])? $_POST['month_bd'] : $vJson->month ; ## เดือน
		$arr['start'] 		= ($_POST['start_day'])? $_POST['start_day'] : $vJson->start ; ## วันที่เริ่มต้น
		$arr['stop'] 		= ($_POST['stop_day'])? $_POST['stop_day'] : $vJson->stop ; ## วันที่สิ้นสุด
		$arr['month'] 		= ($arr['month'])? $arr['month'] : date('m') ; ## เดือน
		
		if($arr['run'] != 'Y'){
			$optionMonth	= get_listmonth_th(0); ## function/general.php
			$optionMonth	= '<option value=""> -- เลือกเดือน -- </option>'.$optionMonth;
			
			$optionStart	= '<option value=""> -- เลือกวันที่ -- </option>';
			$optionStop		= '<option value=""> -- เลือกวันที่ -- </option>';
		
			$arrParam		= $arr;
			$arrParam['run']= 'Y';
			$jsonParam		= json_encode($arrParam);
		
			$html			.= $tpl->tbHtml($thisPage.'.html', 'MAIN');
			$script			.= $tpl->tbHtml($thisPage.'.html', 'SCRIPT');
			
			echo $html.$script;
		}else{
			$res			= $arr;
			$res			= list_birthday($arr); ## function/ic_birthday.php
			
			echo json_encode($res);
		}
		
	break;
}
CloseDB();
?>