<?php
/** Scriptคำนวนคะแนนจากแบบฟอร์มแยกตามชนิด แล้วบันทึกลง follow ssi score
 * # - ผลลัพธ์ จะได้จากคะแนนดิบคำนวนกับค่าน้ำหนักคำถามย่อย และ น้ำหนักคำถามหลัก
 * 1. ผลลัพธ์หลังคำนวนทุกคำตอบที่ลูกค้าตอบต่อคำถามต่อแบบฟอร์ม
 * 2. ผลลัพธ์หลังคำนวนของทุกหมวดคำถามหลักของลูกค้าคนนี้ที่ตอบมาต่อแบบฟอร์ม
 * 3. ผลลัพธ์หลังคำนวนคะแนนของลูกค้าคนนี้ต่อแบบฟอร์ม
 * crete date 11-08-2011
 * by: Pete
 */

header ('Content-type: text/html; charset=utf-8');

$_SERVER['DOCUMENT_ROOT']	= dirname(dirname(dirname(__FILE__)));
require_once( $_SERVER['DOCUMENT_ROOT']. '/ERP_crm/config/connect_db.php' );
require_once( $_SERVER['DOCUMENT_ROOT'].'/ERP_crm/function/create_ssi_score.php' );

Conn2DB();

mysql_query( "SET NAMES UTF8" );
$d=date("H:i:s");

$today = date("Y-m-d");

//กำหนดช่วงเวลา
$byConfigDate 	= false;//true เฉพาะต้องการข้อมูลตามช่วงที่กำหนด ปกติ รันตาม $today
$start_date 	= '2015-04-01';
$stop_date 		= '2015-04-31';
//$debug_followCus = " AND follow_customer.id = '356233' "; // ID Follow Customer //pt@2012-01-24

$config_date = $today;
$condition	 = " LIKE '$today%' ";

if($byConfigDate==true && $start_date && $stop_date){
	$config_date = $start_date.' - '.$stop_date;
	$condition = " BETWEEN '$start_date' AND '$stop_date' ";
	if($debug_followCus){ //pt@2012-01-24
		$condition .= $debug_followCus;
	}
	$byConfigDate = true;
}else{
	$byConfigDate = false;
}

// echo "Date in progress is $config_date\n";

$sql="SELECT id AS follow_id,cus_no,process_date
	FROM $config[db_base_name].follow_customer 
	WHERE status = 2 AND DATE(process_date) $condition ";
$result1 = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
if(!mysql_num_rows($result1)){echo '<br> ***วันนี้ไม่มีข้อมูลการสอบถามลูกค้า';}

//echo "\n".mysql_num_rows($result1)."\n".$sql; exit();//TEST

//echo '<hr>';
$ssi_form_insert_num = 0;
$up_pleasure_value_num = 0;
while( $follow = mysql_fetch_assoc( $result1) ){
	//	echo '<br>CUS_NO : '.$follow[cus_no].' , FOLLOW_CUS_ID : '.$follow[follow_id];
	if($byConfigDate==true){
		$process_date = $follow['process_date'];
	}else {
		$process_date = date('Y-m-d H:i:s');
	}

	$sql = "SELECT ssi_id,ssi_form_type,ssi_form_name,start_date,stop_date, active_maincus 
	FROM $config[db_base_name].follow_ssi_form 
	WHERE (start_date <> '0000-00-00' OR start_date <= '$today')AND (stop_date = '0000-00-00' OR stop_date = '$today') 
	ORDER BY ssi_id ASC";

	$result2 = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	//echo '<br> ใช้งานอยู่ทั้งหมด '.mysql_num_rows($result2).' แบบฟอร์ม<hr><br><br>';
	while($ssi_form = mysql_fetch_assoc( $result2)){
		//echo '<br><br>แบบฟอร์มที่ : '.$ssi_form[ssi_id].' , แบบฟอร์มชนิด : '.$ssi_form[ssi_form_type];

		$arr_sum_list_a 		= array();
		$i						= 0;
		$arr_total_result 		= array();
		$status 				= 'N'; //สถานะ Insert into ssi_score
		$status_maincus 		= 'N'; //สถานะ Insert into maincus

		$cus_no 						=
		$follow_ssi_form_id 			=
		$follow_ssi_title_id 			=
		$follow_ssi_question_weight_id	=
		$follow_answer_id 				=
		$score_question_list 			=
		$score_title 					=
		$score_form 					=
		$ques_title_weight				=
		$use_status_global				=
		$arr_sum_list 					=
		$b_use_per_title 				= array();

		$count_weight = $diff_question = 0; //pt@2012-01-24  add $diff_question

		$sql = "SELECT ssi_title_id, weight AS title_weight
				FROM $config[db_base_name].follow_ssi_title 
				WHERE ssi_id ='$ssi_form[ssi_id]'";
		$result3 = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		
		
		while($ssi_title = mysql_fetch_assoc( $result3)){
			$ques_title_weight[] = $ssi_title['title_weight'];
			$ques 				=
			$ques_weight 		=
			$option_count 		=
			$question_weight_id =
			$score_title_result = array();

			$sql = "SELECT ssi_ques_id, question_list_id, weight AS question_weight , option_count
					FROM $config[db_base_name].follow_ssi_question_weight 
					WHERE ssi_title_id ='$ssi_title[ssi_title_id]' 
					ORDER BY question_list_id";
					
			$result4 = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			while($ssi_question = mysql_fetch_assoc( $result4)){
				$ques[] = $ssi_question['question_list_id']; //ได้list ของ ไอดีคำถามย่อยๆ
				$ques_weight[] = $ssi_question['question_weight']; // list น้ำหนัก ย่อย
				$option_count[] = $ssi_question['option_count'];// list ของ option_count  --->  'N','Y','Y','NNN','O' || ---> '','',''...
				$question_weight_id[] = $ssi_question['ssi_ques_id']; // เก็บไว้ยัดลง arr_insert_table
				$count_weight += 1;  //เอาไว้เชตตอนจะ Insert  เพราะจะเกิดกรณีที่ ได้คะแนน บาง title แต่ไม่ทั้งฟอร์ม จะได้ไม่บันทึก
			}//END QUESTION LIST WEIGHT

			$ssi_ = "'". implode("', '", $ques). "'";
			//	echo '<br>question list ID: '.$ssi_;

			$answer_val 	=
			$use_status		=
			$follow_ans_id 	=
			$title_w		= array();

			$use_b = 0;

			$sql = "SELECT id,answer,ques_list_id,use_status 
					FROM $config[db_base_name].follow_answer 
					WHERE cus_no = '$follow[cus_no]' AND follow_customer_id = '$follow[follow_id]' 
					AND ques_list_id IN($ssi_) 
					GROUP BY ques_list_id 
					ORDER BY ques_list_id ASC";
			
			// echo '<br><br><br>'.$sql;
			
			$result5 = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			while($ans = mysql_fetch_assoc( $result5)){
				$answer_val[] 			= $ans['answer'];
				$use_status[] 			= $ans['use_status'];
				$follow_ans_id[] 		= $ans['id']; // ใช้ตอนINSERT
				$use_status_global[]  	= $ans['use_status'];
				if($ans['use_status'] 	!= 'no'){
					$use_b += 1;
				}
			}//END FOLLOW ANSWER
			$b_use_per_title[$i] = $use_b;

			//เริ่มการคำนวน
			if(count($answer_val)==count($ques_weight)){
				if(/*is_numeric($answer_val[0]) && */$ssi_form['ssi_form_type'] == 'A'){
					$numsub[$i] = count($ques); //ใช้กำหนดรอบตอนยัดค่าน้ำหนักไตเติลเข้า$score_title
					//echo '<br>question weight = ';
					//print_r($ques_weight);
					//	echo '<br>Ans_val = ';
					//	print_r($answer_val);
					$arr_ques_result[$i] = compute_score_a($ques_weight,$answer_val,$use_status); //เรียกคำนวน ผลลัพของ น้ำหนัก ในคำถามย่อย 1.2,2.0,1.1,0.8 ...
					$arr2 = "'". implode("', '", $arr_ques_result[$i]). "'";
					//		echo ' ---- Question Score  = '.$arr2; // echoเชค ค่าคำถามย่อยหลังคำนวน
					array_push($arr_sum_list_a,array_sum($arr_ques_result[$i]));//ทำผลรวมของ ques_result ไปเรียงไว้ในอิกอาเรย์$arr_sum_list ไว้ใช้ตอนหาค่าtable  AND   form
					//$arr_title_result = array_sum(compute_point($ques_title_weight[$i],array_sum($arr_ques_result[$i]))); //คำนวนก่อนเดี่ยวๆหาค่า ผลลัพ titleมาเก็บ ในแต่ละรอบ
					//$numsub[$i] = count($ques); //ใช้กำหนดรอบตอนยัดค่าน้ำหนักไตเติลเข้า$score_title
					$score_q_list_result = $arr_ques_result[$i];//ใช้ตอน array push ในloopข้างล่างเท่านั้น

					$i++;

					for($w=0;$w<count($ques);$w++){ //เก็บไว้เตรียม Insert
						array_push($cus_no,$follow['cus_no']);
						array_push($follow_ssi_form_id,$ssi_form['ssi_id']);
						array_push($follow_ssi_title_id,$ssi_title['ssi_title_id']);
						array_push($follow_ssi_question_weight_id,$question_weight_id[$w]);
						array_push($follow_answer_id,$follow_ans_id[$w]);
						array_push($score_question_list,$score_q_list_result[$w]);
					}//for
				}//end if การคำนวนแบบ A
				else if ($ssi_form['ssi_form_type'] == 'B'){
					//echo '<br>option_count = ';
					//print_r($option_count);
					//echo '<br>answer_val = ';
					//print_r($answer_val);
					$arr_ques_result[$i] = compute_score_b($option_count,$answer_val,$use_status); //เรียกคำนวน ผลลัพของ น้ำหนัก ในคำถามย่อย 0,1,0,0
					$arr2 = "'". implode("', '", $arr_ques_result[$i]). "'";
					//	echo ' ----  Question Score  = '.$arr2; // echoเชค ค่าคำถามย่อยหลังคำนวน
					array_push($arr_sum_list,$arr_ques_result[$i]);//ทำผลลัพธ์เป็นคะแนนของtitle ไปเรียงไว้ในอิกอาเรย์$arr_sum_list
					//$arr_title_result = array_sum(compute_point($ques_title_weight[$i],array_sum($arr_ques_result[$i]))); //คำนวนก่อนเดี่ยวๆหาค่า ผลลัพ titleมาเก็บ ในแต่ละรอบ
					$numsub[$i] = count($ques); //ใช้กำหนดรอบตอนยัดค่าน้ำหนักไตเติลเข้า$score_title
					$score_q_list_result = $arr_ques_result[$i];//ใช้ตอน array push ในloopข้างล่างเท่านั้น
					$i++;
					//for($w=0;$w<$use_b;$w++){ //เก็บไว้เตรียม Insert
					for($w=0;$w<count($ques);$w++){ //เก็บไว้เตรียม Insert    //pt@2012-01-24
						array_push($cus_no,$follow['cus_no']);
						array_push($follow_ssi_form_id,$ssi_form['ssi_id']);
						array_push($follow_ssi_title_id,$ssi_title['ssi_title_id']);
						array_push($follow_ssi_question_weight_id,$question_weight_id[$w]);
						array_push($follow_answer_id,$follow_ans_id[$w]);
						array_push($score_question_list,$score_q_list_result[$w]);
					}//for
				}//if การคำนวนแบบ B
			}// END IF

		}//END TITLE
		if ($ssi_form['ssi_form_type'] == 'A'){

			$arr_total_result = compute_score_a($ques_title_weight,$arr_sum_list_a); //ส่งไปคำนวนผลลัพTITLE รอบสอง เพื่อเอามารวม
			$arr3 = "'". implode("', '", $arr_total_result). "'";
			//	echo ' ---- Title Score : '.$arr3;
			for($b=0;$b<count($arr_total_result);$b++){//เก็บไว้เตรียม Insert
				for($u=0;$u<$numsub[$b];$u++){
					array_push($score_title,$arr_total_result[$b]);
				}
			}

			//เก็บค่าคะแนนฟอร์ม
			//echo '<br>คะแนน แต่ละ title : ';
			//print_r($arr_sum_list_a);
			$arr_total_result = compute_score_a($ques_title_weight,$arr_sum_list_a); //ส่งไปคำนวนผลลัพTITLE รอบสอง เพื่อเอามารวม
			$total_score = array_sum($arr_total_result);
			//	echo '<br> Form Score : '.$total_score.' คะแนน';
			for($y=0;$y<count($cus_no);$y++){//เก็บไว้เตรียม Insert
				array_push($score_form,$total_score);
			}
		}
		else if ($ssi_form['ssi_form_type'] == 'B'){
			//echo '<br> ques_title_weight เอาไปนับ';//print_r($ques_title_weight);//echo '<br> arr_sum_list ';//print_r($arr_sum_list);
			$arr_total_result = compute_title_b($ques_title_weight,$arr_sum_list); //ส่ง 1,0 ไปคำนวน เปอร์เซน ของ title
			$arr3 = "'". implode("%', '", $arr_total_result). "%'";
			//print_r($ques_title_weight);
			//	echo ' ---- Title Score : '.$arr3; //ได้ List เป็น เปอร์เซนในแต่ละไตเติล ---> 60,90,80,...
			for($b=0;$b<count($arr_total_result);$b++){//เก็บไว้เตรียม Insert
				for($u=0;$u<$b_use_per_title[$b];$u++){
					array_push($score_title,$arr_total_result[$b]);
				}
			}
			//เก็บค่าคะแนนฟอร์ม
			$total_score = compute_form_b($ques_title_weight,$arr_sum_list,$use_status);
			//	echo '<br> Form Score : '.$total_score.'%';
			for($y=0;$y<count($cus_no);$y++){//เก็บไว้เตรียม Insert
				array_push($score_form,$total_score);
			}
		}// if form B

		//echo '<br><br>ID: '.$ssi_form['ssi_id'].', Insert Type: '.$ssi_form['ssi_form_type'].', Count Weight: '.$count_weight.', Count CusNo: '.count($cus_no).'  <br>Follow Cus: '. $follow['follow_id']. ' cusNo '.$follow['cus_no'];

		//INSERT
		if ($ssi_form['ssi_form_type'] == 'A' && $count_weight == count($cus_no)){
			for($z=0;$z<count($cus_no);$z++){ //ถ้าไม่มี CUS NO แสดงว่าไม่มีข้อมูลการคำนวน
				if($use_status_global[$z] == NULL){ //กรณีคำถามไม่ถูกถามจะไม่บันทึก 5 5 55 5
					$in_1   = $cus_no[$z];
					$in_2	= $follow_ssi_form_id[$z];
					$in_3	= $follow_ssi_title_id[$z];
					$in_4	= $follow_ssi_question_weight_id[$z];
					$in_5	= $follow_answer_id[$z];
					$in_6	= $score_question_list[$z];
					$in_7	= $score_title[$z];
					$in_8	= $score_form[$z];
					$follow_cus_id_refer = $follow['follow_id'];

					//เช็กซ้ำ
					$sql = "SELECT id_score FROM $config[db_base_name].follow_ssi_score WHERE cus_no = '$in_1' AND follow_ssi_form_id = '$in_2'
							AND follow_ssi_title_id = '$in_3' AND follow_ssi_question_weight_id = '$in_4' AND follow_answer_id = '$in_5' AND follow_cus_id_refer = '$follow_cus_id_refer' ";
					$res = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$check_score = mysql_fetch_assoc($res);
					if(empty($check_score)){//ยังไม่มีถึงจะ insert
						$sql = "INSERT INTO $config[db_base_name].follow_ssi_score (
						cus_no, follow_ssi_form_id, follow_ssi_title_id, follow_ssi_question_weight_id, follow_answer_id, score_question_list, 
						score_title, score_form, follow_cus_id_refer, process_date) 
						VALUES ( '$in_1', '$in_2', '$in_3', '$in_4', '$in_5', '$in_6', '$in_7', '$in_8', '$follow_cus_id_refer', '$process_date' )"; //pt@2012-01-24
						$logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						if($status =='N'){$status = 'Y';}
					}else{ // update record ที่มีอยูแล้ว
						$sql_upA = "UPDATE $config[db_base_name].follow_ssi_score SET score_question_list = '$in_6', score_title = '$in_7', score_form = '$in_8', process_date = '$process_date' WHERE id_score = '$check_score[id_score]'";
						$logDb->queryAndLogSQL( $sql_upA, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						if($status =='N'){$status = 'Y';}
					}
					//			echo '<br>['.$z.']   SQL: '.$sql;
				}
			}
		}else if ($ssi_form['ssi_form_type'] == 'B' && $count_weight == count($cus_no)){
			for($z=0;$z<count($cus_no);$z++){ //ถ้าไม่มี CUS NO แสดงว่าไม่มีข้อมูลการคำนวน
				$in_1   = $cus_no[$z];
				$in_2	= $follow_ssi_form_id[$z];
				$in_3	= $follow_ssi_title_id[$z];
				$in_4	= $follow_ssi_question_weight_id[$z];
				$in_5	= $follow_answer_id[$z];
				$in_6	= $score_question_list[$z];
				$in_7	= $score_title[$z];
				$in_8	= $score_form[$z];
				$follow_cus_id_refer = $follow['follow_id'];

				//เช็กซ้ำ
				$sql = "SELECT id_score FROM $config[db_base_name].follow_ssi_score WHERE cus_no = '$in_1' AND follow_ssi_form_id = '$in_2'
							AND follow_ssi_title_id = '$in_3' AND follow_ssi_question_weight_id = '$in_4' AND follow_answer_id = '$in_5' AND follow_cus_id_refer = '$follow_cus_id_refer' ";
				$res = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$check_score = mysql_fetch_assoc($res);
				if(empty($check_score)){//ยังไม่มีถึงจะ insert
					$sql = "INSERT INTO $config[db_base_name].follow_ssi_score (
					cus_no, follow_ssi_form_id, follow_ssi_title_id, follow_ssi_question_weight_id, follow_answer_id, score_question_list, 
					score_title, score_form, follow_cus_id_refer, process_date) 
					VALUES ( '$in_1', '$in_2', '$in_3', '$in_4', '$in_5', '$in_6', '$in_7', '$in_8', '$follow_cus_id_refer', '$process_date' )"; //pt@2012-01-24
					$logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					if($status =='N'){$status = 'Y';}
				}else { // update record ที่มีอยูแล้ว
					$sql_upB = "UPDATE $config[db_base_name].follow_ssi_score SET score_question_list = '$in_6', score_title = '$in_7', score_form = '$in_8', process_date = '$process_date' WHERE id_score = '$check_score[id_score]'";
					$logDb->queryAndLogSQL( $sql_upB, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					if($status =='N'){$status = 'Y';}
				}
				//		echo '<br>['.$z.']   SQL: '.$sql;
			}
		}
		if ($ssi_form['active_maincus'] == 'active' && $status == 'Y' && $score_form[0]!=''){
			$upPleasure = "UPDATE $config[Cus].MainCusData SET Pleasure_value = '$score_form[0]' WHERE CusNo = '$cus_no[0]'";
			$logDb->queryAndLogSQL( $upPleasure, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$status_maincus = 'Y';
		}
		if ($status == 'Y'){
			$ssi_form_insert_num++;
			//echo "\n********** INSERT SSI  [". $ssi_form[ssi_form_name] ."] Seccess. ";
		}
		if ($status_maincus == 'Y'){
			$up_pleasure_value_num++;
			//echo "\n********** MainCus is already updated ";
		}
		//	echo '<br>';
	}//END FORM
	//echo '<hr><hr><br>';
}//END FOLLOW CUS

// echo nl2br("\n Successfully insert ssi form value $ssi_form_insert_num form., UPDATE pleasure value $up_pleasure_value_num record.");
CloseDB();
?>