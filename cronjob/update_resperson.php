<?php
header('Content-type: text/html; charset=utf-8');

$_SERVER['DOCUMENT_ROOT']	= dirname(dirname(dirname(__FILE__)));
$config['mainPath']			= dirname(dirname(__FILE__));
require_once( $config['mainPath']. '/config/connect_db.php' );
require_once( $config['mainPath']. '/function/general.php' );
// require_once( $config['mainPath']. '/function/nuy.class.php' );

set_time_limit(0);

if(!isset($_SESSION['SESSION_ID_card'])){
	// exit();
}

## สั่งทำงานโค้ด
// $doCode = ""; 		## ทำงาน
// $doCode = "No";  ## ไม่ทำงาน แสดงโค้ด Insert, Update, Delete
// $doCode = "All"; ## ทำงาน แสดงโค้ด Select, Insert, Update, Delete

// $Limit	= "";
// $Limit	= "LIMIT 100";

Conn2DB();

$db			= new getLogRunCode();
$thisDate	= date('Y-m-d');

$opt		= 'delCancelResp'; ## ลบผู้รับผิดชอบลูกค้า

if($opt==""){
	$temp = "<table cellpadding='8' cellspacing='0'>";
		## ยกเลิกการยกเลิกกิจกรรม และผู้รับผิดชอบ
		$temp .= "<tr>";
			$temp .= "<td>";
				$temp .= "<a href='#' onclick='if(confirm(\"ยืนยันการเลือก\")){ window.location.href=\"updateData.php?opt=delCancelResp\" }' title='ยกเลิกการยกเลิกกิจกรรม และผู้รับผิดชอบ'>ยกเลิกการยกเลิกกิจกรรม และผู้รับผิดชอบ</a>";
			$temp .= "</td>";
		$temp .= "</tr>";
	$temp .= "</table>";
	
	echo $temp;
	exit();
}

$arrLog['db']			= $config['logHis'].".runCode";
$arrLog['runPath']		= " FILE : ".__FILE__." LINE : ".__LINE__."";
$arrLog['runTime']		= date('Y-m-d H:i:s');
$arrLog['operator']		= $opt;
$arrLog['status']		= "start";

$db->insertLog($arrLog);

##############################################################

function textLength($text){
	$text = stripslashes(trim($text));
	$len = strlen($text);
	
	return $len;
}

function substr_chassis($text){
	$text = trim($text);
	$len = textLength($text);
	
	if($len>8){
		$text = mb_substr($text,-8,8,'UTF-8');
	}

	return $text;
}

function substr_engine($text){
	$text = trim($text);
	$len = textLength($text);
	
	if($len>6){
		$text = mb_substr($text,-6,6,'UTF-8');
	}

	return $text;
}

##############################################################

switch($opt){
	case 'delCancelResp':
		$objDB		= new manageDb;
	
		## ลบผู้รับผิดชอบลูกค้า
		## ดึงข้อมูลการยกเลิกการติดตามลูกค้า
		$sqlCancel	= "SELECT CusNo, CANCEL_ID ";
		$sqlCancel	.= "FROM ".$config['db_base_name'].".cancel_follow_customer ";
		// $sqlCancel	.= "WHERE ";
		// $sqlCancel	.= "BU_REF = 1 ";
		// $sqlCancel	.= "AND CusNo = 5208436 ";
		$sqlCancel	.= "ORDER BY CANCEL_ID ASC";
		$queCancel 	= $logDb->queryAndLogSQL( $sqlCancel, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($feCancel = mysql_fetch_assoc($queCancel)){
			## ดึงผู้รับผิดชอบในกิจกรรมล่าสุด
			$sqlLFollow	= "SELECT cus_no, emp_id_card, biz_id_ref ";
			$sqlLFollow	.= "FROM ".$config['db_base_name'].".follow_customer ";
			$sqlLFollow	.= "WHERE cus_no LIKE '".$feCancel['CusNo']."' ";
			$sqlLFollow	.= "AND biz_id_ref LIKE '1' ";
			$sqlLFollow	.= "AND emp_id_card > 0 ";
			$sqlLFollow	.= "ORDER BY id DESC ";
			$queLFollow	= $logDb->queryAndLogSQL( $sqlLFollow, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$feLFollow	= mysql_fetch_assoc($queLFollow);
			
			if($feLFollow['cus_no'] && $feLFollow['emp_id_card']){
			
				$sqlOldCus	= "SELECT CusNo ";
				$sqlOldCus	.= "FROM ".$config['Cus'].".MainCusData ";
				$sqlOldCus	.= "WHERE CusNo = '".$feLFollow['cus_no']."' ";
				$sqlOldCus	.= "AND Resperson = '".$feLFollow['emp_id_card']."' LIMIT 1";
				$queOldCus 	= $logDb->queryAndLogSQL( $sqlOldCus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$feOldCus	= mysql_fetch_assoc($queOldCus);
			
				## ดึงผู้รับผิดชอบในกิจกรรมล่าสุด มีข้อมูลเหมือนกับใน MainCusData
				if($feOldCus['CusNo'] > 0){
					## ยกเลิกผู้รับผิดชอบลูกค้า (ตารางลูกค้าเก่า)
					$upOldCus	= "UPDATE ".$config['Cus'].".MainCusData ";
					$upOldCus	.= "SET Resperson = '', ";
					$upOldCus	.= "Resperson_posCode = '', ";
					$upOldCus	.= "respon_date = '' ";
					$upOldCus	.= "WHERE CusNo = '".$feLFollow['cus_no']."' ";
					$upOldCus	.= "AND Resperson = '".$feLFollow['emp_id_card']."' LIMIT 1";
					$logDb->queryAndLogSQL( $upOldCus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					
					## บันทึก LOG(ตารางลูกค้าใหม่)
					$sqlNewCus	= "SELECT * ";
					$sqlNewCus	.= "FROM ".$config['db_maincus'].".MAIN_RESPONSIBILITY ";
					$sqlNewCus	.= "WHERE RESP_CUSNO = '".$feLFollow['cus_no']."' ";
					$sqlNewCus	.= "AND RESP_IDCARD = '".$feLFollow['emp_id_card']."' ";
					$sqlNewCus	.= "AND BUSINESS_REFS = '1' ";
					$queNewCus 	= $logDb->queryAndLogSQL( $sqlNewCus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					
					while($feNewCus	= mysql_fetch_assoc($queNewCus)){
						$arrInsLog					= $feNewCus;
						$arrInsLog['db']			= $config['db_maincus'].".LOG_MAIN_RESPONSIBILITY";
						$arrInsLog['BU']			= 'crm';
						$arrInsLog['DO']			= '3';
						$insLog						= $objDB->insertDb($arrInsLog);
						$logDb->queryAndLogSQL( $insLog, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					}
					
					## ยกเลิกผู้รับผิดชอบลูกค้า (ตารางลูกค้าใหม่)
					$upNewCus	= "DELETE FROM ".$config['db_maincus'].".MAIN_RESPONSIBILITY ";
					$upNewCus	.= "WHERE RESP_CUSNO = '".$feLFollow['cus_no']."' ";
					$upNewCus	.= "AND RESP_IDCARD = '".$feLFollow['emp_id_card']."' ";
					$upNewCus	.= "AND BUSINESS_REFS = '".$feLFollow['biz_id_ref']."' ";
					$logDb->queryAndLogSQL( $upNewCus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					
					## ยกเลิกการยกเลิกการติดตามลูกค้า
					// $delCancelCus	= "DELETE FROM ".$config['db_base_name'].".cancel_follow_customer ";
					// $delCancelCus	.= "WHERE CANCEL_ID = '".$feCancel['CANCEL_ID']."' LIMIT 1";
					// $logDb->queryAndLogSQL( $delCancelCus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				}
			}
		}
		
		## ดึงข้อมูลการยกเลิกการติดตามลูกค้า
		$sqlCancel	= "SELECT CusNo, CANCEL_ID ";
		$sqlCancel	.= "FROM ".$config['db_base_name'].".cancel_follow_customer ";
		// $sqlCancel	.= "WHERE ";
		// $sqlCancel	.= "BU_REF = 1 ";
		// $sqlCancel	.= "AND CusNo = 5208436 ";
		$sqlCancel	.= "ORDER BY CANCEL_ID ASC";
		$queCancel 	= $logDb->queryAndLogSQL( $sqlCancel, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($feCancel = mysql_fetch_assoc($queCancel)){
			## กรณีมีการเปลี่ยนผู้รับผิดชอบลูกค้า
			$sqlOldCus	= "SELECT CusNo, Resperson ";
			$sqlOldCus	.= "FROM ".$config['Cus'].".MainCusData ";
			$sqlOldCus	.= "WHERE CusNo = '".$feCancel['CusNo']."' ";
			$sqlOldCus	.= "LIMIT 1";
			$queOldCus 	= $logDb->queryAndLogSQL( $sqlOldCus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$feOldCus	= mysql_fetch_assoc($queOldCus);
			
			## กรณีมีการเปลี่ยนผู้รับผิดชอบลูกค้า
			if($feOldCus['Resperson'] > 0){
				$delCancelCus	= "DELETE FROM ".$config['db_base_name'].".cancel_follow_customer ";
				$delCancelCus	.= "WHERE CANCEL_ID = '".$feCancel['CANCEL_ID']."' LIMIT 1";
				$logDb->queryAndLogSQL( $delCancelCus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			}else{
				$sqlFollCancel	= "SELECT idCancel, cancel_typeID ";
				$sqlFollCancel	.= "FROM ".$config['db_base_name'].".follow_customer_cancel ";
				$sqlFollCancel	.= "WHERE ";
				$sqlFollCancel	.= "cus_no = '".$feCancel['CusNo']."' ";
				$sqlFollCancel	.= "AND cancel_type = 'stopCus' ";
				$sqlFollCancel	.= "AND cancel_typeID = '6' ";
				$sqlFollCancel	.= "ORDER BY idCancel LIMIT 1";
				$queFollCancel 	= $logDb->queryAndLogSQL( $sqlFollCancel, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$feFollCancel	= mysql_fetch_assoc($queFollCancel);
				
				## ในกรณีเหตุผลในการขอยกเลิกคือ อื่นๆ
				if($feFollCancel['idCancel'] > 0 && $feFollCancel['cancel_typeID'] == 6){
					$delCancelCus	= "DELETE FROM ".$config['db_base_name'].".cancel_follow_customer ";
					$delCancelCus	.= "WHERE CANCEL_ID = '".$feCancel['CANCEL_ID']."' LIMIT 1";
					$logDb->queryAndLogSQL( $delCancelCus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				}
			}
		}
	break;
}

$arrLog['db']			= $config['logHis'].".runCode";
$arrLog['runPath']		= " FILE : ".__FILE__." LINE : ".__LINE__."";
$arrLog['runTime']		= date('Y-m-d H:i:s');
$arrLog['operator']		= $opt;
$arrLog['status']		= "stop";

$db->insertLog($arrLog);

CloseDB();
?>