<?php
header('Content-type: text/html; charset=utf-8');

$_SERVER['DOCUMENT_ROOT']	= dirname(dirname(dirname(__FILE__)));
$config['mainPath']			= dirname(dirname(__FILE__));
require_once( $config['mainPath']. '/config/connect_db.php' );
require_once( $config['mainPath']. '/function/general.php' );
// require_once( $config['mainPath']. '/function/nuy.class.php' );

set_time_limit(0);

if(!isset($_SESSION['SESSION_ID_card'])){
	// exit();
}

## สั่งทำงานโค้ด
// $doCode = ""; 		## ทำงาน
// $doCode = "No";  ## ไม่ทำงาน แสดงโค้ด Insert, Update, Delete
// $doCode = "All"; ## ทำงาน แสดงโค้ด Select, Insert, Update, Delete

// $Limit	= "";
// $Limit	= "LIMIT 100";

Conn2DB();

$db			= new getLogRunCode();
$thisDate	= date('Y-m-d');

$opt	= $_REQUEST['opt'];
// $opt	= 'delCancelResp'; ## ลบผู้รับผิดชอบลูกค้า
$opt    = 'update_emp_posCode';  ## update emp_posCode กรณีที่มีผู้ดูแลแต่ไม่มีข้อมูลอ้างอิงตำแหน่ง

if($opt==""){
	$temp = "<table cellpadding='8' cellspacing='0'>";
		## เคลียร์ข้อมูล sync ฐานข้อมูลรถ
		$temp .= "<tr>";
			$temp .= "<td>";
				$temp .= "<a href='#' onclick='if(confirm(\"ยืนยันการเลือก\")){ window.location.href=\"updateData.php?opt=clearIdRefVehicle\" }' title='เคลียร์ข้อมูล sync ฐานข้อมูลรถ'>เคลียร์ข้อมูล sync ฐานข้อมูลรถ</a>";
			$temp .= "</td>";
		$temp .= "</tr>";
		## ดึงข้อมูลที่ซ้ำกัน
		$temp .= "<tr>";
			$temp .= "<td>";
				$temp .= "<a href='#' onclick='if(confirm(\"ยืนยันการเลือก\")){ window.location.href=\"updateData.php?opt=repeatData\" }' title='ดึงข้อมูลที่ซ้ำกัน'>ดึงข้อมูลที่ซ้ำกัน</a>";
			$temp .= "</td>";
		$temp .= "</tr>";
		## ดึงข้อมูลเดิม ในของขาย มาใส่'
		$temp .= "<tr>";
			$temp .= "<td>";
				$temp .= "<a href='#' onclick='if(confirm(\"ยืนยันการเลือก\")){ window.location.href=\"updateData.php?opt=oldData\" }' title='ดึงข้อมูลเดิม ในของขาย มาใส่'>ดึงข้อมูลเดิม ในของขาย มาใส่</a>";
			$temp .= "</td>";
		$temp .= "</tr>";
		## เคลียร์ข้อมูล ใน crm
		$temp .= "<tr>";
			$temp .= "<td>";
				$temp .= "<a href='#' onclick='if(confirm(\"ยืนยันการเลือก\")){ window.location.href=\"updateData.php?opt=trunCateDb\" }' title='เคลียร์ข้อมูล ใน crm'>เคลียร์ข้อมูล ใน crm</a>";
			$temp .= "</td>";
		$temp .= "</tr>";
		## แก้ไข idRefVehicle ของ sale
		$temp .= "<tr>";
			$temp .= "<td>";
				$temp .= "<a href='#' onclick='if(confirm(\"ยืนยันการเลือก\")){ window.location.href=\"updateData.php?opt=idRefVehicleSale\" }' title='แก้ไข idRefVehicle ของ sale'>แก้ไข idRefVehicle ของ sale</a>";
			$temp .= "</td>";
		$temp .= "</tr>";
		## กำหนดให้รุ่นรถ แต่ละรุ่น มีระยะทางที่ต้องเช็คระยะเท่าไร
		$temp .= "<tr>";
			$temp .= "<td>";
				$temp .= "<a href='#' onclick='if(confirm(\"ยืนยันการเลือก\")){ window.location.href=\"updateData.php?opt=modelDistance\" }' title='กำหนดให้รุ่นรถ แต่ละรุ่น มีระยะทางที่ต้องเช็คระยะเท่าไร'>กำหนดให้รุ่นรถ แต่ละรุ่น มีระยะทางที่ต้องเช็คระยะเท่าไร</a>";
			$temp .= "</td>";
		$temp .= "</tr>";
		## ลบข้อมูล กิจกรรมเช็คระยะ เนื่องจาก คำนวณเช็คระยะผิด
		$temp .= "<tr>";
			$temp .= "<td>";
				$temp .= "<a href='#' onclick='if(confirm(\"ยืนยันการเลือก\")){ window.location.href=\"updateData.php?opt=delAppoint\" }' title='ลบข้อมูล กิจกรรมเช็คระยะ'>ลบข้อมูล กิจกรรมเช็คระยะ</a>";
			$temp .= "</td>";
		$temp .= "</tr>";
		## ลบข้อมูล ที่ 5 โทรหาทุกๆ 3 เดือน ที่สร้างผิดเนื่องจากเป็นกิจกรรมต่อเนื่อง เซตหลังจาก 100 วัน แล้ว ข้อมูลที่ + แล้ว ตรงกับช่วงนี้ ทำให้สร้างกิจกรรมก่อน ที่ต้องสร้างจริงๆ
		$temp .= "<tr>";
			$temp .= "<td>";
				$temp .= "<a href='#' onclick='if(confirm(\"ยืนยันการเลือก\")){ window.location.href=\"updateData.php?opt=delAct5\" }' title='ลบข้อมูล ที่ 5 โทรหาทุกๆ 3 เดือน ที่สร้างผิด'>ลบข้อมูล ที่ 5 โทรหาทุกๆ 3 เดือน ที่สร้างผิด</a>";
			$temp .= "</td>";
		$temp .= "</tr>";
		## ยกเลิกการยกเลิกกิจกรรม และผู้รับผิดชอบ
		$temp .= "<tr>";
			$temp .= "<td>";
				$temp .= "<a href='#' onclick='if(confirm(\"ยืนยันการเลือก\")){ window.location.href=\"updateData.php?opt=delCancelResp\" }' title='ยกเลิกการยกเลิกกิจกรรม และผู้รับผิดชอบ'>ยกเลิกการยกเลิกกิจกรรม และผู้รับผิดชอบ</a>";
			$temp .= "</td>";
		$temp .= "</tr>";
	$temp .= "</table>";
	
	echo $temp;
	exit();
}

$arrLog['db']			= $config['logHis'].".runCode";
$arrLog['runPath']		= " FILE : ".__FILE__." LINE : ".__LINE__."";
$arrLog['runTime']		= date('Y-m-d H:i:s');
$arrLog['operator']		= $opt;
$arrLog['status']		= "start";

$db->insertLog($arrLog);

##############################################################

function textLength($text){
	$text = stripslashes(trim($text));
	$len = strlen($text);
	
	return $len;
}

function substr_chassis($text){
	$text = trim($text);
	$len = textLength($text);
	
	if($len>8){
		$text = mb_substr($text,-8,8,'UTF-8');
	}

	return $text;
}

function substr_engine($text){
	$text = trim($text);
	$len = textLength($text);
	
	if($len>6){
		$text = mb_substr($text,-6,6,'UTF-8');
	}

	return $text;
}

##############################################################

switch($opt){
	case 'delAct5':
		$sql 	= "SELECT follow_customer.id 
				FROM ".$config['db_base_name'].".follow_customer 
				INNER JOIN ".$config['db_maincus'].".MAIN_RESPONSIBILITY
				ON follow_customer.cus_no = MAIN_RESPONSIBILITY.RESP_CUSNO
				WHERE follow_customer.event_id_ref IN ( 5 )
				AND follow_customer.date_value != '0000-00-00'
				AND follow_customer.status IN(0,1)
				AND MAIN_RESPONSIBILITY.BUSINESS_REFS=1
				AND follow_customer.date_value < DATE_FORMAT(DATE_ADD(MAIN_RESPONSIBILITY.RESP_DATE,INTERVAL 100 DAY),'%Y-%m-%d')";
		$que	= $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		$no = 1;
		while($fe = mysql_fetch_assoc($que)){
			echo $no.". ";
			$del = "DELETE FROM ".$config['db_base_name'].".follow_customer WHERE id='".$fe['id']."' LIMIT 1";
			$logDb->queryAndLogSQL( $del, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$no++;
		}
	break;
	case 'delAppoint':
		$idAct	= array(26,27,28);
		$idAct	= implode(',',$idAct);
		
		$sql = "SELECT * FROM ".$config['db_base_name'].".follow_customer WHERE event_id_ref IN(".$idAct.") AND status IN(0,1,90) AND `remark` LIKE '%ติดตาม%'";
		echo '<font color="#0000FF">FILE : '.__FILE__.' >> LINE : '.__LINE__.'</font><br/>'.$sql.'<br/><br/>';
		
		$del = "DELETE FROM ".$config['db_base_name'].".follow_customer WHERE event_id_ref IN(".$idAct.") AND status IN(0,1,90) AND `remark` LIKE '%ติดตาม%'";
		$logDb->queryAndLogSQL( $del, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	break;
	case 'modelDistance':
		$update = "UPDATE ".$config['db_icservice'].".`amodel` ";
		$update .= "SET `distance` = '10000' ";
		$update .= "WHERE (TRIM(`name`) LIKE 'TF%85%' OR TRIM(`name`) LIKE 'TF%86%') AND `agroup_id` IN (2,3,4) AND status != '99'";
		mysql_query($update) or die('<br/>'.$update.'<br/>ERROR '.__FILE__.' Line:'.__LINE__.'');

		$update = "UPDATE  ".$config['db_icservice'].".`amodel` SET `distance` = '5000' where `distance` != '10000' AND `agroup_id` IN (2,3,4)";
		mysql_query($update) or die('<br/>'.$update.'<br/>ERROR '.__FILE__.' Line:'.__LINE__.'');
	break;
	case 'idRefVehicleSale':
		$arr							= array();
		## Sell
		$arr['Sell']['tb']				= $config['db_easysale'].".Sell";
		$arr['Sell']['id']				= "Sell_No";
		$arr['Sell']['engine']			= "Eng_No_S";
		$arr['Sell']['chassis']			= "Chassi_No_S";
		$arr['Sell']['status']			= "";
		## Main_Vehicle
		$arr['Main_Vehicle']['tb']		= $config['db_easysale'].".Main_Vehicle";
		$arr['Main_Vehicle']['id']		= "id";
		$arr['Main_Vehicle']['engine']	= "MV_Eng_No";
		$arr['Main_Vehicle']['chassis']	= "MV_Chass_No";
		$arr['Main_Vehicle']['status']	= "status";
	
		foreach($arr AS $key=>$value){
			$sqlSell = "SELECT ".$arr[$key]['id'].", ".$arr[$key]['engine'].", ".$arr[$key]['chassis']." ";
			$sqlSell .= "FROM ".$arr[$key]['tb']." ";
			$sqlSell .= "WHERE TRIM(".$arr[$key]['engine'].")!='' ";
			$sqlSell .= "AND TRIM(".$arr[$key]['chassis'].")!='' ";
			$sqlSell .= ($arr[$key]['status'])? "AND ".$arr[$key]['status']."!=99 " : " ";
			// $sqlSell .= "LIMIT 10";
			$queSell = $logDb->queryAndLogSQL( $sqlSell, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			
			while($feSell = mysql_fetch_assoc($queSell)){
				$sqlMain = "SELECT VEHICLE_ID ";
				$sqlMain .= "FROM ".$config['db_vehicle'].".VEHICLE_INFO ";
				$sqlMain .= "WHERE VEHICLE_STATUS!=99 ";
				$sqlMain .= "AND RIGHT(TRIM(VEHICLE_ENGINES),6)='".substr_engine($feSell[$arr[$key]['engine']])."' ";
				$sqlMain .= "AND RIGHT(TRIM(VEHICLE_CHASSIS),8)='".substr_chassis($feSell[$arr[$key]['chassis']])."' ";
				$sqlMain .= "LIMIT 1";
				$queMain = $logDb->queryAndLogSQL( $sqlMain, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$feMain  = mysql_fetch_assoc($queMain);
				
				if($feMain['VEHICLE_ID']>0){
					$update = "UPDATE ".$arr[$key]['tb']." ";
					$update .= "SET idRef_vihicle='".$feMain['VEHICLE_ID']."' ";
					$update .= "WHERE ".$arr[$key]['id']."='".$feSell[$arr[$key]['id']]."' LIMIT 1";
					// echo "<font color='#FF0000'>".$update."</font><br/><br/>";
					mysql_query($update) or die('<br/>'.$update.'<br/>ERROR '.__FILE__.' Line:'.__LINE__.'');
				}
			}
		}
	break;
	case 'testSelect':
		require_once( $_SERVER['DOCUMENT_ROOT']. '/ERP_crm/inc/manageMain_data.php' );
		$objMain	= new manageMain_data('service');
	
		$sql = "SELECT id, chassis, engin_id FROM ".$config['db_icservice'].".ahistory WHERE id='17405' LIMIT 1";
		echo "<font color='blue'>FILE :: ".__FILE__." >> LINE :: ".__LINE__."</font><br/>".$sql."<br/><br/>";
		$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($fe = mysql_fetch_assoc($que)){
			$fe['chassis']	= $objMain->substr_chassis($fe['chassis']);
			$fe['engin_id']	= $objMain->substr_engine($fe['engin_id']);
		
			$sqlAhis = "SELECT id, IF(LENGTH(chassis)>8,RIGHT(chassis,8),chassis) AS chassis, IF(LENGTH(engin_id)>6,RIGHT(engin_id,6),engin_id) AS engine ";
			$sqlAhis .= "FROM ".$config['db_icservice'].".ahistory WHERE abrand_id=1 AND chassis='".$fe['chassis']."' AND engin_id='".$fe['engin_id']."' AND status!=99 LIMIT 1";
			echo "<font color='blue'>FILE :: ".__FILE__." >> LINE :: ".__LINE__."</font><br/>".$sqlAhis."<br/><br/>";
			$queAhis = $logDb->queryAndLogSQL( $sqlAhis, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$feAhis  = mysql_fetch_assoc($queAhis);
			
			if($feAhis['id']){
				echo "UPDATE";
				
				echo "<pre>";
				print_r($feAhis);
				echo "</pre>";
				/* $update = "UPDATE ".$config['db_icservice'].".ahistory SET chassis='', engin_id='' WHERE id='".$fe['id']."' LIMIT 1";
				$logDb->queryAndLogSQL( $update, " FILE : ".__FILE__." LINE : ".__LINE__."" ); */
			}else{
				echo "INSERT";
			}
		}
	break;
	case 'clearIdRefVehicle':
		$a_table  =  array();
		$array_main = array();
	
		## service
		$a_table[$config['db_icservice']][] 	= 'ahistory'; //ahistory
		$a_table[$config['db_icservice']][] 	= 'before_credit'; //before_credit
		$a_table[$config['db_icservice']][] 	= 'creditservice'; //creditservice
		$a_table[$config['db_icservice']][] 	= 'cvhc_main'; //cvhc_main
		$a_table[$config['db_icservice']][] 	= 'drepair_inform'; //drepair_inform
		$a_table[$config['db_icservice']][] 	= 'ytax'; //ytax
		$a_table[$config['db_icservice']][] 	= 'abrand'; //abrand
		$a_table[$config['db_icservice']][] 	= 'asell_company'; //ytax
		
		## sale
		$a_table[$config['db_easysale']][] 		= 'Sell'; //Sell
		$a_table[$config['db_easysale']][] 		= 'regis_car'; //regis_car
		$a_table[$config['db_easysale']][] 		= 'Main_Vehicle'; //Main_Vehicle
		
		if(!empty($a_table)){
			foreach($a_table AS $key=>$value){
				if(!empty($value)){
					foreach($value AS $keyS=>$valueS){
						$update = "UPDATE ".$key.".".$valueS." SET idRef_vihicle=0;";
						echo $update."<br/><br/>";
						mysql_query($update) or die('<br/>'.$update.'<br/>ERROR '.__FILE__.' Line:'.__LINE__.'');
					}
				}
			}
		}
	break;
	case 'repeatData':
		// $arrTb = array("regis_car"=>$config['db_easysale'].".regis_car","Sell"=>$config['db_easysale'].".Sell","ahistory"=>$config['db_icservice'].".ahistory");
		$arrTb = array("Sell"=>$config['db_easysale'].".Sell");
	
		foreach($arrTb AS $keyTb=>$valTb){
			$html = '';
		
			switch($keyTb){
				case "regis_car":
					$fieldChassis 	= 'regis_num_chessy';
					$fieldEngine 	= 'regis_num_machine';
				
					$sql = "SELECT no, regis_car_brand, ".$fieldChassis.", ".$fieldEngine.", REPLACE(regis_number,' ','') 
					FROM ".$valTb."
					WHERE status <>99
					GROUP BY
					CONCAT_WS(  '_', ".$fieldChassis.", ".$fieldEngine." ) 
					HAVING
					COUNT( CONCAT_WS(  '_', ".$fieldChassis.", ".$fieldEngine." ) ) > 1";
					
					$sqlRP = "SELECT no, regis_car_brand, ".$fieldChassis.", ".$fieldEngine.", REPLACE(regis_number,' ','') FROM ".$valTb." WHERE status <>99 AND ".$fieldChassis."='#num_chessy#' AND ".$fieldEngine."='#num_machine#' GROUP BY REPLACE(regis_number,' ','')";
				break;
				case "Sell":
					$fieldChassis 	= 'Chassi_No_S';
					$fieldEngine 	= 'Eng_No_S';
				
					$sql = "SELECT Sell_No, '' AS brand, ".$fieldChassis.", ".$fieldEngine.", REPLACE(reg_no,' ','') 
					FROM ".$valTb."
					GROUP BY
					CONCAT_WS(  '_', ".$fieldChassis.", ".$fieldEngine." ) 
					HAVING
					COUNT( CONCAT_WS(  '_', ".$fieldChassis.", ".$fieldEngine." ) ) > 1";
					
					$sqlRP = "SELECT Sell_No, '' AS brand, ".$fieldChassis.", ".$fieldEngine.", REPLACE(reg_no,' ','') FROM ".$valTb." WHERE ".$fieldChassis."='#num_chessy#' AND ".$fieldEngine."='#num_machine#' GROUP BY REPLACE(reg_no,' ','')";
				break;
				/* case "Main_Vehicle":
					$fieldChassis 	= 'MV_Chass_No';
					$fieldEngine 	= 'MV_Eng_No';
				
					$sql = "SELECT id, '' AS brand, ".$fieldChassis.", ".$fieldEngine.", '' AS register, COUNT(CONCAT_WS('_', ".$fieldChassis.", ".$fieldEngine.")) AS num 
					FROM ".$valTb."
					WHERE status <>99
					GROUP BY
					CONCAT_WS(  '_', ".$fieldChassis.", ".$fieldEngine." ) 
					HAVING
					COUNT( CONCAT_WS(  '_', ".$fieldChassis.", ".$fieldEngine." ) ) > 1";
					
					$sqlRP = "SELECT regis_number FROM ".$valTb." WHERE status <>99 AND ".$fieldChassis."='#num_chessy#' AND ".$fieldEngine."='#num_machine#' GROUP BY regis_number";
				break; */
				case "ahistory":
					$fieldChassis 	= 'chassis';
					$fieldEngine 	= 'engin_id';
				
					$sql = "SELECT id, abrand_id AS brand, ".$fieldChassis.", ".$fieldEngine.", REPLACE(register,' ','') ";
					$sql .= "FROM ".$valTb." ";
					$sql .= "WHERE status <>99 ";
					// $sql .= "WHERE status <>99 AND id IN(77256,82892) ";
					$sql .= "GROUP BY ";
					$sql .= "CONCAT_WS(  '_', ".$fieldChassis.", ".$fieldEngine." ) ";
					$sql .= "HAVING ";
					$sql .= "COUNT( CONCAT_WS(  '_', ".$fieldChassis.", ".$fieldEngine." ) ) > 1";
					
					$sqlRP = "SELECT id, abrand_id AS brand, ".$fieldChassis.", ".$fieldEngine.", REPLACE(register,' ','') FROM ".$valTb." WHERE status <>99 AND ".$fieldChassis."='#num_chessy#' AND ".$fieldEngine."='#num_machine#' GROUP BY REPLACE(register,' ','')";
				break;
			}
			$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			
			$no = 0;
			while($fe = mysql_fetch_assoc($que)){
				if($no==0){
					$html .= '<table>
								<tr>';
								
					foreach($fe AS $key=>$value){
						$html .= '<td>'.$key.'</td>';
					}
					
					$html .= '</tr>';
				}
			
				$sqlRPQ = str_replace('#num_chessy#',$fe[$fieldChassis],$sqlRP);
				$sqlRPQ = str_replace('#num_machine#',$fe[$fieldEngine],$sqlRPQ);
				$queRPQ = $logDb->queryAndLogSQL( $sqlRPQ, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$rowRPQ = mysql_num_rows($queRPQ);
				
				if($rowRPQ>1){
					while($feRPQ = mysql_fetch_assoc($queRPQ)){
						$html .= '<tr>';
									
						foreach($feRPQ AS $key=>$value){
							$html .= '<td>'.$value.'</td>';
						}
						
						$html .= '</tr>';
					}
				}
			$no++;
			}
			
			$html .= '</table>';
			
			echo $html;

			/* header("Content-Type: application/vnd.ms-excel");
			header('Content-Disposition: attachment; filename="repeat_'.date('Y-m-d').'.xls"');#ชื่อไฟล
			$excel = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"
			xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv="Content-type" content="text/html;charset=UTF-8"></head><body>';
			$excel .= $html;
			$excel .= '</body></html>';
			echo $excel; */
		}
	break;
	case 'compareAhistory':
		/* SELECT TEST_service.ahistory.id, TEST_service.ahistory.chassis, TEST_service.ahistory.engin_id, TEST_service.ahistory.register, TEST_service.ahistory.idRef_vihicle, TEST_serviceOld.ahistory.id, TEST_serviceOld.ahistory.chassis, TEST_serviceOld.ahistory.engin_id, TEST_serviceOld.ahistory.register, TEST_serviceOld.ahistory.idRef_vihicle
		FROM TEST_service.ahistory
		INNER JOIN TEST_serviceOld.ahistory ON TEST_serviceOld.ahistory.id = TEST_service.ahistory.id
		WHERE TEST_service.ahistory.chassis <> TEST_serviceOld.ahistory.chassis
		OR TEST_service.ahistory.engin_id <> TEST_serviceOld.ahistory.engin_id */
		
		/* SELECT
		TEST_service.ahistory.id,
		TEST_service.ahistory.chassis,
		TEST_service.ahistory.engin_id,
		TEST_vehicle.VEHICLE_INFO.VEHICLE_ID,
		TEST_vehicle.VEHICLE_INFO.VEHICLE_ENGINES,
		TEST_vehicle.VEHICLE_INFO.VEHICLE_CHASSIS
		FROM
		TEST_service.ahistory
		INNER JOIN TEST_vehicle.VEHICLE_INFO ON TEST_service.ahistory.idRef_vihicle = TEST_vehicle.VEHICLE_INFO.VEHICLE_ID
		WHERE
		TEST_service.ahistory.chassis <> TEST_vehicle.VEHICLE_INFO.VEHICLE_CHASSIS OR
		TEST_service.ahistory.engin_id <> TEST_vehicle.VEHICLE_INFO.VEHICLE_ENGINES */
		
		/* SELECT id, chassis, engin_id, register,COUNT(CONCAT_WS(  '_',  chassis ,  engin_id ))  
		FROM  ahistory 
		WHERE status!=99 
		GROUP BY  CONCAT_WS(  '_',  chassis ,  engin_id ) 
		HAVING COUNT(CONCAT_WS(  '_',  chassis ,  engin_id )) >1 */
	break;
	case 'empWrong_SSIreport':
		## ยังไม่นับรวมที่เป็นค่าว่าง
		$sql = "SELECT id, names_code ";
		$sql .= "FROM ".$config['db_easysale'].".SSI_Report ";
		$sql .= "WHERE `type_of_list` =3 ";
		$sql .= "AND `report_type` =4 ";
		$sql .= "AND `status` !=99 ";
		$sql .= "AND SUBSTRING_INDEX(`names_code`,'/',1) !='' ";
		$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		$noTrue		= 1;
		$noFalse	= 1;
		while($fe = mysql_fetch_assoc($que)){
			$nameCode	= "";
		
			list($empId,$split,$empName)	= explode('/',$fe['names_code']);
			$empId		= trim($empId);
			$empName	= trim($empName);
			
			## หาใน ข้อมูลพนักงาน ชื่อตรงกันหรือไม่
			$chkName						= trim(select_emp_nametonickname($empId));
			
			## ชื่อตรงกันหรือไม่
			if($empName!=$chkName){
				echo $noTrue.". <font color='red'><<".$fe['id'].">> ".$empName."!=".$chkName."</font><br/>";
				
				$nameCode	= $empId."/SPLIT/".$chkName;
				$update 	= "UPDATE ".$config['db_easysale'].".SSI_Report SET names_code='".$nameCode."' WHERE id='".$fe['id']."' LIMIT 1";
				echo "<font color='#0000FF'>".$update."</font><br/>";
				// $logDb->queryAndLogSQL( $update, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$noTrue++;
			}else{
				echo $noFalse.". <font color='green'>".$empName."=".$chkName."</font><br/>";
				$noFalse++;
			}
		}
	break;
	case 'oldData':
		$arrReplace	= array(
			1=>62,
			2=>64,
			3=>10,
			4=>13,
			5=>1,
			6=>6,
			7=>3,
			8=>4,
			9=>5,
			10=>58,
			11=>49,
			12=>50
		); ## key = new, value = old

		echo "<pre>";
		print_r($arrReplace);
		echo "</pre>";
	
		foreach($arrReplace AS $key=>$val){
			$arrLine = array();

			## follow_customer
			// $sqlFollow	= "SELECT * FROM ".$config['db_easysale'].".follow_customer WHERE stop_date<='".$thisDate."' AND event_id_ref='".$val."' AND status NOT IN(0,1) $Limit";
			$sqlFollow	= "SELECT * FROM ".$config['db_easysale'].".follow_customer WHERE event_id_ref='".$val."' AND status NOT IN(0,1,90) $Limit";
			$queFollow	= $logDb->queryAndLogSQL( $sqlFollow, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			
			while($feFollow = mysql_fetch_assoc($queFollow)){
				## ถ้ายังไม่เคยหาข้อมูล
				if(!$arrLine[$key]['event_resp_id']){
					## หาว่า เป็นข้อมูลของแผนกไหน
					$sqlLine 	= "SELECT * FROM ".$config['db_base_name'].".follow_event_responsible WHERE main_event_id='".$key."' ORDER BY event_resp_id LIMIT 1";
					$queLine 	= $logDb->queryAndLogSQL( $sqlLine, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$feLine  	= mysql_fetch_assoc($queLine);
					
					$arrLine[$key]['event_resp_id']		= $feLine['event_resp_id']; 	## ไอดี
					$arrLine[$key]['resps_comp']		= $feLine['resps_comp'];		## บริษัท
					$arrLine[$key]['resps_department']	= $feLine['resps_department'];	## แผนก
					$arrLine[$key]['resps_section']		= $feLine['resps_section'];		## ฝ่าย
					$arrLine[$key]['resps_position']	= $feLine['resps_position'];	## ตำแหน่ง
				}
				
				unset($feFollow['department_id']);
				$feFollow['db']							= $config['db_base_name'].".follow_customer";
				$feFollow['event_id_ref']				= $key;
				$feFollow['biz_id_ref']					= 1;
				$feFollow['follow_resps_comp']			= $arrLine[$key]['resps_comp'];
				$feFollow['follow_resps_department']	= $arrLine[$key]['resps_department'];
				$feFollow['follow_resps_section']		= $arrLine[$key]['resps_section'];
				$feFollow['follow_resps_position']		= $arrLine[$key]['resps_position'];
				
				## ถ้ามี chassi_no_s ให้หา idRefVehicle
				if($feFollow['chassi_no_s']){
					$sqlVehicle	= "SELECT VEHICLE_ID FROM ".$config['db_vehicle'].".VEHICLE_INFO WHERE VEHICLE_FULL_CHASSIS LIKE '%".$feFollow['chassi_no_s']."' LIMIT 1";
					$queVehicle = $logDb->queryAndLogSQL( $sqlVehicle, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$feVehicle 	= mysql_fetch_assoc($queVehicle);
					
					if($feVehicle['VEHICLE_ID']){
						$feFollow['chassi_no_s']	= $feVehicle['VEHICLE_ID'];
					}else{
						$feFollow['chassi_no_s']	= "";
					}
				}
				
				## insert follow_customer
				$insert = $db->insertDb($feFollow);
				$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			}
			
			## follow_main_question
			$sqlQues	= "SELECT * FROM ".$config['db_easysale'].".follow_main_question WHERE event_id_ref='".$val."' $Limit";
			$queQues	= $logDb->queryAndLogSQL( $sqlQues, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			
			while($feQues = mysql_fetch_assoc($queQues)){
				$inQues	= $feQues;
				
				$inQues['db']			= $config['db_base_name'].".follow_main_question";
				$inQues['event_id_ref']	= $key;

				## insert
				$insert = $db->insertDb($inQues);
				$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				
				## follow_question_list
				$sqlQList = "SELECT * FROM ".$config['db_easysale'].".follow_question_list WHERE main_question_id='".$feQues['id']."' $Limit";
				$queQList = $logDb->queryAndLogSQL( $sqlQList, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				
				while($feQList = mysql_fetch_assoc($queQList)){
					$inQList	= $feQList;
				
					$inQList['db']					= $config['db_base_name'].".follow_question_list";
					$inQList['main_question_id']	= $feQues['id'];

					## insert
					$insert = $db->insertDb($inQList);
					$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					
					$idQList			= mysql_insert_id();
				}
				
				## follow_answer
				$sqlAns	= "SELECT * FROM ".$config['db_easysale'].".follow_answer WHERE event_id_ref='".$val."' AND main_ques_id='".$feQues['id']."' $Limit";
				$queAns	= $logDb->queryAndLogSQL( $sqlAns, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				
				while($feAns = mysql_fetch_assoc($queAns)){
					$feAns['db']			= $config['db_base_name'].".follow_answer";
					$feAns['event_id_ref']	= $key;

					## insert
					$insert = $db->insertDb($feAns);
					$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				}
			}
		}
		
		## ส่วนของ D_NET ไม่เกี่ยวข้องกับส่วนอื่น
		$sqlDNet = "SELECT * FROM ".$config['db_easysale'].".D_NET $Limit";
		$queDNet = $logDb->queryAndLogSQL( $sqlDNet, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($feDNet = mysql_fetch_assoc($queDNet)){
			$feDNet['db']			= $config['db_base_name'].".D_NET";

			## insert
			$insert = $db->insertDb($feDNet);
			$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		}
		
		## ส่วนของ follow_ssi_form ไม่เกี่ยวข้องกับส่วนอื่น
		$sqlSSI = "SELECT * FROM ".$config['db_easysale'].".follow_ssi_form $Limit";
		$queSSI = $logDb->queryAndLogSQL( $sqlSSI, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($feSSI = mysql_fetch_assoc($queSSI)){
			$feSSI['db']			= $config['db_base_name'].".follow_ssi_form";

			## insert
			$insert = $db->insertDb($feSSI);
			$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		}
		
		## ส่วนของ follow_ssi_question_weight ไม่เกี่ยวข้องกับส่วนอื่น
		$sqlSSI = "SELECT * FROM ".$config['db_easysale'].".follow_ssi_question_weight $Limit";
		$queSSI = $logDb->queryAndLogSQL( $sqlSSI, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($feSSI = mysql_fetch_assoc($queSSI)){
			$feSSI['db']			= $config['db_base_name'].".follow_ssi_question_weight";

			## insert
			$insert = $db->insertDb($feSSI);
			$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		}
		
		## ส่วนของ follow_ssi_score ไม่เกี่ยวข้องกับส่วนอื่น
		$sqlSSI = "SELECT * FROM ".$config['db_easysale'].".follow_ssi_score $Limit";
		$queSSI = $logDb->queryAndLogSQL( $sqlSSI, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($feSSI = mysql_fetch_assoc($queSSI)){
			$feSSI['db']			= $config['db_base_name'].".follow_ssi_score";

			## insert
			$insert = $db->insertDb($feSSI);
			$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		}
		
		## ส่วนของ follow_ssi_title ไม่เกี่ยวข้องกับส่วนอื่น
		$sqlSSI = "SELECT * FROM ".$config['db_easysale'].".follow_ssi_title $Limit";
		$queSSI = $logDb->queryAndLogSQL( $sqlSSI, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($feSSI = mysql_fetch_assoc($queSSI)){
			$feSSI['db']			= $config['db_base_name'].".follow_ssi_title";

			## insert
			$insert = $db->insertDb($feSSI);
			$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		}
		
		/* ## ส่วนของ SSI_Report ไม่เกี่ยวข้องกับส่วนอื่น
		$sqlSSI = "SELECT * FROM ".$config['db_easysale'].".SSI_Report $Limit";
		$queSSI = $logDb->queryAndLogSQL( $sqlSSI, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($feSSI = mysql_fetch_assoc($queSSI)){
			$feSSI['db']			= $config['db_base_name'].".SSI_Report";

			## insert
			$insert = $db->insertDb($feSSI);
			$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		} */
	break;
	case 'trunCateDb':
		$tbSync	= array("follow_customer","follow_main_question","follow_question_list","follow_answer","D_NET","follow_ssi_form","follow_ssi_question_weight","follow_ssi_score","follow_ssi_title","SSI_Report");
	
		foreach($tbSync AS $key=>$value){
			$sql = "TRUNCATE TABLE ".$value;
			$logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		}
	break;
	case 'testLog':
		/* echo $sql = "INSERT INTO ERP_crm.follow_main_question ( `event_id_ref`, `question_title`, `question_detail`, `question_talk_script`, `status`)VALUES( '1', 'คำถามสำหรับถาม ทุกๆ 3 เดือนครั้ง', 'สิ่งที่จะต้องถามลูกค้าทุก ๆ 3 เดือน 1.ถามเรื่องทั่วไป หน้าที่การงานธุรกิจ 2.ถามการใช้งานรถ 3.ถามการนำรถเข้ารับบริการของศูนย์บริการ 4.ถามการบริการของศูนย์บริการ 5.แจ้งข่าวสาร 6.อัพเดทข้อมูลลูกค้า 6.1.ชื่อ, ที่อยู่, เบอร์โทรฯ ใหม่ 6.2.เหตุการณ์ต่าง ๆของลูกค้า วันแต่ง บวช งานศพ 6.3.งานที่ทำ,หน้าที่การงาน,เปลี่ยนงานหรือไม่,เลื่อนขั้น ,ขยายธุรกิจ หรือเปล่า 7.วันเวลาสถานที่ที่สะดวกให้การติดต่อ 8.ถามหาลูกค้าแนะนำ หากคะแนนความพึงพอใจถึง 9.ถามการเปลี่ยนรถหากคะแนนความพึงพอใจถึง 10.ข้อเสนอแนะของลูกค้า คำติชม 11.ข้อร้องขอ 12.ข้อร้องเรียน 13.ประเภทของการติดตาม', 'สิ่งที่ต้องพูดคุยด้วย หรือแนวทางในการเริ่มสนทนากับลูกค้า แนะนำตัว:สวัสดีครับ/ค่ะ (ชื่อพนักงาน) จากอีซูซุเชียงราย ครับ/ค่ะ เรียนสายคุณ(ชื่อลูกค้า)ใช่ไหมครับ/ค่ะ ขอเวลาลูกค้า : สะดวกคุยไหมครับ/ค่ะ ตัวอย่างสคริปคำถาม 1.รถใช้งานเป็นยังไงบ้างครับ/ค่ะ 2.ได้นำรถเข้าศูนย์บริการหรือยังครับ/ค่ะ แล้วเขาบริการเป็นยังไงบ้างครับ/ค่ะ 3.ขออัพเดทข้อมูลนะครับ/ค่ะ ชื่อ ที่อยู่เบอร์โทร ได้มีการเปลี่ยนแปลงบ้างไหมครับ/ค่ะ ช่วงนี้มีที่บ้านจะมีงานกันไหมครับ/ค่ะ อาจจะเข้าไปเยี่ยม 4.งานช่วงนี้เป็นยังไงบ้างครับ/ค่ะ ไปได้ดีไหม หากจะติดต่อสะดวกให้ติดต่อเวลานี้ได้ไหมครับ/ค่ะ 5.พอมีใครสนใจรถใหม่บ้างไหมครับ/ค่ะ 6.สนใจจะซื้อรถใหม่อีกไหมครับ/ค่ะ 7.มีข้อเสนอแนะ หรือคำติชม เพื่อที่ทางบริษัทจะได้นำไปปรับปรุงแก้ไขให้ดีขึ้นไหมครับ/ค่ะ ขอบคุณลูกค้า : ขอขอบคุณนะครับ/ค่ะที่เสียสละเวลาในครั้งนี้ หากสงสัย มีปัญหา ต้องการอะไรเกี่ยวกับรถสามารถบอกได้นะครับ/ค่ะ (เบอร์พนักงานขาย) นะครับ/ค่ะ', '');"; */
		
		$sql = "INSERT INTO ERP_crm.follow_customer ( id, follow_resps_department, follow_resps_section, follow_resps_position, event_id_ref, biz_id_ref, follow_type, chassi_no_s, cus_no, follow_resps_comp, cus_name, emp_id_card, emp_posCode, start_date, date_value, stop_date, process_by, process_by_posCode, process_date, cancel_type, remark, status, TIS_Event_Group, TIS_Event_Period, TIS_Event_PeriodCode, send_to_tis, FC_status_tis, FC_Date_upstatus, follow_db_name, source_db_table, source_primary_field, source_primary_id)VALUES('', '', '', '', '31', '1', '2', '52430', '5243122', '44', 'บริษัท ลานนาไทยขนส่ง จำกัด โดย นายนิรันดร์ ศรีผดุงพร', '', '', '2013-05-01', '2013-05-08', '2013-05-31', '', '', '', '', '', '0', '', '', '', 'no', '', '', 'db_easysale', 'Sell','Sell_No', '12915')";
		$logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		/* ## follow_main_question
		$sqlQues	= "SELECT * FROM ".$config['db_easysale'].".follow_main_question WHERE event_id_ref='1' AND status!='99' $Limit";
		$queQues	= $logDb->queryAndLogSQL( $sqlQues, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($feQues = mysql_fetch_assoc($queQues)){
			$inQues	= $feQues;
			unset($inQues['id']);
			
			$inQues['db']			= $config['db_base_name'].".follow_main_question";
			$inQues['event_id_ref']	= $key;

			## insert
			echo $sql = $db->insertDb($inQues);
		}
		$sql		= trim(preg_replace('/\s\s+/', ' ', $sql));
		
		$arr 		= array();
			
		$patUpdate 	= "/^(INSERT INTO (?<dbInsert>.*)[\(]{1}(?<field>.*)[\)]{1}([\s]?VALUES[\s]?)[\(]{1}(?<value>.*)[\)]{1})|";
		$patUpdate .= "((UPDATE(?<dbUpdate>.*)SET(?<content>.*))|(DELETE FROM (?<dbDel>.*)))WHERE(?<where>.*)/";
		
		if(preg_match_all( $patUpdate, $sql, $match )){
			if($match['dbInsert'][0]){
				list($db,$tb) = explode('.',trim($match['dbInsert'][0]));
				if(!$tb){
					$tb = $db;
					$db = '';
				}
				
				$arr['db'] 		= $db;
				$arr['tb'] 		= $tb;
				$arr['do'] 		= 'insert';
				$arr['field']	= trim($match['field'][0]);
				$arr['value']	= trim($match['value'][0]);
			}else if($match['dbUpdate'][0]){
				list($db,$tb) = explode('.',trim($match['dbUpdate'][0]));
				if(!$tb){
					$tb = $db;
					$db = '';
				}
			
				$arr['db'] 		= $db;
				$arr['tb'] 		= $tb;
				$arr['do'] 		= 'update';
				$arr['field']	= array();
				$arr['value']	= array();
			}else if($match['dbDel'][0]){
				list($db,$tb) = explode('.',trim($match['dbDel'][0]));
				if(!$tb){
					$tb = $db;
					$db = '';
				}
			
				$arr['db'] = $db;
				$arr['tb'] = $tb;
				$arr['do'] = 'del';
				$arr['field']	= array();
				$arr['value']	= array();
			}
			
			$arr['where'] = $match['where'][0];
		}
		
		echo "<pre>";
		print_r($arr);
		echo "</pre>"; */
	break;

	case 'getDataSellCar_45_54' :

		/*
			CREATE TABLE ERP_crm.1A_sellcar_45_54 AS SELECT *, '2013-08-31' AS Date_Deliver_set FROM  ERP_icmba.Sell  WHERE DATE_FORMAT(STR_TO_DATE(Sell.Date_Deliver,'%d-%m-%Y'), '%Y-%m-%d') BETWEEN '2002-01-01' AND '2011-12-31'  GROUP BY SUBSTRING_INDEX(Sell.cusBuy,'_',1)
		*/

		$sql  = " CREATE TABLE ERP_crm.1A_sellcar_45_54 AS ";
		$sql .= " SELECT *, '2013-08-31' AS Date_Deliver_set FROM  ERP_icmba.Sell ";
		$sql .= " WHERE DATE_FORMAT(STR_TO_DATE(Sell.Date_Deliver,'%d-%m-%Y'), '%Y-%m-%d') BETWEEN '2002-01-01' AND '2011-12-31' ";
		$sql .= " GROUP BY SUBSTRING_INDEX(Sell.cusBuy,'_',1) ";

		echo "<font color='#0000FF'>FILE :: ".__FILE__." >> LINE :: ".__LINE__."</font><br/>".$sql."<br/><br/>";
		mysql_query($sql) or die("<br/>".$sql."<br/>FILE : ".__FILE__." , LINE : ".__LINE__." , ERROR : ".mysql_error());

	break;

	case 'updateDataSellCar_45_54' :

		#--- เวียงป่าเป้า ---#

		$sql = '';
		$re  = '';
		$sql = " SELECT  MAIN_ADDRESS.ADDR_CUS_NO FROM  ERP_new_cusdata.MAIN_ADDRESS ";
		$sql .= " WHERE ";
		$sql .= " MAIN_ADDRESS.ADDR_DISTRICT LIKE '667%' AND MAIN_ADDRESS.ADDR_TYPE = '2' ";
		$sql .= " AND MAIN_ADDRESS.ADDR_PROVINCE LIKE '45%' ";
		$re = mysql_query($sql) or die("<br/>".$sql."<br/>FILE : ".__FILE__." , LINE : ".__LINE__." , ERROR : ".mysql_error());
		$cusNo = '';
		while( $rows = mysql_fetch_assoc($re)){
			$cusNo .= $rows['ADDR_CUS_NO'].',';
		}
		$cusNo = substr($cusNo, 0, -1);
		$sql_update = " UPDATE ERP_crm.follow_customer SET follow_customer.follow_resps_department = '', follow_customer.follow_resps_section = '179' ";
		$sql_update .= " WHERE follow_customer.cus_no IN(".$cusNo.") AND follow_customer.event_id_ref = '35' ";

		echo "<font color='#0000FF'>FILE :: ".__FILE__." >> LINE :: ".__LINE__."</font><br/>".$sql_update."<br/><br/>";
		mysql_query($sql_update) or die("<br/>".$sql_update."<br/>FILE : ".__FILE__." , LINE : ".__LINE__." , ERROR : ".mysql_error());
		echo " Effect rows -> ".mysql_affected_rows()."<br><br>";

		#--- เชียงของ ---#

		$sql = '';
		$re  = '';
		$sql = " SELECT  MAIN_ADDRESS.ADDR_CUS_NO FROM  ERP_new_cusdata.MAIN_ADDRESS ";
		$sql .= " WHERE ((MAIN_ADDRESS.ADDR_DISTRICT LIKE '659%' AND MAIN_ADDRESS.ADDR_TYPE = '2') OR (MAIN_ADDRESS.ADDR_DISTRICT LIKE '669%' AND MAIN_ADDRESS.ADDR_TYPE = '2')) AND MAIN_ADDRESS.ADDR_PROVINCE LIKE '45%' ";
		$re = mysql_query($sql) or die("<br/>".$sql."<br/>FILE : ".__FILE__." , LINE : ".__LINE__." , ERROR : ".mysql_error());
		$cusNo = '';
		while( $rows = mysql_fetch_assoc($re)){
			$cusNo .= $rows['ADDR_CUS_NO'].',';
		}
		$cusNo = substr($cusNo, 0, -1);
		$sql_update = " UPDATE ERP_crm.follow_customer SET follow_customer.follow_resps_department = '', follow_customer.follow_resps_section = '158' ";
		$sql_update .= " WHERE follow_customer.cus_no IN(".$cusNo.") AND follow_customer.event_id_ref = '35' ";

		echo "<font color='#0000FF'>FILE :: ".__FILE__." >> LINE :: ".__LINE__."</font><br/>".$sql_update."<br/><br/>";
		mysql_query($sql_update) or die("<br/>".$sql_update."<br/>FILE : ".__FILE__." , LINE : ".__LINE__." , ERROR : ".mysql_error());
		echo " Effect rows -> ".mysql_affected_rows()."<br><br>";

		#--- เทิง ---#

		$sql = '';
		$re  = '';
		$sql = " SELECT MAIN_ADDRESS.ADDR_CUS_NO FROM  ERP_new_cusdata.MAIN_ADDRESS ";
		$sql .= " WHERE ((MAIN_ADDRESS.ADDR_DISTRICT LIKE '660%' AND MAIN_ADDRESS.ADDR_TYPE = '2') OR (MAIN_ADDRESS.ADDR_DISTRICT LIKE '670%' AND MAIN_ADDRESS.ADDR_TYPE = '2') OR (MAIN_ADDRESS.ADDR_DISTRICT LIKE '668%' AND MAIN_ADDRESS.ADDR_TYPE = '2')) AND MAIN_ADDRESS.ADDR_PROVINCE LIKE '45%' ";
		$re = mysql_query($sql) or die("<br/>".$sql."<br/>FILE : ".__FILE__." , LINE : ".__LINE__." , ERROR : ".mysql_error());
		$cusNo = '';
		while( $rows = mysql_fetch_assoc($re)){
			$cusNo .= $rows['ADDR_CUS_NO'].',';
		}
		$cusNo = substr($cusNo, 0, -1);
		$sql_update = " UPDATE ERP_crm.follow_customer SET follow_customer.follow_resps_department = '', follow_customer.follow_resps_section = '177' ";
		$sql_update .= " WHERE follow_customer.cus_no IN(".$cusNo.") AND follow_customer.event_id_ref = '35' ";

		echo "<font color='#0000FF'>FILE :: ".__FILE__." >> LINE :: ".__LINE__."</font><br/>".$sql_update."<br/><br/>";
		mysql_query($sql_update) or die("<br/>".$sql_update."<br/>FILE : ".__FILE__." , LINE : ".__LINE__." , ERROR : ".mysql_error());
		echo " Effect rows -> ".mysql_affected_rows()."<br><br>";
		
	break;
	
	case 'delCancelResp':
		$objDB		= new manageDb;
	
		## ดึงข้อมูลการยกเลิกการติดตามลูกค้า
		$sqlCancel	= "SELECT * ";
		$sqlCancel	.= "FROM ".$config['db_base_name'].".cancel_follow_customer ";
		// $sqlCancel	.= "WHERE ";
		// $sqlCancel	.= "BU_REF = 1 ";
		// $sqlCancel	.= "AND CusNo = 5208436 ";
		$sqlCancel	.= "ORDER BY CANCEL_ID ASC";
		$queCancel 	= $logDb->queryAndLogSQL( $sqlCancel, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($feCancel = mysql_fetch_assoc($queCancel)){
			## ดึงผู้รับผิดชอบในกิจกรรมล่าสุด
			$sqlLFollow	= "SELECT cus_no, emp_id_card, biz_id_ref ";
			$sqlLFollow	.= "FROM ".$config['db_base_name'].".follow_customer ";
			$sqlLFollow	.= "WHERE cus_no LIKE '".$feCancel['CusNo']."' ";
			$sqlLFollow	.= "AND biz_id_ref LIKE '1' ";
			$sqlLFollow	.= "AND emp_id_card > 0 ";
			$sqlLFollow	.= "ORDER BY id DESC ";
			$queLFollow	= $logDb->queryAndLogSQL( $sqlLFollow, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$feLFollow	= mysql_fetch_assoc($queLFollow);
			
			if($feLFollow['cus_no'] && $feLFollow['emp_id_card']){
			
				$sqlOldCus	= "SELECT CusNo ";
				$sqlOldCus	.= "FROM ".$config['Cus'].".MainCusData ";
				$sqlOldCus	.= "WHERE CusNo = '".$feLFollow['cus_no']."' ";
				$sqlOldCus	.= "AND Resperson = '".$feLFollow['emp_id_card']."' LIMIT 1";
				$queOldCus 	= $logDb->queryAndLogSQL( $sqlOldCus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$feOldCus	= mysql_fetch_assoc($queOldCus);
			
				## ดึงผู้รับผิดชอบในกิจกรรมล่าสุด มีข้อมูลเหมือนกับใน MainCusData
				if($feOldCus['CusNo'] > 0){
					## ยกเลิกผู้รับผิดชอบลูกค้า (ตารางลูกค้าเก่า)
					$upOldCus	= "UPDATE ".$config['Cus'].".MainCusData ";
					$upOldCus	.= "SET Resperson = '', ";
					$upOldCus	.= "Resperson_posCode = '', ";
					$upOldCus	.= "respon_date = '' ";
					$upOldCus	.= "WHERE CusNo = '".$feLFollow['cus_no']."' ";
					$upOldCus	.= "AND Resperson = '".$feLFollow['emp_id_card']."' LIMIT 1";
					$logDb->queryAndLogSQL( $upOldCus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					
					## บันทึก LOG(ตารางลูกค้าใหม่)
					$sqlNewCus	= "SELECT * ";
					$sqlNewCus	.= "FROM ".$config['db_maincus'].".MAIN_RESPONSIBILITY ";
					$sqlNewCus	.= "WHERE RESP_CUSNO = '".$feLFollow['cus_no']."' ";
					$sqlNewCus	.= "AND RESP_IDCARD = '".$feLFollow['emp_id_card']."' ";
					$sqlNewCus	.= "AND BUSINESS_REFS = '1' ";
					$queNewCus 	= $logDb->queryAndLogSQL( $sqlNewCus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					
					while($feNewCus	= mysql_fetch_assoc($queNewCus)){
						$arrInsLog					= $feNewCus;
						$arrInsLog['db']			= $config['db_maincus'].".LOG_MAIN_RESPONSIBILITY";
						$arrInsLog['BU']			= 'crm';
						$arrInsLog['DO']			= '3';
						$insLog						= $objDB->insertDb($arrInsLog);
						$logDb->queryAndLogSQL( $insLog, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					}
					
					## ยกเลิกผู้รับผิดชอบลูกค้า (ตารางลูกค้าใหม่)
					$upNewCus	= "DELETE FROM ".$config['db_maincus'].".MAIN_RESPONSIBILITY ";
					$upNewCus	.= "WHERE RESP_CUSNO = '".$feLFollow['cus_no']."' ";
					$upNewCus	.= "AND RESP_IDCARD = '".$feLFollow['emp_id_card']."' ";
					$upNewCus	.= "AND BUSINESS_REFS = '".$feLFollow['biz_id_ref']."' ";
					$logDb->queryAndLogSQL( $upNewCus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					
					## ยกเลิกการยกเลิกการติดตามลูกค้า
					// $delCancelCus	= "DELETE FROM ".$config['db_base_name'].".cancel_follow_customer ";
					// $delCancelCus	.= "WHERE CANCEL_ID = '".$feCancel['CANCEL_ID']."' LIMIT 1";
					// $logDb->queryAndLogSQL( $delCancelCus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				}
			}
		}
		
		## ดึงข้อมูลการยกเลิกการติดตามลูกค้า
		$sqlCancel	= "SELECT * ";
		$sqlCancel	.= "FROM ".$config['db_base_name'].".cancel_follow_customer ";
		// $sqlCancel	.= "WHERE ";
		// $sqlCancel	.= "BU_REF = 1 ";
		// $sqlCancel	.= "AND CusNo = 5208436 ";
		$sqlCancel	.= "ORDER BY CANCEL_ID ASC";
		$queCancel 	= $logDb->queryAndLogSQL( $sqlCancel, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		while($feCancel = mysql_fetch_assoc($queCancel)){
			$sqlOldCus	= "SELECT CusNo, Resperson ";
			$sqlOldCus	.= "FROM ".$config['Cus'].".MainCusData ";
			$sqlOldCus	.= "WHERE CusNo = '".$feCancel['CusNo']."' ";
			$sqlOldCus	.= "LIMIT 1";
			$queOldCus 	= $logDb->queryAndLogSQL( $sqlOldCus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$feOldCus	= mysql_fetch_assoc($queOldCus);
			
			if($feOldCus['Resperson'] > 0){
				$delCancelCus	= "DELETE FROM ".$config['db_base_name'].".cancel_follow_customer ";
				$delCancelCus	.= "WHERE CANCEL_ID = '".$feCancel['CANCEL_ID']."' LIMIT 1";
				$logDb->queryAndLogSQL( $delCancelCus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			}
		}
	break;
	## update emp_posCode กรณีที่มีผู้ดูแลแต่ไม่มีข้อมูลอ้างอิงตำแหน่ง
	case 'update_emp_posCode':
	  	$result 	= array();
	  	$count  	= 0;
	  	$true_rows  = 0;
	  	$listDetail = '';
	    $sql  	=  "SELECT follow_customer.id,follow_customer.event_id_ref,follow_customer.cus_no,follow_customer.cus_name,
					    follow_customer.emp_id_card,
					    follow_main_event.pattern_id_ref 		 AS pattern_ref,
					    follow_main_event.follow_up_activities 	 AS active,
					    follow_main_event.field_name_specificate AS id_card_main,
					    follow_main_event.field_code_specificate AS poscode_main
	    		    FROM ".$config['db_base_name'].".follow_customer ";
	    $sql   .= "LEFT JOIN ".$config['db_base_name'].".follow_main_event ON follow_customer.event_id_ref = follow_main_event.id ";
	    $sql   .= "WHERE follow_customer.biz_id_ref = '1' AND follow_customer.emp_id_card != '' AND follow_customer.emp_posCode = ''";
	    $que  	= $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while($fe = mysql_fetch_assoc($que))
		{

			if($fe['active'] == '1') // ผู้รับผิดชอบลูกค้า
			{
				$sql_main  = "SELECT RESP_ID,POS_EMP_CODE,RESP_IDCARD FROM ".$config['db_maincus'].".MAIN_RESPONSIBILITY ";
				$sql_main .= "WHERE RESP_CUSNO = '".$fe['cus_no']."' AND BUSINESS_REFS = '1' AND POS_EMP_CODE !='' ";
		    	$que_main  = $logDb->queryAndLogSQL( $sql_main, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		    	$re_main   = mysql_fetch_assoc($que_main);
		    	if($re_main)
		    	{
		    		if($re_main['RESP_IDCARD'] == $fe['emp_id_card'])
		    		{
		    			## update emp_posCode ในตาราง follow_customer
						$up_posCode	 = "UPDATE ".$config['db_base_name'].".follow_customer ";
						$up_posCode	.= "SET emp_posCode = '".$re_main['POS_EMP_CODE']."' ";
						$up_posCode	.= "WHERE id = '".$fe['id']."' LIMIT 1";
						$logDb->queryAndLogSQL( $up_posCode, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		    		}else{
		    			$result['data'][] = $fe;
		    			$count++;
		    			$listDetail .= '<tr>
							<td align="center" scope="col" style="border:1px solid #424242; vertical-align: middle;">'.$count.'</td>
							<td align="center" scope="col" style="border:1px solid #424242; vertical-align: middle;">'.$fe['id'].'</td>
							<td align="center" scope="col" style="border:1px solid #424242; vertical-align: middle;">'.$fe['event_id_ref'].'</td>
							<td align="center" scope="col" style="border:1px solid #424242; vertical-align: middle;">'.$fe['cus_no'].'</td>
							<td align="center" scope="col" style="border:1px solid #424242; vertical-align: middle;">'.$fe['cus_name'].'</td>
							<td align="center" scope="col" style="border:1px solid #424242; vertical-align: middle;">'.$fe['emp_id_card'].'</td>
							<td align="center" scope="col" style="border:1px solid #424242; vertical-align: middle;">'.$re_main['RESP_IDCARD'].'</td>
						</tr>';
		    		}
		    	}
		    	$true_rows++;
		    }else if($fe['active'] == '3' && $fe['id_card_main'] != '' && $fe['poscode_main'] != '') // อื่นๆ
		    {
		    	$sql_pattern = "SELECT * FROM ".$config['db_base_name'].".follow_field_pattern WHERE id = '".$fe['pattern_ref']."' AND status  != '99'";
		    	$que_pattern = $logDb->queryAndLogSQL( $sql_pattern, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		    	$re_pattern  = mysql_fetch_assoc($que_pattern);
		    	if($re_pattern)
		    	{
			    	$sql_target  = "SELECT * FROM ".$config[$re_pattern['db_name']].".".$re_pattern['table_name']." ";
		    		$sql_target .= "WHERE ".$re_pattern['field_cus_name']." = '".$fe['cus_no']."' AND ".$fe['poscode_main']." != '' ";
			    	$sql_target .= "ORDER BY '".$fe['field_primary']."' DESC LIMIT 1";
			    	$que_target  = $logDb->queryAndLogSQL( $sql_target, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			    	$re_target   = mysql_fetch_assoc($que_target);
			    	if($re_target)
			    	{
			    		if($re_target[$fe['id_card_main']] == $fe['emp_id_card'])
			    		{
			    			## update emp_posCode ในตาราง follow_customer
							$up_posCode	 = "UPDATE ".$config['db_base_name'].".follow_customer ";
							$up_posCode	.= "SET emp_posCode = '".$re_target[$fe['poscode_main']]."' ";
							$up_posCode	.= "WHERE id = '".$fe['id']."' LIMIT 1";
							$logDb->queryAndLogSQL( $up_posCode, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			    		}else{
			    			$result['data'][] = $fe;
			    			$count++;
			    			$listDetail .= '<tr>
								<td align="center" scope="col" style="border:1px solid #424242; vertical-align: middle;">'.$count.'</td>
								<td align="center" scope="col" style="border:1px solid #424242; vertical-align: middle;">'.$fe['id'].'</td>
								<td align="center" scope="col" style="border:1px solid #424242; vertical-align: middle;">'.$fe['event_id_ref'].'</td>
								<td align="center" scope="col" style="border:1px solid #424242; vertical-align: middle;">'.$fe['cus_no'].'</td>
								<td align="center" scope="col" style="border:1px solid #424242; vertical-align: middle;">'.$fe['cus_name'].'</td>
								<td align="center" scope="col" style="border:1px solid #424242; vertical-align: middle;">'.$fe['emp_id_card'].'</td>
								<td align="center" scope="col" style="border:1px solid #424242; vertical-align: middle;">'.$re_target[$fe['id_card_main']].'</td>
							</tr>';
			    		}
			    	}	
		    	}	
		    	$true_rows++;
		    }
		}
		if($listDetail != '')
		{
			$html = ' <table width="100%">
					<tr>
						<td align="center" width="5%" style="border:1px solid #424242;"><b>ลำดับ</b></td>
						<td align="center" width="20%" style="border:1px solid #424242;"><b>follow id</b></td>
						<td align="center" width="20%" style="border:1px solid #424242;"><b>event_id_ref</b></td>
						<td align="center" width="20%" style="border:1px solid #424242;"><b>cus_no</b></td>
						<td align="center" width="20%" style="border:1px solid #424242;"><b>cus_name</b></td>
						<td align="center" width="20%" style="border:1px solid #424242;"><b>emp_id_card</b></td>
						<td align="center" width="20%" style="border:1px solid #424242;"><b>target_id_card</b></td>
					</tr>
					'.$listDetail.'
				</table>';
			header("Content-Type: application/vnd.ms-excel");
			header('Content-Disposition: attachment; filename=ดึงข้อมูลการจ่ายเงินย้อนหลัง.xls'); # -- ชื่อไฟล
			$content = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">
						<HTML><HEAD><meta http-equiv="Content-type" content="text/html;charset=UTF-8" /></HEAD><BODY>';
			$content .= $html;
			$content .= '</BODY></HTML>';
			echo  $content;
		}
		$result['true_rows'] = $true_rows;
		$result['count'] 	 = $count;
		echo '<pre>';print_r($result);echo'</pre>';
	break;
}

$arrLog['db']			= $config['logHis'].".runCode";
$arrLog['runPath']		= " FILE : ".__FILE__." LINE : ".__LINE__."";
$arrLog['runTime']		= date('Y-m-d H:i:s');
$arrLog['operator']		= $opt;
$arrLog['status']		= "stop";

$db->insertLog($arrLog);

CloseDB();
?>