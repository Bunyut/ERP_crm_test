<?php

set_time_limit(0);
ini_set('memory_limit', '-1');

header('Content-Type: text/html; charset=utf-8');

define('ROOT_PATH', dirname(dirname(dirname(__FILE__))));
define('CRON_PATH', dirname(dirname(__FILE__)));

$mainFunc = ROOT_PATH.'/ERP_masterdata/inc/main_function.php';
if (!file_exists($mainFunc)) {
	$mainFunc = CRON_PATH.'/ERP_masterdata/inc/main_function.php';
}

require_once $mainFunc;

$configMain = new configAllDatabase();
$configMain->calConfigAllDatabase();
$config = $configMain->con;

$db = new mysqli($config['ServerName'], $config['UserName'], $config['UserPassword']);
if ($db->connect_error) {
	die ($db->connect_error." FILE: ".__FILE__." LINE: ".__LINE__);
}
$db->query("SET NAMES UTF8");

################################## [FUNCTION] ##################################

function findCusNo($cus_id) {
	global $db, $config;

	$sql_cus = "SELECT customer.cus_id FROM ".$config['db_service'].".customer WHERE customer.id = '".$cus_id."' AND customer.status <> '99' ";
	$que_cus = $db->query($sql_cus);
	if (!$que_cus) {
		die ($db->error." FILE: ".__FILE__." LINE: ".__LINE__);
	}
	$fet_cus = $que_cus->fetch_assoc();
	return $fet_cus['cus_id'];
}

function checkExistInfo($idRef_vihicle) {
	global $db, $config;

	$sql = "SELECT VEHICLE_INFO.Date_Deliver FROM ".$config['db_vehicle'].".VEHICLE_INFO WHERE VEHICLE_INFO.VEHICLE_STATUS <> '99' AND VEHICLE_INFO.VEHICLE_ID = '".$idRef_vihicle."' ";
	$que = $db->query($sql);
	if (!$que) {
		die ($db->error." FILE: ".__FILE__." LINE: ".__LINE__);
	}	
	if ($que->num_rows > 0) {
		$fet = $que->fetch_assoc();
		return $fet['Date_Deliver'];
	}else {
		return false;
	}
}

function updateDateDeliverInfo($idRef_vihicle, $deliver_day) {
	global $db, $config;

	$sql = "UPDATE ".$config['db_vehicle'].".VEHICLE_INFO SET VEHICLE_INFO.Date_Deliver = '".$deliver_day."' WHERE VEHICLE_INFO.VEHICLE_STATUS <> '99' AND VEHICLE_INFO.VEHICLE_ID = '".$idRef_vihicle."' ";
	if (!$db->query($sql)) {
		die ($db->error." FILE: ".__FILE__." LINE: ".__LINE__);
	}else {
		return true;
	}
}

function getSellCompInfo($id_sellcomp) {
	global $db, $config;

	$sql_sellcomp = "SELECT VEHICLE_COMPANY.SELLER_COMP_ID FROM ".$config['db_service'].".asell_company INNER JOIN ".$config['db_vehicle'].".VEHICLE_COMPANY ON VEHICLE_COMPANY.SELLER_COMP_ID = asell_company.idRef_vihicle WHERE asell_company.id = '".$id_sellcomp."' AND asell_company.status <> '99' AND VEHICLE_COMPANY.SELLER_COMP_STATUS <> '99' ";

	$que_sellcomp = $db->query($sql_sellcomp);
	if (!$que_sellcomp) {
		die ($db->error." FILE: ".__FILE__." LINE: ".__LINE__);
	}
	$fet_sellcomp = $que_sellcomp->fetch_assoc();
	return $fet_sellcomp['SELLER_COMP_ID'];
}

function getTextCusType($cus_type, $reverse = false) {

	switch ($reverse) {
		case true:
			switch ($cus_type) {
				case 'เจ้าของ':
					return "owner";
				break;

				case 'ผู้ใช้':
					return "user";
				break;

				case 'คนขับ':
					return "driver";
				break;

				case 'ผู้นำรถมาซ่อม':
					return "cus_repair";
				break;

				case 'ผู้มารับรถ':
					return "cus_receive_car";
				break;
			}
		break;

		case false:
		default:
			switch ($cus_type) {
				case 'owner':
					return "เจ้าของ";
				break;

				case 'user':
					return "ผู้ใช้";
				break;

				case 'driver':
					return "คนขับ";
				break;

				case 'cus_repair':
					return "ผู้นำรถมาซ่อม";
				break;

				case 'cus_receive_car':
					return "ผู้มารับรถ";
				break;
			}
		break;
	}

}

function checkExistRelationInfo($vehicle_id, $type) {
	global $db, $config;

	$sql_rela = "SELECT VEHICLE_RELATIONSHIP.RELATES_ID, VEHICLE_RELATIONSHIP.RELATES_CUS_ID, VEHICLE_RELATIONSHIP.Date_Deliver FROM ".$config['db_vehicle'].".VEHICLE_RELATIONSHIP WHERE VEHICLE_RELATIONSHIP.RELATES_VEHICLE_ID = '".$vehicle_id."' AND VEHICLE_RELATIONSHIP.RELATES_TYPE = '".getTextCusType($type)."' AND VEHICLE_RELATIONSHIP.RELATES_STATUS <> '99' LIMIT 1 ";
	$que_rela = $db->query($sql_rela);
	if (!$que_rela) {
		die ($db->error." FILE: ".__FILE__." LINE: ".__LINE__);
	}
	$result = array();
	while ($fet_rela = $que_rela->fetch_assoc()) {
		$result[] = $fet_rela;
	}
	return $result;

}

function updateDateDeliverRelation($id_relation, $deliver_day) {
	global $db, $config;

	$sql = "UPDATE ".$config['db_vehicle'].".VEHICLE_RELATIONSHIP SET VEHICLE_RELATIONSHIP.Date_Deliver = '".$deliver_day."' WHERE VEHICLE_RELATIONSHIP.RELATES_STATUS <> '99' AND VEHICLE_RELATIONSHIP.RELATES_ID = '".$id_relation."' ";
	if (!$db->query($sql)) {
		die ($db->error." FILE: ".__FILE__." LINE: ".__LINE__);
	}else {
		return true;
	}
}

function updateRelation99($id_relation) {
	global $db, $config;

	$sql_up99 = "UPDATE ".$config['db_vehicle'].".VEHICLE_RELATIONSHIP SET VEHICLE_RELATIONSHIP.RELATES_STATUS = '99', VEHICLE_RELATIONSHIP.RELATES_MARKS = 'Change person from script update' WHERE VEHICLE_RELATIONSHIP.RELATES_ID = '".$id_relation."' ";
	if ($db->query($sql_up99)) {
		return true;
	}else {
		die ($db->error." FILE: ".__FILE__." LINE: ".__LINE__);
	}
}

function insertRelation($arrInsert = null) {
	global $db, $config;

	$fields = array();
	$values = array();
	if ($arrInsert !== null) {
		foreach ($arrInsert as $key => $value) {
			if (!empty($key)) {
				$fields[] = $key;
				$values[] = $value;
			}
		}
		$sql_rela_insert = "INSERT INTO ".$config['db_vehicle'].".VEHICLE_RELATIONSHIP (".implode(',', $fields).") VALUES ('".implode('\',\'', $values)."') ";
		if (!$db->query($sql_rela_insert)) {
			die ($db->error." FILE: ".__FILE__." LINE: ".__LINE__);
		}else {
			return $db->insert_id;
		}
	}

}


################################## [BEGIN] ##################################

echo "<h1>Begin: ".date('Y-m-d H:i:s')."</h1>";

$sql_ahis = "SELECT 
    ahistory.owner_id,
    ahistory.user_id,
    ahistory.driver_id,
    ahistory.cus_repair,
    ahistory.cus_receive_car,
    FROM_UNIXTIME(ahistory.deliver_day, '%Y-%m-%d') AS deliver_day,
    ahistory.idRef_vihicle,
    ahistory.asell_com_id
FROM
    ".$config['db_service'].".ahistory
WHERE
    ahistory.status <> '99'
        AND ahistory.idRef_vihicle > 0
        AND ahistory.deliver_day > 0
GROUP BY ahistory.idRef_vihicle
HAVING COUNT(ahistory.id) = 1
ORDER BY ahistory.id ASC ";

$que_ahis = $db->query($sql_ahis);
if (!$que_ahis) {
	die ($db->error." FILE: ".__FILE__." LINE: ".__LINE__);
}

$result = array();
while ($fet_ahis = $que_ahis->fetch_assoc()) {

	$result['owner'][]				= findCusNo($fet_ahis['owner_id']);
	$result['user'][] 				= findCusNo($fet_ahis['user_id']);
	$result['driver'][] 			= findCusNo($fet_ahis['driver_id']);
	$result['cus_repair'][]			= findCusNo($fet_ahis['cus_repair']);
	$result['cus_receive_car'][]	= findCusNo($fet_ahis['cus_receive_car']);
	$result['deliver_day'][] 		= $fet_ahis['deliver_day'];
	$result['idRef_vihicle'][] 		= $fet_ahis['idRef_vihicle'];
	$result['asell_com_id'][]		= $fet_ahis['asell_com_id'];

}

$num_rows = $que_ahis->num_rows;
for ($i = 0; $i < $num_rows; $i++) {

	$info_date = checkExistInfo($result['idRef_vihicle'][$i]);
	if ($info_date !== false) {

		if ($info_date == '0000-00-00') {
			updateDateDeliverInfo($result['idRef_vihicle'][$i], $result['deliver_day'][$i]);
		}

		/* เลขบริษัทที่จำหน่าย */
		$sellcom_id = getSellCompInfo($result['asell_com_id'][$i]);

		/* owner */
		if (!empty($result['owner'][$i])) {
			$owner = checkExistRelationInfo($result['idRef_vihicle'][$i], 'owner');
			if (count($owner) > 0) {
				foreach ($owner as $key => $value) {
					if ($value['RELATES_CUS_ID'] == $result['owner'][$i]) {
						if ($value['Date_Deliver'] == '0000-00-00') {
							updateDateDeliverRelation($value['RELATES_ID'], $result['deliver_day'][$i]);
						}
					}else {
						if (updateRelation99($value['RELATES_ID'])) {
							$arrInsert = array(
								'RELATES_VEHICLE_ID' => $result['idRef_vihicle'][$i],
								'RELATES_CUS_ID' => $result['owner'][$i],
								'RELATES_TYPE' => getTextCusType('owner'),
								'RELATES_COMP_ID' => $sellcom_id,
								'RELATES_MARKS' => 'Change person from script update',
								'Date_Deliver' => $result['deliver_day'][$i],
								'RELATES_ADD_DATE' => date('Y-m-d H:i:s')
							);
							insertRelation($arrInsert);
						}
					}
				}
			}else {
				$arrInsert = array(
					'RELATES_VEHICLE_ID' => $result['idRef_vihicle'][$i],
					'RELATES_CUS_ID' => $result['owner'][$i],
					'RELATES_TYPE' => getTextCusType('owner'),
					'RELATES_COMP_ID' => $sellcom_id,
					'RELATES_MARKS' => 'Change person from script update',
					'Date_Deliver' => $result['deliver_day'][$i],
					'RELATES_ADD_DATE' => date('Y-m-d H:i:s')
				);
				insertRelation($arrInsert);
			}
		}/* owner */

		/* user */
		if (!empty($result['user'][$i])) {
			$user = checkExistRelationInfo($result['idRef_vihicle'][$i], 'user');
			if (count($user) > 0) {
				foreach ($user as $key => $value) {
					if ($value['RELATES_CUS_ID'] == $result['user'][$i]) {
						if ($value['Date_Deliver'] == '0000-00-00') {
							updateDateDeliverRelation($value['RELATES_ID'], $result['deliver_day'][$i]);
						}
					}else {
						if (updateRelation99($value['RELATES_ID'])) {
							$arrInsert = array(
								'RELATES_VEHICLE_ID' => $result['idRef_vihicle'][$i],
								'RELATES_CUS_ID' => $result['user'][$i],
								'RELATES_TYPE' => getTextCusType('user'),
								'RELATES_COMP_ID' => $sellcom_id,
								'RELATES_MARKS' => 'Change person from script update',
								'Date_Deliver' => $result['deliver_day'][$i],
								'RELATES_ADD_DATE' => date('Y-m-d H:i:s')
							);
							insertRelation($arrInsert);
						}
					}
				}
			}else {
				$arrInsert = array(
					'RELATES_VEHICLE_ID' => $result['idRef_vihicle'][$i],
					'RELATES_CUS_ID' => $result['user'][$i],
					'RELATES_TYPE' => getTextCusType('user'),
					'RELATES_COMP_ID' => $sellcom_id,
					'RELATES_MARKS' => 'Change person from script update',
					'Date_Deliver' => $result['deliver_day'][$i],
					'RELATES_ADD_DATE' => date('Y-m-d H:i:s')
				);
				insertRelation($arrInsert);
			}
		}/* user */

		/* driver */
		if (!empty($result['driver'][$i])) {
			$driver = checkExistRelationInfo($result['idRef_vihicle'][$i], 'driver');
			if (count($driver) > 0) {
				foreach ($driver as $key => $value) {
					if ($value['RELATES_CUS_ID'] == $result['driver'][$i]) {
						if ($value['Date_Deliver'] == '0000-00-00') {
							updateDateDeliverRelation($value['RELATES_ID'], $result['deliver_day'][$i]);
						}
					}else {
						if (updateRelation99($value['RELATES_ID'])) {
							$arrInsert = array(
								'RELATES_VEHICLE_ID' => $result['idRef_vihicle'][$i],
								'RELATES_CUS_ID' => $result['driver'][$i],
								'RELATES_TYPE' => getTextCusType('driver'),
								'RELATES_COMP_ID' => $sellcom_id,
								'RELATES_MARKS' => 'Change person from script update',
								'Date_Deliver' => $result['deliver_day'][$i],
								'RELATES_ADD_DATE' => date('Y-m-d H:i:s')
							);
							insertRelation($arrInsert);
						}
					}
				}
			}else {
				$arrInsert = array(
					'RELATES_VEHICLE_ID' => $result['idRef_vihicle'][$i],
					'RELATES_CUS_ID' => $result['driver'][$i],
					'RELATES_TYPE' => getTextCusType('driver'),
					'RELATES_COMP_ID' => $sellcom_id,
					'RELATES_MARKS' => 'Change person from script update',
					'Date_Deliver' => $result['deliver_day'][$i],
					'RELATES_ADD_DATE' => date('Y-m-d H:i:s')
				);
				insertRelation($arrInsert);
			}
		}/* driver */
		
		/* cus_repair */
		if (!empty($result['cus_repair'][$i])) {
			$cus_repair = checkExistRelationInfo($result['idRef_vihicle'][$i], 'cus_repair');
			if (count($cus_repair) > 0) {
				foreach ($cus_repair as $key => $value) {
					if ($value['RELATES_CUS_ID'] == $result['cus_repair'][$i]) {
						if ($value['Date_Deliver'] == '0000-00-00') {
							updateDateDeliverRelation($value['RELATES_ID'], $result['deliver_day'][$i]);
						}
					}else {
						if (updateRelation99($value['RELATES_ID'])) {
							$arrInsert = array(
								'RELATES_VEHICLE_ID' => $result['idRef_vihicle'][$i],
								'RELATES_CUS_ID' => $result['cus_repair'][$i],
								'RELATES_TYPE' => getTextCusType('cus_repair'),
								'RELATES_COMP_ID' => $sellcom_id,
								'RELATES_MARKS' => 'Change person from script update',
								'Date_Deliver' => $result['deliver_day'][$i],
								'RELATES_ADD_DATE' => date('Y-m-d H:i:s')
							);
							insertRelation($arrInsert);
						}
					}
				}
			}else {
				$arrInsert = array(
					'RELATES_VEHICLE_ID' => $result['idRef_vihicle'][$i],
					'RELATES_CUS_ID' => $result['cus_repair'][$i],
					'RELATES_TYPE' => getTextCusType('cus_repair'),
					'RELATES_COMP_ID' => $sellcom_id,
					'RELATES_MARKS' => 'Change person from script update',
					'Date_Deliver' => $result['deliver_day'][$i],
					'RELATES_ADD_DATE' => date('Y-m-d H:i:s')
				);
				insertRelation($arrInsert);
			}
		}/* cus_repair */

		/* cus_receive_car */
		if (!empty($result['cus_receive_car'][$i])) {
			$cus_receive_car = checkExistRelationInfo($result['idRef_vihicle'][$i], 'cus_receive_car');
			if (count($cus_receive_car) > 0) {
				foreach ($cus_receive_car as $key => $value) {
					if ($value['RELATES_CUS_ID'] == $result['cus_receive_car'][$i]) {
						if ($value['Date_Deliver'] == '0000-00-00') {
							updateDateDeliverRelation($value['RELATES_ID'], $result['deliver_day'][$i]);
						}
					}else {
						if (updateRelation99($value['RELATES_ID'])) {
							$arrInsert = array(
								'RELATES_VEHICLE_ID' => $result['idRef_vihicle'][$i],
								'RELATES_CUS_ID' => $result['cus_receive_car'][$i],
								'RELATES_TYPE' => getTextCusType('cus_receive_car'),
								'RELATES_COMP_ID' => $sellcom_id,
								'RELATES_MARKS' => 'Change person from script update',
								'Date_Deliver' => $result['deliver_day'][$i],
								'RELATES_ADD_DATE' => date('Y-m-d H:i:s')
							);
							insertRelation($arrInsert);
						}
					}
				}
			}else {
				$arrInsert = array(
					'RELATES_VEHICLE_ID' => $result['idRef_vihicle'][$i],
					'RELATES_CUS_ID' => $result['cus_receive_car'][$i],
					'RELATES_TYPE' => getTextCusType('cus_receive_car'),
					'RELATES_COMP_ID' => $sellcom_id,
					'RELATES_MARKS' => 'Change person from script update',
					'Date_Deliver' => $result['deliver_day'][$i],
					'RELATES_ADD_DATE' => date('Y-m-d H:i:s')
				);
				insertRelation($arrInsert);
			}
		}/* cus_receive_car */

	}

}

$db->close();
echo "<h1>End: ".date('Y-m-d H:i:s')."</h1>";

?>