<?php
/*
 * crete by pt@2012-01-03
 * คำนวนเก็บไว้แสดงผลหน้ารายงานSSI
 * ทั้งหมด 4 ชนิดรายงาน 
 */

header ('Content-type: text/html; charset=utf-8');

$_SERVER['DOCUMENT_ROOT']	= dirname(dirname(dirname(__FILE__)));
require_once( $_SERVER['DOCUMENT_ROOT']. '/ERP_crm/config/connect_db.php' );

Conn2DB();

set_time_limit(0);

## สั่งทำงานโค้ด
// $doCode = ""; 		## ทำงาน
// $doCode = "No";  ## ไม่ทำงาน แสดงโค้ด Insert, Update, Delete
// $doCode = "All"; ## ทำงาน แสดงโค้ด Select, Insert, Update, Delete

mysql_query( "SET NAMES UTF8" );

$today_date = date("Y-m-d");
// $report_ssi_Year = '2012';  #ปิดเพื่อกำหนด foreach ในการวนคำนวณ
//$report_ssi_Year = '2011';   ############################## FIX

$RUN_SSI_DEALER = true; #-เปิดการรันสคริปต์ รายผู้จำน่าย
$RUN_SSI_BRANCH = true; #-เปิดการรันสคริปต์ รายสาขา
$RUN_SSI_EMP 	= true; #-เปิดการรันสคริปต์ รายพนักงาน
$RUN_SSI_CUS 	= true; #-เปิดการรันสคริปต์ รายลูกค้า
$CALCULATE_FORMS_ONLY_CURRENT_USE = true; #-true=คำนวนเฉพาะแบบฟอร์มที่ใช้งานปัจจุบัน / false=คำนวนแบบฟอร์มที่ยกเลิกใช้แล้วด้วยทั้งหมดด้วย

############################################
####-- pt@OU - ส่วนกำหนดSQLกรองแบบฟอร์ม --####
($CALCULATE_FORMS_ONLY_CURRENT_USE) 
#- SQL เปิด การคำวน ทุกๆแบบฟอร์มที่เคยสร้างมา ให้ก็อบไปใส่
? $sql_form = "SELECT ssi_id, ssi_form_type, ssi_form_name FROM $config[db_base_name].follow_ssi_form WHERE start_date <> '0000-00-00' AND start_date <= '$today_date' AND (stop_date >= '$today_date' OR stop_date IS NULL)  ORDER BY ssi_form_type " 
#- SQL เปิดคำนวนแบบฟอร์มแค่ฟอร์มที่ ใช้งาน ปัจจุบัน
: $sql_form = "SELECT ssi_id, ssi_form_type, ssi_form_name FROM $config[db_base_name].follow_ssi_form WHERE start_date <> '0000-00-00' AND start_date <= '$today_date' ORDER BY ssi_form_type ";
####-- สิ้นสุดส่วนกำหนดSQLกรองแบบฟอร์ม --####
########################################

//echo nl2br(" Process date : $today_date \n\n ");

####################################################################################################################################
############################################################ DEALER ################################################################
####################################################################################################################################


#กำหนดปี	
$report_ssi_Year = date('Y');
// $report_ssi_Year = '2014';

//echo "Calculation of SSI Dealer Report is "; if($RUN_SSI_DEALER) echo 'TURN_ON:';else echo nl2br("TURN_OFF \n");
if($RUN_SSI_DEALER){ //เช็คการเปิดโหมดให้รันสคริปต์
	$COUNT_FORM_DEALER = $COUNT_DEALER = 0;
	$sql_all_dealer = "SELECT DISTINCT(dealercode) FROM $config[db_easysale].Sell JOIN $table_config[working_company] ON Sell.sell_branch = working_company.id WHERE working_company.dealercode != '' ORDER BY working_company.id";
	$res_all_dealer = $logDb->queryAndLogSQL( $sql_all_dealer, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	
	if(mysql_num_rows($res_all_dealer)){ // ถ่าไม่เจอการขาย ใน ตางราง Sell จะไม่คำนวน
		while($dealer = mysql_fetch_assoc($res_all_dealer)){
			$result_form = $logDb->queryAndLogSQL( $sql_form, " FILE : ".__FILE__." LINE : ".__LINE__."" );

			while($ssi_form = mysql_fetch_assoc($result_form)){

				#-#-#-#-#--------- เคลียร์ข้อมูลเดิม ของฟอร์มนี้ --------#-#-#-#
				$sql_clear_list = " DELETE FROM $config[db_base_name].SSI_Report WHERE SSI_Report.year = '$report_ssi_Year' AND SSI_Report.names_code = '$dealer[dealercode]' AND SSI_Report.form_id = '$ssi_form[ssi_id]'  AND SSI_Report.report_type = '1'";
				$logDb->queryAndLogSQL( $sql_clear_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				#-#-#-#-#----------------------------------------#-#-#-#

				$count_sell = array();
				$Iform_id = $ssi_form['ssi_id']; // INSERT
				// หาเดือนสุดท้ายของปี
				if(date('Y') == $report_ssi_Year){
					$lastMonth = date('m');
				}else {$lastMonth = 12;
				}

				////////////////******************* A *****************////////////////
				if($ssi_form['ssi_form_type'] == 'A'){
					$total_answer_sum 			= array(); //จำนวนครั้งคำถามทั้งหมดที่สัมพาษ

					$list_question_id 			= array();
					$Iaverage_pleasure			=
					$Iaverage_per_per			=
					$Ilist_pleasure				=
					$Ilist_interview 			=
					$Ilist_score_max			=
					$Ilist_per_per				= '';

					$question_list_title 		=
					$pleasure_of_the_month 		=
					$percent_perfect 			=
					$perfect_man 				=
					$total_people_answer 		=
					$list_weight 				= array();
					$avg_pleasure 				= 0; //pt@2011-11-21 ค่าเฉลี่ย pleasure ความพึงพอใจ หาจาก การเอาค่าเฉลี่ยของแต่ละเดือนไปคูณจำนวนคนที่ตอบคำถามในเดือนนั้นแล้วรวมกันทุกเดือนแล้วเอาไปหารจำนวนคนตอบทั้งหมดในปีนั้น
					$avg_perper					= 0; //pt@2011-11-21 ค่า%เพอเฟค หาเหมือน $avg_pleasure ง่ายเนอะTT
					//หา น้ำหนัก title
					$sql = "SELECT ssi_title_id, ssi_title_name, weight FROM $config[db_base_name].follow_ssi_title WHERE ssi_id ='$ssi_form[ssi_id]' ORDER BY ssi_title_id";
					$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$main_question_id = $title_name = $title_weight = array();

					while($ssi_title = mysql_fetch_assoc($result)){
						$main_question_id[] = $ssi_title['ssi_title_id']; // INSERT
						$title_name[] = $ssi_title['ssi_title_name'];
						$title_weight[] = $ssi_title['weight'];
					}

					$question_list	=
					$title 	= array();
					$t 		=
					$t_old 	=
					$i_list = 0;
					
					//หา question list เอาไปหา ans
					$sql = "SELECT ssi_ques_id, ssi_title_id, question_list_id, weight, ssi_question_code FROM $config[db_base_name].follow_ssi_question_weight WHERE ssi_id ='$ssi_form[ssi_id]' ORDER BY ssi_title_id, ssi_ques_id";
					$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					
					while($ssi_weight = mysql_fetch_assoc( $result)){
						$question_list[] =  $ssi_weight['question_list_id']; //เก็บไปค้นคำตอบ
						if($ssi_weight['ssi_title_id'] != $title_id){
							if($t){
								$t+=1;// เลื่อน arr ช่องถัดไปเอาไว้เก็บ คำถามย่อย
							}
						}

						if($ssi_weight['ssi_title_id'] == $title_id||!$title[$t]){
							$title_id = $ssi_weight['ssi_title_id'];//เอาไว้เชค ตอนวนรอบต่อไป
							$list_weight[$t][] = $ssi_weight['weight'];

							$sql2 = "SELECT list_title FROM $config[db_base_name].follow_question_list WHERE id ='$ssi_weight[question_list_id]' ";
							$result2 = $logDb->queryAndLogSQL( $sql2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							$ssi_questionlist = mysql_fetch_assoc( $result2);
							$question_list_title[$t][] = $ssi_questionlist['list_title'];
							$list_question_id[$t][] = $ssi_weight['question_list_id']; // INSERT

							$title[$t] +=1;
						} else{
							$t+=1; //ไม่ควรขยับ ตน.
							$title_id = $ssi_weight['ssi_title_id'];//เอาไว้เชค ตอนวนรอบต่อไป
							$sql3 = "SELECT list_title FROM $config[db_base_name].follow_question_list WHERE id ='$ssi_weight[question_list_id]' ";
							$result3 = $logDb->queryAndLogSQL( $sql3, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							while($ssi_questionlist = mysql_fetch_assoc( $result3)){
								$question_list_title[$t][$i_list] = $ssi_questionlist['list_title'];
								$list_question_id[$t][$i_list] = $ssi_weight['question_list_id']; // INSERT
							}
							$list_weight[$t][] = $ssi_weight['weight'];
							$title[$t] +=1;
						}
					}
					$question_list_id = "'". implode("', '", $question_list). "'";//เก็บไปค้นคำตอบ
					unset($big_ans);

					for($m=1;$m<=$lastMonth;$m++){
						$mth = sprintf('%02d',$m);
						$year_month  = $mth.'-'.$report_ssi_Year;

						$sql_cus = "SELECT follow_ssi_score.cus_no, follow_ssi_score.follow_cus_id_refer ";
						$sql_cus .= "FROM ".$config['db_easysale'].".Sell ";
						$sql_cus .= "INNER JOIN ".$config['db_base_name'].".follow_ssi_score ";
						$sql_cus .= "ON SUBSTRING_INDEX(Sell.cusBuy,'_',1) = follow_ssi_score.cus_no ";
						$sql_cus .= "INNER JOIN ".$config['db_base_name'].".follow_customer ";
						$sql_cus .= "ON follow_ssi_score.follow_cus_id_refer = follow_customer.id ";
						$sql_cus .= "AND Sell.idRef_vihicle = follow_customer.chassi_no_s ";
						$sql_cus .= "INNER JOIN ".$table_config['working_company']." ";
						$sql_cus .= "ON Sell.sell_branch = working_company.id ";
						$sql_cus .= "WHERE ";
						$sql_cus .= "Sell.Date_Deliver LIKE '%".$year_month."' ";
						$sql_cus .= "AND follow_ssi_score.follow_ssi_form_id = '".$ssi_form['ssi_id']."' ";
						$sql_cus .= "AND working_company.dealercode = '".$dealer['dealercode']."' ";
						$sql_cus .= "AND follow_ssi_score.follow_cus_id_refer != '' ";
						$sql_cus .= "GROUP BY ";
						$sql_cus .= "follow_ssi_score.follow_cus_id_refer ";
						$result_cus = $logDb->queryAndLogSQL( $sql_cus, " FILE : ".__FILE__." LINE : ".__LINE__."" );

						while($ssi_score = mysql_fetch_assoc($result_cus)){
							$count_sell[$m] = mysql_num_rows($result_cus);
							$total_answer[$m] = 0;
							$count_perfect = 0;

							for($count_list=0;$count_list<count($question_list);$count_list++){  //ต้อง for ถ้า IN ช่องที่ID คำถามซ้ำมันจะข้ามไป
								
								$sql4 = "SELECT REPLACE(answer,'\'','' ) AS answer, use_status ";
								$sql4 .= "FROM ".$config['db_base_name'].".follow_answer ";
								$sql4 .= "WHERE follow_customer_id ='".$ssi_score['follow_cus_id_refer']."' ";
								$sql4 .= "AND ques_list_id = '".$question_list[$count_list]."' ";
								$sql4 .= "AND status !=99 LIMIT 1 ";
								$result4 = $logDb->queryAndLogSQL( $sql4, " FILE : ".__FILE__." LINE : ".__LINE__."" );
								$answer = mysql_fetch_assoc($result4);

								if($ssi_score[cus_no]!= $cusno_old ){
									$count_perfect = 0;
									unset($total_answer[$m]);
									$cusno_old = $ssi_score[cus_no];
									$month_old = $year_month;
								}else{
									$cusno_old = $ssi_score[cus_no];
									$month_old = $year_month;
								}
								if($answer['use_status'] != 'no'){
									if($answer['answer'] == 10){
										$count_perfect +=1;//ใช้เทียบ ถ้าจำนวนนี้เท่ากับ จำนวนคำถาม จะเป็นperfect man !!!
									}
									$total_answer_sum[$m] += 1;
									$total_answer[$m] += 1;
									$big_ans[$m][$count_list] +=  $answer['answer'];
									$total_people_answer[$m][$count_list] += 1;
								}else {
									$big_ans[$m][$count_list] +=  0;
								}
							}//for
							if($count_perfect == $total_answer[$m]){
								$perfect_man[$m][] = $ssi_score['cus_no'];
							}//เก็บไอดีคนที่ตอบได้ เพอเฟก!!
						}//ssi csore
						if(isset($big_ans[$m])){
							$pleasure_of_the_month[$m] = array_sum($big_ans[$m])/$total_answer_sum[$m];
							$percent_perfect[$m] = count($perfect_man[$m])*100/$count_sell[$m];
						}
					}//month

					if(!$perfect_man[$m]){$perfect_man[$m] = 0;}
					$arrMonthName = array('ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');

					//ส่วนหัวของรายงาน สรุป ความพึงพอใจรวม จำนวนสัมภาษ์ คันที่ได้คะแนนเต็ม และ เปอร์เซ็นต์คัที่ได้เต็ม

					$sign_pleasure = $sign_interview = $sign_score_max = $sign_per_per = '';
					for($m=1;$m<=$lastMonth;$m++){
						if($pleasure_of_the_month[$m]){
							$Ilist_pleasure .= $sign_pleasure.sprintf('%.2f',$pleasure_of_the_month[$m]);$sign_pleasure = ',';  // INSERT
							$avg_pleasure +=  $pleasure_of_the_month[$m]*$count_sell[$m];
						}else {
							$Ilist_pleasure .= $sign_pleasure.'0.00';$sign_pleasure = ',';  // INSERT
						}
						if($count_sell[$m]){
							$Ilist_interview .= $sign_interview.$count_sell[$m];$sign_interview = ',';  // INSERT
						}else {
							$Ilist_interview .= $sign_interview.'0';$sign_interview = ',';  // INSERT
						}
						if($perfect_man[$m]){
							$Ilist_score_max .= $sign_score_max.count($perfect_man[$m]);$sign_score_max = ',';  // INSERT
						}else {
							$Ilist_score_max .= $sign_score_max.'0';$sign_score_max = ',';  // INSERT
						}
						if($percent_perfect[$m]){
							$Ilist_per_per .= $sign_per_per.sprintf('%.2f',$percent_perfect[$m]);$sign_per_per = ',';  // INSERT
							$avg_perper += $percent_perfect[$m]*$count_sell[$m];
						}else{
							$Ilist_per_per .= $sign_per_per.'0.00';$sign_per_per = ',';  // INSERT
						}
					}

					//หาจำนวนเดือนที่ มีการคำนวนในแบบฟอร์มนี้ pt 2011-10-27  เพื่อให้ค่าเฉลี่ยมีเฉพาะเดือนที่มีข้อมูลเท่านั้น
					$sql_exist_date = "SELECT COUNT(DISTINCT(SUBSTRING(Date_Deliver,4,2))) AS numMonthExist ";
					$sql_exist_date .= "FROM ".$config['db_easysale'].".Sell ";
					$sql_exist_date .= "INNER JOIN ".$config['db_base_name'].".follow_ssi_score ";
					$sql_exist_date .= "ON SUBSTRING_INDEX(Sell.cusBuy,'_',1) = follow_ssi_score.cus_no ";
					$sql_exist_date .= "INNER JOIN ".$config['db_base_name'].".follow_customer ";
					$sql_exist_date .= "ON follow_ssi_score.follow_cus_id_refer = follow_customer.id ";
					$sql_exist_date .= "AND Sell.idRef_vihicle = follow_customer.chassi_no_s ";
					$sql_exist_date .= "INNER JOIN ".$table_config['working_company']." ";
					$sql_exist_date .= "ON Sell.sell_branch = working_company.id ";
					$sql_exist_date .= "WHERE ";
					$sql_exist_date .= "Sell.Date_Deliver LIKE '%".$report_ssi_Year."' ";
					$sql_exist_date .= "AND follow_ssi_score.follow_ssi_form_id = '".$ssi_form['ssi_id']."' ";
					$sql_exist_date .= "AND working_company.dealercode = '".$dealer['dealercode']."' ";
					$sql_exist_date .= "AND follow_ssi_score.follow_cus_id_refer != '' ";
					$sql_exist_date .= "GROUP BY ";
					$sql_exist_date .= "follow_ssi_score.follow_cus_id_refer ";
					$re_exs_d = $logDb->queryAndLogSQL( $sql_exist_date, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$count_date_exist = mysql_fetch_assoc($re_exs_d);

					if(!$count_date_exist['numMonthExist']){
						$count_date_exist['numMonthExist'] = 1;
					}

					//คิดค่าความพึงพอใจ
					if(array_sum($count_sell)!=0){
						$avg_perper = $avg_perper/array_sum($count_sell);
						$avg_pleasure = $avg_pleasure/array_sum($count_sell);
					}
					else {
						$avg_perper   = 0;
						$avg_pleasure = 0;
					}

					//แปลงค่าความพึงพอใจให้เป็นทศนิยมเท่านั้น
					$Iaverage_pleasure = sprintf('%.2f',$avg_pleasure); // INSERT
					if($avg_perper){
						$Iaverage_per_per = sprintf('%.2f',$avg_perper); // INSERT
					}else {
						$Iaverage_per_per = '0.00'; // INSERT
					}

					$i_score = 0;
					for($i=0;$i<count($title);$i++){
						$Iaverage_main	= $Ilist_main = $Iweight_main = '';
						$Iweight_main = $title_weight[$i];  // INSERT

						$sum_this_month = array();
						for($j=0;$j<$title[$i];$j++,$i_score++){
							$iNew=$i;if($iNew!=$iOld){$numrsList++; $iOld=$iNew;}
							$sub_question = $Iaverage = '';

							$sum_this_question =
							$avg_this_question = 0;
							$Ilist_score = $signList = '';


							for($m=1;$m<=$lastMonth;$m++){
								if($big_ans[$m][$i_score] != ''){
									$show_answer = $big_ans[$m][$i_score]/$total_people_answer[$m][$i_score];//Flow [1]
									$Ilist_score  .= $signList.sprintf('%.2f',$show_answer);$signList = ',';  // INSERT
								}else{
									$show_answer = 0;
									$Ilist_score  .= $signList.'0.00';$signList = ',';  // INSERT
								}
								$avg_this_question += $show_answer*$count_sell[$m];
								$sum_this_month[$i][$m] += $show_answer;

							}
							if(array_sum($count_sell)!=0){
								$avg_this_question = $avg_this_question/array_sum($count_sell);
							}else{
								$avg_this_question =  0;
							}
							$Imain_question_id = $main_question_id[$i]; // INSERT
							$Ilist_question_id = $list_question_id[$i][$j]; // INSERT
							$Iweight = $list_weight[$i][$j]; // INSERT
							$Iaverage = sprintf('%.2f',$avg_this_question); // INSERT
							$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
												VALUES ('$report_ssi_Year', '$dealer[dealercode]', '$Iform_id', '$Imain_question_id', '$Ilist_question_id', '0', '$Iweight', '$Ilist_score', '$Iaverage' , '3', '1', '', '$today_date' )";
							$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						}
						$sum_this_main = 0;
						$avg_this_main = 0; //pt@2011-11-21 ค่าเฉลี่ยหัวข้อหลัก หาเหมือน $avg_pleasure

						$sign_main = $Ilist_main = '';
						for($m=1;$m<=$lastMonth;$m++){
							$main_ques_total = $sum_this_month[$i][$m]/$title[$i];
							if($main_ques_total != 0){
								$Ilist_main .= $sign_main.sprintf('%.2f',$main_ques_total);$sign_main = ',';  // INSERT
							}else{
								$Ilist_main .= $sign_main.'0.00';$sign_main = ',';  // INSERT
							}
							$avg_this_main += $main_ques_total*$count_sell[$m];
						}

						if(array_sum($count_sell)!=0){
							$avg_this_main = $avg_this_main/array_sum($count_sell);
						}else{
							$avg_this_main =0;
						}
						$Iaverage_main = sprintf('%.2f',$avg_this_main); // INSERT

						$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
												VALUES ('$report_ssi_Year', '$dealer[dealercode]', '$Iform_id', '$Imain_question_id', '', '0', '$Iweight_main', '$Ilist_main', '$Iaverage_main' , '2', '1', '', '$today_date' )";
						$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					}

					$btn_id = $ssi_form['ssi_id']; // id ปุ่ม Download

					$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$dealer[dealercode]', '$Iform_id', '', '', '1', '', '$Ilist_pleasure', '$Iaverage_pleasure' , '1', '1', '', '$today_date' )";
					$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

					$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$dealer[dealercode]', '$Iform_id', '', '', '2', '', '$Ilist_interview', '' , '1', '1', '', '$today_date' )";
					$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

					$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$dealer[dealercode]', '$Iform_id', '', '', '3', '', '$Ilist_score_max', '' , '1', '1', '', '$today_date' )";
					$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

					$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$dealer[dealercode]', '$Iform_id', '', '', '4', '', '$Ilist_per_per', '$Iaverage_per_per' , '1', '1', '', '$today_date' )";
					$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				}// END FORM TYPE = A

				///////////***************B***************////////////////////
				else{ // IF its type is = B
					$list_question_id 			= array();
					$Iaverage_pleasure			=
					$Iaverage_per_per			=
					$Ilist_pleasure				=
					$Ilist_interview 			=
					$Ilist_score_max			=
					$Ilist_per_per				= '';

					$total_answer_sum =
					$question_list_title =
					$pleasure_of_the_month =
					$percent_perfect =
					$perfect_man =
					$total_people_answer =
					$list_code = array();
					$pleasure = '';//ทำให้บรรทัดนี้หายไป เพราะไม่งั้น มัน จะเอามาจาก ฟอร์มเอ
					$main_question = '';

					//หา น้ำหนัก title
					$sql = "SELECT ssi_title_id, ssi_title_name, weight FROM $config[db_base_name].follow_ssi_title WHERE ssi_id ='$ssi_form[ssi_id]' ORDER BY ssi_title_id";
					$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$main_question_id = $title_name = array();

					while($ssi_title = mysql_fetch_assoc( $result)){
						$main_question_id[] = $ssi_title['ssi_title_id']; // INSERT
						$title_name[] = $ssi_title['ssi_title_name'];
					}
					$question_list =
					$title  = array() ;
					$t =
					$t_old =
					$i_list = 0;
					//หา question list เอาไปหา ans
					$sql = "SELECT ssi_ques_id, ssi_title_id, question_list_id, weight, ssi_question_code FROM $config[db_base_name].follow_ssi_question_weight WHERE ssi_id ='$ssi_form[ssi_id]' ORDER BY ssi_title_id, ssi_ques_id";
					$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					while($ssi_weight = mysql_fetch_assoc( $result)){
						$question_list[] =  $ssi_weight['question_list_id']; //เก็บไปค้นคำตอบ
						if($ssi_weight['ssi_title_id'] != $title_id){
							if($t){
								$t+=1;// เลื่อน arr ช่องถัดไปเอาไว้เก็บ คำถามย่อย
							}
						}

						if($ssi_weight['ssi_title_id'] == $title_id||!$title[$t]){
							$title_id = $ssi_weight['ssi_title_id'];//เอาไว้เชค ตอนวนรอบต่อไป
							$list_weight[$t][] = $ssi_weight['weight'];

							$sql2 = "SELECT list_title
								FROM $config[db_base_name].follow_question_list
								WHERE id ='$ssi_weight[question_list_id]' ";
							$result2 = $logDb->queryAndLogSQL( $sql2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							$ssi_questionlist = mysql_fetch_assoc( $result2);
							$question_list_title[$t][] = $ssi_questionlist['list_title'];
							$list_question_id[$t][] = $ssi_weight['question_list_id']; // INSERT

							$title[$t] +=1;
						} else{
							$t+=1; //ไม่ควรขยับ ตน.
							$title_id = $ssi_weight['ssi_title_id'];//เอาไว้เชค ตอนวนรอบต่อไป
							$sql3 = "SELECT list_title
								FROM $config[db_base_name].follow_question_list
								WHERE id ='$ssi_weight[question_list_id]' ";
							$result3 = $logDb->queryAndLogSQL( $sql3, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							while($ssi_questionlist = mysql_fetch_assoc( $result3)){
								$question_list_title[$t][$i_list] = $ssi_questionlist[list_title];
								$list_question_id[$t][$i_list] = $ssi_weight['question_list_id']; // INSERT
							}
							$list_weight[$t][] = $ssi_weight['weight'];
							$title[$t] +=1;
						}
					}
					$question_list_id = "'". implode("', '", $question_list). "'";//เก็บไปค้นคำตอบ

					unset($big_ans);
					//วนทั้งปี   ต้องแก้ตอนที่ เลือกปี มาแล้วอีกที   *****
					for($m=1;$m<=$lastMonth;$m++){
						$mth = sprintf('%02d',$m);
						$year_month  = $mth.'-'.$report_ssi_Year;
						$sql_cus = "SELECT follow_ssi_score.cus_no, follow_ssi_score.follow_cus_id_refer ";
						$sql_cus .= "FROM ".$config['db_easysale'].".Sell ";
						$sql_cus .= "INNER JOIN ".$config['db_base_name'].".follow_ssi_score ";
						$sql_cus .= "ON SUBSTRING_INDEX(Sell.cusBuy,'_',1) = follow_ssi_score.cus_no ";
						$sql_cus .= "INNER JOIN ".$config['db_base_name'].".follow_customer ";
						$sql_cus .= "ON follow_ssi_score.follow_cus_id_refer = follow_customer.id ";
						$sql_cus .= "AND Sell.idRef_vihicle = follow_customer.chassi_no_s ";
						$sql_cus .= "INNER JOIN ".$table_config['working_company']." ";
						$sql_cus .= "ON Sell.sell_branch = working_company.id ";
						$sql_cus .= "WHERE ";
						$sql_cus .= "Sell.Date_Deliver LIKE '%".$year_month."' ";
						$sql_cus .= "AND follow_ssi_score.follow_ssi_form_id = '".$ssi_form['ssi_id']."' ";
						$sql_cus .= "AND working_company.dealercode = '".$dealer['dealercode']."' ";
						$sql_cus .= "AND follow_ssi_score.follow_cus_id_refer != '' ";
						$sql_cus .= "GROUP BY ";
						$sql_cus .= "follow_ssi_score.follow_cus_id_refer ";
						//
						$result = $logDb->queryAndLogSQL( $sql_cus, " FILE : ".__FILE__." LINE : ".__LINE__."" );

						while($ssi_score = mysql_fetch_assoc($result)){
							$count_sell[$m] = mysql_num_rows($result);
							$total_answer[$m] = 0;
							$count_perfect = 0;
							for($count_list=0;$count_list<count($question_list);$count_list++){  //ต้อง for ถ้า IN ช่องที่ID คำถามซ้ำมันจะข้ามไป
								
								$sql4 = "SELECT id, REPLACE(answer,'\'','' ) AS answer, use_status ";
								$sql4 .= "FROM ".$config['db_base_name'].".follow_answer ";
								$sql4 .= "WHERE follow_customer_id ='".$ssi_score['follow_cus_id_refer']."' ";
								$sql4 .= "AND ques_list_id = '".$question_list[$count_list]."' ";
								$sql4 .= "AND status !=99 LIMIT 1 ";
								//ques_list_id IN ($question_list_id)
								$result4 = $logDb->queryAndLogSQL( $sql4, " FILE : ".__FILE__." LINE : ".__LINE__."" );
								$answer = mysql_fetch_assoc($result4);

								if($ssi_score[cus_no]!= $cusno_old ){$count_perfect = 0;unset($total_answer[$m]);$cusno_old = $ssi_score[cus_no];$month_old = $year_month;}else{$cusno_old = $ssi_score[cus_no];$month_old = $year_month;}
								
								//ค้นค่าคำตอบ
								$sql5 = "SELECT score_question_list ";
								$sql5 .= "FROM ".$config['db_base_name'].".follow_ssi_score ";
								$sql5 .= "WHERE follow_cus_id_refer ='".$ssi_score['follow_cus_id_refer']."' ";
								$sql5 .= "AND follow_answer_id = '".$answer['id']."' LIMIT 1";
								//ques_list_id IN ($question_list_id)
								$result5 = $logDb->queryAndLogSQL( $sql5, " FILE : ".__FILE__." LINE : ".__LINE__."" );
								$score_fetch = mysql_fetch_assoc($result5);

								if($answer['use_status'] != 'no'){
									if($score_fetch['score_question_list'] == 1){
										$count_perfect +=1;//ใช้เทียบ ถ้าจำนวนนี้เท่ากับ จำนวนคำถาม จะเป็นperfect man !!!
									}
									$total_answer_sum[$m] += 1;
									$total_answer[$m] += 1;
									$big_ans[$m][$count_list] +=  $score_fetch['score_question_list'];
									$total_people_answer[$m][$count_list] += 1;
								}else {
									$big_ans[$m][$count_list] +=  0;
								}
							}//for
							if($count_perfect == $total_answer[$m]){
								$perfect_man[$m][] = $ssi_score['cus_no'];
							}//เก็บไอดีคนที่ตอบได้ เพอเฟก!!
						}//ssi csore
						if(isset($big_ans[$m])){
							$percent_perfect[$m] = count($perfect_man[$m])*100/$count_sell[$m];
						}
					}//month
					if(!$perfect_man[$m]){$perfect_man[$m] = 0;}

					$arrMonthName = array('ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');

					$avg_per_per = 0;
					$sum_pleasure = 0;

					$sign_pleasure = $sign_interview = $sign_score_max = $sign_per_per = '';
					//ส่วนหัวของรายงาน สรุป ความพึงพอใจรวม จำนวนสัมภาษ์ คันที่ได้คะแนนเต็ม และ เปอร์เซ็นต์คัที่ได้เต็ม
					for($m=1;$m<=$lastMonth;$m++){
						if($count_sell[$m]){
							$Ilist_interview .= $sign_interview.$count_sell[$m];$sign_interview = ',';  // INSERT
						}else {
							$Ilist_interview .= $sign_interview.'0';$sign_interview = ',';  // INSERT
						}
						if($perfect_man[$m]){
							$Ilist_score_max .= $sign_score_max.count($perfect_man[$m]);$sign_score_max = ',';  // INSERT
						}else {
							$Ilist_score_max .= $sign_score_max.'0';$sign_score_max = ',';  // INSERT
						}
						if($percent_perfect[$m]){
							$Ilist_per_per .= $sign_per_per.sprintf('%.2f',$percent_perfect[$m]);$sign_per_per = ',';  // INSERT
							$avg_per_per += $percent_perfect[$m]*$count_sell[$m];
						}else{
							$Ilist_per_per .= $sign_per_per.'0.00';$sign_per_per = ',';  // INSERT
						}

					}

					//หาจำนวนเดือนที่ มีการคำนวนในแบบฟอร์มนี้ pt 2011-10-27  เพื่อให้ค่าเฉลี่ยมีเฉพาะเดือนที่มีข้อมูลเท่านั้น
					$sql_exist_date = "SELECT COUNT(DISTINCT(SUBSTRING(Date_Deliver,4,2))) AS numMonthExist ";
					$sql_exist_date .= "FROM ".$config['db_easysale'].".Sell ";
					$sql_exist_date .= "INNER JOIN ".$config['db_base_name'].".follow_ssi_score ";
					$sql_exist_date .= "ON SUBSTRING_INDEX(Sell.cusBuy,'_',1) = follow_ssi_score.cus_no ";
					$sql_exist_date .= "INNER JOIN ".$config['db_base_name'].".follow_customer ";
					$sql_exist_date .= "ON follow_ssi_score.follow_cus_id_refer = follow_customer.id ";
					$sql_exist_date .= "AND Sell.idRef_vihicle = follow_customer.chassi_no_s ";
					$sql_exist_date .= "INNER JOIN ".$table_config['working_company']." ";
					$sql_exist_date .= "ON Sell.sell_branch = working_company.id ";
					$sql_exist_date .= "WHERE ";
					$sql_exist_date .= "Sell.Date_Deliver LIKE '%".$report_ssi_Year."' ";
					$sql_exist_date .= "AND follow_ssi_score.follow_ssi_form_id = '".$ssi_form['ssi_id']."' ";
					$sql_exist_date .= "AND working_company.dealercode = '".$dealer['dealercode']."' ";
					$sql_exist_date .= "AND follow_ssi_score.follow_cus_id_refer != '' ";
					$sql_exist_date .= "GROUP BY ";
					$sql_exist_date .= "follow_ssi_score.follow_cus_id_refer ";
					$re_exs_d = $logDb->queryAndLogSQL( $sql_exist_date, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$count_date_exist = mysql_fetch_assoc($re_exs_d);

					if(!$count_date_exist['numMonthExist']){$count_date_exist['numMonthExist'] = 1;}

					if(array_sum($count_sell)!=0){ //pt 2011-11-21
						$avg_per_per = $avg_per_per/array_sum($count_sell);
					}else{
						$avg_per_per = 0;
					}
					if($avg_per_per){
						$Iaverage_per_per = sprintf('%.2f',$avg_per_per); // INSERT
					}else {
						$Iaverage_per_per = '0.00'; // INSERT
					}

					$i_score = 0;
					for($i=0;$i<count($title);$i++){
						$sum_this_month = array();
						$sub_question = '';
						for($j=0;$j<$title[$i];$j++,$i_score++){
							$sub_question = $Iaverage = '';
							$avg_this_question = 0;
							$Ilist_score = $signList = '';

							for($m=1;$m<=$lastMonth;$m++){
								if($big_ans[$m][$i_score] != ''){
									$show_answer = $big_ans[$m][$i_score]*100/$total_people_answer[$m][$i_score];
									$Ilist_score  .= $signList.sprintf('%.2f',$show_answer);$signList = ',';  // INSERT
								}else{
									$show_answer = 0;
									$Ilist_score  .= $signList.'0.00';$signList = ',';  // INSERT
								}
								$avg_this_question += $show_answer*$count_sell[$m];
								$sum_this_month[$i][$m] += $show_answer;

							}
							if(array_sum($count_sell)!=0){
								$avg_this_question = $avg_this_question/array_sum($count_sell);
							}else{
								$avg_this_question =0;
							}
							$Imain_question_id = $main_question_id[$i]; // INSERT
							$Ilist_question_id = $list_question_id[$i][$j]; // INSERT
							$Iaverage = sprintf('%.2f',$avg_this_question); // INSERT
							$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
												VALUES ('$report_ssi_Year', '$dealer[dealercode]', '$Iform_id', '$Imain_question_id', '$Ilist_question_id', '0', '', '$Ilist_score', '$Iaverage' , '3', '1', '', '$today_date' )";
							$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						}
					}

					$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$dealer[dealercode]', '$Iform_id', '', '', '2', '', '$Ilist_interview', '' , '1', '1', '', '$today_date' )";
					$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

					$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$dealer[dealercode]', '$Iform_id', '', '', '3', '', '$Ilist_score_max', '' , '1', '1', '', '$today_date' )";
					$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

					$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$dealer[dealercode]', '$Iform_id', '', '', '4', '', '$Ilist_per_per', '$Iaverage_per_per' , '1', '1', '', '$today_date' )";
					$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				}// END FORM TYPE = B
				$COUNT_FORM_DEALER++;
			}// Form
			$COUNT_DEALER++;
		} // while dealer

	}// have dealer
	//echo nl2br(" $COUNT_FORM_DEALER form(s), and $COUNT_DEALER dealer(s) are calculated. \n");
	#nongnong insert เพื่อเช็คว่าครบหรือไม่
	$nongnong_insert = "INSERT INTO $config[db_base_name].SSI_Report (year,list_score,remark)
						VALUES ('$report_ssi_Year', 'เสร็จสิ้นการรันข้อมูลของปี รายผู้จำหน่าย','PITSAMAI01' )";
	$logDb->queryAndLogSQL( $nongnong_insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );

}// จบโหมด รายผู้จำหน่าย


####################################################################################################################################
############################################################ BRANCH ################################################################
####################################################################################################################################
//echo "Calculation of SSI Branch Report is "; if($RUN_SSI_BRANCH) echo 'TURN_ON:';else echo nl2br("TURN_OFF \n");
if($RUN_SSI_BRANCH){ //เช็คการเปิดโหมดให้รันสคริปต์
	$COUNT_FORM_BRANCH = $COUNT_BRANCH = 0;
	$sql_b = "SELECT DISTINCT(sell_branch), working_company.name, working_company.short_name
			FROM $config[db_easysale].Sell JOIN $table_config[working_company] ON Sell.sell_branch = working_company.id
			WHERE Date_Deliver LIKE '%$report_ssi_Year'
			AND sell_branch LIKE '%'
 			ORDER BY working_company.id ";

	$result_b = $logDb->queryAndLogSQL( $sql_b, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	while($sell_brn = mysql_fetch_assoc($result_b)){
		$branch_id = $sell_brn['sell_branch'];

		$result_form = $logDb->queryAndLogSQL( $sql_form, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while($ssi_form = mysql_fetch_assoc($result_form)){

			#-#-#-#-#--------- เคลียร์ข้อมูลเดิม ของฟอร์มนี้ --------#-#-#-#
			$sql_clear_list = " DELETE FROM $config[db_base_name].SSI_Report WHERE SSI_Report.year = '$report_ssi_Year' AND SSI_Report.names_code = '$sell_brn[sell_branch]' AND SSI_Report.form_id = '$ssi_form[ssi_id]'  AND SSI_Report.report_type = '2'";
			$logDb->queryAndLogSQL( $sql_clear_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			#-#-#-#-#----------------------------------------#-#-#-#

			$Iform_id = $ssi_form['ssi_id']; // INSERT
			$count_sell = array();
			// หาเดือนสุดท้ายของปี
			if(date('Y') == $report_ssi_Year){
				$lastMonth = date('m');
			}else {$lastMonth = 12;
			}

			////////////////******************* A *****************////////////////
			if($ssi_form['ssi_form_type'] == 'A'){
				$list_question_id 			= array();
				$Iaverage_pleasure			=
				$Iaverage_per_per			=
				$Ilist_pleasure				=
				$Ilist_interview 			=
				$Ilist_score_max			=
				$Ilist_per_per				= '';

				$total_answer_sum =
				$question_list_title =
				$pleasure_of_the_month =
				$percent_perfect =
				$perfect_man =
				$total_people_answer =
				$list_weight = array();
				//หา น้ำหนัก title
				$sql = "SELECT ssi_title_id, ssi_title_name, weight FROM $config[db_base_name].follow_ssi_title WHERE ssi_id ='$ssi_form[ssi_id]' ORDER BY ssi_title_id";
				$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$main_question_id = $title_name = $title_weight = array();
				while($ssi_title = mysql_fetch_assoc( $result)){
					$main_question_id[] = $ssi_title['ssi_title_id']; // INSERT
					$title_name[] = $ssi_title['ssi_title_name'];
					$title_weight[] = $ssi_title['weight'];
				}

				$question_list = $title  = array() ;
				$t =
				$t_old =
				$i_list = 0;
				//หา question list เอาไปหา ans
				$sql = "SELECT ssi_ques_id, ssi_title_id, question_list_id, weight, ssi_question_code
					FROM $config[db_base_name].follow_ssi_question_weight
					WHERE ssi_id ='$ssi_form[ssi_id]'
					ORDER BY ssi_title_id, ssi_ques_id";
				$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				while($ssi_weight = mysql_fetch_assoc( $result)){
					$question_list[] =  $ssi_weight['question_list_id']; //เก็บไปค้นคำตอบ
					if($ssi_weight['ssi_title_id'] != $title_id){
						if($t){
							$t+=1;// เลื่อน arr ช่องถัดไปเอาไว้เก็บ คำถามย่อย
						}
					}

					if($ssi_weight['ssi_title_id'] == $title_id||!$title[$t]){
						$title_id = $ssi_weight['ssi_title_id'];//เอาไว้เชค ตอนวนรอบต่อไป
						$list_weight[$t][] = $ssi_weight['weight'];

						$sql2 = "SELECT list_title
							FROM $config[db_base_name].follow_question_list
							WHERE id ='$ssi_weight[question_list_id]' ";
						$result2 = $logDb->queryAndLogSQL( $sql2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						$ssi_questionlist = mysql_fetch_assoc( $result2);
						$question_list_title[$t][] = $ssi_questionlist['list_title'];
						$list_question_id[$t][] = $ssi_weight['question_list_id']; // INSERT
						$title[$t] +=1;
					} else{
						$t+=1; //ไม่ควรขยับ ตน.
						$title_id = $ssi_weight['ssi_title_id'];//เอาไว้เชค ตอนวนรอบต่อไป
						$sql3 = "SELECT list_title
							FROM $config[db_base_name].follow_question_list
							WHERE id ='$ssi_weight[question_list_id]' ";
						$result3 = $logDb->queryAndLogSQL( $sql3, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						while($ssi_questionlist = mysql_fetch_assoc( $result3)){
							$question_list_title[$t][$i_list] = $ssi_questionlist['list_title'];
							$list_question_id[$t][$i_list] = $ssi_weight['question_list_id']; // INSERT
						}
						$list_weight[$t][] = $ssi_weight['weight'];
						$title[$t] +=1;
					}
				}

				$question_list_id = "'". implode("', '", $question_list). "'";//เก็บไปค้นคำตอบ
				$cusno_old = 0;
				unset($big_ans);
				for($m=1;$m<=$lastMonth;$m++){
					$mth = sprintf('%02d',$m);
					$year_month  = $mth.'-'.$report_ssi_Year;

					$sql_cus = "SELECT follow_ssi_score.cus_no, follow_ssi_score.follow_cus_id_refer ";
					$sql_cus .= "FROM ".$config['db_easysale'].".Sell ";
					$sql_cus .= "INNER JOIN ".$config['db_base_name'].".follow_ssi_score ";
					$sql_cus .= "ON SUBSTRING_INDEX(Sell.cusBuy,'_',1) = follow_ssi_score.cus_no ";
					$sql_cus .= "INNER JOIN ".$config['db_base_name'].".follow_customer ";
					$sql_cus .= "ON follow_ssi_score.follow_cus_id_refer = follow_customer.id ";
					$sql_cus .= "AND Sell.idRef_vihicle = follow_customer.chassi_no_s ";
					$sql_cus .= "WHERE ";
					$sql_cus .= "Sell.Date_Deliver LIKE '%".$year_month."' ";
					$sql_cus .= "AND follow_ssi_score.follow_ssi_form_id = '".$ssi_form['ssi_id']."' ";
					$sql_cus .= "AND Sell.sell_branch = '".$sell_brn['sell_branch']."' ";
					$sql_cus .= "AND follow_ssi_score.follow_cus_id_refer != '' ";
					$sql_cus .= "GROUP BY ";
					$sql_cus .= "follow_ssi_score.follow_cus_id_refer ";
					$result_cus = $logDb->queryAndLogSQL( $sql_cus, " FILE : ".__FILE__." LINE : ".__LINE__."" );

					while($ssi_score = mysql_fetch_assoc($result_cus)){
						$count_sell[$m] = mysql_num_rows($result_cus);

						$total_answer[$m] = 0;
						$count_perfect = 0;
						for($count_list=0;$count_list<count($question_list);$count_list++){  //ต้อง for ถ้า IN ช่องที่ID คำถามซ้ำมันจะข้ามไป

							$sql4 = "SELECT REPLACE(answer,'\'','' ) AS answer, use_status ";
							$sql4 .= "FROM ".$config['db_base_name'].".follow_answer ";
							$sql4 .= "WHERE follow_customer_id ='".$ssi_score['follow_cus_id_refer']."' ";
							$sql4 .= "AND ques_list_id = '".$question_list[$count_list]."' ";
							$sql4 .= "AND status !=99 LIMIT 1 ";
							//ques_list_id IN ($question_list_id)
							$result4 = $logDb->queryAndLogSQL( $sql4, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							$answer = mysql_fetch_assoc($result4);

							if($ssi_score['cus_no']!= $cusno_old ){
								$count_perfect = 0;
								unset($total_answer[$m]);
								$cusno_old = $ssi_score['cus_no'];
								$month_old = $year_month;
							}else{
								$cusno_old = $ssi_score['cus_no'];
								$month_old = $year_month;
							}
							if($answer['use_status'] != 'no'){
								if($answer['answer'] == 10){
									$count_perfect +=1;//ใช้เทียบ ถ้าจำนวนนี้เท่ากับ จำนวนคำถาม จะเป็นperfect man !!!
								}
								$total_answer_sum[$m] += 1;
								$total_answer[$m] += 1;
								$big_ans[$m][$count_list] +=  $answer['answer'];
								$total_people_answer[$m][$count_list] += 1;
							}else {
								$big_ans[$m][$count_list] +=  0;
							}
						}//for

					
						if($count_perfect == $total_answer[$m]){
							$perfect_man[$m][] = $ssi_score['cus_no'];
						}//เก็บไอดีคนที่ตอบได้ เพอเฟก!!
					}//ssi csore
					if(isset($big_ans[$m])){
						$pleasure_of_the_month[$m] = array_sum($big_ans[$m])/$total_answer_sum[$m];
						$percent_perfect[$m] = count($perfect_man[$m])*100/$count_sell[$m];
					}
				}//month
				if(!$perfect_man[$m]){$perfect_man[$m] = 0;}
				$arrMonthName = array('ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');

				$avg_pleasure = 0;
				$avg_per_per = 0;

				//ส่วนหัวของรายงาน สรุป ความพึงพอใจรวม จำนวนสัมภาษ์ คันที่ได้คะแนนเต็ม และ เปอร์เซ็นต์คัที่ได้เต็ม
				$sign_pleasure = $sign_interview = $sign_score_max = $sign_per_per = '';

				for($m=1;$m<=$lastMonth;$m++){
					if($pleasure_of_the_month[$m]){
						$sum_pleasure += $pleasure_of_the_month[$m];
						$avg_pleasure += $pleasure_of_the_month[$m]*$count_sell[$m];
						$Ilist_pleasure .= $sign_pleasure.sprintf('%.2f',$pleasure_of_the_month[$m]);$sign_pleasure = ',';  // INSERT
					}else {
						$Ilist_pleasure .= $sign_pleasure.'0.00';$sign_pleasure = ',';  // INSERT
					}

					if($count_sell[$m]){
						$Ilist_interview .= $sign_interview.$count_sell[$m];$sign_interview = ',';  // INSERT
					}else {
						$Ilist_interview .= $sign_interview.'0';$sign_interview = ',';  // INSERT
					}
					if($perfect_man[$m]){
						$Ilist_score_max .= $sign_score_max.count($perfect_man[$m]);$sign_score_max = ',';  // INSERT
					}else {
						$Ilist_score_max .= $sign_score_max.'0';$sign_score_max = ',';  // INSERT
					}
					if($percent_perfect[$m]){
						$Ilist_per_per .= $sign_per_per.sprintf('%.2f',$percent_perfect[$m]);$sign_per_per = ',';  // INSERT
						$avg_per_per += $percent_perfect[$m]*$count_sell[$m];

					}else{
						$Ilist_per_per .= $sign_per_per.'0.00';$sign_per_per = ',';  // INSERT
					}

				}

				//หาจำนวนเดือนที่ มีการคำนวนในแบบฟอร์มนี้ pt 2011-10-27  เพื่อให้ค่าเฉลี่ยมีเฉพาะเดือนที่มีข้อมูลเท่านั้น
				$sql_exist_date = "SELECT COUNT(DISTINCT(SUBSTRING(Date_Deliver,4,2))) AS numMonthExist ";
				$sql_exist_date .= "FROM ".$config['db_easysale'].".Sell ";
				$sql_exist_date .= "INNER JOIN ".$config['db_base_name'].".follow_ssi_score ";
				$sql_exist_date .= "ON SUBSTRING_INDEX(Sell.cusBuy,'_',1) = follow_ssi_score.cus_no ";
				$sql_exist_date .= "INNER JOIN ".$config['db_base_name'].".follow_customer ";
				$sql_exist_date .= "ON follow_ssi_score.follow_cus_id_refer = follow_customer.id ";
				$sql_exist_date .= "AND Sell.idRef_vihicle = follow_customer.chassi_no_s ";
				$sql_exist_date .= "WHERE ";
				$sql_exist_date .= "Sell.Date_Deliver LIKE '%".$report_ssi_Year."' ";
				$sql_exist_date .= "AND follow_ssi_score.follow_ssi_form_id = '".$ssi_form['ssi_id']."' ";
				$sql_exist_date .= "AND Sell.sell_branch = '".$sell_brn['sell_branch']."' ";
				$sql_exist_date .= "AND follow_ssi_score.follow_cus_id_refer != '' ";
				$sql_exist_date .= "GROUP BY ";
				$sql_exist_date .= "follow_ssi_score.follow_cus_id_refer ";
				$re_exs_d = $logDb->queryAndLogSQL( $sql_exist_date, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$count_date_exist = mysql_fetch_assoc($re_exs_d);

				if(!$count_date_exist['numMonthExist']){$count_date_exist['numMonthExist'] = 1;}

				if(array_sum($count_sell)!=0){
					$avg_pleasure = $avg_pleasure/array_sum($count_sell);
					$avg_per_per = $avg_per_per/array_sum($count_sell);
				}
				else{
					$avg_pleasure = 0;
					$avg_per_per = 0;
				}
				$Iaverage_pleasure = sprintf('%.2f',$avg_pleasure); // INSERT

				if($avg_per_per){
					$Iaverage_per_per = sprintf('%.2f',$avg_per_per); // INSERT
				}else {
					$Iaverage_per_per = '0.00'; // INSERT
				}
				$i_score = 0;

				for($i=0;$i<count($title);$i++){
					$Iaverage_main	= $Ilist_main = $Iweight_main = '';
					$Iweight_main = $title_weight[$i];  // INSERT

					$sum_this_month = array();
					$sub_question = '';

					for($j=0;$j<$title[$i];$j++,$i_score++){
						$iNew=$i;if($iNew!=$iOld){$numrsList++; $iOld=$iNew;}
						$sub_question = $Iaverage = '';

						$avg_this_question = 0;
						$Ilist_score = $signList = '';


						for($m=1;$m<=$lastMonth;$m++){
							if($big_ans[$m][$i_score] != ''){
								$show_answer = $big_ans[$m][$i_score]/$total_people_answer[$m][$i_score];
								$Ilist_score  .= $signList.sprintf('%.2f',$show_answer);$signList = ',';  // INSERT
							}else{
								$show_answer = 0;
								$Ilist_score  .= $signList.'0.00';$signList = ',';  // INSERT
							}
							$avg_this_question += $show_answer*$count_sell[$m];
							$sum_this_month[$i][$m] += $show_answer;

						}
						if(array_sum($count_sell)!=0) $avg_this_question = $avg_this_question/array_sum($count_sell);
						else $avg_this_question =0;

						$Imain_question_id = $main_question_id[$i]; // INSERT
						$Ilist_question_id = $list_question_id[$i][$j]; // INSERT
						$Iweight = $list_weight[$i][$j]; // INSERT
						$Iaverage = sprintf('%.2f',$avg_this_question); // INSERT
						$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
												VALUES ('$report_ssi_Year', '$sell_brn[sell_branch]', '$Iform_id', '$Imain_question_id', '$Ilist_question_id', '0', '$Iweight', '$Ilist_score', '$Iaverage' , '3', '2', '', '$today_date' )";
						$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					}
					$avg_this_main = 0;
					$sign_main = $Ilist_main = '';
					for($m=1;$m<=$lastMonth;$m++){
						$main_ques_total = $sum_this_month[$i][$m]/$title[$i];
						if($main_ques_total != 0){
							$Ilist_main .= $sign_main.sprintf('%.2f',$main_ques_total);$sign_main = ',';  // INSERT
						}else{
							$Ilist_main .= $sign_main.'0.00';$sign_main = ',';  // INSERT
						}
						$avg_this_main += $main_ques_total*$count_sell[$m];

					}

					if(array_sum($count_sell)!=0) $avg_this_main = $avg_this_main/array_sum($count_sell);
					else $avg_this_main = 0;
					$Iaverage_main = sprintf('%.2f',$avg_this_main); // INSERT

					$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
												VALUES ('$report_ssi_Year', '$sell_brn[sell_branch]', '$Iform_id', '$Imain_question_id', '', '0', '$Iweight_main', '$Ilist_main', '$Iaverage_main' , '2', '2', '', '$today_date' )";
					$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				}

				$list_question = $pre_LIST_QUESTION;
				$btn_id = $ssi_form[ssi_id]; // id ปุ่ม Download

				$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$sell_brn[sell_branch]', '$Iform_id', '', '', '1', '', '$Ilist_pleasure', '$Iaverage_pleasure' , '1', '2', '', '$today_date' )";
				$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$sell_brn[sell_branch]', '$Iform_id', '', '', '2', '', '$Ilist_interview', '' , '1', '2', '', '$today_date' )";
				$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$sell_brn[sell_branch]', '$Iform_id', '', '', '3', '', '$Ilist_score_max', '' , '1', '2', '', '$today_date' )";
				$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$sell_brn[sell_branch]', '$Iform_id', '', '', '4', '', '$Ilist_per_per', '$Iaverage_per_per' , '1', '2', '', '$today_date' )";
				$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );


			}// END FORM TYPE = A

			///////////***************B***************////////////////////
			else{ // IF its type is = B
				$list_question_id 			= array();
				$Iaverage_pleasure			=
				$Iaverage_per_per			=
				$Ilist_pleasure				=
				$Ilist_interview 			=
				$Ilist_score_max			=
				$Ilist_per_per				= '';

				$total_answer_sum =
				$question_list_title =
				$pleasure_of_the_month =
				$percent_perfect =
				$perfect_man =
				$total_people_answer =
				$list_code = array();
				$pleasure = $main_question = '';

				//หา น้ำหนัก title
				$sql = "SELECT ssi_title_id , ssi_title_name, weight
						FROM $config[db_base_name].follow_ssi_title
						WHERE ssi_id ='$ssi_form[ssi_id]'
						ORDER BY ssi_title_id";
				$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				$main_question_id = $title_name = array();
				while($ssi_title = mysql_fetch_assoc( $result)){
					$main_question_id[] = $ssi_title['ssi_title_id']; // INSERT
					$title_name[] = $ssi_title['ssi_title_name'];
				}
				$question_list =
				$title  = array() ;
				$t =
				$t_old =
				$i_list = 0;
				//หา question list เอาไปหา ans
				$sql = "SELECT ssi_ques_id, ssi_title_id, question_list_id, weight, ssi_question_code
						FROM $config[db_base_name].follow_ssi_question_weight
						WHERE ssi_id ='$ssi_form[ssi_id]'
						ORDER BY ssi_title_id, ssi_ques_id";
				$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				while($ssi_weight = mysql_fetch_assoc( $result)){
					$question_list[] =  $ssi_weight['question_list_id']; //เก็บไปค้นคำตอบ

					if($ssi_weight['ssi_title_id'] != $title_id){
						if($t){
							$t+=1;// เลื่อน arr ช่องถัดไปเอาไว้เก็บ คำถามย่อย
						}
					}
					if($ssi_weight['ssi_title_id'] == $title_id||!$title[$t]){
						$title_id = $ssi_weight['ssi_title_id'];//เอาไว้เชค ตอนวนรอบต่อไป

						$list_code[$t][] = $ssi_weight['ssi_question_code'];

						$sql2 = "SELECT list_title
							FROM $config[db_base_name].follow_question_list
							WHERE id ='$ssi_weight[question_list_id]' ";
						$result2 = $logDb->queryAndLogSQL( $sql2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						$ssi_questionlist = mysql_fetch_assoc( $result2);
						$question_list_title[$t][] = $ssi_questionlist['list_title'];
						$list_question_id[$t][] = $ssi_weight['question_list_id']; // INSERT
						$title[$t] +=1;
					} else{
						$t+=1; //ไม่ควรขยับ ตน.
						$title_id = $ssi_weight['ssi_title_id'];//เอาไว้เชค ตอนวนรอบต่อไป
						$sql3 = "SELECT list_title
							FROM $config[db_base_name].follow_question_list
							WHERE id ='$ssi_weight[question_list_id]' ";
						$result3 = $logDb->queryAndLogSQL( $sql3, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						while($ssi_questionlist = mysql_fetch_assoc( $result3)){
							$question_list_title[$t][$i_list] = $ssi_questionlist[list_title];
							$list_question_id[$t][$i_list] = $ssi_weight['question_list_id']; // INSERT
						}
						$list_code[$t][] = $ssi_weight['ssi_question_code'];
						$title[$t] +=1;
					}
				}
				$question_list_id = "'". implode("', '", $question_list). "'";//เก็บไปค้นคำตอบ
				$cusno_old = 0;
				unset($big_ans);
				for($m=1;$m<=$lastMonth;$m++){
					$mth = sprintf('%02d',$m);
					$year_month  = $mth.'-'.$report_ssi_Year;

					$sql_cus = "SELECT follow_ssi_score.cus_no, follow_ssi_score.follow_cus_id_refer ";
					$sql_cus .= "FROM ".$config['db_easysale'].".Sell ";
					$sql_cus .= "INNER JOIN ".$config['db_base_name'].".follow_ssi_score ";
					$sql_cus .= "ON SUBSTRING_INDEX(Sell.cusBuy,'_',1) = follow_ssi_score.cus_no ";
					$sql_cus .= "INNER JOIN ".$config['db_base_name'].".follow_customer ";
					$sql_cus .= "ON follow_ssi_score.follow_cus_id_refer = follow_customer.id ";
					$sql_cus .= "AND Sell.idRef_vihicle = follow_customer.chassi_no_s ";
					$sql_cus .= "WHERE ";
					$sql_cus .= "Sell.Date_Deliver LIKE '%".$year_month."' ";
					$sql_cus .= "AND follow_ssi_score.follow_ssi_form_id = '".$ssi_form['ssi_id']."' ";
					$sql_cus .= "AND Sell.sell_branch = '".$sell_brn['sell_branch']."' ";
					$sql_cus .= "AND follow_ssi_score.follow_cus_id_refer != '' ";
					$sql_cus .= "GROUP BY ";
					$sql_cus .= "follow_ssi_score.follow_cus_id_refer ";
					$result = $logDb->queryAndLogSQL( $sql_cus, " FILE : ".__FILE__." LINE : ".__LINE__."" );

					while($ssi_score = mysql_fetch_assoc($result)){
						$count_sell[$m] = mysql_num_rows($result);

						$total_answer[$m] = 0;
						$count_perfect = 0;

						for($count_list=0;$count_list<count($question_list);$count_list++){  //ต้อง for ถ้า IN ช่องที่ID คำถามซ้ำมันจะข้ามไป

							$sql4 = "SELECT id, REPLACE(answer,'\'','' ) AS answer, use_status ";
							$sql4 .= "FROM ".$config['db_base_name'].".follow_answer ";
							$sql4 .= "WHERE follow_customer_id ='".$ssi_score['follow_cus_id_refer']."' ";
							$sql4 .= "AND ques_list_id = '".$question_list[$count_list]."' ";
							$sql4 .= "AND status !=99 LIMIT 1 ";
							//ques_list_id IN ($question_list_id)
							$result4 = $logDb->queryAndLogSQL( $sql4, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							$answer = mysql_fetch_assoc($result4);

							if($ssi_score[cus_no]!= $cusno_old ){$count_perfect = 0;unset($total_answer[$m]);$cusno_old = $ssi_score[cus_no];$month_old = $year_month;}else{$cusno_old = $ssi_score[cus_no];$month_old = $year_month;}
							
							//ค้นค่าคำตอบ
							$sql5 = "SELECT score_question_list ";
							$sql5 .= "FROM $config[db_base_name].follow_ssi_score ";
							$sql5 .= "WHERE follow_cus_id_refer ='".$ssi_score['follow_cus_id_refer']."' ";
							$sql5 .= "AND follow_answer_id = '".$answer['id']."' LIMIT 1";
							//ques_list_id IN ($question_list_id)
							$result5 = $logDb->queryAndLogSQL( $sql5, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							$score_fetch = mysql_fetch_assoc($result5);

							if($answer['use_status'] != 'no'){
								if($score_fetch['score_question_list'] == 1){

									$count_perfect +=1;//ใช้เทียบ ถ้าจำนวนนี้เท่ากับ จำนวนคำถาม จะเป็นperfect man !!!
								}
								$total_answer_sum[$m] += 1;
								$total_answer[$m] += 1;
								$big_ans[$m][$count_list] +=  $score_fetch['score_question_list'];
								$total_people_answer[$m][$count_list] += 1;

							}else {
								$big_ans[$m][$count_list] +=  0;
							}

						}//for
						if($count_perfect == $total_answer[$m]){

							$perfect_man[$m][] = $ssi_score['cus_no'];
						} //เก็บไอดีคนที่ตอบได้ เพอเฟก!!
					}//ssi csore

					if(isset($big_ans[$m])){
						$percent_perfect[$m] = count($perfect_man[$m])*100/$count_sell[$m];
					}
					//บันทึกคนที่perfect ด้วย
				}//month

				if(!$perfect_man[$m]){$perfect_man[$m] = 0;}

				$arrMonthName = array('ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');

				$avg_per_per = 0;

				$sign_pleasure = $sign_interview = $sign_score_max = $sign_per_per = '';
				//ส่วนหัวของรายงาน สรุป ความพึงพอใจรวม จำนวนสัมภาษ์ คันที่ได้คะแนนเต็ม และ เปอร์เซ็นต์คัที่ได้เต็ม
				for($m=1;$m<=$lastMonth;$m++){

					if($count_sell[$m]){
						$Ilist_interview .= $sign_interview.$count_sell[$m];$sign_interview = ',';  // INSERT
					}else {
						$Ilist_interview .= $sign_interview.'0';$sign_interview = ',';  // INSERT
					}
					if($perfect_man[$m]){
						$Ilist_score_max .= $sign_score_max.count($perfect_man[$m]);$sign_score_max = ',';  // INSERT
					}else {
						$Ilist_score_max .= $sign_score_max.'0';$sign_score_max = ',';  // INSERT
					}
					if($percent_perfect[$m]){
						$Ilist_per_per .= $sign_per_per.sprintf('%.2f',$percent_perfect[$m]);$sign_per_per = ',';  // INSERT
						$avg_per_per += $percent_perfect[$m]*$count_sell[$m];

					}else{
						$Ilist_per_per .= $sign_per_per.'0.00';$sign_per_per = ',';  // INSERT
					}

				}

				//หาจำนวนเดือนที่ มีการคำนวนในแบบฟอร์มนี้ pt 2011-10-27  เพื่อให้ค่าเฉลี่ยมีเฉพาะเดือนที่มีข้อมูลเท่านั้น
				$sql_exist_date = "SELECT COUNT(DISTINCT(SUBSTRING(Date_Deliver,4,2))) AS numMonthExist ";
				$sql_exist_date .= "FROM ".$config['db_easysale'].".Sell ";
				$sql_exist_date .= "INNER JOIN ".$config['db_base_name'].".follow_ssi_score ";
				$sql_exist_date .= "ON SUBSTRING_INDEX(Sell.cusBuy,'_',1) = follow_ssi_score.cus_no ";
				$sql_exist_date .= "INNER JOIN ".$config['db_base_name'].".follow_customer ";
				$sql_exist_date .= "ON follow_ssi_score.follow_cus_id_refer = follow_customer.id ";
				$sql_exist_date .= "AND Sell.idRef_vihicle = follow_customer.chassi_no_s ";
				$sql_exist_date .= "WHERE ";
				$sql_exist_date .= "Sell.Date_Deliver LIKE '%".$report_ssi_Year."' ";
				$sql_exist_date .= "AND follow_ssi_score.follow_ssi_form_id = '".$ssi_form['ssi_id']."' ";
				$sql_exist_date .= "AND Sell.sell_branch = '".$sell_brn['sell_branch']."' ";
				$sql_exist_date .= "AND follow_ssi_score.follow_cus_id_refer != '' ";
				$sql_exist_date .= "GROUP BY ";
				$sql_exist_date .= "follow_ssi_score.follow_cus_id_refer ";
				$re_exs_d = $logDb->queryAndLogSQL( $sql_exist_date, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$count_date_exist = mysql_fetch_assoc($re_exs_d);

				if(!$count_date_exist['numMonthExist']){$count_date_exist['numMonthExist'] = 1;}

				if(array_sum($count_sell)!=0) $avg_per_per = $avg_per_per/array_sum($count_sell);
				else $avg_per_per = 0;
				if($avg_per_per){
					$Iaverage_per_per = sprintf('%.2f',$avg_per_per); // INSERT
				}else {
					$Iaverage_per_per = '0.00'; // INSERT
				}

				$i_score = 0;

				for($i=0;$i<count($title);$i++){
					$sum_this_month = array();
					$sub_question = '';
					for($j=0;$j<$title[$i];$j++,$i_score++){
						$sub_question = $Iaverage = '';
						$avg_this_question = 0;
						$Ilist_score = $signList = '';

						for($m=1;$m<=$lastMonth;$m++){
							if($big_ans[$m][$i_score] != ''){
								$show_answer = $big_ans[$m][$i_score]*100/$total_people_answer[$m][$i_score];
								$Ilist_score  .= $signList.sprintf('%.2f',$show_answer);$signList = ',';  // INSERT
							}else{
								$show_answer = 0;
								$Ilist_score  .= $signList.'0.00';$signList = ',';  // INSERT
							}
							$avg_this_question += $show_answer*$count_sell[$m];
							$sum_this_month[$i][$m] += $show_answer;

						}
						if(array_sum($count_sell)!=0) $avg_this_question = $avg_this_question/array_sum($count_sell);
						else $avg_this_question =0;

						$Imain_question_id = $main_question_id[$i]; // INSERT
						$Ilist_question_id = $list_question_id[$i][$j]; // INSERT
						$Iaverage = sprintf('%.2f',$avg_this_question); // INSERT
						$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
												VALUES ('$report_ssi_Year', '$sell_brn[sell_branch]', '$Iform_id', '$Imain_question_id', '$Ilist_question_id', '0', '', '$Ilist_score', '$Iaverage' , '3', '2', '', '$today_date' )";
						$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

					}
				}

				$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$sell_brn[sell_branch]', '$Iform_id', '', '', '2', '', '$Ilist_interview', '' , '1', '2', '', '$today_date' )";
				$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$sell_brn[sell_branch]', '$Iform_id', '', '', '3', '', '$Ilist_score_max', '' , '1', '2', '', '$today_date' )";
				$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$sell_brn[sell_branch]', '$Iform_id', '', '', '4', '', '$Ilist_per_per', '$Iaverage_per_per' , '1', '2', '', '$today_date' )";
				$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

			}// END FORM TYPE = B
			$COUNT_FORM_BRANCH++;
		}// Form
		$COUNT_BRANCH++;
	}// branch SQL
	//echo nl2br(" $COUNT_FORM_BRANCH form(s), and $COUNT_BRANCH branch(s) are calculated. \n");
	#nongnong insert เพื่อเช็คว่าครบหรือไม่
	$nongnong_insert = "INSERT INTO $config[db_base_name].SSI_Report (year,list_score,remark)
						VALUES ('$report_ssi_Year', 'เสร็จสิ้นการรันข้อมูลของปี รายสาขา','PITSAMAI02' )";
	$logDb->queryAndLogSQL( $nongnong_insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );

} //จบโหมด รายสาขา

####################################################################################################################################
############################################################# EMP ##################################################################
####################################################################################################################################
//echo "Calculation of SSI Emp Report is "; if($RUN_SSI_EMP) echo 'TURN_ON:';else echo nl2br("TURN_OFF \n");
if($RUN_SSI_EMP){ //เช็คการเปิดโหมดให้รันสคริปต์
	$COUNT_FORM_EMP = $COUNT_EMP = 0;

	$sql_e = "SELECT Sell.Sale_Sell ";
	$sql_e .= "FROM ".$config['db_easysale'].".Sell ";
	$sql_e .= "INNER JOIN ".$config['db_base_name'].".follow_ssi_score ";
	$sql_e .= "ON SUBSTRING_INDEX(Sell.cusBuy,'_',1) = follow_ssi_score.cus_no ";
	$sql_e .= "INNER JOIN ".$config['db_base_name'].".follow_customer ";
	$sql_e .= "ON follow_ssi_score.follow_cus_id_refer = follow_customer.id ";
	$sql_e .= "AND Sell.idRef_vihicle = follow_customer.chassi_no_s ";
	$sql_e .= "WHERE follow_ssi_score.id_score != '' ";
	$sql_e .= "AND Sell.Date_Deliver LIKE '%-".$report_ssi_Year."' ";
	$sql_e .= "GROUP BY ";
	$sql_e .= "Sell.Sale_Sell ";
 	$sql_e .= "ORDER BY Sell.Date_Deliver ";
	$result_e = $logDb->queryAndLogSQL( $sql_e, " FILE : ".__FILE__." LINE : ".__LINE__."" );

	while($sell_e = mysql_fetch_assoc($result_e)){
		$emp_id = $sell_e['Sale_Sell'];

		$result_form = $logDb->queryAndLogSQL( $sql_form, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while($ssi_form = mysql_fetch_assoc($result_form)){

			#-#-#-#-#--------- เคลียร์ข้อมูลเดิม ของฟอร์มนี้ --------#-#-#-#
			$sql_clear_list = " DELETE FROM $config[db_base_name].SSI_Report WHERE SSI_Report.year = '$report_ssi_Year' AND SSI_Report.names_code = '$emp_id' AND SSI_Report.form_id = '$ssi_form[ssi_id]'  AND SSI_Report.report_type = '3'";
			$logDb->queryAndLogSQL( $sql_clear_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			#-#-#-#-#----------------------------------------#-#-#-#

			$Iform_id = $ssi_form['ssi_id']; // INSERT
			$count_sell = array();
			// หาเดือนสุดท้ายของปี
			if(date('Y') == $report_ssi_Year){
				$lastMonth = date('m');
			}else {$lastMonth = 12;	}

			////////////////******************* A *****************////////////////
			if($ssi_form['ssi_form_type'] == 'A'){

				$list_question_id 			= array();
				$Iaverage_pleasure			=
				$Iaverage_per_per			=
				$Ilist_pleasure				=
				$Ilist_interview 			=
				$Ilist_score_max			=
				$Ilist_per_per				= '';

				$total_answer_sum =
				$question_list_title =
				$pleasure_of_the_month =
				$percent_perfect =
				$perfect_man =
				$total_people_answer =
				$list_weight = array();

				//หา น้ำหนัก title
				$sql = "SELECT ssi_title_id,ssi_title_name, weight
					FROM $config[db_base_name].follow_ssi_title
					WHERE ssi_id ='$ssi_form[ssi_id]'
					ORDER BY ssi_title_id";
				$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				$main_question_id = $title_name = $title_weight = array();
				while($ssi_title = mysql_fetch_assoc( $result)){
					$main_question_id[] = $ssi_title['ssi_title_id']; // INSERT
					$title_name[] = $ssi_title['ssi_title_name'];
					$title_weight[] = $ssi_title['weight'];
				}

				$question_list =
				$title  = array() ;
				$t =
				$t_old =
				$i_list = 0;
				//หา question list เอาไปหา ans
				$sql = "SELECT ssi_ques_id, ssi_title_id, question_list_id, weight, ssi_question_code
					FROM $config[db_base_name].follow_ssi_question_weight
					WHERE ssi_id ='$ssi_form[ssi_id]'
					ORDER BY ssi_title_id, ssi_ques_id";
				$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				while($ssi_weight = mysql_fetch_assoc( $result)){
					$question_list[] =  $ssi_weight['question_list_id']; //เก็บไปค้นคำตอบ

					if($ssi_weight['ssi_title_id'] != $title_id){
						if($t){
							$t+=1;// เลื่อน arr ช่องถัดไปเอาไว้เก็บ คำถามย่อย
						}
					}

					if($ssi_weight['ssi_title_id'] == $title_id||!$title[$t]){
						$title_id = $ssi_weight['ssi_title_id'];//เอาไว้เชค ตอนวนรอบต่อไป

						$list_weight[$t][] = $ssi_weight['weight'];

						$sql2 = "SELECT list_title
							FROM $config[db_base_name].follow_question_list
							WHERE id ='$ssi_weight[question_list_id]' ";
						$result2 = $logDb->queryAndLogSQL( $sql2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						$ssi_questionlist = mysql_fetch_assoc( $result2);
						$question_list_title[$t][] = $ssi_questionlist['list_title'];
						$list_question_id[$t][] = $ssi_weight['question_list_id']; // INSERT
						$title[$t] +=1;
					} else{
						$t+=1; //ไม่ควรขยับ ตน.
						$title_id = $ssi_weight['ssi_title_id'];//เอาไว้เชค ตอนวนรอบต่อไป
						$sql3 = "SELECT list_title
							FROM $config[db_base_name].follow_question_list
							WHERE id ='$ssi_weight[question_list_id]' ";
						$result3 = $logDb->queryAndLogSQL( $sql3, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						while($ssi_questionlist = mysql_fetch_assoc( $result3)){
							$question_list_title[$t][$i_list] = $ssi_questionlist[list_title];
							$list_question_id[$t][$i_list] = $ssi_weight['question_list_id']; // INSERT
						}
						$list_weight[$t][] = $ssi_weight['weight'];
						$title[$t] +=1;
					}
				}
				$question_list_id = "'". implode("', '", $question_list). "'";//เก็บไปค้นคำตอบ

				unset($big_ans);
				for($m=1;$m<=$lastMonth;$m++){
					$mth = sprintf('%02d',$m);
					$year_month  = $mth.'-'.$report_ssi_Year;

					$sql_cus = "SELECT follow_ssi_score.cus_no, follow_ssi_score.follow_cus_id_refer ";
					$sql_cus .= "FROM ".$config['db_easysale'].".Sell ";
					$sql_cus .= "INNER JOIN ".$config['db_base_name'].".follow_ssi_score ";
					$sql_cus .= "ON SUBSTRING_INDEX(Sell.cusBuy,'_',1) = follow_ssi_score.cus_no ";
					$sql_cus .= "INNER JOIN ".$config['db_base_name'].".follow_customer ";
					$sql_cus .= "ON follow_ssi_score.follow_cus_id_refer = follow_customer.id ";
					$sql_cus .= "AND Sell.idRef_vihicle = follow_customer.chassi_no_s ";
					$sql_cus .= "WHERE ";
					$sql_cus .= "Sell.Date_Deliver LIKE '%".$year_month."' ";
					$sql_cus .= "AND follow_ssi_score.follow_ssi_form_id = '".$ssi_form['ssi_id']."' ";
					$sql_cus .= "AND Sell.Sale_Sell = '".$emp_id."' ";
					$sql_cus .= "AND follow_ssi_score.follow_cus_id_refer != '' ";
					$sql_cus .= "GROUP BY ";
					$sql_cus .= "follow_ssi_score.follow_cus_id_refer ";
					//
					$result_cus = $logDb->queryAndLogSQL( $sql_cus, " FILE : ".__FILE__." LINE : ".__LINE__."" );

					while($ssi_score = mysql_fetch_assoc($result_cus)){
						$count_sell[$m] = mysql_num_rows($result_cus);

						$total_answer[$m] = 0;
						$count_perfect = 0;

						for($count_list=0;$count_list<count($question_list);$count_list++){  //ต้อง for ถ้า IN ช่องที่ID คำถามซ้ำมันจะข้ามไป
							$sql4 = "SELECT REPLACE(answer,'\'','' ) AS answer, use_status ";
							$sql4 .= "FROM ".$config['db_base_name'].".follow_answer ";
							$sql4 .= "WHERE follow_customer_id ='".$ssi_score['follow_cus_id_refer']."' ";
							$sql4 .= "AND ques_list_id = '".$question_list[$count_list]."' ";
							$sql4 .= "AND status !=99 LIMIT 1 ";
							//ques_list_id IN ($question_list_id)
							$result4 = $logDb->queryAndLogSQL( $sql4, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							$answer = mysql_fetch_assoc($result4);
							if($ssi_score[cus_no]!= $cusno_old ){$count_perfect = 0;unset($total_answer[$m]);$cusno_old = $ssi_score[cus_no];$month_old = $year_month;}else{$cusno_old = $ssi_score[cus_no];$month_old = $year_month;}
							if($answer['use_status'] != 'no'){
								if($answer['answer'] == 10){
									$count_perfect +=1;//ใช้เทียบ ถ้าจำนวนนี้เท่ากับ จำนวนคำถาม จะเป็นperfect man !!!
								}
								$total_answer_sum[$m] += 1;
								$total_answer[$m] += 1;
								$big_ans[$m][$count_list] +=  $answer['answer'];
								$total_people_answer[$m][$count_list] += 1;
							}else {
								$big_ans[$m][$count_list] +=  0;
							}

						}//for
						if($count_perfect == $total_answer[$m]){
							$perfect_man[$m][] = $ssi_score['cus_no'];
						}//เก็บไอดีคนที่ตอบได้ เพอเฟก!!
					}//ssi csore
					if(isset($big_ans[$m])){

						$pleasure_of_the_month[$m] = array_sum($big_ans[$m])/$total_answer_sum[$m];
						$percent_perfect[$m] = count($perfect_man[$m])*100/$count_sell[$m];
					}
				}//month

				if(!$perfect_man[$m]){$perfect_man[$m] = 0;}

				$arrMonthName = array('ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');

				$avg_pleasure = 0;
				$avg_per_per = 0;

				//ส่วนหัวของรายงาน สรุป ความพึงพอใจรวม จำนวนสัมภาษ์ คันที่ได้คะแนนเต็ม และ เปอร์เซ็นต์คัที่ได้เต็ม
				$sign_pleasure = $sign_interview = $sign_score_max = $sign_per_per = '';

				for($m=1;$m<=$lastMonth;$m++){
					if($pleasure_of_the_month[$m]){
						$avg_pleasure += $pleasure_of_the_month[$m]*$count_sell[$m];
						$Ilist_pleasure .= $sign_pleasure.sprintf('%.2f',$pleasure_of_the_month[$m]);$sign_pleasure = ',';  // INSERT
					}else {
						$Ilist_pleasure .= $sign_pleasure.'0.00';$sign_pleasure = ',';  // INSERT
					}

					if($count_sell[$m]){
						$Ilist_interview .= $sign_interview.$count_sell[$m];$sign_interview = ',';  // INSERT
					}else {
						$Ilist_interview .= $sign_interview.'0';$sign_interview = ',';  // INSERT
					}
					if($perfect_man[$m]){
						$Ilist_score_max .= $sign_score_max.count($perfect_man[$m]);$sign_score_max = ',';  // INSERT
					}else {
						$Ilist_score_max .= $sign_score_max.'0';$sign_score_max = ',';  // INSERT
					}
					if($percent_perfect[$m]){
						$Ilist_per_per .= $sign_per_per.sprintf('%.2f',$percent_perfect[$m]);$sign_per_per = ',';  // INSERT
						$avg_per_per += $percent_perfect[$m]*$count_sell[$m];

					}else{
						$Ilist_per_per .= $sign_per_per.'0.00';$sign_per_per = ',';  // INSERT
					}

				}

				//หาจำนวนเดือนที่ มีการคำนวนในแบบฟอร์มนี้ pt 2011-10-27  เพื่อให้ค่าเฉลี่ยมีเฉพาะเดือนที่มีข้อมูลเท่านั้น
				$sql_exist_date = "SELECT COUNT(DISTINCT(SUBSTRING(Date_Deliver,4,2))) AS numMonthExist ";
				$sql_exist_date .= "FROM ".$config['db_easysale'].".Sell ";
				$sql_exist_date .= "INNER JOIN ".$config['db_base_name'].".follow_ssi_score ";
				$sql_exist_date .= "ON SUBSTRING_INDEX(Sell.cusBuy,'_',1) = follow_ssi_score.cus_no ";
				$sql_exist_date .= "INNER JOIN ".$config['db_base_name'].".follow_customer ";
				$sql_exist_date .= "ON follow_ssi_score.follow_cus_id_refer = follow_customer.id ";
				$sql_exist_date .= "AND Sell.idRef_vihicle = follow_customer.chassi_no_s ";
				$sql_exist_date .= "WHERE ";
				$sql_exist_date .= "Sell.Date_Deliver LIKE '%".$report_ssi_Year."' ";
				$sql_exist_date .= "AND follow_ssi_score.follow_ssi_form_id = '".$ssi_form['ssi_id']."' ";
				$sql_exist_date .= "AND Sell.Sale_Sell = '".$emp_id."' ";
				$sql_exist_date .= "AND follow_ssi_score.follow_cus_id_refer != '' ";
				$sql_exist_date .= "GROUP BY ";
				$sql_exist_date .= "follow_ssi_score.follow_cus_id_refer ";
				$re_exs_d = $logDb->queryAndLogSQL( $sql_exist_date, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$count_date_exist = mysql_fetch_assoc($re_exs_d);

				if(!$count_date_exist['numMonthExist']){$count_date_exist['numMonthExist'] = 1;}

				if(array_sum($count_sell)!=0){
					$avg_pleasure = $avg_pleasure/array_sum($count_sell);
					$avg_per_per = $avg_per_per/array_sum($count_sell);
				}else{
					$avg_pleasure =
					$avg_per_per = 0;
				}

				$Iaverage_pleasure = sprintf('%.2f',$avg_pleasure); // INSERT

				if($avg_per_per){
					$Iaverage_per_per = sprintf('%.2f',$avg_per_per); // INSERT
				}else {
					$Iaverage_per_per = '0.00'; // INSERT
				}

				$i_score = 0;
				for($i=0;$i<count($title);$i++){
					$Iaverage_main	= $Ilist_main = $Iweight_main = '';
					$Iweight_main = $title_weight[$i];  // INSERT
					$sum_this_month = array();
					$sub_question = '';

					for($j=0;$j<$title[$i];$j++,$i_score++){

						$sub_question = $Iaverage = '';
						$avg_this_question = 0;
						$Ilist_score = $signList = '';

						for($m=1;$m<=$lastMonth;$m++){
							if($big_ans[$m][$i_score] != ''){
								$show_answer = $big_ans[$m][$i_score]/$total_people_answer[$m][$i_score];
								$sub_question .= '<td align="center">'.sprintf('%.2f',$show_answer).'</td>';
								$Ilist_score  .= $signList.sprintf('%.2f',$show_answer);$signList = ',';  // INSERT
							}else{
								$sub_question .= '<td align="center">0</td>';
								$show_answer = 0;
								$Ilist_score  .= $signList.'0.00';$signList = ',';  // INSERT
							}
							$avg_this_question += $show_answer*$count_sell[$m];
							$sum_this_month[$i][$m] += $show_answer;

						}

						if(array_sum($count_sell)!=0) $avg_this_question = $avg_this_question/array_sum($count_sell);
						else $avg_this_question = 0;

						$Imain_question_id = $main_question_id[$i]; // INSERT
						$Ilist_question_id = $list_question_id[$i][$j]; // INSERT
						$Iweight = $list_weight[$i][$j]; // INSERT
						$Iaverage = sprintf('%.2f',$avg_this_question); // INSERT
						$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
												VALUES ('$report_ssi_Year', '$emp_id', '$Iform_id', '$Imain_question_id', '$Ilist_question_id', '0', '$Iweight', '$Ilist_score', '$Iaverage' , '3', '3', '', '$today_date' )";
						$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					}
					$avg_this_main = 0;

					$sign_main = $Ilist_main = '';
					for($m=1;$m<=$lastMonth;$m++){
						$main_ques_total = $sum_this_month[$i][$m]/$title[$i];

						if($main_ques_total != 0){

							$Ilist_main .= $sign_main.sprintf('%.2f',$main_ques_total);$sign_main = ',';  // INSERT
						}else{

							$Ilist_main .= $sign_main.'0.00';$sign_main = ',';  // INSERT
						}
						$avg_this_main += $main_ques_total*$count_sell[$m];
					}

					if(array_sum($count_sell)!=0) $avg_this_main = $avg_this_main/array_sum($count_sell);
					else $avg_this_main = 0;

					$Iaverage_main = sprintf('%.2f',$avg_this_main); // INSERT

					$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
												VALUES ('$report_ssi_Year', '$emp_id', '$Iform_id', '$Imain_question_id', '', '0', '$Iweight_main', '$Ilist_main', '$Iaverage_main' , '2', '3', '', '$today_date' )";
					$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				}

				$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$emp_id', '$Iform_id', '', '', '1', '', '$Ilist_pleasure', '$Iaverage_pleasure' , '1', '3', '', '$today_date' )";
				$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$emp_id', '$Iform_id', '', '', '2', '', '$Ilist_interview', '' , '1', '3', '', '$today_date' )";
				$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$emp_id', '$Iform_id', '', '', '3', '', '$Ilist_score_max', '' , '1', '3', '', '$today_date' )";
				$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$emp_id', '$Iform_id', '', '', '4', '', '$Ilist_per_per', '$Iaverage_per_per' , '1', '3', '', '$today_date' )";
				$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );


			}// END FORM TYPE = A

			///////////***************B***************////////////////////
			else{ // IF its type is = B

				$list_question_id 			= array();
				$Iaverage_pleasure			=
				$Iaverage_per_per			=
				$Ilist_pleasure				=
				$Ilist_interview 			=
				$Ilist_score_max			=
				$Ilist_per_per				= '';

				$total_answer_sum =
				$question_list_title =
				$pleasure_of_the_month =
				$percent_perfect =
				$perfect_man =
				$total_people_answer =
				$list_code = array();
				$pleasure =
				$main_question = '';

				//หา น้ำหนัก title
				$sql = "SELECT ssi_title_id, ssi_title_name, weight
			FROM $config[db_base_name].follow_ssi_title
			WHERE ssi_id ='$ssi_form[ssi_id]'
			ORDER BY ssi_title_id";
				$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$title_name = array();

				while($ssi_title = mysql_fetch_assoc( $result)){
					$main_question_id[] = $ssi_title['ssi_title_id']; // INSERT
					$title_name[] = $ssi_title['ssi_title_name'];

				}
				$question_list =
				$title  = array() ;
				$t =
				$t_old =
				$i_list = 0;
				//หา question list เอาไปหา ans
				$sql = "SELECT ssi_ques_id, ssi_title_id, question_list_id, weight, ssi_question_code
				FROM $config[db_base_name].follow_ssi_question_weight
				WHERE ssi_id ='$ssi_form[ssi_id]'
				ORDER BY ssi_title_id,ssi_ques_id";
				$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				while($ssi_weight = mysql_fetch_assoc( $result)){
					$question_list[] =  $ssi_weight['question_list_id']; //เก็บไปค้นคำตอบ

					if($ssi_weight['ssi_title_id'] != $title_id){
						if($t){
							$t+=1;// เลื่อน arr ช่องถัดไปเอาไว้เก็บ คำถามย่อย
						}
					}

					if($ssi_weight['ssi_title_id'] == $title_id||!$title[$t]){
						$title_id = $ssi_weight['ssi_title_id'];//เอาไว้เชค ตอนวนรอบต่อไป

						$list_code[$t][] = $ssi_weight['ssi_question_code'];

						$sql2 = "SELECT list_title
							FROM $config[db_base_name].follow_question_list
							WHERE id ='$ssi_weight[question_list_id]' ";
						$result2 = $logDb->queryAndLogSQL( $sql2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						$ssi_questionlist = mysql_fetch_assoc( $result2);
						$question_list_title[$t][] = $ssi_questionlist['list_title'];
						$list_question_id[$t][] = $ssi_weight['question_list_id']; // INSERT

						$title[$t] +=1;
					} else{
						$t+=1; //ไม่ควรขยับ ตน.
						$title_id = $ssi_weight['ssi_title_id'];//เอาไว้เชค ตอนวนรอบต่อไป
						$sql3 = "SELECT list_title
							FROM $config[db_base_name].follow_question_list
							WHERE id ='$ssi_weight[question_list_id]' ";
						$result3 = $logDb->queryAndLogSQL( $sql3, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						while($ssi_questionlist = mysql_fetch_assoc( $result3)){
							$question_list_title[$t][$i_list] = $ssi_questionlist[list_title];
							$list_question_id[$t][$i_list] = $ssi_weight['question_list_id']; // INSERT
						}
						$list_code[$t][] = $ssi_weight['ssi_question_code'];
						$title[$t] +=1;
					}
				}
				$question_list_id = "'". implode("', '", $question_list). "'";//เก็บไปค้นคำตอบ

				unset($big_ans);

				for($m=1;$m<=$lastMonth;$m++){
					$mth = sprintf('%02d',$m);
					$year_month  = $mth.'-'.$report_ssi_Year;

					$sql_cus = "SELECT follow_ssi_score.cus_no, follow_ssi_score.follow_cus_id_refer ";
					$sql_cus .= "FROM ".$config['db_easysale'].".Sell ";
					$sql_cus .= "INNER JOIN ".$config['db_base_name'].".follow_ssi_score ";
					$sql_cus .= "ON SUBSTRING_INDEX(Sell.cusBuy,'_',1) = follow_ssi_score.cus_no ";
					$sql_cus .= "INNER JOIN ".$config['db_base_name'].".follow_customer ";
					$sql_cus .= "ON follow_ssi_score.follow_cus_id_refer = follow_customer.id ";
					$sql_cus .= "AND Sell.idRef_vihicle = follow_customer.chassi_no_s ";
					$sql_cus .= "WHERE ";
					$sql_cus .= "Sell.Date_Deliver LIKE '%".$year_month."' ";
					$sql_cus .= "AND follow_ssi_score.follow_ssi_form_id = '".$ssi_form['ssi_id']."' ";
					$sql_cus .= "AND Sell.Sale_Sell = '".$emp_id."' ";
					$sql_cus .= "AND follow_ssi_score.follow_cus_id_refer != '' ";
					$sql_cus .= "GROUP BY ";
					$sql_cus .= "follow_ssi_score.follow_cus_id_refer ";
					//
					$result = $logDb->queryAndLogSQL( $sql_cus, " FILE : ".__FILE__." LINE : ".__LINE__."" );

					while($ssi_score = mysql_fetch_assoc($result)){
						$count_sell[$m] = mysql_num_rows($result);

						$total_answer[$m] = 0;
						$count_perfect = 0;

						for($count_list=0;$count_list<count($question_list);$count_list++){  //ต้อง for ถ้า IN ช่องที่ID คำถามซ้ำมันจะข้ามไป

							$sql4 = "SELECT id, REPLACE(answer,'\'','' ) AS answer, use_status ";
							$sql4 .= "FROM ".$config['db_base_name'].".follow_answer ";
							$sql4 .= "WHERE follow_customer_id ='".$ssi_score['follow_cus_id_refer']."' ";
							$sql4 .= "AND ques_list_id = '".$question_list[$count_list]."' ";
							$sql4 .= "AND status !=99 LIMIT 1 ";
							//ques_list_id IN ($question_list_id)
							$result4 = $logDb->queryAndLogSQL( $sql4, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							$answer = mysql_fetch_assoc($result4);

							if($ssi_score[cus_no]!= $cusno_old ){$count_perfect = 0;unset($total_answer[$m]);$cusno_old = $ssi_score[cus_no];$month_old = $year_month;}else{$cusno_old = $ssi_score[cus_no];$month_old = $year_month;}
							
							//ค้นค่าคำตอบ
							$sql5 = "SELECT score_question_list ";
							$sql5 .= "FROM ".$config['db_base_name'].".follow_ssi_score ";
							$sql5 .= "WHERE follow_cus_id_refer ='".$ssi_score['follow_cus_id_refer']."' ";
							$sql5 .= "AND follow_answer_id = '".$answer['id']."' LIMIT 1";
							//ques_list_id IN ($question_list_id)
							$result5 = $logDb->queryAndLogSQL( $sql5, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							$score_fetch = mysql_fetch_assoc($result5);

							if($answer['use_status'] != 'no'){
								if($score_fetch['score_question_list'] == 1){
									$count_perfect +=1;//ใช้เทียบ ถ้าจำนวนนี้เท่ากับ จำนวนคำถาม จะเป็นperfect man !!!
								}

								$total_answer_sum[$m] += 1;
								$total_answer[$m] += 1;
								$big_ans[$m][$count_list] +=  $score_fetch['score_question_list'];
								$total_people_answer[$m][$count_list] += 1;
							}else {
								$big_ans[$m][$count_list] +=  0;
							}

						}//for

						if($count_perfect == $total_answer[$m]){
							$perfect_man[$m][] = $ssi_score['cus_no'];
						}//เก็บไอดีคนที่ตอบได้ เพอเฟก!!
					}//ssi csore

					if(isset($big_ans[$m])){

						$percent_perfect[$m] = count($perfect_man[$m])*100/$count_sell[$m];
					}

				}//month

				if(!$perfect_man[$m]){$perfect_man[$m] = 0;}

				$arrMonthName = array('ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');

				$avg_per_per = 0;

				//ส่วนหัวของรายงาน สรุป ความพึงพอใจรวม จำนวนสัมภาษ์ คันที่ได้คะแนนเต็ม และ เปอร์เซ็นต์คัที่ได้เต็ม
				$sign_pleasure = $sign_interview = $sign_score_max = $sign_per_per = '';
				for($m=1;$m<=$lastMonth;$m++){

					if($count_sell[$m]){

						$Ilist_interview .= $sign_interview.$count_sell[$m];$sign_interview = ',';  // INSERT
					}else {

						$Ilist_interview .= $sign_interview.'0';$sign_interview = ',';  // INSERT
					}
					if($perfect_man[$m]){

						$Ilist_score_max .= $sign_score_max.count($perfect_man[$m]);$sign_score_max = ',';  // INSERT
					}else {

						$Ilist_score_max .= $sign_score_max.'0';$sign_score_max = ',';  // INSERT
					}
					if($percent_perfect[$m]){

						$avg_per_per += $percent_perfect[$m]*$count_sell[$m];
						$Ilist_per_per .= $sign_per_per.sprintf('%.2f',$percent_perfect[$m]);$sign_per_per = ',';  // INSERT
					}else{

						$Ilist_per_per .= $sign_per_per.'0.00';$sign_per_per = ',';  // INSERT
					}

				}

				//หาจำนวนเดือนที่ มีการคำนวนในแบบฟอร์มนี้ pt 2011-10-27  เพื่อให้ค่าเฉลี่ยมีเฉพาะเดือนที่มีข้อมูลเท่านั้น
				$sql_exist_date = "SELECT COUNT(DISTINCT(SUBSTRING(Date_Deliver,4,2))) AS numMonthExist ";
				$sql_exist_date .= "FROM ".$config['db_base_name'].".follow_ssi_score ";
				$sql_exist_date .= "INNER JOIN ".$config['db_base_name'].".follow_customer ";
				$sql_exist_date .= "ON follow_ssi_score.follow_cus_id_refer = follow_customer.id ";
				$sql_exist_date .= "AND follow_customer.source_db_table = 'Sell' ";
				$sql_exist_date .= "AND follow_customer.source_primary_field = 'Sell_No' ";
				$sql_exist_date .= "INNER JOIN ".$config['db_easysale'].".Sell ";
				$sql_exist_date .= "ON Sell.Sell_No = follow_customer.source_primary_id ";
				$sql_exist_date .= "WHERE ";
				$sql_exist_date .= "Sell.Date_Deliver LIKE '%".$report_ssi_Year."' ";
				$sql_exist_date .= "AND follow_ssi_score.follow_ssi_form_id = '".$ssi_form['ssi_id']."' ";
				$sql_exist_date .= "AND Sell.Sale_Sell = '".$emp_id."' ";
				$sql_exist_date .= "AND follow_ssi_score.follow_cus_id_refer != '' ";
				$sql_exist_date .= "GROUP BY ";
				$sql_exist_date .= "follow_ssi_score.follow_cus_id_refer ";
				$re_exs_d = $logDb->queryAndLogSQL( $sql_exist_date, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$count_date_exist = mysql_fetch_assoc($re_exs_d);

				if(!$count_date_exist['numMonthExist']){$count_date_exist['numMonthExist'] = 1;}

				if(array_sum($count_sell)!=0) $avg_per_per = $avg_per_per/array_sum($count_sell);
				else $avg_per_per = 0;

				if($avg_per_per){

					$Iaverage_per_per = sprintf('%.2f',$avg_per_per); // INSERT
				}else {

					$Iaverage_per_per = sprintf('%.2f',$avg_per_per); // INSERT
				}

				$i_score = 0;

				for($i=0;$i<count($title);$i++){

					$sum_this_month = array();
					$sub_question = '';

					for($j=0;$j<$title[$i];$j++,$i_score++){
						$sub_question = $Iaverage = '';

						$avg_this_question = 0;
						$Ilist_score = $signList = '';

						for($m=1;$m<=$lastMonth;$m++){
							if($big_ans[$m][$i_score] != ''){
								$show_answer = $big_ans[$m][$i_score]*100/$total_people_answer[$m][$i_score];

								$Ilist_score  .= $signList.sprintf('%.2f',$show_answer);$signList = ',';  // INSERT
							}else{

								$show_answer = 0;
								$Ilist_score  .= $signList.'0.00';$signList = ',';  // INSERT
							}

							$avg_this_question += $show_answer*$count_sell[$m];
							$sum_this_month[$i][$m] += $show_answer;

						}

						if(array_sum($count_sell)!= 0 ) $avg_this_question =$avg_this_question/array_sum($count_sell);
						else $avg_this_question=0;

						$Imain_question_id = $main_question_id[$i]; // INSERT
						$Ilist_question_id = $list_question_id[$i][$j]; // INSERT
						$Iaverage = sprintf('%.2f',$avg_this_question); // INSERT
						$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
												VALUES ('$report_ssi_Year', '$emp_id', '$Iform_id', '$Imain_question_id', '$Ilist_question_id', '0', '', '$Ilist_score', '$Iaverage' , '3', '3', '', '$today_date' )";
						$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

					}
				}
				$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$emp_id', '$Iform_id', '', '', '2', '', '$Ilist_interview', '' , '1', '3', '', '$today_date' )";
				$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$emp_id', '$Iform_id', '', '', '3', '', '$Ilist_score_max', '' , '1', '3', '', '$today_date' )";
				$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

				$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date )
											VALUES ('$report_ssi_Year', '$emp_id', '$Iform_id', '', '', '4', '', '$Ilist_per_per', '$Iaverage_per_per' , '1', '3', '', '$today_date' )";
				$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

			}// END FORM TYPE = B
			$COUNT_FORM_EMP++;
		}// Form
		$COUNT_EMP++;
	}// emp SQL
	//echo nl2br(" $COUNT_FORM_EMP form(s), and $COUNT_EMP employee(s) are calculated. \n");
	#nongnong insert เพื่อเช็คว่าครบหรือไม่
	$nongnong_insert = "INSERT INTO $config[db_base_name].SSI_Report (year,list_score,remark)
						VALUES ('$report_ssi_Year', 'เสร็จสิ้นการรันข้อมูลของปี รายพนักงาน','PITSAMAI03' )";
	$logDb->queryAndLogSQL( $nongnong_insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );

} //จบโหมด EMP
 
####################################################################################################################################
############################################################# CUST #################################################################
####################################################################################################################################
//echo "Calculation of SSI Cus Report is "; if($RUN_SSI_CUS) echo 'TURN_ON:';else echo nl2br("TURN_OFF \n");
if($RUN_SSI_CUS){ //เช็คการเปิดโหมดให้รันสคริปต์
	$COUNT_FORM_CUS = $COUNT_CUS = 0;

	$result = $logDb->queryAndLogSQL( $sql_form, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	while($ssi_form = mysql_fetch_assoc($result)){

		#-#-#-#-#--------- เคลียร์ข้อมูลเดิม ของฟอร์มนี้ --------#-#-#-#
		$sql_clear_list = " DELETE FROM $config[db_base_name].SSI_Report WHERE SSI_Report.year LIKE '$report_ssi_Year-%' AND SSI_Report.form_id = '$ssi_form[ssi_id]' AND SSI_Report.report_type = '4'";
		$logDb->queryAndLogSQL( $sql_clear_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		#-#-#-#-#----------------------------------------#-#-#-#

		$Iform_id = $ssi_form['ssi_id'];  // INSERT
		$Iweight_list = $sign_weightList = $iCode_list = $sign_codeList = '';

		$question_list =
		$follow_question_weight_id = array();
		$tr3 = '';

		$sql1 = "SELECT ssi_ques_id, question_list_id, weight, ssi_question_code
			FROM $config[db_base_name].follow_ssi_question_weight
			WHERE ssi_id ='$ssi_form[ssi_id]'
			ORDER BY ssi_ques_id";
		$result1 = $logDb->queryAndLogSQL( $sql1, " FILE : ".__FILE__." LINE : ".__LINE__."" );

		while($ssi_weight = mysql_fetch_assoc( $result1)){
			$question_list[] =  $ssi_weight['question_list_id']; //เก็บไปค้นคำตอบ
			$follow_question_weight_id[] = $ssi_weight['ssi_ques_id'];
			if($ssi_weight['ssi_question_code']){
				$Iweight_list .= $sign_weightList.$ssi_weight['weight']; $sign_weightList =',';
				$iCode_list .= $sign_codeList.$ssi_weight['ssi_question_code']; $sign_codeList = ',';
			}else {
				$Iweight_list .= $sign_weightList.'';  $sign_weightList =',';
				$iCode_list .= $sign_codeList.''; $sign_codeList = ',';
			}

		}//end :sql1

		$f_q_w_id = "'". implode("', '", $follow_question_weight_id). "'";//เก็บไปค้นcus

		$sql2 = "SELECT DISTINCT(follow_ssi_score.follow_cus_id_refer) , 
			Sell.Chassi_No_S, 
			Sell.idRef_vihicle , 
			SUBSTRING_INDEX(Sell.cusBuy,'_',1) AS cusBuy, 
			SUBSTRING_INDEX(Sell.cusBuy,'_',-1) AS cusBuy2, 
			SUBSTRING_INDEX(Sell.SaleNameFull,'_',-1) AS SaleNameFull, 
			Sell.sell_branch, 
			Sell.Sale_Sell,  
			Sell.Date_Deliver, 
			follow_ssi_score.score_form 
			FROM $config[db_easysale].Sell 
			RIGHT JOIN $config[db_base_name].follow_ssi_score 
			ON SUBSTRING_INDEX(Sell.cusBuy,'_',1) = follow_ssi_score.cus_no 
			INNER JOIN $config[db_base_name].follow_customer 
			ON follow_ssi_score.follow_cus_id_refer = follow_customer.id 
			AND Sell.idRef_vihicle = follow_customer.chassi_no_s 
			WHERE follow_ssi_score.follow_ssi_question_weight_id IN ($f_q_w_id) 
			AND follow_ssi_score.follow_ssi_form_id = '$ssi_form[ssi_id]' 
			AND Sell.Date_Deliver LIKE '%-$report_ssi_Year' 
			AND Sell.Sale_Sell LIKE '%' 
			AND follow_ssi_score.follow_cus_id_refer != '' 
			GROUP BY follow_ssi_score.follow_cus_id_refer  
			ORDER BY Sell.Sell_No ";

		$result2 = $logDb->queryAndLogSQL( $sql2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$details = '';
		// เพิ่มลงมาอีก1 Row
		if(mysql_num_rows($result2)){
			while($cus_score = mysql_fetch_assoc($result2)){
				$Iaverage_score_form =  '';
				$Ilist_score = $iCode_list.'/SPLIT/';
				$sign_listScore = '';

				$sqlComp = "SELECT name FROM ".$table_config['working_company']." WHERE id = '".$cus_score['sell_branch']."' LIMIT 1";
				$queComp = $logDb->queryAndLogSQL( $sqlComp, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$feComp  = mysql_fetch_assoc($queComp);
				$cus_score['SaleNameFull']	= $feComp['name'];

				$Iemp_id = $cus_score['Sale_Sell'].'/SPLIT/'.$cus_score['SaleNameFull'];

				$IcusBuy = $cus_score['cusBuy'].'/SPLIT/'.$cus_score['cusBuy2'];

				$Sell_Date_ = explode('-',$cus_score['Date_Deliver']);  // INSERT
				$Sell_Date = $Sell_Date_[2].'-'.$Sell_Date_[1].'-'.$Sell_Date_[0];

				for($i=0;$i<count($question_list);$i++){ // ใช้ for เพราะจะมีกรณีที่ในฟอร์มนั้นมีคำถามไอดีซ้ำกัน ใช้ IN($question_list_id) จะไม่ได้ตัวซ้ำ

					if($ssi_form['ssi_form_type'] == 'A'){
						$sql3 = "SELECT REPLACE(answer,'\'','' ) AS answer, use_status 
								FROM $config[db_base_name].follow_answer
								WHERE ques_list_id = '$question_list[$i]' 
								AND follow_customer_id = '$cus_score[follow_cus_id_refer]' 
								ORDER BY ques_list_id";

						$result3 = $logDb->queryAndLogSQL( $sql3, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						$answer = mysql_fetch_assoc( $result3);
						if($answer['use_status'] == 'no'){
							$Ilist_score .= $sign_listScore.'-';$sign_listScore = ',';
						}
						else{
							$Ilist_score .= $sign_listScore.$answer['answer'];$sign_listScore = ',';
						}
					}// end A

					else if($ssi_form['ssi_form_type'] == 'B'){
						$sql3 = "SELECT REPLACE(answer,'\'','' ) AS answer, use_status, follow_ssi_score.score_question_list 
								FROM $config[db_base_name].follow_answer JOIN $config[db_base_name].follow_ssi_score ON follow_answer.id = follow_ssi_score.follow_answer_id
								WHERE ques_list_id = '$question_list[$i]' 
								AND follow_customer_id = '$cus_score[follow_cus_id_refer]' 
								ORDER BY ques_list_id";
						$result3 = $logDb->queryAndLogSQL( $sql3, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						$answer = mysql_fetch_assoc( $result3);

						if($answer['use_status'] == 'no'){
							$Ilist_score .= $sign_listScore.'-';$sign_listScore = ',';
						}
						else{
							$Ilist_score .= $sign_listScore.$answer['score_question_list'];$sign_listScore = ',';
						}
					}
				}//for			
				$Iaverage_score_form = sprintf('%.2f',$cus_score['score_form']);

				$sql_insert_list = "INSERT INTO $config[db_base_name].SSI_Report (year, names_code, form_id, main_question_id, list_question_id, head_sum, weight, list_score, average, type_of_list, report_type, status, process_date, remark  )
									VALUES ('$Sell_Date', '$Iemp_id', '$Iform_id', '', '', '0', '$Iweight_list', '$Ilist_score', '$Iaverage_score_form' , '3', '4', '', '$today_date', '$IcusBuy' )";
				$logDb->queryAndLogSQL( $sql_insert_list, " FILE : ".__FILE__." LINE : ".__LINE__."" );

			}//end :sql2
			$COUNT_CUS++;
		}//CUS
		$COUNT_FORM_CUS++;
	}// end :sql
	//echo nl2br(" $COUNT_FORM_CUS form(s), and $COUNT_CUS customer(s) are calculated. \n");
	#nongnong insert เพื่อเช็คว่าครบหรือไม่
	$nongnong_insert = "INSERT INTO $config[db_base_name].SSI_Report (year,list_score,remark)
						VALUES ('$report_ssi_Year', 'เสร็จสิ้นการรันข้อมูลของปี รายพนักงาน/ลูกค้า','PITSAMAI04' )";
	$logDb->queryAndLogSQL( $nongnong_insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );

}//จบโหมด CUS

CloseDB();
?>