<?php

set_time_limit(0);
ini_set('memory_limit', '-1');

header('Content-Type: text/html; charset=utf-8');

define('ROOT_PATH', dirname(dirname(dirname(__FILE__))));
define('CRON_PATH', dirname(dirname(__FILE__)));

define('DEBUG', false);			// true = echo SQL command, execute only SELECT statement

$SAVE_LOG 			= true;

$UP_VEHICLE_INFO	= true; 	// false = skip update in 'VEHICLE_INFO' table
$UP_OWNER			= true;		// false = skip insert/update 'เจ้าของ' in 'VEHICLE_RELATIONSHIP' table
$UP_USER 			= true;		// false = skip insert/update 'ผู้ใช้' in 'VEHICLE_RELATIONSHIP' table
$UP_DRIVER 			= true;		// false = skip insert/update 'คนขับ' in 'VEHICLE_RELATIONSHIP' table
$UP_CUS_REPAIR 		= true;		// false = skip insert/update 'ผู้นำรถมาซ่อม' in 'VEHICLE_RELATIONSHIP' table
$UP_CUS_RECEIVE_CAR = true;		// false = skip insert/update 'ผู้มารับรถ' in 'VEHICLE_RELATIONSHIP' table

$mainFunc = ROOT_PATH.'/ERP_masterdata/inc/main_function.php';
$pathlogs = ROOT_PATH.'/ERP_crm/files/log_upmain/';
if (!file_exists($mainFunc)) {
	$mainFunc = CRON_PATH.'/ERP_masterdata/inc/main_function.php';
	$pathlogs = CRON_PATH.'/ERP_crm/files/log_upmain/';
}

require_once $mainFunc;

$configMain = new configAllDatabase();
$configMain->calConfigAllDatabase();
$config = $configMain->con;

// $config['ServerName'] 	= '10.0.0.247';
// $config['UserName']		= 'root';
// $config['UserPassword'] = '123456';

// $config['db_service'];
// $config['db_vehicle'];

// echo "<pre>";print_r($config);echo "</pre>";exit();

$db = new mysqli($config['ServerName'], $config['UserName'], $config['UserPassword']);
if ($db->connect_error) {
	die ($db->connect_error." FILE: ".$_SERVER['PHP_SELF']." LINE: ".__LINE__);
}
$db->query("SET NAMES UTF8");

$date = date('Y-m-d');
$time = date('Y-m-d H:i:s');

if ($SAVE_LOG) {
	if (!is_dir($pathlogs)) {
		mkdir($pathlogs, 0777, true);
		chmod($pathlogs, 0777);
	}

	$filelogs = $pathlogs.$date.'.csv';
	$savelogs = fopen($filelogs, 'a+');
	if (!$savelogs) {
		$SAVE_LOG = false;
	}else {
		chmod($filelogs, 0777);
		$save = array(date('Y-m-d H:i:s'), 'START '.$config['UserName'].'@'.$config['ServerName']);
		if ($SAVE_LOG) {
			fputcsv($savelogs, $save);
		}
	}
}

/**
################################################## FUNCTION ##################################################
**/

function getCustomerById($cusId) {
	global $config, $db;

	$result = 0;

	if ($cusId > 0 && $cusId != '') {

		$sql_cus = "SELECT cus_id FROM ".$config['db_service'].".customer ";
		$sql_cus.= "WHERE status <> '99' AND cus_id > 0 AND id = '".$cusId."' LIMIT 1 ";

		if (DEBUG) {
			echo "<br />" . $sql_cus . "<br />";
		}
		$que_cus = $db->query($sql_cus);
		if (!$que_cus) {
			die ($db->error . " (" . $sql_cus . ") " . "FILE: " . $_SERVER['PHP_SELF'] . " LINE: " . __LINE__);
		}

		$num_cus = $que_cus->num_rows;
		if ($num_cus > 0) {
			$res_cus 	= $que_cus->fetch_assoc();
			$result 	= $res_cus['cus_id'];
		}

	}

	return $result;
}

function getVehicleCompany($asell_com_id) {
	global $config, $db;

	$sql_com = "SELECT idRef_vihicle FROM ".$config['db_service'].".asell_company ";
	$sql_com.= "WHERE status <> '99' AND id = '".$asell_com_id."' LIMIT 1 ";

	if (DEBUG) {
		echo "<br />" . $sql_com . "<br />";
	}
	$que_com = $db->query($sql_com);
	if (!$que_com) {
		die ($db->error . " (" . $sql_com . ") " . "FILE: " . $_SERVER['PHP_SELF'] . " LINE: " . __LINE__);
	}

	$result = 0;
	$num_com = $que_com->num_rows;
	if ($num_com > 0) {
		$res_com 	= $que_com->fetch_assoc();
		$result 	= $res_com['idRef_vihicle'];
	}

	return $result;
}

function isExistsVehicleInfo($idRef_vihicle) {
	global $config, $db;

	$sql_vi = "SELECT 1 FROM ".$config['db_vehicle'].".VEHICLE_INFO ";
	$sql_vi.= "WHERE VEHICLE_STATUS <> '99' AND Date_Deliver = '0000-00-00' ";
	$sql_vi.= "AND VEHICLE_ID = '".$idRef_vihicle."' LIMIT 1 ";

	if (DEBUG) {
		echo "<br />" . $sql_vi . "<br />";
	}
	$que_vi = $db->query($sql_vi);
	if (!$que_vi) {
		die ($db->error . " (" . $sql_vi . ") " . "FILE: " . $_SERVER['PHP_SELF'] . " LINE: " . __LINE__);
	}

	$num_vi = $que_vi->num_rows;
	if ($num_vi > 0) {
		return true;
	}else {
		return false;
	}

}

function getRelatesByVehicleId($RELATES_VEHICLE_ID, $RELATES_TYPE) {
	global $config, $db;

	$result = array();

	$sql_rl = "SELECT RELATES_ID, RELATES_CUS_ID, Date_Deliver ";
	$sql_rl.= "FROM ".$config['db_vehicle'].".VEHICLE_RELATIONSHIP ";
	$sql_rl.= "WHERE RELATES_STATUS <> '99' AND RELATES_VEHICLE_ID = '".$RELATES_VEHICLE_ID."' ";
	$sql_rl.= "AND RELATES_TYPE = '".$RELATES_TYPE."' ";

	if (DEBUG) {
		echo "<br />" . $sql_rl . "<br />";
	}
	$que_rt = $db->query($sql_rl);
	if (!$que_rt) {
		die ($db->error . " (" . $sql_rl . ") " . "FILE: " . $_SERVER['PHP_SELF'] . " LINE: " . __LINE__);
	}

	$num_rt = $que_rt->num_rows;
	if ($num_rt > 0) {
		while ($res_rt = $que_rt->fetch_assoc()) {
			$result[] = $res_rt;
		}
	}

	return $result;
}

function update_data($tbName = null, $data = array(), $where = null) {
	global $config, $db;

	$result = false;

	if ($tbName !== null && count($data) > 0 && $where !== null) {

		$updates = array();
		foreach ($data as $field => $value) {
			$updates[] = $field . " = '" . $value . "'";
		}

		if (count($updates) > 0) {
			$sql_up = "UPDATE " . $tbName . " SET " . implode(', ', $updates) . " WHERE " . $where;

			if (DEBUG) {
				echo "<br />" . $sql_up . "<br />";
				$result = true;
			}else {
				if ($db->query($sql_up)) {
					$result = true;
				}
			}
		}

	}

	return $result;
}

function insert_data($tbName = null, $data = array()) {
	global $config, $db;

	$fields = array();
	$values = array();

	$result = false;

	if ($tbName !== null && count($data) > 0) {
		foreach ($data as $field => $value) {
			$fields[] = $field;
			$values[] = $value;
		}
	}
	if (count($fields) > 0 && count($values) > 0) {
		$sql_in = "INSERT INTO ".$tbName." (".implode(', ', $fields).") VALUES ('".implode('\', \'', $values)."')";
		if (DEBUG) {
			echo "<br />" . $sql_in . "<br />";
			$result = true;
		}else {
			if ($db->query($sql_in)) {
				$result = $db->insert_id;
			}
		}
	}

	return $result;
}

function getSellData(){
	global $config, $db;
	
	$sql = "SELECT 
				Sell_No, CusNo, cusBuy, idRef_vihicle 
			FROM ".$config['db_easysale'].".Sell 
			WHERE 
				idRef_vihicle > 0 ";
	$results = $db->query($sql) or die (__LINE__.":".$db->error);

	$arraySell  = array();

	while($row = $results->fetch_assoc())	
	{
		list($cusNo, $CusInfo) = explode('_',$row['cusBuy']);
		
		$arraySell[$row['idRef_vihicle']] = $cusNo;
	}
	
	return $arraySell;
}

function getAhistoryData(){
	global $config, $db;
	
	$sql = "SELECT 
			id, owner_id, cusNo_owner, idRef_vihicle, 
			chassis, full_chassis, engin_id, full_engine
		FROM
			".$config['db_service'].".ahistory
		WHERE
			idRef_vihicle > 0
			AND (owner_id = '' OR (owner_id IN(20040) AND cusNo_owner IN (3900002)) ) ";
  	$results = $db->query($sql) or die (__LINE__.":".$db->error);
		
	$arrayAhistory  = array();

	while($row = $results->fetch_assoc())	
	{
		$arrayAhistory[$row['idRef_vihicle']] = $row['id'];
	}
		
	return $arrayAhistory;
}

function getCustomerData($arraySellData = array()){
	global $config, $db;
	
	$sql = "SELECT
				id, cus_id, `status`
			FROM
				".$config['db_service'].".customer
			WHERE
				`status` <> 99 AND 
				cus_id IN (" . implode(',',$arraySellData) . ") ";
	$results = $db->query($sql) or die (__LINE__.":".$db->error);
  
	$arrayCust  = array();

	while($row = $results->fetch_assoc())	
	{
		$arrayCust[$row['cus_id']] = $row['id'];
	}
		
	return $arrayCust;
}
/**
################################################## START SALE ##################################################
**/

$arraySellData = getSellData();
$arrayAhistoryData = getAhistoryData();

$arrayCustData = getCustomerData( $arraySellData );
$i = 0;
foreach($arrayAhistoryData as $key => $dt){

	if(isset($arraySellData[$key])){
		//$sql  = " UPDATE ".$config['db_service'].".ahistory ";
		//$sql .= " SET owner_id = '".$arrayCustData[$arraySellData[$key]]."', cusNo_owner = '".$arraySellData[$key]."' ";
		//$sql .= " WHERE id = '".$arrayAhistoryData[$key]."' ";
      
        $sql  = " UPDATE ".$config['db_service'].".ahistory ";
		$sql .= " SET owner_id = '".$arrayCustData[$arraySellData[$key]]."', cusNo_owner = '".$arraySellData[$key]."', ";
		$sql .= "  user_id = '".$arrayCustData[$arraySellData[$key]]."', cusNo_user = '".$arraySellData[$key]."', ";
		$sql .= "  cus_id = '".$arrayCustData[$arraySellData[$key]]."', cusNo = '".$arraySellData[$key]."', ";
		$sql .= "  driver_id = '".$arrayCustData[$arraySellData[$key]]."', cusNo_driver = '".$arraySellData[$key]."', ";
		$sql .= "  cus_repair = '".$arrayCustData[$arraySellData[$key]]."', ";
		$sql .= "  cus_receive_car = '".$arrayCustData[$arraySellData[$key]]."' ";
		$sql .= " WHERE id = '".$arrayAhistoryData[$key]."' ";
		
		$results = $db->query($sql) or die (__LINE__.":".$db->error);
		
	}
}

/**
################################################## START SERVICE ##################################################
**/

$sql_ahis = "SELECT id, owner_id, user_id, driver_id, cus_repair, cus_receive_car, asell_com_id, ";
$sql_ahis.= "FROM_UNIXTIME(deliver_day, '%Y-%m-%d') AS deliver_day, idRef_vihicle ";
$sql_ahis.= "FROM ".$config['db_service'].".ahistory ";
$sql_ahis.= "WHERE status <> '99' AND idRef_vihicle > 0 AND deliver_day > 0 ";
$sql_ahis.= "AND (CAST(owner_id AS UNSIGNED) > 0 OR CAST(user_id AS UNSIGNED) > 0 OR CAST(driver_id AS UNSIGNED) > 0 OR cus_repair > 0 OR cus_receive_car > 0) ";
$sql_ahis.= "GROUP BY idRef_vihicle HAVING COUNT(id) = 1 ORDER BY id ASC ";
// $sql_ahis.= "LIMIT 1000 ";

// echo $sql_ahis;exit();

$que_ahis = $db->query($sql_ahis);
if (!$que_ahis) {
	die ($db->error . " (" . $sql_ahis . ") " . "FILE: " . $_SERVER['PHP_SELF'] . " LINE: " . __LINE__);
}

$num_ahis = $que_ahis->num_rows;

// echo $num_ahis;

$ahis_arr = array();

if ($num_ahis > 0) {
	while ($res_ahis = $que_ahis->fetch_assoc()) {
		$ahis_arr[]				= array(
			'ahis_id'			=> $res_ahis['id'],
			'owner' 			=> getCustomerById($res_ahis['owner_id']),
			'user' 				=> getCustomerById($res_ahis['user_id']),
			'driver' 			=> getCustomerById($res_ahis['driver_id']),
			'cus_repair' 		=> getCustomerById($res_ahis['cus_repair']),
			'cus_receive_car' 	=> getCustomerById($res_ahis['cus_receive_car']),
			'asell_com_id'		=> getVehicleCompany($res_ahis['asell_com_id']),
			'deliver_day'		=> $res_ahis['deliver_day'],
			'idRef_vihicle'		=> $res_ahis['idRef_vihicle']
		);
	}
}

// echo "<pre>";
// print_r($ahis_arr);
// echo "</pre>";
$count_ahis = count($ahis_arr);
if ($count_ahis > 0) {
	for ($i=0; $i<$count_ahis; $i++) {

		// echo "<pre>";
		// print_r($ahis_arr[$i]);
		// echo "</pre>";

		if ($SAVE_LOG) {
			$save = array(date('Y-m-d H:i:s'), 'AHIS id='.$ahis_arr[$i]['ahis_id'].', VEHICLE id='.$ahis_arr[$i]['idRef_vihicle']);
			fputcsv($savelogs, $save);
		}

		if (isExistsVehicleInfo($ahis_arr[$i]['idRef_vihicle']) && ($ahis_arr[$i]['owner'] > 0 || $ahis_arr[$i]['user'] > 0 || $ahis_arr[$i]['driver'] > 0 || $ahis_arr[$i]['cus_repair'] > 0 || $ahis_arr[$i]['cus_receive_car'] > 0)) {
			// UPDATE
			if ($UP_VEHICLE_INFO) {
				$tbName = $config['db_vehicle'].".VEHICLE_INFO";
				$data 	= array(
					'Date_Deliver' => $ahis_arr[$i]['deliver_day']
				);
				$where 	= "VEHICLE_ID = '".$ahis_arr[$i]['idRef_vihicle']."' ";

				update_data($tbName, $data, $where);
				// if ($SAVE_LOG) {
				// 	$save = array(date('Y-m-d H:i:s'), 'UPDATE VEHICLE_INFO.Date_Deliver = '.$ahis_arr[$i]['deliver_day'], 'WHERE '.$where, 'LINE: '.__LINE__);
				// 	fputcsv($savelogs, $save);
				// }
			}

			if ($UP_OWNER) {
				/**
				เจ้าของ
				**/
				if ($ahis_arr[$i]['owner'] > 0) {
					
					$relatesInfo 		= getRelatesByVehicleId($ahis_arr[$i]['idRef_vihicle'], 'เจ้าของ');
					$num_relatesInfo 	= count($relatesInfo);

					// echo "<pre>";
					// print_r($relatesInfo);
					// echo "</pre>";

					if ($num_relatesInfo > 0) {

						if ($num_relatesInfo == 1) {

							// CHECK Cus_Id
							if ($relatesInfo[0]['RELATES_CUS_ID'] == $ahis_arr[$i]['owner']) {
								// Cus_No Is true
								// CHECK Date_Deliver
								if ($relatesInfo[0]['Date_Deliver'] == '0000-00-00') {
									// UPDATE Date_Deliver
									$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
									$data 	= array(
										'Date_Deliver' 	=> $ahis_arr[$i]['deliver_day'],
										'RELATES_MARKS'	=> $_SERVER['PHP_SELF']
									);
									$where 	= "RELATES_ID = '".$relatesInfo[0]['RELATES_ID']."' ";

									update_data($tbName, $data, $where);
									// if ($SAVE_LOG) {
									// 	$save = array(date('Y-m-d H:i:s'), 'UPDATE VEHICLE_RELATIONSHIP.Date_Deliver = '.$ahis_arr[$i]['deliver_day'], 'WHERE '.$where, 'LINE: '.__LINE__);
									// 	fputcsv($savelogs, $save);
									// }

								}

							}else {
								// Cus_No Is false
								// UPDATE99
								$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
								$data 	= array(
									'RELATES_STATUS' 	=> '99',
									'RELATES_MARKS'		=> $_SERVER['PHP_SELF']
								);
								$where 	= "RELATES_ID = '".$relatesInfo[0]['RELATES_ID']."' ";

								update_data($tbName, $data, $where);
								// if ($SAVE_LOG) {
								// 	$save = array(date('Y-m-d H:i:s'), 'UPDATE VEHICLE_RELATIONSHIP.RELATES_STATUS = 99', 'WHERE '.$where);
								// }

								// INSERT
								$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
								$data 	= array(
									'RELATES_VEHICLE_ID' 	=> $ahis_arr[$i]['idRef_vihicle'],
									'RELATES_CUS_ID'		=> $ahis_arr[$i]['owner'],
									'RELATES_TYPE'			=> 'เจ้าของ',
									'RELATES_COMP_ID'		=> $ahis_arr[$i]['asell_com_id'],
									'RELATES_MARKS'			=> $_SERVER['PHP_SELF'],
									'Date_Deliver'			=> $ahis_arr[$i]['deliver_day']
								);
								$res_insert_id = insert_data($tbName, $data);
								// if ($SAVE_LOG) {
								// 	array_push($save, 'INSERT VEHICLE_RELATIONSHIP', 'INSERT id='.$res_insert_id, 'LINE: '.__LINE__);
								// 	fputcsv($savelogs, $save);
								// }
							}

						}else {
							// MULTIROW -> DULP -> UPDATE99 -> INSERT
							// if ($SAVE_LOG) {
							// 	$save = array(date('Y-m-d H:i:s'));
							// }
							for ($x=0; $x<$num_relatesInfo; $x++) {
								// UPDATE99
								$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
								$data 	= array(
									'RELATES_STATUS' 	=> '99',
									'RELATES_MARKS'		=> $_SERVER['PHP_SELF']
								);
								$where 	= "RELATES_ID = '".$relatesInfo[$x]['RELATES_ID']."' ";

								update_data($tbName, $data, $where);
								// if ($SAVE_LOG) {
								// 	array_push($save, 'UPDATE VEHICLE_RELATIONSHIP.RELATES_STATUS = 99', 'WHERE '.$where);
								// }
							}
							// INSERT
							$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
							$data 	= array(
								'RELATES_VEHICLE_ID' 	=> $ahis_arr[$i]['idRef_vihicle'],
								'RELATES_CUS_ID'		=> $ahis_arr[$i]['owner'],
								'RELATES_TYPE'			=> 'เจ้าของ',
								'RELATES_COMP_ID'		=> $ahis_arr[$i]['asell_com_id'],
								'RELATES_MARKS'			=> $_SERVER['PHP_SELF'],
								'Date_Deliver'			=> $ahis_arr[$i]['deliver_day']
							);
							$res_insert_id = insert_data($tbName, $data);
							// if ($SAVE_LOG) {
							// 	array_push($save, 'INSERT VEHICLE_RELATIONSHIP', 'INSERT id='.$res_insert_id, 'LINE: '.__LINE__);
							// 	fputcsv($savelogs, $save);
							// }

						}

					}else {
						// INSERT
						$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
						$data 	= array(
							'RELATES_VEHICLE_ID' 	=> $ahis_arr[$i]['idRef_vihicle'],
							'RELATES_CUS_ID'		=> $ahis_arr[$i]['owner'],
							'RELATES_TYPE'			=> 'เจ้าของ',
							'RELATES_COMP_ID'		=> $ahis_arr[$i]['asell_com_id'],
							'RELATES_MARKS'			=> $_SERVER['PHP_SELF'],
							'Date_Deliver'			=> $ahis_arr[$i]['deliver_day']
						);
						$res_insert_id = insert_data($tbName, $data);
						// if ($SAVE_LOG) {
						// 	$save = array(date('Y-m-d H:i:s'), 'INSERT VEHICLE_RELATIONSHIP', 'INSERT id='.$res_insert_id, 'LINE: '.__LINE__);
						// 	fputcsv($savelogs, $save);
						// }
					}

				}
			}

			if ($UP_USER) {
				/**
				ผู้ใช้
				**/
				if ($ahis_arr[$i]['user'] > 0) {
					
					$relatesInfo 		= getRelatesByVehicleId($ahis_arr[$i]['idRef_vihicle'], 'ผู้ใช้');
					$num_relatesInfo 	= count($relatesInfo);

					// echo "<pre>";
					// print_r($relatesInfo);
					// echo "</pre>";

					if ($num_relatesInfo > 0) {

						if ($num_relatesInfo == 1) {

							// CHECK Cus_Id
							if ($relatesInfo[0]['RELATES_CUS_ID'] == $ahis_arr[$i]['user']) {
								// Cus_No Is true
								// CHECK Date_Deliver
								if ($relatesInfo[0]['Date_Deliver'] == '0000-00-00') {
									// UPDATE Date_Deliver
									$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
									$data 	= array(
										'Date_Deliver' 	=> $ahis_arr[$i]['deliver_day'],
										'RELATES_MARKS'	=> $_SERVER['PHP_SELF']
									);
									$where 	= "RELATES_ID = '".$relatesInfo[0]['RELATES_ID']."' ";

									update_data($tbName, $data, $where);
									// if ($SAVE_LOG) {
									// 	$save = array(date('Y-m-d H:i:s'), 'UPDATE VEHICLE_RELATIONSHIP.Date_Deliver = '.$ahis_arr[$i]['deliver_day'], 'WHERE '.$where, 'LINE: '.__LINE__);
									// 	fputcsv($savelogs, $save);
									// }

								}

							}else {
								// Cus_No Is false
								// UPDATE99
								$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
								$data 	= array(
									'RELATES_STATUS' 	=> '99',
									'RELATES_MARKS'		=> $_SERVER['PHP_SELF']
								);
								$where 	= "RELATES_ID = '".$relatesInfo[0]['RELATES_ID']."' ";

								update_data($tbName, $data, $where);
								// if ($SAVE_LOG) {
								// 	$save = array(date('Y-m-d H:i:s'), 'UPDATE VEHICLE_RELATIONSHIP.RELATES_STATUS = 99', 'WHERE '.$where);
								// }

								// INSERT
								$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
								$data 	= array(
									'RELATES_VEHICLE_ID' 	=> $ahis_arr[$i]['idRef_vihicle'],
									'RELATES_CUS_ID'		=> $ahis_arr[$i]['user'],
									'RELATES_TYPE'			=> 'ผู้ใช้',
									'RELATES_COMP_ID'		=> $ahis_arr[$i]['asell_com_id'],
									'RELATES_MARKS'			=> $_SERVER['PHP_SELF'],
									'Date_Deliver'			=> $ahis_arr[$i]['deliver_day']
								);
								$res_insert_id = insert_data($tbName, $data);
								// if ($SAVE_LOG) {
								// 	array_push($save, 'INSERT VEHICLE_RELATIONSHIP', 'INSERT id='.$res_insert_id, 'LINE: '.__LINE__);
								// 	fputcsv($savelogs, $save);
								// }
							}

						}else {
							// MULTIROW -> DULP -> UPDATE99 -> INSERT
							// if ($SAVE_LOG) {
							// 	$save = array(date('Y-m-d H:i:s'));
							// }
							for ($x=0; $x<$num_relatesInfo; $x++) {
								// UPDATE99
								$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
								$data 	= array(
									'RELATES_STATUS' 	=> '99',
									'RELATES_MARKS'		=> $_SERVER['PHP_SELF']
								);
								$where 	= "RELATES_ID = '".$relatesInfo[$x]['RELATES_ID']."' ";

								update_data($tbName, $data, $where);
								// if ($SAVE_LOG) {
								// 	array_push($save, 'UPDATE VEHICLE_RELATIONSHIP.RELATES_STATUS = 99', 'WHERE '.$where);
								// }
							}
							// INSERT
							$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
							$data 	= array(
								'RELATES_VEHICLE_ID' 	=> $ahis_arr[$i]['idRef_vihicle'],
								'RELATES_CUS_ID'		=> $ahis_arr[$i]['user'],
								'RELATES_TYPE'			=> 'ผู้ใช้',
								'RELATES_COMP_ID'		=> $ahis_arr[$i]['asell_com_id'],
								'RELATES_MARKS'			=> $_SERVER['PHP_SELF'],
								'Date_Deliver'			=> $ahis_arr[$i]['deliver_day']
							);
							$res_insert_id = insert_data($tbName, $data);
							// if ($SAVE_LOG) {
							// 	array_push($save, 'INSERT VEHICLE_RELATIONSHIP', 'INSERT id='.$res_insert_id, 'LINE: '.__LINE__);
							// 	fputcsv($savelogs, $save);
							// }

						}

					}else {
						// INSERT
						$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
						$data 	= array(
							'RELATES_VEHICLE_ID' 	=> $ahis_arr[$i]['idRef_vihicle'],
							'RELATES_CUS_ID'		=> $ahis_arr[$i]['user'],
							'RELATES_TYPE'			=> 'ผู้ใช้',
							'RELATES_COMP_ID'		=> $ahis_arr[$i]['asell_com_id'],
							'RELATES_MARKS'			=> $_SERVER['PHP_SELF'],
							'Date_Deliver'			=> $ahis_arr[$i]['deliver_day']
						);
						$res_insert_id = insert_data($tbName, $data);
						// if ($SAVE_LOG) {
						// 	$save = array(date('Y-m-d H:i:s'), 'INSERT VEHICLE_RELATIONSHIP', 'INSERT id='.$res_insert_id, 'LINE: '.__LINE__);
						// 	fputcsv($savelogs, $save);
						// }
					}

				}
			}

			if ($UP_DRIVER) {
				/**
				คนขับ
				**/
				if ($ahis_arr[$i]['driver'] > 0) {
					
					$relatesInfo 		= getRelatesByVehicleId($ahis_arr[$i]['idRef_vihicle'], 'คนขับ');
					$num_relatesInfo 	= count($relatesInfo);

					// echo "<pre>";
					// print_r($relatesInfo);
					// echo "</pre>";

					if ($num_relatesInfo > 0) {

						if ($num_relatesInfo == 1) {

							// CHECK Cus_Id
							if ($relatesInfo[0]['RELATES_CUS_ID'] == $ahis_arr[$i]['driver']) {
								// Cus_No Is true
								// CHECK Date_Deliver
								if ($relatesInfo[0]['Date_Deliver'] == '0000-00-00') {
									// UPDATE Date_Deliver
									$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
									$data 	= array(
										'Date_Deliver' 	=> $ahis_arr[$i]['deliver_day'],
										'RELATES_MARKS'	=> $_SERVER['PHP_SELF']
									);
									$where 	= "RELATES_ID = '".$relatesInfo[0]['RELATES_ID']."' ";

									update_data($tbName, $data, $where);
									// if ($SAVE_LOG) {
									// 	$save = array(date('Y-m-d H:i:s'), 'UPDATE VEHICLE_RELATIONSHIP.Date_Deliver = '.$ahis_arr[$i]['deliver_day'], 'WHERE '.$where, 'LINE: '.__LINE__);
									// 	fputcsv($savelogs, $save);
									// }

								}

							}else {
								// Cus_No Is false
								// UPDATE99
								$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
								$data 	= array(
									'RELATES_STATUS' 	=> '99',
									'RELATES_MARKS'		=> $_SERVER['PHP_SELF']
								);
								$where 	= "RELATES_ID = '".$relatesInfo[0]['RELATES_ID']."' ";

								update_data($tbName, $data, $where);
								// if ($SAVE_LOG) {
								// 	$save = array(date('Y-m-d H:i:s'), 'UPDATE VEHICLE_RELATIONSHIP.RELATES_STATUS = 99', 'WHERE '.$where);
								// }

								// INSERT
								$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
								$data 	= array(
									'RELATES_VEHICLE_ID' 	=> $ahis_arr[$i]['idRef_vihicle'],
									'RELATES_CUS_ID'		=> $ahis_arr[$i]['driver'],
									'RELATES_TYPE'			=> 'คนขับ',
									'RELATES_COMP_ID'		=> $ahis_arr[$i]['asell_com_id'],
									'RELATES_MARKS'			=> $_SERVER['PHP_SELF'],
									'Date_Deliver'			=> $ahis_arr[$i]['deliver_day']
								);
								$res_insert_id = insert_data($tbName, $data);
								// if ($SAVE_LOG) {
								// 	array_push($save, 'INSERT VEHICLE_RELATIONSHIP', 'INSERT id='.$res_insert_id, 'LINE: '.__LINE__);
								// 	fputcsv($savelogs, $save);
								// }
							}

						}else {
							// MULTIROW -> DULP -> UPDATE99 -> INSERT
							// if ($SAVE_LOG) {
							// 	$save = array(date('Y-m-d H:i:s'));
							// }
							for ($x=0; $x<$num_relatesInfo; $x++) {
								// UPDATE99
								$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
								$data 	= array(
									'RELATES_STATUS' 	=> '99',
									'RELATES_MARKS'		=> $_SERVER['PHP_SELF']
								);
								$where 	= "RELATES_ID = '".$relatesInfo[$x]['RELATES_ID']."' ";

								update_data($tbName, $data, $where);
								// if ($SAVE_LOG) {
								// 	array_push($save, 'UPDATE VEHICLE_RELATIONSHIP.RELATES_STATUS = 99', 'WHERE '.$where);
								// }
							}
							// INSERT
							$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
							$data 	= array(
								'RELATES_VEHICLE_ID' 	=> $ahis_arr[$i]['idRef_vihicle'],
								'RELATES_CUS_ID'		=> $ahis_arr[$i]['driver'],
								'RELATES_TYPE'			=> 'คนขับ',
								'RELATES_COMP_ID'		=> $ahis_arr[$i]['asell_com_id'],
								'RELATES_MARKS'			=> $_SERVER['PHP_SELF'],
								'Date_Deliver'			=> $ahis_arr[$i]['deliver_day']
							);
							$res_insert_id = insert_data($tbName, $data);
							// if ($SAVE_LOG) {
							// 	array_push($save, 'INSERT VEHICLE_RELATIONSHIP', 'INSERT id='.$res_insert_id, 'LINE: '.__LINE__);
							// 	fputcsv($savelogs, $save);
							// }

						}

					}else {
						// INSERT
						$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
						$data 	= array(
							'RELATES_VEHICLE_ID' 	=> $ahis_arr[$i]['idRef_vihicle'],
							'RELATES_CUS_ID'		=> $ahis_arr[$i]['driver'],
							'RELATES_TYPE'			=> 'คนขับ',
							'RELATES_COMP_ID'		=> $ahis_arr[$i]['asell_com_id'],
							'RELATES_MARKS'			=> $_SERVER['PHP_SELF'],
							'Date_Deliver'			=> $ahis_arr[$i]['deliver_day']
						);
						$res_insert_id = insert_data($tbName, $data);
						// if ($SAVE_LOG) {
						// 	$save = array(date('Y-m-d H:i:s'), 'INSERT VEHICLE_RELATIONSHIP', 'INSERT id='.$res_insert_id, 'LINE: '.__LINE__);
						// 	fputcsv($savelogs, $save);
						// }
					}

				}
			}

			if ($UP_CUS_REPAIR) {
				/**
				ผู้นำรถมาซ่อม
				**/
				if ($ahis_arr[$i]['cus_repair'] > 0) {
					
					$relatesInfo 		= getRelatesByVehicleId($ahis_arr[$i]['idRef_vihicle'], 'ผู้นำรถมาซ่อม');
					$num_relatesInfo 	= count($relatesInfo);

					// echo "<pre>";
					// print_r($relatesInfo);
					// echo "</pre>";

					if ($num_relatesInfo > 0) {

						if ($num_relatesInfo == 1) {

							// CHECK Cus_Id
							if ($relatesInfo[0]['RELATES_CUS_ID'] == $ahis_arr[$i]['cus_repair']) {
								// Cus_No Is true
								// CHECK Date_Deliver
								if ($relatesInfo[0]['Date_Deliver'] == '0000-00-00') {
									// UPDATE Date_Deliver
									$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
									$data 	= array(
										'Date_Deliver' 	=> $ahis_arr[$i]['deliver_day'],
										'RELATES_MARKS'	=> $_SERVER['PHP_SELF']
									);
									$where 	= "RELATES_ID = '".$relatesInfo[0]['RELATES_ID']."' ";

									update_data($tbName, $data, $where);
									// if ($SAVE_LOG) {
									// 	$save = array(date('Y-m-d H:i:s'), 'UPDATE VEHICLE_RELATIONSHIP.Date_Deliver = '.$ahis_arr[$i]['deliver_day'], 'WHERE '.$where, 'LINE: '.__LINE__);
									// 	fputcsv($savelogs, $save);
									// }

								}

							}else {
								// Cus_No Is false
								// UPDATE99
								$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
								$data 	= array(
									'RELATES_STATUS' 	=> '99',
									'RELATES_MARKS'		=> $_SERVER['PHP_SELF']
								);
								$where 	= "RELATES_ID = '".$relatesInfo[0]['RELATES_ID']."' ";

								update_data($tbName, $data, $where);
								// if ($SAVE_LOG) {
								// 	$save = array(date('Y-m-d H:i:s'), 'UPDATE VEHICLE_RELATIONSHIP.RELATES_STATUS = 99', 'WHERE '.$where);
								// }

								// INSERT
								$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
								$data 	= array(
									'RELATES_VEHICLE_ID' 	=> $ahis_arr[$i]['idRef_vihicle'],
									'RELATES_CUS_ID'		=> $ahis_arr[$i]['cus_repair'],
									'RELATES_TYPE'			=> 'ผู้นำรถมาซ่อม',
									'RELATES_COMP_ID'		=> $ahis_arr[$i]['asell_com_id'],
									'RELATES_MARKS'			=> $_SERVER['PHP_SELF'],
									'Date_Deliver'			=> $ahis_arr[$i]['deliver_day']
								);
								$res_insert_id = insert_data($tbName, $data);
								// if ($SAVE_LOG) {
								// 	array_push($save, 'INSERT VEHICLE_RELATIONSHIP', 'INSERT id='.$res_insert_id, 'LINE: '.__LINE__);
								// 	fputcsv($savelogs, $save);
								// }
							}

						}else {
							// MULTIROW -> DULP -> UPDATE99 -> INSERT
							// if ($SAVE_LOG) {
							// 	$save = array(date('Y-m-d H:i:s'));
							// }
							for ($x=0; $x<$num_relatesInfo; $x++) {
								// UPDATE99
								$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
								$data 	= array(
									'RELATES_STATUS' 	=> '99',
									'RELATES_MARKS'		=> $_SERVER['PHP_SELF']
								);
								$where 	= "RELATES_ID = '".$relatesInfo[$x]['RELATES_ID']."' ";

								update_data($tbName, $data, $where);
								// if ($SAVE_LOG) {
								// 	array_push($save, 'UPDATE VEHICLE_RELATIONSHIP.RELATES_STATUS = 99', 'WHERE '.$where);
								// }
							}
							// INSERT
							$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
							$data 	= array(
								'RELATES_VEHICLE_ID' 	=> $ahis_arr[$i]['idRef_vihicle'],
								'RELATES_CUS_ID'		=> $ahis_arr[$i]['cus_repair'],
								'RELATES_TYPE'			=> 'ผู้นำรถมาซ่อม',
								'RELATES_COMP_ID'		=> $ahis_arr[$i]['asell_com_id'],
								'RELATES_MARKS'			=> $_SERVER['PHP_SELF'],
								'Date_Deliver'			=> $ahis_arr[$i]['deliver_day']
							);
							$res_insert_id = insert_data($tbName, $data);
							// if ($SAVE_LOG) {
							// 	array_push($save, 'INSERT VEHICLE_RELATIONSHIP', 'INSERT id='.$res_insert_id, 'LINE: '.__LINE__);
							// 	fputcsv($savelogs, $save);
							// }

						}

					}else {
						// INSERT
						$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
						$data 	= array(
							'RELATES_VEHICLE_ID' 	=> $ahis_arr[$i]['idRef_vihicle'],
							'RELATES_CUS_ID'		=> $ahis_arr[$i]['cus_repair'],
							'RELATES_TYPE'			=> 'ผู้นำรถมาซ่อม',
							'RELATES_COMP_ID'		=> $ahis_arr[$i]['asell_com_id'],
							'RELATES_MARKS'			=> $_SERVER['PHP_SELF'],
							'Date_Deliver'			=> $ahis_arr[$i]['deliver_day']
						);
						$res_insert_id = insert_data($tbName, $data);
						// if ($SAVE_LOG) {
						// 	$save = array(date('Y-m-d H:i:s'), 'INSERT VEHICLE_RELATIONSHIP', 'INSERT id='.$res_insert_id, 'LINE: '.__LINE__);
						// 	fputcsv($savelogs, $save);
						// }
					}

				}
			}

			if ($UP_CUS_RECEIVE_CAR) {
				/**
				ผู้มารับรถ
				**/
				if ($ahis_arr[$i]['cus_receive_car'] > 0) {
					
					$relatesInfo 		= getRelatesByVehicleId($ahis_arr[$i]['idRef_vihicle'], 'ผู้มารับรถ');
					$num_relatesInfo 	= count($relatesInfo);

					// echo "<pre>";
					// print_r($relatesInfo);
					// echo "</pre>";

					if ($num_relatesInfo > 0) {

						if ($num_relatesInfo == 1) {

							// CHECK Cus_Id
							if ($relatesInfo[0]['RELATES_CUS_ID'] == $ahis_arr[$i]['cus_receive_car']) {
								// Cus_No Is true
								// CHECK Date_Deliver
								if ($relatesInfo[0]['Date_Deliver'] == '0000-00-00') {
									// UPDATE Date_Deliver
									$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
									$data 	= array(
										'Date_Deliver' 	=> $ahis_arr[$i]['deliver_day'],
										'RELATES_MARKS'	=> $_SERVER['PHP_SELF']
									);
									$where 	= "RELATES_ID = '".$relatesInfo[0]['RELATES_ID']."' ";

									update_data($tbName, $data, $where);
									// if ($SAVE_LOG) {
									// 	$save = array(date('Y-m-d H:i:s'), 'UPDATE VEHICLE_RELATIONSHIP.Date_Deliver = '.$ahis_arr[$i]['deliver_day'], 'WHERE '.$where, 'LINE: '.__LINE__);
									// 	fputcsv($savelogs, $save);
									// }

								}

							}else {
								// Cus_No Is false
								// UPDATE99
								$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
								$data 	= array(
									'RELATES_STATUS' 	=> '99',
									'RELATES_MARKS'		=> $_SERVER['PHP_SELF']
								);
								$where 	= "RELATES_ID = '".$relatesInfo[0]['RELATES_ID']."' ";

								update_data($tbName, $data, $where);
								// if ($SAVE_LOG) {
								// 	$save = array(date('Y-m-d H:i:s'), 'UPDATE VEHICLE_RELATIONSHIP.RELATES_STATUS = 99', 'WHERE '.$where);
								// }

								// INSERT
								$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
								$data 	= array(
									'RELATES_VEHICLE_ID' 	=> $ahis_arr[$i]['idRef_vihicle'],
									'RELATES_CUS_ID'		=> $ahis_arr[$i]['cus_receive_car'],
									'RELATES_TYPE'			=> 'ผู้มารับรถ',
									'RELATES_COMP_ID'		=> $ahis_arr[$i]['asell_com_id'],
									'RELATES_MARKS'			=> $_SERVER['PHP_SELF'],
									'Date_Deliver'			=> $ahis_arr[$i]['deliver_day']
								);
								$res_insert_id = insert_data($tbName, $data);
								// if ($SAVE_LOG) {
								// 	array_push($save, 'INSERT VEHICLE_RELATIONSHIP', 'INSERT id='.$res_insert_id, 'LINE: '.__LINE__);
								// 	fputcsv($savelogs, $save);
								// }
							}

						}else {
							// MULTIROW -> DULP -> UPDATE99 -> INSERT
							// if ($SAVE_LOG) {
							// 	$save = array(date('Y-m-d H:i:s'));
							// }
							for ($x=0; $x<$num_relatesInfo; $x++) {
								// UPDATE99
								$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
								$data 	= array(
									'RELATES_STATUS' 	=> '99',
									'RELATES_MARKS'		=> $_SERVER['PHP_SELF']
								);
								$where 	= "RELATES_ID = '".$relatesInfo[$x]['RELATES_ID']."' ";

								update_data($tbName, $data, $where);
								// if ($SAVE_LOG) {
								// 	array_push($save, 'UPDATE VEHICLE_RELATIONSHIP.RELATES_STATUS = 99', 'WHERE '.$where);
								// }
							}
							// INSERT
							$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
							$data 	= array(
								'RELATES_VEHICLE_ID' 	=> $ahis_arr[$i]['idRef_vihicle'],
								'RELATES_CUS_ID'		=> $ahis_arr[$i]['cus_receive_car'],
								'RELATES_TYPE'			=> 'ผู้มารับรถ',
								'RELATES_COMP_ID'		=> $ahis_arr[$i]['asell_com_id'],
								'RELATES_MARKS'			=> $_SERVER['PHP_SELF'],
								'Date_Deliver'			=> $ahis_arr[$i]['deliver_day']
							);
							$res_insert_id = insert_data($tbName, $data);
							// if ($SAVE_LOG) {
							// 	array_push($save, 'INSERT VEHICLE_RELATIONSHIP', 'INSERT id='.$res_insert_id, 'LINE: '.__LINE__);
							// 	fputcsv($savelogs, $save);
							// }

						}

					}else {
						// INSERT
						$tbName = $config['db_vehicle'].".VEHICLE_RELATIONSHIP";
						$data 	= array(
							'RELATES_VEHICLE_ID' 	=> $ahis_arr[$i]['idRef_vihicle'],
							'RELATES_CUS_ID'		=> $ahis_arr[$i]['cus_receive_car'],
							'RELATES_TYPE'			=> 'ผู้มารับรถ',
							'RELATES_COMP_ID'		=> $ahis_arr[$i]['asell_com_id'],
							'RELATES_MARKS'			=> $_SERVER['PHP_SELF'],
							'Date_Deliver'			=> $ahis_arr[$i]['deliver_day']
						);
						$res_insert_id = insert_data($tbName, $data);
						// if ($SAVE_LOG) {
						// 	$save = array(date('Y-m-d H:i:s'), 'INSERT VEHICLE_RELATIONSHIP', 'INSERT id='.$res_insert_id, 'LINE: '.__LINE__);
						// 	fputcsv($savelogs, $save);
						// }
					}

				}
			}

		}

	}
}

if ($SAVE_LOG) {
	$save = array(date('Y-m-d H:i:s'), 'END');
	fputcsv($savelogs, $save);
	fclose($savelogs);
}

$db->close();

?>