<?php
require_once("config/general.php");
require_once("config/ic_followcus.php");
require_once("function/general.php");
require_once("function/ic_followcus.php");
require_once("function/ic_followcus_form.php");
include_once("function/helper_create_log.php");

/****************************
* css = css/followcus.css   *
* 	    css/nz_button.css	 *
* js  = java/followcus.js   *
****************************/
Conn2DB();

/**
 * @author Zan @2010-12-XX
 * ใช้สำหรับ สร้าง Event และ คำถาม สำหรับโทรหาลูกค้า
 */

if(!isset($_GET['func'])){//แสดงเมนูหลัก แค่ครั้งแรกที่เข้ามา
	echo $tpl->tbHtml('ic_followcus_form.html','EVENT_HEAD');
}

$func = $_GET['func'];
if($func == "view_event_list" || $func==""){    //list event
	if($_GET['view_refer']){//ส่งมาจากหน้า ดูเหตุการณ์ส่งต่อ/รอสัมภาษณ์
		$condition	 = "AND id IN (". stripcslashes($_GET['view_refer']) .")";
		$refer_title = '<p class="title2" align="center" style="color:seagreen"><u>'.$_GET['refer_title'].'</u></p><br>';
	}
	$sql = "SELECT * FROM $config[db_base_name].follow_main_event  WHERE status != '99' $condition
			ORDER by IF(send_to_tis!='yes',1,2),CONVERT(event_title USING TIS620) ";
	$result = mysql_query($sql);
	$cls[0] = "event_tr1";
	$cls[1] = "event_tr2";
	$a = 0;
	while($row = mysql_fetch_assoc($result)) {
		if($a>1){$a=0;}
		$bg	= $cls[$a];
		$date_follow = "";
		$check_pattern = $row[pattern_id_ref];

		$sqlPT  = "SELECT pattern_name,status FROM $config[db_base_name].follow_field_pattern WHERE id = '$row[pattern_id_ref]'";
		$resultPT = mysql_query($sqlPT) or die("เกิดข้อผิดพลาด :: ". mysql_error());
		$pattern = mysql_fetch_assoc($resultPT);

		if($pattern['status']!="99"){
			$row['pattern_id_ref'] 	= $pattern['pattern_name'];
		}else {
			$row['pattern_id_ref'] 	= "<font color='red'>(ไม่มีวันที่!!)</font>";
		}

		$waitColor = "";
		$func = "form_edit_event";
		if($row['follow_type']=='1'){
			$row['follow_type'] = "คน";
		}else if($row['follow_type']=='2'){
			$row['follow_type'] = "รถ";
		}
	//	if($row['pattern_id_ref']==""){
		if($check_pattern==""){
			$row['follow_type'] 	= "สัมภาษณ์";
			$row['pattern_id_ref']	= "-";
			$date_follow 			= "วันที่ทำรายการ";
			$waitColor 				= "blue";
			$func 					= "form_edit_wait_event";
		}

		$row['remind_unit'] = strtolower($row['remind_unit']);
		$row['cal_unit'] = strtolower($row['cal_unit']);
		$row['start_unit'] = strtolower($row['start_unit']);
		$row['stop_unit'] = strtolower($row['stop_unit']);
		$unit = array("day"=>"วัน","week"=>"สัปดาห์","month"=>"เดือน","year"=>"ปี");
		switch($row['remind_type']) {
			case "once":	$row['remind_type']	="ครั้งเดียว";	break;
			case "alway":	$row['remind_type']	="ทุกๆ <font color='#009900'>".$row['remind_value']." ".$unit[$row['remind_unit']]."</font>";break;
		}

		switch(strtolower($row['cal_format'])) {
			case "ondate":
				$row['cal_format']	= "ตามวันที่ $row[pattern_id_ref] ".$date_follow;
				break;
			case "before":
				$row['cal_format']	=	"ก่อนหน้า $row[pattern_id_ref] <font color='#009900'>".$row['cal_value']." ".$unit[$row['cal_unit']]."</font>";
				break;
			case "after":
				$row['cal_format']	=	"หลังจาก $row[pattern_id_ref] $date_follow <font color='#009900'>".$row['cal_value']." ".$unit[$row['cal_unit']]."</font>";
				break;
		}
		if(trim($row['start_value'])){$start_date = $row['start_value']." ".$unit[$row['start_unit']];}else {$start_date="-";}
		if(trim($row['stop_value'])){$stop_date = $row['stop_value']." ".$unit[$row['stop_unit']];}else {$stop_date="-";}

		### ชั่วคราว ### $row['department'] = get_department_from_cut($row['department']);// function/ic_followcus_form.php
		if($row['department']==""){
			### ชั่วคราว ### $row['department'] = select_emp_nametonickname($row['who_response']);// function/general.php
		}
		switch(strtolower($row['send_to_tis'])) {//ส่งให้ตรีเพชร
			case "yes":		$color = "green";$row['send_to_tis']="ส่ง";	break;
			default: 		$color = "red";$row['send_to_tis']="ไม่ส่ง";	break;
		}
		switch(strtolower($row['visible'])) {
			case "no":		$row['visible'] = "<font color='red'>ไม่แสดง</font>";	break;
			default:			$row['visible'] = "<font color='green'>แสดง</font>";	break;
		}
		switch(strtolower($row['target_count'])) {
			case "no":		$row['target_count'] = "<font color='red'>ไม่นับ</font>";	break;
			default:			$row['target_count'] = "<font color='green'>นับ</font>";	break;
		}

		$row['send_to_tis'] = "<span style='color:$color'>$row[send_to_tis]</font>";

		$event_list .= $tpl->tbHtml('ic_followcus_form.html','EVENT_LIST');
		$a++;
	}
	mysql_free_result($result);
	echo $tpl->tbHtml('ic_followcus_form.html','EVENT_TABLE');

}else if($func=="preSubmitFromEvent"){
	
	$objQuery = mysql_query("SELECT field_vehicle_name FROM $config[db_base_name].follow_field_pattern WHERE 
	id = '".$_GET['follow_field_pattern']."' ") or die(" error : ".__LINE__.":".__FILE__.mysql_error());
	$row = mysql_fetch_assoc($objQuery);
	if(empty($row['field_vehicle_name'])){
		echo "วันที่ของกิจกรรมของไม่มี field ข้อมูลของรถ";
	}
}else if($func=="follow_organization_boxs"){
	
	echo $tpl->tbHtml('ic_followcus_form.html','FOLLOW_ORGANIZATION_BOXS');
}else if($func=="follow_organization_selected"){
	// echo "<textarea>";
	// print_r($_GET);
	// echo "</textarea>";	

	$follower_name = $tr_ids = ''; 
	switch($_GET['suggest']){
		case 'suggest_company':  
			$arr = explode(' - ',$_GET['value']);
			$arr['comp'] = $arr[1];
			$tr_ids 	 = 'comp'.$_GET['id_value'];
			$ids_value   = $_GET['id_value'];
			$follower_name	= "follower_up_comp[]";
		break;
		case 'suggest_department':  
			//value	บริษัท อีซูซุเชียงราย จำกัด MK การตลาด
			$arr = explode(' - ',$_GET['value']);
			$arr['comp'] = $arr[2];
			$arr['depa'] = $arr[1];
			$tr_ids 	 = 'depa'.$_GET['id_value']; 
			$ids_value   = $_GET['company_id'].'#'.$_GET['id_value'];
			$follower_name	= "follower_up_depa[]";
		break;
		case 'suggest_section':  
			//value	บริษัท อีซูซุเชียงราย จำกัด MK การตลาด  
			$arr = explode(' - ',$_GET['value']);
			$arr['comp'] = $arr[2];
			$arr['depa'] = $arr[1];
			$arr['sect'] = $arr[0];
			$tr_ids 	 = 'sect'.$_GET['id_value']; 
			$ids_value   = $_GET['company_id'].'#'.$_GET['departs_id'].'#'.$_GET['id_value'];
			$follower_name	= "follower_up_sect[]";
		break;
		case 'suggest_position':  
			//value	บริษัท อีซูซุเชียงราย จำกัด MK การตลาด  
			$arr = explode(' - ',$_GET['value']);
			$arr['comp'] = $arr[3];
			$arr['depa'] = $arr[2];
			$arr['sect'] = $arr[1];
			$arr['posi'] = $arr[0];
			$tr_ids 	 = 'posi'.$_GET['id_value']; 
			$ids_value   = $_GET['company_id'].'#'.$_GET['departs_id'].'#'.$_GET['section_id'].'#'.$_GET['id_value'];
			$follower_name	= "follower_up_posi[]";
		break;
	}
	
	echo $ids_value."*@#@*";
	echo $tpl->tbHtml('ic_followcus_form.html','the-follwer-list');
}else if($func=="pre_event_form"){
	/* $loop = count($config['BU_LIST']);
	for($i=0;$i<$loop;$i++){
		//$inp_value = trim(str_replace(' ','',$config['BU_LIST'][$i]));
		$inp_value = trim($config['BU_LIST'][$i]);
		//$add_attr = "rem_attr('#module_all','checked');add_attr('#$inp_value','checked')";
		$add_attr = "rem_attr('#module_all','checked');";
		$tr_list_module .= '<tr>
			<td align="right" class="text-right"><label for="'.$inp_value.'">'.$config['BU_LIST'][$i].'</label></td>
			<td align="left">
				&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="chk_module[]" id="'.$inp_value.'" value="'.$inp_value.'" 
				onclick="rem_attr(\'#module_all\',\'checked\')"/>
			</td>
		</tr>';
		// <input type="checkbox" name="chk_module[]" id="'.$inp_value.'" value="'.$inp_value.'" 
		// onclick="if(this.checked == true){'.$add_attr.'}"/>
	} */
	$obj_query = mysql_query("SELECT * FROM $config[db_organi].biz_name WHERE status <> '99' ") or die(" error : ".__LINE__.":".__FILE__.mysql_error());
	while($rs = mysql_fetch_assoc($obj_query)){
		$inp_value = $rs['biz_id'];
		$add_attr = "rem_attr('#module_all','checked');";
		if(!$tr_list_module){ $checked = 'checked="checked"'; }else{ $checked = ''; }
		$tr_list_module .= '<tr>
			<td align="right" class="text-right"><label for="'.$inp_value.'">'.$rs['biz_name'].'</label></td>
			<td align="left">
				&nbsp;&nbsp;&nbsp;
				<input type="radio" name="chk_module[]" id="'.$inp_value.'" value="'.$inp_value.'" '.$checked.'/>
			</td>
		</tr>';
		
		// <input type="radio" name="chk_module[]" id="'.$inp_value.'" value="'.$inp_value.'" 
		// onclick="rem_attr(\'#module_all\',\'checked\')"/>
	}
	
	
	//$event_id = 
	
	$onClickAction = '';
	if($_GET['action']=='prev_event'){
		$onClickAction = "javascript:if(nzCheckForm('frm_pattern')==true){
			formPrevTitleModule('$event_id');
			set_jnone('div_popup');
		}";
	}else{
		$onClickAction = "javascript:if(nzCheckForm('frm_pattern')==true){form_title_module();set_jnone('div_popup');}";
	}
	
	echo $tpl->tbHtml('ic_followcus_form.html','PRE_EVENT_FORM');
	
	
}else if($func=="event_form"){//แบบฟอร์มสำหรับเพิ่มเหตุการณ์
	
	$_GET['chk_module'] = array_filter($_GET['chk_module']);
	
	// if(!empty($_GET['chk_module'])){
		// $inum = 0;
		// foreach($_GET['chk_module'] as $biz_name){
			// $business_unit_name .= $biz_name.'  ,  ';
			// $business_inps_name .= $biz_name.'@#@';
			// $inum++;
		// }
	// }
	if(!empty($_GET['chk_module'])){
		$inum = 1;
		foreach($_GET['chk_module'] as $biz_id){
			if($inum != 1){
				$business_inps_name .= '@#@';
				$business_unit_name .= '  ,  ';
			}
			
			$biz_name = query("SELECT biz_name FROM $config[db_organi].biz_name WHERE biz_id = '$biz_id' ");
		
			$business_inps_name .= $biz_id;
			$business_unit_name .= $biz_name; 
			$inum = 2;
		}
	}
	
	
	
	//print_r($_GET['chk_module']);
	
	$filed_pattern = getFieldPatternDate();
	$cancel_filed_dates = getCancelPatternDate();    // เงื่อนไขการยกเลิกกิจกรรม
	
	// $arr['field_value'] = 'id'; 
	// $option_company	= get_company_option($arr);
	
	$rs['random_display'] = "100";
	$send2 = 'checked="checked"';
	$form_title = "แบบฟอร์ม <font color='red'>เพิ่ม</font> หัวข้อกิจกรรมใหม่ &nbsp;&nbsp;";
	$active1 = 'checked="checked"';
	
	$make_hidden = "display:none";
	
	echo $tpl->tbHtml('ic_followcus_form.html','MAIN_EVENT_FORM');
}else if($func=="event_permiss"){//แบบฟอร์มสำหรับเพิ่มเหตุการณ์
	/* Array(
		[option_company] => 12
		[option_department] => 58
		[option_group] => 115
		[option_position] => 
		[inp_option_person] => 
		[func] => event_permiss
		[case_name] => tr_option_group
	)
	*/
	switch($_GET['case_name']){
		case 'tr_option_department':
			$arr['field_value'] = 'id';
			$arr['where'] = "and company = '".$_GET['option_company']."' ";
			echo get_department_option($arr);
		break;
		case 'tr_option_group':
			$arr['field_value'] = 'id';
			$arr['where'] = "and department = '".$_GET['option_department']."' ";
			echo get_group_option($arr);
		break;
		case 'tr_option_position':
			$arr['where'] = "and WorkCompany = '".$_GET['option_company']."' and Department = '".$_GET['option_department']."' and Section = '".$_GET['option_group']."' ";
			$arr['group'] = 'group by Name';
			echo get_position_option($arr);
		break;
		case 'tr_option_person':
			$position_id = select_to_sql_in("SELECT id FROM $config[db_organi].position WHERE Status != '99' 
			AND Name <> '' AND WorkCompany = '".$_GET['option_company']."' AND Department = '".$_GET['option_department']."' 
			AND Section = '".$_GET['option_group']."' AND Name = '".$_GET['option_position']."' ORDER BY id");  
			
			$arr['field_value'] = 'id_card';
			$arr['where'] = "and position_id IN (".$position_id.") ";
			echo get_person_option($arr);
		break;
	}

}elseif($func=="form_edit_event") {
	$event_id = $_GET['event_id'];
	$form_title = "แบบฟอร์ม <font color='orangered'>แก้ไข</font> กิจกรรม";
	$sql = "SELECT * FROM $config[db_base_name].follow_main_event WHERE id = '$event_id' ";
	$result = mysql_query($sql);
	$rs = mysql_fetch_assoc($result);

	$filed_pattern	= getFieldPatternDate($rs['pattern_id_ref']);
	
	//$department		= getDepartment($rs['department']);

	

	
	#--- BUSINESS UNIT NAME
	$objQuery = mysql_query("SELECT biz_id_ref FROM $config[db_base_name].follow_event_biz WHERE event_id_ref = '$event_id' ");
	$inum = 1;
	while($rsm = mysql_fetch_assoc($objQuery)){
		if($inum != 1){
			$business_inps_name .= '@#@';
			$business_unit_name .= '  ,  ';
		}
		
		$biz = query("SELECT biz_name, biz_id FROM $config[db_organi].biz_name WHERE biz_id = '$rsm[biz_id_ref]' ",1);
	
		$business_inps_name .= $biz['biz_id'];
		$business_unit_name .= $biz['biz_name']; 
		$inum = 2;
	}
	
	
	
	$arrPattern = query("SELECT db_name, table_name FROM $config[db_base_name].follow_field_pattern WHERE 
	id = '$rs[pattern_id_ref]' ",1);  
	$field_expire_date = getFieldName($config[$arrPattern[db_name]].'.'.$arrPattern[table_name],$rs[field_expire_date]); 
	
	
	switch($rs['follow_type']) {
		case "1":	$check1 = "checked='checked'";	break;
		case "2":	$check2 = "checked='checked'";	break;
	}
	switch($rs['remind_unit']) {
		case "day":		$re_sel1 = "selected='selected'";	break;
		case "week":	$re_sel2 = "selected='selected'";	break;
		case "month":	$re_sel3 = "selected='selected'";	break;
		case "year":	$re_sel4 = "selected='selected'";	break;
	}
	switch(strtolower($rs['cal_unit'])) {
		case "day":		$cal_day = "selected='selected'";	break;
		case "week":	$cal_week = "selected='selected'";	break;
		case "month":	$cal_month = "selected='selected'";	break;
		case "year":	$cal_year = "selected='selected'";	break;
	}
	switch(strtolower($rs['start_unit'])) {
		case "day":		$start_day = "selected='selected'";	break;
		case "week":	$start_week = "selected='selected'";	break;
		case "month":	$start_month = "selected='selected'";	break;
		case "year":	$start_year = "selected='selected'";	break;
	}
	switch(strtolower($rs['stop_unit'])) {
		case "day":		$stop_day = "selected='selected'";	break;
		case "week":	$stop_week = "selected='selected'";	break;
		case "month":	$stop_month = "selected='selected'";	break;
		case "year":	$stop_year = "selected='selected'";	break;
	}
	switch(strtolower($rs['send_to_tis'])) {
		case "yes":		$send1 = "checked='checked'";	break;
		default:			$send2 = "checked='checked'";	break;
	}
	switch(strtolower($rs['visible'])) {
		case "no":		$visible2 = "checked='checked'";	break;
		default:			$visible1 = "checked='checked'";	break;
	}
	switch(strtolower($rs['target_count'])) {
		case "no":		$target_count2 = "checked='checked'";	break;
		default:			$target_count1 = "checked='checked'";	break;
	}
	switch(strtolower($rs['status'])) {
		case "10":		$active2 = "checked='checked'";	break;
		default:		$active1 = "checked='checked'";	break;
	}
	
	switch($rs['follow_up_activities']) {
		case "2":		
			$upActivities2 = "checked='checked'";	
			
			# -- select follow_event_responsible "กำหนดความสัมพันธ์บุคคลที่ติดตามกิจกรรม"
			$eventResponsible = mysql_query("SELECT * FROM $config[db_base_name].follow_event_responsible 
			WHERE main_event_id = '$event_id' ") or die("error : ".__LINE__.":".__FILE__.mysql_error()) ; 
			while($eveRes = mysql_fetch_assoc($eventResponsible)){
				$arrChk = array(
					"comp" 		=> $eveRes['resps_comp'],
					"depa" 		=> $eveRes['resps_department'],
					"sect" 		=> $eveRes['resps_section'],
					"posi" 		=> $eveRes['resps_position']
				);
				
				$arrChkeys 	= array_keys(array_filter($arrChk));
				$tr_ids		= $arrChkeys[0].$arrChk[$arrChkeys[0]]; 
				$arr[comp] = $arr[depa] = $arr[sect] = $arr[posi] = '';
				switch($arrChkeys[0]){
					case 'comp': 
						$arr[comp] = query("SELECT name FROM $config[db_organi].working_company WHERE 
						id = '".$arrChk[$arrChkeys[0]]."' AND status <> '99' ");
						$follower_name	= "follower_up_comp[]"; 
					break;
					case 'depa': 
						$depa = query("SELECT working_company.name AS work_name, department.name AS depart_name FROM 
						$config[db_organi].working_company Inner Join $config[db_organi].department ON 
						working_company.id = department.company WHERE department.id = '".$arrChk[$arrChkeys[0]]."' 
						AND working_company.status <> '99' AND department.status <> '99' ",1);
						
						$arr[comp] = $depa['work_name'];
						$arr[depa] = $depa['depart_name'];
						
						$follower_name	= "follower_up_depa[]"; 
					break;
					case 'sect':
						$sect = query("SELECT section.name AS section_name, department.name AS department_name,
						department.company FROM $config[db_organi].section Inner Join $config[db_organi].department ON department.id = section.department WHERE section.status <> '99' AND department.status <> '99' 
						AND section.id = '".$arrChk[$arrChkeys[0]]."' ",1);
						
						$arr[comp] = query("SELECT name FROM $config[db_organi].working_company WHERE 
						id = '".$sect['company']."' AND status <> '99' ");
						$arr[depa] = $sect['department_name'];
						$arr[sect] = $sect['section_name'];
						
						$follower_name	= "follower_up_sect[]"; 
					break;
					case 'posi': 
						$posi = query("SELECT Name, WorkCompany, Department, Section FROM 
						$config[db_organi].position WHERE Status != '99' ",1);  

						$arr[comp] = query("SELECT name FROM $config[db_organi].working_company WHERE 
						id = '".$posi['WorkCompany']."' AND status <> '99' ");
						$arr[depa] = query("SELECT name FROM $config[db_organi].department WHERE 
						id = '".$posi['Department']."' AND status <> '99' ");
						$arr[sect] = query("SELECT name FROM $config[db_organi].section WHERE 
						id = '".$posi['Section']."' AND status <> '99' ");
						$arr[posi] = $posi['Name'];
						
						$follower_name	= "follower_up_posi[]";  
					break;
				}
				$ids_value = $arrChk[$arrChkeys[0]];
				
				$the_follwer_list .= $tpl->tbHtml('ic_followcus_form.html','the-follwer-list');
			}
			
			$follow_organization_boxs = $tpl->tbHtml('ic_followcus_form.html','FOLLOW_ORGANIZATION_BOXS');
		break;
		default:		
			$upActivities1 = "checked='checked'";	
		break;
	}

	
	$make_hidden = "display:none";
	
	$obj_query = mysql_query("SELECT * FROM $config[db_base_name].follow_event_responsible WHERE main_event_id = '".$event_id."' ") 
	or die("error : ".__LINE__.":".__FILE__.mysql_error()) ; 
	$row = mysql_fetch_assoc($obj_query);
	
	if($row['resps_comp']){  # -- c บริษัทที่ติดตามกิจกรรม
		$make_hidden = "";
		$comp['field_value'] = 'id'; 
		$comp['selected'] = $row['resps_comp'];
		$option_company	= get_company_option($comp);
	}else{
		$make_hidden = "display:none";
	}
	
	if($row['resps_department']){	# -- c แผนกที่ติดตามกิจกรรม
		$make_hidden = "";
		$depar['field_value'] = 'id';
		$depar['where'] = "and company = '".$row['resps_comp']."' ";
		$depar['selected'] = $row['resps_department'];
		$option_department = get_department_option($depar);
	}else{
		$make_hidden = "display:none";
	}
	
	if($row['resps_section']){	# -- c แผนกที่ติดตามกิจกรรม
		$make_hidden = "";
		$sect['field_value'] = 'id';
		$sect['where'] = "and department = '".$row['resps_department']."' ";
		$sect['selected'] = $row['resps_section'];
		$option_group = get_group_option($sect);
	}else{
		$make_hidden = "display:none";
	}
	
	if($row['resps_position']){	# -- c แผนกที่ติดตามกิจกรรม
		$make_hidden = "";
		$pos['where'] = "and WorkCompany = '".$row['resps_comp']."' and Department = '".$row['resps_department']."' 
		and Section = '".$row['resps_section']."' ";
		$pos['group'] = 'group by Name';
		$sect['selected'] = $row['resps_position'];
		$option_position = get_position_option($pos);
	}else{
		$make_hidden = "display:none";
	}
	
	if($row['event_responsible']){	# -- c แผนกที่ติดตามกิจกรรม
		$make_hidden = "";
		$position_id = select_to_sql_in("SELECT id FROM $config[db_organi].position WHERE Status != '99' 
		AND Name <> '' AND WorkCompany = '".$row['resps_comp']."' AND Department = '".$row['resps_department']."' 
		AND Section = '".$row['resps_section']."' AND Name = '".$row['resps_position']."' ORDER BY id");  
		
		$_resp['field_value'] = 'id_card';
		$_resp['where'] = "and position_id IN (".$position_id.") ";
		$sect['selected'] = $row['event_responsible'];
		$option_person = get_person_option($_resp);
	}else{
		$make_hidden = "display:none";
	}
	
	
	$form = $tpl->tbHtml('ic_followcus_form.html','MAIN_EVENT_FORM');

	//ต้องใช้การคลิก เพื่อแสดงผล
	if(strtolower($rs['remind_type'])=="once"){
		$remind_check = "re_type1";
	}else {
		$remind_check = "re_type2";
	}

	switch(strtolower($rs['cal_format'])) {
		case "ondate":	$cal_check = "cal1"; break;
		case "before":	$cal_check = "cal2"; break;
		case "after":	$cal_check = "cal3"; break;
	}
	
	$js = "<script tyle='text/javascript'> 
			var follow_up_activities = $rs[follow_up_activities]; 
			if(follow_up_activities==2){
				rem_attr('#organization_boxs','style');
			}
			
			document.getElementById('$remind_check').click();
			document.getElementById('$cal_check').click();
			document.getElementById('input_list_title').focus();
		</script>";
	echo $form.$js;

}else if($func=="add_event") {     //บันทึกเหตุการณ์ลงฐานข้อมูล (ใช้ร่วมกันกับ เหตุการณ์ส่งต่อรอสัมภาษณ์)
	//if($_POST['event_title']!="" && ($_POST['department']!="" || $_POST['who_response']!='') ) {//check

	/*if(!empty($_POST['follower_up_depa'])){
		foreach ($_POST['follower_up_depa'] as $arr_value){
			$depa = explode('#',$arr_value);
			echo "INSERT INTO $config[db_base_name].follow_event_responsible (main_event_id, resps_bu, resps_comp, resps_department, resps_section, resps_position, event_responsible) 
			VALUES ('".$new_event_id."', '".$resps_bu."', '','".trim($depa[1])."', '', '', '')";
		}
	}

	 echo "<textarea>";
	 print_r($_POST);
	 echo "</textarea>";
	 exit();*/
	
	if($_POST['event_title']!="") {//check
		$id						= $_POST['event_id'];
		$pattern_id_ref			= $_POST['pattern_id_ref'];
		$event_title			= $_POST['event_title'];
		$follow_type			= $_POST['follow_type'];
		$remind_type			= $_POST['remind_type'];
		$remind_value			= $_POST['remind_value'];
		$remind_unit			= $_POST['remind_unit'];
		$cal_format				= $_POST['cal_format'];
		$cal_value				= $_POST['cal_value'];
		$cal_unit				= $_POST['cal_unit'];
		$start_value			= $_POST['start_value'];
		$start_unit				= $_POST['start_unit'];
		$stop_value				= $_POST['stop_value'];
		$stop_unit				= $_POST['stop_unit'];
		$department				= $_POST['department'];
		$who_response			= $_POST['who_response'];//จะส่งมาเฉพาะตอนสร้าง เหตุการสัมภาษณ์
		$random_display			= $_POST['random_display'];
		$TIS_Event_Group		= $_POST['TIS_Event_Group'];
		$TIS_Event_Period		= $_POST['TIS_Event_Period'];
		$TIS_Event_PeriodCode	= $_POST['TIS_Event_PeriodCode'];
		$send_to_tis			= $_POST['send_to_tis'];
		$visible 				= $_POST["visible"];
		$target_count 			= $_POST["target_count"];

		switch ($_POST['active']){
			case 'yes' : $status = ''; break;
			default : $status = '10';  break;//กิจกรรมที่ยังไม่สร้าง
		}

		if($start_value==""){$start_value=0;}
		if($stop_value==""){$stop_value=0;}
		if(empty($follow_type)){$follow_type=0;}
		
		if($id){//if edit event
			//-- BACKUP Z@2012-02-14
			include_once ("function/helper_create_log.php");//ZAN @ 2012-02-14
			$backup = array(
				'table' => " $config[db_base_name].follow_main_event ",
				'where' => " id = '$id' ",
	 			'line' => __LINE__,
	 			'remark' => 'แก้ไขข้อมูลกิจกรรม',
	 			'action_type' => '1',
	 		);
	 		if(function_exists('insert_backup_from_sql')){
				insert_backup_from_sql($backup, "$config[db_base_name].ZLogHistory");
	 		}
	 		//-- BACKUP 
		}

		# -- c ผู้ที่ติดตามกิจกรรม 1.ผู้รับผิดชอบลูกค้า,2ระบุตามโครงสร้างบริษัท
		if($_POST['follow_up_activities']=='responsibility'){$follow_activities = '1';}else{$follow_activities = '2';}
		
		# -- ถ้ามีการแก้ไข ให้เก็บ LOG
		if(!empty($_POST['event_edit_id'])){   
			$mainOuery = mysql_query("SELECT * FROM $config[db_base_name].follow_main_event WHERE id = '$id' ") or die(" error : ".__LINE__.":".__FILE__.mysql_error());
			$rows = mysql_fetch_assoc($mainOuery);
			mysql_query("INSERT INTO $config[db_base_name].follow_main_event_cancel (id, id_ref, pattern_id_ref, event_title, follow_type, remind_type, remind_value, remind_unit, cal_format, cal_value, cal_unit, start_value, start_unit, stop_value, stop_unit, department, who_response, follow_up_activities, random_display, random_date, TIS_Event_Group, TIS_Event_Period, TIS_Event_PeriodCode, send_to_tis, visible, target_count, status, field_expire_date, chk_cancle_follwer, remark) VALUES ('',
			'".$rows['id']."', '".$rows['pattern_id_ref']."', '".$rows['event_title']."', '".$rows['follow_type']."',
			'".$rows['remind_type']."', '".$rows['remind_value']."', '".$rows['remind_unit']."', '".$rows['cal_format']."',
			'".$rows['cal_value']."', '".$rows['cal_unit']."', '".$rows['start_value']."', '".$rows['start_unit']."',
			'".$rows['stop_value']."', '".$rows['stop_unit']."', '".$rows['department']."', '".$rows['who_response']."', 
			'".$rows['follow_up_activities']."', '".$rows['random_display']."', '".$rows['random_date']."',
			'".$rows['TIS_Event_Group']."', '".$rows['TIS_Event_Period']."', '".$rows['TIS_Event_PeriodCode']."',
			'".$rows['send_to_tis']."', '".$rows['visible']."', '".$rows['target_count']."', '".$rows['status']."',
			'".$rows['field_expire_date']."', '".$rows['chk_cancle_follwer']."', 'EDIT')") or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
		
			$followQuery = mysql_query("SELECT * FROM $config[db_base_name].follow_customer WHERE event_id_ref = '$id' ") or die(" error : ".__LINE__.":".__FILE__.mysql_error());
			while($follow = mysql_fetch_assoc($followQuery)){
				mysql_query("INSERT INTO $config[db_base_name].follow_customer_cancel (id, event_id_ref, follow_type, 
				chassi_no_s, cus_no, cus_name, biz_id_ref, follow_resps_comp, follow_resps_department, follow_resps_section, follow_resps_position, emp_id_card, emp_posCode, start_date, date_value, stop_date, process_by, process_by_posCode, process_date, cancel_type, remark, status, TIS_Event_Group, TIS_Event_Period, TIS_Event_PeriodCode, send_to_tis, FC_status_tis, FC_Date_upstatus, follow_db_name, source_db_table, source_primary_field, source_primary_id, cancel_remark) VALUES ('".$follow['id']."', '".$id."',
				'".$follow['follow_type']."', '".$follow['chassi_no_s']."', '".$follow['cus_no']."', '".$follow['cus_name']."',
				'".$follow['biz_id_ref']."', '".$follow['follow_resps_comp']."', '".$follow['follow_resps_department']."',
				'".$follow['follow_resps_section']."', '".$follow['follow_resps_position']."', '".$follow['emp_id_card']."',
				'".$follow['emp_posCode']."', '".$follow['start_date']."', '".$follow['date_value']."',
				'".$follow['stop_date']."', '".$follow['process_by']."', '".$follow['process_by_posCode']."',
				'".$follow['process_date']."', '".$follow['cancel_type']."', '".$follow['remark']."', '".$follow['status']."',
				'".$follow['TIS_Event_Group']."', '".$follow['TIS_Event_Period']."', '".$follow['TIS_Event_PeriodCode']."',
				'".$follow['send_to_tis']."', '".$follow['FC_status_tis']."', '".$follow['FC_Date_upstatus']."',
				'".$follow['follow_db_name']."', '".$follow['source_db_table']."', '".$follow['source_primary_field']."',
				'".$follow['source_primary_id']."', 'EDIT_MAIN_EVENT') ") or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
			}
			
			mysql_query("DELETE FROM $config[db_base_name].follow_customer WHERE follow_customer.event_id_ref = '$id' ") 
			or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
			
			mysql_query("DELETE FROM $config[db_base_name].follow_main_event WHERE follow_main_event.id = '$id' ") 
			or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
		
		}
		
		$sql = "INSERT INTO $config[db_base_name].follow_main_event ";
		$sql .= "( id,pattern_id_ref, event_title, follow_type, remind_type, remind_value, remind_unit, cal_format,
				cal_value, cal_unit, start_value, start_unit, stop_value, stop_unit, department,  						who_response, random_display, TIS_Event_Group, TIS_Event_Period, TIS_Event_PeriodCode, send_to_tis,
				visible, target_count, status, follow_up_activities, field_expire_date";
		$sql .= ")VALUES(";
		$sql .= " '','$pattern_id_ref', '$event_title', '$follow_type', '$remind_type',  '$remind_value',  '$remind_unit',
				'$cal_format','$cal_value',  '$cal_unit', '$start_value',  '$start_unit', '$stop_value', 
				'$stop_unit','$department','$who_response','$random_display','$TIS_Event_Group','$TIS_Event_Period',
				'$TIS_Event_PeriodCode', '$send_to_tis','$visible','$target_count','$status', '$follow_activities',
				'".$_POST['field_expire_date']."')";
				
				
		//	echo $sql ;exit();
		mysql_query('SET NAMES UTF8');
		mysql_query($sql) or die("เกิดข้อผิดพลาด :: ". mysql_error());
		$new_event_id = mysql_insert_id();
		
		if($id==""){    //ตอนสร้างเหตุการ์ใหม่
			
			if($_POST['prev_event_id']){ //เพิ่มเหตุการณ์ส่งต่อ รอสัมภาษณ์ (จะมี id ของเหตุการณ์ที่ทำก่อนมาด้วย)
				$this_event_id = $new_event_id; //เหตุการณ์ที่เพิ่งสร้างเป็นเหตุการณ์ทำครั้งต่อไป
				$prev_event_id = $_POST['prev_event_id']; //เหตุการณ์ที่สร้างก่อนเป็น evnt_id_ref

				$sql2 = "INSERT INTO $config[db_base_name].follow_event_refer ";
				$sql2.= "(event_id_ref,	next_event_id)VALUES('$prev_event_id', '$this_event_id')";
				$result2 = mysql_query($sql2) or die("เกิดข้อผิดพลาด :: ". mysql_error());

				if($result2){
					if($_POST['follow_up_activities']=='organization'){ 
						# -- loop company
						if(!empty($_POST['follower_up_comp'])){
							foreach ($_POST['follower_up_comp'] as $arr_value){
								mysql_query("INSERT INTO $config[db_base_name].follow_event_responsible (main_event_id, resps_bu, resps_comp, resps_department, resps_section, resps_position, event_responsible) 
								VALUES ('".$new_event_id."', '".$resps_bu."', '".trim($arr_value)."', '', '', '', '')") 
								or die(" error : ".__LINE__.":".__FILE__.mysql_error());
							}
						}
								
						# -- loop department
						if(!empty($_POST['follower_up_depa'])){
							foreach ($_POST['follower_up_depa'] as $arr_value){
								$depa = explode('#',$arr_value);
								mysql_query("INSERT INTO $config[db_base_name].follow_event_responsible (main_event_id, resps_bu, resps_comp, resps_department, resps_section, resps_position, event_responsible) 
								VALUES ('".$new_event_id."', '".$resps_bu."', '','".trim($depa[1])."', '', '', '')") 
								or die(" error : ".__LINE__.":".__FILE__.mysql_error());
							}
						}
								
						# -- loop section
						if(!empty($_POST['follower_up_sect'])){
							foreach ($_POST['follower_up_sect'] as $arr_value){
								$sect = explode('#',$arr_value);
								mysql_query("INSERT INTO $config[db_base_name].follow_event_responsible (main_event_id, resps_bu, resps_comp, resps_department, resps_section, resps_position, event_responsible) 
								VALUES ('".$new_event_id."', '".$resps_bu."', '','', '".trim($sect[2])."', '', '')") or die(" error : ".__LINE__.":".__FILE__.mysql_error());
							}
						}
								
						# -- loop position
						if(!empty($_POST['follower_up_posi'])){
							foreach ($_POST['follower_up_posi'] as $arr_value){
								$posi = explode('#',$arr_value);
								mysql_query("INSERT INTO $config[db_base_name].follow_event_responsible (main_event_id, resps_bu, resps_comp, resps_department, resps_section, resps_position, event_responsible) 
								VALUES ('".$new_event_id."', '".$resps_bu."', '','', '', '".trim($posi[3])."', '')") or die(" error : ".__LINE__.":".__FILE__.mysql_error());
							}
						}
							
					}  // end if($_POST['follow_up_activities']=='organization'){
					
					
					$arr_bu = explode('@#@',$_POST['business_unit_name']);
					foreach($arr_bu as $values){
						if(!empty($values)){
							mysql_query("INSERT INTO $config[db_base_name].follow_event_biz (event_id_ref, biz_id_ref, status) VALUES ('".$new_event_id."', '".$values."', '')") or die(" error : ".__LINE__.":".__FILE__.mysql_error());
						} // end if(!empty($values)){
						
					}  //end foreach($arr_bu as $values){
				

				
					$msg = "<p align='center' class='title2 green'>บันทึกข้อมูลเรียบร้อย กำลังโหลดข้อมูลใหม่... <img src='images/loader_32x32.gif' align='absmiddle'></p>";
					$js = "<script type='text/javascript'>setTimeout(\"javascript:followGetData('display_event','ic_followcus_form.php?func=');changeClass(this.id,'BTblack','BTwhite');\",1500);</script>";
					echo $msg.$js;
				}
			}else { //เพิ่มเหตุการ create_follow.php
				
				if(!empty($new_event_id)){		//ให้แสดงแบบฟอร์มเพิ่มหัวข้อ "คำถามหลัก"

					if($_POST['follow_up_activities']=='organization'){ 
					
						//$arr_bu = explode('@#@',$_POST['business_unit_name']);
						//foreach($arr_bu as $values){
							# --------------------------------------------------------------
							# -- c ตาราง : follow_event_responsible "กำหนดความสัมพันธ์บุคคลที่ติดตามกิจกรรม"
							# --------------------------------------------------------------
							//if(!empty($values)){
								
							
								# -- loop company
								if(!empty($_POST['follower_up_comp'])){
									foreach ($_POST['follower_up_comp'] as $arr_value){
										mysql_query("INSERT INTO $config[db_base_name].follow_event_responsible (main_event_id, resps_bu, resps_comp, resps_department, resps_section, resps_position, event_responsible) 
										VALUES ('".$new_event_id."', '".$resps_bu."', '".trim($arr_value)."', '', '', '', '')") 
										or die(" error : ".__LINE__.":".__FILE__.mysql_error());
									}
								}
								
								# -- loop department
								if(!empty($_POST['follower_up_depa'])){
									foreach ($_POST['follower_up_depa'] as $arr_value){
										$depa = explode('#',$arr_value);
										mysql_query("INSERT INTO $config[db_base_name].follow_event_responsible (main_event_id, resps_bu, resps_comp, resps_department, resps_section, resps_position, event_responsible) 
										VALUES ('".$new_event_id."', '".$resps_bu."', '','".trim($depa[1])."', '', '', '')") 
										or die(" error : ".__LINE__.":".__FILE__.mysql_error());
									}
								}
								
								# -- loop section
								if(!empty($_POST['follower_up_sect'])){
									foreach ($_POST['follower_up_sect'] as $arr_value){
										$sect = explode('#',$arr_value);
										mysql_query("INSERT INTO $config[db_base_name].follow_event_responsible (main_event_id, resps_bu, resps_comp, resps_department, resps_section, resps_position, event_responsible) 
										VALUES ('".$new_event_id."', '".$resps_bu."', '','', '".trim($sect[2])."', '', '')") or die(" error : ".__LINE__.":".__FILE__.mysql_error());
									}
								}
								
								# -- loop position
								if(!empty($_POST['follower_up_posi'])){
									foreach ($_POST['follower_up_posi'] as $arr_value){
										$posi = explode('#',$arr_value);
										mysql_query("INSERT INTO $config[db_base_name].follow_event_responsible (main_event_id, resps_bu, resps_comp, resps_department, resps_section, resps_position, event_responsible) 
										VALUES ('".$new_event_id."', '".$resps_bu."', '','', '', '".trim($posi[3])."', '')") or die(" error : ".__LINE__.":".__FILE__.mysql_error());
									}
								}
							
							//} // end if(!empty($values)){
							
						//}  //end foreach($arr_bu as $values){
						
					}  // end if($_POST['follow_up_activities']=='organization'){
					
					
					$arr_bu = explode('@#@',$_POST['business_unit_name']);
					foreach($arr_bu as $values){
						if(!empty($values)){
							mysql_query("INSERT INTO $config[db_base_name].follow_event_biz (event_id_ref, biz_id_ref, status) VALUES ('".$new_event_id."', '".$values."', '')") or die(" error : ".__LINE__.":".__FILE__.mysql_error());
						} // end if(!empty($values)){
					}  //end foreach($arr_bu as $values){
					
					
					
	
					$event_id = $new_event_id;
					echo $tpl->tbHtml('ic_followcus_form.html','MAIN_QUESTION_FORM');
				}
			}
		}else {  //ถ้าเป็นการแก้ไข
			if(!empty($id)){		//ให้แสดงแบบฟอร์มเพิ่มหัวข้อ "คำถามหลัก"

				
				
				if($_POST['follow_up_activities']=='organization'){ 
					# -- backup date to cancel log
					$evenQuery = mysql_query("SELECT * FROM $config[db_base_name].follow_event_responsible WHERE 
					main_event_id = '$id' ") or die(" error : ".__LINE__.":".__FILE__.mysql_error());
					while($eve = mysql_fetch_assoc($evenQuery)){
						mysql_query("INSERT INTO $config[db_base_name].follow_event_responsible_cancel (id_ref , main_event_id, resps_bu, resps_comp, resps_department, resps_section, resps_position, event_responsible) 
						VALUES ('$eve[event_resp_id]', '$eve[main_event_id]', '$eve[resps_bu]', '$eve[resps_comp]',
						'$eve[resps_department]', '$eve[resps_section]', '$eve[resps_position]', '$eve[event_responsible]')") 
						or die(" error : ".__LINE__.":".__FILE__.mysql_error());
					}
					 
					mysql_query("DELETE FROM $config[db_base_name].follow_event_responsible WHERE main_event_id  = '$id'") or die(" error : ".__LINE__.":".__FILE__.mysql_error());
					# -- end backup date to cancel log
					
					//$arr_bu = explode('@#@',$_POST['business_unit_name']);
					//foreach($arr_bu as $values){
						# --------------------------------------------------------------
						# -- c ตาราง : follow_event_responsible "กำหนดความสัมพันธ์บุคคลที่ติดตามกิจกรรม"
						# --------------------------------------------------------------
						//if(!empty($values)){
							
							# -- loop company
							if(!empty($_POST['follower_up_comp'])){
								foreach ($_POST['follower_up_comp'] as $arr_value){
									mysql_query("INSERT INTO $config[db_base_name].follow_event_responsible (main_event_id, resps_bu, resps_comp, resps_department, resps_section, resps_position, event_responsible) 
									VALUES ('".$new_event_id."', '".$resps_bu."', '".trim($arr_value)."', '', '', '', '')") 
									or die(" error : ".__LINE__.":".__FILE__.mysql_error());
								}
							}
							
							# -- loop department
							if(!empty($_POST['follower_up_depa'])){
								foreach ($_POST['follower_up_depa'] as $arr_value){
									$depa = explode('#',$arr_value);
									mysql_query("INSERT INTO $config[db_base_name].follow_event_responsible (main_event_id, resps_bu, resps_comp, resps_department, resps_section, resps_position, event_responsible) 
									VALUES ('".$new_event_id."', '".$resps_bu."', '','".trim($depa[1])."', '', '', '')") 
									or die(" error : ".__LINE__.":".__FILE__.mysql_error());
								}
							}
							
							# -- loop section
							if(!empty($_POST['follower_up_sect'])){
								foreach ($_POST['follower_up_sect'] as $arr_value){
									$sect = explode('#',$arr_value);
									mysql_query("INSERT INTO $config[db_base_name].follow_event_responsible (main_event_id, resps_bu, resps_comp, resps_department, resps_section, resps_position, event_responsible) 
									VALUES ('".$new_event_id."', '".$resps_bu."', '','', '".trim($sect[2])."', '', '')") or die(" error : ".__LINE__.":".__FILE__.mysql_error());
								}
							}
							
							# -- loop position
							if(!empty($_POST['follower_up_posi'])){
								foreach ($_POST['follower_up_posi'] as $arr_value){
									$posi = explode('#',$arr_value);
									mysql_query("INSERT INTO $config[db_base_name].follow_event_responsible (main_event_id, resps_bu, resps_comp, resps_department, resps_section, resps_position, event_responsible) 
									VALUES ('".$new_event_id."', '".$resps_bu."', '','', '', '".trim($posi[3])."', '')") or die(" error : ".__LINE__.":".__FILE__.mysql_error());
								}
							}
						
						//} // end if(!empty($values)){
						
					//}  //end foreach($arr_bu as $values){
					
				}  // end if($_POST['follow_up_activities']=='organization'){
				
				
				$arr_bu = explode('@#@',$_POST['business_unit_name']);
				foreach($arr_bu as $values){
					if(!empty($values)){
						mysql_query("INSERT INTO $config[db_base_name].follow_event_biz (event_id_ref, biz_id_ref, status) VALUES ('".$new_event_id."', '".$values."', '')") or die(" error : ".__LINE__.":".__FILE__.mysql_error());
					} // end if(!empty($values)){
				}  //end foreach($arr_bu as $values){
			
			} // end if(!empty($id)){
			
			$msg = "<p align='center' class='title2 green'>แก้ไขข้อมูลเรียบร้อย กำลังโหลดข้อมูลใหม่... <img src='images/loader_32x32.gif' align='absmiddle'></p>";
			$js = "<script type='text/javascript'>setTimeout(\"javascript:followGetData('display_event','ic_followcus_form.php?func=');changeClass(this.id,'BTblack','BTwhite');\",1500);</script>";
			echo $msg.$js;
		
		} //  end }else {//ถ้าเป็นการแก้ไข
		
	}//end check

}elseif($func=="confirm_del_event") {//แสดงรายการก่อนลบ
	if($_POST['event_id']){
		$event_id_del = join("','",$_REQUEST['event_id']);
	}else {
		$event_id_del = $_GET['event_id'];
	}
	$sql_fol  = "SELECT id,event_title FROM $config[db_base_name].follow_main_event WHERE id IN ('$event_id_del') ";
	$result_fol = mysql_query($sql_fol) or die("เกิดข้อผิดพลาด :: ". mysql_error());
	$cls[0] = "all_cus_tr1";
	$cls[1] = "all_cus_tr2";
	while($rs = mysql_fetch_assoc($result_fol)) {
		$num_follow = query("SELECT count(id) FROM $config[db_base_name].follow_customer WHERE event_id_ref='$rs[id]' AND status IN ('0','1','3','4','90') ");
		$a++;
		if($a>1){$a=0;}
		$bg	= $cls[$a];
		$tr_list .= "<tr class='$bg' style='color:green'><td width=''>$rs[id]</td><td width='' align=''>$rs[event_title]</td><td width='' align='right' style='padding-right:10px'>". number_format($num_follow)."</td></tr>";
	}
	mysql_free_result($result_fol);
	$event_id = str_replace("'","",$event_id_del);//ส่งไปเป็นค่า post
	$table = '<div style="margin:0px 20px;">
				<div class="title3" style="color:blue;line-height:22px">คุณต้องการลบชื่อกิจกรรมที่เลือกไว้ทั้งหมด หรือไม่?<br></div>
				<table class="nz_table" width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px solid #999;border-right:1px solid #999">
				<tr style="background:orange"><th width="" align="">รหัส</th><th width="" align="">ชื่อชื่อกิจกรรม</th><th width="" align="">จำนวนรายการที่ติดตามอยู่</th>	</tr>';
	$table .= 	$tr_list;
	$table .= '</table>';
	$table .= '<form onsubmit=\'javascript:if(verify.value=="OK" || verify.value=="ok"){followPostData("nz_popup_div","ic_followcus_form.php?func=del_event","event_id='.$event_id.'");}else{alert("หากต้องการลบ พิมพ์คำว่า OK เพื่อยืนยันการลบครับ");} return false;\'>
				<div style="color:orangered"><br>หากชื่อกิจกรรมที่ลบมีการติดตาม รายการที่ติดตามอยู่นั้นก็จะถูกยกเลิกไปด้วย!!<br></div>
				<div style="color:red"><br><input type="text" size="3" id="verify" name="verify"> * พิมพ์คำว่า OK เพื่อยืนยันการลบ <br><br></div>
				<button type="button" class="nz_button BTwhite bt_del_all" onclick=\'javascript:if(verify.value=="OK" || verify.value=="ok"){followPostData("nz_popup_div","ic_followcus_form.php?func=del_event","event_id='.$event_id.'");}else{alert("หากต้องการลบ พิมพ์คำว่า OK เพื่อยืนยันการลบครับ");}\'><img src="images/trash.png" align="absbottom" height="20" width="25"> ยืนยันการลบ</button>
				<button type="button" class="nz_button BTwhite bt_blue_all" style="width:100px;" onclick="hideObj(\'nz_popup_div\');unCheckAllBox(\'event_check\');"><img src="images/closed.png" align="absbottom" title="ยกเลิก"> ยกเลิก</button></form>
				</div>';
	echo $table;

}else if($func=="del_event"){
	$event_id 	= "'". str_replace(",","','",$_POST['event_id'])."'";//output = '1','2','3',...,'n'
	$sql = "UPDATE $config[db_base_name].follow_main_event SET status='99' WHERE id IN($event_id) ";
	$result = mysql_query($sql) or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
	
	
	# -- last update 30-04-2012 by.k
	# -- insert to follow_main_event_cancel LOG
	$obj_query = mysql_query("SELECT * FROM $config[db_base_name].follow_main_event WHERE id IN($event_id) ") or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
	while($rs = mysql_fetch_assoc($obj_query)){
		mysql_query("INSERT INTO $config[db_base_name].follow_main_event_cancel (id, id_ref, pattern_id_ref, event_title, follow_type, remind_type, remind_value, remind_unit, cal_format, cal_value, cal_unit, start_value, start_unit, stop_value, stop_unit, department, who_response, follow_up_activities, random_display, random_date, TIS_Event_Group, TIS_Event_Period, TIS_Event_PeriodCode, send_to_tis, visible, target_count, status, field_expire_date, chk_cancle_follwer, remark) 
		VALUES ('', '".$rs['id']."', '".$rs['pattern_id_ref']."', '".$rs['event_title']."', '".$rs['follow_type']."',
		'".$rs['remind_type']."', '".$rs['remind_value']."', '".$rs['remind_unit']."', '".$rs['cal_format']."',
		'".$rs['cal_value']."', '".$rs['cal_unit']."', '".$rs['start_value']."', '".$rs['start_unit']."',
		'".$rs['stop_value']."', '".$rs['stop_unit']."', '".$rs['department']."', '".$rs['who_response']."', 
		'".$rs['follow_up_activities']."', '".$rs['random_display']."', '".$rs['random_date']."', '".$rs['TIS_Event_Group']."',
		'".$rs['TIS_Event_Period']."', '".$rs['TIS_Event_PeriodCode']."', '".$rs['send_to_tis']."', '".$rs['visible']."',
		'".$rs['target_count']."', '".$rs['status']."', '".$rs['field_expire_date']."', '".$rs['chk_cancle_follwer']."',
		'DEL_EVENT')") or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
	
	
		$objFollowCus = mysql_query("SELECT * FROM $config[db_base_name].follow_customer WHERE event_id_ref IN($event_id) ")  or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
		while($foll = mysql_fetch_assoc($objFollowCus)){
			mysql_query("INSERT INTO $config[db_base_name].follow_customer_cancel (idCancel, id, event_id_ref, follow_type, chassi_no_s, cus_no, cus_name, biz_id_ref, follow_resps_comp, follow_resps_department, follow_resps_section, follow_resps_position, emp_id_card, emp_posCode, start_date, date_value, stop_date, process_by, process_by_posCode, process_date, cancel_type, remark, status, TIS_Event_Group, TIS_Event_Period, TIS_Event_PeriodCode, send_to_tis, FC_status_tis, FC_Date_upstatus, follow_db_name, source_db_table, source_primary_field, source_primary_id, cancel_remark, status_confirm) VALUES ('', '".$foll['id']."', '".$rs['id']."', '".$foll['follow_type']."',
			'".$foll['chassi_no_s']."', '".$foll['cus_no']."', '".$foll['cus_name']."', '".$foll['biz_id_ref']."',
			'".$foll['follow_resps_comp']."', '".$foll['follow_resps_department']."', '".$foll['follow_resps_section']."',
			'".$foll['follow_resps_position']."', '".$foll['emp_id_card']."', '".$foll['emp_posCode']."',
			'".$foll['start_date']."', '".$foll['date_value']."', '".$foll['stop_date']."', '".$foll['process_by']."',
			'".$foll['process_by_posCode']."', '".$foll['process_date']."', '".$foll['cancel_type']."',
			'".$foll['remark']."', '".$foll['status']."', '".$foll['TIS_Event_Group']."', '".$foll['TIS_Event_Period']."',
			'".$foll['TIS_Event_PeriodCode']."', '".$foll['send_to_tis']."', '".$foll['FC_status_tis']."',
			'".$foll['FC_Date_upstatus']."', '".$foll['follow_db_name']."', '".$foll['source_db_table']."',
			'".$foll['source_primary_field']."', '".$foll['source_primary_id']."', '".$foll['cancel_remark']."',
			'".$foll['status_confirm']."') ") or die("<p class='error'>เกิดข้อผิดพลาด insert follow_customer_cancel</p>". mysql_error());
	
		}
	
	
	
	
		mysql_query("DELETE FROM $config[db_base_name].follow_main_event WHERE follow_main_event.id = '".$rs['id']."' ") 
		or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());

	
	
	
	}
	

	# -- End insert to follow_main_event_cancel LOG
	
	
	
	if($result){
		//ยกเลิกการส่งต่อมายังเหตุการณ์นี้
		$up_refer  = "UPDATE $config[db_base_name].follow_event_refer SET status='99' ";
		$up_refer .= "WHERE next_event_id IN($event_id) OR event_id_ref IN($event_id)"; //ยกเลิกเหตุการณ์ส่งต่อ ที่มีไอดีที่ถูกยกเลิก
		mysql_query($up_refer) or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());

		
		# -- last update 30-04-2012 by.k
		# -- insert to follow_main_event_cancel LOG
		$obj_query = mysql_query("SELECT * FROM $config[db_base_name].follow_event_refer WHERE next_event_id IN($event_id) OR 
		event_id_ref IN($event_id)") or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
		while($rs = mysql_fetch_assoc($obj_query)){
			mysql_query("INSERT INTO $config[db_base_name].follow_event_refer_cancel (id, id_ref, event_id_ref, next_event_id, status) VALUES ('', '".$rs['id']."', '".$rs['event_id_ref']."', '".$rs['next_event_id']."', '".$rs['status']."')") 
			or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
		
			mysql_query("DELETE FROM $config[db_base_name].follow_event_refer WHERE follow_event_refer.id = '".$rs['id']."' ")
			or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
		
		}
		
		
		# -- End insert to follow_event_refer LOG
		
		
		//ยกเลิกรายการติดตาม
		//	$cancel_follow = "UPDATE $config[db_base_name].follow_customer SET status='99',process_by='Delete Event',process_date=NOW() ";
		//	$cancel_follow .= "WHERE event_id_ref IN($event_id) AND status < 98 ";
		//	mysql_query($cancel_follow) or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());

		$table = "$config[db_base_name].follow_customer";
		$where = "event_id_ref IN($event_id) AND status IN ('0','1','3','4','90')";//เอาเฉพาะที่ยังไม่หมดอายุ และไม่ได้ทำรายการ
		//-- แบ็กอัพ ก่อนลบ
		$array = array(
					'table'=>$table,//ตารางที่จะแบ็กอัพ
					'where'=>$where,//เงื่อนไขที่ใช้ (จะตรงกับที่เลือกลบด้านล่าง ***)
					'remark'=>"ลบกิจกรรมเฟส F จาก follow_main_event",
					'action_type'=>'2',//DELETE
					'line'=>__LINE__,
				);
		insert_backup_from_sql($array);//BACKUP  (function/helper_create_log.php)
		// ลบ การติดตามที่สร้างไว้
		$cancel_follow = "DELETE FROM $table WHERE $where";//*** เงื่อนไขที่ใช้ลบ
		mysql_query($cancel_follow) or show_error(__FILE__,__LINE__,$cancel_follow);
		//--end

		//ซ่อนแถวที่ลบแล้ว
		$event_id = str_replace("'","",$event_id);
		$tr_hide = "event_tr". str_replace(",",",event_tr",$event_id);//output = 'tr_pattern01,tr_pattern111,...,tr_patternNNN'
		$success = "<br><div style='padding: 15px;'><p class='title2' style='color:green;height:100px;line-height:100px'>ลบรายการที่เลือกเรียบร้อยแล้วครับ.</p></div>";
		$success .= "<script type='text/javascript'>
						hideObj('$tr_hide');//ซ่อนแถวที่ถูกลบแล้ว
						setTimeout(\"hideObj('nz_popup_div')\",1500);
					</script>";
	}else {
		$success = "<p class='error'>ไม่สามารถลบข้อมูลได้ครับ</p>"; 
	}
	echo $success;

}else if($func=="add_main_question") {//บันทึกคำถามหลัก
	if($_POST['event_id']){//check
		$event_id_ref			=	$_POST['event_id'];
		$question_title			=	$_POST['question_title'];
		$question_detail		=	$_POST['question_detail'];
		$question_talk_script	=	$_POST['question_talk_script'];

		$sql = "INSERT INTO $config[db_base_name].follow_main_question ";
		$sql .= "(event_id_ref,		question_title,		question_detail,		question_talk_script";
		$sql .= ")VALUES(";
		$sql .= "'$event_id_ref',	'$question_title',	'$question_detail',	'$question_talk_script')";

		mysql_query('SET NAMES UTF8');
		$result = mysql_query($sql);
		$main_question_id = mysql_insert_id();
		if($result){
			if($_GET['add_question']=="manage_question"){//ถ้าเป็นการเพิ่มจากหน้า จัดการคำถาม manage question
				$js = "<script type='text/javascript'>
						setTimeout(\"followGetData('display_event','ic_followcus_form.php?func=manage_question&event_id=$event_id_ref')\",1000);</script>";
				echo "<font color='green'>เพิ่มข้อมูลเรียบร้อย กำลังโหลดข้อมูลใหม่... <img src='images/loader_32x32.gif' align='absmiddle'> </font>".$js;
			}else {//เพิ่มคำถามตอนสร้าง event ใหม่
				if($main_question_id){	// ให้สร้างแบบฟอร์ม รายการคำถามย่อย
					$pleasure_check = "0";
					$question_list_form_title = "เพิ่มรายการคำถามย่อย สำหรับหัวข้อคำถามนี้";
					$list_head = $tpl->tbHtml('ic_followcus_form.html','QUESTION_FORM_HEAD');
					$list_form = $tpl->tbHtml('ic_followcus_form.html','QUESTION_LIST_FORM');
					echo $list_head.'<div id="question_list" style="color:black">'.$list_form.'</div>';
				}
			}
		}
	}//end check

}else if($func=="add_question_list"){//บันทึกรายการคำถามย่อย ทั้งแก้ไข และเพิ่มใหม่
	if($_POST['main_question_id']){//check
		$main_question_id	= $_POST['main_question_id'];
		$id					= $_POST['list_id'];//ได้ตอนแก้ไข
		$list_title			= $_POST['list_title'];
		$input_type			= $_POST['input_type'];
		//2011-08-05
		if($_POST['add_br']=='on'){
			$other_option = "<br>";
		}
		if($_POST['answer_option']){
			//$answer_option	= str_replace("\n","",$_POST['answer_option']);
			//$answer_option	= "\'". str_replace(",","\',\'",$answer_option)."\'";//จะรับค่า \n ไปด้วย
			$ans	= explode(",",$_POST['answer_option']);
			$ansopt = array();
			foreach($ans as $val){
				$ansopt[] = trim($val);
			}
			$answer_option = "'". implode("','",$ansopt) ."'";
			$answer_option = addslashes($answer_option);
		}
		$pleasure_check		=  $_POST['pleasure_check'];

		$sql  = "REPLACE INTO $config[db_base_name].follow_question_list ";
		$sql .= "(id,		main_question_id,		list_title,		input_type,		answer_option,	other_option,	pleasure_check)VALUES";
		$sql .= "('$id',	'$main_question_id',	'$list_title',	'$input_type',	'$answer_option', '$other_option',	'$pleasure_check')";

		mysql_query('SET NAMES UTF8');
		$result = mysql_query($sql) or die('ERROR : '. mysql_error());
		if($result){
			$event_id = $_POST['event_id'];
			if($event_id){//ถ้ามาจากหน้า จัดการคำถาม
				//$msg = "<p class='title3 green'>บันทึกรายการคำถามเรียบร้อย<br>		<span style='color:blue;font-weight:100'>รอสักครู่ กำลังโหลดข้อมูลใหม่...</span><p>";
				$msg = "";
				$js  = "document.getElementById('frm_q_list').style.display='none';
						//setTimeout(\"followGetData('display_event','ic_followcus_form.php?func=manage_question&event_id=$event_id')\",1000);
						followGetData('td_question_list_table$main_question_id','ic_followcus_form.php?func=get_question_list&main_question_id=$main_question_id');
						";
			}else {//ตอนสร้าง event ใหม่
				$msg = "<span style='font-weight:100;font-size:12px;color:blue'>บันทึกรายการคำถามเรียบร้อย. เพิ่มคำถามข้อต่อไป หรือ ไปที่หน้า <a href='javascript:void(0)' onclick=\"followGetData('main','ic_followcus_form.php')\" style='color:orangered'>ชื่อกิจกรรมหลัก</a></span>";
				//reset form
				$js = "document.getElementById('frm_q_list').reset();
						document.getElementById('add_br').checked = false;
						document.getElementById('input_list_title').value='';
						document.getElementById('pleasure_check').value='0';
						document.getElementById('answer_option').value='';
						document.getElementById('view_input').innerHTML = '';
						document.getElementById('input_list_title').focus();
						followGetData('show_ques_list','ic_followcus_form.php?func=view_question_list&main_question_id=$main_question_id');
				";
			}
			$javascript = "<script type='text/javascript'>".$js."</script>";
			echo $msg.$javascript;
		}
	}//end check

//MANAGE QUESTION
}elseif($func=="manage_question") {
	$event_id = $_GET['event_id'];
	$manage_question = "event_id=$event_id";
	$sql_fol  = "SELECT id,question_title FROM $config[db_base_name].follow_main_question ";
	$sql_fol .= "WHERE event_id_ref = '$event_id' AND status != 99 ORDER by id";
	$result_fol = mysql_query($sql_fol) or die("เกิดข้อผิดพลาด :: ". mysql_error());

	$event_title = query("SELECT event_title FROM $config[db_base_name].follow_main_event WHERE id = '$event_id'");	//function/general.php

	$cls[1] = "bg_green1";
	$cls[2] = "bg_green2";
	while($rs = mysql_fetch_assoc($result_fol)) {
		$no++;
		if($no % 2){$i=1;}else{$i=2;}
		$tr_bg = $cls[$i];

		$main_question_id = $rs['id'];
		//รายการคำถามย่อย
		$question_list = "";
		$question_list = get_question_list($main_question_id);//function/ic_followcus_form.php
		//คำถามหลัก
		$manage_list .= $tpl->tbHtml('ic_followcus_form.html','MANAGE_MAINQUEST_LIST');
	}
	if($manage_list==""){
		$colspan = 	4;
		$config['null'] = "ยังไม่ได้สร้างคำถามสำหรับกิจกรรมนี้ครับ <a href='javascript:void(0)' style='color:green;margin:20px;' onclick=\"followGetData('question_list','ic_followcus_form.php?func=add_new_main_question&event_id=$event_id');\">เพิ่มคำถาม คลิกที่นี่</a>";
		$manage_list = $tpl->tbHtml('ic_followcus.html','NULL');
	}
	mysql_free_result($result_fol);

	$prev_sql = "SELECT event_id_ref FROM $config[db_base_name].follow_event_refer WHERE next_event_id = '$event_id' AND status != '99'";
	$prev_event_id_in = sql_in($prev_sql);
	$prev_title = "รายชื่อกิจกรรมก่อนหน้าที่จะส่งมา หลังจากที่ทำรายการเสร็จ";
	$prev_event = "<span title='$prev_title'><img src='images/inbox_green.png' align='absbottom' width='32' height='32'> .................................................</span>";
	if($prev_event_id_in){
		$prev_event_id_in = str_replace("'", "", $prev_event_id_in);
		$prev_event = "<a href='javascript:void(0)' style='color:saddlebrown' title='$prev_title' onclick=\"followGetData('div_tb_question','ic_followcus_form.php?func=view_event_list&view_refer=$prev_event_id_in&refer_title=$prev_title');hideObj('question_list');\">
		<img src='images/inbox_green.png' align='absbottom' width='32' height='32'>[ รายชื่อกิจกรรมที่ส่งเข้ามา ]</a>";
	}

	$next_sql = "SELECT next_event_id FROM $config[db_base_name].follow_event_refer WHERE event_id_ref = '$event_id' AND status != '99'";
	$next_event_id_in = sql_in($next_sql);
	$next_title = 'รายชื่อกิจกรรมที่จะทำต่อไป หลังจากที่ทำรายการเสร็จ';
	$next_event = "<span title='$next_title'>................................................. <img src='images/outbox_yellow.png'  align='absbottom' width='32' height='32'></span>";
	if($next_event_id_in){
		$next_event_id_in = str_replace("'", "", $next_event_id_in);
		$next_event = "<a href='javascript:void(0)' style='color:saddlebrown' title='$next_title' onclick=\"followGetData('div_tb_question','ic_followcus_form.php?func=view_event_list&view_refer=$next_event_id_in&refer_title=$next_title');hideObj('question_list');\">[ รายชื่อกิจกรรมที่จะส่งต่อ/รอสัมภาษณ์ ]<img src='images/outbox_yellow.png'  align='absbottom' width='32' height='32'></a>";
	}

	$table = $tpl->tbHtml('ic_followcus_form.html','MANAGE_QUESTION_HEAD');
	echo $table;

}elseif($func=="add_new_main_question") {	//แบบฟอร์มเพิ่มคำถาม จาก manage_question
	$event_id = $_GET['event_id'];
	$display  = "none";
	$manage_question = "add_question=manage_question";//ส่งตัวแปรว่ามาจากหน้าจัดการคำถาม
	echo $tpl->tbHtml('ic_followcus_form.html','MAIN_QUESTION_FORM');

}elseif($func=="create_wait_event") {		//สร้างรายการส่งต่อ รอสัมภาษณ์
	$_GET['chk_module'] = array_filter($_GET['chk_module']);
	
	if(!empty($_GET['chk_module'])){
		$inum = 1;
		foreach($_GET['chk_module'] as $biz_id){
			if($inum != 1){
				$business_inps_name .= '@#@';
				$business_unit_name .= '  ,  ';
			}
			
			$biz_name = query("SELECT biz_name FROM $config[db_organi].biz_name WHERE biz_id = '$biz_id' ");
		
			$business_inps_name .= $biz_id;
			$business_unit_name .= $biz_name; 
			$inum = 2;
		}
	}

	$prev_event_id = $_GET['event_id'];
	$wait_form_title = "<font color='seagreen'>แบบฟอร์ม <font color='orange'>สร้าง</font> กิจกรรมที่ต้องการส่งต่อ (รอสัมภาษณ์)</font>";
	//$department = getDepartment('','leader');
	$send2 = 'checked="checked"';
	echo $tpl->tbHtml('ic_followcus_form.html','WAIT_EVENT_FORM');

}elseif($func=="form_edit_wait_event") {	//แก้ไขรายการส่งต่อ รอสัมภาษณ์
	$event_id = $_GET['event_id'];
	$sql = "SELECT * FROM $config[db_base_name].follow_main_event WHERE id = '$event_id' ";
	$result = mysql_query($sql);
	$rs = mysql_fetch_assoc($result);

	//$department = getDepartment($rs['department'],'leader');	// function/ic_followcus_form.php
	//$who_response = $rs['who_response'];			//ใส่ลงใน value ของ checkbox
	//$who_name	  = select_emp_nametonickname($rs['who_response']);// function/general.php
	
	
	#--- BUSINESS UNIT NAME
	$objQuery = mysql_query("SELECT biz_id_ref FROM $config[db_base_name].follow_event_biz WHERE event_id_ref = '$event_id' ");
	$inum = 1;
	while($rsm = mysql_fetch_assoc($objQuery)){
		if($inum != 1){
			$business_inps_name .= '@#@';
			$business_unit_name .= '  ,  ';
		}
		
		$biz = query("SELECT biz_name, biz_id FROM $config[db_organi].biz_name WHERE biz_id = '$rsm[biz_id_ref]' ",1);
	
		$business_inps_name .= $biz['biz_id'];
		$business_unit_name .= $biz['biz_name']; 
		$inum = 2;
	}
	
	
	
	$wait_form_title = "<font color='seagreen'>แบบฟอร์ม <font color='orange'>แก้ไข</font> กิจกรรมส่งต่อ (รอสัมภาษณ์)</font>";
	switch($rs['remind_unit']) { 			//หน่วย รูปแบบการแจ้งเตือน
		case "day":		$re_sel1 = "selected='selected'";	break;
		case "week":	$re_sel2 = "selected='selected'";	break;
		case "month":	$re_sel3 = "selected='selected'";	break;
		case "year":	$re_sel4 = "selected='selected'";	break;
	}
	switch(strtolower($rs['cal_unit'])) {	//หน่วย กำหนดการแจ้งเตือน
		case "day":		$cal_day = "selected='selected'";	break;
		case "week":	$cal_week = "selected='selected'";	break;
		case "month":	$cal_month = "selected='selected'";	break;
		case "year":	$cal_year = "selected='selected'";	break;
	}
	switch(strtolower($rs['start_unit'])) {	//หน่วย แจ้งเตือนก่อนกำหนด
		case "day":		$start_day = "selected='selected'";	break;
		case "week":	$start_week = "selected='selected'";	break;
		case "month":	$start_month = "selected='selected'";	break;
		case "year":	$start_year = "selected='selected'";	break;
	}
	switch(strtolower($rs['stop_unit'])) {	//หน่วย แจ้งเตือนหลังกำหนด
		case "day":		$stop_day = "selected='selected'";	break;
		case "week":	$stop_week = "selected='selected'";	break;
		case "month":	$stop_month = "selected='selected'";	break;
		case "year":	$stop_year = "selected='selected'";	break;
	}

	switch(strtolower($rs['send_to_tis'])) {	//ส่งให้ตรีเพชร
		case "yes":	$send1 = "checked='checked'"; break;
		default: $send2 = "checked='checked'"; break;
	}
	
	
	//ต้องใช้การคลิก เพื่อแสดงผล
	switch(strtolower($rs['remind_type'])){
		case "once":	$remind_check = "re_type1";break;
		case "alway":	$remind_check = "re_type2";break;
	}
	switch(strtolower($rs['cal_format'])) {
		case "ondate":	$cal_check = "cal1"; break;
		case "after":	$cal_check = "cal3"; break;
	}
	
	
	
	// -- list ผู้ที่ติดตามกิจกรรม
	switch($rs['follow_up_activities']){
		case '1':
			$who = "follow_up1";
		break;
		case '2':
			$who = "follow_up2";
			$javaCheck = $who;
	
			# -- select follow_event_responsible "กำหนดความสัมพันธ์บุคคลที่ติดตามกิจกรรม"
			$eventResponsible = mysql_query("SELECT * FROM $config[db_base_name].follow_event_responsible 
			WHERE main_event_id = '$event_id' ") or die("error : ".__LINE__.":".__FILE__.mysql_error()) ; 
			while($eveRes = mysql_fetch_assoc($eventResponsible)){
				$arrChk = array(
					"comp" 		=> $eveRes['resps_comp'],
					"depa" 		=> $eveRes['resps_department'],
					"sect" 		=> $eveRes['resps_section'],
					"posi" 		=> $eveRes['resps_position']
				);
				
				$arrChkeys 	= array_keys(array_filter($arrChk));
				$tr_ids		= $arrChkeys[0].$arrChk[$arrChkeys[0]]; 
				$arr[comp] = $arr[depa] = $arr[sect] = $arr[posi] = '';
				switch($arrChkeys[0]){
					case 'comp': 
						$arr[comp] = query("SELECT name FROM $config[db_organi].working_company WHERE 
						id = '".$arrChk[$arrChkeys[0]]."' AND status <> '99' ");
						$follower_name	= "follower_up_comp[]"; 
					break;
					case 'depa': 
						$depa = query("SELECT working_company.name AS work_name, department.name AS depart_name FROM 
						$config[db_organi].working_company Inner Join $config[db_organi].department ON 
						working_company.id = department.company WHERE department.id = '".$arrChk[$arrChkeys[0]]."' 
						AND working_company.status <> '99' AND department.status <> '99' ",1);
						
						$arr[comp] = $depa['work_name'];
						$arr[depa] = $depa['depart_name'];
						
						$follower_name	= "follower_up_depa[]"; 
					break;
					case 'sect':
						$sect = query("SELECT section.name AS section_name, department.name AS department_name,
						department.company FROM $config[db_organi].section Inner Join $config[db_organi].department ON department.id = section.department WHERE section.status <> '99' AND department.status <> '99' 
						AND section.id = '".$arrChk[$arrChkeys[0]]."' ",1);
						
						$arr[comp] = query("SELECT name FROM $config[db_organi].working_company WHERE 
						id = '".$sect['company']."' AND status <> '99' ");
						$arr[depa] = $sect['department_name'];
						$arr[sect] = $sect['section_name'];
						
						$follower_name	= "follower_up_sect[]"; 
					break;
					case 'posi': 
						$posi = query("SELECT Name, WorkCompany, Department, Section FROM 
						$config[db_organi].position WHERE Status != '99' ",1);  

						$arr[comp] = query("SELECT name FROM $config[db_organi].working_company WHERE 
						id = '".$posi['WorkCompany']."' AND status <> '99' ");
						$arr[depa] = query("SELECT name FROM $config[db_organi].department WHERE 
						id = '".$posi['Department']."' AND status <> '99' ");
						$arr[sect] = query("SELECT name FROM $config[db_organi].section WHERE 
						id = '".$posi['Section']."' AND status <> '99' ");
						$arr[posi] = $posi['Name'];
						
						$follower_name	= "follower_up_posi[]";  
					break;
				}
				
				$ids_value = $arrChk[$arrChkeys[0]];
				
				$the_follwer_list .= $tpl->tbHtml('ic_followcus_form.html','the-follwer-list');
				
			}
			
			$follow_organization_boxs = $tpl->tbHtml('ic_followcus_form.html','FOLLOW_ORGANIZATION_BOXS');
		
		break;
	}
	
	// -- end list ผู้ที่ติดตามกิจกรรม 
	
	
	//แบบฟอร์ม เหตุการณ์ส่งต่อ
	$form = $tpl->tbHtml('ic_followcus_form.html','WAIT_EVENT_FORM');



	// if($rs['department']){ 
		// $who = "who1";
	// }else if($rs['who_response']) {	
		// $who = "who2";	
	// }
	

	$js = "<script tyle='text/javascript'>
			document.getElementById('$remind_check').click();
			document.getElementById('$cal_check').click();
			
			document.getElementById('$who').checked=true;
			
			if('$who'=='follow_up2'){ 
				$('#organization_boxs').show();
			}
			
			if(document.getElementById('input_list_title')){
				document.getElementById('input_list_title').focus();
			}
		</script>";
	echo $form.$js;

}elseif($func=="auto_emp_name") {
	$serial = follow_auto_emp_name($_GET['keyword']);//function/general.php
	$arr = explode("|",$serial);
	foreach ($arr as $val){
		$a++;
		$class = 'all_cus_tr2';
		if($a % 2){ $class = 'all_cus_tr1';}
		$exp = explode("_",$val);
		if($val){
		$list .= "<div class='$class' style='padding:3px;color:blue' onclick=\"follow_get_id_card('$exp[0]','$exp[1]')\">".$exp[1]."</div>";
		}
	}
	$text = '<div size="2" style="color:#660000;background:#F7D092;border:1px solid #eee">ค้นหาคำเหมือน [ <i style="color:blue"><u>';
	$text .= $keyword.'</u></i> ]<br> กรุณาเลือกตามรายการด้านล่าง</div>';
	$text .= $list;
	echo $text;

//สำหรับต้องการ echo ตารางแสดงรายการย่อย ด้วย javascript function show_question_list()
}elseif($func=="get_question_list") {
	$main_question_id = $_GET['main_question_id'];
	echo get_question_list($main_question_id);//function/ic_followcus_form.php
//end

}else if($func=="show_question_detail"){//ป๊อบอัพแก้ไข หัวข้อ , รายละเอียด , สคริปต์คำพูด แบบแยกทีละฟิลด์
	$main_question_id = $_GET['main_question_id'];
	$field			  = $_GET['field'];
	$sql = "SELECT $field FROM $config[db_base_name].follow_main_question WHERE id = '$main_question_id' AND status != '99' ";
	$result = mysql_query($sql);
	$rs = mysql_fetch_assoc($result);
	if($field=="question_title"){
		$title = "หัวข้อคำถามหลัก";
		$pointer = "bt_title$main_question_id";//จัดอ้างอิง
		$left = -468;//เลื่อนจาก pointer ไปทางซ้าย
	}else if($field=="question_detail"){
		$title = "รายละเอียดของคำถาม";
		$pointer = "bt_detail$main_question_id";//จัดอ้างอิง
		$left = -562;//เลื่อนจาก pointer ไปทางซ้าย
	}else if($field=="question_talk_script"){
		$title = "บทสนทนา แนวทางในการพูดคุย";
		$pointer = "bt_talk$main_question_id";
		$left = -658;
	}
	//$text = nl2br($rs[$field]); //ใช้แท็ก pre จัดรูปแบบ
	$text = $rs[$field];
	echo $tpl->tbHtml('ic_followcus_form.html','MANAGE_MAINQUEST_DETAIL');
	//แสดงป๊อบอัพ
	echo "<script type='text/javascript'>
			showBeside('manage_popup','$pointer',$left ,34);
			showBeside('pointer','$pointer',45,22);
		</script>";

}elseif($func=="update_mainquestion_detail") {//แก้ไขคำถามหลัก ตามชื่อ $field ที่ส่งมา
	$message 		  = $_POST['message'];
	$field			  = $_POST['field'];
	$main_question_id = $_POST['main_question_id'];
	$sql = "UPDATE $config[db_base_name].follow_main_question SET $field = '$message' WHERE id = '$main_question_id' ";
	mysql_query("SET NAMES UTF8");
	$result = mysql_query($sql) or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
	if($result){
		//$message = str_replace(" ", "&nbsp;", $message);
		//$alert = nl2br($message);//ข้อความี่แก้ไขใหม่
		/* ใช้ tag PRE ในการจัดรูปแบบแทน */
		$alert = $message;
		if($field=="question_title"){
			$change_title = "document.getElementById('title$main_question_id').innerHTML = '$message';";
		}
		//ซ่อนกล่องแก้ไข
		$js = "<script type='text/javascript'>
				showObj('bt_edit_detail,show_manage_msg');
				document.getElementById('show_manage_msg').innerHTML='<p align=\"left\" class=\"green\">บันทึกข้อมูลเรียบร้อย</p>';
				$change_title
			</script>";
	}else {
		$alert = "<font color='red'>ไม่สามารถบันทึกข้อมูลได้</font>";
	}
	echo $alert.$js;

}elseif($func=="confirm_del_mainquestion") {//ยืนยันการลบ คำถามหลัก
	$question_title = $_GET['question_title'];
	$main_question_id = $_GET['main_question_id'];
	$rs[0] = 0;
	$sql_list  = "SELECT count(id) FROM $config[db_base_name].follow_question_list ";
	$sql_list .= "WHERE main_question_id = '$main_question_id' AND status != '99' ORDER by id";
	$result_list = mysql_query($sql_list) or die("เกิดข้อผิดพลาด :: ". mysql_error());
	$rs = mysql_fetch_array($result_list);
	echo $tpl->tbHtml('ic_followcus_form.html','CONFIRM_DEL_QUESTION');

}elseif($func=="del_main_question") {//ลบคำถามหลัก
	$event_id = $_GET['event_id'];
	$main_question_id = $_GET['main_question_id'];
	$sql = "UPDATE $config[db_base_name].follow_main_question SET status = '99' WHERE id = '$main_question_id' ";
	$result1 = mysql_query($sql) or die("<p class='error'>เกิดข้อผิดพลาด</p>".mysql_error());
	if($result1){
		$sql2 = "UPDATE $config[db_base_name].follow_question_list SET status = '99' WHERE main_question_id = '$main_question_id' ";
		$result2 = mysql_query($sql2) or die("<p class='error'>เกิดข้อผิดพลาด</p>".mysql_error());
	}
	if($result2){
		$msg = "<p class='title2 green'>ลบคำถามเรียบร้อย โหลดข้อมูลใหม่... <img src='images/loader_32x32.gif' align='absmiddle'></p>";
		//ให้รีโหลดหน้าจัดการคำถาม
		$js  = "<script type='text/javascript'>
				setTimeout(function(){
					followGetData('display_event','ic_followcus_form.php?func=manage_question&event_id=$event_id');
					hideObj('div_popup');
					},1000);
				</script>";
		echo $msg.$js;
	}

//question list
}else if($func=="view_question_list") {//แสดงรายการคำถามย่อยที่เพิ่มเข้าไปใหม่
	$main_question_id = $_GET["main_question_id"];
	$sql_fol  = "SELECT id,main_question_id,list_title,input_type,answer_option,pleasure_check,status FROM $config[db_base_name].follow_question_list ";
	$sql_fol .= "WHERE main_question_id = '$main_question_id' AND status != '99' ORDER by order_number ";
	$result_fol = mysql_query($sql_fol) or die("เกิดข้อผิดพลาด :: ". mysql_error());
	$class[1] = "event_tr1";
	$class[2] = "event_tr2";
	while($row_fol = mysql_fetch_assoc($result_fol)) {
		$a++;
		if($a>2){$a=1;}
		$bg = $class[$a];
		$row_fol['answer_option'] = stripcslashes($row_fol['answer_option']);
		$tr_list .= $tpl->tbHtml('ic_followcus_form.html','QUESTION_LIST');
	}
	mysql_free_result($result_fol);
	echo $tpl->tbHtml('ic_followcus_form.html','QUESTION_LIST_HEAD');

}else if($func=="new_question_list") {//แสดงแบบฟอร์มสำหรับเพิ่ม คำถามยอย
	$question_list_form_title = "<font color='#FF9C08'>เพิ่ม</font> รายการคำถามย่อย ของหัวข้อคำถาม : <font color='blue'>$main_question_title</font>";
	$main_question_id 	= $_GET['main_question_id'];
	$manage_question = "event_id=".$_GET['event_id'];//มาจากหน้า manage question
	$pleasure_check = "0";
	echo $tpl->tbHtml('ic_followcus_form.html','QUESTION_LIST_FORM');

}else if($func=="edit_question_list") {//แก้ไขรายการคำถามย่อย
	$list_id = $_GET['list_id'];
	//ส่งมาจากหน้า manage_question
	$manage_question = "event_id=$_GET[event_id]";
	$main_question_title = $_GET['main_question_title'];
	//
	$sql  = "SELECT * FROM $config[db_base_name].follow_question_list WHERE id = '$list_id'";
	$result = mysql_query($sql) or die("เกิดข้อผิดพลาด :: ". mysql_error());
	$rs = mysql_fetch_assoc($result);

	$main_question_id 	= $rs['main_question_id'];
	$list_title 		= $rs['list_title'];
	$pleasure_check 	= $rs['pleasure_check'];

	if(empty($pleasure_check)){$pleasure_check=0;}
	$display1 = "none";$display2 = "none";$display3 = "none";
	switch($rs['input_type']) {
		case "textbox": $type = 'type1';		break;
		case "textarea": $type = 'type2';		break;
		case "checkbox": $type = 'type3';		break;
		case "radio": $type = 'type4';			break;
		case "selectbox": $type = 'type5';		break;
		case "pleasure_value": $type = 'type6';	break;
	}
	$rs['answer_option'] = stripcslashes($rs['answer_option']);
	$answer			= substr($rs['answer_option'],1);//ตัดเครื่องหมาย ' ด้านหน้า
	$answer_option 	= substr($answer,0,-1);			//ตัดเครื่องหมาย ' ด้านหลัง
	$answer_option	= str_replace("','",",",$answer_option);

	$question_list_form_title = "<font color='orange'>แก้ไข</font> รายการคำถามย่อย";

	echo $tpl->tbHtml('ic_followcus_form.html','QUESTION_LIST_FORM');
	echo "<script tyle='text/javascript'>
			document.getElementById('$type').click();
			window.scrollBy(0,document.body.scrollHeight);
		</script>";

}else if($func=="del_question_list") {//ลบ คำถามย่อย
	$id_in = "'". str_replace(",","','",$_GET['list_id'])."'";//output = '01','111',...,'NNN'
	$sql = "UPDATE $config[db_base_name].follow_question_list SET status='99' WHERE id IN($id_in) ";
	$result = mysql_query($sql) or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
	if($result){
		$tr_hide = "tr_". str_replace(",",",tr_",$_GET['list_id']);//output = 'tr_01,tr_111,...,tr_NNN'
		$success = "<br><p class='title3' style='color:green'>ลบรายการที่เลือกเรียบร้อยแล้วครับ.</p>";
		$success .= "<script type='text/javascript'>hideObj('$tr_hide');</script>";//ซ่อนแถวที่ถูกลบแล้ว
	}else {
		$success = "<p class='error'>ไม่สามารถลบข้อมูลได้ครับ</p>";
	}
	echo $success;

//**** Pattern
}else if($func=="field_pattern_list") {//แสดงรายการ pattern วันที่ ทั้งหมด
	$cls[1] = "all_cus_tr1";
	$cls[2] = "all_cus_tr2";
	$sql  = "SELECT * FROM $config[db_base_name].follow_field_pattern WHERE status != '99' ";
	$sql .= "ORDER by id,pattern_name ";
	$result = mysql_query($sql) or die("เกิดข้อผิดพลาด :: ". mysql_error());
	$id_num = 1;
	while($row = mysql_fetch_assoc($result)) {
		$a++;
		if($a>2){$a=1;}
		$class = $cls[$a];
		$tr_list .= $tpl->tbHtml('ic_followcus_form.html','FIELD_PATTERN_LIST');
		$id_num++;
	}
	mysql_free_result($result);
	if($tr_list==""){
		$colspan = 10;
		$config['null'] = "<span class='text-center'>ไม่มีรายการ pattern วันที่ สำหรับติดตามลูกค้าครับ</span>";
		$tr_list = $tpl->tbHtml('ic_followcus.html','NULL');
	}
	echo $tpl->tbHtml('ic_followcus_form.html','FIELD_PATTERN');

//ลิสต์รายชื่อฐานข้อมูล
}else if($func=="field_pattern_form") {//แบบฟอร์มเพิ่ม pattern
	$db_name = getDatabaseName();
	//$db_name = get_db_name();
	echo $tpl->tbHtml('ic_followcus_form.html','FIELD_PATTERN_FORM');
}else if($func=="change_database"){//ลิสต์รายชื่อตาราง
	$tb_name = getTableName($_GET['db_name'],'');
	if($tb_name==""){
		$first = "<option value='' style='color:red' class='text-left'> ไม่มีฟิลด์ที่เป็นวันที่</option>";
	}
	echo $first.$tb_name;

}else if($func=="change_table"){//ลิสต์รายชื่อฟิลด์
	$tb_name = $_GET['tb_name'];
	$field_name = getFieldName($tb_name);
	$first = "<option value='' class='text-left'>&nbsp;&nbsp;เลือกชื่อฟิลด์ข้อมูล </option>";
	if($field_name==""){
		$first = "<option value='' style='color:red' class='text-left'> ไม่มีฟิลด์ที่เป็นวันที่</option>";
	}
	echo $first.$field_name;

}else if($func=="change_table_expire"){//ลิสต์รายชื่อฟิลด์
	$tb = query("SELECT db_name,table_name FROM $config[db_base_name].follow_field_pattern 
	WHERE id = '".$_GET['pattern_id_ref']."' ",1);
	if(!empty($tb)){ $tb_name = $config[$tb['db_name']].'.'.$tb['table_name']; }else{$tb_name='';} 
	$field_name = getFieldName($tb_name);
	$first = "<option value='' class='text-left'>&nbsp;&nbsp;เลือกชื่อฟิลด์ข้อมูล </option>";
	if($field_name==""){
		$first = "<option value='' style='color:red' class='text-left'> ไม่มีฟิลด์ที่เป็นวันที่</option>";
	}
	echo $first.$field_name;
}else if($func=="change_table_field_cus"){//ลิสต์รายชื่อฟิลด์
	$tb_name = $_GET['tb_name'];
	$field_name = getFieldNameCus($tb_name);
	$first = "<option value='' class='text-left'>&nbsp;&nbsp;เลือกชื่อฟิลด์ข้อมูลคน </option>";
	if($field_name==""){
		$first = "<option value='' style='color:red' class='text-left'> ไม่มีฟิลด์ที่เป็นข้อมูลคน</option>";
	}
	echo $first.$field_name;

}else if($func=="change_table_field_vehicle"){//ลิสต์รายชื่อฟิลด์
	$tb_name = $_GET['tb_name'];
	$field_name = getFieldNameVehicle($tb_name);
	$first = "<option value='' class='text-left'>&nbsp;&nbsp;เลือกชื่อฟิลด์ข้อมูลรถ </option>";
	if($field_name==""){
		$first = "<option value='' style='color:red' class='text-left'> ไม่มีฟิลด์ที่เป็นข้อมูลรถ </option>";
	}
	echo $first.$field_name;

}else if($func=="change_field_branch_name"){//ลิสต์รายชื่อฟิลด์
	$tb_name = $_GET['tb_name'];
	$field_name = getFieldNameBrand($tb_name);
	$first = "<option value='' class='text-left'>&nbsp;&nbsp;เลือกชื่อฟิลด์ข้อมูลสาขา </option>";
	if($field_name==""){
		$first = "<option value='' style='color:red' class='text-left'> ไม่มีฟิลด์ที่เป็นข้อมูลสาขา </option>";
	}
	echo $first.$field_name;

}else if($func=="edit_pattern_form") {//แก้ไข pattern วันที่
	$pattern_id  = $_GET['pattern_id'];
	$pattern_name= $_GET['pattern_name'];
	$db			 = $config[$_GET['db']];
	$tb 		 = $_GET['tb'];
	$field 		 = $_GET['field'];
	$table 		 = $db.".".$tb;
	$add_page	 = $_GET['add_page'];
	$db_name = getDatabaseName($db);
	$tb_name = getTableName($db,$tb);
	$field_cus_name = getFieldNameCus($table,$_GET['field_cus_name']);
	$field_vehicle_name = getFieldNameVehicle($table,$_GET['field_vehicle_name']);
	$field_branch_name = getFieldNameBrand($table,$_GET['field_branch']);
	$field_expire_date = getFieldName($table,$_GET['field_expire_date']);
	$field_name = getFieldName($table,$field);
	echo $tpl->tbHtml('ic_followcus_form.html','FIELD_PATTERN_FORM');

}else if($func=="add_pattern") {//เพิ่ม pattern วันที่
	if($_POST["pattern_name"]){//check
		$pattern_id 	= $_POST["pattern_id"];
		$pattern_name 	= $_POST["pattern_name"];
		$field_name = $_POST["field_name"];
		$table_name = $_POST["tb_name"];
		//$db_name 	= $_POST["db_name"];
		$arr_db_name = array_keys($config, $_POST["db_name"]);
		$db_name 	 = $arr_db_name[0];
		$add_page	 = $_POST["add_page"];

		$sql  = "REPLACE INTO $config[db_base_name].follow_field_pattern ";
		$sql .= "(id, pattern_name, field_name, table_name, db_name, field_cus_name, field_vehicle_name,field_branch";
		$sql .= ")VALUES(";
		$sql .= "'$pattern_id',	'$pattern_name', '$field_name', '$table_name', '$db_name', '".$_POST["field_cus_name"]."',
		'".$_POST["field_vehicle_name"]."','".$_POST["field_branch_name"]."') ";
		mysql_query("SET NAMES UTF8");
		$result = mysql_query($sql) or die("<p class='error'>เกิดข้อผิดพลาด ::</p>". mysql_error());
		if($result){
			$success 	= "<p class='title3 green'>บันทึกข้อมูลเรียบร้อย</p>";
			if($pattern_id){//ถ้าเป็นการ แก้ไขข้อมูล จะมีการส่ง pattern_id มาด้วย
				$javascript = "<script type='text/javascript'>";
				$javascript .= "setTimeout(function(){";
				$javascript .= "document.getElementById('pattern_name$_POST[pattern_id]').innerHTML = '$_POST[pattern_name]';";
				$javascript .= "document.getElementById('pattern_field$_POST[pattern_id]').innerHTML = '$_POST[field_name]';";
				$javascript .= "document.getElementById('pattern_table$_POST[pattern_id]').innerHTML = '$_POST[tb_name]';";
				$javascript .= "document.getElementById('pattern_db$_POST[pattern_id]').innerHTML = '$_POST[db_name]';";
				$javascript .= "setTimeout(function(){ hideObj('div_popup');},10); ";
				$javascript .= "},1000);</script>";
			}else {//ถ้าไม่มีไอดี คือการเพิ่มข้อมูลใหม่
				$javascript = "<script type='text/javascript'>";//javascript
				if($add_page==""){//ถ้าเพิ่มจากหน้า จัดการ pattern
					$success .= ' กรุณารอสักครู่ กำลังโหลดข้อมูลใหม่... <img src="images/loader_32x32.gif" align="absmiddle">';
					$javascript .= "setTimeout(function(){
									hideObj('div_popup');
									followGetData('display_event','ic_followcus_form.php?func=field_pattern_list');
	           					},1000); ";
	           	}else if($add_page=="add_event"){//ถ้าเป็นการเพิ่ม จากหน้า เพิ่มเหตุการณ์ใหม่ จะให้ echo option ลงใน Selectbox
					$pt_id = mysql_insert_id();
					$filed_pattern = getFieldPatternDate($pt_id);
					$javascript .= '$("#pattern_id_ref").html("'.$filed_pattern.'");';//ใช้ ฟังก์ชั่นของ jquery เพิ่ม option ใหม่ลงใน selectbox
					$javascript .= "setTimeout(function(){ hideObj('div_popup');},1000); ";
	           	}
				$javascript .= "</script>";
			}
		}else {
			$success = "<p class='error'>ไม่สามารถบันทึกข้อมูลได้ครับ</p>";
		}
		echo $success.$javascript;
	}//end check

}else if($func=="confirm_del_pattern") {//ป๊อบอัพ ยืนยันการลบ pattern วันที่
	if($_POST){
		$check_id = "'". join("','",$_POST['pattern_id']) ."'";
	}else if($_GET['pattern_id']){
		$check_id = $_GET['pattern_id'];
	}
	$sql = "SELECT pattern_name FROM $config[db_base_name].follow_field_pattern WHERE id IN($check_id)";
	$result = mysql_query($sql);
	while($row = mysql_fetch_array($result)) {
		$n++;
		$list .= $n.". ".$row[0]."<br>";
	}
	$send_data = str_replace("'","",$check_id);
	$table = "<div id='show_success' style='color:blue;padding:5px;line-height:20px;width:100%' align='left'>
				<p class='title2' style='color:orangered;' align='left'>รายการที่ต้องการลบ</p>";
	$table .= "<p style='padding-left:10px'>".$list."</p></div>";
	$table .= '<div style="padding:10px" align="center">
				<button class="nz_button bt_add_all" onclick="followPostData(\'show_success\',\'ic_followcus_form.php?func=del_pattern\',\'pattern_id='.$send_data.'\');this.style.display=\'none\';document.getElementById(\'cancel\').innerHTML=\'ปิด\';"><img src="images/trash.png" align="absbottom" height="20" width="25">ยืนยัน การลบ</button>
				
				<button class="nz_button bt_del_all" onclick="hideObj(\'show_list\')" height="20" width="25">
				<img src="images/closed.png" align="absbottom" > <span id ="cancel">ยกเลิก</span></button>';
	echo $table;

}else if($func=="del_pattern") {//ลบ pattern วันที่
	$pattern_id_in = "'". str_replace(",","','",$_POST['pattern_id'])."'";//output = '01','111',...,'NNN'
	$sql = "UPDATE $config[db_base_name].follow_field_pattern SET status='99' WHERE id IN($pattern_id_in) ";
	$result = mysql_query($sql) or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
	
	$obj_query = mysql_query("SELECT * FROM $config[db_base_name].follow_field_pattern WHERE id IN($pattern_id_in) ") 
	or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
	while($rs = mysql_fetch_assoc($obj_query)){
		mysql_query("INSERT INTO $config[db_base_name].follow_field_pattern_cancel (id, id_ref, pattern_name, field_name, field_cus_name, field_vehicle_name, table_name, db_name, status) VALUES ('', '".$rs['id']."',
		'".$rs['pattern_name']."', '".$rs['field_name']."', '".$rs['field_cus_name']."', '".$rs['field_vehicle_name']."',
		'".$rs['table_name']."', '".$rs['db_name']."', '".$rs['status']."')")
		or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
	
		mysql_query("DELETE FROM $config[db_base_name].follow_field_pattern WHERE 
		follow_field_pattern.id = '".$rs['id']."' ")  or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
	}
	

	if($result){
		$tr_hide = "tr_pattern". str_replace(",",",tr_pattern",$_POST['pattern_id']);//output = 'tr_pattern01,tr_pattern111,...,tr_patternNNN'
		$success = "<br><p class='title2 text-center' style='color:green'>ลบรายการที่เลือกเรียบร้อยแล้วครับ.</p>";
		$success .= "<script type='text/javascript'>hideObj('$tr_hide')</script>";//ซ่อนแถวที่ถูกลบแล้ว
	}else {
		$success = "<p class='error text-center'>ไม่สามารถลบข้อมูลได้ครับ</p>";
	}
	echo "<div id='show_success' style='color:blue;padding:5px;line-height:20px;width:100%' align='left'>".$success."</div>";

}else if($func=='update_question_list_order'){
	$sql = "UPDATE $config[db_base_name].follow_question_list SET order_number='$_POST[number]' WHERE id = '$_POST[q_list_id]' ";
	$result = mysql_query($sql) or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error().' '.__FILE__.' '.__LINE__);
}

/*  follow_cancel_pattern */

else if($func=="field_cancel_pattern_form") {   //แบบฟอร์มเพิ่ม  เงื่อนไขการยกเลิก  	
	$db_name = getDatabaseName();
	echo $tpl->tbHtml('ic_followcus_form.html','FIELD_CANCEL_PATTERN_FORM');
}else if($func=="follow_cancel_list") {//แสดงรายการ pattern วันที่ ทั้งหมด
	$cls[1] = "all_list_tr1";
	$cls[2] = "all_list_tr2"; 
	$sql  = "SELECT id,pattern_name,field_name,table_name,db_name FROM $config[db_base_name].follow_cancel_pattern WHERE status != '99' ";
	$sql .= "ORDER by id,pattern_name ";
	$result = mysql_query($sql) or die("เกิดข้อผิดพลาด :: ". mysql_error());
	$id_num = 1;
	while($row = mysql_fetch_assoc($result)) {
		$a++;
		if($a>2){$a=1;}
		$class = $cls[$a];
		$tr_list .= $tpl->tbHtml('ic_followcus_form.html','FIELD_CANCEL_PATTERN_LIST');
		$id_num++;
	}
	mysql_free_result($result);
	if($tr_list==""){
		$colspan = 6;
		$config['null'] = "<span class='text-center'>ไม่มีรายการ pattern วันที่ สำหรับติดตามลูกค้าครับ</span>";
		$tr_list = $tpl->tbHtml('ic_followcus.html','NULL');
	}
	echo $tpl->tbHtml('ic_followcus_form.html','FIELD_CANCEL_PATTERN');

//ลิสต์รายชื่อฐานข้อมูล
}else if($func=="confirm_del_cancel_pattern") {//ป๊อบอัพ ยืนยันการลบ pattern วันที่
	if($_POST){
		$check_id = "'". join("','",$_POST['pattern_id']) ."'";
	}else if($_GET['pattern_id']){
		$check_id = $_GET['pattern_id'];
	}
	$sql = "SELECT pattern_name FROM $config[db_base_name].follow_cancel_pattern WHERE id IN($check_id)";
	$result = mysql_query($sql);
	while($row = mysql_fetch_array($result)) {
		$n++;
		$list .= $n.". ".$row[0]."<br>";
	}
	$send_data = str_replace("'","",$check_id);
	$table = "<div id='show_success' style='color:blue;padding:5px;line-height:20px;width:100%' align='left'>
				<p class='title2' style='color:orangered;' align='left'>รายการที่ต้องการลบ</p>";
	$table .= "<p style='padding-left:10px'>".$list."</p></div>";
	$table .= '<div style="padding:10px" align="center">
				<button class="nz_button bt_add_all" onclick="followPostData(\'show_success\',\'ic_followcus_form.php?func=del_cancel_pattern\',\'pattern_id='.$send_data.'\');this.style.display=\'none\';document.getElementById(\'cancel\').innerHTML=\'ปิด\';"><img src="images/trash.png" align="absbottom" height="20" width="25">ยืนยัน การลบ</button>
				
				<button class="nz_button bt_del_all" onclick="hideObj(\'show_list\')" height="20" width="25">
				<img src="images/closed.png" align="absbottom" > <span id ="cancel">ยกเลิก</span></button>';
	echo $table;

}else if($func=="add_cancel_pattern") {     //เพิ่ม สร้างเงื่อนไขการยกเลิกใหม่
	if($_POST["pattern_name"]){//check
		$pattern_id 	= $_POST["pattern_id"];
		$pattern_name 	= $_POST["pattern_name"];
		$field_name 	= $_POST["field_name"];
		$table_name 	= $_POST["tb_name"];
		$db_name 		= $_POST["db_name"];
		$add_page		= $_POST["add_page"];

		$sql  = "REPLACE INTO $config[db_base_name].follow_cancel_pattern ";
		$sql .= "(id,	pattern_name,		field_name,		table_name,		db_name";
		$sql .= ")VALUES(";
		$sql .= "'$pattern_id',	'$pattern_name',	'$field_name',	'$table_name',	'$db_name') ";
		mysql_query("SET NAMES UTF8");
		$result = mysql_query($sql) or die("<p class='error'>เกิดข้อผิดพลาด ::</p>". mysql_error());
		if($result){
			$success 	= "<p class='title3 green'>บันทึกข้อมูลเรียบร้อย</p>";
			if($pattern_id){//ถ้าเป็นการ แก้ไขข้อมูล จะมีการส่ง pattern_id มาด้วย
				$javascript = "<script type='text/javascript'>";
				$javascript .= "document.getElementById('pattern_name$_POST[pattern_id]').innerHTML = '$_POST[pattern_name]';";
				$javascript .= "document.getElementById('pattern_field$_POST[pattern_id]').innerHTML = '$_POST[field_name]';";
				$javascript .= "document.getElementById('pattern_table$_POST[pattern_id]').innerHTML = '$_POST[tb_name]';";
				$javascript .= "document.getElementById('pattern_db$_POST[pattern_id]').innerHTML = '$_POST[db_name]';";
				$javascript .= "setTimeout(function(){ hideObj('div_popup');},1000); ";
				$javascript .= "</script>";
			}else {//ถ้าไม่มีไอดี คือการเพิ่มข้อมูลใหม่
				$javascript = "<script type='text/javascript'>";//javascript
				if($add_page==""){//ถ้าเพิ่มจากหน้า จัดการ pattern
					$success .= ' กรุณารอสักครู่ กำลังโหลดข้อมูลใหม่... <img src="images/loader_32x32.gif" align="absmiddle">';
					$javascript .= "setTimeout(function(){
									hideObj('div_popup');
									followGetData('display_event','ic_followcus_form.php?func=follow_cancel_list');
	           					},1000); ";
	           	}else if($add_page=="add_event"){//ถ้าเป็นการเพิ่ม จากหน้า เพิ่มเหตุการณ์ใหม่ จะให้ echo option ลงใน Selectbox
					$pt_id = mysql_insert_id();
					$filed_pattern = getFieldPatternDate($pt_id);
					$javascript .= '$("#pattern_id_ref").html("'.$filed_pattern.'");';//ใช้ ฟังก์ชั่นของ jquery เพิ่ม option ใหม่ลงใน selectbox
					$javascript .= "setTimeout(function(){ hideObj('div_popup');},1000); ";
	           	}
				$javascript .= "</script>";
			}
		}else {
			$success = "<p class='error'>ไม่สามารถบันทึกข้อมูลได้ครับ</p>";
		}
		echo $success.$javascript;
	}//end check
}else if($func=="del_cancel_pattern") {//ลบ pattern วันที่
	$pattern_id_in = "'". str_replace(",","','",$_POST['pattern_id'])."'";//output = '01','111',...,'NNN'
	$sql = "UPDATE $config[db_base_name].follow_cancel_pattern SET status='99' WHERE id IN($pattern_id_in) ";
	$result = mysql_query($sql) or die("<p class='error'>เกิดข้อผิดพลาด</p>". mysql_error());
	if($result){
		$tr_hide = "tr_pattern". str_replace(",",",tr_pattern",$_POST['pattern_id']);//output = 'tr_pattern01,tr_pattern111,...,tr_patternNNN'
		$success = "<br><p class='title2 text-center' style='color:green'>ลบรายการที่เลือกเรียบร้อยแล้วครับ.</p>";
		$success .= "<script type='text/javascript'>hideObj('$tr_hide')</script>";//ซ่อนแถวที่ถูกลบแล้ว
	}else {
		$success = "<p class='error text-center'>ไม่สามารถลบข้อมูลได้ครับ</p>";
	}
	echo "<div id='show_success' style='color:blue;padding:5px;line-height:20px;width:100%' align='left'>".$success."</div>";

}else if($func=="edit_cancel_pattern_form") {//แก้ไข pattern วันที่
	$pattern_id 	= $_GET['pattern_id'];
	$pattern_name	=$_GET['pattern_name'];
	$db				= $_GET['db'];
	$tb 			= $_GET['tb'];
	$field 			= $_GET['field'];
	$table 			= $db.".".$tb;
	$add_page		= $_GET['add_page'];
	$db_name = getDatabaseName($db);
	$tb_name = getTableName($db,$tb);
	$field_name = getFieldName($table,$field);
	echo $tpl->tbHtml('ic_followcus_form.html','FIELD_CANCEL_PATTERN_FORM');
}


CloseDB();

?>
