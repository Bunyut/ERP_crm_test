<?php
if(!isset($_SESSION['bUserQC'])){ @session_start(); } 
$_SESSION = array_filter($_SESSION); /* clear array ที่เป็น value ว่าง */  
require_once 'connect_db.php';
require_once 'inc/TplParser.php';
require_once 'inc/TplFunction.php';
require_once 'inc/pagination.inc.php';
require_once 'inc/genMenu_ou.php';

$chkLogin = new genMenu();
$chkLogin->chkSession_login();

ini_set('display_errors', 1); 
ini_set('log_errors', 1); 
error_reporting(E_ERROR | E_WARNING | E_PARSE);

$config['cookiename'] 	= "chipmunkcookie1";
$config['charset']		= "utf-8";
$config['UserTimeOut']	= "3600";
$config['title']		= "CRM   |   Customer Relationship Management   :: ";

$PSave = null;
$config['dir_libraries'] = 'libraries/';
$config['change_status'] = 1;
$config['la_line'] 		 = '15';
$config['countDay'] 	 = '8';
$config['la_level'] 	 = '3';   
$config['Show_TellBook'] = '3';

$config['saltkey']		= "momay2520pttamon";
$config['LinksPage']	= "../icchecktimeV2New/admin.php";
$config['ppang']		= 'http://'.$_SERVER['SERVER_NAME']."/";
$config['ppang2']		= 'http://'.$_SERVER['SERVER_NAME']."/";
$config['ppangs']		= 'http://'.$_SERVER['SERVER_NAME']."/";
$config['PerPageS']		= 20;

 # -- default date
$config['checkdate']	= date("Y-m-d");

##nongnong create
$config['custome_normal'] = "../ERP_maincus2/customers_sale.php?followcus_no=";
$config['custome_all'] = "../ERP_maincus2/customers_all.php?program=service&followcus_no=";

# -- DEFINE CONSTANT VARIABLE
$config['PART_HOME'] = 'crm/';
$config['DEPRECIATION_FIRST_YEAR'] = '20'; 													#--            ค่าเสื่อมปีแรก
$config['VAT'] = 0.07; 				   														#--            ภาษี
$config['VAT_INT'] = 7; 

$config['TO_DATE'] = date('d-m').'-'.(date('Y')+543);										# -- 	แสดงวันที่ปัจจุบัน


# -- DEFINE VARIABLE FOR PERMISSION
$config['DEPARTMENT'] = substr($_SESSION['SESSION_member_id'],0,3);                  	# -- check ว่าเป็น GA หรือ ACC
/* DEFINE CONSTANT VARIABLE */
$config['cus_image_path'] = '../icver03/cus_image/';  // path  สำหรับเก็บรูปภาพ main cus

$content = null;
$lang['operation_saved'] = '';

# -- TEMPLATE CLASS INITIAL
$tpl    = new TplParser;
$tpl->setDir('templates/');

/* PAGINATION CLASS INITIAL */
$page =  new Pagination();

# -- DEFINE CONSTANT VARIABLE
define('DIR_LIBRARIES',$config['dir_libraries']);
$config['LAST_VERSION'] = '0.1';
$config['LAST_UPDATE']  = '27-03-2012';




function changeUTF8($txt){
	$txt=str_replace("&nbsp;"," ",$txt);$out = ""; 
	for($i=0;$i<strlen($txt);$i++){if(ord($txt[$i]) <= 126){ $out .= $txt[$i];}else{ $out .= "&#" . (ord($txt[$i]) - 161 + 3585) . ";"; }}
	return $out; 
} 

function utf8_to_tis620($string){
	$str = $string; $res = "";
	for ($i = 0; $i < strlen($str); $i++) {
	  if (ord($str[$i]) == 224) {
		$unicode = ord($str[$i+2]) & 0x3F;
		$unicode |= (ord($str[$i+1]) & 0x3F) << 6;
		$unicode |= (ord($str[$i]) & 0x0F) << 12;
		$res .= chr($unicode-0x0E00+0xA0);
		$i += 2;
	  }else {
		$res .= $str[$i];
	  }
	}
	return $res;
}
	
//print "iconv_strlen: ".iconv_strlen($str,'UTF-8')."\n";

require_once DIR_LIBRARIES.'preferences.php';
?>
