<?php
session_start();

require_once $_SERVER['DOCUMENT_ROOT'].'/ERP_masterdata/inc/main_function.php';

$objDb	= '';
$objDb	= new configAllDatabase; // ERP_masterdata/inc/main_function.php
$objDb->calConfigAllDatabase(); // ERP_masterdata/inc/main_function.php

$config['ServerName']	= $objDb->con['ServerName'];
$config['UserName']		= $objDb->con['UserName'];
$config['UserPassword'] = $objDb->con['UserPassword'];

/// c การตั้งชื่อตัวแปร DB ให้ใช้ คำว่า db_ นำหน้าด้วย
/* if($_SERVER['SERVER_ADDR'] == "199.199.194.170"){
	$config['db_base_name'] = "main_crm";   			 	# - crm
	$config['db_emp'] 		= "icchecktime2";   			# -	checktime
	$config['db_maincus'] 	= "main_cusdata";         	 	# - maincus
	$config['Cus']			= 'new_maincusdata';			# - old maincus
	$config['db_organi'] 	= "OU";     					# - ou
	$config['db_leasing'] 	= "icleasing";     				# - leasing
	$config['db_easysale'] 	= "new_icmba";     				# - easysale
	$config['db_vehicle'] 	= "MainVehicle";     			# - mainVehicle
	$config['db_icservice'] = "icservice";     			# - dem_icservice
}else if($_SERVER['SERVER_ADDR'] == "199.199.194.180"){
	$config['db_base_name'] = "main_crm";   			 	# - crm
	$config['db_emp'] 		= "easyhr_icchecktime2";   		# -	checktime
	$config['db_maincus'] 	= "main_cusdata";         	 	# - maincus
	$config['Cus']			= 'new_maincusdata';			# - old maincus
	$config['db_organi'] 	= "easyhr_OU";     				# - ou
	$config['db_leasing'] 	= "easyloan_icleasing";     	# - leasing
	$config['db_easysale'] 	= "new_icmba";     				# - easysale
	$config['db_vehicle'] 	= "main_vehicle";     			# - mainVehicle
	$config['db_icservice'] = "dem_icservice";     			# - dem_icservice
}
 */
 
$config['db_base_name'] = $objDb->con['db_base_name']; 	# - crm
$config['db_emp'] 		= $objDb->con['db_emp']; 		# -	checktime
$config['db_maincus'] 	= $objDb->con['db_maincus'];   	# - maincus
$config['Cus']			= $objDb->con['Cus'];			# - old maincus
$config['db_organi'] 	= $objDb->con['db_organi'];   	# - ou
$config['db_leasing'] 	= $objDb->con['db_leasing']; 	# - leasing
$config['db_easysale'] 	= $objDb->con['db_easysale'];	# - easysale
$config['db_vehicle'] 	= $objDb->con['db_vehicle'];  	# - mainVehicle
$config['db_icservice'] = $objDb->con['db_service'];	# - dem_icservice 
$config['logHis'] 		= $objDb->con['db_logCRM'];		# - log crm z

# ---------------------------------
# ---------------------------------
# -- DEFINE BU MEMBER VARIABLE
# ---------------------------------
// $config['BU_EASYSALE'] 		= 'EASY SALE';
// $config['BU_EASYSERVICE'] 	= 'EASY SERVICE';
// $config['BU_EASYLEASING'] 	= 'EASY LEASING';

// $config['BU_LIST'] = array($config['BU_EASYSALE'],$config['BU_EASYSERVICE'],$config['BU_EASYLEASING']);

#-- fix database and table --#
$fix_db = array($config['db_base_name'],$config['db_maincus'],$config['db_leasing'],$config['db_easysale'],$config['db_vehicle'],$config['db_icservice']);

//$fix_tb = array("MainCusData","Sell","follow_CVIP","Booking","CusInt");  
$fix_tb = array();  
//fix ตารางที่จะใช้หา  pattern วันที่ เพราะถ้าเยอะไปแต่ไม่ได้ใช้จะหาลำบาก

$db_comment[$config['db_base_name']]['comment'] 	= "ฐานข้อมูล crm";
$db_comment[$config['db_maincus']]['comment']   	= "ฐานข้อมูลลูกค้า";
$db_comment[$config['db_vehicle']]['comment']   	= "ฐานข้อมูลรถ";
$db_comment[$config['db_leasing']]['comment']   	= "ฐานข้อมูลินเชื่อ";
$db_comment[$config['db_easysale']]['comment']   	= "ฐานข้อมูลขาย";
$db_comment[$config['db_icservice']]['comment']   	= "ฐานข้อมูลเซอร์วิส";

# ---------------------------------
# -- END DEFINE BU MEMBER VARIABLE
# ---------------------------------
# ---------------------------------

## new log History
// require_once($_SERVER['DOCUMENT_ROOT'].'/ERP_masterdata/function/nuy.class.php');
$logDb = new getLogDatabase($config['logHis'],$_SESSION['SESSION_ID_card']);
######################################

//ตั้งค่าตาราง ที่มีโอกาสเปลี่ยนฐานข้อมูลไปมาสูง    ***** TABLE *****
$table_config['working_company'] = "$config[db_organi].working_company";

if(!function_exists('Conn2DB')){
	function Conn2DB(){
		global $conn,$config;
		
		$conn = mysql_connect($config['ServerName'],$config['UserName'],$config['UserPassword'] );
		if (!$conn)die("ไม่สามารถติดต่อกับ ฐานข้อมูลได้ ".$config['ServerName'].$config['UserName'].$config['UserPassword'].mysql_error());
		if($config['db_emp']){
			mysql_select_db($config['db_emp'],$conn)or die("ไม่สามารถเลือกใช้งานฐานข้อมูลได้".mysql_error());
			@mysql_db_query($config['db_emp'], "SET NAMES UTF8");
		}
		if($config['db_maincus']){
			mysql_select_db($config['db_maincus'],$conn)or die("ไม่สามารถเลือกใช้งานฐานข้อมูลได้".mysql_error());
			@mysql_db_query($config['db_maincus'], "SET NAMES UTF8");
		}
		if($config['db_vehicle']){
			mysql_select_db($config['db_vehicle'],$conn)or die("ไม่สามารถเลือกใช้งานฐานข้อมูลได้      ".$config['db_vehicle'].mysql_error());
			@mysql_db_query($config['db_vehicle'], "SET NAMES UTF8");
		}
		if($config['db_easysale']){
			mysql_select_db($config['db_easysale'],$conn)or die("ไม่สามารถเลือกใช้งานฐานข้อมูลได้      ".$config['db_easysale'].mysql_error());
			@mysql_db_query($config['db_easysale'], "SET NAMES UTF8");
		}
		if($config['db_organi']){
			mysql_select_db($config['db_organi'],$conn)or die("ไม่สามารถเลือกใช้งานฐานข้อมูลได้      ".$config['db_organi'].mysql_error());
			@mysql_db_query($config['db_organi'], "SET NAMES UTF8");
		}
		if($config['db_base_name']){
			mysql_select_db($config['db_base_name'],$conn)or die("ไม่สามารถเลือกใช้งานฐานข้อมูลได้" );
			@mysql_db_query($config['db_base_name'], "SET NAMES UTF8");
		}
	}
}

if(!function_exists('CloseDB')){function CloseDB(){global $conn;@mysql_close( $conn );}}
?>