<?php


class dbManagement
{
	var $tablename;                      # table name
	var $dbname;                         # database name
	var $rows_per_page;                  # used in pagination
	var $pageno;                         # current page number
	var $lastpage;                       # highest page number
	var $fieldlist;                      # list of fields in this table
	var $data_array;                     # data from the database
	var $errors;                         # array of error messages
	var $order;                          # used order data <desc-asc>
	var $last_insert_id;                    # get last insert id

	function Default_Table (){
		$this->tablename       = 'default';
		$this->dbname          = 'default';
		$this->rows_per_page   = 10;

		$this->fieldlist = array('column1', 'column2', 'column3');
		$this->fieldlist['column1'] = array('pkey' => 'y');
	}  # constructor


	 

	function insertRecord ($fieldarray){
		 
		global $query;

		$fieldlist = $this->fieldlist;

		foreach ($fieldarray as $field => $fieldvalue) {
			if (!in_array($field, $fieldlist)) {           # check have array or not - if not in array return <true> else return <false>
				unset ($fieldarray[$field]);
			}
		}          # foreach
		 
		$query = "INSERT INTO $this->tablename SET ";
		foreach ($fieldarray as $item => $value) {
			$query .= "$item='$value', ";
		}    # foreach
		 
		$query = rtrim($query, ', ');

		mysql_query( "SET NAMES UTF8" ) ;
		$result = @mysql_query($query);
		$last_insert = mysql_insert_id();

		$this->last_insert_id = $last_insert;

		if (mysql_errno() <> 0) {
			if (mysql_errno() == 1062) {
				$this->errors[] = "A record already exists with this ID.";
			} else {
				trigger_error("SQL", E_USER_ERROR);
			}
		}


		// return "เพิ่มข้อมูลเรียบร้อยแล้ว !! ";
		 
	} # insertRecord
	 
	 
	 
	 
	 
	function getData ($where){

		$this->data_array = array();
		$pageno          = $this->pageno;
		$rows_per_page   = $this->rows_per_page;
		$this->numrows   = 0;
		$this->lastpage  = 0;
		$order           = $this->order;

		global $query;

		if (empty($where)) {
			$where_str = NULL;
		} else {
			$where_str = "WHERE $where";
		} # end if
	
		if(empty($order)){
			$order_str = NULL;
		}else{
			$order_str = "ORDER BY $order";
		}

		$query = "SELECT * FROM $this->tablename $where_str ";
		$result = mysql_query($query) or trigger_error("SQL", E_USER_ERROR);
		$query_data = mysql_fetch_row($result);
		$this->numrows = $query_data[0];
		$this->numrows;
		
		 
		if ($this->numrows <= 0) {
			$this->pageno = 0;
			return;
		} # end if
		
		
		if ($rows_per_page > 0) {
			$this->lastpage = ceil($this->numrows/$rows_per_page);
		} else {
			$this->lastpage = 1;
		}  # end if
		 
		 
		
		if ($pageno == '' OR $pageno <= '1') {
			$pageno = 1;
		} elseif ($pageno > $this->lastpage) {
			$pageno = $this->lastpage;
		}  #end if

		$this->pageno = $pageno;

		 
		if ($rows_per_page > 0) {
			 
			$limit_str = 'LIMIT ' .($pageno - 1) * $rows_per_page .',' .$rows_per_page;

		} else {
			 
			 $limit_str = 'LIMIT 150 ' ;

		}  # end if
		
		//echo "<br>";
		$query = "SELECT * FROM $this->tablename $where_str $order_str $limit_str ";
		mysql_query( "SET NAMES UTF8" );
		$result = mysql_query($query) or trigger_error("SQL", E_USER_ERROR);

		while ($row = mysql_fetch_assoc($result)) {
			 
			$this->data_array[] = $row;
			 
		} # end while


		mysql_free_result($result);
		 
		return $this->data_array;

	} # end getData
	 
	 
	 
	 
	function deleteRecord ($fieldarray){
		global  $query;
		 
		 

		$fieldlist = $this->fieldlist;
		$where  = NULL;

		foreach ($fieldarray as $item => $value){

			if (isset($fieldlist[$item]['pkey'])) {
				$where .= "$item='$value' AND ";
			} # end if
			 
		} # end foreach

		$where  = rtrim($where, ' AND ');


		$query = "DELETE FROM $this->tablename WHERE $where";
		$result = mysql_query($query) or trigger_error("SQL", E_USER_ERROR);

		return "ลบข้อมูลเรียบร้อยแล้ว !! ";

	} # end deleteRecord



	function updateRecord ($fieldarray){
		 
		global $query;

		$fieldlist = $this->fieldlist;

		foreach ($fieldarray as $field => $fieldvalue) {
			if (!in_array($field, $fieldlist)) {      # check have array or not - if not in array return <true> else return <false>
				unset ($fieldarray[$field]);
			}  # end if
		} # end foreach

		 
		$where  = NULL;
		$update = NULL;
		 
		foreach ($fieldarray as $item => $value) {
			if (isset($fieldlist[$item]['pkey'])) {
				$where .= "$item='$value' AND ";
			} else {
				$update .= "$item='$value', ";
			} # end if
		} # end foreach

		 
		$where  = rtrim($where, ' AND ');
		$update = rtrim($update, ', ');

		 
		$query = "UPDATE $this->tablename SET $update WHERE $where";
		mysql_query( "SET NAMES UTF8" );
		$result = mysql_query($query) or trigger_error("SQL", E_USER_ERROR);
		 
		return "แก้ไขข้อมูลเรียบร้อยแล้ว !! ";
		 
	} # end updateRecord





	function selectMax($field){
		global $query;

		$query = "SELECT MAX($field) FROM $this->tablename";
		mysql_query( "SET NAMES UTF8" );
		$result = mysql_query($query) or die(mysql_error());
		 
		while ($row = mysql_fetch_assoc($result)) {
			 
			$this->data_array[] = $row;
			 
		} # end while


		mysql_free_result($result);
		 
		return $this->data_array;

	}    #end selectMax






}   # end class

?>