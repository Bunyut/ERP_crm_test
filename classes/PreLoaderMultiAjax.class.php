<?php 

	class PreLoaderMultiAjax 
	{

		#	param
		private $config;//	obj config db
		private $progress_width;
		private $txt;
		public 	$totalLoop;
		private $rWidth;
		private $counter;
		public $msgContent;
		#--

		public function __construct($totalLoop = array()){


			ini_set('output_buffering', 'off');
			// Turn off PHP output compression
			ini_set('zlib.output_compression', false);      
			//Flush (send) the output buffer and turn off output buffering
			while (@ob_end_flush());       
			// Implicitly flush the buffer(s)
			ini_set('implicit_flush', false);
			ob_implicit_flush(false);

			$this->config = $config;
			$this->progress_width 	= 0;
			$this->txt 				= '';
			$this->totalLoop 		= $totalLoop;#total loop		
			$this->rWidth			= 100;#fix
			

			foreach ($this->totalLoop as $no => $value) 
			{
				$this->counter[$no] = 0;

			}

			ob_start();
			

		}


		public function setLoopMax($no,$max)
		{
			$this->counter[$no] = 0;
			$this->totalLoop[$no] = $max;

		}

		public function eachTell($fixCount = '',$tagNo)
		{

				$this->msgContent .= ob_get_contents();

				
				if($fixCount  != ''){
					$this->counter[$tagNo]  = $fixCount;
				}else{
					$this->counter[$tagNo]++;
				}

				//echo "(".$this->counter[$tagNo]." * ".$this->rWidth.") / ".$this->totalLoop[$tagNo]."<br> \n";

				$progress_width = ($this->counter[$tagNo] * $this->rWidth) / $this->totalLoop[$tagNo];
				$progress_width = number_format($progress_width, 0, '.', '');

				$arOut = array();
				$arOut['percen'] 	= $progress_width;
				$arOut['tagNo'] 	= 'prgIner_'.$tagNo;
				$arOut['no'] 		= $tagNo;

			
				ob_end_clean();
				ob_end_flush();
				#-- before

				ob_start();
				ob_end_clean();  
				echo json_encode($arOut); 
				flush();
				ob_end_flush(); 

				#-- after
				ob_start();
				

		}

	

		public function msg($txt)
		{

			
			ob_start();
			echo $txt;
			$this->msgContent .= ob_get_contents();
			ob_end_clean();
		}
		public function setTitle($msg)
		{

			$this->msgContent .= ob_get_contents();

			$arOut = array();
			$arOut['percen'] 	= '';
			$arOut['titleChange'] 	= $msg;

		
			ob_end_clean();
			ob_end_flush();
			#-- before

			ob_start();
			ob_end_clean();  
			echo json_encode($arOut); 
			flush();
			ob_end_flush(); 

			#-- after
			ob_start();
		}

		public function endPreLoad()
		{


			$this->msgContent .= ob_get_contents();

			#	finish
			ob_end_clean();
			ob_end_flush();
			#--
			// sleep(1);
			usleep(500000);
			$arOut = array();
			$arOut['percen'] 	= 100;
			$arOut['msg'] 		= $this->msgContent;
			$arOut['comp'] 	= 'yes';
			echo json_encode($arOut); 
		}

	}#end class


?>