<?php

//error_reporting( E_ALL & ~E_NOTICE );
require_once(dirname(dirname(dirname(__FILE__))).'/ERP_masterdata/inc/main_function.php' );	
require_once(dirname(dirname(__FILE__)).'/inc/TplParser.php');

class ServiceHistory {
    #	param
    private $config;//
    private $tpl;
    public $debug;
    private $html_f;
   

    public function __construct($config){
        
        $this->config 	= $config;

        //echo '<pre>';print_r($this->config);echo '</pre>';
        
        $this->tpl  	= new TplParser();
        $this->tpl->directory = "templates/";
        $this->debug = false;
        $this->html_f = "ic_followcus_show.html";
   
    }

    public function showCars($cusNo){
        global $data;

        $resOut = array();
        $resOut['status'] = true;
        $resOut['html'] = '';
        $data['list'] = ''; 

        $arrBrand = array();
        $sql = "SELECT * FROM ".$this->config['db_icservice'].".abrand WHERE status != 99 ";
        $rt_abrand = mysql_query($sql) or die ("error sql:".$sql." FILE : ".__FILE__." LINE : ".__LINE__." ->".mysql_error()); 
        while($r_abrand = mysql_fetch_assoc($rt_abrand)){

            $arrBrand[$r_abrand['id']] = $r_abrand['name'];
        }
        


        $sql = "SELECT * FROM ".$this->config['db_icservice'].".ahistory WHERE cusNo_user = '".$cusNo."' AND status != 99 ";
        $rt_ahis = mysql_query($sql) or die ("error sql:".$sql." FILE : ".__FILE__." LINE : ".__LINE__." ->".mysql_error()); 
        
        if(mysql_num_rows($rt_ahis) > 0){

            while($r_ahis = mysql_fetch_assoc($rt_ahis)){

                $data['ahistoryId'] = $r_ahis['id'];
                $data['brand'] = $arrBrand[$r_ahis['abrand_id']];
                $data['engin_id'] =  $r_ahis['engin_id'];
                $data['chassis'] =  $r_ahis['chassis'];
                $data['register'] =  $r_ahis['register'];
                $data[''] =  $r_ahis[''];
                $data[''] =  $r_ahis[''];

                $data['list'] .=  $this->tpl->tbHtml($this->html_f, 'history_car_list');
            }

        }else{
            $data['list'] = '<td colspan="5">ไม่มีข้อมูล</td>';
        }
       

        $resOut['html'] =  $this->tpl->tbHtml($this->html_f, 'history_car');

        return $resOut;

    }

    public function getHistory($ahisId){
        global $blockData;

        $resOut = array();
        $resOut['status'] = true;
        $resOut['html'] = '';

        $resComs = $this->getWorkingCompanys();

        $sql = "SELECT * FROM ".$this->config['db_icservice'].".cvhc_main WHERE ahistory_id = '".$ahisId."' AND status IN(26) ORDER BY id DESC LIMIT 3";
        $rt_cvhc = mysql_query($sql) or die ("error sql:".$sql." FILE : ".__FILE__." LINE : ".__LINE__." ->".mysql_error()); 
        
        $repairList = 0;
		if(mysql_num_rows($rt_cvhc) > 0){

            while($r_cvhc = mysql_fetch_assoc($rt_cvhc))
			{
                $blockData['cvhc_main'] = $r_cvhc['id'];
                $blockData['comp_name']  = $resComs[$r_cvhc['brach_id']];

                if(isset($r_cvhc['guard_time']) && $r_cvhc['guard_time'] != ''){

                    $blockData['date_in'] = date('d/m/Y',$r_cvhc['guard_time']);
                    //$blockData['date_in'] .= (date('Y',$r_cvhc['guard_time']) + 543); 

                }
              
                $ytaxInf = $this->getPaymentDay($r_cvhc['id']);
                $blockData['date_pay'] = $ytaxInf['date_time'];
                
                $blockData['queue'] = $r_cvhc['queue'];
                $blockData['technician'] = $this->gettechnician($r_cvhc['id']);
                $blockData['sa_name'] = $this->getEmpdata($r_cvhc['sa_id']);
				
				$r_cvhc['mile'] = ($r_cvhc['mile'] != '')? number_format($r_cvhc['mile'],0) : 0;
                $blockData['mile_in'] = $r_cvhc['mile'];
				
				$r_cvhc['mileout'] = ($r_cvhc['mileout'] != '')? number_format($r_cvhc['mileout'],0) : 0;
                $blockData['mile_out'] = $r_cvhc['mileout'];
				
                $drepair = $this->getDrepairInform($r_cvhc['id']);

                $blockData['comment'] = $drepair['Commentsa'];
                $blockData['repair_inf'] = $this->tpl->tbHtml($this->html_f, 'repair_inf');;

                $blockData['repair_list'] = $this->getRepairListHtml($r_cvhc['id']);


                $resOut['html'] .= $this->tpl->tbHtml($this->html_f, 'history_repairs');
				
				$repairList++;
            }
			
			if($repairList < 3)
			{
				$limit = (3 - $repairList);
                $resWork = $this->getHistoryOld($ahisId, $resComs, $limit);
                
                $resOut['html'] .= $resWork['html'];
			}

        }else{

            $limit = (3 - $repairList);
            $resWork = $this->getHistoryOld($ahisId, $resComs, $limit);
            $resOut['html'] .= $resWork['html'];
			
			if($resOut['html'] == 'ไม่มีข้อมูลการซ่อม')
			{
				$resOut['html'] = 'ไม่มีข้อมูลการซ่อม';
			}
        }
       
    
        return $resOut;
    }

    public function getPaymentDay($cvhcId){

        $sql = "SELECT day FROM ".$this->config['db_icservice'].".ytax WHERE ref_id = ".$cvhcId." AND ref_status IN (1,3) AND ref_ytax = ''  LIMIT 1";
        $rt_ytax = mysql_query($sql) or die ("error sql:".$sql." FILE : ".__FILE__." LINE : ".__LINE__." ->".mysql_error()); 
        $r_ytax = mysql_fetch_assoc($rt_ytax);

        $r_ytax['date_time'] = date('d/m/Y',$r_ytax['day']);

        return $r_ytax;

    }

    public function getDrepairInform($cvhcId){

        $sql = "SELECT Commentsa FROM ".$this->config['db_icservice'].".drepair_inform WHERE cvhc_main_id = '".$cvhcId."' LIMIT 1";
        $rt_drepair = mysql_query($sql) or die ("error sql:".$sql." FILE : ".__FILE__." LINE : ".__LINE__." ->".mysql_error()); 
        $r_drepair = mysql_fetch_assoc($rt_drepair);

        return $r_drepair;

    }

    public function getWorkingCompanys(){

        $resOut;

        $sql = "SELECT id,name  FROM ".$this->config['db_organi'].".working_company WHERE status != 99 ";
        $re = mysql_query($sql) or die($sql."<br>".__FILE__."<br>".__LINE__);
        while($wk = mysql_fetch_assoc($re)){

            $resOut[$wk['id']] = $wk['name'];
        }

        return $resOut;

    }

    public function gettechnician($cvhc){

        $keep = array();

        $sql 	= "SELECT * FROM ".$this->config['db_icservice'].".erepair_send WHERE cvhc_main_id='".$cvhc."' AND status != '99'";
        $re_ers = mysql_query($sql) or die($sql."<br>".__FILE__."<br>".__LINE__);
        $rs_ers = mysql_fetch_assoc($re_ers);

        $sql 	= "SELECT * FROM ".$this->config['db_icservice'].".erepair_sent_list WHERE cvhc_main_id='".$cvhc."' AND status_active ='1' ";
        $re_ersl = mysql_query($sql) or die($sql."<br>".__FILE__."<br>".__LINE__);
        $rs_ersl = mysql_fetch_assoc($re_ersl);

        if($rs_ers['erepair_count'] > 0){

            $sql = "SELECT technician_name FROM ".$this->config['db_icservice'].". erepair_send_all_mechanic WHERE erepair_send_list_id='".$rs_ersl['id']."' AND status != '99' AND status_active ='1' ";
            $rt_name_mach = mysql_query($sql);

          
            while($r_name_mach = mysql_fetch_array($rt_name_mach)){
            
                $keep[] =  $r_name_mach['technician_name'];
            
            }
          


        }else{

            $sql = "SELECT technician_name FROM ".$this->config['db_icservice'].".erepair_send_mechanic WHERE cvhc_main_id='".$cvhc."' AND status != '99'";
            $rt_name_mach = mysql_query($sql);

           
            while($r_name_mach = mysql_fetch_array($rt_name_mach)){
            
                $keep[] =  $r_name_mach['technician_name'];
            
            }

        }

        return implode(',',$keep);
    }

    public function getEmpdata($id_card){

        $sql = "SELECT CONCAT(Name,' ',Surname,'(',Nickname,')') AS name FROM ".$this->config['db_emp'].".emp_data WHERE ID_card='".$id_card."' LIMIT 1";
        $rt_emp = mysql_query($sql) or die ("error sql:".$sql." FILE : ".__FILE__." LINE : ".__LINE__." ->".mysql_error()); 
        
        $r_emp = mysql_fetch_assoc($rt_emp);


        return $r_emp['name'];
    }

    public function getRepairListHtml($cvhc){
        global $priceInf;

        $html = '';
        $priceInf = array();

        $sql = "SELECT id,sumwage,sumparts,sumall FROM ".$this->config['db_icservice'].".ytax WHERE ref_id = '".$cvhc."' AND ref_status IN(1,3) LIMIT 1";
        //echo $sql;
        $rt_ytax = mysql_query($sql) or die ("error sql:".$sql." FILE : ".__FILE__." LINE : ".__LINE__." ->".mysql_error()); 
        
        $ytax = mysql_fetch_assoc($rt_ytax);
       

        $priceInf['txt_head'] = 'รวมเงินค่าแรง '.$ytax['sumwage'].' บาท  รวมเงินค่าอะไหล่ '.$ytax['sumparts'].' บาท  เป็นเงิน '.$ytax['sumall'].' บาท';

     
        $sql = "SELECT pw_code,pw_name,unit FROM ".$this->config['db_icservice'].".ytax_list WHERE ytax_id = '".$ytax['id']."' AND status != 99 ";
        $rt_ytaxList = mysql_query($sql) or die ("error sql:".$sql." FILE : ".__FILE__." LINE : ".__LINE__." ->".mysql_error()); 
        
        while($r_ytaxList = mysql_fetch_assoc($rt_ytaxList)){

            $priceInf['pw_code'] = $r_ytaxList['pw_code'];
            $priceInf['pw_name'] = $r_ytaxList['pw_name'];
            $priceInf['unit'] = $r_ytaxList['unit'];

            $priceInf['lists'] .= $this->tpl->tbHtml($this->html_f, 'repair_list_tr');
        }

        $sql = "SELECT pw_code,pw_name,unit FROM ".$this->config['db_icservice'].".ytax_list_parts WHERE ytax_id = '".$ytax['id']."' AND status != 99 ";
        $rt_ytaxList = mysql_query($sql) or die ("error sql:".$sql." FILE : ".__FILE__." LINE : ".__LINE__." ->".mysql_error()); 
        
        while($r_ytaxList = mysql_fetch_assoc($rt_ytaxList)){

            $priceInf['pw_code'] = $r_ytaxList['pw_code'];
            $priceInf['pw_name'] = $r_ytaxList['pw_name'];
            $priceInf['unit'] = $r_ytaxList['unit'];

            $priceInf['lists'] .= $this->tpl->tbHtml($this->html_f, 'repair_list_tr');
        }


        $html = $this->tpl->tbHtml($this->html_f, 'repair_list');

        return $html;

    }

    public function getRepairListWorkOldHtml($headId){
        global $priceInf;

        $html = '';
        $priceInf = array();
        $sumPart = 0;
        $sumWage = 0;

        $sql = "SELECT CODE,TITLE,QTY_C,SALE 
					FROM ".$this->config['db_icservice'].".work3_wage 
					WHERE id_work3_head = '".$headId."' ";
        $rt_ytaxList = mysql_query($sql) 
			or die ("error sql:".$sql." FILE : ".__FILE__." LINE : ".__LINE__." ->".mysql_error()); 
        
        while($r_ytaxList = mysql_fetch_assoc($rt_ytaxList))
		{
            $priceInf['pw_code'] = $r_ytaxList['CODE'];
            $priceInf['pw_name'] = $r_ytaxList['TITLE'];
            $priceInf['unit'] = $r_ytaxList['QTY_C'];

            $sumPart += (float)$r_ytaxList['SALE'];

            $priceInf['lists'] .= $this->tpl->tbHtml($this->html_f, 'repair_list_tr');
        }

        $sql = "SELECT CODE_name,TITLE,QTY_C,SALE 
					FROM ".$this->config['db_icservice'].".work3_parts 
					WHERE id_work3_head = '".$headId."'  ";
        $rt_ytaxList = mysql_query($sql) 
			or die ("error sql:".$sql." FILE : ".__FILE__." LINE : ".__LINE__." ->".mysql_error()); 
        
        while($r_ytaxList = mysql_fetch_assoc($rt_ytaxList))
		{
            $priceInf['pw_code'] = $r_ytaxList['CODE_name'];
            $priceInf['pw_name'] = $r_ytaxList['TITLE'];
            $priceInf['unit'] = $r_ytaxList['QTY_C'];

            $sumWage += (float)$r_ytaxList['SALE'];

            $priceInf['lists'] .= $this->tpl->tbHtml($this->html_f, 'repair_list_tr');
        }

        $priceInf['txt_head'] = 'รวมเงินค่าแรง '.number_format($sumWage,2).' บาท  รวมเงินค่าอะไหล่ '.number_format($sumPart,2).' บาท  เป็นเงิน '.number_format(($sumWage+$sumPart),2).' บาท';


        $html = $this->tpl->tbHtml($this->html_f, 'repair_list');

        return $html;

    }
	
	public function getHistoryOld($ahisId, $resComs, $limit){
        global $blockData;

        $resOut = array();
        $resOut['status'] = true;
        $resOut['html'] = '';
				
        $sql = "SELECT id, '' AS brach_id, DAT AS guard_time, '' AS queue, MILE AS mile, MILE2 AS mileout, NAMEBG, EXP
					FROM ".$this->config['db_icservice'].".work3_head 
					WHERE ahistory_id = '".$ahisId."' 
					ORDER BY DAT DESC LIMIT ".$limit;
						
        $rt_cvhc = mysql_query($sql) or die ("error sql:".$sql." FILE : ".__FILE__." LINE : ".__LINE__." ->".mysql_error()); 
       
        if(mysql_num_rows($rt_cvhc) > 0){
			
            while($r_cvhc = mysql_fetch_assoc($rt_cvhc))
			{
                $blockData['cvhc_main'] = $r_cvhc['id'];
                $blockData['comp_name']  = $resComs[$r_cvhc['brach_id']];
                $blockData['date_in'] = $r_cvhc['guard_time'];
                $blockData['queue'] = $r_cvhc['queue'];
                $blockData['technician'] = $r_cvhc['EXP'];
                $blockData['sa_name'] = $r_cvhc['NAMEBG'];
				
				$r_cvhc['mile'] = ($r_cvhc['mile'] != '')? number_format($r_cvhc['mile'],2) : 0;
                $blockData['mile_in'] = $r_cvhc['mile'];
				
				$r_cvhc['mileout'] = ($r_cvhc['mileout'] != '')? number_format($r_cvhc['mileout'],2) : 0;
                $blockData['mile_out'] = number_format($r_cvhc['mileout'],2);
				
                $blockData['repair_inf'] = $this->tpl->tbHtml($this->html_f, 'repair_inf');;

                $blockData['repair_list'] = $this->getRepairListWorkOldHtml($r_cvhc['id']);

                $resOut['html'] .= $this->tpl->tbHtml($this->html_f, 'history_repairs');
            }
		
        }else{
			
            $resOut['html'] = 'ไม่มีข้อมูลการซ่อม';
        }
       
    
        return $resOut;
    }

}

?>