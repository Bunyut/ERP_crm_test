<?php
require_once dirname(dirname(dirname(__FILE__))).'/ERP_masterdata/inc/main_function.php';

class pLoadmore extends getLogDatabase 
{
    
    public $start;
    public $limit;
    private $count;
    private $contents;
    
    public function __construct() {
        ob_start();
    }
    
    public function queryAndLoadmore($sql, $msg=null) {
        if (isset($_REQUEST['start']) && isset($_REQUEST['limit'])) {
            $this->count = 0;
            $this->start = intval($_REQUEST['start']);
            $this->limit = intval($_REQUEST['limit']);
            if ($this->count == 0) {
                $que = $this->queryAndLogSQL($sql, $msg);
                $this->total = mysql_num_rows($que);
            }
            $sql.= " LIMIT " . $this->start . ", " . $this->limit;
            $que = $this->queryAndLogSQL($sql, $msg);
            return $que;
        }else {
            $que = $this->queryAndLogSQL($sql, $msg);
            return $que;
        }
    }
    
    public function eachLoadmore() {
        $this->start++;
        $this->count++;
        
        $this->contents.= ob_get_clean();
        ob_end_flush();
        
        ob_start();
        $percen = round(($this->count/$this->limit)*100, 2);
        echo json_encode(array('percen' => $percen));
        ob_flush();
        flush();
        ob_end_flush();
        
        ob_start();
    }
    
    public function endLoadmore() {
        
        $this->contents.= ob_get_clean();
        ob_end_flush();
        
        echo json_encode(array('total' => $this->total, 'current' => $this->start, 'html' => $this->contents));
    }
}
?>