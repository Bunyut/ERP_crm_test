<?php

class Report_follow_customers {

	public function __construct($thisPage)
	{
		global $logDb, $config, $tpl;
		$this->config 		= $config;
		$this->thisPage 	= $thisPage;
		$this->logDb		= $logDb;
		$this->tpl 			= $tpl;		
		//echo "<pre>".print_r($this->config)."</pre>";
	}//end __construct

	public function get_slCompany(){		
		$sql_b    = " SELECT id, name FROM ".$this->config['db_organi'].".working_company WHERE status = '1' AND name != '' ";
		$result_b = $this->logDb->queryAndLogSQL( $sql_b, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$slCompany = '<option value="" class="textCenter">-- เลือกบริษัท --</option>';
		while($fe_b = mysql_fetch_assoc($result_b)){
			$slCompany .= '<option class="textCenter" value="'.$fe_b['id'].'"> '.$fe_b['name'].' </option>';
		}
		return $slCompany;
	}//end function

	public function get_NameCompany($idWorking){		
		$sql_b    = " SELECT id, name FROM ".$this->config['db_organi'].".working_company WHERE status = '1' AND name != '' AND id=".$idWorking;
		$result_b = $this->logDb->queryAndLogSQL( $sql_b, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$fe_b = mysql_fetch_assoc($result_b);
		$arr_Working = $fe_b;
		return $arr_Working;
	}//end function

	public function get_slTeam($company, $idSection = ""){
		$where = "";
		if($idSection != ""){ $where .= " AND id IN (".$idSection.")"; }
		$slTeam = '<option value="" class="textCenter">-- เลือกทีม --</option>';

		$sql_department = "SELECT * FROM ".$this->config['db_organi'].".department WHERE company  ='".$company."' AND status !='99' ";
		$res_department = $this->logDb->queryAndLogSQL( $sql_department, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while($arr_department = mysql_fetch_assoc($res_department)){
			$sql_section	= "SELECT * FROM ".$this->config['db_organi'].".section WHERE  department='".$arr_department['id']."' AND status !='99' $where";
			$res_section 	= $this->logDb->queryAndLogSQL( $sql_section, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			 while($arr_section = mysql_fetch_assoc($res_section)){
			 	 $slTeam .='<option '.$selected_event.' value="'.$arr_section['id'] .'" >'.$arr_section['name'] .'</option>';
			 }
		}
		return $slTeam;	
	}//end function


	public function get_slEven(){
		global $config,$logDb;
		$sql_even = "SELECT *  FROM $config[db_base_name].follow_main_event WHERE `status` != '99'";
		$res_even = $logDb->queryAndLogSQL( $sql_even, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while($arr_even = mysql_fetch_assoc($res_even)){
			$slEven .='<option value="'.$arr_even['id'] .'" >'.$arr_even['event_title'] .'</option>';
		}
		
		return $slEven;
	}//end function

	public function get_Month(){
		$strMonth = "";
		$months = array('','มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม');
			for($i = 1; $i <= 12; $i++){
				if($i<10){
					$strMonth .= '<option value="0'.$i.'">'.$months[$i].'</option>';
				}else{ $strMonth .= '<option value="'.$i.'">'.$months[$i].'</option>';}				
			}
		return $strMonth;
	}//end function

	public function get_Year(){
		$strYear = "";
		for($i = 2540; $i <= 2570; $i++){
			$strYear .= '<option value="'.($i - 543).'">'.$i.'</option>';
		}
		return $strYear;
	}//end function

	public function getTeam_OU($idWorking = "", $idSection = "", $id_card = "", $idPosition = ""){		
		$arr_ou = array();
		$where = "";
		if($idWorking != ""){ $where .= " AND PST.WorkCompany 	= ".$idWorking; }
		if($idSection != ""){ $where .= " AND PST.Section 		= ".$idSection; }
		if($id_card   != ""){ $where .= " AND EMP.ID_Card in(".$id_card.")"; 	}
		if($idPosition!= ""){ 
			$idPosition = str_replace( ',', "','", $idPosition );
			$where .= " AND PST.PositionCode in('".$idPosition."')"; 	
		}
		$sql_working  = "SELECT  
							 PST.WorkCompany
							,WCN.`name` AS WorkName
							,PST.Department
							,DPM.`name` AS DeparmentName
							,PST.Section
							,ST.`name` AS SectionName
							,PST.id AS Position
							,PST.PositionCode
							,PST.`Name` AS PositionName
							,EMP.ID_Card
							,CONCAT(EMP.Be, EMP.`Name`,'  ', EMP.Surname,' (', EMP.Nickname, ')') AS EMP_Fullname
							,EMP.Nickname
						FROM ".$this->config['db_organi'].".position AS PST
						LEFT JOIN ".$this->config['db_organi'].".working_company AS WCN ON PST.WorkCompany = WCN.id AND WCN.`status` = 1 AND WCN.name != ''
						LEFT JOIN ".$this->config['db_organi'].".department AS DPM ON PST.Department = DPM.id AND DPM.`status` = 1 AND DPM.name != ''
						LEFT JOIN ".$this->config['db_organi'].".section AS ST ON PST.Section = ST.id AND ST.`status` =1  AND ST.name != ''
						LEFT JOIN ".$this->config['db_organi'].".relation_position AS CT ON PST.id = CT.position_id AND CT.`status` = 1
						LEFT JOIN ".$this->config['db_emp'].".emp_data AS EMP ON CT.id_card = EMP.ID_Card
						WHERE PST.`Status` = 1	$where AND EMP.`Name` != ''
						ORDER BY PST.WorkCompany , PST.Department,  PST.Section";
		$res_working = $this->logDb->queryAndLogSQL( $sql_working, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while($arr_data = mysql_fetch_assoc($res_working)){
			$arr_ou[] = $arr_data;
		}
		return $arr_ou;
	}//end function

	public function getDataFollowCus($idWorking = "", $idDepartment = "", $idSection = "", $PositionCode = "" , $idEven = "", $StartDate = "" , $StopDate = "", $ID_Card = ""){
		$arr_data = array();
		$where = "";
		if($idWorking 	 != ""){ $where .= " AND r_working_company 	= ".$idWorking; }
		if($idDepartment != ""){ $where .= " AND r_department 		= ".$idDepartment; }
		if($idSection 	 != ""){ $where .= " AND r_section 			= ".$idSection; }
		if($PositionCode != ""){ $where .= " AND r_position 		= ".$PositionCode; }
		if($idEven 		 != ""){ $where .= " AND event_id_ref 		= ".$idEven; }
		if($ID_Card 	 != ""){ $where .= " AND r_id_card IN (".$ID_Card.")"; }
		if($StartDate 	 != "" && $StopDate 	 != ""){
			$where .= " AND (
						  		 ( start_date BETWEEN '".$this->ConvertDateToYearMonthDate($StartDate)."'  AND '".$this->ConvertDateToYearMonthDate($StopDate)."' )
							 OR  ( stop_date  BETWEEN '".$this->ConvertDateToYearMonthDate($StartDate)."'  AND '".$this->ConvertDateToYearMonthDate($StopDate)."' )
							 OR  (
									( '".$this->ConvertDateToYearMonthDate($StartDate)."'  BETWEEN  start_date AND stop_date ) AND
									( '".$this->ConvertDateToYearMonthDate($StopDate)."'   BETWEEN  start_date AND stop_date )
								 )
							)";
		}

		$sql_Follow = "	
			SELECT
				id,	cus_no,	event_id_ref, emp_id_card, emp_posCode, r_working_company, process_date,
				r_department, r_section, r_position, r_id_card,	follow_repeat_type,	follow_contact_status,
				follow_convenient_status, follow_prospect_status, follow_contact_reason, follow_status_remark,
				cancel_type, remark, `status`
			FROM
				".$this->config['db_base_name'].".follow_customer
			WHERE 1 = 1 $where				

			UNION

			SELECT
				id,	cus_no,	event_id_ref, emp_id_card, emp_posCode, r_working_company, process_date,
				r_department, r_section, r_position, r_id_card,	follow_repeat_type,	follow_contact_status,
				follow_convenient_status, follow_prospect_status, follow_contact_reason, follow_status_remark,
				cancel_type, remark, `status`
			FROM
				".$this->config['db_base_name'].".follow_customer_cancel
			WHERE 1 = 1 $where";
		$res_Follow = $this->logDb->queryAndLogSQL( $sql_Follow, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while($arr_Follow = mysql_fetch_assoc($res_Follow)){
			$arr_data[] = $arr_Follow;
		}

		return $arr_data;
	}//end function

	public function getColunmCustomer($arr_Follow){ //arr_Follow ช้อมูลจาก table follow_customer  ลูกค้า (ราย)
		$arrColumCustomer 	= array();
		$arr_data 			= array();

		for ($i=0; $i < count($arr_Follow) ; $i++) { 
			if($arr_Follow[$i]['r_working_company'] != 0){ // ลูกค้า (ราย) บริษัท
				$arr_data[ $arr_Follow[$i]['r_working_company'] ] [0] [0] [0] [0] [ $arr_Follow[$i]['cus_no'] ] += 1;
			}

			if($arr_Follow[$i]['r_department'] != 0){ // ลูกค้า (ราย) แผนก
				$arr_data[ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [0] [0] [0] [ $arr_Follow[$i]['cus_no'] ] += 1;
			}

			if($arr_Follow[$i]['r_section'] != 0){ // ลูกค้า (ราย) ฝ่าย
				$arr_data[ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [0] [0] [ $arr_Follow[$i]['cus_no'] ] += 1;
			}

			if($arr_Follow[$i]['r_position'] != 0){ // ลูกค้า (ราย) ตำแหน่ง
				$arr_data[ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [0] [ $arr_Follow[$i]['cus_no'] ] += 1;
			}

			if($arr_Follow[$i]['r_id_card'] != ""){ // ลูกค้า (ราย) พนักงาน
				$arr_data[ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [ $arr_Follow[$i]['r_id_card'] ]  [ $arr_Follow[$i]['cus_no'] ] += 1;
			}
		}

		foreach ($arr_data as $k_W => $Work) {
			foreach ($Work as $k_D => $Deparment) {
				foreach ($Deparment as $k_S => $Section) {
					foreach ($Section as $k_P => $Position) {
						foreach ($Position as $k_E => $Employee) {
							$arrColumCustomer[$k_W][$k_D][$k_S][$k_P][$k_E] = count($Employee);
						}	
					}
				}
			}
		}

		return $arrColumCustomer;
	}

	public function getColunmEven($arr_Follow){//arr_Follow ช้อมูลจาก table follow_customer  กิจกรรม
		$arrColumEven 	= array();
		$arr_data 		= array();
		//echo " arr_Follow <pre>";print_r($arr_Follow)."</pre>"; exit();
		for ($i=0; $i < count($arr_Follow) ; $i++) { 
			if($arr_Follow[$i]['r_working_company'] != 0 && $arr_Follow[$i]['r_id_card'] == 0){ // ลูกค้า (ราย) บริษัท
				$arr_data['ALL'][ $arr_Follow[$i]['r_working_company'] ] [0] [0] [0] [0] += 1;//ส่วนร่วม
			}else if($arr_Follow[$i]['r_working_company'] != 0 && $arr_Follow[$i]['r_id_card'] != 0){
				$arr_data['Private'][ $arr_Follow[$i]['r_working_company'] ] [0] [0] [0] [0] += 1;//ส่วนตัว
			}

			if($arr_Follow[$i]['r_department'] != 0 && $arr_Follow[$i]['r_id_card'] == 0){ // ลูกค้า (ราย) แผนก
				$arr_data['ALL'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [0] [0] [0] += 1;//ส่วนร่วม
			}else if($arr_Follow[$i]['r_department'] != 0 && $arr_Follow[$i]['r_id_card'] != 0){
				$arr_data['Private'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [0] [0] [0] += 1;//ส่วนตัว
			}

			if($arr_Follow[$i]['r_section'] != 0 && $arr_Follow[$i]['r_id_card'] == 0){ // ลูกค้า (ราย) ฝ่าย
				$arr_data['ALL'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [0] [0] += 1;//ส่วนร่วม
			}else if($arr_Follow[$i]['r_section'] != 0 && $arr_Follow[$i]['r_id_card'] != 0){
				$arr_data['Private'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [0] [0] += 1;//ส่วนตัว
			}

			if($arr_Follow[$i]['r_position'] != 0 && $arr_Follow[$i]['r_id_card'] == 0 ){ // ลูกค้า (ราย) ตำแหน่ง
				$arr_data['ALL'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [0] += 1;
			}else if($arr_Follow[$i]['r_position'] != 0 && $arr_Follow[$i]['r_id_card'] != 0){
				$arr_data['Private'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [0] += 1;//ส่วนตัว
			}

			if($arr_Follow[$i]['r_id_card'] == ""){ // ลูกค้า (ราย) พนักงาน
				$arr_data['ALL'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [ $arr_Follow[$i]['r_id_card'] ] += 1;
			}else if($arr_Follow[$i]['r_id_card'] != ""){
				$arr_data['Private'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [ $arr_Follow[$i]['r_id_card'] ] += 1;//ส่วนตัว
			}
		}
		if (count($arr_data['ALL']) > 0) {
			foreach ($arr_data['ALL'] as $k_W => $Work) {
				foreach ($Work as $k_D => $Deparment) {
					foreach ($Deparment as $k_S => $Section) {
						foreach ($Section as $k_P => $Position) {
							foreach ($Position as $k_E => $Employee) {
								$arrColumEven['ALL'][$k_W][$k_D][$k_S][$k_P][$k_E] = $Employee;
							}	
						}
					}
				}
			}
		}

		if (count($arr_data['Private']) > 0) {
			foreach ($arr_data['Private'] as $k_W => $Work) {
				foreach ($Work as $k_D => $Deparment) {
					foreach ($Deparment as $k_S => $Section) {
						foreach ($Section as $k_P => $Position) {
							foreach ($Position as $k_E => $Employee) {
								$arrColumEven['Private'][$k_W][$k_D][$k_S][$k_P][$k_E] = $Employee;
							}	
						}
					}
				}
			}
		}
		return $arrColumEven;
	}


	public function getColunmWaitEven($arr_Follow){ //arr_Follow ช้อมูลจาก table follow_customer  รอทำกิจกรรม
		$arrColumWaitEven 	= array();
		$arr_data 			= array();
		//echo " arr_Follow <pre>";print_r($arr_Follow)."</pre>"; exit();
		for ($i=0; $i < count($arr_Follow) ; $i++) { 
			if($arr_Follow[$i]['status'] == 1 || $arr_Follow[$i]['status'] == 4 || $arr_Follow[$i]['status'] == 10){ // check status 1,4,10,90
				if($arr_Follow[$i]['r_working_company'] != 0 && $arr_Follow[$i]['follow_repeat_type'] == 0){ // ลูกค้า (ราย) บริษัท
					$arr_data['NotRepeat'][ $arr_Follow[$i]['r_working_company'] ] [0] [0] [0] [0] += 1;//ยังไม่ได้ทำ				
				}else if($arr_Follow[$i]['r_working_company'] != 0 && $arr_Follow[$i]['follow_repeat_type'] == 1){
					$arr_data['Repeat'][ $arr_Follow[$i]['r_working_company'] ] [0] [0] [0] [0] += 1;//ทำซ้ำ
				}

				if($arr_Follow[$i]['r_department'] != 0 && $arr_Follow[$i]['follow_repeat_type'] == 0){ // ลูกค้า (ราย) แผนก
					$arr_data['NotRepeat'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [0] [0] [0] += 1;//ยังไม่ได้ทำ
				}else if($arr_Follow[$i]['r_department'] != 0 && $arr_Follow[$i]['follow_repeat_type'] == 1){
					$arr_data['Repeat'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [0] [0] [0] += 1;//ทำซ้ำ
				}

				if($arr_Follow[$i]['r_section'] != 0 && $arr_Follow[$i]['follow_repeat_type'] == 0){ // ลูกค้า (ราย) ฝ่าย
					$arr_data['NotRepeat'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [0] [0] += 1;//ยังไม่ได้ทำ
				}else if($arr_Follow[$i]['r_section'] != 0 && $arr_Follow[$i]['follow_repeat_type'] == 1){
					$arr_data['Repeat'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [0] [0] += 1;//ทำซ้ำ
				}

				if($arr_Follow[$i]['r_position'] != 0 && $arr_Follow[$i]['follow_repeat_type'] == 0 ){ // ลูกค้า (ราย) ตำแหน่ง
					$arr_data['NotRepeat'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [0] += 1;//ยังไม่ได้ทำ
				}else if($arr_Follow[$i]['r_position'] != 0 && $arr_Follow[$i]['follow_repeat_type'] == 1){
					$arr_data['Repeat'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [0] += 1;//ทำซ้ำ
				}

				if($arr_Follow[$i]['r_id_card'] != "" && $arr_Follow[$i]['follow_repeat_type'] == 0){ // ลูกค้า (ราย) พนักงาน
					$arr_data['NotRepeat'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [ $arr_Follow[$i]['r_id_card'] ] += 1;//ยังไม่ได้ทำ
				}else if($arr_Follow[$i]['r_id_card'] != "" && $arr_Follow[$i]['follow_repeat_type'] == 1){
					$arr_data['Repeat'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [ $arr_Follow[$i]['r_id_card'] ] += 1;//ทำซ้ำ
				}
			}//end if status
		}
//echo "<pre>".print_r($arr_data)."</pre>"; exit();
		if (count($arr_data['NotRepeat']) > 0) {
			foreach ($arr_data['NotRepeat'] as $k_W => $Work) {
				foreach ($Work as $k_D => $Deparment) {
					foreach ($Deparment as $k_S => $Section) {
						foreach ($Section as $k_P => $Position) {
							foreach ($Position as $k_E => $Employee) {
								$arrColumWaitEven['NotRepeat'][$k_W][$k_D][$k_S][$k_P][$k_E] = $Employee;
							}	
						}
					}
				}
			}
		}// end if count

		if (count($arr_data['Repeat']) > 0) {	
			foreach ($arr_data['Repeat'] as $k_W => $Work) {
				foreach ($Work as $k_D => $Deparment) {
					foreach ($Deparment as $k_S => $Section) {
						foreach ($Section as $k_P => $Position) {
							foreach ($Position as $k_E => $Employee) {
								$arrColumWaitEven['Repeat'][$k_W][$k_D][$k_S][$k_P][$k_E] = $Employee;
							}	
						}
					}
				}
			}
		}// end if count
		return $arrColumWaitEven;
	}


	public function getColunm_Complete_Expire_Cancel_WaitCancel_Interested($arr_Follow){ //arr_Follow ช้อมูลจาก table follow_customer  ทำแล้ว หมดอายุ ยกเลิก รอยกเลิก สนใจ  ไม่สนใจ
		$arrAllColum	= array();
		$arr_data 		= array();

		for ($i=0; $i < count($arr_Follow) ; $i++) { 
			if($arr_Follow[$i]['r_working_company'] != 0){ 
				if($arr_Follow[$i]['status'] == 2 || $arr_Follow[$i]['status'] == 5)
				{ $arr_data['Complete'][ $arr_Follow[$i]['r_working_company'] ] [0] [0] [0] [0]  += 1; }// ทำแล้ว บริษัท
				
				if($arr_Follow[$i]['status'] == 98)
				{ $arr_data['Expire'][ $arr_Follow[$i]['r_working_company'] ] [0] [0] [0] [0]  += 1; }// หมดอายุ บริษัท

				if($arr_Follow[$i]['status'] == 99)
				{ $arr_data['Cancel'][ $arr_Follow[$i]['r_working_company'] ] [0] [0] [0] [0]  += 1; }// ยกเลิก บริษัท

				if($arr_Follow[$i]['status'] == 90)
				{ $arr_data['WaitCancel'][ $arr_Follow[$i]['r_working_company'] ] [0] [0] [0] [0]  += 1; }// รอยกเลิก บริษัท

				if($arr_Follow[$i]['follow_prospect_status'] == 1)
				{ $arr_data['Interested'][ $arr_Follow[$i]['r_working_company'] ] [0] [0] [0] [0]  += 1; }// สนใจ บริษัท
				else if($arr_Follow[$i]['follow_prospect_status'] == 2){ $arr_data['NotCare'][ $arr_Follow[$i]['r_working_company'] ] [0] [0] [0] [0]  += 1; }// ไม่สนใจ บริษัท
			}

			if($arr_Follow[$i]['r_department'] != 0){ 
				if($arr_Follow[$i]['status'] == 2 || $arr_Follow[$i]['status'] == 5)
				{ $arr_data['Complete'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [0] [0] [0]  += 1; }// ทำแล้ว แผนก
				
				if($arr_Follow[$i]['status'] == 98)
				{ $arr_data['Expire'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [0] [0] [0]  += 1; }// หมดอายุ แผนก

				if($arr_Follow[$i]['status'] == 99)
				{ $arr_data['Cancel'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [0] [0] [0]  += 1; }// ยกเลิก แผนก

				if($arr_Follow[$i]['status'] == 90)
				{ $arr_data['WaitCancel'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [0] [0] [0]  += 1; }// รอยกเลิก แผนก

				if($arr_Follow[$i]['follow_prospect_status'] == 1)
				{ $arr_data['Interested'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [0] [0] [0]  += 1; }// สนใจ แผนก
				else if($arr_Follow[$i]['follow_prospect_status'] == 2){ $arr_data['NotCare'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [0] [0] [0]  += 1; }// ไม่สนใจ แผนก
			}

			if($arr_Follow[$i]['r_section'] != 0){ 
				if($arr_Follow[$i]['status'] == 2 || $arr_Follow[$i]['status'] == 5)
				{ $arr_data['Complete'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [0] [0]  += 1; }// ทำแล้ว ฝ่าย
				
				if($arr_Follow[$i]['status'] == 98)
				{ $arr_data['Expire'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [0] [0]  += 1; }// หมดอายุ ฝ่าย

				if($arr_Follow[$i]['status'] == 99)
				{ $arr_data['Cancel'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [0] [0]  += 1; }// ยกเลิก ฝ่าย

				if($arr_Follow[$i]['status'] == 90)
				{ $arr_data['WaitCancel'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [0] [0]  += 1; }// รอยกเลิก ฝ่าย

				if($arr_Follow[$i]['follow_prospect_status'] == 1)
				{ $arr_data['Interested'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [0] [0]  += 1; }// สนใจ ฝ่าย
				else if($arr_Follow[$i]['follow_prospect_status'] == 2){ $arr_data['NotCare'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [0] [0]  += 1; }// ไม่สนใจ ฝ่าย
			}

			if($arr_Follow[$i]['r_position'] != 0){ 
				if($arr_Follow[$i]['status'] == 2 || $arr_Follow[$i]['status'] == 5)
				{ $arr_data['Complete'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [0]  += 1; }// ทำแล้ว ตำแหน่ง
				
				if($arr_Follow[$i]['status'] == 98)
				{ $arr_data['Expire'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [0]  += 1; }// หมดอายุ ตำแหน่ง

				if($arr_Follow[$i]['status'] == 99)
				{ $arr_data['Cancel'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [0]  += 1; }// ยกเลิก ตำแหน่ง

				if($arr_Follow[$i]['status'] == 90)
				{ $arr_data['WaitCancel'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [0]  += 1; }// รอยกเลิก ตำแหน่ง

				if($arr_Follow[$i]['follow_prospect_status'] == 1)
				{ $arr_data['Interested'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [0]  += 1; }// สนใจ ตำแหน่ง
				else if($arr_Follow[$i]['follow_prospect_status'] == 2){ $arr_data['NotCare'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [0]  += 1; }// ไม่สนใจ ตำแหน่ง
			}

			if($arr_Follow[$i]['r_id_card'] != ""){
				if($arr_Follow[$i]['status'] == 2 || $arr_Follow[$i]['status'] == 5)
				{ $arr_data['Complete'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [ $arr_Follow[$i]['r_id_card'] ]  += 1; }// ทำแล้ว พนักงาน
				
				if($arr_Follow[$i]['status'] == 98)
				{ $arr_data['Expire'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [ $arr_Follow[$i]['r_id_card'] ]  += 1; }// หมดอายุ พนักงาน

				if($arr_Follow[$i]['status'] == 99)
				{ $arr_data['Cancel'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [ $arr_Follow[$i]['r_id_card'] ]  += 1; }// ยกเลิก พนักงาน

				if($arr_Follow[$i]['status'] == 90)
				{ $arr_data['WaitCancel'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [ $arr_Follow[$i]['r_id_card'] ]  += 1; }// รอยกเลิก พนักงาน
			
				if($arr_Follow[$i]['follow_prospect_status'] == 1)
				{ $arr_data['Interested'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [ $arr_Follow[$i]['r_id_card'] ]  += 1; }// สนใจ พนักงาน
				else if($arr_Follow[$i]['follow_prospect_status'] == 2){ $arr_data['NotCare'][ $arr_Follow[$i]['r_working_company'] ] [ $arr_Follow[$i]['r_department'] ] [ $arr_Follow[$i]['r_section'] ] [ $arr_Follow[$i]['r_position'] ] [ $arr_Follow[$i]['r_id_card'] ]  += 1; }// ไม่สนใจ พนักงาน
			}
		}
//echo "<pre>".print_r($arr_data)."</pre>"; exit();
		if (count($arr_data['Complete']) > 0) {	
			foreach ($arr_data['Complete'] as $k_W => $Work) {
				foreach ($Work as $k_D => $Deparment) {
					foreach ($Deparment as $k_S => $Section) {
						foreach ($Section as $k_P => $Position) {
							foreach ($Position as $k_E => $Employee) {
								$arrAllColum['Complete'][$k_W][$k_D][$k_S][$k_P][$k_E] = $Employee;
							}	
						}
					}
				}
			}
		}// end if count

		if (count($arr_data['Expire']) > 0) {	
			foreach ($arr_data['Expire'] as $k_W => $Work) {
				foreach ($Work as $k_D => $Deparment) {
					foreach ($Deparment as $k_S => $Section) {
						foreach ($Section as $k_P => $Position) {
							foreach ($Position as $k_E => $Employee) {
								$arrAllColum['Expire'][$k_W][$k_D][$k_S][$k_P][$k_E] = $Employee;
							}	
						}
					}
				}
			}
		}// end if count

		if (count($arr_data['Cancel']) > 0) {	
			foreach ($arr_data['Cancel'] as $k_W => $Work) {
				foreach ($Work as $k_D => $Deparment) {
					foreach ($Deparment as $k_S => $Section) {
						foreach ($Section as $k_P => $Position) {
							foreach ($Position as $k_E => $Employee) {
								$arrAllColum['Cancel'][$k_W][$k_D][$k_S][$k_P][$k_E] = $Employee;
							}	
						}
					}
				}
			}
		}// end if count

		if (count($arr_data['WaitCancel']) > 0) {	
			foreach ($arr_data['WaitCancel'] as $k_W => $Work) {
				foreach ($Work as $k_D => $Deparment) {
					foreach ($Deparment as $k_S => $Section) {
						foreach ($Section as $k_P => $Position) {
							foreach ($Position as $k_E => $Employee) {
								$arrAllColum['WaitCancel'][$k_W][$k_D][$k_S][$k_P][$k_E] = $Employee;
							}	
						}
					}
				}
			}
		}// end if count

		if (count($arr_data['Interested']) > 0) {	
			foreach ($arr_data['Interested'] as $k_W => $Work) {
				foreach ($Work as $k_D => $Deparment) {
					foreach ($Deparment as $k_S => $Section) {
						foreach ($Section as $k_P => $Position) {
							foreach ($Position as $k_E => $Employee) {
								$arrAllColum['Interested'][$k_W][$k_D][$k_S][$k_P][$k_E] = $Employee;
							}	
						}
					}
				}
			}
		}// end if count

		if (count($arr_data['NotCare']) > 0) {	
			foreach ($arr_data['NotCare'] as $k_W => $Work) {
				foreach ($Work as $k_D => $Deparment) {
					foreach ($Deparment as $k_S => $Section) {
						foreach ($Section as $k_P => $Position) {
							foreach ($Position as $k_E => $Employee) {
								$arrAllColum['NotCare'][$k_W][$k_D][$k_S][$k_P][$k_E] = $Employee;
							}	
						}
					}
				}
			}
		}// end if count

		return $arrAllColum;
	}

	public function getDataFollowCusJoinMainCusData($idWorking = "", $idDepartment = "", $idSection = "", $PositionCode = "" , $idEven = "", $StartDate = "" , $StopDate = "", $id_card = ""){
		$arr_data = array();
		$where = "";
		if($idWorking 	 != ""){ $where .= " AND r_working_company 	= ".$idWorking; }
		if($idDepartment != ""){ $where .= " AND r_department 		= ".$idDepartment; }
		if($idSection 	 != ""){ $where .= " AND r_section 			= ".$idSection; }
		if($PositionCode != ""){ $where .= " AND r_position 		= ".$PositionCode; }
		if($idEven 		 != ""){ $where .= " AND event_id_ref 		= ".$idEven; }
		if($id_card 	 != ""){ $where .= " AND r_id_card IN (".$id_card.")"; }
		if($StartDate 	 != "" && $StopDate 	 != ""){
		$where .= " AND (
					  		 ( start_date BETWEEN '".$this->ConvertDateToYearMonthDate($StartDate)."'  AND '".$this->ConvertDateToYearMonthDate($StopDate)."' )
						 OR  ( stop_date  BETWEEN '".$this->ConvertDateToYearMonthDate($StartDate)."'  AND '".$this->ConvertDateToYearMonthDate($StopDate)."' )
						 OR  (
								( '".$this->ConvertDateToYearMonthDate($StartDate)."'  BETWEEN  start_date AND stop_date ) AND
								( '".$this->ConvertDateToYearMonthDate($StopDate)."'   BETWEEN  start_date AND stop_date )
							 )
						)";
		}
		
		$sql_Follow = " SELECT
							follow_customer.id AS id_follow_customer,
							follow_main_event.id AS follow_main_event_id,
							follow_main_event.event_title,
							follow_customer.cus_no,
							CONCAT(MAIN_CUS_GINFO.Be, MAIN_CUS_GINFO.Cus_Name, ' ', MAIN_CUS_GINFO.Cus_Surename) AS CusFullName	,
	 			 			CONCAT(emp_data.Be, emp_data.`Name`, ' ', emp_data.Surname) AS EmpFullName ,
							MAIN_ADDRESS.ADDR_VILLAGE,
							MAIN_ADDRESS.ADDR_NUMBER,
							MAIN_ADDRESS.ADDR_GROUP_NO,
							SUBSTRING_INDEX( MAIN_ADDRESS.ADDR_SUB_DISTRICT, '_', -1 ) AS ADDR_SUB_DISTRICT,
							SUBSTRING_INDEX( MAIN_ADDRESS.ADDR_DISTRICT , '_', -1 ) AS ADDR_DISTRICT,
							SUBSTRING_INDEX( MAIN_ADDRESS.ADDR_PROVINCE , '_', -1 ) AS ADDR_PROVINCE,
							MAIN_ADDRESS.ADDR_POSTCODE,
							MAIN_TELEPHONE.TEL_NUM,
							AMIAN_CUS_TYPE.CUS_TYPE_LEVEL_DESC AS ClassCus,
							working_company.`name` AS WorkName,
							DATE_FORMAT(DATE_ADD(follow_customer.process_date,INTERVAL 543 YEAR), '%d/%m/%Y')	AS process_date,
							follow_customer_historys.id_log,
							follow_customer_historys.follow_contact_status,
							follow_customer_historys.follow_convenient_status,
							follow_customer_historys.follow_prospect_status,
							follow_customer_historys.follow_contact_reason,
							follow_customer_historys.follow_status_remark,
  							DATE_FORMAT(DATE_ADD(follow_customer_historys.historys_date,INTERVAL 543 YEAR), '%d/%m/%Y') AS historys_date,
							DATE_FORMAT(DATE_ADD(follow_customer_historys.date_value,INTERVAL 543 YEAR), '%d/%m/%Y') AS date_value,
							DATE_FORMAT(follow_customer_historys.date_value,'%H:%i:%s') AS time_value							
						FROM
							(
								SELECT
									id,	cus_no,	event_id_ref, emp_id_card, emp_posCode, r_working_company, process_date,
									r_department, r_section, r_position, r_id_card,	follow_repeat_type,	follow_contact_status,
									follow_convenient_status, follow_prospect_status, follow_contact_reason, follow_status_remark,
									cancel_type, remark, `status`
								FROM
									".$this->config['db_base_name'].".follow_customer
								WHERE 1 = 1 $where				

								UNION

								SELECT
									id,	cus_no,	event_id_ref, emp_id_card, emp_posCode, r_working_company, process_date,
									r_department, r_section, r_position, r_id_card,	follow_repeat_type,	follow_contact_status,
									follow_convenient_status, follow_prospect_status, follow_contact_reason, follow_status_remark,
									cancel_type, remark, `status`
								FROM
									".$this->config['db_base_name'].".follow_customer_cancel
								WHERE 1 = 1 $where
							) AS follow_customer
							LEFT JOIN ".$this->config['db_base_name'].".follow_main_event 	ON follow_customer.event_id_ref 		= follow_main_event.id
							LEFT JOIN ".$this->config['db_maincus'].".MAIN_CUS_GINFO 		ON follow_customer.cus_no 				= MAIN_CUS_GINFO.CusNo
							LEFT JOIN ".$this->config['db_maincus'].".MAIN_ADDRESS 			ON MAIN_ADDRESS.ADDR_CUS_NO 			= follow_customer.cus_no
							LEFT JOIN ".$this->config['db_maincus'].".MAIN_TELEPHONE 		ON MAIN_TELEPHONE.TEL_CUS_NO 			= follow_customer.cus_no 	AND MAIN_TELEPHONE.TEL_MAIN_ACTIVE = 1
							LEFT JOIN ".$this->config['db_emp'].".emp_data 					ON follow_customer.r_id_card 			= emp_data.ID_Card
							LEFT JOIN ".$this->config['db_organi'].".working_company 		ON follow_customer.r_working_company 	= working_company.id
							LEFT JOIN ".$this->config['db_maincus'].".MIAN_CUS_TYPE_REF 	ON follow_customer.cus_no 				= MIAN_CUS_TYPE_REF.CTYPE_REF_CUSNO
							LEFT JOIN ".$this->config['db_maincus'].".AMIAN_CUS_TYPE 		ON MIAN_CUS_TYPE_REF.CTYPE_GRADE_REF 	= AMIAN_CUS_TYPE.CUS_TYPE_ID
							LEFT JOIN ".$this->config['db_base_name'].".follow_customer_historys ON follow_customer.id = follow_customer_historys.ref_follow_id 
								  AND follow_customer.event_id_ref = follow_customer_historys.event_id_ref
								  AND follow_customer_historys.id_log IN(
									SELECT MAX(follow_customer_historys.id_log)
									FROM ".$this->config['db_base_name'].".follow_customer_historys
									GROUP BY follow_customer_historys.ref_follow_id
								  )
						WHERE
							1 = 1 
						GROUP BY follow_customer.id
						ORDER BY follow_main_event.id"; 
		$res_Follow = $this->logDb->queryAndLogSQL( $sql_Follow, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while($arr_Follow = mysql_fetch_assoc($res_Follow)){
			$arr_data[] = $arr_Follow;
		}

		return $arr_data;
	}//end function

	public function CrateHTML_Working($work_name, $arr_clCustomer, $arr_clEven, $arr_clWaitEven, $arr_clAllEven, $WorkCompany){
		$clCustomer_W   	= (int)$arr_clCustomer				[ $WorkCompany ] [0] [0] [0] [0];// บริษัท ลูกค้า (ราย)
		$clEvenAll_W		= (int)$arr_clEven['ALL'] 			[ $WorkCompany ] [0] [0] [0] [0];// บริษัท ส่วนรวม
		$clEvenPrivate_W	= (int)$arr_clEven['Private'] 		[ $WorkCompany ] [0] [0] [0] [0];// บริษัท ส่วนตัว
		$clWaitEven_NotRe_W	= (int)$arr_clWaitEven['NotRepeat'] [ $WorkCompany ] [0] [0] [0] [0];// บริษัท ยังไม่ได้ทำ
		$clWaitEven_Re_W	= (int)$arr_clWaitEven['Repeat'] 	[ $WorkCompany ] [0] [0] [0] [0];// บริษัท ทำซ้ำ
		$clComplete_W		= (int)$arr_clAllEven['Complete']	[ $WorkCompany ] [0] [0] [0] [0];// บริษัท ทำแล้ว
		$clExpire_W			= (int)$arr_clAllEven['Expire']		[ $WorkCompany ] [0] [0] [0] [0];// บริษัท หมดอายุ
		$clCancel_W			= (int)$arr_clAllEven['Cancel']		[ $WorkCompany ] [0] [0] [0] [0];// บริษัท ยกเลิก
		$clWaitCancel_W		= (int)$arr_clAllEven['WaitCancel']	[ $WorkCompany ] [0] [0] [0] [0];// บริษัท รอยกเลิก
		$clInterested_W		= (int)$arr_clAllEven['Interested']	[ $WorkCompany ] [0] [0] [0] [0];// บริษัท สนใจ
		$clNotCare_W		= (int)$arr_clAllEven['NotCare']		[ $WorkCompany ] [0] [0] [0] [0];// บริษัท ไม่สนใจ
		$clAllEven_W		= $clWaitEven_NotRe_W + $clWaitEven_Re_W + $clComplete_W + $clExpire_W + $clCancel_W + $clWaitCancel_W;// บริษัท ทั้งหมด = ยังไม่ได้ทำ + ทำซ้ำ + ทำแล้ว + หมดอายุ + ยกเลิก + รอยกเลิก
		$EvenVlue_W			= $clEvenAll_W + $clEvenPrivate_W;
		$clComplePersen_W	= number_format( ($EvenVlue_W == 0 ? '0' : ( $clComplete_W * 100 ) / $EvenVlue_W ) , 2); //แผนก ทำแล้ว (%) = (ทำแล้ว*100 )/กิจกรรม *** กิจกรรม = ส่วนรวม + ส่วนตัว 
		$class 				= 'W'.$WorkCompany;
		$strHTML ="<tr style='background-color: #6495ED;color:#000000;cursor:pointer;' class='$class' id='$class' onclick='ChangRowHideShow(jQuery(this),\"$class\")'>
						<td colspan='5' style='text-align:left;'>$work_name</td>
						<td>$clCustomer_W</td>
						<td>$clEvenAll_W</td>
						<td>$clEvenPrivate_W</td>
						<td>$clWaitEven_NotRe_W</td>
						<td>$clWaitEven_Re_W</td>
						<td>$clComplete_W</td>
						<td>$clExpire_W</td>
						<td>$clCancel_W</td>
						<td>$clWaitCancel_W</td>
						<td>$clInterested_W</td>
						<td>$clNotCare_W</td>
						<td>$clAllEven_W</td>
						<td>$clComplePersen_W</td>
					</tr>";
		return $strHTML;
	}


	public function CrateHTML_Department($DeparmentName, $arr_clCustomer, $arr_clEven, $arr_clWaitEven, $arr_clAllEven, $WorkCompany, $Department){
		$clCustomer_D 		= (int)$arr_clCustomer				[ $WorkCompany ] [ $Department ] [0] [0] [0];// แผนก ลูกค้า (ราย)
		$clEvenAll_D	  	= (int)$arr_clEven['ALL'] 			[ $WorkCompany ] [ $Department ] [0] [0] [0];// แผนก ส่วนรวม
		$clEvenPrivate_D	= (int)$arr_clEven['Private'] 		[ $WorkCompany ] [ $Department ] [0] [0] [0];// แผนก ส่วนตัว
		$clWaitEven_NotRe_D	= (int)$arr_clWaitEven['NotRepeat'] [ $WorkCompany ] [ $Department ] [0] [0] [0];// แผนก ยังไม่ได้ทำ
		$clWaitEven_Re_D	= (int)$arr_clWaitEven['Repeat'] 	[ $WorkCompany ] [ $Department ] [0] [0] [0];// แผนก ทำซ้ำ
		$clComplete_D		= (int)$arr_clAllEven['Complete']	[ $WorkCompany ] [ $Department ] [0] [0] [0];// แผนก ทำแล้ว
		$clExpire_D			= (int)$arr_clAllEven['Expire']		[ $WorkCompany ] [ $Department ] [0] [0] [0];// แผนก หมดอายุ
		$clCancel_D			= (int)$arr_clAllEven['Cancel']		[ $WorkCompany ] [ $Department ] [0] [0] [0];// แผนก ยกเลิก
		$clWaitCancel_D		= (int)$arr_clAllEven['WaitCancel']	[ $WorkCompany ] [ $Department ] [0] [0] [0];// แผนก รอยกเลิก
		$clInterested_D		= (int)$arr_clAllEven['Interested']	[ $WorkCompany ] [ $Department ] [0] [0] [0];// แผนก สนใจ
		$clNotCare_D		= (int)$arr_clAllEven['NotCare']		[ $WorkCompany ] [ $Department ] [0] [0] [0];// แผนก ไม่สนใจ
		$clAllEven_D		= (int)$clWaitEven_NotRe_D + $clWaitEven_Re_D + $clComplete_D + $clExpire_D + $clCancel_D + $clWaitCancel_D;// แผนก ทั้งหมด = ยังไม่ได้ทำ + ทำซ้ำ + ทำแล้ว + หมดอายุ + ยกเลิก + รอยกเลิก
		$EvenVlue_D			= $clEvenAll_D + $clEvenPrivate_D;
		$clComplePersen_D	= number_format( ($EvenVlue_D == 0 ? '0' : ( $clComplete_D * 100 ) / $EvenVlue_D ) , 2); //แผนก ทำแล้ว (%) = (ทำแล้ว*100 )/กิจกรรม *** กิจกรรม = ส่วนรวม + ส่วนตัว 
		$class 				= 'W'.$WorkCompany.'D'.$Department;
		$strHTML = "<tr style='background-color: #0099FF;color:#000000;cursor:pointer;' class='$class' id='$class' onclick='ChangRowHideShow(jQuery(this),\"$class\")'>
						<td>&nbsp;</td>
						<td colspan='4' style='text-align:left;'>$DeparmentName</td>
						<td>$clCustomer_D</td>
						<td>$clEvenAll_D</td>
						<td>$clEvenPrivate_D</td>
						<td>$clWaitEven_NotRe_D</td>
						<td>$clWaitEven_Re_D</td>
						<td>$clComplete_D</td>
						<td>$clExpire_D</td>
						<td>$clCancel_D</td>
						<td>$clWaitCancel_D</td>
						<td>$clInterested_D</td>
						<td>$clNotCare_D</td>
						<td>$clAllEven_D</td>
						<td>$clComplePersen_D</td>
					</tr>";
		return $strHTML;
	}


	public function CrateHTML_Section($SectionName, $arr_clCustomer, $arr_clEven, $arr_clWaitEven, $arr_clAllEven, $WorkCompany, $Department, $Section){
		$clCustomer_S 		= (int)$arr_clCustomer				[ $WorkCompany ] [ $Department ] [ $Section ] [0] [0];// ฝ่าย ลูกค้า (ราย) 
		$clEvenAll_S		= (int)$arr_clEven['ALL'] 			[ $WorkCompany ] [ $Department ] [ $Section ] [0] [0];// ฝ่าย ส่วนรวม 
		$clEvenPrivate_S	= (int)$arr_clEven['Private'] 		[ $WorkCompany ] [ $Department ] [ $Section ] [0] [0];// ฝ่าย ส่วนตัว 
		$clWaitEven_NotRe_S	= (int)$arr_clWaitEven['NotRepeat'] [ $WorkCompany ] [ $Department ] [ $Section ] [0] [0];// ฝ่าย ยังไม่ได้ทำ 
		$clWaitEven_Re_S	= (int)$arr_clWaitEven['Repeat'] 	[ $WorkCompany ] [ $Department ] [ $Section ] [0] [0];// ฝ่าย ทำซ้ำ 
		$clComplete_S		= (int)$arr_clAllEven['Complete']	[ $WorkCompany ] [ $Department ] [ $Section ] [0] [0];// ฝ่าย ทำแล้ว 
		$clExpire_S			= (int)$arr_clAllEven['Expire']		[ $WorkCompany ] [ $Department ] [ $Section ] [0] [0];// ฝ่าย หมดอายุ 
		$clCancel_S			= (int)$arr_clAllEven['Cancel']		[ $WorkCompany ] [ $Department ] [ $Section ] [0] [0];// ฝ่าย ยกเลิก 
		$clWaitCancel_S		= (int)$arr_clAllEven['WaitCancel']	[ $WorkCompany ] [ $Department ] [ $Section ] [0] [0];// ฝ่าย รอยกเลิก 
		$clInterested_S		= (int)$arr_clAllEven['Interested']	[ $WorkCompany ] [ $Department ] [ $Section ] [0] [0];// ฝ่าย สนใจ 
		$clNotCare_S		= (int)$arr_clAllEven['NotCare']		[ $WorkCompany ] [ $Department ] [ $Section ] [0] [0];// ฝ่าย ไม่สนใจ 
		$clAllEven_S		= $clWaitEven_NotRe_S + $clWaitEven_Re_S + $clComplete_S + $clExpire_S + $clCancel_S + $clWaitCancel_S;// ฝ่าย ทั้งหมด = ยังไม่ได้ทำ + ทำซ้ำ + ทำแล้ว + หมดอายุ + ยกเลิก + รอยกเลิก
		$EvenVlue_S			= $clEvenAll_S + $clEvenPrivate_S;
		$clComplePersen_S	= number_format( ($EvenVlue_S == 0 ? '0' : ( $clComplete_S * 100 ) / $EvenVlue_S ) , 2); //แผนก ทำแล้ว (%) = (ทำแล้ว*100 )/กิจกรรม *** กิจกรรม = ส่วนรวม + ส่วนตัว 
		$class 				= 'W'.$WorkCompany.'D'.$Department.'S'.$Section;
		$strHTML = "<tr style='background-color: #8EE5EE;color:#000000;cursor:pointer;' class='$class' id='$class' onclick='ChangRowHideShow(jQuery(this),\"$class\")'>
						<td colspan='2'>&nbsp;</td>
						<td colspan='3' style='text-align:left;'>$SectionName</td>
						<td>$clCustomer_S</td>
						<td>$clEvenAll_S</td>
						<td>$clEvenPrivate_S</td>
						<td>$clWaitEven_NotRe_S</td>
						<td>$clWaitEven_Re_S</td>
						<td>$clComplete_S</td>
						<td>$clExpire_S</td>
						<td>$clCancel_S</td>
						<td>$clWaitCancel_S</td>
						<td>$clInterested_S</td>
						<td>$clNotCare_S</td>
						<td>$clAllEven_S</td>
						<td>$clComplePersen_S</td>
					</tr>";
		return $strHTML;
	}


	public function CrateHTML_Position($PositionName, $arr_clCustomer, $arr_clEven, $arr_clWaitEven, $arr_clAllEven, $WorkCompany, $Department, $Section, $Position){
		$clCustomer_P 		= (int)$arr_clCustomer				[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [0];// ตำแหน่ง ลูกค้า (ราย) 
		$clEvenAll_P	  	= (int)$arr_clEven['ALL'] 			[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [0];// ตำแหน่ง ส่วนรวม 
		$clEvenPrivate_P	= (int)$arr_clEven['Private'] 		[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [0];// ตำแหน่ง ส่วนตัว 
		$clWaitEven_NotRe_P	= (int)$arr_clWaitEven['NotRepeat'] [ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [0];// ตำแหน่ง ยังไม่ได้ทำ 
		$clWaitEven_Re_P	= (int)$arr_clWaitEven['Repeat'] 	[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [0];// ตำแหน่ง ทำซ้ำ 
		$clComplete_P		= (int)$arr_clAllEven['Complete']	[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [0];// ตำแหน่ง ทำแล้ว 
		$clExpire_P			= (int)$arr_clAllEven['Expire']		[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [0];// ตำแหน่ง หมดอายุ  
		$clCancel_P			= (int)$arr_clAllEven['Cancel']		[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [0];// ตำแหน่ง ยกเลิก 
		$clWaitCancel_P		= (int)$arr_clAllEven['WaitCancel']	[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [0];// ตำแหน่ง รอยกเลิก 
		$clInterested_P		= (int)$arr_clAllEven['Interested']	[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [0];// ตำแหน่ง สนใจ 
		$clNotCare_P		= (int)$arr_clAllEven['NotCare']		[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [0];// ตำแหน่ง ไม่สนใจ 
		$clAllEven_P		= $clWaitEven_NotRe_P + $clWaitEven_Re_P + $clComplete_P + $clExpire_P + $clCancel_P + $clWaitCancel_P;// ตำแหน่ง ทั้งหมด = ยังไม่ได้ทำ + ทำซ้ำ + ทำแล้ว + หมดอายุ + ยกเลิก + รอยกเลิก
		$EvenVlue_P			= $clEvenAll_P + $clEvenPrivate_P;
		$clComplePersen_P	= number_format( ($EvenVlue_P == 0 ? '0' : ( $clComplete_P * 100 ) / $EvenVlue_P ) , 2); //แผนก ทำแล้ว (%) = (ทำแล้ว*100 )/กิจกรรม *** กิจกรรม = ส่วนรวม + ส่วนตัว 
		$class 				= 'W'.$WorkCompany.'D'.$Department.'S'.$Section.'P'.$Position;
		$strHTML = "<tr style='background-color: #BCD2EE;color:#000000;cursor:pointer;' class='$class' id='$class' onclick='ChangRowHideShow(jQuery(this),\"$class\")'> 
						<td colspan='3'>&nbsp;</td>
						<td colspan='2' style='text-align:left;'>$PositionName</td>
						<td>$clCustomer_P</td>
						<td>$clEvenAll_P</td>
						<td>$clEvenPrivate_P</td>
						<td>$clWaitEven_NotRe_P</td>
						<td>$clWaitEven_Re_P</td>
						<td>$clComplete_P</td>
						<td>$clExpire_P</td>
						<td>$clCancel_P</td>
						<td>$clWaitCancel_P</td>
						<td>$clInterested_P</td>
						<td>$clNotCare_P</td>
						<td>$clAllEven_P</td>
						<td>$clComplePersen_P</td>
					</tr>";
		return $strHTML;
	}



	public function CrateHTML_Employee($EMP_Fullname, $arr_clCustomer, $arr_clEven, $arr_clWaitEven, $arr_clAllEven, $WorkCompany, $Department, $Section, $Position, $ID_Card){
		$clCustomer_E 		= (int)$arr_clCustomer				[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [ $ID_Card ];// พนักงาน ลูกค้า (ราย) 
		$clEvenAll_E	  	= (int)$arr_clEven['ALL'] 			[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [ $ID_Card ];// พนักงาน ส่วนรวม 
		$clEvenPrivate_E	= (int)$arr_clEven['Private'] 		[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [ $ID_Card ];// พนักงาน ส่วนตัว 
		$clWaitEven_NotRe_E	= (int)$arr_clWaitEven['NotRepeat'] [ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [ $ID_Card ];// พนักงาน ยังไม่ได้ทำ 
		$clWaitEven_Re_E	= (int)$arr_clWaitEven['Repeat'] 	[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [ $ID_Card ];// พนักงาน ทำซ้ำ 
		$clComplete_E		= (int)$arr_clAllEven['Complete']	[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [ $ID_Card ];// พนักงาน ทำแล้ว 
		$clExpire_E			= (int)$arr_clAllEven['Expire']		[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [ $ID_Card ];// พนักงาน หมดอายุ 
		$clCancel_E			= (int)$arr_clAllEven['Cancel']		[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [ $ID_Card ];// พนักงาน ยกเลิก 
		$clWaitCancel_E		= (int)$arr_clAllEven['WaitCancel']	[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [ $ID_Card ];// พนักงาน รอยกเลิก 
		$clInterested_E		= (int)$arr_clAllEven['Interested']	[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [ $ID_Card ];// พนักงาน สนใจ 
		$clNotCare_E		= (int)$arr_clAllEven['NotCare']		[ $WorkCompany ] [ $Department ] [ $Section ] [ $Position ] [ $ID_Card ];// พนักงาน ไม่สนใจ 
		$clAllEven_E		= $clWaitEven_NotRe_E + $clWaitEven_Re_E + $clComplete_E + $clExpire_E + $clCancel_E + $clWaitCancel_E;// พนักงาน ทั้งหมด = ยังไม่ได้ทำ + ทำซ้ำ + ทำแล้ว + หมดอายุ + ยกเลิก + รอยกเลิก
		$EvenVlue_E			= $clEvenAll_E + $clEvenPrivate_E;
		$clComplePersen_E	= number_format( ($EvenVlue_E == 0 ? '0' : ( $clComplete_E * 100 ) / $EvenVlue_E ) , 2); //แผนก ทำแล้ว (%) = (ทำแล้ว*100 )/กิจกรรม *** กิจกรรม = ส่วนรวม + ส่วนตัว 
		$class 				= 'W'.$WorkCompany.'D'.$Department.'S'.$Section.'P'.$Position.'E'.$ID_Card;
		$strHTML = "<tr style='background-color: #FFFFFF;color:#000000' class='$class'>
						<td colspan='4'>&nbsp;</td>
						<td style='text-align:left;'> $EMP_Fullname </td>
						<td>$clCustomer_E</td>
						<td>$clEvenAll_E</td>
						<td>$clEvenPrivate_E</td>
						<td>$clWaitEven_NotRe_E</td>
						<td>$clWaitEven_Re_E</td>
						<td>$clComplete_E</td>
						<td>$clExpire_E</td>
						<td>$clCancel_E</td>
						<td>$clWaitCancel_E</td>
						<td>$clInterested_E</td>
						<td>$clNotCare_E</td>
						<td>$clAllEven_E</td>
						<td>$clComplePersen_E</td>
					</tr>";
		return $strHTML;
	}


	public function CrateHTML_tFoot($work_name, $arr_clCustomer, $arr_clEven, $arr_clWaitEven, $arr_clAllEven, $WorkCompany){
		$clCustomer_W   	= (int)$arr_clCustomer				[ $WorkCompany ] [0] [0] [0] [0];// บริษัท ลูกค้า (ราย)
		$clEvenAll_W		= (int)$arr_clEven['ALL'] 			[ $WorkCompany ] [0] [0] [0] [0];// บริษัท ส่วนรวม
		$clEvenPrivate_W	= (int)$arr_clEven['Private'] 		[ $WorkCompany ] [0] [0] [0] [0];// บริษัท ส่วนตัว
		$clWaitEven_NotRe_W	= (int)$arr_clWaitEven['NotRepeat'] [ $WorkCompany ] [0] [0] [0] [0];// บริษัท ยังไม่ได้ทำ
		$clWaitEven_Re_W	= (int)$arr_clWaitEven['Repeat'] 	[ $WorkCompany ] [0] [0] [0] [0];// บริษัท ทำซ้ำ
		$clComplete_W		= (int)$arr_clAllEven['Complete']	[ $WorkCompany ] [0] [0] [0] [0];// บริษัท ทำแล้ว
		$clExpire_W			= (int)$arr_clAllEven['Expire']		[ $WorkCompany ] [0] [0] [0] [0];// บริษัท หมดอายุ
		$clCancel_W			= (int)$arr_clAllEven['Cancel']		[ $WorkCompany ] [0] [0] [0] [0];// บริษัท ยกเลิก
		$clWaitCancel_W		= (int)$arr_clAllEven['WaitCancel']	[ $WorkCompany ] [0] [0] [0] [0];// บริษัท รอยกเลิก
		$clInterested_W		= (int)$arr_clAllEven['Interested']	[ $WorkCompany ] [0] [0] [0] [0];// บริษัท สนใจ
		$clNotCare_W		= (int)$arr_clAllEven['NotCare']		[ $WorkCompany ] [0] [0] [0] [0];// บริษัท ไม่สนใจ
		$clAllEven_W		= $clWaitEven_NotRe_W + $clWaitEven_Re_W + $clComplete_W + $clExpire_W + $clCancel_W + $clWaitCancel_W;// บริษัท ทั้งหมด = ยังไม่ได้ทำ + ทำซ้ำ + ทำแล้ว + หมดอายุ + ยกเลิก + รอยกเลิก
		$EvenVlue_W			= $clEvenAll_W + $clEvenPrivate_W;
		$clComplePersen_W	= number_format( ($EvenVlue_W == 0 ? '0' : ( $clComplete_W * 100 ) / $EvenVlue_W ) , 2); //แผนก ทำแล้ว (%) = (ทำแล้ว*100 )/กิจกรรม *** กิจกรรม = ส่วนรวม + ส่วนตัว 
		$class 				= 'W'.$WorkCompany;
		$strHTML ="<tr style='background: #c3d9ff;'>
					<td colspan='5' style='text-align:left;'>รวม</td>
					<td>$clCustomer_W</td>
					<td>$clEvenAll_W</td>
					<td>$clEvenPrivate_W</td>
					<td>$clWaitEven_NotRe_W</td>
					<td>$clWaitEven_Re_W</td>
					<td>$clComplete_W</td>
					<td>$clExpire_W</td>
					<td>$clCancel_W</td>
					<td>$clWaitCancel_W</td>
					<td>$clInterested_W</td>
					<td>$clNotCare_W</td>
					<td>$clAllEven_W</td>
					<td>$clComplePersen_W</td>
				</tr>";
		return $strHTML;
	}

	public function ConvertDateToYearMonthDate($strDate){
		$arrDate = explode('-', $strDate);
		if($arrDate[2] > 1000){
			if($arrDate[2] > 2500){ $arrDate[2] = (int)$arrDate[2]-543; }
			$strDate = $arrDate[2].'-'.$arrDate[1].'-'.$arrDate[0];
		}else{
			if($arrDate[1] > 2500){ $arrDate[1] = (int)$arrDate[1]-543; }
			$strDate = $arrDate[0].'-'.$arrDate[1].'-'.$arrDate[1];
		}
		return $strDate;
	}


	public function getTeamCommand($login_position , $include_leader=1){
		$follower_result 	= array();
		$follower_id_card 	= array();
		$follower_pos 		= array();
		global $follower_result , $follower_id_card , $follower_pos, $logDb;

		if($include_leader == 2){
		
			$follower_id_card[] = $_SESSION['SESSION_ID_card'];
			$follower_pos[]	= $login_position;
		}
		
		$check_follower  = " SELECT Follower,Status FROM ".$this->config['db_organi'].".command ";
		$check_follower .= " WHERE Leader = '$login_position'  ";

		$check_follower_result = mysql_query($check_follower) or die ('<br>'.mysql_error() .'<br>'. $check_follower );
		
		$check_follwer_num_rows = mysql_num_rows($check_follower_result);
		if($check_follwer_num_rows > 0){
			while($follower_rows = mysql_fetch_object($check_follower_result)){
				if($follower_rows->Status != 99 ) {
					$emp_query = "SELECT id_card,position_id FROM ".$this->config['db_organi'].".relation_position WHERE position_id = '".$follower_rows->Follower."' AND status = 1";
					$emp_result = mysql_query($emp_query) or die ('<br>'.mysql_error() .'<br>'. $emp_query );
					
					while($emp_row = mysql_fetch_object($emp_result)){
						$follower_id_card[] 	= $emp_row->id_card;
						$follower_pos[]			= $emp_row->position_id;                                                                                                                                                                                                            
					}
					$this->getTeamCommand($follower_rows->Follower);
				}
				
			}
		}
		$follower_result['id_card'] 	= $follower_id_card;
		$follower_result['position'] 	= $follower_pos;
		return $follower_result;

	}//end function	


	public function getExcel_Head($objWorkSheet, $title){
		$Row = 1;
    	$objWorkSheet->setCellValue('A'.$Row, $title);	    // Rename sheet
	    $Row++;
	    //Write cells
	    $objWorkSheet->setCellValue('A'.$Row, "หมายเลขลูกค้า");
	    $objWorkSheet->setCellValue('B'.$Row, 'กิจกรรม');
	    $objWorkSheet->setCellValue('C'.$Row, 'ชื่อ - สกุล');


	    $objWorkSheet->setCellValue('D'.$Row, 'หมู่บ้าน');
	    $objWorkSheet->setCellValue('E'.$Row, 'บ้านเลขที่');
	    $objWorkSheet->setCellValue('F'.$Row, 'หมู่ที่');


	    $objWorkSheet->setCellValue('G'.$Row, 'ตำบล');
	    $objWorkSheet->setCellValue('H'.$Row, 'อำเภอ');
	    $objWorkSheet->setCellValue('I'.$Row, 'จังหวัด');
	    $objWorkSheet->setCellValue('J'.$Row, "รหัสไปรษณีย์");
	    $objWorkSheet->setCellValue('K'.$Row, 'เบอร์โทรศัพท์');
	    $objWorkSheet->setCellValue('L'.$Row, 'เกรดลูกค้า');
	    $objWorkSheet->setCellValue('M'.$Row, 'เซลล์');
	    $objWorkSheet->setCellValue('N'.$Row, "บริษัท");
	    $objWorkSheet->setCellValue('O'.$Row, 'วันที่ทำกิจกรรม');	   
	    $objWorkSheet->setCellValue('P'.$Row, 'ชุดคำถามมาตรฐาน');	    
	    $objWorkSheet->setCellValue('X'.$Row, 'โทรซ้ำ');
	   
	    $Row++;
	    $objWorkSheet->setCellValue('P'.$Row, "ติดต่อได้");
	    $objWorkSheet->setCellValue('Q'.$Row, 'ติดต่อไม่ได้');
	    $objWorkSheet->setCellValue('R'.$Row, 'สะดวกคุย');
	    $objWorkSheet->setCellValue('S'.$Row, 'ไม่สะดวกคุย');
	    $objWorkSheet->setCellValue('T'.$Row, "สนใจ");
	    $objWorkSheet->setCellValue('U'.$Row, 'ไม่สนใจ');
	    $objWorkSheet->setCellValue('V'.$Row, 'สาเหตุ');
	    $objWorkSheet->setCellValue('W'.$Row, 'หมายเหตุ');
	    $objWorkSheet->setCellValue('X'.$Row, 'วันที่โทรซ้ำ');
	    $objWorkSheet->setCellValue('Y'.$Row, 'เวลาที่โทรซ้ำ');
		return $objWorkSheet;
	}


}//end class

