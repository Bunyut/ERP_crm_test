<?php
require_once dirname(dirname(dirname(__FILE__))).'/ERP_crm/config/connect_db.php';
require_once dirname(dirname(dirname(__FILE__))).'/ERP_crm/function/ic_followcus.php';
require_once dirname(dirname(dirname(__FILE__))).'/ERP_masterdata/inc/main_function.php';

Conn2DB();

class PopUpCRM extends configAllDatabase {

	private $thisDemo = '';

	public function __construct() {

		/* ตรวจสอบหากตั้งเป็น Demo */
		if ($this->dbCh == 'DEMO') {
			$this->thisDemo = 'DEMO/';
		}

		/* เรียกดูการตั้งค่าฐานข้อมูล */
		$this->calConfigAllDatabase();

		/* ตั้งค่า */
		$this->config = $this->con;
		$this->dirCRM = 'ERP_crm';
		$this->pathCRM = dirname(dirname(dirname(__FILE__))).'/'.$this->dirCRM;
		$this->tempPopUpCRM = 'PopUpCRM.html';
		$this->arrPage = array(
			'emp' => 'ic_followcus_show.php?func=cuslist',
			'emps' => 'ic_followcus_show_special.php?func=cuslist',
			'head' => 'ic_followcus2.php?operator=cuslist&listData=LINE&can_self_approve=1',
			'heads' => 'ic_followcus2.php?operator=cuslist&listData=LINE&can_self_approve=2'
		);

		/* เรียกใช้งาน Template */
		if (!class_exists('TplParser')) {
			require_once dirname(dirname(dirname(__FILE__))).'/ERP_masterdata/inc/TplParser.php';
		}
		$this->tpl = new TplParser();
		$this->tpl->setDir($this->pathCRM.'/templates/');
	}

	/*
	| ใช้หา place_id หมายเลข โปรแกรม
	| ==============================================
	| input => ชื่อฐานข้อมูล, ชื่อ directory โปรแกรม
	| output => id ของ โปรแกรม
	*/
	private function getPlace() {
		global $logDb;

		$sql = "SELECT place.id FROM ".$this->config['db_emp'].".place WHERE place.path = '".$this->thisDemo.$this->dirCRM."' AND place.status != '99' LIMIT 1 ";
		$que = $logDb->queryAndLogSQL($sql, " FILE : ".__FILE__." LINE : ".__LINE__);
		$fet = mysql_fetch_assoc($que);

		return $fet['id'];
	}

	/*
	| ใช้หา ตรวจสอบสิทธิ์การเห็น Link menu
	| ==============================================
	| input => place_id = หมายเลช BU, link = link ของเมนู, SESSION ของผู้ล๊อกอิน 
	| output => true = มีสิทธิ์, false = ไม่มีสิทธิ์
	*/
	private function checkPermissionPopUpCRM($place_id, $link) {
		global $logDb;

		$sql = "SELECT 
				menu.id AS menu_id, 
				menu_manage.id AS menu_manage_id, 
				menu_manage.id_card, 
				menu_manage.working_company_id, 
				menu_manage.department_id, 
				menu_manage.section_id,
				menu_manage.position_id
			FROM ".$this->config['db_emp'].".menu 
			LEFT JOIN ".$this->config['db_emp'].".menu_manage ON menu_manage.menu_id = menu.id 
			WHERE menu.place_id = '".$place_id."' 
			AND (
				menu_manage.working_company_id = '".$_SESSION['SESSION_Working_Company']."' 
				OR menu_manage.department_id = '".$_SESSION['SESSION_Department']."' 
				OR menu_manage.section_id = '".$_SESSION['SESSION_Section']."' 
				OR menu_manage.position_id = '".$_SESSION['SESSION_Position_id']."'
				OR menu_manage.id_card = '".$_SESSION['SESSION_ID_card']."'
			)
			AND menu.link LIKE '%".$link."%' 
			AND menu.status != '99' 
			AND menu_manage.status != '99'
			LIMIT 1";
		$que = $logDb->queryAndLogSQL($sql, " FILE : ".__FILE__." LINE : ".__LINE__);
		$fet = mysql_fetch_assoc($que);

		if ($fet['menu_manage_id']) {
			return true;
		}else {
			return false;
		}
	}

	/*
	| ใช้สร้าง ลิ้ง html
	| ==============================================
	| input => type = ประเภทของผู้ใช้งาน, thisCRM = {true=มาจาก CRM, false=มาจาก login}
	| output => list_html = html รายการ
	*/
	private function getLinkPopUp($type, $thisCRM) {
		global $logDb, $arr;

		switch ($type) {
			case 'emp':

				$listNumFollow = getNumFollowCusEventNew($this->dayExpire);
				$list_html = '';

				if (count($listNumFollow) > 0) {

					foreach ($listNumFollow as $key => $arr) {

						if ($this->checkPermissionPopUpCRM($this->getPlace(), $this->arrPage['emp']) || $this->checkPermissionPopUpCRM($this->getPlace(), $this->arrPage['emps'])) {

							if ($thisCRM) {
								$arr['func_emp'] = "browseCheckNew('main', '".$this->arrPage['emp']."', '', function(){ jQuery('input#btn_search_cust').click();});";
							}else {
								$arr['func_emp'] = "window.location='/".$this->thisDemo.$this->dirCRM."?linkFromPopupCRM=emp&place_id=".$this->getPlace()."'";
							}
							
						}else {

							$arr['func_emp'] = "alert('ผู้ใช้งานไม่สามารถเข้าโปรแกรมได้ เนื่องจากไม่เห็นเมนูบันทึกผลการทำกิจกรรม');";
						}

						$list_html.= $this->tpl->tbHtml($this->tempPopUpCRM, 'LIST_EMP_LINK');
					}

				}
				return $list_html;

			break;
			case 'head':

				$command_array = getCommandByPosition($_SESSION['SESSION_Position_id']);
				$list_html = '';
				
				if (!empty($command_array[0])){ 
					$emp_id_card = implode(",",$command_array[0]); 
				}

				if ($emp_id_card){

					$whereCancel = "AND (process_by IN($emp_id_card) OR emp_id_card IN($emp_id_card))";
					$sql = "SELECT id FROM ".$this->config['db_base_name'].".follow_customer WHERE status = '90' $whereCancel GROUP by cus_no,cancel_type";

					$re = $logDb->queryAndLogSQL($sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$arr['NumHead'] = mysql_num_rows($re);

					if ($this->checkPermissionPopUpCRM($this->getPlace(), $this->arrPage['head']) || $this->checkPermissionPopUpCRM($this->getPlace(), $this->arrPage['heads'])) {

						if ($thisCRM) {
							$arr['func_head'] = "browseCheckNew('main', '".$this->arrPage['head']."', '', function(){browseCheck('follow_content','ic_followcus2.php?operator=wait_for_approve','');hideObj('divMainDetail,cursor');});";
						}else {
							$arr['func_head'] = "window.location='/".$this->thisDemo.$this->dirCRM."?linkFromPopupCRM=head&place_id=".$this->getPlace()."'";
						}

					}else {
						$arr['func_head'] = "alert('ผู้ใช้งานไม่สามารถเข้าโปรแกรมได้ เนื่องจากไม่เห็นเมนูบันทึกผลการทำกิจกรรม');";
					}

					$list_html = $this->tpl->tbHtml($this->tempPopUpCRM, 'LIST_HEAD_LINK');
				}
				
				return $list_html;

			break;
		}

	}

	private function getDayExpire() {
		global $logDb;

		$sql = "SELECT CAST(k_valid.validdata AS UNSIGNED) AS DAY 
			FROM ".$this->config['db_base_name'].".k_valid 
			WHERE k_valid.validtype = 'POPUP' AND k_valid.validstatus = '1' ORDER BY k_valid.validkeyid DESC LIMIT 1";
	
		$que = $logDb->queryAndLogSQL($sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		if (mysql_num_rows($que) > 0) {
			$fet = mysql_fetch_assoc($que);
		}else{
			$fet['DAY'] = 0;
		}

		return $fet['DAY'];
	}

	public function createPopUp($thisCRM=true) {
		global $link_emp, $link_head, $numDayConfig;

		$this->dayExpire = $this->getDayExpire();

		$link_emp = $this->getLinkPopUp('emp', $thisCRM);
		$link_head = $this->getLinkPopUp('head', $thisCRM);

		$numDayConfig = $this->dayExpire;

		if (!empty($link_emp) || !empty($link_head)) {

			if (isset($_GET['linkFromPopupCRM'])) {
				echo "<script>";
				switch ($_GET['linkFromPopupCRM']) {
					case 'emp':
						echo "browseCheckNew('main', '".$this->arrPage['emp']."', '', function(){ jQuery('input#btn_search_cust').click();});";
					break;
					case 'head':
						echo "browseCheckNew('main', '".$this->arrPage['head']."', '', function(){browseCheck('follow_content','ic_followcus2.php?operator=wait_for_approve','');hideObj('divMainDetail,cursor');});";
					break;
				}
				echo "</script>";
			}else {
				echo $this->tpl->tbHtml($this->tempPopUpCRM, 'MAIN_PAGE');
			}

		}
	}

}
?>