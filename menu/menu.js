var arrowimages={down:['downarrowclass', 'down.gif', 23], right:['rightarrowclass', 'images/arrow-right.gif']}

var jqueryslidemenu={
	buildmenu:function(arrowsvar){
		$(document).ready(function($){
			/*$('div.ui-widget-overlay').mouseover(function() {
			alert('Y');
				$('div.ui-widget-overlay').css('display','none');
			});*/
		
			$('ul.menu li').each(function(){
				var $li = $(this);
				$li.children("a:eq(0)").click(function(){
					/*if($li.has('div')){
					$('ul.menu li div:visible').not($li).css('visibility','hidden');
					}*/
					this.istopheader = $li.parents("ul:eq(0)").not("ul.menu").length==1? true : false
					this.istopheader2 = $li.has("div").length==1? true : false

					if(this.istopheader==true && this.istopheader2==false){
						$li.children("div:eq(0)").css('visibility','hidden');
					}else{
						$('ul.menu li div:visible').css('visibility','hidden');
					}
					
					$li.parents('div:eq(0)').css('visibility','visible');
					$li.children("div:eq(0)").css('visibility','visible');
				});

				$('div#menu').mouseleave(function(){
					//$('ul.menu li div:visible').css('visibility','hidden');
					$li.children("div:eq(0)").css('visibility','hidden');
				});
				
				this.istopheader = $li.parents("ul").not("ul.menu").length==1? true : false
				this.istopheader2 = $li.children("div").length==1? true : false
				if(this.istopheader==true && this.istopheader2==true){ 
					$li.children("a:eq(0)").append(
						'<img src="'+ (arrowsvar.right[1])
						+'" class="' + (arrowsvar.right[0])
						+ '" style="border:0;" />'
					); 
				}
			});
		});
	}
}

var loop = 1;
var jqueryresizemenu={
    resizemenu:function(){
		$(document).ready(function($){
			var slideLeft = 0;
			var divPo = 0,divPop = 0,allWidth = 0,allWidths = 0;

			$('div#menu div div').each(function(){
				divPo = +$(this).offset().left + +$(this).width();

				if(divPo > $('div#menu').width()){
					slideLeft = +$(this).width() + +$(this).find('li').eq(0).width() - 8;
               //alert(slideLeft+":"+$(this).width()+":"+$(this).find('li').eq(0).width());

               $(this).animate({"left": "-="+slideLeft+"px"}, "fast");
            }
			});
			
			allWidth = 0;
			//alert(allWidth);
			$('ul.menu').children('li').each(function(){
				//allWidth = allWidth +$(this).width()+$(this).outerWidth();
				allWidth = allWidth + $(this).width();
				//alert(allWidth+':'+$(this).width());
			});
			allWidth = parseFloat(allWidth);
			
			allWidths = 0;
			$('div#menu').children('ul').children('li').children('div').each(function(){
				//alert($(this).width());
				divPop = $(this).offset().left + $(this).width();			
				//alert(divPop+' > '+$('div#menu').width());
				if(divPop > $('div#menu').width()){
					if( divPop > allWidths ){ allWidths = divPop; }
				}
			});
			allWidths = parseFloat(allWidths);			
			
			//alert($('div#menu').width()+'<'+allWidth+':'+allWidths);
			if($('div#menu').width()<allWidth || $('div#menu').width()<allWidths){
				if(loop==1){
					$('link[href="menu/menu.css"]').animate({
						'rel': 'alternate stylesheet'
					},{ 
						duration: 0,
						complete: function() {
							
						}
					});
					$('link[href="menu2/menu.css"]').animate({
						'rel': 'stylesheet'
					},{ 
						//duration: 'slow',
						duration: 0,
						complete: function() {
							//alert(allWidth);
							loop++;
							jqueryresizemenu.resizemenu();
						}
					});
					//$('link[href="menu/menu.css"]').attr('rel','alternate stylesheet'); 
					//$('link[href="menu2/menu.css"]').attr('rel','stylesheet');
					
					//loop++;
					//jqueryresizemenu.resizemenu();
				}else{
					//$('div#menu').width(allWidth);
					//alert(allWidths);
					
					
					
					//jqueryscrollmenu.scrollmenu();  ให้ menu scroll ตามหน้า page
		
					if(allWidth<allWidths){
						$('div#menu').width(allWidths);
					}else{
						$('div#menu').width(allWidth);
					}
				}
				//res = jqueryanimate.animatemenu(1);
			}	
		});
    }
}

var jqueryscrollmenu={
	scrollmenu:function(){
    	$(document).ready(function($){
    		/* $(window).scroll(function() {
				$('div#menu').animate({ 
					'top': window.pageYOffset-1
				},0);
			}); */
		});
   }
}
/*var count =0;
var jqueryanimate={
    animatemenu:function(opt){
		$(document).ready(function($){
		
			switch(opt){
				case 1:
					$('div#menu').animate({
						'height': '-=2'
					},{ 
						duration: "fast", 
						complete: function() {
							jqueryanimate.animatemenu(2);
						}
					});
				break;
				case 2:
					$('div#menu li div').animate({
						'top': '-=1',
						'min-width': '-=1',
						'padding-left': '-=1',
						'padding-right': '-=1',
						'margin-right':'-=1'
					},{ 
						duration: "fast", 
						complete: function() {
							//alert(count);
							jqueryanimate.animatemenu(3);
						}
					});
				break;
				case 3:
					$('div#menu a').animate({
						'font-size': '-=1',
						'height': '-=2',
						'line-height': '-=2',
						'padding-bottom': '-=1',
						'padding-right': '-=1'
					},{ 
						duration: "fast", 
						complete: function() {
							//alert('3');
							//jqueryanimate.animatemenu(4);
						}
					});
				break;
				case 4:
					$('div#menu span').animate({
						'font-size': '-=1',
						'padding-top': '-=1'
					},{ 
						duration: "fast", 
						complete: function() {
							jqueryanimate.animatemenu(5);
						}
					});
				break;
				case 5:
					$('div#menu li.last span').animate({
						'padding-top': '-=1',
						'padding-bottom': '-=1',
						'padding-left': '-=1'
					},{ 
						duration: "fast", 
						complete: function() {
							jqueryanimate.animatemenu(6);
						}
					});
				break;
				case 6:
					$('div#menu ul ul li').animate({
						'padding-right': '-=1',
						'padding-left': '-=1'
					},{ 
						duration: "fast", 
						complete: function() {
							jqueryanimate.animatemenu(7);
						}
					});
				break;
				case 7:
					$('div#menu ul ul').animate({
						'padding-right': '-=1',
						'padding-left': '-=1'
					},{ 
						duration: "fast", 
						complete: function() {
							jqueryanimate.animatemenu(8);
						}
					});
				break;
				case 8:
					$('div#menu ul ul a').animate({
						'padding-bottom': '-=1',
						'padding-right': '-=1',
						'padding-left': '-=1',
						'margin-bottom':'-=1'
					},{ 
						duration: "fast", 
						complete: function() {
							jqueryanimate.animatemenu(9);
						}
					});
				break;
				case 9:
					$('div#menu ul ul a').animate({
						'padding-bottom': '-=1',
						'padding-right': '-=1',
						'line-height': '-=2',
						'font-size':'-=1'
					},{ 
						duration: "fast", 
						complete: function() {
							jqueryanimate.animatemenu(10);
						}
					});
				break;
				case 10:
					$('.rightarrowclass').animate({
						'top': '-=1',
						'right': '-=1'
					},{ 
						duration: "fast", 
						complete: function() {
							jqueryanimate.animatemenu(11);
						}
					});
				break;
				case 11:
					$('div#menu ul.menu').animate({
						'padding-left': '-=2'
					},{ 
						duration: "fast", 
						complete: function() {
							//jqueryresizemenu.resizemenu(); //end
						}
					});
				break;
			}		
		});
    }
}*/

jqueryslidemenu.buildmenu(arrowimages)
jqueryresizemenu.resizemenu()