<?php

require_once("config/general.php");
require_once("config/ic_followcus.php");
require_once("function/general.php");
require_once("function/ic_followcus.php");
require_once('function/ic_eventcus.php');
require_once("function/create_ssi_score.php");

require_once('inc/Thailand.php'); //pt@201-01-26
require_once("helper/dbquery.php");

require_once("classes/PreLoaderMultiAjax.class.php");
require_once("classes/pLoadmore.class.php");
#echo '<pre>'; print_r($_SESSION); echo '</pre>';

if($_SESSION['customers_all'] == 'N'){
	//$view_detail_cuss = 'customers_sale.php';
	$view_detail_cuss = $config['custome_normal'];
}else{
	//$view_detail_cuss = 'customers_all.php';
	$view_detail_cuss = $config['custome_all'];
}


//ส่วนของหัวหน้า sale , รอสัมภาษณ์ , รออนุมัติยกเลิกการติดตาม
/****************************
* css = css/followcus.css   *
* 	    css/nz_button.css	*
* js  = java/followcus.js   *
****************************/

## สั่งทำงานโค้ด
$doCode = ""; 		## ทำงาน
// $doCode = "No";  ## ไม่ทำงาน แสดงโค้ด Insert, Update, Delete
// $doCode = "All"; ## ทำงาน แสดงโค้ด Select, Insert, Update, Delete

Conn2DB();

$operator = $_GET['operator'].$_POST['operator'];

## กำหนดสิทธิว่าให้แสดงแบบไหน
## ตาม สายงาน = LINE
## ตาม OU = OU
if($_GET['listData']){
	$_SESSION['listData'] = $_GET['listData'];
}

if($_SESSION['listData']!='LINE' && $_SESSION['listData']!='OU'){
	echo '<script type="text/javascript">alert(\'เกิดข้อผิดพลาด :: กรุณาเข้าเมนูใหม่อีกครั้ง\'); ';
	exit();
}

if(isset($_GET['can_self_approve'])){
	$_SESSION['can_self_approve'] = $_GET['can_self_approve'];
}

## End กำหนดสิทธิว่าให้แสดงแบบไหน

/*$arr	= 	get_permission();
$CheckManager			=	$arr['check_manager'];  		// EX
$CheckLevel				=	$arr['check_level'];
$CheckSale				=	$arr['check_sale'];
$CheckDepartment		=	$arr['check_department'];
$checkEventDepartment	= 	$arr['event_department'];*/

//fix cut department for sale
if($CheckSale=="1"){
	$checkEventDepartment = "1";
}

$followPage = "ic_followcus2.php";
$followHTML = "ic_followcus2.html";

#-- ตั้งค่าให้ปุ่ม back
$div_back = 'main';
if($_GET['div_back']){$div_back=$_GET['div_back'];}

//นับ #ZAN@2012-03-01 เพิ่ม POSITION CODE แล้ว
getNumFollowCusEvent(); //จะนับจำนวน $_SESSION[numFollow]และสุ่มรายการใหม่

//สัมภาษณ์ ได้เซสชั่นรายการรอสัมภาษณ์ จากการสุ่มตอนล็อกอิน
if ($_SESSION['checkInterview'] == "wait") { //getNumInterviewEvent() ใน function/ic_followcus.php
	$_SESSION[numInterview] = getNumInterviewEvent(date('Y-m-d'));
	$interviewMenu = "<a href=\"javascript:void(0)\" class='nz_button BTwhite' onclick=\"javascript:browseCheck('follow_content','$followPage?operator=wait','');\">สัมภาษณ์เหตุการณ์ลูกค้า (<span id='wait'>$_SESSION[numInterview]</span>)</a> ";
}

$menu = "<a href=\"javascript:void(0)\" class='nz_button BTwhite' onclick=\"javascript:browseCheck('follow_content','$followPage?operator=getAllCustomer&id_card=$_SESSION[SESSION_ID_card]','');\">รายชื่อลูกค้าที่ดูแลทั้งหมด</a> ";
$menu .= "<a href=\"javascript:void(0)\" class='nz_button BTwhite' onclick=\"javascript:browseCheck('main','$followPage?operator=cuslist','');\">รายชื่อลูกค้าที่ต้องโทรหา ($_SESSION[numFollow])</a> ";

//$emp_id_card = getEmp_follower($_SESSION['SESSION_ID_card']); //function/general.php
# BEN Add 2013-02-06 DEV10534 
# == กรณี จะแยกเป็นสองหน้า เห็นผู้ล็อกอินเอง แยกโดย ใส่ parameter ตัวที่สอง ให้กับฟังก์ชั่น getCommandByPosition 
#== สถานะ ดังนี้  1 ไม่แสดงข้อมูลผู้ล็อกอิน 2 แสดงข้อมูลผู้ล็อกอิน
$command_array = getCommandByPosition($_SESSION['SESSION_Position_id'],$_SESSION['can_self_approve']);

$emp_id_card = implode(",",$command_array[0]);
$emp_pos_id = implode(",",$command_array[1]);

if(!empty($emp_id_card)){
	// echo "emp_id_card :: ".$emp_id_card."<br/>";
	$whereCancel = "AND (process_by IN($emp_id_card) OR emp_id_card IN($emp_id_card))";
	$sql = "SELECT id FROM $config[db_base_name].follow_customer WHERE status = '90' $whereCancel GROUP by cus_no,cancel_type";
	// echo $sql."<br/>";
	$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	$approve_num 	= mysql_num_rows($re);
	# P'Add 2012-11-28 DEV10534 
	$arr_id_card 	= explode(",", $emp_id_card);
	// $arr_id_card = get_underling_position();

	$numEmp 		= count($arr_id_card);
}else{
	$approve_num	= 0;
	$numEmp 		= 0;
}
# End P'Add 2012-11-28 DEV10534 

$saleSuperMenu = "<a href=\"javascript:void(0)\" id='bt_super' class='nz_button BTwhite' onclick=\"javascript:$js_progress;browseCheck('follow_content','$followPage?operator=emplist','');hideObj('divMainDetail,cursor');\">รายชื่อพนักงานในความรับผิดชอบ ($numEmp)</a> ";
$saleSuperMenu .= "<a href=\"javascript:void(0)\" id='bt_wait_approve' class='nz_button BTwhite' onclick=\"javascript:browseCheck('follow_content','$followPage?operator=wait_for_approve','');hideObj('divMainDetail,cursor');\">รออนุมัติยกเลิกการติดตาม (<span id='approve_num'>$approve_num</span>)</a> ";

$_SESSION['follow_menu'] = $menu.$saleSuperMenu.$interviewMenu;
$menu = $_SESSION['follow_menu'];

$table =null;
$tr = null;

switch($operator){

	case 'cuslist':

		$onchange 		= "followPostData('main','$followPage?operator=cuslist$order_view_detail','order='+document.getElementById('order').value + '&order_event='+document.getElementById('order_event').value + '&order_province='+document.getElementById('order_province').value + '&order_aumphur='+document.getElementById('order_aumphur').value + '&order_tumbon='+document.getElementById('order_tumbon').value );";
		$onchange_pro 	= "followPostData('main','$followPage?operator=cuslist$order_view_detail','order='+document.getElementById('order').value + '&order_event='+document.getElementById('order_event').value + '&order_province='+document.getElementById('order_province').value );";
		$onclick_excel 	= "javascript:window.open('$followPage?operator=cuslist_progress$order_view_detail&cuslist_excel=1&id_card=$_GET[id_card]&view_de=$_GET[view_de]&order='+document.getElementById('order').value + '&order_event='+document.getElementById('order_event').value + '&order_province='+document.getElementById('order_province').value + '&order_aumphur='+document.getElementById('order_aumphur').value + '&order_tumbon='+document.getElementById('order_tumbon').value  +'&regis_no='+document.getElementById('regis_no').value  + '&machine_no='+document.getElementById('machine_no').value  + '&start_car_out='+document.getElementById('start_car_out').value  + '&end_car_out='+document.getElementById('end_car_out').value  + '&tel_no='+document.getElementById('tel_no').value  + '&start_regis='+document.getElementById('start_regis').value  + '&end_regis='+document.getElementById('end_regis').value );";
		
		$styleHidden = 'none';

		if($cus_list == ""){
			$colspan  	 = 10;
			$cus_list 	 = $tpl->tbHtml($followHTML, 'NULL');
		}

		$break_up_menu 	= "<img src='img/break_up2.png' align='absmiddle' width='25' height='25' title='ยุติการติดตาม'> = ยกเลิกรายการติดตาม |";

		$popup_block 	= $tpl->tbHtml($followHTML, 'POPUP_BLOCK');

		//ถ้าเป็นหน้าโทรหาลูกค้าปกติ ถึงจะ โชว์แถบกรอง เช่นถ้าเป็นหน้ากิจกรรมพิเศษ จะส่งรหัสกิจกรรมมาโดยเฉาะ ก็จะไม่แสดงแถบกรอง
		if($_GET['select_event']){
			$fornormalmode 	= 'none'; $textEvent = $textPro = $textAmp = $textTum = '';
		}else{
			$fornormalmode 	= '';
			$textEvent 		= 'กิจกรรม: ';
			$textPro 		= 'จังหวัด: ';
			$textAmp 		= 'อำเภอ: ';
			$textTum 		= 'ตำบล: ';
			$textEx  		= 'ออกรายงาน: ';
		}

		//เลือกจังหวัด
		$op_province = GETProvince($_POST['order_province'],null);


		$today 	   	= date('Y-m-d');
		$tb_follow 	= "$config[db_base_name].follow_customer";
		$tb_event  	= "$config[db_base_name].follow_main_event";

		$id_card = $_GET['id_card'];
		$view_de = $_GET['view_de'];
		
		if ($id_card) { //ถ้าส่งค่ามาจากหน้ารายชื่อพนักงานของ Super Manager Admin
			$emp = select_emp_nametonickname($id_card);
			$back_display = '';
			if ($_GET['back']) { //ถ้าส่งมาจากหน้า รายชื่อพนักงานในความรับผิดชอบ จะเก็บ session ใหม่
				$_SESSION['back_emplist'] = $_GET['back'];
				$_SESSION['anchor_emplist'] = $_GET['anchor'];
			}
			
			$empOu = select_empOu($id_card); //function/general.php
			
			$emp_response 	= "<div align='left' style='width:100%;padding:10px;color:green;background:#ffffff' class='title3'>";
			$emp_response	.= "รายการที่กำลังติดตามอยู่ในขณะนี้ของ ::  <font color='orangered'>$emp</font> :: [พนักงานผู้ดูแลลูกค้า] <a href=\"javascript:void(0)\" class='nz_button BTorange' style='color:#ffffff' onclick=\"javascript:browseCheck('follow_content','$followPage?operator=getAllCustomer&id_card=$id_card&mode=forward&back=$followPage?operator=cuslist','');\">รายชื่อลูกค้าทั้งหมด</a> </div>";
			$backID 		= "id_card=".$id_card;
			$onchange 		= "followPostData('main','$followPage?operator=cuslist&$backID','order='+document.getElementById('order').value);";
			$leader 		= "leader=yes";
		}
	
		## id_card เลือกมาจากผู้รับผิดชอบลูกค้า
		if ($id_card == "") { //ถ้าเข้าดูรายการของตัวเอง
			if($_GET['select_idEmp']){//pt@2012-01-04
				$id_card = parseInt($_GET['select_idEmp']);
				$empOu = select_empOu($id_card); //function/general.php
			}else{
				$id_card = $_SESSION['SESSION_ID_card'];
				$empOu = select_empOu($id_card,'session'); //function/general.php
			}
			//end pt@2012-01-04
			$back_display = 'none';
		}

		## ถ้าเลือกแบบสายงาน
		if($view_de == 'flow'){
			unset($empOu['id_card']);
		## แบบอื่นๆ ที่ไม่ใช่สายงาน
		}else if($view_de == 'resperson'){
			unset($empOu['position']);
			unset($empOu['section']);
			unset($empOu['department']);
			unset($empOu['company']);
		}
		if($view_de == ''){
			$id_card = '';
		}
		# Check CR
		//if( $checkEventDepartment == $config['cr_department'] ){
		//เข้าฟังชั่น เรียงเงื่อนไข select ข้อมูลที่จะแสดง
		$whereOu = condition_sortOU($empOu,'cus'); //function/general.php
		//ดึงมาเฉพาะเหตุการณ์รอสัมภาษณ์ของ CR pattern_id_ref ต้องเป็นค่าว่าง
		$sql = "SELECT event.id,event.event_title,event.pattern_id_ref FROM $config[db_base_name].follow_main_event event LEFT JOIN ";
		$sql .= "$config[db_base_name].follow_customer cus ON event.id=cus.event_id_ref WHERE $whereOu AND event.status!= '99' AND event.visible != 'no' AND ";
		$sql .= "cus.status!= '99' GROUP BY event.id ORDER BY event.event_title ASC ";
		#echo '<br><br>'.$sql;
		$arr_opt = array(
				'value_field'	=> 'id', 						// ฟิลด์ที่จะใช้เป็น value ของ option
				'text_field'	=> 'event_title',				// ฟิลด์ที่จะใช้เป็น text ของ option
				'select_value' 	=> $_GET['select_event'],		// ตัวเลือกเริ่มต้น
		);
		// pt 2011-11-25
		$event_dropdown  = get_event_dropdowns($sql, $arr_opt );
		$select_event_cr = $event_dropdown['dropdown'];
		//exit();
			
		//สาขา ให้ CR เลือก
		$arr_comp = array(
			'field_value' => 'id',
			'selected'    => $_GET['s_branch']
		);
		$select_branch_cr = get_company_option($arr_comp);     # -- เลือกบริษัท

		//end CR
		$whereOu = condition_sortOU($empOu,$tb_follow); //function/general.php

		//เลือกEvent
		$sql_e = " SELECT  $tb_event.event_title, $tb_event.id";
		$sql_e .= " FROM $tb_follow LEFT JOIN $tb_event ON $tb_follow.event_id_ref=$tb_event.id  $joinSell_b";
		$sql_e .= " WHERE $whereOu $other_where $other_where_b AND ";
		$sql_e .= "(start_date <= '$today' AND stop_date >= '$today') AND $tb_follow.status ='1' AND $tb_event.visible!='no' GROUP by event_id_ref";
		$res_e = $logDb->queryAndLogSQL( $sql_e, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while($fet_e = mysql_fetch_assoc($res_e)){
			if($_POST['order_event'] == $fet_e['id']){
				$selected_event = 'selected';
			}else {
				$selected_event = '';
			}
			$GETEvent .='<option '.$selected_event.' value="'.$fet_e['id'] .'" >'.$fet_e['event_title'] .'</option>';
		}
		echo $tpl->tbHtml($followHTML, 'MAIN_FOLLOW');
	break;

	case 'cuslist_progress':
		$response  		= array();
		if(!$_GET['cuslist_excel']){
			$obj_pre 		= new PreLoaderMultiAjax();
		}
		// echo '<pre>';print_r($_GET);echo'</pre>';exit();
		$special 	= $_GET['special'];

		$cusName		= "CONVERT(cus_name USING TIS620)";
		$filter_other 	= '';
		
		//pt@2012-01-26  กำหนดตัวแปรที่ส่งมา
		if($_GET['order']){ 
			$_POST['order'] 			= $_GET['order'];
		}
		if($_GET['order_event']){
			$_POST['order_event'] 		= $_GET['order_event'];
		}
		if($_GET['order_province']){
			$expPro0 				= explode("_",$_GET['order_province']);
			$_GET['order_province'] = $expPro0[1];
			$filter_other .= "AND IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_PROVINCE,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_PROVINCE,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_PROVINCE,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1),'' ))) = '$_GET[order_province]' ";

		}
		if($_GET['order_aumphur']){ 
			$expAmp0 				= explode("_",$_GET['order_aumphur']);
			$_GET['order_aumphur']  = $expAmp0[1];
			$filter_other .= "AND IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1),'' ))) = '$_GET[order_aumphur]' ";
		}	
		if($_GET['order_tumbon']){ 
			$expTum0 				= explode("_",$_GET['order_tumbon']);
			$_GET['order_tumbon'] 	= $expTum0[1];
			$filter_other .= "AND IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_SUB_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_SUB_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_SUB_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1),'' ))) = '$_GET[order_tumbon]' ";

		}
		// LAST EDIT 2012-11-27 BY KEN
		if($_GET['cust_name']){ 
			$_POST['cust_name'] 		= $_GET['cust_name'];
		}	
		if($_GET['cust_lastname']){
			$_POST['cust_lastname'] 	= $_GET['cust_lastname'];
		}
		if($_GET['s_branch']){ 
			$_POST['s_branch'] 			= $_GET['s_branch'];
			//$_GET['select_event']		= $_GET['s_branch'];
		}	
			// ---------------------------------------------------------
		if ($_GET['regis_no']) {  // กรองตามเลขทะเบียน
			$filter_other .= "AND follow_customer.chassi_no_s = (SELECT VEHICLE_ID FROM $config[db_vehicle].VEHICLE_INFO WHERE VEHICLE_ID = follow_customer.chassi_no_s AND VEHICLE_REGIS LIKE '%$_GET[regis_no]%' LIMIT 1) ";
		}
		if ($_GET['machine_no']) {  // กรองตามหมายเลขเครื่อง
			$filter_other .= "AND IF((SELECT COUNT(*) FROM $config[db_vehicle].VEHICLE_INFO WHERE VEHICLE_ID = follow_customer.chassi_no_s AND VEHICLE_ENGINES LIKE '%$_GET[machine_no]%' LIMIT 1) > 0,follow_customer.chassi_no_s = (SELECT VEHICLE_ID FROM $config[db_vehicle].VEHICLE_INFO WHERE VEHICLE_ID = follow_customer.chassi_no_s AND VEHICLE_ENGINES LIKE '%$_GET[machine_no]%' LIMIT 1),follow_customer.chassi_no_s = (SELECT VEHICLE_ID FROM $config[db_vehicle].VEHICLE_INFO WHERE VEHICLE_ID = follow_customer.chassi_no_s AND VEHICLE_FULL_ENGINES LIKE '%$_GET[machine_no]%' LIMIT 1)) ";
		}
		if ($_GET['start_car_out'] && $_GET['end_car_out']) { // กรองตามช่วงวันที่ออกรถ

			/* แก้ไขรูปแบบวันที่ เนื่องจากใน table เก็บเป็น คศ */
			$arr_tmp = explode('-', $_GET['start_car_out']);
			$_GET['start_car_out'] = intval($arr_tmp[2]-543)."-".$arr_tmp[1]."-".$arr_tmp[0];

			$arr_tmp = explode('-', $_GET['end_car_out']);
			$_GET['end_car_out'] = intval($arr_tmp[2]-543)."-".$arr_tmp[1]."-".$arr_tmp[0];

			$filter_other .= "AND follow_customer.chassi_no_s = (SELECT VEHICLE_ID FROM $config[db_vehicle].VEHICLE_INFO WHERE VEHICLE_ID = follow_customer.chassi_no_s AND (Date_Deliver >= '$_GET[start_car_out]' AND Date_Deliver <= '$_GET[end_car_out]')  LIMIT 1) ";
		}
		if ($_GET['tel_no']) {  // กรองตามหมายเลขโทรศัพท์
			$filter_other .= "AND IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_TELEPHONE WHERE $config[db_maincus].MAIN_TELEPHONE.TEL_CUS_NO = follow_customer.cus_no AND $config[db_maincus].MAIN_TELEPHONE.TEL_TYPE = '1' AND TEL_NUM LIKE '%$_GET[tel_no]%' LIMIT 1) > 0 ,'1=1',IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_TELEPHONE WHERE $config[db_maincus].MAIN_TELEPHONE.TEL_CUS_NO = follow_customer.cus_no AND $config[db_maincus].MAIN_TELEPHONE.TEL_TYPE = '2' AND TEL_NUM LIKE '%$_GET[tel_no]%' LIMIT 1) > 0 ,'1=1',IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_TELEPHONE WHERE $config[db_maincus].MAIN_TELEPHONE.TEL_CUS_NO = follow_customer.cus_no AND $config[db_maincus].MAIN_TELEPHONE.TEL_TYPE = '3' AND TEL_NUM LIKE '%$_GET[tel_no]%' LIMIT 1) > 0,'1=1',''))) ";
		}
		if ($_GET['start_regis'] && $_GET['end_regis']) {  // กรองตามช่วงวันที่จดทะเบียน

			/* แก้ไขรูปแบบวันที่ เนื่องจากใน table เก็บเป็น คศ */
			$arr_tmp = explode('-', $_GET['start_regis']);
			$_GET['start_regis'] = intval($arr_tmp[2]-543)."-".$arr_tmp[1]."-".$arr_tmp[0];

			$arr_tmp = explode('-', $_GET['end_regis']);
			$_GET['end_regis'] = intval($arr_tmp[2]-543)."-".$arr_tmp[1]."-".$arr_tmp[0];

			$filter_other .= "AND follow_customer.chassi_no_s = (SELECT REGIS_VEHICLE_ID FROM $config[db_vehicle].VEHICLE_REGIS WHERE REGIS_VEHICLE_ID = follow_customer.chassi_no_s AND (REGIS_DATE >= '$_GET[start_regis]' AND REGIS_DATE <= '$_GET[end_regis]')  LIMIT 1) ";
		}
		// ---------------------------------------------------------

		if ($_POST['order']) { //การจัดเรียง
			$_SESSION["SESSION_order"]  = $_POST['order'];
		}else{
			unset($_SESSION["SESSION_order"]);
		}
		
		//switch ($_SESSION["SESSION_order"]) {
		switch ($_POST['order']) {
			case "cusname_asc":
				$order 		= "$cusName ,stop_date";
				$select1 	= "selected='selected'";
				break;
			case "cusname_desc":
				$order 		= "$cusName DESC,stop_date";
				$select2 	= "selected='selected'";
				break;
			case "expire_date_asc":
				$order 		= "stop_date,$cusName";
				$select3 	= "selected='selected'";
				break;
			case "expire_date_desc":
				$order 		= "stop_date DESC,$cusName";
				$select4 	= "selected='selected'";
				break;
			default:
				$order 		= "stop_date,$cusName";
				break; //default = เรียงตามชื่อลูกค้า ก - ฮ
		}

		$id_card = $_GET['id_card'];
		$view_de = $_GET['view_de'];


		if ($id_card) { //ถ้าส่งค่ามาจากหน้ารายชื่อพนักงานของ Super Manager Admin
			$emp = select_emp_nametonickname($id_card);
			$back_display = '';
			if ($_GET['back']) { //ถ้าส่งมาจากหน้า รายชื่อพนักงานในความรับผิดชอบ จะเก็บ session ใหม่
				$_SESSION['back_emplist'] 	= $_GET['back'];
				$_SESSION['anchor_emplist'] = $_GET['anchor'];
			}
			
			$empOu = select_empOu($id_card); //function/general.php
			
			$emp_response 	= "<div align='left' style='width:100%;padding:10px;color:green;background:#ffffff' class='title3'>";
			$emp_response  .= "รายการที่กำลังติดตามอยู่ในขณะนี้ของ ::  <font color='orangered'>$emp</font> :: [พนักงานผู้ดูแลลูกค้า] <a href=\"javascript:void(0)\" class='nz_button BTorange' style='color:#ffffff' onclick=\"javascript:browseCheck('follow_content','$followPage?operator=getAllCustomer&id_card=$id_card&mode=forward&back=$followPage?operator=cuslist','');\">รายชื่อลูกค้าทั้งหมด</a> </div>";
			$backID 		= "id_card=".$id_card;
			$onchange 		= "followPostData('main','$followPage?operator=cuslist&$backID','order='+document.getElementById('order').value);";
			$leader 		= "leader=yes";
			//$check_super_enter = true;
		}
	
		if ($id_card == "") { //ถ้าเข้าดูรายการของตัวเอง

			if($_GET['select_idEmp']){//pt@2012-01-04
				$id_card = parseInt($_GET['select_idEmp']);
				$empOu 	 = select_empOu($id_card); //function/general.php
			}else if($_GET["id_card"]){ // รับจากหน้ารายชื่อพนักงานในความรับผิดชอบ (2015-12-29)
				$id_card = parseInt($_GET['id_card']);
				$empOu 	 = select_empOu($id_card); //function/general.php
			}else{
				$id_card = $_SESSION['SESSION_ID_card'];
				$empOu   = select_empOu($id_card,'session'); //function/general.php
			}
	
			//end pt@2012-01-04
			$back_display = 'none';
		}
		// echo $id_card;exit();

		## ถ้าเลือกแบบสายงาน
		$row_flow  = array();
		$whereFlow = $wherePerson = '';
		if($view_de == 'flow'){
			unset($empOu['id_card']);

			$sql_flow = "SELECT id,follow_up_activities  FROM $config[db_base_name].follow_main_event WHERE follow_up_activities = '2' ";
			$sql_flow = mysql_query($sql_flow);
			while($flow = mysql_fetch_assoc($sql_flow)){
				$row_flow[] = $flow['id']; 
			}

			$txtImplode = implode(",",array_filter($row_flow));
			$whereFlow = "AND event_id_ref IN(".$txtImplode.") AND (process_by = '' OR process_by = '-')  ";

		## แบบอื่นๆ ที่ไม่ใช่สายงาน
		}else if($view_de == 'resperson'){
			unset($empOu['position']);
			unset($empOu['section']);
			unset($empOu['department']);
			unset($empOu['company']);

			$wherePerson =  "AND ($config[db_base_name].follow_customer.process_by =".$id_card." OR emp_id_card = ".$id_card.")";
		}
		// echo $whereFlow.'<br>';
		// echo $whereFlow;exit();
		$whereOu = condition_sortOU($empOu,'cus'); //function/general.php
		
		//ดึงมาเฉพาะเหตุการณ์รอสัมภาษณ์ของ CR pattern_id_ref ต้องเป็นค่าว่าง
		$sql  = "SELECT event.id,event.event_title,event.pattern_id_ref ";
		$sql .= "FROM $config[db_base_name].follow_main_event event LEFT JOIN ";
		$sql .= "$config[db_base_name].follow_customer cus ON event.id=cus.event_id_ref ";
		$sql .= "WHERE $whereOu AND event.status!= '99' AND event.visible != 'no' AND ";
		$sql .= "cus.status= '1' GROUP BY event.id ORDER BY event.event_title ASC";
		
		$arr_opt = array(
				'value_field'	=> 'id', 						// ฟิลด์ที่จะใช้เป็น value ของ option
				'text_field'	=> 'event_title',				// ฟิลด์ที่จะใช้เป็น text ของ option
				'select_value'	=> $_GET['select_event'],		// ตัวเลือกเริ่มต้น
		);
		// pt 2011-11-25
		$event_dropdown = get_event_dropdowns($sql, $arr_opt );

		$arr_comp = array(
			'field_value' => 'id',
			'selected'    => $_GET['s_branch']
		);
		$branch_cr = get_company_option($arr_comp);     # -- เลือกบริษัท


		if($_GET['select_event']){//กรองชื่อลูกค้าตาม Event

			$other_where = " AND event_id_ref = '$_GET[select_event]' ";
		}
		//กรองตามชื่อสาขา
		if($_GET['s_branch'] && $_GET['s_branch']!='all'){//กรองชื่อลูกค้าตาม Event
			if(!empty($event_dropdown['pattern_id_ref'])){
				foreach($event_dropdown['pattern_id_ref'] AS $key => $value){
					$idEvent = $event_dropdown['id'][$key];
					
					$sqlP  = "SELECT field_name,field_cus_name,field_vehicle_name,table_name, ";
					$sqlP .= "db_name,field_branch FROM $config[db_base_name].follow_field_pattern ";
					$sqlP .= "WHERE id='".$value."' LIMIT 1";
					$queP  = $logDb->queryAndLogSQL( $sqlP, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					$feP   = mysql_fetch_assoc($queP);
					
					$arrPat['pattern'][$idEvent] = $value;
					foreach($feP AS $keyP => $valueP){
						$arrPat[$keyP][$idEvent] = $valueP;
					}
					
					$arrPat['id'][$idEvent] = $idEvent;
				}

			}
		}
	
		//pt@2012-01-26
		$other_where_event = '';
		if($_POST['order_event']){
			$other_where_event .= " AND follow_customer.event_id_ref = '$_POST[order_event]'";
		}

		// LAST EDIT 2012-11-27 BY KEN
		$cusInfo 		= '';
		$cust_name		= trim($_POST['cust_name']);
		$cust_lastname	= trim($_POST['cust_lastname']);
		
		$cusInfo 		= $cust_name.' '.$cust_lastname;

		if($cusInfo != ''){
			$other_where_event .= $other_where_event." AND cus_name LIKE '%".trim($cusInfo)."%' ";
		}
		// END LAST EDIT 2012-11-27 BY KEN
		
		
		$today 		= date('Y-m-d');
		$tb_follow 	= "$config[db_base_name].follow_customer";
		$tb_event 	= "$config[db_base_name].follow_main_event";
		
		//เข้าฟังชั่น เรียงเงื่อนไข select ข้อมูลที่จะแสดง
		$whereOu 	= condition_sortOU($empOu,$tb_follow); //function/general.php
		if($objDb->dbCh == 'DEMO'){
			$where_subquery = str_replace("DEMO_ERP_crm.follow_customer","count",$whereOu);
		}else{
			$where_subquery = str_replace("ERP_crm.follow_customer","count",$whereOu);
		}
		
		# P'Add 2012-11-28 ให้แสดงรายการตามสายงาน  
		$emp_id_card = getEmp_follower($_SESSION['SESSION_ID_card']); //function/general.php
		// if($emp_id_card){ $conditionOU = "AND (process_by IN($emp_id_card) OR emp_id_card IN($emp_id_card))"; }
		if($emp_id_card){ 
			$conditionOU = "AND (process_by NOT IN('updateExpire99','updateExpire98','updateExpire95','updateExpire1','updateExpire') OR emp_id_card IN($emp_id_card))"; 
		}
		# End P'Add 2012-11-28 ให้แสดงรายการตามสายงาน

		if($_GET['start'] == 0){
			$numRowAll = 0;
			$sql_all  = "SELECT id AS follow_id,event_id_ref,follow_type,chassi_no_s,cus_no,cus_name,start_date,date_value, ";
			$sql_all .= "(SELECT event_title FROM $tb_event WHERE id = event_id_ref) as event_title, ";
			$sql_all .= "(SELECT field_expire_date FROM $tb_event WHERE id = event_id_ref) as field_expire_date, ";
			$sql_all .= "emp_id_card,MIN(stop_date) AS stop_date,status AS follow_status,process_by, ";
			$sql_all .= "follow_db_name,source_db_table,source_primary_field,source_primary_id, ";
			$sql_all .= "(SELECT Time_for_talk FROM $config[db_maincus].MAIN_CUS_GINFO WHERE $config[db_maincus].MAIN_CUS_GINFO.CusNo = follow_customer.cus_no) as Time_for_talk, ";
			$sql_all .= "IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_SUB_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_SUB_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_SUB_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1),'no address' ))) as C_Tum, ";
			$sql_all .= "IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1),'no address' ))) as C_Amp, ";
			$sql_all .= "IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_PROVINCE,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_PROVINCE,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_PROVINCE,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1),'no address' ))) as C_Pro, ";
			$sql_all .= "(SELECT CUS_TYPE_LEVEL_DESC FROM $config[db_maincus].AMIAN_CUS_TYPE WHERE $config[db_maincus].AMIAN_CUS_TYPE.CUS_TYPE_ID = (SELECT CTYPE_GRADE_REF FROM $config[db_maincus].MIAN_CUS_TYPE_REF WHERE $config[db_maincus].MIAN_CUS_TYPE_REF.CTYPE_REF_CUSNO = follow_customer.cus_no LIMIT 1)LIMIT 1) as cusType, ";
			$sql_all .= "(SELECT TEL_NUM FROM $config[db_maincus].MAIN_TELEPHONE WHERE $config[db_maincus].MAIN_TELEPHONE.TEL_CUS_NO = follow_customer.cus_no AND $config[db_maincus].MAIN_TELEPHONE.TEL_TYPE='1' LIMIT 1) as telMobile, ";
			$sql_all .= "(SELECT TEL_NUM FROM $config[db_maincus].MAIN_TELEPHONE WHERE $config[db_maincus].MAIN_TELEPHONE.TEL_CUS_NO = follow_customer.cus_no AND $config[db_maincus].MAIN_TELEPHONE.TEL_TYPE='2' LIMIT 1) as telWork, ";
			$sql_all .= "(SELECT TEL_NUM FROM $config[db_maincus].MAIN_TELEPHONE WHERE $config[db_maincus].MAIN_TELEPHONE.TEL_CUS_NO = follow_customer.cus_no AND $config[db_maincus].MAIN_TELEPHONE.TEL_TYPE='3' LIMIT 1) as telHome, ";
			$sql_all .= "IF(chassi_no_s != '',(SELECT VEHICLE_REGIS FROM $config[db_vehicle].VEHICLE_INFO WHERE VEHICLE_ID = follow_customer.chassi_no_s LIMIT 1),'') as regis_no, ";
			$sql_all .= "IF(chassi_no_s != '',(SELECT VEHICLE_ENGINES FROM $config[db_vehicle].VEHICLE_INFO WHERE VEHICLE_ID = follow_customer.chassi_no_s LIMIT 1),'') as machine_no, ";
			$sql_all .= "IF(chassi_no_s != '',(SELECT VEHICLE_FULL_ENGINES FROM $config[db_vehicle].VEHICLE_INFO WHERE VEHICLE_ID = follow_customer.chassi_no_s LIMIT 1),'') as full_machine_no, ";
			$sql_all .= "IF(chassi_no_s != '',(SELECT Date_Deliver FROM $config[db_vehicle].VEHICLE_INFO WHERE VEHICLE_ID = follow_customer.chassi_no_s LIMIT 1),'') as Date_Deliver, ";
			$sql_all .= "IF(chassi_no_s != '',(SELECT DATE_FORMAT(REGIS_DATE,'%Y-%m-%d') FROM $config[db_vehicle].VEHICLE_REGIS WHERE REGIS_VEHICLE_ID = follow_customer.chassi_no_s LIMIT 1),'') as regis_date, ";
			$sql_all .= "(SELECT COUNT(*) FROM $tb_follow count WHERE $where_subquery AND count.status = '1' AND count.cus_no = follow_customer.cus_no AND count.follow_tracking_type = '1') as track_tel, ";
			$sql_all .= "(SELECT COUNT(*) FROM $tb_follow count WHERE $where_subquery AND count.status = '1' AND count.cus_no = follow_customer.cus_no AND count.follow_tracking_type = '2') as track_out ";
			$sql_all .= "FROM $tb_follow  WHERE $whereOu $other_where $other_where_b $other_where_event $conditionOU $whereFlow  $wherePerson AND ";
			$sql_all .= "(start_date <= '$today' AND stop_date >= '$today') AND $tb_follow.status='1' $filter_other ";
			$sql_all .= "GROUP by cus_no ORDER by $order ";
			$QueryAll = $logDb->queryAndLogSQL( $sql_all, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			if($_GET['s_branch'] && $_GET['s_branch']!='all'){
				while($fe = mysql_fetch_assoc($QueryAll)){ 
					$idEvent = $fe['event_id_ref'];
					if($arrPat['field_branch'][$idEvent]){
						$sqlA  = "SELECT ".$arrPat['field_branch'][$idEvent]." FROM ".$config[$arrPat['db_name'][$idEvent]].".".$arrPat['table_name'][$idEvent]." WHERE ";
						$sqlA .= "SUBSTRING_INDEX(CAST(".$arrPat['field_cus_name'][$idEvent]." AS CHAR),'_',1)='".$fe['cus_no']."' ";
						$sqlA .= "AND ".$arrPat['field_branch'][$idEvent]." IN('".$_GET['s_branch']."') ";
						$sqlA .= "AND ".$fe['source_primary_field']."='".$fe['source_primary_id']."' LIMIT 1";
						$queA  = $logDb->queryAndLogSQL( $sqlA, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						$feA   = mysql_fetch_assoc($queA);
						if($feA[$arrPat['field_branch'][$idEvent]]){
							$numRowAll++;
						}
					}
				}
			}else{
				$numRowAll 	= mysql_num_rows($QueryAll);
			}
			$response['numAll']		= $numRowAll; # จำนวนทั้งหมด*/
		}
		$limit = '';
		if($_GET['s_branch'] && $_GET['s_branch']!='all'){ 
			$limit = "LIMIT ".$_GET['num']." ,18446744073709551615 ";
		}else{
			$limit = "LIMIT ".$_GET['start'].", ".$_GET['limit']."";
		}
				// --------------------------------------------
		$sql  = "SELECT id AS follow_id,event_id_ref,follow_type,follow_tracking_type,chassi_no_s,cus_no,cus_name,start_date,date_value, ";
		$sql .= "(SELECT event_title FROM $tb_event WHERE id = event_id_ref) as event_title, ";
		$sql .= "(SELECT field_expire_date FROM $tb_event WHERE id = event_id_ref) as field_expire_date, ";
		$sql .= "emp_id_card,MIN(stop_date) AS stop_date,status AS follow_status,process_by, ";
		$sql .= "follow_db_name,source_db_table,source_primary_field,source_primary_id, ";
		$sql .= "(SELECT Time_for_talk FROM $config[db_maincus].MAIN_CUS_GINFO WHERE $config[db_maincus].MAIN_CUS_GINFO.CusNo = follow_customer.cus_no) as Time_for_talk, ";
		$sql .= "IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_SUB_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_SUB_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_SUB_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1),'no address' ))) as C_Tum, ";
		$sql .= "IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_DISTRICT,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1),'no address' ))) as C_Amp, ";
		$sql .= "IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_PROVINCE,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='1' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_PROVINCE,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='2' LIMIT 1), IF((SELECT COUNT(*) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1) > 0, (SELECT SUBSTRING_INDEX(ADDR_PROVINCE,'_',-1) FROM $config[db_maincus].MAIN_ADDRESS WHERE ADDR_CUS_NO = follow_customer.cus_no AND ADDR_TYPE='3' LIMIT 1),'no address' ))) as C_Pro, ";
		$sql .= "(SELECT CUS_TYPE_LEVEL_DESC FROM $config[db_maincus].AMIAN_CUS_TYPE WHERE $config[db_maincus].AMIAN_CUS_TYPE.CUS_TYPE_ID = (SELECT CTYPE_GRADE_REF FROM $config[db_maincus].MIAN_CUS_TYPE_REF WHERE $config[db_maincus].MIAN_CUS_TYPE_REF.CTYPE_REF_CUSNO = follow_customer.cus_no LIMIT 1)LIMIT 1) as cusType, ";
		$sql .= "(SELECT TEL_NUM FROM $config[db_maincus].MAIN_TELEPHONE WHERE $config[db_maincus].MAIN_TELEPHONE.TEL_CUS_NO = follow_customer.cus_no AND $config[db_maincus].MAIN_TELEPHONE.TEL_TYPE='1' LIMIT 1) as telMobile, ";
		$sql .= "(SELECT TEL_NUM FROM $config[db_maincus].MAIN_TELEPHONE WHERE $config[db_maincus].MAIN_TELEPHONE.TEL_CUS_NO = follow_customer.cus_no AND $config[db_maincus].MAIN_TELEPHONE.TEL_TYPE='2' LIMIT 1) as telWork, ";
		$sql .= "(SELECT TEL_NUM FROM $config[db_maincus].MAIN_TELEPHONE WHERE $config[db_maincus].MAIN_TELEPHONE.TEL_CUS_NO = follow_customer.cus_no AND $config[db_maincus].MAIN_TELEPHONE.TEL_TYPE='3' LIMIT 1) as telHome, ";
		$sql .= "IF(chassi_no_s != '',(SELECT VEHICLE_REGIS FROM $config[db_vehicle].VEHICLE_INFO WHERE VEHICLE_ID = follow_customer.chassi_no_s LIMIT 1),'') as regis_no, ";
		$sql .= "IF(chassi_no_s != '',(SELECT VEHICLE_ENGINES FROM $config[db_vehicle].VEHICLE_INFO WHERE VEHICLE_ID = follow_customer.chassi_no_s LIMIT 1),'') as machine_no, ";
		$sql .= "IF(chassi_no_s != '',(SELECT VEHICLE_FULL_ENGINES FROM $config[db_vehicle].VEHICLE_INFO WHERE VEHICLE_ID = follow_customer.chassi_no_s LIMIT 1),'') as full_machine_no, ";
		$sql .= "IF(chassi_no_s != '',(SELECT Date_Deliver FROM $config[db_vehicle].VEHICLE_INFO WHERE VEHICLE_ID = follow_customer.chassi_no_s LIMIT 1),'') as Date_Deliver, ";
		$sql .= "IF(chassi_no_s != '',(SELECT DATE_FORMAT(REGIS_DATE,'%Y-%m-%d') FROM $config[db_vehicle].VEHICLE_REGIS WHERE REGIS_VEHICLE_ID = follow_customer.chassi_no_s LIMIT 1),'') as regis_date, ";
		$sql .= "(SELECT COUNT(*) FROM $tb_follow count WHERE $where_subquery AND count.status = '1' AND count.cus_no = follow_customer.cus_no AND count.follow_tracking_type = '1') as track_tel, ";
		$sql .= "(SELECT COUNT(*) FROM $tb_follow count WHERE $where_subquery AND count.status = '1' AND count.cus_no = follow_customer.cus_no AND count.follow_tracking_type = '2') as track_out ";
		$sql .= "FROM $tb_follow WHERE $whereOu $other_where $other_where_b $other_where_event $conditionOU $whereFlow $wherePerson AND ";
		$sql .= "(start_date <= '$today' AND stop_date >= '$today') AND $tb_follow.status='1' $filter_other ";
		if($_GET['cuslist_excel']){
			$sql .= "GROUP by follow_id ORDER by $order ";
		}else{
			$sql .= "GROUP by cus_no ORDER by $order $limit ";
		}
		// echo $sql."<br>";exit();
		// --------------------------------------------
		$cls[1] 		= "background-color:#C1FFC1;";
		$cls[2] 		= "background-color:#B4EEB4;";
		$clsToday[1] 	= "background-color:#FFCC99;";
		$clsToday[2] 	= "background-color:#F89A3D;";
		$clsTomorrow[1] = "background-color:#F7F6BE";
		$clsTomorrow[2] = "background-color:#F8F791";
		
		$a = 0;
		mysql_query('SET NAMES UTF8');
		if(!$_GET['cuslist_excel']){
			# จำนวนทั้งหมดของการ query ครั้งนี้ (ใช้กับ progress bar)
			$count_query = @mysql_num_rows($sql);
			$obj_pre->setLoopMax(0,$count_query);
			$eachTell = 0; # จำนวนที่จะวน progressbar
			# --
		}
		$num = 0;
		//echo 'sql --->'.$sql.'<br>';exit();
		$follow = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while ($rs = mysql_fetch_assoc($follow)){
			if(!$_GET['cuslist_excel']){
				# ตอบกลับ progressbar
				$eachTell++; # จำนวนที่จะวน progressbar
				if($eachTell %5 == 0){

					$obj_pre->eachTell($eachTell,0);
					
				}
				# --
			}
			$idEvent 	= $rs['event_id_ref'];
			$border 	= '';
			$sShow 		= 'Y';

			// echo 'debug::<br>';
			// echo $rs['source_primary_field'].":".$rs['source_primary_id']."<br>";

			if($rs['source_primary_field'] && $rs['source_primary_id']){
				$arrChk['follow_id']			= $rs['follow_id'];
				$arrChk['follow_db_name']		= $rs['follow_db_name'];
				$arrChk['source_db_table']		= $rs['source_db_table'];
				$arrChk['source_primary_field']	= $rs['source_primary_field'];
				$arrChk['source_primary_id']	= $rs['source_primary_id'];
				$arrChk['field_expire_date']	= $rs['field_expire_date'];
				$sShow 							= chkExpireFollow($arrChk);
			}
			// echo $sShow."<br>";
			if($sShow == 'Y'){
				// echo "if(".$_GET['s_branch']." && ".$_GET['s_branch']."!='all'){<br/>";
				if($_GET['s_branch'] && $_GET['s_branch']!='all'){
					// echo $idEvent."==".$arrPat['id'][$idEvent]."<br/>";
					if($idEvent==$arrPat['id'][$idEvent]){
						
						$arrPat['idEvent'][$idEvent] = $idEvent;
						
						if($arrPat['field_branch'][$idEvent]){
							$sqlA  = "SELECT ".$arrPat['field_branch'][$idEvent]." FROM ".$config[$arrPat['db_name'][$idEvent]].".".$arrPat['table_name'][$idEvent]." WHERE ";
							$sqlA .= "SUBSTRING_INDEX(CAST(".$arrPat['field_cus_name'][$idEvent]." AS CHAR),'_',1)='".$rs['cus_no']."' ";
							$sqlA .= "AND ".$arrPat['field_branch'][$idEvent]." IN('".$_GET['s_branch']."') ";
							$sqlA .= "AND ".$rs['source_primary_field']."='".$rs['source_primary_id']."' LIMIT 1";
							// echo $sqlA."<br/><br/>";
							$queA  = $logDb->queryAndLogSQL( $sqlA, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							$feA   = mysql_fetch_assoc($queA);
							
							if($feA[$arrPat['field_branch'][$idEvent]]){
								$sShow = 'Y';
							}else{
								$sShow = 'N';
							}
						}else{
							$sShow = 'N';
						}
					}else{
						$sShow = 'N';
					}
				}
			}
			
			//ถ้าให้แสดง
			if($sShow=='Y'){
				
				$a++;
				if ($a > 2) {
					$a = 1;
				}
	
				$today 		 = date("Y-m-d");
				$numDiffDate = numDiffDate($today, $rs['stop_date']);
				$TRtitle 	 = "";
				if ($rs['stop_date'] == $today) { //ถ้าวันสุดท้ายตรงกับวันนี้
					$class 	 = $clsToday[$a];
					$TRtitle = "จะต้องทำภายในวันนี้แล้วนะครับ";
				} else if ($numDiffDate == 1) {
					$class 	 = $clsTomorrow[$a];
					$TRtitle = "วันพรุ่งนี้เป็นวันสุดท้ายแล้วนะครับ";
				} else {
					$class 	 = $cls[$a];
				}
	
				$follow_id 		= $rs['follow_id'];
				$cus_no 		= $rs['cus_no'];
				$cus_name 		= $rs['cus_name'];
				$event_title 	= $rs['event_title'];
				$event_id 		= $rs['event_id_ref'];
				$chassi_no_s 	= $rs['chassi_no_s'];
		
				// --------------------------------------
				$track_type = '';
				if($rs['follow_tracking_type'] ==  '1'){
					$track_type = "โทรติดตาม";
				}else if($rs['follow_tracking_type'] == '2'){
					$track_type = "ออกติดตาม";
				}

				// --------------------------------------

				// --------------------------------------------
				$time_redial 	= '--:-- น.';
				$date_value	  	= date_create($rs['date_value']);
				$date_value 	= date_format($date_value,'Y-m-d');
				// --------------------------------------------
	
				//วันที่ติดตามจริง เท่ากับ วันสุดท้าย - ค่าที่บวกเพิ่ม อ้างอิงจาก follow_main_event
				if($rs['start_date'] == '0000-00-00' || $rs['start_date'] == ''){
					$start_date   = '00/00/0000';
				}else{
					$start 		  = standard2thaidate($rs['start_date'], '-'); 					## function/ic_followcus.php
					$start_date   = "<div title='$start[text]'>$start[number]</div>"; 			## วันที่ครบกำหนดของเหตุการณ์
				}
				if($date_value == '0000-00-00' || $date_value == ''){
					$real_date    = '00/00/0000';
					$real_time    = $time_redial;
				}else{
					$real 	      = standard2thaidate($date_value, '-'); 						## function/ic_followcus.php
					$real_date 	  = "<div title='$real[text]'>$real[number]</div>"; 			## วันที่ครบกำหนดของเหตุการณ์

					$date_time	  = date_create($rs['date_value']);
					$real_time 	  = date_format($date_time,"H:i");
					if($real_time == '00:00'){
						$real_time = $time_redial;
					}else{
						$real_time = $real_time.' น.';
					}
				}
				if($rs['stop_date'] == '0000-00-00' || $rs['stop_date'] == ''){
					$expire_date  = '00/00/0000';
				}else{
					$expire	 	  = standard2thaidate($rs['stop_date'], '-');
					$expire_date  = "<div title='$expire[text]'>$expire[number]</div>"; //วันที่เริ่มติดต่อ
				}

				if($rs['Date_Deliver'] == '0000-00-00' || $rs['Date_Deliver'] == ''){
					$date_deliver = '00/00/0000';
				}else{
					$date_deliver = standard2thaidate($rs['Date_Deliver'], '-');
					$date_deliver = $date_deliver['number'];
				}
				if($rs['regis_date'] == '00-00-0000' || $rs['regis_date'] == ''){
					$regis_date = '00/00/0000';
				}else{
					$regis_date = standard2thaidate($rs['regis_date'], '-');
					$regis_date = $regis_date['number'];
				}
								// -----------------------
				$cus_level 		= $rs['cusType'];
				$cus_telnumber  = checkNull($rs['telMobile'])." / ".checkNull($rs['telHome'])." / ".checkNull($rs['telWork']);
				$time_for_talk 	= $rs['Time_for_talk'];

				$track_out_icon = '';
				$track_tel_icon = '';
				if($rs['track_out'] > 0){
					$track_out_icon = "<img src='img/people.gif' align='center' width='25' height='25' title='ออกติดตาม'>";
				}
				if($rs['track_tel'] > 0){
					$track_tel_icon = "<img src='img/telephone.gif' align='center' width='25' height='25' title='โทรติดตาม'>";
				}

				// -----------------------

				$cus_info = "<a href='javascript:void(0);' onclick=\"javascript:window.open('$view_detail_cuss"."$cus_no','newWin1','height=600,width=1000,top=0,left=0,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes,directories=no,status=no,titlebar=no');\"><img src='img/directory.gif' border='0' title='ดูข้อมูลประวัติส่วนตัวลูกค้า'></a>";

				$TRonclick = "follow_toggle('detail$cus_no','$followPage?operator=showCusEvent&id_card=$id_card&cus_no=$cus_no&$leader&$fix_event&empId=$rs[emp_id_card]','$cus_no')";
				
				//check ว่ามีคนจองไว้หรือยัง
				$cr_book = '';
				
				# P'Add 2012-11-27 DEV10538
				$cancel = '';
				# End P'Add 2012-11-27 DEV10538
				
				//if( $checkEventDepartment == $config['cr_department'] && strlen($rs['process_by'])==13 && abs($rs['process_by']) ){
				if(!$rs['emp_id_card'] && strlen($rs['process_by'])==13 && abs($rs['process_by']) ){
					$bgColor = "yellow";
					$booking_name = select_emp_nametonickname($rs['process_by']);
					$title	= "จองไว้โดยคุณ ".$booking_name;
					if($_SESSION['SESSION_ID_card'] != $rs['process_by']){
						$TRonclick	= "alert('ผู้ที่จองไว้ ต้องทำการยกเลิกการจองก่อน คุณจึงจะสามารถทำรายการนี้ได้ครับ');";
						$booking  	= "<img src='img/process_by.png'>จองไว้โดย :: <font color='black'>$booking_name</font>";
					}else if($_SESSION['SESSION_ID_card'] == $rs['process_by']) {
						$continue 	= $TRonclick;
						$TRonclick 	= "";
						$cancel 	= "<img src='img/cancel_process.png' align='absbottom' style='margin-top:2px;' title='ยกเลิกการจอง'
								onclick=\"cancel_cr_process('$cus_no','1','$id_card','$checkEventDepartment');\">";
						$booking 	= "<img src='img/process_by.png'>คุณได้จองไว้แล้ว คลิกที่นี่ เพื่อทำรายการต่อ";
					}
					$cr_book = "<div  id='book$cus_no'><span style='border:1px solid orange;background-color:$bgColor;' onclick=\"$continue\">$booking</span>$cancel</div>";
				}
				$click = $TRonclick;
	
				if($sShow == 'Y'){
					if($_GET['cuslist_excel']){ //ถ้ากดโหลดไฟล์ จะเป็นอีกเทมเพลตนึง
						$cus_list .= $tpl->tbHtml($followHTML, 'CUS_LIST_FOR_EXPORT_WITH_ADDRESS');
					}else{
						$cus_list .= $tpl->tbHtml($followHTML, 'CUS_LIST');
					}
					$num++;
					if($_GET['s_branch'] && $_GET['s_branch']!='all'){ 
						if($num == $_GET['limit']){
							$response['start']		= $_GET['start'] + $num; #จำนวนที่เริ่มนับในรอบต่อไป
							$response['html']		= $cus_list;
							$response['num']		= $eachTell; # จำนวนนับ
						}
					}
				}
			} //end if
		} //while loop

		if ($cus_list == "") {
			$colspan  = 10;
			$cus_list = $tpl->tbHtml($followHTML, 'NULL');
		}

		if($_GET['cuslist_excel']){ //ถ้ากดโหลดไฟล์ จะเป็นอีกเทมเพลตนึง
			header("Content-Type: application/vnd.ms-excel");
			header('Content-Disposition: attachment; filename="'.'รายชื่อลูกค้าที่ต้องโทรหา'.date('j_F_Y').'.xls"');#ชื่อไฟล์
			$html  = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"
			xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv="Content-type" content="text/html;charset=UTF-8"></head><body>';
			$html .= $tpl->tbHtml($followHTML, 'TABLE_FOR_EXPORT_WITH_ADDRESS');
			$html .= '</body></html>';
			echo $html;
			exit();
		
		}else{
			if($_GET['s_branch'] && $_GET['s_branch']!='all'){ 
				if($num <= $_GET['limit']){
					$response['start']		= $_GET['start'] + $num; #จำนวนที่เริ่มนับในรอบต่อไป
					$response['html']		= $cus_list;
					$response['num']		= $eachTell; # จำนวนนับ
				}
			}else{
				$response['start']		= $_GET['start'] + $num; #จำนวนที่เริ่มนับในรอบต่อไป
				$response['html']		= $cus_list;
				$response['num']		= $num; # จำนวนนับ
			}
			$response['status']		= 'success';
			echo json_encode($response);
			$obj_pre->endPreLoad();
		}
	break;

	case 'showCusEvent':
		$id_card = $_GET['id_card'];
		$cus_no  = $_GET['cus_no'];
		$empId   = $_GET['empId'];
		
		if($id_card==$_SESSION['SESSION_ID_card']){
			$empOu = select_empOu($id_card,'session'); //function/general.php
		}else if($id_card){
			$empOu = select_empOu($id_card); //function/general.php
		}
		
		//เข้าฟังชั่น เรียงเงื่อนไข select ข้อมูลที่จะแสดง
		$whereOu = condition_sortOU($empOu,$tb_follow); //function/general.php
		
		if( !$empId ){
			$chkBook = "SELECT process_by FROM $config[db_base_name].follow_customer WHERE follow_customer.cus_no='".$cus_no."' AND follow_customer.status = '1' ";
			$chkBook .= "AND process_by!='' LIMIT 1";
			$queBook = $logDb->queryAndLogSQL( $chkBook, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$feBook = mysql_fetch_assoc($queBook);
			
			if(strlen($feBook['process_by'])!=13 || !abs($feBook['process_by']) || $feBook['process_by']==$_SESSION['SESSION_ID_card']){
				#-- ล็อก process_by เป็นชื่อของผู้คลิกเข้ามาทำรายการ
				#-- ล็อกเฉพาะสถานะเป็น 1 หมายถึงกิจกรรมที่สร้างการติดตามจาก create_follow.php
				#-- และเป็นการติดตามที่ลูกค้าที่มีกิจกรรมของตัวเองดูแลเท่านั้น จะไม่เกี่ยวกับที่คนอื่นติดตามลูกค้าคนเดียวกันอยู่
				$lock = "UPDATE $config[db_base_name].follow_customer ";
				$lock.= " SET process_by = '$_SESSION[SESSION_ID_card]' ";
				$lock.= " WHERE follow_customer.cus_no='".$cus_no."' AND follow_customer.status = '1' AND $whereOu";
				$logDb->queryAndLogSQL( $lock, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				//แสดงแถบสีเหลืองว่าจองแล้ว และเซ็ตให้ TR คลิกไม่ได้ คลิกได้แต่แถบเหลือง
				echo "<script type='text/javascript'>add_cr_booking('$cus_no','$id_card','$checkEventDepartment','head');</script>";
			}else{
				$booking_name = select_emp_nametonickname($feBook['process_by']);
				echo '<span style="border: 1px solid orange; background-color: yellow; margin-left: 52px;"><img src="img/process_by.png"><span id = "booking_emp_name'.$cus_no.'">จองไว้โดย :: 
				<font color="black">'.$booking_name.'</font></span></span>';
				exit();
			}
		}
	
		$today = date('Y-m-d');
		$tb_follow = "$config[db_base_name].follow_customer";
		$tb_event = "$config[db_base_name].follow_main_event";
	
		//pt@2012-01-04
		if($_GET['fix_event']){ //ถ้ามีกิจกรรมพิเศษ
			//	$other_id_card = "";
			//	$team_id_card = get_emp_saleTemp($_SESSION['SESSION_ID_card'], 'id_card');
			//	if($team_id_card) $other_id_card =  ",".$team_id_card;
		}
		//end pt@2012-01-04
	
		$sql = "SELECT $tb_follow.id AS follow_id,event_id_ref,$tb_follow.follow_type,$tb_follow.follow_tracking_type,chassi_no_s,cus_no,cus_name,emp_id_card,start_date,date_value,";
		$sql .= "stop_date,$tb_follow.status AS follow_status,$tb_event.event_title,$tb_event.stop_value,$tb_event.stop_unit  ";
		$sql .= "FROM $tb_follow LEFT JOIN $tb_event ON $tb_follow.event_id_ref=$tb_event.id ";
		$sql .= "WHERE $whereOu AND (start_date <= '$today' AND stop_date >= '$today') AND $tb_follow.status ='1' AND cus_no = '$cus_no' AND $tb_event.visible!='no'";
		$sql .= "GROUP BY follow_id ORDER BY stop_date ASC,$tb_follow.follow_tracking_type DESC";
	
		//pt@2012-01-04 add $other_id_card   in SQL
		mysql_query('SET NAMES UTF8');
		$follow = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while ($rs = mysql_fetch_assoc($follow)) {
	
			$follow_id 			= $rs['follow_id'];
			$cus_no 			= $rs['cus_no'];
			$cus_name 			= $rs['cus_name'];
			$event_title 		= $rs['event_title'];
			$send_event_title 	= $rs['event_title'];
			$event_id 			= $rs['event_id_ref'];
			$chassi_no_s 		= $rs['chassi_no_s'];
			#-- 2011-02-07
			// --------------------------------------
			$track_icon = '';
			if($rs['follow_tracking_type'] ==  '1'){
				$track_icon = "<img src='img/telephone.gif' align='center' width='25' height='25' title='โทรติดตาม'>";
			}else if($rs['follow_tracking_type'] == '2'){
				$track_icon = "<img src='img/people.gif' align='center' width='25' height='25' title='ออกติดตาม'>";
			}
			// --------------------------------------
			// --------------------------------------------
			$time_redial 	= '--:-- น.';
			$date_value	  	= date_create($rs['date_value']);
			$date_value 	= date_format($date_value,'Y-m-d');
			// --------------------------------------------

			if($chassi_no_s!=""){
				/* $rss = query("SELECT B_Unit_Model,Eng_No_B FROM $config[db_base_name].Booking WHERE Chassi_No_B = '$chassi_no_s' ",1);
				$reg_no = query("SELECT reg_no FROM $config[db_base_name].Sell WHERE Chassi_No_S = '$chassi_no_s' ");
				
				if($reg_no==""){$reg_no=' ยังไม่มีทะเบียนรถ';}
				if($rss['B_Unit_Model']==""){$rss['B_Unit_Model']='-';}
				if($rss['Eng_No_B']==""){$rss['Eng_No_B']='-';}
				$send_event_title .= ' [ '.$rss['B_Unit_Model'].' / ' .$rss['Eng_No_B'].' / '. $reg_no .' ]';
				$event_title .=  ' <span style=\'color:orange\'>( '.$rss['B_Unit_Model'].' / ' .$rss['Eng_No_B'].' / '. $reg_no .' )</span>';*/
				
				$sqlCar	= "SELECT VEHICLE_FULL_CHASSIS, VEHICLE_FULL_ENGINES, VEHICLE_REGIS FROM ".$config['db_vehicle'].".VEHICLE_INFO WHERE VEHICLE_ID='".$chassi_no_s."' LIMIT 1";
				$queCar	= $logDb->queryAndLogSQL( $sqlCar, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$feCar  = mysql_fetch_assoc($queCar);
				
				if($feCar['VEHICLE_FULL_CHASSIS']==""){ $feCar['VEHICLE_FULL_CHASSIS'] = '-'; }
				if($feCar['VEHICLE_FULL_ENGINES']==""){ $feCar['VEHICLE_FULL_ENGINES'] = '-'; }
				if($feCar['VEHICLE_REGIS']==""){ $feCar['VEHICLE_REGIS'] = 'ยังไม่มีทะเบียนรถ'; }
				
				$send_event_title .= ' [ '.$feCar['VEHICLE_FULL_CHASSIS'].' / '.$feCar['VEHICLE_FULL_ENGINES'].' / '.$feCar['VEHICLE_REGIS'].' ]';
				$event_title .=  ' <span style=\'color:orange\'>( '.$feCar['VEHICLE_FULL_CHASSIS'].' / '.$feCar['VEHICLE_FULL_ENGINES'].' / '.$feCar['VEHICLE_REGIS'].' )</span>';
			}
	
			/* //วันที่ติดตามจริง เท่ากับ วันสุดท้าย - ค่าที่บวกเพิ่ม อ้างอิงจาก follow_main_event
			$real = getBeforeDate($rs['stop_date'], $rs['stop_value'], $rs['stop_unit']);
			$real_date = standard2thaidate($real, '-');
			$real_date = "<div title='$real_date[text]'>$real_date[number]</div>"; //วันที่ครบกำหนดของเหตุการณ์ */
			
			//วันที่ติดตามจริง เท่ากับ วันสุดท้าย - ค่าที่บวกเพิ่ม อ้างอิงจาก follow_main_event
			if($rs['start_date'] == '0000-00-00' || $rs['start_date'] == ''){
				$start_date   = '00/00/0000';
			}else{
				$start 		  = standard2thaidate($rs['start_date'], '-'); 					## function/ic_followcus.php
				$start_date   = "<div title='$start[text]'>$start[number]</div>"; 			## วันที่ครบกำหนดของเหตุการณ์
			}
			if($date_value == '0000-00-00' || $date_value == ''){
				$real_date    = '00/00/0000';
				$real_time 	  = $time_redial;
			}else{
				$real 	      = standard2thaidate($date_value, '-'); 						## function/ic_followcus.php
				$real_date 	  = "<div title='$real[text]'>$real[number]</div>"; 			## วันที่ครบกำหนดของเหตุการณ์
				$date_time	  = date_create($rs['date_value']);
				$real_time 	  = date_format($date_time,"H:i");
				if($real_time == '00:00'){
					$real_time = $time_redial;
				}else{
					$real_time = $real_time.' น.';
				}
			}
			if($rs['stop_date'] == '0000-00-00' || $rs['stop_date'] == ''){
				$expire_date  = '00/00/0000';
			}else{
				$expire	 	  = standard2thaidate($rs['stop_date'], '-');
				$expire_date  = "<div title='$expire[text]'>$expire[number]</div>"; //วันที่เริ่มติดต่อ
			}
	
			$today = date("Y-m-d");
			$numDiffDate = numDiffDate($today, $rs['stop_date']);
			$TRtitle = "";
			if ($rs['stop_date'] == $today) { //ถ้าวันสุดท้ายตรงกับวันนี้
				$class = "class='event_trHilight';"; // hilight tr background color
				$TRtitle = "จะต้องทำภายในวันนี้แล้วนะครับ";
			} else if ($numDiffDate == 1) {
				$class = "class='event_trHilight2';"; // hilight tr background color
				$TRtitle = "วันพรุ่งนี้เป็นวันสุดท้ายแล้วนะครับ";
			}
			//2011-07-27
			$rs['cus_name'] = trim($rs['cus_name']);//ตรวจพบ \n ที่ชื่อของลูกค้า ทำให้เกิด error เปิดแบบฟอร์มคำถามไม่ได้
			$followdata = "follow_cus_id=$follow_id&follow_type=$rs[follow_type]&chassi_no_s=$chassi_no_s&cus_no=$cus_no&cus_name=$rs[cus_name]&emp_id_card=$rs[emp_id_card]&start_date=$rs[start_date]&stop_date=$rs[stop_date]&status=$rs[follow_status]&pleasure_value=$pleasure_value";
	
			$url1 = "$followPage?operator=getEventScript&event_id=$event_id&event_title=$send_event_title&$followdata";
			$url2 = "$followPage?operator=getEventQuestion&event_id=$event_id&event_title=$send_event_title&$followdata";
	
			if ($_GET['leader'] == "yes") {
				$url2 = ""; //ถ้าเป็นหัวหน้าจะไม่เห็นแบบฟอร์มคำถาม
			}
	
			$break_up_td = "";
			$break = "";
			/*
			 $break_up_td = "<a href=\"javascript:void(0)\" onclick=\"javascript:if(confirm('ต้องการยกเลิกการติดตามเหตุการณ์ $event_title ของคุณ $cus_name หรือไม่?')==true){browseCheck('break_$follow_id','$followPage?operator=break_up&follow_id=$follow_id&event_title=$event_title&cus_name=$cus_name','');}breake = true;\"><img src='img/break_up.png' width='25' height='25' title='ยุติการติดตามรายการนี้'></a>";
			 */
			$break_up_td = "<a href=\"javascript:void(0)\" onclick=\"javascript:nzPopupCenter('','$followPage?operator=show_request_form','follow_id=$follow_id&event_title=$send_event_title&cus_name=$cus_name&cus_no=$cus_no&chassi_no=$chassi_no_s&id_card=$id_card&leader=$_GET[leader]',function(){document.getElementById('remark').focus();});\"><img src='img/break_up2.png' width='25' height='25' title='ยุติการติดตามรายการนี้'></a>";
	
			$border = "border-left:0px";
	
	
			# break=false เป็นการเซ็ต global variable เพื่อให้ฟังก์ชั่น showQuestionDetail ทำงาน
			# หาก break=true ฟังก์ชั่น showQuestionDetail จะไม่ทำงาน
			$click = "showQuestionDetail('$url1','$url2','cuslist$follow_id','$follow_id');changeObject('bt_script','div_script');";
			
			$trList .= $tpl->tbHtml($followHTML, 'SHOW_CUS_LIST');
		}
		if ($trList == "") {
			$colspan = 4;
			$config['null'] = "<span style='color:green'>คุณได้รายการของลูกค้ารายนี้ครบแล้วครับ</span>";
			$trList = $tpl->tbHtml($followHTML, 'NULL');
		}
		$table = $tpl->tbHtml($followHTML, 'SHOW_CUS_TABLE');
		echo $table;
	break;
	case 'getEventScript':
		
		$event_id = $_GET['event_id'];
		$event_title = $_GET['event_title'];
		$follow_cus_id = $_GET['follow_cus_id'];
		//$id_card 		= $_SESSION['SESSION_ID_card'];
		
		$CusDataScript = getCusDataScript($_GET['cus_no'],$_GET['cus_name'],$followHTML);

		$detailScript = "<div class='title1' style='color:blue'>เหตุการณ์เรื่อง : $event_title</div><hr><div id='div_cus_detail1'>$CusDataScript</div><div class='scroll_detail'>";
		$sql = "SELECT id,question_title,question_detail,question_talk_script ";
		$sql .= "FROM $config[db_base_name].follow_main_question ";
		$sql .= "WHERE event_id_ref = '$event_id' ";
		$sql .= "AND follow_main_question.question_type != '1' ";
		$sql .= "ORDER by id asc";
		mysql_query('SET NAMES UTF8');
		$ques = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while ($rs = mysql_fetch_assoc($ques)) {
			$main_ques_id = $rs['id'];
			//ค้นหาคำตอบของการติดตามครั้งนี้
			$ansSql = "SELECT follow_answer.id FROM $config[db_base_name].follow_answer ";
			$ansSql .= "INNER JOIN $config[db_base_name].follow_customer_historys ON follow_customer_historys.id_log = follow_answer.follow_customer_historys_id ";
			$ansSql .= "WHERE follow_answer.follow_customer_id = '$follow_cus_id' AND follow_answer.main_ques_id = '$main_ques_id' ";
			$ansSql .= "AND DATE_FORMAT(follow_customer_historys.historys_date,'%Y-%m-%d') = '".date("Y-m-d")."' ";
			$qAns = $logDb->queryAndLogSQL( $ansSql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$ansNum = mysql_num_rows($qAns);
			if ($ansNum == 0) {
				/* ใช้แท็ก <pre> จัดรูปแบบแทน
				 $rs['question_detail'] = str_replace(" ", "&nbsp;",$rs['question_detail']);
				 $rs['question_detail'] = nl2br($rs['question_detail']);
				 $rs['question_talk_script'] = str_replace(" ", "&nbsp;",$rs['question_talk_script']);
				 $rs['question_talk_script'] = nl2br($rs['question_talk_script']);
				 */
				$detail .= $tpl->tbHtml($followHTML, 'QUESTION_DETAIL')."<br><hr>";
			} else { //ถ้ามีการถามตอบแล้วจะข้ามไป
				$detail .= "<div class='title2' style='color:seagreen'>หัวข้อคำถามหลัก : $rs[question_title]</div>";
				$detail .= "<h4 style='color:red;margin-left:10px'>หัวข้อคำถามนี้ วันนี้ได้ทำรายการไปแล้วครับ</h4><br><hr>";
			}
		}
		if ($detail == "") {
			$detail = "<div style='background-color:#ffffff;color:red;font-size:12px'>ไม่มีข้อมูลรายละเอียด และบทสนทนา</div>";
		}
		$detail .= '</div>';
		echo $detailScript.$detail;
	break;
	case 'getEventQuestion':
		$event_title = $_GET['event_title']; //ไว้แสดงผล ที่เหลือเก็บลงฐานข้อมูล
		$event_id = $_GET['event_id'];
		$follow_cus_id = $_GET['follow_cus_id'];
		$follow_type = $_GET['follow_type'];
		$chassi_no_s = $_GET['chassi_no_s'];
		$cus_no = $_GET['cus_no'];
		$cus_name = $_GET['cus_name'];
		$start_date = $_GET['start_date'];
		$stop_date = $_GET['stop_date'];
		$status = $_GET['status'];
		$pleasure_value = $_GET['pleasure_value'];
		$emp_id_card = $_GET['emp_id_card'];
		if ($_GET['wait_submit']) {
			$wait_submit = "wait_submit=".$_GET['wait_submit'];
		}
	
		$sql = "SELECT id,question_title ";
		$sql .= "FROM $config[db_base_name].follow_main_question ";
		$sql .= "WHERE event_id_ref = '$event_id' ";
		$sql .= "AND follow_main_question.question_type != '1' ";
		$sql .= "ORDER by id";
		
		$CusDataScript = getCusDataScript($cus_no,$cus_name,$followHTML);
		# ประวัติการติดตามลูกค้า
		$history_list = get_follow_histroys($event_id,$cus_no);
		foreach($history_list as $key => $val) {
			$follow_customer_historys_list .= $tpl->tbHtml($followHTML,'FOLLOW_HIS_LIST');
		}
		$follow_customer_historys = $tpl->tbHtml($followHTML,'FOLLOW_HIS');

		$question = "<div class='title1' style='color:blue'>เหตุการณ์เรื่อง : $event_title</div><hr><div id='div_cus_detail2'>$CusDataScript</div><div class='scroll_detail'>";
	
		mysql_query('SET NAMES UTF8');
		$ques = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while ($rs = mysql_fetch_assoc($ques)) {
			$main_ques_id = $rs['id'];
			$question_list = "";
			$question_title = '';
			$numPleasure = '';
			$pleasure_input = '';
			$checkpleasure = '';
			$noform = false;
	
			++$formCount; //นับจำนวนแบบฟอร์ม
			$question_title = $rs['question_title'];
			$formNum = " แบบฟอร์มที่ $formCount";
			//ตรวจสอบว่าเคยตอบไปแล้วหรือยังก่อน
			$ansSql = "SELECT follow_answer.id FROM $config[db_base_name].follow_answer ";
			$ansSql .= "INNER JOIN $config[db_base_name].follow_customer_historys ON follow_customer_historys.id_log = follow_answer.follow_customer_historys_id ";
			$ansSql .= "WHERE follow_answer.follow_customer_id = '$follow_cus_id' AND follow_answer.main_ques_id = '$main_ques_id' ";
			$ansSql .= "AND DATE_FORMAT(follow_customer_historys.historys_date,'%Y-%m-%d') = '".date("Y-m-d")."' ";
			//$ansSql .= "WHERE follow_customer_id = '$follow_cus_id' AND main_ques_id = '$main_ques_id' AND emp_id_card IN('$emp_id_card','$checkEventDepartment') ";
			$qAns = $logDb->queryAndLogSQL( $ansSql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$ansNum = mysql_num_rows($qAns);
			if ($ansNum == 0) { //ถ้ายังไม่เคยทำรายการ

				# ดึงคำถามมาตราฐาน
				$StrMQ = "SELECT follow_question_list.id ";
				$StrMQ .= "FROM $config[db_base_name].follow_main_question ";
				$StrMQ .= "INNER JOIN $config[db_base_name].follow_question_list ON follow_question_list.main_question_id = follow_main_question.id ";
				$StrMQ .= "WHERE follow_main_question.question_type = '1' ";
				$StrMQ .= "AND follow_main_question.question_use = '0' ";
				$StrMQ .= "AND follow_main_question.status != '99' ";
				$StrMQ .= "AND follow_question_list.status != '99' ";
				$StrMQ .= "AND follow_main_question.event_id_ref = '".$event_id."' ";
				$StrMQ .= "ORDER BY follow_question_list.id ASC ";
				$QStrMQ = $logDb->queryAndLogSQL($StrMQ,"Error line:".__LINE__." file:".__FILE__." msg:".mysql_error());
				$NumMQ = mysql_num_rows($QStrMQ);
				if($NumMQ){
					$arr_key = 0;
					while($arr_list_id = mysql_fetch_assoc($QStrMQ)){
						$q_id[$arr_key] = $arr_list_id['id'];
						$arr_key++;
					}
					# ดึงวันที่ตั้งค่าโทรซ้ำ
					$StrDate = "SELECT follow_repeat_value,follow_repeat_unit FROM $config[db_base_name].follow_main_event ";
					$StrDate .= "WHERE id = '".$event_id."' AND follow_repeat_value != '0' ";
					$QStrDate = $logDb->queryAndLogSQL($StrDate,"Error line:".__LINE__." file:".__FILE__." msg:".mysql_error());
					$NumDate = mysql_num_rows($QStrDate);
					if($NumDate){
						$arr_date = mysql_fetch_assoc($QStrDate);
						//$date_repeat_val = date('d-m-Y', strtotime('+543 year', strtotime('+'.$arr_date['follow_repeat_value'].' '.$arr_date['follow_repeat_unit'].'', strtotime(date("Y-m-d")))));
						$Y = $M = $D = 0;
						switch($arr_date['follow_repeat_unit']){
							case 'year':$Y = $arr_date['follow_repeat_value'];break;
							case 'month':$M = $arr_date['follow_repeat_value'];break;
							case 'day':$D = $arr_date['follow_repeat_value'];break;
						}
						$date_repeat_val = standard2thai(cal_date(date("Y-m-d"),$Y,$M,$D,'+'),'-');
						
					}else{
						//$date_repeat_val = date('d-m-Y', strtotime('+543 year', strtotime(date("Y-m-d"))));
						$date_repeat_val = standard2thai(date("Y-m-d"),'-');
					}

					$question_list = $tpl->tbHtml($followHTML, 'MAIN_QUESTION_1');
				}

				$sql2 = "SELECT id,list_title,input_type,answer_option ";
				$sql2 .= "FROM $config[db_base_name].follow_question_list ";
				$sql2 .= "WHERE main_question_id = '$main_ques_id' AND ('$pleasure_value' >= pleasure_check OR pleasure_check IN('0','') ) ";
				$sql2 .= "AND status != '99' ORDER by order_number asc";
	
				mysql_query('SET NAMES UTF8');
				$queslist = $logDb->queryAndLogSQL( $sql2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$numRow = mysql_num_rows($queslist); //นับจำนวนแถวทั้งหมด
	
				$numPleasure = query("SELECT count(input_type) FROM $config[db_base_name].follow_question_list WHERE input_type='pleasure_value' AND main_question_id = '$main_ques_id' AND ('$pleasure_value' >= pleasure_check OR pleasure_check IN('0','') ) AND status != '99' ");
	
				$pleasNo = $numRow - $numPleasure; //จำนวนข้อทั้งหมดโดยไม่นับ Pleasure value
				if ($numRow > 0) {
					$n = 0;
					while ($rs2 = mysql_fetch_assoc($queslist)) {
	
						$list_id = $rs2['id'];
						$input_type = strtolower($rs2['input_type']);
						$answer_option = $rs2['answer_option'];
	
						//if( $checkEventDepartment = $config['cr_department'] ){
							//id='nouse_$list_id' บังคับให้ list_id ถูกคั่นด้วย _ และเป็นตัวที่สอง เพราะจะใช้ javascript ตัดไปใช้งาน
							$check_no_use = "<font color='saddlebrown'><input type='checkbox' name='no_user$list_id' id='nouse_$list_id' class='nocheck nouse' onclick=\"nz_use_question(this,'$list_id')\"/> ไม่ใช้คำถามนี้ </font>";
						//}
	
						$input = '';
						$input = genAnswerOption($list_id, $input_type, $answer_option);
	
	
						if ($input_type == "pleasure_value") { //ถ้าเป็นคะแนนความถึงพอใจ เก็บไว้ในตัวแปร ต่อท้ายสุด
							//$pleasNo++;
							//$pleasure_input .= "<div style='margin:10px 0px 10px 0px;' id='div_q$rs2[id]'>
							//					<div class='ques_list_title' style='padding:5px'>$pleasNo. <span class='c_title'>$rs2[list_title]</span> $check_no_use</div>
							//					<div id='no_$list_id' class='quesList' style='height:35px;'>$input</div></div>";
							//$checkpleasure .= " && checkMaxValue('pleasure".$list_id."',$answer_option)==true && check_number('pleasure".$list_id."')==true";
	
							//เรียงต่อกันไปเลย ไม่ต้องแบ่งไปไว้ด้านล่างแล้ว
							$n++;
							$question_list .= "<div style='margin:10px 0px 10px 0px;' id='div_q$rs2[id]'>
												<div class='ques_list_title' style='padding:5px'>$n. <span class='c_title'>$rs2[list_title]</span> $check_no_use</div>
												<div id='no_$list_id' class='quesList' style='height:35px;'>$input</div></div>";
						} else {
							$n++;
							$no = $n.".";
							$answer_option = $input;
							$question_list .= $tpl->tbHtml($followHTML, 'QUESTION_LIST');
						}
	
	
	
					} //while2 คำถามต่อไป
	
					//เรียงต่อกันไปเลย ไม่ต้องแบ่งไปไว้ด้านล่างแล้ว
					//$question_list .= $pleasure_input; //เอาค่าความพึงพอใจต่อท้าย
	
					$quesNo = "ques".$main_ques_id;
					$formName = "frm_".$quesNo;
					$div_quesNO = "div_".$quesNo;
					if ($question_list && $numRow && empty($_GET['readonly'])) { //ถ้ามีรายการคำถาม ให้เพิ่มปุ่ม submit
						$submitButton = "<button id='bt_submit_ques_$formName' class='nz_button BTgray' style='margin-left: 20px;padding:5px 10px;' onclick=\"javascript:";
						$submitButton .= "if(nzCheckForm('$formName')==true $checkpleasure && check_pleasure('$formName')==true){submitAnswerForm('$div_quesNO','$followPage?operator=SendAnswer&event_id=$event_id&follow_cus_id=$follow_cus_id&main_ques_id=$main_ques_id&$wait_submit";
						$submitButton .= "&follow_type=$follow_type&chassi_no_s=$chassi_no_s&cus_no=$cus_no&cus_name=$cus_name";
						$submitButton .= "&emp_name=$emp_name&emp_id_card=$emp_id_card&start_date=$start_date&stop_date=$stop_date&status=$status','','$formName');}\">";
						$submitButton .= "บันทึกข้อมูล</button>"; //ปุ่ม submit
	
						$question_list .= $submitButton;
					}
				} else {
					$noform = true;
					$question_list = "<div style='background-color:#ffffff;color:red;font-size:12px'>ไม่มีข้อมูลคำถาม ของแบบฟอร์มนี้</div>";
				}
			} else {
				$noform = true;
				$question_list = "<h4 style='color:red'>หัวข้อคำถามนี้ วันนี้ได้ทำรายการไปแล้วครับ</h4>";
				$formNum = " แบบฟอร์มที่ $formCount";
			}
	
			if ($noform) {
				$question_form .= $tpl->tbHtml($followHTML, 'NO_FORM')."<hr>"; //แยกเทมเพลต เป็นมีฟอร์มกับไม่มีฟอร์ม เพราะต้องนับแบบฟอร์มตอนทำราการเสร็จ
			} else {
				$question_form .= $tpl->tbHtml($followHTML, 'QUESTION_FORM')."<hr>";
			}
	
		} //while1 สิ้นสุดแบบฟอร์มแรก ถ้ามีคำถามหลักอีกข้อ จะสร้างแบบฟอร์มที่สอง
		$question .= $question_form;
		if ($question_form == "") {
			$question .= "<div style='background-color:#ffffff;color:red;font-size:12px'>ไม่มีข้อมูลคำถาม</div>";
		}
		$question .= '</div>';
		echo $question;
	break;
	case 'SendAnswer':
		//ค่าที่โพสต์มาทั้งหมด จะเป็นคำตอบ พารามิเตอร์อื่นๆ ส่งมาเป็น GET
		//ห้ามส่งค่าโพสต์อื่น ที่ไม่ใช่คำตอบมาด้วย
	
		$event_id = $_GET['event_id'];
		$follow_cus_id = $_GET['follow_cus_id'];
		$main_ques_id = $_GET['main_ques_id'];
		$follow_type = $_GET['follow_type'];
		$chassi_no_s = $_GET['chassi_no_s'];
		$cus_no = $_GET['cus_no'];
		$cus_name = $_GET['cus_name'];
		//$emp_id_card = $_GET['emp_id_card'];
		$emp_id_card = $_SESSION['SESSION_ID_card'];
		$start_date = $_GET['start_date'];
		$stop_date = $_GET['stop_date'];
		$status = $_GET['status'];
		if ($_GET['wait_submit']) {
			$wait_submit = "wait_submit=".$_GET['wait_submit'];
		}
	
		// nz 2011-08-09 ไม่ใช้แล้ว ย้ายไปเก็บจากการคำนวณค่า ssi
		//	if ($_POST['pleasure']) { //ถ้ามีคะแนนความพึงพอใจมาด้วย
		//		$pv = splite_reg($_POST['pleasure'], "=>", '1');
		//		$pleasure_value = str_replace("\'", "", $pv);
		//		$upPleasure = "UPDATE $config[Cus].MainCusData SET Pleasure_value = '$pleasure_value' WHERE CusNo = '$cus_no'";
		//		mysql_query($upPleasure) or die(mysql_error().'<br>'.__FILE__.' line '.__LINE__.'<br>'.$upPleasure) ;
		//	}

		$arr_q_id = explode('Q', splite_reg($_POST['contact_type'], '=>', '0'));
		$arr_q_main = array(
							'contact_type',
							'reason_option',
							'reason_option_remark',
							'repeat_type',
							'repeat_date',
							'talking_type',
							'repeat_time',
							'prostpect_type'
							);
		foreach ($_POST as $key => $val) { //วนลูปบันทึกคำตอบ
			if(!in_array($key, $arr_q_main)){
				$ques_list_id 	= splite_reg($val, '=>', '0');
				$answer 		= splite_reg($val, '=>', '1');
				//2011-08-05
				//$answer = str_replace("'", "", $answer);#### ไม่ต้องเอา ' ออก เพราะใช้เป็นตัวคั่น คำตอบ
				$answer = stripslashes($answer); #### ลองซ้อนกันดู ตัดออกเพื่อ add อีกครั้ง
				$answer = addslashes($answer); #### ตัดแล้วก็เพิ่มไว้/////ไม่ต้องเพราะมีอยู่แล้ว
		
				//ไม่ใช้คำถามนี้
				if($answer=='no_use'){
					$answer = '';
					$use_status = 'no';
				}
		
				$sql = "INSERT INTO $config[db_base_name].follow_answer ";
				$sql .= "(	event_id_ref,	follow_customer_id,	main_ques_id,		ques_list_id,		answer,		use_status,
							follow_type,	chassi_no_s,	cus_no,				cus_name,			emp_id_card,		emp_name)";
				$sql .= "VALUES";
				$sql .= "(	'$event_id',	'$follow_cus_id',	'$main_ques_id',	'$ques_list_id',	'".$answer."',	'$use_status',
						'$follow_type',		'$chassi_no_s',	'$cus_no',			'$cus_name',		'$emp_id_card',		'$emp_name')";
				mysql_query("SET NAMES UTF8");
				$query = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" ); // or die('ERROR : '.mysql_error().'<br>'.__FILE__.' line '.__LINE__); # ย้ายไปไว้ด้านล่างแล้ว
			}else if(in_array($key, $arr_q_main)){
				$ques_list_id = '';
				# ติดต่อได้ / ไม่ได้
				if(isset($_POST['contact_type']) && $key == 'contact_type'){
					$ques_list_id = $arr_q_id[0];
					$answer = splite_reg($_POST['contact_type'], '=>', '1')."'";
					$answer = stripslashes($answer); #### ลองซ้อนกันดู ตัดออกเพื่อ add อีกครั้ง
					$answer = addslashes($answer); #### ตัดแล้วก็เพิ่มไว้/////ไม่ต้องเพราะมีอยู่แล้ว
				}
				# สะดวกคุย / ไม่สะดวกคุย
				if(isset($_POST['talking_type']) && $key == 'talking_type'){
					$ques_list_id = $arr_q_id[1];
					$answer = splite_reg($_POST['talking_type'], '=>', '1')."'";
					$answer = stripslashes($answer); #### ลองซ้อนกันดู ตัดออกเพื่อ add อีกครั้ง
					$answer = addslashes($answer); #### ตัดแล้วก็เพิ่มไว้/////ไม่ต้องเพราะมีอยู่แล้ว
				}
				# สนใจ / ไม่สนใจ
				if(isset($_POST['prostpect_type']) && $key == 'prostpect_type'){
					$ques_list_id = $arr_q_id[2];
					$answer = splite_reg($_POST['prostpect_type'], '=>', '1')."'";
					$answer = stripslashes($answer); #### ลองซ้อนกันดู ตัดออกเพื่อ add อีกครั้ง
					$answer = addslashes($answer); #### ตัดแล้วก็เพิ่มไว้/////ไม่ต้องเพราะมีอยู่แล้ว
				}
				if(trim($ques_list_id) != ''){
					$answer = stripslashes($answer); #### ลองซ้อนกันดู ตัดออกเพื่อ add อีกครั้ง
					$answer = addslashes($answer); #### ตัดแล้วก็เพิ่มไว้/////ไม่ต้องเพราะมีอยู่แล้ว
					$sql = "INSERT INTO $config[db_base_name].follow_answer ";
					$sql .= "(	event_id_ref,	follow_customer_id,	main_ques_id,		ques_list_id,		answer,		use_status,
								follow_type,	chassi_no_s,	cus_no,				cus_name,			emp_id_card,		emp_name)";
					$sql .= "VALUES";
					$sql .= "(	'$event_id',	'$follow_cus_id',	'$main_ques_id',	'$ques_list_id',	'".$answer."',	'$use_status',
							'$follow_type',		'$chassi_no_s',	'$cus_no',			'$cus_name',		'$emp_id_card',		'$emp_name')";
					mysql_query("SET NAMES UTF8");
					$query = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" ); // or die('ERROR : '.mysql_error().'<br>'.__FILE__.' line '.__LINE__); # ย้ายไปไว้ด้านล่างแล้ว
				}
			}
		}
	
		if ($query) {
			/* #ไม่ใช้แล้วเปลี่ยนไปใช้ใน ฟังก์ชั่นเลย ตอนที่ตอบแบบสอบถามเสร็จก็เช็กทันที
			 $postdata = "event_id_ref=$event_id&follow_cus_id=$follow_cus_id&follow_type=$follow_type&chassi_no_s=$chassi_no_s&cus_no=$cus_no&cus_name=$cus_name&emp_id_card=$emp_id_card&start_date=$start_date&stop_date=$stop_date&status=$status&$wait_submit";
			 echo "<div class='title1 green'>บันทึกข้อมูลเรียบร้อย !</div>";
			 //ในกรณีที่มีหลายฟอร์มคำถาม ต้องเช็กดูก่อนว่าหมดทุกฟอร์มหรือยัง
			 echo "<script type='text/javascript'>checkNumForm('$follow_cus_id','$postdata');</script>"; //เช็กว่าหมดทุกฟอร์มรึยัง ถ้าหมดแล้วจะเปลี่ยนสถานะไม่ให้แสดงอีก
			 */
			$return['message'] = "<div class='title1 green'>บันทึกข้อมูลเรียบร้อย !</div>";
			$return['postdata'] = "event_id_ref=$event_id&follow_cus_id=$follow_cus_id&follow_type=$follow_type&chassi_no_s=$chassi_no_s&cus_no=$cus_no&cus_name=$cus_name&emp_id_card=$emp_id_card&start_date=$start_date&stop_date=$stop_date&status=$status&$wait_submit";
			$return['postdata'] .= "&contact_type=".$_POST['contact_type']."&reason_option=".$_POST['reason_option']."&reason_option_remark=".$_POST['reason_option_remark']."&repeat_type=".$_POST['repeat_type']."";
			$return['postdata'] .= "&repeat_date=".$_POST['repeat_date']."&talking_type=".$_POST['talking_type']."&repeat_time=".$_POST['repeat_time']."&prostpect_type=".$_POST['prostpect_type']."";
			$data = json_encode($return);
			echo $data;//ไปคืนไปยังฟังก์ชั่น submitAnswerForm
			unset($return);
			unset($data);
	
		} else {
			echo json_encode(array('message'=>"<div class='title1 red' style='font-size:12px'>เกิดข้อผิดพลาด ! MySQL Error : ".__FILE__.' line '.__LINE__.'<br>'.$sql  ."</div>"));
		}
	break;
	case 'setStatus':
		$follow_cus_id = $_POST['follow_cus_id'];
		$event_id_ref = $_POST['event_id_ref']; //ใช้ตรวจสอบ event ต่อไป
		$follow_type = $_POST['follow_type'];
		$chassi_no_s = $_POST['chassi_no_s'];
		$cus_no = $_POST['cus_no'];
		$cus_name = $_POST['cus_name'];
		$emp_id_card = $_POST['emp_id_card'];
		$start_date = $_POST['start_date'];
		$stop_date = $_POST['stop_date'];
		$status = $_POST['status'];
	
		if ($status == "1") {
			$upStatus = "2"; //2 = โทรหาแล้ว
			$nextStatus = "0"; //0 = เริ่มติดตามใหม่ หากเป็นการติดตามแบบต่อเนื่อง alway
		} else if ($status == "4") {
			$upStatus = "5"; //5 = สัมภาษณ์แล้ว
			$nextStatus = "3"; //3 = รอสัมภาษณ์ (กรณี รอสัมภาษณ์ ต้องทำแบบต่อเนื่อง alway)
		}
		
		//หลังจากอัพเดตสถานะเรียบร้อย ให้ตรวจหาเหตุการณ์ต่อไป
		$esql = "SELECT event_id_ref,next_event_id FROM $config[db_base_name].follow_event_refer WHERE event_id_ref = '$event_id_ref' AND status !='99' ";
		mysql_query("SET NAMES UTF8");
		$eqry = $logDb->queryAndLogSQL( $esql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$num = mysql_num_rows($eqry);
		if ($num) {
	
			//ถ้ามีเหตุการต่อไป สร้างเหตุการนั้นขึ้นมา
			while ($rs = mysql_fetch_assoc($eqry)) {
				$next_event_id = $rs['next_event_id'];
				//สร้างวันที่ start stop ของเหตุการณ์ต่อไป
				$newSQL = "SELECT department,who_response,cal_format,cal_value,cal_unit,start_value,start_unit,stop_value,stop_unit ";
				$newSQL .= "FROM $config[db_base_name].follow_main_event WHERE id = '$next_event_id' AND status != '99' ";
				$result = $logDb->queryAndLogSQL( $newSQL, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$new = mysql_fetch_assoc($result);
	
				$cal_format = $new['cal_format'];
				$start_value = $new['start_value'];
				$start_unit = $new['start_unit'];
				$stop_value = $new['stop_value'];
				$stop_unit = $new['stop_unit'];
	
				if ($cal_format == "ondate") { //การส่งต่อจะมีแค่สองเงื่อนไข คือเริ่มจากวันที่ทำ
					$new_date = date('Y-m-d');
	
				} else if ($cal_format == "after") { //และ เริ่มหลังจากวันที่ทำ
					$new_date = getAfterDate(date('Y-m-d'), $new['cal_value'], $new['cal_unit']);
				}
	
				$status = '3'; //ถ้ามีการส่งต่อ status จะเท่ากับ 3
	
				//ทำล่วงหน้า-เลยกำหนด ได้กี่วัน
				$new_startdate = getBeforeDate($new_date, $start_value, $start_unit); //เริ่มแจ้งเตือน
				$new_stopdate = getAfterDate($new_date, $stop_value, $stop_unit); //สิ้นสุดการแจ้งเตือน
				$idCard = "";
				//ถ้าเป็น Leader จะสแตมป์ idCard ของหัวหน้าที่เป็นคนตรวจ ไปเลย
				if ($new['department'] == "Leader") # ให้หัวหน้าสัมภาณ์
				{
					$idCard = get_leader_idCard($_SESSION['SESSION_ID_card']);
				}
				/* #ไม่ต้องเพิ่มส่วนนี้ลงไป เพราะถ้าไม่ใช่หัวหน้า คนอื่นๆ สามารถเข้าถึงได้จากหน้าสัมภาษณ์เหตุการลูกค้า ที่จะเทียบ SESSION_ID_card
				 * กับฟิลด์ who_responser และ เทียบฝ่ายกับ department เองได้ เพราะ status 3 จะแยกจาก status 1
				 elseif ($new['who_response'])	# ให้บุคคลสัมภาษณ์
				 {
					$idCard = $new['who_response'];
					}
					else	# ให้ฝ่ายสัมภาษณ์
					{
					$idCard = $new['department'];
					}
					*/
	
				//ถ้าเป็นแผนกทั่วไป emp_id_card จะเท่ากับค่าว่าง
	
				$sql = "INSERT INTO $config[db_base_name].follow_customer ";
				$sql .= "( event_id_ref,		follow_type,		chassi_no_s,		cus_no,			cus_name,
							emp_id_card,		start_date,			stop_date,			process_by,	process_date,	status";
				$sql .= ")VALUES(";
				$sql .= " '$next_event_id',		'$follow_type',		'$chassi_no_s',		'$cus_no',		'$cus_name',
							'$idCard',		'$new_startdate',	'$new_stopdate',	'wait',	NOW(), '$status')";
				mysql_query("SET NAMES UTF8");
				$logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			} //while loop
		} //end if
	
		//ดึงข้อมูลเหตุการณ์มาตรวจสอบรูปแบบการแจ้งเตือน
		$sql2 = "SELECT remind_type,remind_value,remind_unit,department,pattern_id_ref,status FROM $config[db_base_name].follow_main_event WHERE id = '$event_id_ref'";
		mysql_query("SET NAMES UTF8");
		$result = $logDb->queryAndLogSQL( $sql2, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$rs = mysql_fetch_assoc($result);
	
		$remind_type = $rs['remind_type']; //รูปแบบการแจ้งเตือน
		$remind_value = $rs['remind_value']; // จำนวน 1,2,3,...,n
		$remind_unit = $rs['remind_unit']; //หน่วย วันเดือนปี
	
		//ตรวจสอบประเภทของเหตุการณ์ว่าเป็นแบบ ต่อเนื่องหรือไม่
		//status = 10 คือกิจกรรมพิเศษที่ไม่ใช้การสร้างต่อเนื่องแบบนี้
		if (strtolower($remind_type) == 'alway' && $rs['status']!='10' && $rs['status']!='99') { //ถ้าพบ alway ให้สร้างการติดตามครั้งต่อไป
			createNextRemind($follow_cus_id, $remind_value, $remind_unit, $rs); //function/ic_followcus.php
		}
	
		//อัพเดตสถานะใน follow_customer ว่าทำรายการแล้ว
		$fsql = "UPDATE $config[db_base_name].follow_customer ";
		$fsql .= "SET status = '$upStatus',process_by='$_SESSION[SESSION_ID_card]',process_date=NOW() WHERE id = '$follow_cus_id'";
		mysql_query("SET NAMES UTF8");
		$result = $logDb->queryAndLogSQL( $fsql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	
	
		if ($result) {
			$today = date('Y-m-d');
			$cus_no = $_POST['cus_no'];
			
			$empOu = select_empOu($_SESSION['SESSION_ID_card'],'session'); //function/general.php
			//เข้าฟังชั่น เรียงเงื่อนไข select ข้อมูลที่จะแสดง
			$whereOu = condition_sortOU($empOu); //function/general.php
			
			//ตรงนี้ไม่ได้เช็กเหตุการณ์ที่เป็นรอสัมภาษณ์ด้วย
			$checkSQL = "SELECT count(id) AS idNum FROM $config[db_base_name].follow_customer ";
			$checkSQL .= "WHERE cus_no = '$cus_no' AND $whereOu AND status = '1' AND (start_date <= '$today' AND stop_date >= '$today') ";
			$checkResult = $logDb->queryAndLogSQL( $checkSQL, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$numCheck = mysql_fetch_array($checkResult);
			if (isset($_POST['wait_submit'])) { //ถ้า submit จากหน้าสัมภาษณ์
				$js = "hideObj('tr_event_$follow_cus_id,cursor');";
			} else { //ถ้า submit จากหน้าโทรหาลูกค้า
				$js = "hideObj('cuslist$follow_cus_id,cursor');";
				if ($numCheck['idNum'] < 1) {
					$js .= "hideObj('TR$cus_no,cus$cus_no');";
				}
			}
			echo "<script type='text/javascript'>$js</script>";
		}
	break;
	case 'cancelProcess1':
		if($_GET['status']==''){$_GET['status']='1';}
		
		$empOu = select_empOu($_GET[id_card],'session'); //function/general.php
		//เข้าฟังชั่น เรียงเงื่อนไข select ข้อมูลที่จะแสดง
		$whereOu = condition_sortOU($empOu); //function/general.php
		
		//ใช้โค๊ดชุดเดียวกันกับตอนล็อกสถานะ
		$lock = "UPDATE $config[db_base_name].follow_customer ";
		$lock.= " SET process_by = '-' ";
		$lock.= " WHERE follow_customer.cus_no='".$_GET['cus_no']."' AND follow_customer.status = '$_GET[status]'
				AND $whereOu ";
		$logDb->queryAndLogSQL( $lock, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		if(mysql_affected_rows()){
			echo 'OK';
		}
	break;
	case 'getMainCusData':
		$cus_no = $_GET['cus_no'];
		echo $tpl->tbHtml($followHTML, 'CUS_DATA');
	break;
	case 'getAllCustomer':

		$id_card = $_GET['id_card'];

		$emp = select_emp_nametonickname($id_card);
		if ($_GET['mode'] == "forward") { //ถ้าเป็นการคลิกเข้ามาของหัวหน้า
			$response = "<div align='left' style='width:100%;padding:10px;color:#ff0000;background:#ffffff' class='title3'>";
			$response .= "รายชื่อลูกค้าทั้งหมดของ ::  <font color='green'>$emp</font> :: [พนักงานผู้ดูแลลูกค้า] </div>";
		}
		if ($_GET['back']) {
			$back_display = '';
		} else {
			$back_display = 'none';
		} //ซ่อนปุ่ม back
		$navDetailDisplay = "none";
		$popup_block = $tpl->tbHtml($followHTML, 'POPUP_BLOCK'); //ป๊อบอัพแสดงรายการที่ทำล่าสุด
		$all_cus_table = $tpl->tbHtml($followHTML, 'ALL_CUS_TABLE');

		echo $all_cus_table.$popup_block;

	break;
	case 'getAllCustomerSearch':

		$pload = new pLoadmore();
		
		$id_card = $_GET['id_card'];

		$sql = "SELECT cusDB.CusNo,cusDB.Cus_Name,cusDB.Cus_Surename ";
		$sql .= "FROM $config[db_maincus].MAIN_RESPONSIBILITY as respon LEFT JOIN $config[db_maincus].MAIN_CUS_GINFO as cusDB ON respon.RESP_CUSNO=cusDB.CusNo ";
		$sql .= "WHERE respon.RESP_IDCARD = '$id_card' ORDER by CONCAT(cusDB.Cus_Name,' ',cusDB.Cus_Surename) ASC";
		
		$result = $pload->queryAndLoadmore($sql, " FILE : ".__FILE__." LINE : ".__LINE__."");
		$n = $pload->start;
		//$arrCus = array_push($addCard,$addDoc,$telMobile,$telHome,$telWork);
		
		//echo '<br>'.$sql;
		while ($rs = mysql_fetch_assoc($result)) {
			//if(!$n){
			$cus_no = $rs['CusNo'];
			
			//บัตรปชช
			$addCard = selectDB("$config[db_maincus].MAIN_ADDRESS","CONCAT(ADDR_NUMBER,' ',ADDR_GROUP_NO) AS C_Add,ADDR_VILLAGE AS C_Vill,ADDR_SUB_DISTRICT AS C_Tum,
			ADDR_DISTRICT AS C_Amp,ADDR_PROVINCE AS C_Pro,ADDR_POSTCODE AS C_Code","ADDR_CUS_NO='$cus_no' AND ADDR_TYPE='1' LIMIT 1");

			//เอกสาร
			$addDoc = selectDB("$config[db_maincus].MAIN_ADDRESS","CONCAT(ADDR_NUMBER,' ',ADDR_GROUP_NO) AS Cus_Add,ADDR_VILLAGE AS Cus_Vil,
			ADDR_SUB_DISTRICT AS Cus_Tum,ADDR_DISTRICT AS Cus_Amp,ADDR_PROVINCE AS Cus_Pro,ADDR_POSTCODE AS Cus_Code","ADDR_CUS_NO='$cus_no' AND 
			ADDR_TYPE='2' LIMIT 1");

			//เบอร์มือถือ
			$telMobile = selectDB("$config[db_maincus].MAIN_TELEPHONE","TEL_NUM AS M_Tel","TEL_CUS_NO='$cus_no' AND TEL_TYPE='1' LIMIT 1");
			//เบอร์บ้าน
			$telHome = selectDB("$config[db_maincus].MAIN_TELEPHONE","TEL_NUM AS H_Tel","TEL_CUS_NO='$cus_no' AND TEL_TYPE='3' LIMIT 1");
			//เบอร์ที่ทำงาน
			$telWork = selectDB("$config[db_maincus].MAIN_TELEPHONE","TEL_NUM AS W_Tel","TEL_CUS_NO='$cus_no' AND TEL_TYPE='2' LIMIT 1");
			//}
			$rs['H_Tel'] = $telHome['H_Tel'];
			$rs['W_Tel'] = $telWork['W_Tel'];
			$rs['M_Tel'] = $telMobile['M_Tel'];
			
			if($addCard){ $rs = array_merge($rs,$addCard); }else if($addDoc){ $rs = array_merge($rs,$addDoc); }
			
			$n++;
			if ($n % 2) {
				$class = "class='all_cus_tr1'";
			} else {
				$class = "class='all_cus_tr2'";
			}
			$cus_no = $rs['CusNo'];
			$cus_name = $rs['Cus_Name']." ".$rs['Cus_Surename'];
			$address = getMainCusAddress($rs);
	
			//ช้ามากๆ ลูกค้า 100 ใช้เวลาหลายนาที
			$sql = "SELECT process_date FROM $config[db_base_name].follow_customer WHERE cus_no = '$cus_no' AND status = '2' ORDER BY id DESC LIMIT 1";
			$last_call_date = query($sql);
	
			if($last_call_date==""){ $last_call_date = 'ยังไม่มีข้อมูลการติดต่อ'; }
			$all_cus_list .= $tpl->tbHtml($followHTML, 'ALL_CUS_LIST');
	
			# -- last update 27-08-2554 by.k   download to exel
			if($_GET['export'] == 'all_followcus'){
				$address =  str_replace('โทรศัพท์', ' ', str_replace('โทรศัพท์,', ' ', getMainCusAddress($rs,$check_no='NO_BR',$export='export') ) );
				$all_cus_list_download .= $tpl->tbHtml($followHTML, 'ALL_CUS_LIST_DOWNLOAD');
			}else {
				$pload->eachLoadmore();
			}
	
		}
		
		# -- last update 27-08-2554 by.k   download to exel
		if($_GET['export'] == 'all_followcus'){
			header("Content-Type: application/vnd.ms-excel");
			header('Content-Disposition: attachment; filename="'.date('j_F_Y').'_ข้อมูลลูกค้า.xls"');#ชื่อไฟล
			echo '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"
			xmlns="http://www.w3.org/TR/REC-html40"><HTML><HEAD><meta http-equiv="Content-type" content="text/html;charset=UTF-8" /></HEAD><BODY>';
			echo $tpl->tbHtml($followHTML, 'ALL_CUS_TABLE_DOWNLOAD');
			exit();
		}else {
			if ($all_cus_list == "") {
				$colspan = 10;
				$config['null'] = "ไม่มีรายชื่อลูกค้าที่ $emp เป็นผู้รับผิดชอบครับ";
				$all_cus_list = $tpl->tbHtml($followHTML, 'NULL');
			}
			echo $all_cus_list;
			$pload->endLoadmore();
		}
		
	break;
	//ยกเลิกการติดตาม
	case 'show_request_form':
		
		//
		$event_title = $_GET['event_title'];
		$cus_name = $_GET['cus_name'];
		//
		
		$follow_id = $_GET['follow_id'];
		$cus_no = $_GET['cus_no'];
		$chassi_no = $_GET['chassi_no'];
		$stopCarDisp = 'none';
		$marginLeft = '40px';
		$carText = 'รถคันนี้';
		$id_card = $_GET['id_card'];
		$leader = 'leader='.$_GET['leader'];
		if (isset($_GET['allCus'])) {
			$bAct1 = '<!-- '; $bAct2 = ' -->';
			
			$cus_no = $_GET['allCus'];
			#-- ตั้งค่าการแสดงผล
			$typedisplay = 'none';
			$eventDisabled = 'disabled="disabled"';
			$stopCarDisp = '';
			$marginLeft = '5px';
			$carText = 'รถรายคัน';
			$carOnclick = "$('.chkCar').attr('disabled',false);document.getElementById('div_carlist').style.color='#000';";
			$cusOnclick = "$('.chkCar').attr('disabled',true);document.getElementById('div_carlist').style.color='#999';";
			#--
			//ลิสต์รายการซื้อรถ
			/*$sql = "SELECT Eng_No_S,Chassi_No_S,S_Booking_No,reg_no FROM $config[db_base_name].Sell WHERE SUBSTRING_INDEX(cusBuy,'_',1) = $cus_no ";
			$result = mysql_query($sql);*/
			$sql = "SELECT vehi.VEHICLE_ID,vehi.VEHICLE_MODEL_NAME,vehi.VEHICLE_ENGINES,vehi.VEHICLE_FULL_CHASSIS,vehi.VEHICLE_CHASSIS,vehi.VEHICLE_REGIS FROM ";
			$sql .= "$config[db_vehicle].VEHICLE_RELATIONSHIP rel INNER JOIN $config[db_vehicle].VEHICLE_INFO vehi ON rel.RELATES_VEHICLE_ID=vehi.VEHICLE_ID ";
			$sql .= "WHERE rel.RELATES_CUS_ID='".$cus_no."' AND rel.RELATES_STATUS!='99' AND vehi.VEHICLE_STATUS!='99' LIMIT 1";
			$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
	
			while ($rs = mysql_fetch_assoc($result)) {
				/*$follow = query("SELECT id FROM $config[db_base_name].follow_customer WHERE chassi_no_s = '$rs[Chassi_No_S]' AND status IN('0','1') ORDER by id desc 
				LIMIT 1 ");
				if ($follow) { // แสดงเฉพาะรถคันที่ติดตามอยู่
					$bookQry = mysql_query("SELECT B_Unit_Model FROM $config[db_base_name].Booking WHERE Booking_No = '$rs[S_Booking_No]'") or die(mysql_error());
					$bookArr = mysql_fetch_assoc($bookQry);
					$rs['B_Unit_Model'] = $bookArr['B_Unit_Model'];
					$car_list .= $tpl->tbHtml($followHTML, 'CAR_LIST');
				}*/
				$follow = query("SELECT id FROM $config[db_base_name].follow_customer WHERE chassi_no_s = '$rs[VEHICLE_ID]' AND status IN('0','1') ORDER by id desc 
				LIMIT 1");
				
				if ($follow) { // แสดงเฉพาะรถคันที่ติดตามอยู่*/
					$carID = $rs['VEHICLE_ID'];
					$rs['B_Unit_Model'] = $rs['VEHICLE_MODEL_NAME']; //รหัสแบบรถ
					$rs['Eng_No_S'] = $rs['VEHICLE_ENGINES']; //เลขเครื่อง
					if($rs['VEHICLE_FULL_CHASSIS']){ $rs['Chassi_No_S'] = $rs['VEHICLE_FULL_CHASSIS']; }else{ $rs['Chassi_No_S'] = $rs['VEHICLE_CHASSIS']; } //แซสซี
					$rs['reg_no'] = $rs['VEHICLE_REGIS']; //ทะเบียน
					
					$car_list .= $tpl->tbHtml($followHTML, 'CAR_LIST');
				}
			}
			if ($car_list=="") {//ถ้าไม่มีรถที่ติดตามอยู่ให้ซ่อนตารางรถ และปิดยุติรถ
				$stopCarDisp = 'none';
				$disabled = 'disabled="disabled"';
				$gray = '#999';
			}
	
		} else if ($chassi_no == "") {//ถ้าไม่ส่ง chassi มาด้วยให้ปิด ตัวเลือกยุติรถ
			$disabled = 'disabled="disabled"';
			$gray = '#999';
		}
		
		$follow_TypeCancel = follow_TypeCancel(); //function/ic_followcus.php
	
		echo $tpl->tbHtml($followHTML, 'FORM_SENT_REQUEST');
	break;
	//ยกเลิกการสัมภาษณ์
	case 'send_request':
		
		/*print_r($_GET); echo "<br/>";
		print_r($_POST);exit();*/
		
		$follow_id = $_POST['follow_id'];
		$cancelType = $_POST['cancelType'];
		$cancleList = $_POST['cancleList'];
		$remark = $_POST['remark'];
		$cus_no = $_POST['cus_no'];
		$chassi_no = $_POST['chassi_no'];
		$status = '90';
		//ใช้สำหรับ togle
		$id_card = $_GET['id_card'];
		$leader = $_GET['leader'];
		
		//อัพเดตเฉพาะรายการที่ sale ต้องโทรหา
		$hideAll = false;
		switch ($cancelType) {
			case 'stopCus': //หยุดทุกการติดตามของ cus_no นี้
				$where = "cus_no = '$cus_no' AND status IN ('0','1') ";
				$hideAll = true;//ซ่อนลูกค้ารายนี้
				break;
			case 'stopCar': //หยุดทุกการติดตามของ chassi_no_s คันนี้
				if (isset($_POST['carlist'])) {
					$chassi_no = @implode("','", $_POST['carlist']);
				} else {
					$chassi_no = query("SELECT chassi_no_s FROM $config[db_base_name].follow_customer WHERE id = '$follow_id' ");
				}
				$where = "cus_no = '$cus_no' AND chassi_no_s IN ('$chassi_no') AND status IN ('0','1') ";
				//ซ่อนไอดี ที่ Chassi เดียวกัน
				
				$all_follow_id = query( "SELECT id FROM $config[db_base_name].follow_customer WHERE cus_no = '$cus_no' AND chassi_no_s IN ('$chassi_no') AND 
				status IN ('0','1')",2);//select_to_sql_in -> function/general.php
				$hideCus = 'cuslist'.@implode(',cuslist', $all_follow_id['id']);
				break;
			case 'stopEvent':
				$where = "id = '$follow_id'";
				$hideCus = "cuslist$follow_id";//ซ่อนไอดีนี้
				break;
			case 'stopTemporary': //หยุดชั่วคราว
				$where = "id = '$follow_id'";
				$hideCus = "cuslist$follow_id";
				break;

		}
		
		//เก็บลงตาราง ยกเลิก
		$fCancel = "id,event_id_ref,follow_type,chassi_no_s,cus_no,cus_name,biz_id_ref,follow_resps_comp,follow_resps_department,follow_resps_section,";
		$fCancel .= "follow_resps_position,emp_id_card,emp_posCode,start_date,date_value,stop_date,process_by,process_by_posCode,process_date,cancel_type,";
		$fCancel .= "remark,status,TIS_Event_Group,TIS_Event_Period,TIS_Event_PeriodCode,send_to_tis,FC_status_tis,FC_Date_upstatus,follow_db_name,source_db_table,";
		$fCancel .= "source_primary_field,source_primary_id";
		$cancel = "INSERT INTO $config[db_base_name].follow_customer_cancel(".$fCancel.",cancel_typeID) SELECT ".$fCancel.",'".$cancleList."' FROM ";
		$cancel .= "$config[db_base_name].follow_customer WHERE ".$where;
		$qCancel = $logDb->queryAndLogSQL( $cancel, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		//อัพเดตสถานะตามค่าที่หัวหน้าเลือก
		$sql = "UPDATE $config[db_base_name].follow_customer SET cancel_type='$cancelType', remark='$remark', status='$status', 
		process_by='$_SESSION[SESSION_ID_card]', process_date=NOW() WHERE $where ";
		mysql_query('SET NAMES UTF8');
		$result = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		
		//เก็บ Log ไฟล์
		/*if($config['log_cancel_follow']=='on'){
			$dataArray = array(
		  					'TITLE'	=> 'ขอยกเลิกการติดตามหลังการขาย',///** ถ้ามีจะง่ายในการอ่าน Log
		  					'Sent_Request_By'=>select_emp_nametonickname($_SESSION[SESSION_ID_card]),//ผู้ใช้ที่ต้องการเก็บล็อก
							'Sent_Request_Date'=>date('Y-m-d H:i:s'),
		  					'SQL'=>$sql,//ข้อมูลที่ต้องการเก็บ
							'FILE' 	=> __FILE__,//*** จำเป็นต้องมี
							'LINE'	=> __LINE__
			);
			create_log_file('follow_sent_request_cancel', $dataArray);//function/helper_create_log.php
		}*/
		//--
		
		if ($result) {
			$success_msg = "<h3 align='center' class='green'>ส่งคำขอ ยุติการติดตาม เรียบร้อยครับ</h3>";
			//เช็คว่าเหลือรายการที่ให้โทรหาหรือไม่
			$js = "<script type='text/javascript'>";
		
			$numCheck = getNumEvent($cus_no);
			if ($numCheck < 1 || $hideAll == true) {
				$js .= "hideObj('TR$cus_no,cus$cus_no');";
			}else {
				$js .= "hideObj('$hideCus');";
			}
			$js .= "</script>";
			echo $js;
		} else {
			$success_msg = "<p align='center' class='error'>ไม่สามารถบันทึกข้อมูลได้</p>";
		}
		
		echo $success_msg;
	break;
	case 'main_wait_for_approve': //โดดไปที่ หน้ารออนุมัติ ยกเลิกการติดตาม
		// echo 'YYYYYYY';
		echo "<script type='text/javascript'>";
		//ดึงข้อมูลมาแสดงใน <div id=follow_content>
		echo " browseCheck('follow_content','$followPage?operator=wait_for_approve','');";
		echo "</script>";
		echo $tpl->tbHtml($followHTML, 'MAIN_FOLLOW');
	break;

	case 'emplist':

		echo $tpl->tbHtml( $followHTML, 'EMP_TABLE' );

	break;

	case 'emplistSearch':

		$pload = new pLoadmore();

		// require_once 'show_progress.php';
		$this_month = (date('Y')+543).date('m');
		//$saleTemp_id = get_emp_saleTemp($_SESSION['SESSION_ID_card']);//หาไอดีของลูกน้องตัวเอง  function/ic_followcus.php
		//$saleTemp_id = substr(str_replace(",'".$_SESSION['SESSION_ID_card']."'", "", ",".$emp_id_card.","),1,-1);
		
		# P'Add 2012-11-28 DEV10534 
		// $arr_id_card = get_underling_position();
		if(!empty($arr_id_card)){
			$saleTemp_id = implode(",",$arr_id_card);
		}
		# End P'Add 2012-11-28 DEV10534 
		
		if($saleTemp_id){//ถ้ามีลูกน้อง
			$back	= "$followPage?operator=emplist";//สำหรับย้อนกลับ

			/*$team = "SELECT $config[db_base_name].SaleTemp.id_card,SaleLevel,CONCAT_WS(' ',Name,Surname,'(',Nickname,')') AS emp_name FROM $config[db_base_name].SaleTemp ";
			$team .= "LEFT JOIN $config[db_emp].emp_data ON $config[db_emp].emp_data.ID_card=$config[db_base_name].SaleTemp.id_card ";
			$team .= "WHERE id IN($saleTemp_id) AND Prosonnal_Being != '3' ";
			$team .= "ORDER by LENGTH(SaleLevel) desc,CONVERT(emp_name USING TIS620)";*/
			/*$team = "SELECT ID_Card AS id_card,Level AS SaleLevel,CONCAT_WS(' ',Name,Surname,'(',Nickname,')') AS emp_name FROM $config[db_emp].emp_data WHERE ";
			$team .= "ID_Card IN(".$saleTemp_id.") AND Prosonnal_Being!='3' ORDER BY Level DESC,CONVERT(emp_name USING TIS620)";*/
			
			/*
			$team = "SELECT rel.id_card,po.Name AS SaleLevel,NameUse AS emp_name FROM $config[db_organi].relation_position rel LEFT JOIN ";
			$team .= "$config[db_organi].position po ON rel.position_id=po.id WHERE rel.id_card IN(".$saleTemp_id.") AND rel.status!='99' AND po.Status!='99' ";
			$team .= "GROUP BY rel.id_card ORDER BY po.Level,rel.NameUse ASC";
			*/
			//$emp_pos_id
			$team = " SELECT rel.id_card,po.Name AS SaleLevel,NameUse AS emp_name ";
			$team .= " FROM $config[db_organi].relation_position rel";
			$team .= " INNER JOIN $config[db_organi].position po ON rel.position_id=po.id";
			$team .= " WHERE rel.position_id IN ($emp_pos_id) AND rel.id_card IN ($emp_id_card) AND rel.status!='99' AND po.Status!='99' ";
			$team .= " ORDER BY po.department,po.Level,po.PositionCode,rel.NameUse ASC ";
		
			$result = $pload->queryAndLoadmore($team, " FILE : ".__FILE__." LINE : ".__LINE__."");
			$total = mysql_num_rows($result);
			$no = $pload->start;
			while($rs = mysql_fetch_assoc($result)) {
				$num_resperson=$num_follow=$miss_calling=0;
				$no++;
				if($no % 2){$class = "class='emplist_tr1';";}else {$class = "class='emplist_tr2';";}

				$emp_id_card = $rs['id_card'];
				$emp_name 	= $rs['emp_name'];
				$emp_level	= $rs['SaleLevel'];
				$anchor 	= "emp_".$emp_id_card;//แถวที่ต้องการโฟกัส หลังจากคลิกปุ่ม ย้อนกลับ
				if($emp_name=="( )"){$emp_name=$emp_id_card;}

				//นับจำนวนลูกค้าทั้งหมด
				$num_resperson = phf_getNumResperson($rs,$config['db_maincus']);
				//นับจำนวนการแจ้งเตือนใน follw_customer
				$num_follow = getNumFollowCusEventByFlow('',array('id_card'=>$emp_id_card));
				//นับจำนวนรายการที่ไม่ได้ทำ
				//$miss_calling = get_num_miss_call($emp_id_card);
				$num_follow_person = getNumFollowCusEvent2('',array('id_card'=>$emp_id_card), $emp_id_card);

				$onclick = "onclick=\"browseCheck('main','$followPage?operator=cuslist&id_card=$emp_id_card&back=$back&anchor=$anchor','');\" ";//กำลังติดตาม
				$respersonColor=$followColor=$missCallColor="gray";
				$font=$font1=$font2="";

				if($num_resperson){$respersonColor="orangered";$font="14px";}
				if($num_follow){$followColor = "green";$font1="14px";}
				if($miss_calling){$missCallColor="red";$font2="14px";}

				$emplist .= $tpl->tbHtml( $followHTML, 'EMP_LIST' );

				//เขียนแสดงแถบสถานะ
				// if($config['open_progress']=='on'){
				// 	create_progress($no,$total,'emplist');//show_progress.php
				// }
				$pload->eachLoadmore();
			}//while พนักงานคนต่อไป
		}
		/*$result = mysql_query("SELECT MAX(start_month) FROM $config[db_base_name].SaleTemp ");
		$max = mysql_fetch_array($result);
		$thismonth = date('Y')+543 . date('m');
		if($max[0] < $thismonth){
			$oldTeam = " หรือ ยังไม่ได้จัดแบ่งทีมในเดือนนี้ครับ.";
		}*/
		if($emplist==""){
			$colspan = '5';
			$config['null'] = "ไม่มีข้อมูลพนักงานในความรับผิดชอบ $oldTeam";
			$emplist = $tpl->tbHtml( $followHTML, 'NULL' );
		}

		echo $emplist;
		$pload->endLoadmore();
		
		#echo $emp_id_card;

	break;

	case 'EditEmpCus':
		require_once 'show_progress.php';
		$this_month = (date('Y')+543).date('m');
		//$saleTemp_id = get_emp_saleTemp($_SESSION['SESSION_ID_card']);//หาไอดีของลูกน้องตัวเอง  function/ic_followcus.php
		//$saleTemp_id = substr(str_replace(",'".$_SESSION['SESSION_ID_card']."'", "", ",".$emp_id_card.","),1,-1);
		
		# P'Add 2012-11-28 DEV10534 
		// $arr_id_card = get_underling_position();
		if(!empty($arr_id_card)){
			$saleTemp_id = implode(",",$arr_id_card);
		}
		# End P'Add 2012-11-28 DEV10534 
		
		if($saleTemp_id){//ถ้ามีลูกน้อง
			$back	= "$followPage?operator=emplist";//สำหรับย้อนกลับ

			/*$team = "SELECT $config[db_base_name].SaleTemp.id_card,SaleLevel,CONCAT_WS(' ',Name,Surname,'(',Nickname,')') AS emp_name FROM $config[db_base_name].SaleTemp ";
			$team .= "LEFT JOIN $config[db_emp].emp_data ON $config[db_emp].emp_data.ID_card=$config[db_base_name].SaleTemp.id_card ";
			$team .= "WHERE id IN($saleTemp_id) AND Prosonnal_Being != '3' ";
			$team .= "ORDER by LENGTH(SaleLevel) desc,CONVERT(emp_name USING TIS620)";*/
			/*$team = "SELECT ID_Card AS id_card,Level AS SaleLevel,CONCAT_WS(' ',Name,Surname,'(',Nickname,')') AS emp_name FROM $config[db_emp].emp_data WHERE ";
			$team .= "ID_Card IN(".$saleTemp_id.") AND Prosonnal_Being!='3' ORDER BY Level DESC,CONVERT(emp_name USING TIS620)";*/
			
			/*
			$team = "SELECT rel.id_card,po.Name AS SaleLevel,NameUse AS emp_name FROM $config[db_organi].relation_position rel LEFT JOIN ";
			$team .= "$config[db_organi].position po ON rel.position_id=po.id WHERE rel.id_card IN(".$saleTemp_id.") AND rel.status!='99' AND po.Status!='99' ";
			$team .= "GROUP BY rel.id_card ORDER BY po.Level,rel.NameUse ASC";
			*/
			//$emp_pos_id
			$team = " SELECT rel.id_card,po.Name AS SaleLevel,NameUse AS emp_name ";
			$team .= " FROM $config[db_organi].relation_position rel";
			$team .= " INNER JOIN $config[db_organi].position po ON rel.position_id=po.id";
			$team .= " WHERE rel.position_id IN ($emp_pos_id) AND rel.id_card IN ($emp_id_card) AND rel.status!='99' AND po.Status!='99' ";
			$team .= " ORDER BY po.department,po.Level,po.PositionCode,rel.NameUse ASC ";
		
			$result = $logDb->queryAndLogSQL( $team, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$total = mysql_num_rows($result);
			while($rs = mysql_fetch_assoc($result)) {
				$num_resperson=$num_follow=$miss_calling=0;
				$no++;
				if($no % 2){$class = "class='emplist_tr1';";}else {$class = "class='emplist_tr2';";}

				$emp_id_card = $rs['id_card'];
				$emp_name 	= $rs['emp_name'];
				$emp_level	= $rs['SaleLevel'];
				$anchor 	= "emp_".$emp_id_card;//แถวที่ต้องการโฟกัส หลังจากคลิกปุ่ม ย้อนกลับ
				if($emp_name=="( )"){$emp_name=$emp_id_card;}

				//นับจำนวนลูกค้าทั้งหมด
				$num_resperson = phf_getNumResperson($rs,$config['db_maincus']);
				//นับจำนวนการแจ้งเตือนใน follw_customer
				$num_follow = getNumFollowCusEventByFlow('',array('id_card'=>$emp_id_card));
				//นับจำนวนรายการที่ไม่ได้ทำ
				//$miss_calling = get_num_miss_call($emp_id_card);

				$onclick = "onclick=\"browseCheck('main','$followPage?operator=cuslist&id_card=$emp_id_card&back=$back&anchor=$anchor','');\" ";//กำลังติดตาม
				$respersonColor=$followColor=$missCallColor="gray";
				$font=$font1=$font2="";

				if($num_resperson){$respersonColor="orangered";$font="14px";}
				if($num_follow){$followColor = "green";$font1="14px";}
				if($miss_calling){$missCallColor="red";$font2="14px";}

				$edit_emp_list .= '<option id="'.$emp_id_card.'" empname="'.$emp_name.'" value="'.$emp_id_card.'">'.$emp_name.'</option>';
				#$edit_emp_list .= '<input type="hidden" name="'.$emp_name.'">';

				//เขียนแสดงแถบสถานะ
				if($config['open_progress']=='on'){
					create_progress($no,$total,'emplist');//show_progress.php
				}
			}//while พนักงานคนต่อไป
		}
		/*$result = mysql_query("SELECT MAX(start_month) FROM $config[db_base_name].SaleTemp ");
		$max = mysql_fetch_array($result);
		$thismonth = date('Y')+543 . date('m');
		if($max[0] < $thismonth){
			$oldTeam = " หรือ ยังไม่ได้จัดแบ่งทีมในเดือนนี้ครับ.";
		}*/
		if($edit_emp_list ==""){
			$colspan = '5';
			$config['null'] = "ไม่มีข้อมูลพนักงานในความรับผิดชอบ $oldTeam";
			$edit_emp_list = $tpl->tbHtml( $followHTML, 'NULL' );
		}

		echo $edit_emp_list;
	break;

	case 'archives': //รายการของพนักงานที่โทรหาลูกค้าแล้ว (ประวัติการโทรหาของพนักงานขาย) และ รายการที่ไม่ได้ทำ
		//-- ถ้าส่งค่ามาจากหน้ารายชื่อพนักงานของ Super Manager
		$id_card = $_GET['id_card'];
		if($id_card){
			$emp = select_emp_nametonickname($_GET['id_card']);
			$title = "รายการที่เคยสัมภาษณ์ไว้";
			$color = "black";
			$bg		= "#eeeeee";
			if($_GET['type']=="misscall"){
				$title = "รายการไม่ได้โทรหาลูกค้า";
				$color = "red";
				$bg		= "#FFFF00";
				$th_bg	= "style='background-color:#8B1A1A'";
				$misscall = "type=misscall";
			}
			$emp_response = "<div align='left' style='width:98.8%;padding:10px;color:$color;background:$bg' class='title3'>";
			$emp_response .= "$title ของ :: <font color='blue'>$emp</font> [พนักงานผู้ดูแลลูกค้า]</div>";
			$getURL = "id_card=".$id_card;
			$check_super_enter = true;
			$menu	= $_SESSION['follow_menu'];
		}
		if($id_card == ""){
			$check_super_enter = false;
			$id_card = $_SESSION['SESSION_ID_card'];
		}
		if($_GET['year'] && $_GET['month']){
			$year 	= $_GET['year'];
			$month 	= $_GET['month'];
			$timestamp = mktime(0,0,0, $month, 1, $year);
			$day 	= date('t',$timestamp);//วันที่สุดท้ายของเดือน
		}else{
			$year 	= date('Y');
			$month 	= date('m');
			$day 	= date('t');
		}
		//-- ถ้าส่งค่าย้อนกลับมาด้วย
		if($_GET['back']){
			$_SESSION['arch_back'] = $_GET['back'];
			$_SESSION['arch_anchor']= $_GET['anchor'];
		}
		#-- วันที่แรกและวันที่สุดท้ายของเดือน
		$begindate = "$year-$month-01";
		$enddate = "$year-$month-$day";
		#-- กล่องเลือกวันที่
		$selectMonth = genSelectMonth($month);
		$selectYear  = genSelectYear($year);
		$thdate = standard2thaidate($year."-".$month,"-");
		$date_report = $thdate['text'];
		#-- table
		$tb_follow = "$config[db_base_name].follow_customer";
		$tb_event = "$config[db_base_name].follow_main_event";
		#-- รายการที่ทำแล้ว ค่า defualt
		if($_GET['type']==""){
			$date_condition		= "(DATE(process_date) BETWEEN '$begindate' AND '$enddate') ";
			$status 					= "$tb_follow.status ='2' ";
			$option_not_misscall = '<option value="process_date_asc" $process_date_asc> วันที่ทำ ก่อน - หลัง </option>
									<option value="process_date_desc" $process_date_desc> วันที่ทำ หลัง - ก่อน </option>';
			$cls[1] = "class='archives_tr1';";
			$cls[2] = "class='archives_tr2';";
		#-- รายการที่ไม่ได้ทำ
		}else if($_GET['type']=="misscall"){//ถ้าส่งมาจากลิงค์ รายการที่ไม่ได้ทำ
			$date_condition 		= "(stop_date >= '$begindate' AND stop_date <= '$enddate') ";
			$status 			 		= "$tb_follow.status ='98' ";
			$option_not_misscall = '';
			$cls[1] = "class='misscall_tr1';";
			$cls[2] = "class='misscall_tr2';";
		}
		if($_GET['cus_no']){ $fixCusno = "AND follow_customer.cus_no = '".$_GET['cus_no']."' ";}
		$getURL = 'cus_no='.$_GET['cus_no'].'&id_card='.$id_card;
		
		$empOu = select_empOu($id_card); //function/general.php
		//เข้าฟังชั่น เรียงเงื่อนไข select ข้อมูลที่จะแสดง
		$whereOu = condition_sortOU($empOu,$tb_follow); //function/general.php
		
		#-- รายการทั้งหมด
		$totalNum = query("SELECT count(id) FROM $tb_follow WHERE $whereOu AND $status $fixCusno");
		#-- SQL
		$sql = "SELECT $tb_follow.id AS follow_id,event_id_ref,$tb_follow.follow_type,chassi_no_s,cus_no,cus_name,emp_id_card,start_date,";
		$sql .= "stop_date,process_by,DATE(process_date) AS process_date,$tb_follow.status AS follow_status,$tb_event.event_title ";
		$sql .= "FROM $tb_follow LEFT JOIN $tb_event ON $tb_follow.event_id_ref=$tb_event.id ";
		$sql .= "WHERE $whereOu AND $date_condition AND $status $fixCusno";

		#-- Page view
		$onclick['div'] = "main"; //เลเยอร์ที่ต้องการแสดงผล
		$onclick['url'] = "$followPage?operator=archives&$getURL&$misscall"; //url เมื่อคลิกเลขหน้า
		$val = createPageView($sql,$_GET['pageNumber'],$onclick);//return ค่ากลับเป็น อาร์เรย์
		$pager = $val['pageview']; //เอาไปวางจุดที่ต้องการแสดงหมายเลขหน้า
		$goto = $val['goto']; // ค่าเริ่มต้น query ข้อมูล
		#-- SET order
		if($_POST['order']){//สร้าง session ใหม่ถ้ามีการเปลี่ยนการจัดเรียง
			$_SESSION["SESS_order"] = $_POST['order'];
		}
		$orderby = set_order($_SESSION["SESS_order"]);
		$limit = $config['page_limit']; //จำนวนรายการที่ต้องการ

		$sql .= "ORDER by $orderby ";
		$sql .= "LIMIT $goto,$limit";

		$selectIndex	= $_SESSION["SESS_order"];
		$$selectIndex	= "selected='selected'"; //$$selectIndex เป็นการต่อตัวแปรเพื่อเลือก selected ตาม value ที่ส่งมา

		//$border = "border-top : 1px solid #828282;";
		$a = 0;
		mysql_query('SET NAMES UTF8');
		$follow = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		while($rs = mysql_fetch_assoc($follow)) {
			$border = '';
			if($rs['cus_no']!=$prevCusno){ //ถ้า cus_no เปลี่ยน ให้เปลี่ยนสีพื้นหลัง
				//$border = "border-top : 2px solid #4F4F4F;";
				$a++;
				if($a>2){$a=1;}
			}
			$prevCusno 	= $rs['cus_no'];
			$class 		= $cls[$a];

			$follow_id	 = $rs['follow_id'];
			$cus_no 	 = $rs['cus_no'];
			$cus_name 	 = $rs['cus_name'];
			$event_title = $rs['event_title'];
			$event_id	 = $rs['event_id_ref'];
			$chassi_no_s = $rs['chassi_no_s'];

			$start  	= standard2thaidate($rs['start_date'],'-');
			$start_date = "<div title='$start[text]'>$start[number]</div>"; //วันที่เริ่มติดต่อ
			$stop  		= standard2thaidate($rs['stop_date'],'-');
			$stop_date 	= "<div title='$stop[text]'>$stop[number]</div>"; //วันสุดท้าย
			if($rs['follow_status'] == "98"){//ไม่ได้ทำ
				$process_date = "<div style='color:red;font-weight:bold'>ไม่ได้ทำ</div>"; //วันที่ทำรายการ (สำหรับ รายการที่ไม่ได้ทำ)
			}else {
				$do_date  = standard2thaidate($rs['process_date'],'-');
				$process_date = "<div title='$do_date[text]'>$do_date[number]</div>"; //วันที่ทำรายการ (สำหรับรายการที่ทำแล้ว)
			}
			/*$select = "Cus_Type,Time_for_talk,Pleasure_value,M_Tel,H_Tel,W_Tel";
			$cusArr = getMainCusDataFormCusNo($cus_no,'',$select); // funcion/general.php*/
			//ข้อมูลลูกค้า
			$cusArr = selectDB("$config[db_maincus].MAIN_CUS_GINFO","Time_for_talk","CusNo='$cus_no' LIMIT 1");
			//หาไอดีประเภทลูกค้า
			$typeID = selectDB("$config[db_maincus].MIAN_CUS_TYPE_REF","CTYPE_GRADE_REF","CTYPE_REF_CUSNO='$cus_no' LIMIT 1");
			//ประเภทลูกค้า
			$cusType = selectDB("$config[db_maincus].AMIAN_CUS_TYPE","CUS_TYPE_LEVEL_DESC","CUS_TYPE_ID='".$typeID['CTYPE_GRADE_REF']."' LIMIT 1");
			//เบอร์มือถือ
			$telMobile = selectDB("$config[db_maincus].MAIN_TELEPHONE","TEL_NUM","TEL_CUS_NO='$cus_no' AND TEL_TYPE='1' LIMIT 1");
			//เบอร์บ้าน
			$telHome = selectDB("$config[db_maincus].MAIN_TELEPHONE","TEL_NUM","TEL_CUS_NO='$cus_no' AND TEL_TYPE='3' LIMIT 1");
			//เบอร์ที่ทำงาน
			$telWork = selectDB("$config[db_maincus].MAIN_TELEPHONE","TEL_NUM","TEL_CUS_NO='$cus_no' AND TEL_TYPE='2' LIMIT 1");

			#$cus_info  = "<a href='javascript:void(0);' onclick=\"NewWindow('$followPage?operator=getMainCusData&amp;cus_no=$cus_no');\"><img src='img/directory.gif' border='0' title='ดูข้อมูลประวัติส่วนตัวลูกค้า'></a>";
			$cus_info  = "<a href='javascript:void(0);' onclick=\"javascript:window.open('$view_detail_cuss"."$cus_no','newWin1','height=600,width=1000,top=0,left=0,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes,directories=no,status=no,titlebar=no');\"><img src='img/directory.gif' border='0' title='ดูข้อมูลประวัติส่วนตัวลูกค้า'></a>";
			$cus_level = $cusType['CUS_TYPE_LEVEL_DESC']; // เกรดของลูกค้า
			$pleasure_value = $cusArr["Pleasure_value"];
			$cus_telnumber  = checkNull($telMobile['TEL_NUM'])." / ".checkNull($telHome['TEL_NUM'])." / ".checkNull($telWork['TEL_NUM']);
			$time_for_talk  = $cusArr["Time_for_talk"];

			if($_GET['type']=="misscall"){//ถ้าส่งมาจากลิงค์ รายการที่ไม่ได้ทำ
				$followdata = "follow_cus_id=$follow_id&follow_type=$rs[follow_type]&chassi_no_s=$chassi_no_s&cus_no=$cus_no&cus_name=$rs[cus_name]&emp_id_card=$id_card&event_id=$event_id&event_title=$event_title&readonly=readonly";
				$url1 = "$followPage?operator=getEventScript&misscall=misscall&$followdata";
				//ไม่ต้องแสดงแบบฟอร์ม $url2 = "$followPage?operator=getEventQuestion&$followdata";
			}else {//ถ้าเป็นรายการที่เคยทำ
				if($check_super_enter == true){
					$url1 = "$followPage?operator=lastCall&follow_id=$follow_id&event_id=$event_id";
					$url2 = "";
				}
			}

			$click = "breake=false;showQuestionDetail('$url1','$url2','TR$follow_id');changeObject('bt_script','div_script');";
			//ใช้ HTML เทมเพลต เดียวกันกับ รายการที่ทำแล้ว
			$cus_list .= $tpl->tbHtml( $followHTML, 'ARCHIVES_LIST' );
		}

		if($cus_list ==""){
			$colspan = 8;
			$cus_list = $tpl->tbHtml( $followHTML, 'NULL' );
		}

		echo $tpl->tbHtml( $followHTML, 'ARCHIVES_HEAD' );
		$bt_script_display = 'none';
		$bt_script = " ข้อมูลการทำรายการ ";
		echo $tpl->tbHtml( $followHTML, 'POPUP_BLOCK' );//ป๊อบอัพแสดงแบบฟอร์ม
	break;
	
	case 'wait_for_approve':

		$checkbox_head = '<th width="30" style="background-color:#5693F5;padding: 8px 3px;">เลือก</th>';
		$input_submit = '<div class="nz_button BTwhite" onclick="if(checkBox(\'wait_approve\')==true && confirm(\'ยืนยันการ อนุมัติ ยกเลิกการติดตาม ของรายการที่เลือก ?\')==true){followFormSubmit(\'show_success\',\'$followPage?operator=cancel_follow&action=yes\',\'cancel_type='.$_GET[cancel_type].'&cus_no='.$_GET[cus_no].'\',\'\',\'\');}">
				<img src="img/allow.gif" align="absmiddle" width="18"> อนุมัติ ยกเลิกการติดตาม
				</div>
				<div class="nz_button BTwhite" onclick="if(checkBox(\'wait_approve\')==true && confirm(\'ยืนยัน ไม่อนุมัติ รายการที่เลือก ?\')==true){followFormSubmit(\'show_success\',\'$followPage?operator=cancel_follow&action=no\',\'\',\'\',\'\');}">
				<img src="img/not_allow.png" align="absmiddle" width="18"> ไม่อนุมัติ
				</div>';

		echo $tpl->tbHtml("$followHTML","WAIT_APPROVE_HEADER");

	break;

	case 'wait_for_approveSearch'://รายการที่รออนุมัติ ยกเลิกการติดตาม

		$pload = new pLoadmore();
		
		//$emp_id_card = get_emp_saleTemp($_SESSION['SESSION_ID_card'],'id_card');
		//$emp_id_card = getEmp_follower($_SESSION['SESSION_ID_card']); //function/general.php

		if($emp_id_card){//ถ้ามีรายชื่อลูกน้อง
			if($_GET['order']){//สร้าง session ใหม่ถ้ามีการเปลี่ยนการจัดเรียง
				$_SESSION["SES_order"] = $_GET['order'];
			}
			$selectIndex	= $_SESSION["SES_order"];
			$select 		= "selected='selected'";
			if(empty($selectIndex)){
				$selectIndex = "cusno_asc";
				$select = "";
			}
			$orderby = set_order($selectIndex);
			$$selectIndex	= $select; //$$selectIndex เป็นการต่อตัวแปรเพื่อเลือก selected ตาม value ที่ส่งมา

			$tb_event = "$config[db_base_name].follow_main_event";
			$tb_follow = "$config[db_base_name].follow_customer";
			$tb_member = "$config[db_emp].emp_data";

			$join_emp = '';
			if(strpos($selectIndex,'empname')!==FALSE){//ถ้าเรียงตามชื่อพนักงานจะต้อง JOIN
				$join_emp = "LEFT JOIN $tb_member ON $tb_follow.emp_id_card=$tb_member.ID_card ";
				$emp_select_field = ",CONCAT_WS(' ',Name,Surname,'(',Nickname,')') as emp_name";
			}

			//ถ้าคลิกเข้าไปในแต่ละชนิด
			$aprove_where = "";
			$group_by = "GROUP by $tb_follow.cus_no,$tb_follow.cancel_type ";
			// if($_GET['approve']!=''){
				// $checkbox_head = '<th width="30" style="background-color:#5693F5;padding: 8px 3px;"><input type="checkbox" id="checkAll" onclick="nz_toggle_check(this,\'wait_approve\');" style="width:100px;height:50px"></th>';
				// $aprove_where = "AND cus_no = '$_GET[cus_no]' AND cancel_type = '$_GET[cancel_type]'";
				// $group_by = '';
				// if($_GET[cancel_type]=='stopCus'){
				// 	$group_by = 'GROUP by cus_no ';
				// }else if($_GET[cancel_type]=='stopCar'){
					//$group_by = 'GROUP by chassi_no_s ';
				// }//ปิดไว้ก่อน คิดว่าต้องแสดงทุกเหตุการณ์เพื่อเลือกว่าจะอนุมัติเหตุการณ์ไหนบ้าง
			// }

			//2011-06-21 nz
			//$sql = "SELECT $tb_follow.id as id, cus_no, cus_name, chassi_no_s, cancel_type, $tb_event.event_title,emp_id_card,remark,CONCAT_WS(' ',Name,Surname,'(',Nickname,')') as emp_name FROM $tb_follow ";
			$sql = "SELECT $tb_follow.id as id, cus_no, cus_name, chassi_no_s, cancel_type, $tb_event.event_title,emp_id_card,remark,
			process_by,process_date $emp_select_field FROM $tb_follow ";
			$sql .= "LEFT JOIN $tb_event ON $tb_follow.event_id_ref=$tb_event.id ";
			//2011-06-21 nz
			//$sql .= "LEFT JOIN $tb_member ON $tb_follow.emp_id_card=$tb_member.ID_card ";
			$sql .= $join_emp;

			/*if($CheckManager==$config['Manager']){//ถ้าเป็น Manager && Admin จะเห็นทั้งหมด
				$where = "";
			}else {
				$where = " AND $tb_follow.emp_id_card IN($emp_id_card) ";//รายชื่อลูกน้อง
			}*/
			$where = " AND ($tb_follow.process_by IN($emp_id_card) OR $tb_follow.emp_id_card IN($emp_id_card))";//รายชื่อลูกน้อง
			
			/*SELECT main_crm.follow_customer.id as id, cus_no, cus_name, chassi_no_s, cancel_type, main_crm.follow_main_event.event_title,emp_id_card,remark, process_by
			,process_date FROM main_crm.follow_customer LEFT JOIN main_crm.follow_main_event ON main_crm.follow_customer.event_id_ref=main_crm.follow_main_event.id 
			WHERE main_crm.follow_customer.status = '90' AND main_crm.follow_customer.emp_id_card IN('1570700097671','1560100039590','1599999999999','1579900236525',
			'3570300266332','1570400099474','5570700136238','1111111111111','2222222222222','1570500002839','0550500078606') GROUP by main_crm.follow_customer.cus_no,
			main_crm.follow_customer.cancel_type ORDER by cus_no ,stop_date*/

			$sql .= "WHERE $tb_follow.status = '90' $where $aprove_where
					$group_by
					ORDER by $orderby";
			// echo $sql;
					
			$result = $pload->queryAndLoadmore($sql, " FILE : ".__FILE__." LINE : ".__LINE__."");
			$numrow = mysql_num_rows($result);
			$cls[1] = "emplist_tr1";
			$cls[2] = "emplist_tr2";
			$a = 1;
			$numRow = $pload->start;
			while($rs = mysql_fetch_assoc($result)) {
				$numRow++;
				if($a>2){$a=1;}
				if($oldCus==$rs['cus_no']){
					$class = $oldClass;
				}else {
					$class = $cls[$a];
					$a++;
				}
				$oldClass = $class;
				$oldCus = $rs['cus_no'];

				$cus_arr = selectDB($config['db_maincus'].".MAIN_RESPONSIBILITY","RESP_IDCARD AS Resperson","RESP_CUSNO='".$rs['cus_no']."'");
				$rs['emp_name'] = select_emp_nametonickname($cus_arr['Resperson']);

				/* ปิดส่วนของการดึงชื่อแผนกมาแสดง
				if( strlen($rs['emp_id_card'])<13){//ถ้าไม่ใช่ รหัสประชาชน ให้หาชื่อแผนก
					$rs['emp_name']= get_department_from_cut($rs['emp_id_card']);// function/ic_followcus_form.php
				}
				*/
				$rs['cancel_by'] = select_emp_nametonickname($rs['process_by']);
				$rs['cancel_date'] = $rs['process_date'];

				$rs['no'] = $numRow;
				switch($rs['cancel_type']) {
					case 'stopCus'://หยุดทุกการติดตามของ cus_no นี้
						$rs['cancelType'] = 'ยกเลิกการดูแลลูกค้า';
						// if($_GET['approve']==''){$rs['event_title']='ยกเลิกกิจกรรมทั้งหมดของลูกค้า';}
					break;
					case 'stopCar'://หยุดทุกการติดตามของ chassi_no_s คันนี้
						$rs['cancelType'] = 'ยกเลิกการดูแลรถ';
						// if($_GET['approve']==''){$rs['event_title']='ยกเลิกกิจกรรมทั้งหมดของรถที่เลือก';}
					break;
					case 'stopEvent':
						$rs['cancelType'] = 'ยกเลิกกิจกรรม';
						// if($_GET['approve']==''){$rs['event_title']='ยกเลิกกิจกรรมเลือก';}
					break;
					case 'stopTemporary':
						$rs['cancelType'] = 'ยกเลิกชั่วคราว';
						// if($_GET['approve']==''){$rs['event_title']='ยกเลิกชั่วคราว';}
					break;
					default:
						$rs['cancelType'] = 'ยกเลิก... ';
						// if($_GET['approve']==''){$rs['event_title']='ยกเลิกกิจกรรม';}
					break;
				}

				// if($_GET['approve']!=''){//ถ้าคลิกเข้าไปในแต่ละชนิด
				$rs['checkbox_list'] = '<td align="center" style="padding: 8px 3px;"><input type="checkbox" name="check[]" id="check'.$rs[id].'" value="'.$rs[id].'" class="wait_approve"></td>';
				$cancel_link = $rs[cancelType];
				//ถ้ามีเลขเครื่อง
				$chassi_no_s = $rs['chassi_no_s'];
				if($rs['cancel_type']=='stopCus'){
					$rs['event_title'] = 'ยกเลิกทุกกิจกรรม';
				}else {

					if($chassi_no_s!=""){
						//$rss = query("SELECT B_Unit_Model,Eng_No_B FROM $config[db_base_name].Booking WHERE Chassi_No_B = '$chassi_no_s' ",1);
						//$reg_no = query("SELECT reg_no FROM $config[db_base_name].Sell WHERE Chassi_No_S = '$chassi_no_s' ");
						$sqlVe = "SELECT VEHICLE_MODEL_NAME,VEHICLE_ENGINES,VEHICLE_FULL_CHASSIS,VEHICLE_CHASSIS,VEHICLE_REGIS FROM $config[db_vehicle].VEHICLE_INFO ";
						$sqlVe .= "WHERE VEHICLE_ID='".$chassi_no_s."' AND VEHICLE_STATUS!='99' LIMIT 1";
						$queVe = $logDb->queryAndLogSQL( $sqlVe, " FILE : ".__FILE__." LINE : ".__LINE__."" );
						$feVe = mysql_fetch_assoc($queVe);
						
						if($feVe['VEHICLE_REGIS']==""){ $feVe['VEHICLE_REGIS']=' ยังไม่มีทะเบียนรถ'; }
						if($feVe['VEHICLE_MODEL_NAME']==""){ $feVe['VEHICLE_MODEL_NAME']='-'; }
						if($feVe['VEHICLE_ENGINES']==""){ $feVe['VEHICLE_ENGINES']='-'; }
						
						if($feVe['VEHICLE_FULL_CHASSIS']){ $feVe['CHASSIS'] = $feVe['VEHICLE_FULL_CHASSIS']; }
						else if($feVe['VEHICLE_CHASSIS']){ $feVe['CHASSIS'] = $feVe['VEHICLE_CHASSIS']; }
						else{ $feVe['CHASSIS'] = '-'; }
						
						//รุ่นรถ / เลขเครื่อง / เลขแซสซีส์ / ทะเบียน
						$rs['event_title'] .= ' <span style=\'color:orange\'>( '.$feVe['VEHICLE_MODEL_NAME'].' / ' .$feVe['VEHICLE_ENGINES'].' / ';
						$rs['event_title'] .= $feVe['CHASSIS'].' / '.$feVe['VEHICLE_REGIS'].' )</span>';
					}
				}
				// }else {
				// 	$cancel_link = '<a href="javascript:void(0)" onclick="javascript:browseCheck(\'follow_content\',\'$followPage?operator=wait_for_approve&cus_no=$rs[cus_no]&cancel_type=$rs[cancel_type]&approve=true\',\'\');hideObj(\'divMainDetail,cursor\');" style="text-decoration:underline">$rs[cancelType] <img src="img/white_edit.png"></a>';
				// }
				$wait_app_list .= $tpl->tbHtml("$followHTML","WAIT_APPROVE_LIST");

				$pload->eachLoadmore();

			}
		}

		if ($wait_app_list == "") {
			$colspan = '10';
			$config['null'] = "ไม่มีรายการรออนุมัติยกเลิกการติดตาม";
			$wait_app_list = $tpl->tbHtml( $followHTML, 'NULL' );
		}
		echo $wait_app_list;
		$pload->endLoadmore();
		
	break;

	case 'wait':	//CR  ข้อมูลที่ส่งต่อ รอสัมภาษณ์
		getNumInterviewEvent();// จะนับจำนวน $_SESSION[numInterview] และสุ่มอีกครั้ง
		echo "<script type='text/javascript'>document.getElementById('wait').innerHTML = '$_SESSION[numInterview]';</script>";

		$idCard = $_SESSION['SESSION_ID_card'];
		$backURL = "back_url=$followPage?operator=wait&back_display=follow_content&pageNumber=$_GET[pageNumber]";//สำหรับปุ่ม back
/* ใช้ left join แล้ว
		$sql = "SELECT id FROM $config[db_base_name].follow_main_event ";
		$sql .= "WHERE (department='".$checkEventDepartment."' OR who_response = '$idCard') AND pattern_id_ref = '' ";
		$fl_event_id_in = select_to_sql_in($sql);

		$fl_event_id	= str_replace("'","",$fl_event_id_in);//รหัสเหตุการณ์ที่รับผิดชอบ  1,7,5,...,n (สำหรับส่งผ่าน GET)
*/
		if($_POST['order']){//การจัดเรียง
			$_SESSION["SESS_wait_order"] = $_POST['order'];
		}
		$selectIndex	= $_SESSION["SESS_wait_order"];
		$select 		= "selected='selected'";
/* ค่าเริ่มต้นที่ต้องการ
		if(empty($selectIndex)){
			$selectIndex = "cusno_asc";
			$select = "";
		}
*/
		$order = set_order($selectIndex);// function/ic_followcus.php
		$$selectIndex	= $select; //$$selectIndex เป็นการต่อตัวแปรเพื่อเลือก selected ตาม value ที่ส่งมา

		if($_GET['select_event']){//กรองชื่อลูกค้าตาม Event
			$other_where = " AND follow_customer.event_id_ref = '$_GET[select_event]' ";
		}

		//ดึงมาเฉพาะเหตุการณ์รอสัมภาษณ์ของ CR pattern_id_ref ต้องเป็นค่าว่าง
		$sql = "SELECT id,event_title FROM $config[db_base_name].follow_main_event
			WHERE (department='".$checkEventDepartment."'
				OR who_response = '$idCard'
			) AND pattern_id_ref = ''  AND status != '99'  AND visible != 'no'";

		$arr_opt = array(
						'value_field'=>'id', // ฟิลด์ที่จะใช้เป็น value ของ option
						'text_field'=>'event_title',	// ฟิลด์ที่จะใช้เป็น text ของ option
						'select_value'=>$_GET['select_event'],	// ตัวเลือกเริ่มต้น
				);
		$event_dropdown = get_event_dropdown($sql, $arr_opt );


		#-- status = 4 = รอสัมภาษณ์ ดังนั้น emp_id_card จะเท่ากับคนสัมภาษณ์ ไม่ใช่ของ sale ที่ทำเหตุการณ์ก่อนหน้านี้
		$sql = "SELECT cus_no,cus_name,process_by,follow_customer.id as id,MIN(stop_date) AS stop_date FROM $config[db_base_name].follow_customer ";
		$sql.= "LEFT JOIN $config[db_base_name].follow_main_event ON follow_customer.event_id_ref = follow_main_event.id ";
		$sql.= "WHERE follow_customer.status = '4'
				AND (
					follow_main_event.department='".$checkEventDepartment."'
					OR follow_main_event.who_response = '$idCard'
					OR ( follow_customer.emp_id_card LIKE '%$idCard%' AND follow_main_event.department = 'Leader')
				)
				AND follow_main_event.pattern_id_ref = ''  AND follow_main_event.status !='99'
				AND follow_main_event.visible !='no' $other_where ";
		$sql.= " GROUP by cus_no ";

		$onclick['div'] = "follow_content"; //เลเยอร์ที่ต้องการแสดงผล
		$onclick['url'] = "$followPage?operator=wait&$getURL"; //url เมื่อคลิกเลขหน้า
		$val = createPageView($sql,$_GET['pageNumber'],$onclick);//return ค่ากลับเป็น อาร์เรย์
		$pager = $val['pageview']; //เอาไปวางจุดที่ต้องการแสดงหมายเลขหน้า
		$goto = $val['goto']; // ค่าเริ่มต้น query ข้อมูล
		$limit = $config['page_limit']; //จำนวนรายการที่ต้องการ

		$sql .= "ORDER by $order LIMIT $goto,$limit";
		$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );

		while($rs=mysql_fetch_array($re)){
			++$a;
			if($a % 2){	$class = "class='event_tr1'";}else {$class = "class='event_tr2'";}

			$follow_id	= $rs['id'];
			$cus_no		= $rs['cus_no'];
			$cus_name 	= $rs['cus_name'];
			$process_by = $rs['process_by'];//เก็บ ID_card ผู้ที่จะสัมภาษณ์ (เพราะเป็นเหตุการณ์ส่งต่อ process_by จึงเป็นค่าว่าง ถ้าจะดูชื่อ sale ที่ทำรายการก่อนหน้า
			//  ให้ใช้ emp_id_card หรือไปดูเหตุการณ์ที่ส่งมาในตาราง follow_event_refer แล้วหา Max ID ของเหตุการณ์นั้น และลูกค้าคนนั้นๆ ที่ status = '2')
			$date_exp	= standard2thaidate($rs['stop_date'],'-');
			$date_expire = "<div title='$date_exp[text]'>$date_exp[number]</div>"; //วันสุดท้ายของการติดต่อ

			$arr 		= getMainCusDataFormCusNo($cus_no,'','Cus_Type');
			$cus_level 	= $arr['Cus_Type'].$arr['CusNo'];

			$TRonclick = "if(confirm('คุณเลือกทำรายการของลูกค้ารายนี้? โปรแกรมจะทำการจองรายการนี้ไว้สำหรับคุณ.')==true){openCusEvent('follow_content','$followPage?operator=wait_main_event&event_id_ref=$fl_event_id&cus_no=$rs[cus_no]&$backURL','$rs[id]');}";//1579900078539

			$booking = "";
			$booking_name = "";
			$display = "none";
			$bgColor = "#FFFF99";
			$cancel = "";
			$title	= "";
			//if($process_by){
			if(strlen($process_by)==13 && abs($process_by) ){
				$title	= "จองไว้โดยคุณ $booking_name";
				$booking_name 	= select_emp_nametonickname($process_by);
				$booking 		= "<img src='img/process_by.png'><span id = 'booking_emp_name".$cus_no."'>จองไว้โดย :: <font color='black'>$booking_name</font></span>";
				$display = "";
				if($_SESSION['SESSION_ID_card'] != $process_by){
					$TRonclick	= "alert('ผู้ที่จองไว้ ต้องทำการยกเลิกการจองก่อน คุณจึงจะสามารถทำรายการนี้ได้ครับ');";
				}else if($_SESSION['SESSION_ID_card'] == $process_by) {
					$TRonclick = "";
					$title = "คุณได้จองรายการนี้ไว้แล้ว คลิกที่แถบสีเหลือง เพื่อทำรายการต่อ";
					$click 	= "browseCheck('follow_content','$followPage?operator=wait_main_event&event_id_ref=$fl_event_id&cus_no=$cus_no&$backURL','');";
					$bgColor = "yellow";
					$cancel = "<img src='img/cancel_process.png' align='absbottom' title='ยกเลิกการจอง' onclick=\"browseCheck('process$follow_id','$followPage?operator=cancelProcess4&cus_no=$cus_no&event_id_in=$fl_event_id&tr_id=$cus_no','');document.getElementById('process$follow_id').style.backgroundColor='#ffffff';this.style.display='none';\">";
					$booking = "<img src='img/process_by.png'><span id = 'booking_emp_name".$cus_no."'>คุณได้จองรายการนี้ไว้แล้ว คลิกที่นี่ เพื่อทำรายการต่อ</span>";

				}
			}

		$wait_list .= $tpl->tbHtml( $followHTML, 'WAIT_LIST' );

		}
		if($wait_list==""){
			//echo "ไม่มีรายการ";
			$colspan = 4;
			$wait_list = $tpl->tbHtml( $followHTML, 'NULL' );
		}

		echo $tpl->tbHtml( $followHTML, 'WAIT_TABLE' );

		break;

	case 'cancel_follow':
		
		/*echo 'POST : '; print_r($_POST);
		echo "<br/>";
		echo 'GET : '; print_r($_GET);
		exit();*/
		
		if (!empty($_POST['check'])) {

			$action = $_GET['action'];

			switch ($action) {
				case "yes":
					$status = "1";
					$remark = "อนุมัติ ให้ยกเลิกการติดตาม";
					$success = "<div align='center' style='color:green'>อนุมัติ รายการที่ต้องการยุติการติดตามเรียบร้อย</div>";
				break;
				case "no":
					$status = "2"; //เนื่องจากรายการที่ขออนุมัติ คือรายการที่สุ่มขึ้นมาแล้ว
					$remark = "ไม่อนุมัติ การยกเลิกการติดตาม";
					$success = "<div align='center' style='color:blue'>ไม่อนุมัติ รายการที่เลือกเรียบร้อย</div>";
				break;
			}
	
			foreach($_POST['check'] AS $key=>$value){

				$sql 			= "SELECT cancel_type,cus_no,chassi_no_s,remark,biz_id_ref,event_id_ref ";
				$sql 			.= "FROM $config[db_base_name].follow_customer ";
				$sql 			.= "WHERE id = '".$value."' LIMIT 1";
				$res2 			= $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$follow_row 	= mysql_fetch_assoc($res2);
				$cancel_type 	= trim($follow_row['cancel_type']);
				$cus_no 		= $follow_row['cus_no'];

				### start nuy edit 08-02-56
				switch($cancel_type){
					case 'stopCus':
						if($action=='yes'){//ถ้าให้ยกเลิก เคลียร์ผู้ดูหลังการขายด้วย
							
							$insert  = "INSERT INTO $config[db_base_name].cancel_follow_customer(CANCEL_ID,BU_REF,CusNo) ";
							$insert .= "VALUES(NULL,'".$follow_row['biz_id_ref']."','".$cus_no."')";
							
							$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							
						}else{//ถ้าไม่อนุมัติ ให้คืนค่าเฉพาะที่เลือกยุติลูกค้า แยกต่างหากกับยุติกิจกรรม/ยุติรถ
							
						}
						
						//cus_no ทุกเรคอร์ดจะเป็น status = 90 อยู่แล้วจากตอนเสนอฯ
						$whereD  = "status='90' AND cus_no = '$cus_no' ";
						$whereD .= "AND cancel_type = 'stopCus'";
						
						//cus_no ทุกเรคอร์ดจะเป็น status_confirm = 0 อยู่แล้วจากตอนเสนอฯ
						$whereC  = "status_confirm='0' AND cus_no = '$cus_no'";
					break;
					case 'stopCar':
						
						if($action=='yes'){//ถ้าให้ยกเลิก เคลียร์ผู้ดูหลังการขายด้วย
							
							$relID  = "SELECT RELATES_ID FROM $config[db_vehicle].VEHICLE_RELATIONSHIP ";
							$relID .= "WHERE RELATES_VEHICLE_ID='".$follow_row['chassi_no_s']."' AND ";
							$relID .= "RELATES_CUS_ID='".$cus_no."' AND RELATES_STATUS!='99' LIMIT 1";
							
							$qrelID = $logDb->queryAndLogSQL( $relID, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							
							$frelID = mysql_fetch_assoc($qrelID);
							
							$insert  = "INSERT INTO $config[db_base_name].cancel_follow_car(CANCEL_ID,BU_REF,CANCEL_RELATES_ID) ";
							$insert .= "VALUES(NULL,'".$follow_row['event_id_ref']."',";
							$insert .= "'".$frelID['RELATES_ID']."')";
							
							$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							
							
						}else{//ถ้าไม่อนุมัติ ให้คืนค่าเฉพาะที่เลือกยุติลูกค้า แยกต่างหากกับยุติกิจกรรม/ยุติรถ
							
						}
						
						//cus_no ทุกเรคอร์ดจะเป็น status = 90 อยู่แล้วจากตอนเสนอฯ
						$whereD  = "status='90' AND chassi_no_s = '".$follow_row['chassi_no_s']."' ";
						$whereD .= "AND cancel_type = 'stopCar'";
						
						//cus_no ทุกเรคอร์ดจะเป็น status_confirm = 0 อยู่แล้วจากตอนเสนอฯ
						$whereC  = "status_confirm='0' AND chassi_no_s = '".$follow_row['chassi_no_s']."'";
					break;
					case 'stopEvent':
						
						if($action=='yes'){//ถ้าให้ยกเลิก เคลียร์ผู้ดูหลังการขายด้วย
							$insert  = "INSERT INTO $config[db_base_name].cancel_follow_event(CANCEL_ID,BU_REF,CusNo,EVENT_ID) ";
							$insert .= "VALUES(NULL,'".$follow_row['biz_id_ref']."',";
							$insert .= "'".$cus_no."','".$follow_row['event_id_ref']."')";
							
							$logDb->queryAndLogSQL( $insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
							
							//cus_no ทุกเรคอร์ดจะเป็น status = 90 อยู่แล้วจากตอนเสนอฯ
							$whereD  = "status='90' AND cus_no = '".$cus_no."' ";
							$whereD .= "AND cancel_type = 'stopEvent' AND event_id_ref='".$follow_row['event_id_ref']."' ";
							$whereD .= "AND biz_id_ref='".$follow_row['biz_id_ref']."'";
							
							//cus_no ทุกเรคอร์ดจะเป็น status_confirm = 0 อยู่แล้วจากตอนเสนอฯ
							$whereC  = "status_confirm='0' AND cus_no = '".$cus_no."' ";
							$whereC .= "AND event_id_ref='".$follow_row['event_id_ref']."' ";
							$whereC .= "AND biz_id_ref='".$follow_row['biz_id_ref']."'";
							
						}else{//ถ้าไม่อนุมัติ ให้คืนค่าเฉพาะที่เลือกยุติลูกค้า แยกต่างหากกับยุติกิจกรรม/ยุติรถ
							//cus_no ทุกเรคอร์ดจะเป็น status = 90 อยู่แล้วจากตอนเสนอฯ
							$whereD  = "status='90' AND cus_no = '".$cus_no."' ";
							$whereD .= "AND cancel_type = 'stopEvent' AND event_id_ref='".$follow_row['event_id_ref']."' ";
							$whereD .= "AND biz_id_ref='".$follow_row['biz_id_ref']."'";
							
							//cus_no ทุกเรคอร์ดจะเป็น status_confirm = 0 อยู่แล้วจากตอนเสนอฯ
							$whereC  = "status_confirm='0' AND cus_no = '".$cus_no."' ";
							$whereC .= "AND event_id_ref='".$follow_row['event_id_ref']."' ";
							$whereC .= "AND biz_id_ref='".$follow_row['biz_id_ref']."'";
						}
						
					break;
					case 'stopTemporary':
						if($action=='yes'){
							$whereD  = "status='90' AND id = ".$value;
							$whereD .= " AND cancel_type = 'stopTemporary'";
							
							//cus_no ทุกเรคอร์ดจะเป็น status_confirm = 0 อยู่แล้วจากตอนเสนอฯ
							// $whereC  = "id = ".$value. " AND status_confirm='0' AND cancel_type = 'stopTemporary'" ;
							$whereC  = "id = ".$value. " AND status_confirm='0' " ;
						}else{

							$whereD  = "status='90' AND id = ".$value;
							$whereD .= " AND cancel_type = 'stopTemporary'";
							
							//cus_no ทุกเรคอร์ดจะเป็น status_confirm = 0 อยู่แล้วจากตอนเสนอฯ
							// $whereC  = "id = ".$value." AND status_confirm='0' AND cancel_type = 'stopTemporary'" ;
							$whereC  = "id = ".$value. " AND status_confirm='0' " ;
						}
					break;
				}

				if($action=='yes'){
					$del     = "DELETE FROM $config[db_base_name].follow_customer WHERE $whereD";
					$result  = $logDb->queryAndLogSQL( $del, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					
					$update  = "UPDATE $config[db_base_name].follow_customer_cancel ";
					$update .= "SET status_confirm='".$status."',idEmp_cancel='".$_SESSION['SESSION_ID_card']."',";
					$update .= "cancel_remark='".$remark."',remark='".$follow_row['remark']."', ";
					$update .= "cancel_type='".$cancel_type."',cancel_time=NOW() WHERE $whereC";				
					$result  = $logDb->queryAndLogSQL( $update, " FILE : ".__FILE__." LINE : ".__LINE__."" );			
				}else{
					$del 	= "UPDATE $config[db_base_name].follow_customer SET status = '0',cancel_type='',remark='' WHERE $whereD";
					$result = $logDb->queryAndLogSQL( $del, " FILE : ".__FILE__." LINE : ".__LINE__."" );
					
					$update = "UPDATE $config[db_base_name].follow_customer_cancel SET status_confirm='".$status."',idEmp_cancel='".$_SESSION['SESSION_ID_card']."',";
					$update .= "cancel_remark='".$remark."',remark='".$follow_row['remark']."',cancel_type='".$cancel_type."',cancel_time=NOW() WHERE $whereC";
					$result = $logDb->queryAndLogSQL( $update, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				}
			} // end foreach
		} // end if

		echo $success;
		//นับจำนวนรายการที่รออนุมัติ เพื่ออัพเดตค่าบนเมนูปุ่มกด
		$approve_num = 0;
		//$emp_id_card = get_emp_saleTemp($_SESSION['SESSION_ID_card'], 'id_card');
		if ($emp_id_card) {
			/*if ($CheckManager == $config['Manager']) {
				$where = "";
			} else {
				$where = "AND emp_id_card IN($emp_id_card)";
			}*/
			
			$where = "AND (process_by IN($emp_id_card) OR emp_id_card IN($emp_id_card))";
			$sql = "SELECT id FROM $config[db_base_name].follow_customer WHERE status = '90' $where GROUP by cus_no,cancel_type";
			// echo $sql;
			$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$approve_num = mysql_num_rows($re);
		}

		echo "<script type='text/javascript'>
			document.getElementById('approve_num').innerHTML = '$approve_num';
			browseCheck('follow_content','$followPage?operator=wait_for_approve','');
		</script>";
		//end

	break;
	case 'ic_eventcus':
		$getmonth	=getOptionMonth();
		$getyear	=getOptionYear();
		
		$recordby=" and SUBSTRING_INDEX(TelCusBirthday.Recorder,'_',1) = '$_SESSION[SESSION_ID_card]' AND Recorder_Position_Code = '$_SESSION[SESSION_member_id]' "; //PT@OU 
		
		if(isset($_GET['click'])){
			$sql_SUpdate="SELECT custel.id,custel.Cus_No,custel.Viewer,custel.Topic,custel.Detail,custel.Recorder,custel.Record_date,
							concat(custel.Cus_No,'_',cusdata.Cus_Name,' ',cusdata.Cus_Surename) as cusname,custel.alert_date,custel.edit_by
							FROM $config[db_base_name].TelCusBirthday as custel
							Left join $config[db_maincus].MAIN_CUS_GINFO as cusdata ON cusdata.CusNo=custel.Cus_No
							WHERE custel.status!='99' and custel.id='$_GET[click]' ";
			$rs_SUpdate	=$logDb->queryAndLogSQL( $sql_SUpdate, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$dataviewer	=mysql_fetch_assoc($rs_SUpdate);
		
			$alertdate=explode('/',$dataviewer[alert_date]);
		
		
			$dataviewer[edit_by]!=''?$editby="<b>แก้ไขโดย</b> #$dataviewer[edit_by]":$editby='';
			$datepost	=explode(' ',$dataviewer[Record_date]);
			$dateadd=date('d-m-',strtotime($datepost[0])).(date('Y',strtotime($datepost[0]))+543);
		
		
			$nl2brdetail=nl2br($dataviewer[Detail]);
			$viewer=explode(',',$dataviewer[Viewer]);
			if(!in_array($_SESSION[SESSION_ID_card],$viewer)){
				$sql_update=" UPDATE $config[db_base_name].TelCusBirthday SET Viewer=CONCAT('$dataviewer[Viewer]','$_SESSION[SESSION_ID_card],') WHERE  id='$_GET[click]' ";
				$logDb->queryAndLogSQL( $sql_update, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			}
			$idcard=explode('_',$dataviewer[Recorder]);
			if($_SESSION['SESSION_ID_card']==$idcard[0]){
				/*$operator="<div class=\"ชิดขวา\"><span><a href=\"javascript:void(0);\" onclick='editevent(\"$_GET[click]\")'><img src=\"img/white_edit.png\" title=\"แก้ไข\"></a></span>
				<span class=\"สุดท้าย\"><a href=\"javascript:void(0);\" onclick='deleteevent(\"$_GET[click]\")'><img src=\"img/delete.png\" title=\"ลบ\"></a><span></div>";*/
				$operator="<div class=\"ชิดขวา\">
						<span>
							<a href=\"javascript:void(0);\" onclick='showQuestionDetail(\"$followPage?operator=ic_eventcus&eventform=eventform&eventID=$_GET[click]\",\"\",\"beginBox\");'><img src=\"img/white_edit.png\" title=\"แก้ไข\"></a>
						</span>
						<span class=\"สุดท้าย\">
							<a href=\"javascript:void(0);\" onclick='deleteevent(\"$followPage?operator=ic_eventcus\",\"$_GET[click]\")'><img src=\"img/delete.png\" title=\"ลบ\"></a>
						<span>
					</div>";
			}
			echo $tpl->tbHtml($followHTML,'EVENTDETAIL');
			exit;
		}
		if(isset($_GET['delete']) and $_GET['delete']!=''){	//#############ลบ
			$sql_update=" UPDATE $config[db_base_name].TelCusBirthday SET status='99' WHERE id='$_GET[delete]' LIMIT 1";
			$logDb->queryAndLogSQL( $sql_update, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		}
		if(isset($_GET['eventform'])){		//#############แสดงกล่องเพิมเหตุการณ์
			
			if($_GET['eventID']){
				$sql = "SELECT custel.id,custel.Cus_No,custel.Viewer,custel.Topic,custel.Detail,custel.Recorder,custel.Record_date,
				concat(custel.Cus_No,'_',cusdata.Cus_Name,' ',cusdata.Cus_Surename) as cusname,custel.alert_date,custel.edit_by
				FROM $config[db_base_name].TelCusBirthday as custel
				Left join $config[db_maincus].MAIN_CUS_GINFO as cusdata ON cusdata.CusNo=custel.Cus_No
				WHERE custel.status!='99' and custel.id='".$_GET['eventID']."' LIMIT 1";
				$que = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
				$fe = mysql_fetch_assoc($que);
				
				$alertdate = explode('/',$fe['alert_date']);
				
				$getmonth	= getOptionMonth($alertdate[0]);
				$getyear	= getOptionYear($alertdate[1]);
				$_GET['cusname'] = $fe['cusname'];
				$cusID = $fe['Cus_No'];
				$topic = $fe['Topic'];
				$detail = $fe['Detail'];
				
				$idEvent = $fe['id'];
			}
			
			echo $tpl->tbHtml($followHTML,'ADDEVENT');
			exit;
		}
		if(isset($_POST['add2db'])){
			$cus_no=explode('_',$_POST['cusname']);
			$recorder	=$_SESSION[SESSION_ID_card].'_'.$_SESSION[SESSION_Name].'  '.$_SESSION[SESSION_Surname];
			mysql_query( "SET NAMES UTF8" );
			
			if($_POST['idEvent']){
				$sql_update="UPDATE $config[db_base_name].TelCusBirthday
				SET Cus_No='$cus_no[0]',Topic='$_POST[event]',Detail='$_POST[eventdetail]',
				alert_date='$_POST[smonth]/$_POST[syear]',edit_by='$recorder'
				WHERE id='$_POST[idEvent]' ";
				$logDb->queryAndLogSQL( $sql_update, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			}else{
				$sql_insert	="INSERT INTO $config[db_base_name].TelCusBirthday
				(eventtype,Cus_No,Topic,Detail,Recorder, Recorder_Position_Code,Record_date,alert_date) Values
				('2_event','$cus_no[0]','$_POST[event]','$_POST[eventdetail]','$recorder', '$_SESSION[SESSION_member_id]', now(),'$_POST[smonth]/$_POST[syear]')";
				$logDb->queryAndLogSQL( $sql_insert, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			}
		}
		/*if(isset($_POST['updateevent']) and $_POST['updateevent']!=""){
			$cus_no=explode('_',$_POST['cusname']);
			$recorder	=$_SESSION[SESSION_ID_card].'_'.$_SESSION[SESSION_Name].'  '.$_SESSION[SESSION_Surname];
			$sql_update="UPDATE $config[db_base_name].TelCusBirthday
			SET Cus_No='$cus_no[0]',Topic='$_POST[event]',Detail='$_POST[eventdetail]',
			alert_date='$_POST[smonth]/$_POST[syear]',edit_by='$recorder'
			WHERE id='$_POST[updateevent]' ";
			mysql_query($sql_update) or die(show_sql_error($sql_update,__FILE__,$line=__LINE__));
		}*/
		if(isset($_GET['searchcus'])){
			mysql_query( "SET NAMES UTF8" );
			$sname	=explode(' ',$_GET[searchcus],2);
			$sql_cus	="SELECT CusNo,Cus_Name,Cus_Surename
							FROM $config[db_maincus].MainCusData
							WHERE status!='99' and (Cus_Name like '%$sname[0]%' and Cus_Surename like '%$sname[1]%') Limit 150";
			$rscus=$logDb->queryAndLogSQL( $sql_cus, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$numrow	=mysql_num_rows($rscus);
			if($numrow==0){
				$none= 'ไม่พบข้อมูล';
			}else{
				while($row_cus=mysql_fetch_array($rscus)){
					$li_cus		.="<li style='padding:10px;'><a href='javascript:void(0);' onclick='selectcus(\"$row_cus[CusNo]_$row_cus[Cus_Name]  $row_cus[Cus_Surename]\");'>$row_cus[CusNo] - $row_cus[Cus_Name]  $row_cus[Cus_Surename]</a></li>";
				}
			}
			echo $tpl->tbHtml($followHTML,'CUSLIST');
			exit;
		}
		
		//END Condition
		
		if(isset($_GET['search'])){
			if(isset($_GET['Sname']) and $_GET['Sname']!=''){$Snames	=" and (cusdata.Cus_Name like '%$_GET[Sname]%' or cusdata.Cus_Surename  like '%$_GET[Sname]%') ";}
			if(isset($_GET['Smonth']) and $_GET['Smonth']!=0){$Smonths	="and  substr(TelCusBirthday.alert_date,1,1)='$_GET[Smonth]' ";}
			if(isset($_GET['Syear']) and $_GET['Syear']!=0){$Syears	=" and substr(TelCusBirthday.alert_date,3,4) ='$_GET[Syear]' ";}
		}else{
			$nowMY	=	date('n/').(date('Y')+543);
			$nosearch	=" and TelCusBirthday.alert_date='$nowMY' ";
		}
		
			mysql_query( "SET NAMES UTF8" );
			$sql ="SELECT TelCusBirthday.id,TelCusBirthday.eventtype,TelCusBirthday.Cus_No,TelCusBirthday.Emp_ID_Card,TelCusBirthday.Date_Tel,TelCusBirthday.Topic,
			TelCusBirthday.Detail,TelCusBirthday.Recorder,TelCusBirthday.Record_date,TelCusBirthday.Viewer,cusdata.Cus_Name,cusdata.Cus_Surename,
			substring(TelCusBirthday.Recorder,1,13),TelCusBirthday.alert_date FROM $config[db_base_name].TelCusBirthday Left join $config[db_maincus].MAIN_CUS_GINFO as 
			cusdata ON cusdata.CusNo=TelCusBirthday.Cus_No WHERE TelCusBirthday.status!='99' and TelCusBirthday.eventtype='2_event' $nosearch  $Snames $Smonths $Syears 
			$recordby ORDER by TelCusBirthday.Record_date desc";
		
			//echo '<hr>'.$sql;
		
			$rs=$logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$num_rows=mysql_num_rows($rs);
			if($num_rows==0){$nodata="ไม่พบข้อมูล";$shownavi="<tr><td colspan=5 align=center><div style=\"color:red;font:20px;\">$nodata</div></td></tr>";}
				######แบ่งหน้า
				$pagesize=10;
				$totalpage = ceil($num_rows_popup/$pagesize);
				if(!isset($_GET['gotopage'])){
					$startpage = 0;
				}else{
					$startpage = $pagesize * ($_GET['gotopage'] - 1);
				}
				$sql.=" LIMIT $startpage,$pagesize";
				$current = $_GET['gotopage'];
				if($num_rows){
					$nave = "";
					$nave .= '<div id="navigator" style="margin:0 auto;">';
					$nave .=page_navigator($current,$pagesize,5,$num_rows,'tbBirthday',"$followPage?operator=ic_eventcus&search=search&Sname=$_GET[Sname]&Smonth=$_GET[Smonth]&Syear=$_GET[Syear]");
					$nave .= '</div>';
					if($num_rows>$pagesize){
						$shownavi="<tr><td colspan=5 align=center>$nave <div style=\"color:red;font:20px;\">$nodata</div></td></tr>";
					}
				}
			$rs	=$logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
			$count = 1; //naizan
			while($row=mysql_fetch_array($rs)){
				//naizan edit
				if($count%2){
					$bgColor="style='background:#F3F3F3'";      // Gray color
				}else{
					$bgColor="style='background:#e9e9e9';";      // bold Gray color
				}
				$count++;
				//end naizan edit
		
				$datepost	=explode(' ',$row[Record_date]);
				$strdate=date('d-m-',strtotime($datepost[0])).(date('Y',strtotime($datepost[0]))+543);
				$showrecorder	=explode('_',$row[Recorder]);
				$viewer		=explode(',',$row[Viewer]);
				if(in_array($_SESSION[SESSION_ID_card],$viewer)){
					$message='<img src="images/message-already-read.png" title="อ่านแล้ว">'	;
				}else{
					$message='<img src="images/new-message.png" title="ยังไม่ได้อ่าน">';
				}
				$showrecorder[1]==""?$showrecorder[1]='n/a':$showrecorder[1];
				$eventlist .=$tpl->tbHtml($followHTML,'EVENTLIST');
			}
		//$addbox	= $tpl->tbHtml($followHTML,'ADDEVENT');
		if(!isset($_GET['search'])){
			$cus_event = $tpl->tbHtml($followHTML,'CONTAINER');
			$cus_event .= $tpl->tbHtml($followHTML,'TABLE');
			echo $tpl->tbHtml($followHTML,'MAINEVENT');
		}else{
			echo $tpl->tbHtml($followHTML,'TABLE');
		}
	break;
	case 'lastCall':
		/* หารายการติดตามล่าสุด และ ตาม $fl_cus_id */
		$event_id 	= $_GET['event_id'];
		$chassi_no 	= $_GET['chassi_no'];
		$cus_no 	= $_GET['cus_no'];

		$fl_cus_id  = $_GET['follow_id'];		//จากการคลิกของหัวหน้า ในหน้ารายการที่เคยสัมภาษณ์ของลูกน้อง ic_followcus2.php?operator=archives
		$all_cus_data = $_GET['all_cus_data'];	//ส่งมาจากหน้า รายชื่อลูกค้าทั้งหมด ic_followcus.php?operator=getAllCustomer
		#-- คลิกเข้ามาจากหน้ารอสัมภาษณ์
		if($fl_cus_id == "" && $all_cus_data == ""){
			$prev_sql = "SELECT event_id_ref FROM $config[db_base_name].follow_event_refer ";
			$prev_sql .= "WHERE next_event_id='".$event_id."' AND status !='99'";

			$event_id = query($prev_sql);//หา event_id ก่อนที่จะส่งมารอสัมภาษณ์
			//หา id ล่าสุดที่ทำรายการของเหตุการณ์ที่ได้นี้
			$fl_cus_id = query( "SELECT MAX(id) FROM $config[db_base_name].follow_customer WHERE event_id_ref='".$event_id."' AND chassi_no_s='".$chassi_no."' AND cus_no='".$cus_no."' AND status='2'");//หาไอดี อ้างอิงที่มากที่สุด  (รายการล่าสุด)
		#-- รายการที่ทำล่าสุด จากหน้าลูกค้าที่ดูแลทั้งหมด
		}else if($fl_cus_id == "" && $all_cus_data == "lastdata") {
			$all_cus_sql = "SELECT id,event_id_ref FROM $config[db_base_name].follow_customer ";
			$all_cus_sql .= "WHERE cus_no ='$cus_no'  AND status ='2' ";
			$all_cus_sql .= "ORDER by id desc LIMIT 1";//เรียงไอดีมากไปหาน้อย จะได้ id ที่มากที่สุด
			$all_result  = query($all_cus_sql,1);

			$event_id    = $all_result['event_id_ref'];//ไอดี เหตุการณ์
			$fl_cus_id   = $all_result['id'];			//(รายการล่าสุด)
		}
		//ดึงข้อมูลเหตุการณ์มาแสดง
		$sql = "SELECT event_title,status FROM $config[db_base_name].follow_main_event WHERE id='".$event_id."'";
		$headQuery = query($sql,1);
		if($headQuery['status']=="99"){
			$headQuery['event_title'] = "<span class='red'>กิจกรรรมนี้ได้ถูกยกเลิกไปแล้ว <span style='font-size:12px'>($headQuery[event_title])</span></span>";
		}

		$head = "<div align='center' class='title1'><div style='width:99%;text-align:left'>ชื่อกิจกรรม :: $headQuery[event_title] </div></div>";
		// แสดงข้อมูลที่ลูกค้าตอบ
		$sql  = " SELECT * FROM $config[db_base_name].follow_answer ";
		$sql .= " Inner Join $config[db_base_name].follow_question_list ";
		$sql .= " ON follow_answer.ques_list_id = follow_question_list.id ";
		$sql .= " WHERE follow_answer.follow_customer_id='".$fl_cus_id."' ";
		$sql .= " GROUP BY ques_list_id ";
		$sql .= " ORDER BY follow_question_list.order_number ASC ";
		
		// $sql .= " , follow_answer.main_ques_id ASC, ";
		// $sql .= " follow_answer.ques_list_id  ASC ";
		
		// echo $sql;
		
		$re = $logDb->queryAndLogSQL( $sql, " FILE : ".__FILE__." LINE : ".__LINE__."" );
		$main_ques_id 	= "";
		
		$headAns		= "";
		$arrPleasure	= array();
		$arrNormal		= array();
		
		$numrow = mysql_num_rows($re);
		while($ans=mysql_fetch_array($re)){
			
			if($main_ques_id != $ans['main_ques_id']){  //ถ้าเป็นหัวข้อใหม่
				
				$num++;
				$emp_name     =  select_emp_nametonickname($ans['emp_id_card']);
				$main_ques_id =  $ans['main_ques_id'];
				
				$main_ques = query("SELECT question_title FROM $config[db_base_name].follow_main_question 
				WHERE id='".$main_ques_id."'",1);
				
				$headAns 	.= "<div class='title2'>หัวข้อคำถามหลัก : $main_ques[question_title]</div>";
				$headAns 	.= "<div class='title2'>ผู้ทำรายการ : $emp_name</div>";
				$headAns 	.= "<div class='title3' style='margin-top:5px'><u>รายการคำถามแบบฟอร์มที่ $num</u></div>";

			}else{//ถ้าไม่ใช่หัวข้อใหม่ให้ทิ้งเป็นค่าว่าง
				$main_ques['question_title']='&nbsp';
			}

			//$question_list = query("SELECT list_title,input_type FROM $config[db_base_name].follow_question_list
			//WHERE id='".$ans['ques_list_id']."' ORDER by order_number ",1);
			//echo "<br><br>",$ans['list_title'];
			
			$question_list['input_type'] = $ans['input_type'];
			$question_list['list_title'] = $ans['list_title'];
			
			//echo "<br>>> ",$question_list['list_title'];
			
			$answer = str_replace("'"," ",$ans['answer']);
			
			if($question_list['input_type'] == "pleasure_value"){ //แยกความพึงพอใจไว้หลังสุด
				$arrPleasure[] = 	"<div class='title3' style='padding:3px;margin-top:10px'>
									ข้อที่ ##runNo##. ".$question_list['list_title']." </div>
									<div class='content' style='padding:1px;margin-left:55px'>
									<u>คำตอบ</u> <font color='orangered'>$answer</font></div>";
				
			}else {//คำถามทั่วไป
				$arrNormal[] 	= 	"<div class='title3' style='padding:3px;margin-top:10px'>
									ข้อที่ ##runNo##. ". $question_list['list_title'] ." </div>
									<div class='content' style='padding:1px;margin-left:55px'>
									<u>คำตอบ</u> <font color='orangered'>$answer</font></div>";
			}
		}//while loop
		
		$list	= "";
		$no		= 1;
		if(!empty($arrPleasure) || !empty($arrNormal)){
		
			if(!empty($arrPleasure)){
				foreach($arrPleasure AS $key=>$value){
					$list	.= str_replace('##runNo##.',$no,$value);
					
					$no++;
				}
			}
		
			if(!empty($arrNormal)){
				foreach($arrNormal AS $key=>$value){
					$list	.= str_replace('##runNo##.',$no,$value);
					
					$no++;
				}	
			}
		
			$table = $head.'<div align="center"><div style="margin-top:5px;padding:10px;width:99%;border:1px solid" align="left">';
			$table .= $headAns.$list;
			$table .= '</div></div>';

			echo $table;
		}else{
			echo "<center>ไม่มีข้อมูลล่าสุดของรายการนี้</center>";
		}

	break;
}
CloseDB();
?>